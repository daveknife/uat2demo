<?php

/**
 * Fired during plugin activation
 *
 * @link       https://azontables.com
 * @since      1.0.0
 *
 * @package    azontables_Pluginname
 * @subpackage azontables_Pluginname/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    azontables_Pluginname
 * @subpackage azontables_Pluginname/includes
 * @author     Your Name <email@example.com>
 */
class azontables_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {




		/**
		 * Set up our default licensing values
		 *
		 * @since    1.0.0
		 */

		$azontables_license = get_option( 'azontables_license' );
	    // Are our options saved in the DB?
	    if ( false === $azontables_license ) {
	        // If not, we'll save our default options
	        add_option( 'azontables_license', '' );
	        add_option( 'azontables_license_status', 'deactivated' );
	    }


		


	}

}
