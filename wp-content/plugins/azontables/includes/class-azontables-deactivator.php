<?php

/**
 * Fired during plugin deactivation
 *
 * @link       https://azontables.com
 * @since      1.0.0
 *
 * @package    azontables_Pluginname
 * @subpackage azontables_Pluginname/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    azontables_Pluginname
 * @subpackage azontables_Pluginname/includes
 * @author     Your Name <email@example.com>
 */
class azontables_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

	

}
