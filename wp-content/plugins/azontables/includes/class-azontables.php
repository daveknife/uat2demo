<?php

/**
 * The file that defines the core plugin class
 *
 * A class definition that includes attributes and functions used across both the
 * public-facing side of the site and the admin area.
 *
 * @link       https://azontables.com
 * @since      1.0.0
 *
 * @package    azontables_Pluginname
 * @subpackage azontables_Pluginname/includes
 */

/**
 * The core plugin class.
 *
 * This is used to define internationalization, admin-specific hooks, and
 * public-facing site hooks.
 *
 * Also maintains the unique identifier of this plugin as well as the current
 * version of the plugin.
 *
 * @since      1.0.0
 * @package    azontables_Pluginname
 * @subpackage azontables_Pluginname/includes
 * @author     Your Name <email@example.com>
 */
class azontables {

	/**
	 * The loader that's responsible for maintaining and registering all hooks that power
	 * the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      azontables_Loader    $loader    Maintains and registers all hooks for the plugin.
	 */
	protected $loader;

	/**
	 * The unique identifier of this plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $azontables    The string used to uniquely identify this plugin.
	 */
	protected $azontables;

	/**
	 * The current version of the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $version    The current version of the plugin.
	 */
	protected $version;

	/**
	 * Define the core functionality of the plugin.
	 *
	 * Set the plugin name and the plugin version that can be used throughout the plugin.
	 * Load the dependencies, define the locale, and set the hooks for the admin area and
	 * the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function __construct() {

		$this->azontables = 'azontables';
		$this->version = '1.0.3';
		$this->edd_product = 'Azon Tables';
		$this->edd_store = 'http://azontables.com';

		$this->load_dependencies();
		$this->set_locale();
		$this->define_admin_hooks();
		$this->define_public_hooks();

	}

	/**
	 * Load the required dependencies for this plugin.
	 *
	 * Include the following files that make up the plugin:
	 *
	 * - azontables_Loader. Orchestrates the hooks of the plugin.
	 * - azontables_i18n. Defines internationalization functionality.
	 * - azontables_Admin. Defines all hooks for the admin area.
	 * - azontables_Public. Defines all hooks for the public side of the site.
	 *
	 * Create an instance of the loader which will be used to register the hooks
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function load_dependencies() {

		/**
		 * The class responsible for orchestrating the actions and filters of the
		 * core plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-azontables-loader.php';

		/**
		 * The class responsible for defining internationalization functionality
		 * of the plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-azontables-i18n.php';

		/**
		 * The class responsible for defining all actions that occur in the admin area.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/class-azontables-admin.php';

		/**
		 * The class responsible for defining all actions that occur in the public-facing
		 * side of the site.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'public/class-azontables-public.php';

		// 4. Include ACF
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/advanced-custom-fields-pro/acf.php';

		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-azontables-amazon-functions.php';


		if( !class_exists( 'AZONTABLES_Plugin_Updater' ) ) {
			// load our custom updater
			require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/partials/AZONTABLES_Plugin_Updater.php';
		}

		$this->loader = new azontables_Loader();

	}

	/**
	 * Define the locale for this plugin for internationalization.
	 *
	 * Uses the azontables_i18n class in order to set the domain and to register the hook
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function set_locale() {

		$plugin_i18n = new azontables_i18n();

		$this->loader->add_action( 'plugins_loaded', $plugin_i18n, 'load_plugin_textdomain' );

	}

	/**
	 * Register all of the hooks related to the admin area functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_admin_hooks() {

		$plugin_admin = new azontables_Admin( $this->get_azontables(), $this->get_version(), $this->edd_product, $this->edd_store );

		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_styles' );
		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_scripts' );

		// load ACF fields for this plugin
		$this->loader->add_action( 'acf/init', $plugin_admin, 'azontables_include_acf_fields' );
		
		// load updater function
		$this->loader->add_action( 'admin_init', $plugin_admin, 'azontables_plugin_updater', 0 );

		$this->loader->add_action( 'azontables_add_extension_license', $plugin_admin, 'azontables_license_management' );

		$this->loader->add_action( 'init', $plugin_admin, 'azontables_create_post_type' );

		$this->loader->add_action( 'admin_menu', $plugin_admin, 'azontables_menu_options', 100 );

		$this->loader->add_action( 'admin_footer', $plugin_admin, 'amazon_search_thickbox' );

		$this->loader->add_action( 'activate_azontables', $plugin_admin, 'azontables_options_init' );

		// Add amazon api lookup ajax function
		$this->loader->add_action( 'wp_ajax_azontables_find_amazon_products_ajax', $plugin_admin, 'azontables_find_amazon_products_ajax' );

		// Add amazon api lookup ajax function to load new pages of results
		$this->loader->add_action( 'wp_ajax_azontables_load_amazon_products_newpage_ajax', $plugin_admin, 'azontables_load_amazon_products_newpage_ajax' );

		// get the asin amazon info for displaying on the asontable creation page
		$this->loader->add_action( 'wp_ajax_get_asins_info_for_admin', $plugin_admin, 'get_asins_info_for_admin' );

		$this->loader->add_filter( 'manage_posts_columns', $plugin_admin, 'azontables_admin_column', 5 );

		$this->loader->add_action( 'manage_posts_custom_column', $plugin_admin, 'azontables_admin_column_content',5,2 );

		$this->loader->add_action( 'acf/save_post', $plugin_admin, 'azontables_save_table_array', 50 );

		$this->loader->add_action( 'wp_ajax_activate_azontables_license', $plugin_admin, 'activate_azontables_license' );

		$this->loader->add_action( 'wp_ajax_deactivate_azontables_license', $plugin_admin, 'deactivate_azontables_license' );

		$this->loader->add_filter( 'acf/settings/path', $plugin_admin, 'azontables_acf_settings_path');
		$this->loader->add_filter( 'acf/settings/dir', $plugin_admin, 'azontables_acf_settings_dir');
		

	}

	/**
	 * Register all of the hooks related to the public-facing functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_public_hooks() {

		$plugin_public = new azontables_Public( $this->get_azontables(), $this->get_version() );

		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_styles' );
		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_scripts' );

		$this->loader->add_action( 'wp_head', $plugin_public, 'azontables_add_custom_ext_css_tables' );

		// add our shortcode function for displaying a table
		$this->loader->add_shortcode( 'azontables', $plugin_public, 'azontables_layout_table' );

		// add our shortcode function for displaying a table
		$this->loader->add_shortcode( 'azontables', $plugin_public, 'azontables_layout_table' );

		$this->loader->add_filter( 'the_content', $plugin_public, 'add_azon_table_to_single');


	}

	/**
	 * Run the loader to execute all of the hooks with WordPress.
	 *
	 * @since    1.0.0
	 */
	public function run() {
		$this->loader->run();
	}

	/**
	 * The name of the plugin used to uniquely identify it within the context of
	 * WordPress and to define internationalization functionality.
	 *
	 * @since     1.0.0
	 * @return    string    The name of the plugin.
	 */
	public function get_azontables() {
		return $this->azontables;
	}

	/**
	 * The reference to the class that orchestrates the hooks with the plugin.
	 *
	 * @since     1.0.0
	 * @return    azontables_Loader    Orchestrates the hooks of the plugin.
	 */
	public function get_loader() {
		return $this->loader;
	}

	/**
	 * Retrieve the version number of the plugin.
	 *
	 * @since     1.0.0
	 * @return    string    The version number of the plugin.
	 */
	public function get_version() {
		return $this->version;
	}

}
