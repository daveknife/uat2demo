<?php

/**
 * Hold all Amazon Product Advertising API functions
 *
 * @link       https://azontables.com
 * @since      1.0.0
 *
 * @package    azontables
 * @subpackage azontables/includes
 */

/**
 * Hold all Amazon Product Advertising API functions
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    azontables
 * @subpackage azontables/includes
 * @author     Your Name <email@example.com>
 */
class azontables_amazon_api {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */



	public function amazon_api_get_endpoint() {


		$locale = trim( get_option( 'azontables-options_default_amazon_search_locale' ) );

		// The region you are interested in
		if ( $locale == 'US' ) {
			$endpoint = "webservices.amazon.com";
		}
		else if ( $locale == 'UK' ) {
			$endpoint = "webservices.amazon.co.uk";
		}
		else if ( $locale == 'BR' ) {
			$endpoint = "webservices.amazon.com.br";
		}
		else if ( $locale == 'CA' ) {
			$endpoint = "webservices.amazon.ca";
		}
		else if ( $locale == 'CN' ) {
			$endpoint = "webservices.amazon.cn";
		}
		else if ( $locale == 'FR' ) {
			$endpoint = "webservices.amazon.fr";
		}
		else if ( $locale == 'DE' ) {
			$endpoint = "webservices.amazon.de";
		}
		else if ( $locale == 'IN' ) {
			$endpoint = "webservices.amazon.in";
		}
		else if ( $locale == 'IT' ) {
			$endpoint = "webservices.amazon.it";
		}
		else if ( $locale == 'JP' ) {
			$endpoint = "webservices.amazon.co.jp";
		}
		else if ( $locale == 'ES' ) {
			$endpoint = "webservices.amazon.es";
		}
		else if ( $locale == 'MX' ) {
			$endpoint = "webservices.amazon.com.mx";
		}

		return $endpoint;

	}




	public function amazon_api_request( $post_id=null, $response_group, $keywords ) {


		/****************************************
		*** Get our product info from the api ***
		****************************************/

		// http://webservices.amazon.com/scratchpad/index.html

		$locale = trim( get_option( 'azontables-options_default_amazon_search_locale' ) );

		// Your AWS Access Key ID, as taken from the AWS Your Account page
		$aws_access_key_id = trim( get_option( 'azontables-options_amazon_api_key' ) );

		// Your AWS Secret Key corresponding to the above ID, as taken from the AWS Your Account page
		$aws_secret_key = trim( get_option( 'azontables-options_amazon_api_secret' ) );

		$affiliate_id = trim( get_option( 'azontables-options_amazon_affiliate_id_'.$locale ) );

		if ( $aws_access_key_id && $aws_secret_key && $affiliate_id ) {

			$endpoint = $this->amazon_api_get_endpoint();
			

			$uri = "/onca/xml";

			$params = array(
			    "Service" => "AWSECommerceService",
			    "Operation" => "ItemSearch",
			    "AWSAccessKeyId" => $aws_access_key_id,
			    "AssociateTag" => $affiliate_id,
			    "SearchIndex" => "All",
			    "Keywords" => $keywords,
			    "ResponseGroup" => $response_group
			);

			// Set current timestamp if not set
			if (!isset($params["Timestamp"])) {
			    $params["Timestamp"] = gmdate('Y-m-d\TH:i:s\Z');
			}

			// Sort the parameters by key
			ksort($params);

			$pairs = array();

			foreach ($params as $key => $value) {
			    array_push($pairs, rawurlencode($key)."=".rawurlencode($value));
			}

			// Generate the canonical query
			$canonical_query_string = join("&", $pairs);

			// Generate the string to be signed
			$string_to_sign = "GET\n".$endpoint."\n".$uri."\n".$canonical_query_string;

			// Generate the signature required by the Product Advertising API
			$signature = base64_encode(hash_hmac("sha256", $string_to_sign, $aws_secret_key, true));

			// Generate the signed URL
			$request_url = 'https://'.$endpoint.$uri.'?'.$canonical_query_string.'&Signature='.rawurlencode($signature);

			$amazon_request_return = wp_remote_get($request_url);
			$request_response = $amazon_request_return['response']['code'];

			if ( $request_response == '200' ) {
				$api_response = wp_remote_retrieve_body($amazon_request_return);
			}
			else {
				$api_response = 'NOT 200 - Request Denied ny Amazon';
			}


			return $api_response;

		}
		else {
			return 'No Credentials';
		}


	}





	public function amazon_api_item_lookup( $post_id, $response_group, $asin ) {

		/****************************************
		*** Get our product info from the api ***
		****************************************/

		// http://webservices.amazon.com/scratchpad/index.html

		$locale = trim( get_option( 'azontables-options_default_amazon_search_locale' ) );

		// Your AWS Access Key ID, as taken from the AWS Your Account page
		$aws_access_key_id = trim( get_option( 'azontables-options_amazon_api_key' ) );

		// Your AWS Secret Key corresponding to the above ID, as taken from the AWS Your Account page
		$aws_secret_key = trim( get_option( 'azontables-options_amazon_api_secret' ) );

		$affiliate_id = trim( get_option( 'azontables-options_amazon_affiliate_id_'.$locale ) );

		$endpoint = $this->amazon_api_get_endpoint();
		

		$uri = "/onca/xml";

		$params = array(
		    "Service" => "AWSECommerceService",
		    "Operation" => "ItemLookup",
		    "IdType" => "ASIN",
		    "ItemId" => $asin,
		    "AWSAccessKeyId" => $aws_access_key_id,
		    "AssociateTag" => $affiliate_id,
		    "ResponseGroup" => $response_group
		);

		//http://webservices.amazon.com/onca/xml?


		//print_r($params);

		// Set current timestamp if not set
		if (!isset($params["Timestamp"])) {
		    $params["Timestamp"] = gmdate('Y-m-d\TH:i:s\Z');
		}

		// Sort the parameters by key
		ksort($params);

		$pairs = array();

		foreach ($params as $key => $value) {
		    array_push($pairs, rawurlencode($key)."=".rawurlencode($value));
		}

		// Generate the canonical query
		$canonical_query_string = join("&", $pairs);

		// Generate the string to be signed
		$string_to_sign = "GET\n".$endpoint."\n".$uri."\n".$canonical_query_string;

		// Generate the signature required by the Product Advertising API
		$signature = base64_encode(hash_hmac("sha256", $string_to_sign, $aws_secret_key, true));

		// Generate the signed URL
		$request_url = 'https://'.$endpoint.$uri.'?'.$canonical_query_string.'&Signature='.rawurlencode($signature);

		$amazon_request_return = wp_remote_get($request_url);

		if ( !is_wp_error($amazon_request_return) ) {

			$request_response = $amazon_request_return['response']['code'];

			if ( $request_response == '200' ) {
				$api_response = wp_remote_retrieve_body($amazon_request_return);
			}
			else {
				$api_response = 'NOT 200 - Request Denied ny Amazon';
			}

		}

		else {
			$api_response = 'Request Not Successful';
		}


		return $api_response;


	}











	public function get_amazon_review_specs( $xml_response ) {

		$amazon_spec_val['prod_title'] = htmlspecialchars( $xml_response->Items->Item->ItemAttributes->Title );
		$amazon_spec_val['prod_asin'] = $xml_response->Items->Item->ASIN;
		//$prod_url = $xml_response->Items->Item->DetailPageURL; // pulled in above for featured image
		$amazon_spec_val['prod_brand'] = $xml_response->Items->Item->ItemAttributes->Brand;
		$amazon_spec_val['prod_model'] = $xml_response->Items->Item->ItemAttributes->Model;
		$amazon_spec_val['prod_upc'] = $xml_response->Items->Item->ItemAttributes->UPC;
		$amazon_spec_val['prod_features_array'] = $xml_response->Items->Item->ItemAttributes->Feature;
		$amazon_spec_val['prod_warranty'] = $xml_response->Items->Item->ItemAttributes->Warranty;

	    $amazon_spec_val['prod_formatted_price'] = $xml_response->Items->Item->ItemAttributes->ListPrice->FormattedPrice;

	    $amazon_spec_val['prod_lowest_new_price'] = $xml_response->Items->Item->OfferSummary->LowestNewPrice->FormattedPrice;
	    $amazon_spec_val['prod_lowest_used_price'] = $xml_response->Items->Item->OfferSummary->LowestUsedPrice->FormattedPrice;

	    $amazon_spec_val['prod_lowest_new_price_currency'] = $xml_response->Items->Item->OfferSummary->LowestNewPrice->CurrencyCode;
	    $amazon_spec_val['prod_lowest_used_price_currency'] = $xml_response->Items->Item->OfferSummary->LowestUsedPrice->CurrencyCode;

	    $amazon_spec_val['prod_price_offer'] = $xml_response->Items->Item->Offers->Offer->OfferListing->Price->FormattedPrice;
	    $amazon_spec_val['prod_amount_saved'] = $xml_response->Items->Item->Offers->Offer->OfferListing->AmountSaved->FormattedPrice;
	    $amazon_spec_val['prod_amount_saved_currency'] = $xml_response->Items->Item->Offers->Offer->OfferListing->AmountSaved->CurrencyCode;
	    $amazon_spec_val['prod_percentage_saved'] = $xml_response->Items->Item->Offers->Offer->OfferListing->PercentageSaved;

	    $amazon_spec_val['prod_total_new'] = $xml_response->Items->Item->OfferSummary->TotalNew;
	    $amazon_spec_val['prod_total_used'] = $xml_response->Items->Item->OfferSummary->TotalNew;

	    $amazon_spec_val['prod_listprice_currencycode'] = $xml_response->Items->Item->ItemAttributes->ListPrice->CurrencyCode;

	    $amazon_spec_val['small-img'] = $xml_response->Items->Item->ImageSets->ImageSet->SmallImage->URL;
	    $amazon_spec_val['medium-img'] = $xml_response->Items->Item->ImageSets->ImageSet->MediumImage->URL;
	    $amazon_spec_val['large-img'] = $xml_response->Items->Item->ImageSets->ImageSet->LargeImage->URL;

	    return $amazon_spec_val; 


	}








	public function get_azontables_table_global_settings() {

		//enable state saving
			$table_attributes['enable_table_memory'] = get_option( 'azontables-options_enable_table_memory' );

			// look into "key" for persistent state on different pages


			// enable hide the header row
			$table_attributes['hide_header'] = get_option( 'azontables-options_hide_the_table_header' );


			//enable table sorting
			$table_attributes['enable_table_sorting'] = get_option( 'azontables-options_enable_table_sorting' );


			//echo '::: '.$enable_table_sorting;

			//enable filtering
			$table_attributes['enable_table_filtering']['enabled'] = get_option( 'azontables-options_enable_table_filtering' );




			// add a no results message when filtering returns 0 results
			$table_attributes['enable_table_filtering']['no_results_message'] = get_option( 'azontables-options_no_results_message' );



			// ad custom filter input placeholder text
			$table_attributes['enable_table_filtering']['filter_placeholder_text'] = get_option( 'azontables-options_filter_placeholder_text' );



			// add a title to the filter columns dropdown
			$table_attributes['enable_table_filtering']['filter_dropdown_heading'] = get_option( 'azontables-options_filter_dropdown_heading' );


			$table_attributes['enable_table_filtering']['filter_position'] = get_option( 'azontables-options_filter_position' );


			//enable pagination
			$table_attributes['enable_table_pagination']['enabled'] = get_option( 'azontables-options_enable_table_pagination' );




			$table_attributes['enable_table_pagination']['number_of_rows_per_page'] = get_option( 'azontables-options_number_of_rows_per_page' );


			//enable pagination limit to pages shown in pagination
			$table_attributes['enable_table_pagination']['pages_displayed_limit'] = get_option( 'azontables-options_pages_displayed_limit' );


			// set the pagination positioning
			$table_attributes['enable_table_pagination']['pagination_position'] = get_option( 'azontables-options_pagination_position' );




			//enable breakpoints to run off parent container width
			$table_attributes['breakpoints_off_container'] = get_option( 'azontables-options_breakpoints_off_container' );



			// use custom breakpoints
			$table_attributes['custom_breakpoints']['enabled'] = get_option( 'azontables-options_custom_breakpoints' );



				// set defaults as a fallback
				$default_breakpoints = array(
					'xs' => '480',
					'sm' => '768',
					'md' => '992',
					'lg' => '1200',
					'xl' => (int) '1400'
				);

				// set our user chosen values if they exist and don't equal 0
				$xs = get_option( 'azontables-options_extra_small_breakpoint' );
				$sm = get_option( 'azontables-options_small_breakpoint' );
				$md = get_option( 'azontables-options_medium_breakpoint' );
				$lg = get_option( 'azontables-options_large_breakpoint' );

				if ( $xs && $xs != '0' ) { $default_breakpoints['xs'] = (int) $xs; }
				if ( $sm && $sm != '0' ) { $default_breakpoints['sm'] = (int) $sm; }
				if ( $md && $md != '0' ) { $default_breakpoints['md'] = (int) $md; }
				if ( $lg && $lg != '0' ) { $default_breakpoints['lg'] = (int) $lg; }

				$table_attributes['custom_breakpoints']['points'] = $default_breakpoints;



			// display the first row open by default
			$table_attributes['expand_the_first_row_by_default'] = get_option( 'azontables-options_expand_the_first_row_by_default' );



			// display the first row open by default
			$table_attributes['expand_all_rows_by_default'] = get_option( 'azontables-options_expand_all_rows_by_default' );


			// hide the row toggle button
			$table_attributes['hide_the_toggle_button'] = get_option( 'azontables-options_hide_the_toggle_button' );

			

			$global_table_settings = $table_attributes;

			return $global_table_settings;

	}




	/** Set our table attributes
	 *
	 * @since    1.0.0
	 */
	public function get_azontables_table_attributes( $post_id ) {


		$table_attributes = '';
		$override = get_post_meta( $post_id, 'overide_global_table_settings', true );

		if ( $override ) {

			//enable state saving
			$enable_table_memory = get_post_meta( $post_id, 'enable_table_memory', true );
			if ( $enable_table_memory ) { $table_attributes .= ' data-state="true"'; }

			// look into "key" for persistent state on different pages

			// enable hide the header row
			$hide_header = get_post_meta( $post_id, 'hide_the_table_header', true );
			if ( $hide_header ) { $table_attributes .= ' data-show-header="false"'; }

			//enable table sorting
			$enable_table_sorting = get_post_meta( $post_id, 'enable_table_sorting', true );
			if ( $enable_table_sorting ) { $table_attributes .= ' data-sorting="true"'; }

			//enable filtering
			$enable_table_filtering = get_post_meta( $post_id, 'enable_table_filtering', true );
			if ( $enable_table_filtering ) { 

				$table_attributes .= ' data-filtering="true"'; 

				// add a no results message when filtering returns 0 results
				$no_results_message = get_post_meta( $post_id, 'no_results_message', true );
				if ( $no_results_message ) { $table_attributes .= ' data-empty="'.$no_results_message.'"'; }


				// ad custom filter input placeholder text
				$filter_placeholder_text = get_post_meta( $post_id, 'filter_placeholder_text', true );
				if ( $filter_placeholder_text ) { $table_attributes .= ' data-filter-placeholder="'.$filter_placeholder_text.'"'; }


				// add a title to the filter columns dropdown
				$filter_dropdown_heading = get_post_meta( $post_id, 'filter_dropdown_heading', true );
				if ( $filter_dropdown_heading ) { $table_attributes .= ' data-filter-dropdown-title="'.$filter_dropdown_heading.'"'; }

			}


			//enable pagination
			$enable_table_pagination = get_post_meta( $post_id, 'enable_table_pagination', true );
			if ( $enable_table_pagination ) { 

				$table_attributes .= ' data-paging="true"'; 

				$number_of_rows_per_page = get_post_meta( $post_id, 'number_of_rows_per_page', true );
				if ( $number_of_rows_per_page ) { 
					$table_attributes .= ' data-paging-size="'.$number_of_rows_per_page.'"'; 
				}

				//enable pagination limit to pages shown in pagination
				$pages_displayed_limit = get_post_meta( $post_id, 'pages_displayed_limit', true );
				if ( $pages_displayed_limit ) { $table_attributes .= ' data-paging-limit="'.$pages_displayed_limit.'"'; }

				// set the pagination positioning
				$pagination_position = get_post_meta( $post_id, 'pagination_position', true );
				if ( $pagination_position ) { $table_attributes .= ' data-paging-position="'.$pagination_position.'"'; }

			}


			//enable breakpoints to run off parent container width
			$breakpoints_off_container = get_post_meta( $post_id, 'breakpoints_off_container', true );
			if ( $breakpoints_off_container ) { $table_attributes .= ' data-use-parent-width="true"'; }


			// use custom breakpoints
			$custom_breakpoints = get_post_meta( $post_id, 'custom_breakpoints', true );

			if ( $custom_breakpoints ) {

				// set defaults as a fallback
				$default_breakpoints = array(
					'xs' => '480',
					'sm' => '768',
					'md' => '992',
					'lg' => '1200',
					'xl' => (int) '1400'
				);

				// set our user chosen values if they exist and don't equal 0
				$xs = get_post_meta( $post_id, 'extra_small_breakpoint', true );
				$sm = get_post_meta( $post_id, 'small_breakpoint', true );
				$md = get_post_meta( $post_id, 'medium_breakpoint', true );
				$lg = get_post_meta( $post_id, 'large_breakpoint', true );

				if ( $xs && $xs != '0' ) { $default_breakpoints['xs'] = (int) $xs; }
				if ( $sm && $sm != '0' ) { $default_breakpoints['sm'] = (int) $sm; }
				if ( $md && $md != '0' ) { $default_breakpoints['md'] = (int) $md; }
				if ( $lg && $lg != '0' ) { $default_breakpoints['lg'] = (int) $lg; }

				$encoded = json_encode( $default_breakpoints );
				$table_attributes .= " data-breakpoints='".htmlspecialchars($encoded)."'";

			}


			// display the first row open by default
			$expand_the_first_row_by_default = get_post_meta( $post_id, 'expand_the_first_row_by_default', true );
			if ( $expand_the_first_row_by_default ) { $table_attributes .= ' data-expand-first="true"'; }


			// display the first row open by default
			$expand_all_rows_by_default = get_post_meta( $post_id, 'expand_all_rows_by_default', true );
			if ( $expand_all_rows_by_default ) { $table_attributes .= ' data-expand-all="true"'; }

			// hide the row toggle button
			$hide_the_toggle_button = get_post_meta( $post_id, 'hide_the_toggle_button', true );
			if ( $hide_the_toggle_button ) { $table_attributes .= ' data-show-toggle="false"'; }
			

			// hide the row toggle button
			$filter_position = get_post_meta( $post_id, 'filter_position', true );
			if ( $filter_position ) { $table_attributes .= ' data-filter-position="'.$filter_position.'"'; }

		} // end if $override

		else { // else use global settings


			//enable state saving
			$enable_table_memory = get_option( 'azontables-options_enable_table_memory' );
			if ( $enable_table_memory ) { $table_attributes .= ' data-state="true"'; }

			// look into "key" for persistent state on different pages


			// enable hide the header row
			$hide_header = get_option( 'azontables-options_hide_the_table_header' );
			if ( $hide_header ) { $table_attributes .= ' data-show-header="false"'; }

			//enable table sorting
			$enable_table_sorting = get_option( 'azontables-options_enable_table_sorting' );
			if ( $enable_table_sorting ) { $table_attributes .= ' data-sorting="true"'; }

			//echo '::: '.$enable_table_sorting;

			//enable filtering
			$enable_table_filtering = get_option( 'azontables-options_enable_table_filtering' );
			if ( $enable_table_filtering ) { 

				$table_attributes .= ' data-filtering="true"'; 

				// add a no results message when filtering returns 0 results
				$no_results_message = get_option( 'azontables-options_no_results_message' );
				if ( $no_results_message ) { $table_attributes .= ' data-empty="'.$no_results_message.'"'; }


				// ad custom filter input placeholder text
				$filter_placeholder_text = get_option( 'azontables-options_filter_placeholder_text' );
				if ( $filter_placeholder_text ) { $table_attributes .= ' data-filter-placeholder="'.$filter_placeholder_text.'"'; }


				// add a title to the filter columns dropdown
				$filter_dropdown_heading = get_option( 'azontables-options_filter_dropdown_heading' );
				if ( $filter_dropdown_heading ) { $table_attributes .= ' data-filter-dropdown-title="'.$filter_dropdown_heading.'"'; }

			}


			//enable pagination
			$enable_table_pagination = get_option( 'azontables-options_enable_table_pagination' );
			if ( $enable_table_pagination ) { 

				$table_attributes .= ' data-paging="true"'; 

				$number_of_rows_per_page = get_option( 'azontables-options_number_of_rows_per_page' );
				if ( $number_of_rows_per_page ) { 
					$table_attributes .= ' data-paging-size="'.$number_of_rows_per_page.'"'; 
				}

				//enable pagination limit to pages shown in pagination
				$pages_displayed_limit = get_option( 'azontables-options_pages_displayed_limit' );
				if ( $pages_displayed_limit ) { $table_attributes .= ' data-paging-limit="'.$pages_displayed_limit.'"'; }

				// set the pagination positioning
				$pagination_position = get_option( 'azontables-options_pagination_position' );
				if ( $pagination_position ) { $table_attributes .= ' data-paging-position="'.$pagination_position.'"'; }

			}


			//enable breakpoints to run off parent container width
			$breakpoints_off_container = get_option( 'azontables-options_breakpoints_off_container' );
			if ( $breakpoints_off_container ) { $table_attributes .= ' data-use-parent-width="true"'; }


			// use custom breakpoints
			$custom_breakpoints = get_option( 'azontables-options_custom_breakpoints' );

			if ( $custom_breakpoints ) {

				// set defaults as a fallback
				$default_breakpoints = array(
					'xs' => '480',
					'sm' => '768',
					'md' => '992',
					'lg' => '1200',
					'xl' => (int) '1400'
				);

				// set our user chosen values if they exist and don't equal 0
				$xs = get_option( 'azontables-options_extra_small_breakpoint' );
				$sm = get_option( 'azontables-options_small_breakpoint' );
				$md = get_option( 'azontables-options_medium_breakpoint' );
				$lg = get_option( 'azontables-options_large_breakpoint' );

				if ( $xs && $xs != '0' ) { $default_breakpoints['xs'] = (int) $xs; }
				if ( $sm && $sm != '0' ) { $default_breakpoints['sm'] = (int) $sm; }
				if ( $md && $md != '0' ) { $default_breakpoints['md'] = (int) $md; }
				if ( $lg && $lg != '0' ) { $default_breakpoints['lg'] = (int) $lg; }

				$encoded = json_encode( $default_breakpoints );
				$table_attributes .= " data-breakpoints='".htmlspecialchars($encoded)."'";

			}


			// display the first row open by default
			$expand_the_first_row_by_default = get_option( 'azontables-options_expand_the_first_row_by_default' );
			if ( $expand_the_first_row_by_default ) { $table_attributes .= ' data-expand-first="true"'; }


			// display the first row open by default
			$expand_all_rows_by_default = get_option( 'azontables-options_expand_all_rows_by_default' );
			if ( $expand_all_rows_by_default ) { $table_attributes .= ' data-expand-all="true"'; }

			// hide the row toggle button
			$hide_the_toggle_button = get_option( 'azontables-options_hide_the_toggle_button' );
			if ( $hide_the_toggle_button ) { $table_attributes .= ' data-show-toggle="false"'; }
			

			// hide the row toggle button
			$filter_position = get_option( 'azontables-options_filter_position' );
			if ( $filter_position ) { $table_attributes .= ' data-filter-position="'.$filter_position.'"'; }

		}

		return $table_attributes;


	}





















	/** Build our product data in groups of 10 asins at a time
	 *
	 * @since    1.0.0
	 */
	public function azontables_table_get_asin_groups_data( $final_table, $prod_id ) {


		// set our empty table_asins_groups array
		$table_asins_groups = array();


		// this is the array not broken into ten asins, that we will loop through below to create our final amazon data array to pull into the table below
		$table_asins_full_group = array();

		$i=0;
		$z = 0;
		// loop through our table rows and save the asins into groups of 10
		foreach ( $final_table['body'] as $tr ) {


			$table_asins_groups[$z][$i]['prod_id'] = $tr[0]['prod_id'];
			$table_asins_groups[$z][$i]['asin'] = $tr[0]['prod_id'];
			$i++;

			if ($i % 10 == 0 ) {
				$z++;
			}
		}

		$x=0;

		// let's change each group of ten into a string and make our api calls for ten items at a time
		foreach ( $table_asins_groups as $asin_group ) {
			// create our empty asins string
			$asin_string = '';
			// add our asins to the string
			foreach ( $asin_group as $asin) {
				$asin_string .= $asin['asin'].',';
			}
			// make our api call to get the product data for the ten asins
			$api_response = $this->amazon_api_item_lookup( $prod_id, 'Images,ItemAttributes,Offers', $asin_string );
			// save our products data into the $all_products_info_arr

			if($api_response) {
				$all_products_info_arr[$x] = simplexml_load_string($api_response);
			}
			else {
				$all_products_info_arr[$x] = null;
			}
			$x++;
		}


		// narrow down our all prodyucts data array to just the "item" array form our xml response
		$new_products_info_arr = array();
		$w=0;
		$x=0;
		if ( $all_products_info_arr ) {
			foreach ( $all_products_info_arr as $arr ) {
				if ($arr) {
					foreach($arr->Items->Item as $item) {
						$items_array[$x] = $item;
						$x++;
					}
				}
				$w++;
			}
		}

		// let's now build our final array pf product data, organized ny ASIN to look up quickly later
		$final_item_data = array();
		// loop through all of our items
		if ($items_array) {
			foreach($items_array as $item) {
				$asin = $item->ASIN;
				// add each product's data to the final array, with the asin for the key for easy lookup later
				$final_item_data[''.$asin.''] = $item;
			}
		}

		return $final_item_data;

	}










	/** Build our product data in groups of 10 asins at a time
	 *
	 * @since    1.0.0
	 */
	public function azontables_get_admin_asins_info( $ama_asins_arr ) {


		// set our empty table_asins_groups array
		$table_asins_groups = array();


		// this is the array not broken into ten asins, that we will loop through below to create our final amazon data array to pull into the table below
		$table_asins_full_group = array();

		$i=0;
		$z = 0;
		// loop through our table rows and save the asins into groups of 10
		foreach ( $ama_asins_arr as $asin ) {


			$table_asins_groups[$z][$i]['asin'] = $asin;
			$i++;

			if ($i % 10 == 0 ) {
				$z++;
			}
		}

		$x=0;

		// let's change each group of ten into a string and make our api calls for ten items at a time
		foreach ( $table_asins_groups as $asin_group ) {
			// create our empty asins string
			$asin_string = '';
			// add our asins to the string
			foreach ( $asin_group as $asin) {
				$asin_string .= $asin['asin'].',';
			}
			// make our api call to get the product data for the ten asins
			$api_response = $this->amazon_api_item_lookup( $prod_id, 'Images,ItemAttributes,Offers', $asin_string );
			// save our products data into the $all_products_info_arr
			$all_products_info_arr[$x] = simplexml_load_string($api_response);
			$x++;

		}


		// narrow down our all prodyucts data array to just the "item" array form our xml response
		$new_products_info_arr = array();
		$w=0;
		$x=0;
		foreach ( $all_products_info_arr as $arr ) {
			foreach($arr->Items->Item as $item) {
				$items_array[$x] = $item;
				$x++;
			}
			$w++;
		}

		// let's now build our final array pf product data, organized ny ASIN to look up quickly later
		$final_item_data = array();
		// loop through all of our items
		foreach($items_array as $item) {
			$asin = $item->ASIN;
			// add each product's data to the final array, with the asin for the key for easy lookup later
			$final_item_data[''.$asin.''] = $item;
		}

		return $final_item_data;

	}










}