<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    azontables_Pluginname
 * @subpackage azontables_Pluginname/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    azontables_Pluginname
 * @subpackage azontables_Pluginname/admin
 * @author     Your Name <email@example.com>
 */
class azontables_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $azontables    The ID of this plugin.
	 */
	private $azontables;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $azontables       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $azontables, $version, $edd_product, $edd_store ) {

		$this->azontables = $azontables;
		$this->version = $version;
		$this->edd_product = $edd_product;
		$this->edd_store = $edd_store;

	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in azontables_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The azontables_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->azontables, plugin_dir_url( __FILE__ ) . 'css/azontables-admin-min.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in azontables_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The azontables_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->azontables, plugin_dir_url( __FILE__ ) . 'js/azontables-admin-min.js', array( 'jquery' ), $this->version, false );

	}








	function azontables_acf_settings_path( $path ) {
	    // update path
	    $path = plugin_dir_url( 'azontables.php' ) . 'azontables/includes/advanced-custom-fields-pro/';
	    // return
	    return $path;
	}


	function azontables_acf_settings_dir( $dir ) {
	    // update path
	    $dir = plugin_dir_url( 'azontables.php' ) . 'azontables/includes/advanced-custom-fields-pro/';
	    // return
	    return $dir;
	}






	public function azontables_create_post_type() {

    	register_post_type( 'azontable',
	        array(
	            'labels' => array(
	                'name' => __('Azon Tables', 'wp-compear'),
	                'singular_name' => __('Azon Table', 'wp-compear'),
	                'add_new' => __('Add New', 'wp-compear'),
	                'add_new_item' => __('Add New Azon Table', 'wp-compear'),
	                'edit' => __('Edit', 'wp-compear'),
	                'edit_item' => __('Edit Azon Table', 'wp-compear'),
	                'new_item' => __('New Azon Table', 'wp-compear'),
	                'view' => __('View', 'wp-compear'),
	                'view_item' => __('View Azon Table', 'wp-compear'),
	                'search_items' => __('Search Azon Tables', 'wp-compear'),
	                'not_found' => __('No Azon Table', 'wp-compear'),
	                'not_found_in_trash' => __('No Azon Table found in Trash', 'wp-compear'),
	                'parent' => __('Parent Azon Table', 'wp-compear')
	            ),
	 
	            'public' => true,
	            'menu_position' => 6,
	            'rewrite' => true,
	            'supports' => array( 'title', 'editor'),
	            'taxonomies' => array( '' ),
	            'menu_icon' => 'dashicons-editor-table',
	            'has_archive' => true
	        )
	    );
	}


	


	
	/**
	 * Include the Plugin Updater
	 *
	 * @since    1.0.0
	 */
	function azontables_plugin_updater() {

		// retrieve our license key from the DB
		$license_key = trim( get_option( 'azontables_license' ) );

		$plugin_file = ABSPATH . 'wp-content/plugins/azontables/azontables.php';

		// setup the updater
		$edd_updater = new AZONTABLES_Plugin_Updater( $this->edd_store, $plugin_file, array(
				'version' 	=> $this->version, 				// current version number
				'license' 	=> $license_key, 		// license key (used get_option above to retrieve from DB)
				'item_name' => $this->edd_product, 	// name of this plugin
				'author' 	=> 'Dave Nicosia',  // author of this plugin
				'url'       => home_url()
			)
		);

	}




	// set up our default licensing values
	function azontables_options_init() {
	    $azontables_license = get_option( 'azontables_license' );
	    // Are our options saved in the DB?
	    if ( false === $azontables_license ) {
	        // If not, we'll save our default options
	        add_option( 'azontables_license', '' );
	        add_option( 'azontables_license_status', 'deactivated' );
	    }
	    // In other case we don't need to update the DB
	}



	// Add "Licenses" link to the "Azon Tables" menu
	function azontables_menu_options() {
	    // add_theme_page( $page_title, $menu_title, $capability, $menu_slug, $function);
	    add_submenu_page('azontables-settings', 'Azon Tables - Licensing', 'Licenses', 'edit_theme_options', 'azontables-licenses', 'azontables_licensing_page', '' );


	    function azontables_licensing_page () {
		?>


		    <div class="wrap settings-section">
		        <h2><?php _e( 'Azon Tables Licensing Center', 'azontables' ); ?></h2>
		        
		        <?php // the ID in the box must be the license option name: azontables_CHILDTHEMEOREXTENSIONNAME_license ?>
		        <div class="gf-option-box" id="azontables_license">

		            <h3><?php _e( 'Azon Tables License Key', 'azontables' ) ?></h3>

		            <div class="gf-option-box-inner">

		                <?php
		                $license    = get_option('azontables_license');
		                $status     = get_option('azontables_license_status');

		                if($status=='deactivated'):
		                echo '<p><span class="license-message azontables-license-deactivated">' . __( 'The license key you entered has been deactivated on this website.', 'azontables' ) . '</span> <img class="license-loading-gif" src="/wp-admin/images/loading.gif" alt="loading" /></p>';
		                endif;

		                if($status=='invalid'):
		                echo '<p><span class="license-message azontables-license-invalid">' . __( 'The license key you entered is invalid. Please try again.', 'azontables' ) . '</span> <img class="license-loading-gif" src="/wp-admin/images/loading.gif" alt="loading" /></p>';
		                endif;


		                if($status=='valid'):
		                echo '<p><span class="license-message azontables-license-valid">' . __( 'Your license is active.', 'azontables' ) . '</span> <img class="license-loading-gif" src="/wp-admin/images/loading.gif" alt="loading" /></p>';
		                endif;
		                ?>

		                <div class="license-form">

		                    <?php // the ID & name attributes must be the option name stored in the DB. It should follow this format:   azontables_CHILDorEXTENSIONTITLE_license ?>
		                    <input id="azontables_license" type="text" class="regular-text azontables-license" name="azontables_license" value="<?php echo $license; ?>" />

		                    <?php // the ID and name attributes must be "azontables_product" and the value must be the name of the product in the azontables.com store. The input type must be "hidden" ?>
		                    <input id="azontables_product" type="hidden" class="azontables-product" name="azontables_product" value="Azon Tables" />

		                    <?php if ( $license != '' && $status == 'valid' ) : ?>

		                        <?php // The deactivate button must have  azontables-deactivate  as a class ?>
		                        <?php wp_nonce_field( 'edd_sample_nonce', 'edd_sample_nonce' ); ?>
		                            <input type="button" class="button-secondary azontables-deactivate" name="azontablesdeactivate" value="<?php _e('Deactivate', 'azontables'); ?>"/>

		                    <?php else: ?>

		                        <?php // The activate button must have  azontables-activate  as a class ?>
		                        <?php wp_nonce_field( 'edd_sample_nonce', 'edd_sample_nonce' ); ?>
		                            <input type="button" class="button-secondary azontables-activate" name="azontablesactivate" value="<?php _e('Activate', 'azontables'); ?>"/>

		                    <?php endif; ?>

		                </div>



		            </div>

		        </div>


		    </div>
		    <div class="clear"></div>

		<?php
		}
	}





	// function for activating azon tables themes and plugins licenses
	function activate_azontables_license() {

	    global $wpdb;

	    $license        = $_POST['license'];
	    $license_box_ID    = $_POST['license_box_ID'];
	    $azontables_product    = $_POST['azontables_product'];

	    $old_license    = get_option($license_box_ID);
	    $old_status     = get_option($license_box_ID.'_status');

	    $results['license_box_ID'] = $license_box_ID;

	    // if the license in the db is empty or matches the new valaue and is all numbers and letters
	    if ( ( $license != '' ) && ctype_alnum($license) ) {

	        // this is a fix to remove http from url to make api resonse not result in a 403 error
	        $full_home_url = get_home_url();
	        $find = array( 'http://', 'https://' );
	        $replace = '';
	        $short_home_url = str_replace( $find, $replace, $full_home_url );

	        // data to send in our API request
	        $api_params = array( 
	            'edd_action'=> 'activate_license', 
	            'license'   => $license, 
	            'item_name' => urlencode( $azontables_product ), // the name of our product in EDD,
	            'url'       => $short_home_url
	        );

	        
	        // Call the custom API.
	        $response = wp_remote_get( add_query_arg( $api_params, 'http://azontables.com' ) );


	        // make sure the response came back okay
	        if ( is_wp_error( $response ) ) {
	            $results['result'] = 'false1';
	        }

	        else {

	            // decode the license data
	            $license_data = json_decode( wp_remote_retrieve_body( $response ) );

	            if ( $license_data->license == 'valid' ) {
	                // $license_data->license will be either "valid" or "invalid"
	                delete_option( $license_box_ID.'_status' );
	                update_option( $license_box_ID.'_status', $license_data->license );

	                if ( $license != $old_license ) {
	                    delete_option( $license_box_ID );
	                    update_option( $license_box_ID, $license );
	                }

	                
	            }

	            $results['result'] = $license_data->license;

	        }


	    }

	    else {
	        $results['result'] = 'false2';
	    }

	    echo json_encode($results);
	    die(1);

	}


	// function for deactivating azon tables themes and plugins licenses
	function deactivate_azontables_license() {

	    global $wpdb;

	    $license        = $_POST['license'];
	    $license_box_ID    = $_POST['license_box_ID'];
	    $azontables_product    = $_POST['azontables_product'];

	    $old_license    = get_option($license_box_ID);
	    $old_status     = get_option($license_box_ID.'_status');

	    $results['license_box_ID'] = $license_box_ID;

	    if ( $license == $old_license ) {

	        // this is a fix to remove http from url to make api resonse not result in a 403 error
	        $full_home_url = get_home_url();
	        $find = array( 'http://', 'https://' );
	        $replace = '';
	        $short_home_url = str_replace( $find, $replace, $full_home_url );


	        // data to send in our API request
	        $api_params = array( 
	            'edd_action'=> 'deactivate_license', 
	            'license'   => $license, 
	            'item_name' => urlencode( $azontables_product ), // the name of our product in EDD,
	            'url'       => $short_home_url
	        );

	        
	        // Call the custom API.
	        $response = wp_remote_get( add_query_arg( $api_params, 'http://azontables.com' ) );

	        //echo'<pre>'; print_r($response); echo'</pre>'; die;

	        // make sure the response came back okay
	        if ( is_wp_error( $response ) ) {
	            $results['result'] = 'false';
	        }

	        else {

	            // decode the license data
	            $license_data = json_decode( wp_remote_retrieve_body( $response ) );

	            // $license_data->license will be either "deactivated" or "failed"
	            if( $license_data->license == 'deactivated' ) {
	                delete_option( $license_box_ID.'_status' );
	                update_option( $license_box_ID.'_status', $license_data->license );

	                //delete_option( 'uat2_license' );
	            }

	            $results['result'] = $license_data->license;

	        }


	    }

	    else {
	        $results['result'] = 'false';
	    }

	    echo json_encode($results);
	    die(1);

	}




	public function amazon_search_thickbox() {
	?>

		<?php add_thickbox(); ?>
		<div id="azontables-amazon-lookup" style="display:none;">
		    

			<div id="azontables-api-lookup">

			<form>

				<label>Enter your Keywords or the product ASIN here</label>
				<input id="azontables-api-search-term" type="text" value="">

				<?php $chosen_locale = get_option('azontables-options_default_amazon_search_locale'); ?>

				<input id="azontables-search-locale" type="hidden" name="azontables-search-locale" value="<?php echo $chosen_locale; ?>">
				<input id="azontables-search-specs" type="hidden" name="azontables-search-specs" value="custom-specs">

				<?php 
				if ( $chosen_locale == 'US') { echo '<span class="search-locale-label">'.__('Locale: United States', 'azontables' ).'</span>'; }
				else if ( $chosen_locale == 'UK') { echo '<span class="search-locale-label">'.__('Locale: United Kingdom', 'azontables' ).'</span>'; }
				else if ( $chosen_locale == 'BR') { echo '<span class="search-locale-label">'.__('Locale: Brazil', 'azontables' ).'</span>'; }
				else if ( $chosen_locale == 'CA') { echo '<span class="search-locale-label">'.__('Locale: Canada', 'azontables' ).'</span>'; }
				else if ( $chosen_locale == 'CN') { echo '<span class="search-locale-label">'.__('Locale: China', 'azontables' ).'</span>'; }
				else if ( $chosen_locale == 'FR') { echo '<span class="search-locale-label">'.__('Locale: France', 'azontables' ).'</span>'; }
				else if ( $chosen_locale == 'DE') { echo '<span class="search-locale-label">'.__('Locale: Germany', 'azontables' ).'</span>'; }
				else if ( $chosen_locale == 'IN') { echo '<span class="search-locale-label">'.__('Locale: India', 'azontables' ).'</span>'; }
				else if ( $chosen_locale == 'MX') { echo '<span class="search-locale-label">'.__('Locale: Mexico', 'azontables' ).'</span>'; }
				else if ( $chosen_locale == 'IT') { echo '<span class="search-locale-label">'.__('Locale: Italy', 'azontables' ).'</span>'; }
				else if ( $chosen_locale == 'JP') { echo '<span class="search-locale-label">'.__('Locale: Japan', 'azontables' ).'</span>'; }
				else if ( $chosen_locale == 'ES') { echo '<span class="search-locale-label">'.__('Locale: Spain', 'azontables' ).'</span>'; }
				else { _e('Locale: United States', 'azontables' ); }
				?>

				<input id="azontables-api-search-submit" type="button" class="button button-primary button-large" value="<?php _e('Search', 'azontables' ); ?>" />

				</form>

			</div>

			<div id="azontables-amazon-api-results" class="amazon-api-search-results"></div>

			<div class="azontables-amazon-loading-overlay test">
				<img src="/wp-admin/images/loading.gif" alt="loading" />
			</div>

		</div>



		<?php
	}






	/** prepare our ajax call to the amazon product advertising api
	 *
	 * @since    1.0.0
	 */
	public function azontables_find_amazon_products_ajax() {

		global $wpdb; // this is how you get access to the database

	    $term = $_POST['term'];
	    $locale = $_POST['locale'];
	    $specstype = $_POST['searchspecs'];
	    $page = '1';

	    // make our amazon api call
	    $api_response = $this->azontables_amazon_api_request( $term, $locale, $page );

	    // parse our amazon api call response
	    echo $this->azontables_amazon_api_response_parse( $api_response, $term, $locale, $specstype );

	    die(); // this is required to return a proper result

	}



	/** make our ajax call to the amazon product advertising api
	 *
	 * @since    1.0.0
	 */
	public function azontables_amazon_api_request( $term, $locale, $page ) {


		// http://webservices.amazon.com/scratchpad/index.html

		// Your AWS Access Key ID, as taken from the AWS Your Account page
		$aws_access_key_id = trim( get_option( 'azontables-options_amazon_api_key' ) );

		// Your AWS Secret Key corresponding to the above ID, as taken from the AWS Your Account page
		$aws_secret_key = trim( get_option( 'azontables-options_amazon_api_secret' ) );
		$affiliate_id = trim( get_option( 'azontables-options_amazon_affiliate_id_'.$locale ) );

		$azontables_amazon_api = new azontables_amazon_api();
		$endpoint = $azontables_amazon_api->amazon_api_get_endpoint();

		$uri = "/onca/xml";

		$params = array(
		    "Service" => "AWSECommerceService",
		    "Operation" => "ItemSearch",
		    "AWSAccessKeyId" => $aws_access_key_id,
		    "AssociateTag" => $affiliate_id,
		    "SearchIndex" => "All",
		    "Keywords" => $term,
		    "ItemPage" => $page,
		    "ResponseGroup" => "Images,ItemAttributes,Offers,Reviews"
		);

		// Set current timestamp if not set
		if (!isset($params["Timestamp"])) {
		    $params["Timestamp"] = gmdate('Y-m-d\TH:i:s\Z');
		}

		// Sort the parameters by key
		ksort($params);

		$pairs = array();

		foreach ($params as $key => $value) {
		    array_push($pairs, rawurlencode($key)."=".rawurlencode($value));
		}

		// Generate the canonical query
		$canonical_query_string = join("&", $pairs);

		// Generate the string to be signed
		$string_to_sign = "GET\n".$endpoint."\n".$uri."\n".$canonical_query_string;

		// Generate the signature required by the Product Advertising API
		$signature = base64_encode(hash_hmac("sha256", $string_to_sign, $aws_secret_key, true));

		// Generate the signed URL
		$request_url = 'https://'.$endpoint.$uri.'?'.$canonical_query_string.'&Signature='.rawurlencode($signature);

		$amazon_request_return = wp_remote_get($request_url);
		

		if ( !is_wp_error($amazon_request_return) ) {

			$request_response = $amazon_request_return['response']['code'];

			if ( $request_response == '200' ) {
				$response = wp_remote_retrieve_body($amazon_request_return);
			}
			else {
				$response = 'NOT 200 - Request Denied ny Amazon';
				echo $response;
			}

		}

		else {
			$response = 'Request Not Successful';
		}

		return $response;


	}




	/** parse our response form our ajax call to the amazon product advertising api
	 *
	 * @since    1.0.0
	 */
	function azontables_amazon_api_response_parse( $api_response, $term, $locale, $specstype ) {

		// convert xml response
		$arr = simplexml_load_string($api_response);

		if ( $arr->Items->Request->IsValid != 'True' ) {
			echo '<p class="amazon-api-failed-request">'.__('There was a problem retreiving the data from Amazon. Make sure your Amazon API key, Secret key, and amazon id for the locale you are searching are saved and correct in the', 'azontables' ).' <a href="/wp-admin/admin.php?page=extension-prp-settings" target="_blank">'.__('settings', 'azontables' ).'</a>.</p>';
			return;
		}

		echo '<div class="amazon-api-search-header">';

			echo '<h3>'.__('Search Results', 'azontables' ).':</h3>';
			//$results = (int)$arr->Items->TotalResults;
			$formatted_results = number_format( (int)$arr->Items->TotalResults );
			echo '<p>'.$formatted_results.' '.__('Results', 'azontables' ).'</p>';

		echo '</div>';

		echo '<div class="amazon-search-pagination">';
			echo '<span class="amazon-search-page current" data-api-page="1" data-api-term="'.$term.'" data-api-locale="'.$locale.'" data-api-specstype="'.$specstype.'">Page 1</span>';
			echo '<span class="amazon-search-page" data-api-page="2" data-api-term="'.$term.'" data-api-locale="'.$locale.'" data-api-specstype="'.$specstype.'">2</span>';
			echo '<span class="amazon-search-page" data-api-page="3" data-api-term="'.$term.'" data-api-locale="'.$locale.'" data-api-specstype="'.$specstype.'">3</span>';
			echo '<span class="amazon-search-page" data-api-page="4" data-api-term="'.$term.'" data-api-locale="'.$locale.'" data-api-specstype="'.$specstype.'">4</span>';
			echo '<span class="amazon-search-page" data-api-page="5" data-api-term="'.$term.'" data-api-locale="'.$locale.'" data-api-specstype="'.$specstype.'">5</span>';
			echo '<span class="amazon-search-page refine">... or refine your search!</span>';
		echo '</div>';

		echo '<div id="azontables-amazon-api-results-inner">';
		
			// loop through our results and parse
			foreach($arr->Items->Item as $item)
			{

				$prod_title = htmlspecialchars( $item->ItemAttributes->Title );
				$prod_asin = $item->ASIN;
				$prod_url = $item->DetailPageURL;
				$prod_brand = $item->ItemAttributes->Brand;
				$prod_model = $item->ItemAttributes->Model;
				$prod_upc = $item->ItemAttributes->UPC;
				$prod_features_array = $item->ItemAttributes->Feature;
				$prod_warranty = $item->ItemAttributes->Warranty;

			    $prod_formatted_price = $item->ItemAttributes->ListPrice->FormattedPrice;

			    $prod_lowest_new_price = $item->OfferSummary->LowestNewPrice->FormattedPrice;
			    $prod_lowest_used_price = $item->OfferSummary->LowestUsedPrice->FormattedPrice;

			    $prod_lowest_new_price_currency = $item->OfferSummary->LowestNewPrice->CurrencyCode;
			    $prod_lowest_used_price_currency = $item->OfferSummary->LowestUsedPrice->CurrencyCode;

			    $prod_price_offer = $item->Offers->Offer->OfferListing->Price->FormattedPrice;

			    $prod_amount_saved = $item->Offers->Offer->OfferListing->AmountSaved->FormattedPrice;
			    $prod_amount_saved_currency = $item->Offers->Offer->OfferListing->AmountSaved->CurrencyCode;
			    $prod_percentage_saved = $item->Offers->Offer->OfferListing->PercentageSaved;

			    $prod_total_new = $item->OfferSummary->TotalNew;
			    $prod_total_used = $item->OfferSummary->TotalNew;

			    $prod_listprice_currency_code = $item->ItemAttributes->ListPrice->CurrencyCode;


			    $medium_img_url = $item->MediumImage->URL;
			    if ( $medium_img_url == '' ) {
			    	$image_set = $item->ImageSets;
			    	if ( is_array( $image_set ) ) {
			    		$medium_img_url = $item->ImageSets->ImageSet[0]->MediumImage->URL;
			    	}
			    	else {
			    		$medium_img_url = $item->ImageSets->ImageSet->MediumImage->URL;
			    	}
			    }

			    echo '<div class="amazon-api-prod-box">';

			    	echo "<span class=\"insert-amazon-product\" data-prod-asin=\"".$prod_asin."\">Choose This Item</span>";

			    	echo '<a href="'.$prod_url.'" target="_blank" class="amazon-api-result-title">';
			    		echo $prod_title;
			    	echo '</a>';


			    	echo '<img src="'.$medium_img_url.'" alt="'.$prod_title.'" class="amazon-api-prod-img" />';


			    	echo '<p><strong>'.__('ASIN', 'azontables' ).':</strong> '.$prod_asin.'</p>';

			    	if ( $prod_brand == '' ) { echo '<p><strong>'.__('Brand', 'azontables' ).':</strong> N/A</p>'; }
			    	else {
				    	echo '<p><strong>'.__('Brand', 'azontables' ).':</strong> '.$prod_brand.'</p>';
				    }


				    if ( $prod_model == '' ) { echo '<p><strong>'.__('Model', 'azontables' ).':</strong> '.__('N/A', 'azontables' ).'</p>'; }
			    	else {
				    	echo '<p><strong>'.__('Model', 'azontables' ).':</strong> '.$prod_model.'</p>';
				    }


				    if ( $prod_upc == '' ) { echo '<p><strong>'.__('UPC', 'azontables' ).':</strong> N/A</p>'; }
			    	else {
				    	echo '<p><strong>'.__('UPC', 'azontables' ).':</strong> '.$prod_upc.'</p>';
				    }


				    if ( $prod_lowest_new_price == '' ) { echo '<p><strong>'.__('Lowest New Price', 'azontables' ).':</strong> '.__('N/A', 'azontables' ).'</p>'; }
			    	else {
			    		echo '<p><strong>'.__('Lowest New Price', 'azontables' ).':</strong> '.$prod_lowest_new_price.' '.$prod_lowest_new_price_currency.'</p>';
			    	}


			    	if ( $prod_lowest_used_price == '' ) { echo '<p><strong>'.__('Lowest Used Price', 'azontables' ).': </strong>'.__('N/A', 'azontables' ).'</p>'; } 
			    	else {
			    		echo '<p><strong>'.__('Lowest Used Price', 'azontables' ).':</strong> '.$prod_lowest_used_price.' '.$prod_lowest_used_price_currency.'</p>';
			    	}


			    	if ( $prod_formatted_price == '' ) { echo '<p><strong>'.__('List Price', 'azontables' ).':</strong> '.__('N/A', 'azontables' ).'</p>'; }
			    	else {
			    		echo '<p><strong>'.__('List Price', 'azontables' ).':</strong> '.$prod_formatted_price.' '.$prod_listprice_currency_code.'</p>';
			    	}


		    		if ( $prod_price_offer == '' ) { echo '<p id="chosen-api-product-bestoffer"><strong>'.__('Best Offer', 'azontables' ).': </strong> <span>'.__('N/A', 'azontables' ).'</span></p>'; } 
			    	else {
			    		echo '<p id="chosen-api-product-bestoffer"><strong>'.__('Best Offer', 'azontables' ).':</strong> <span>'.$prod_price_offer.' '.$prod_listprice_currencycode.'</span></p>';
			    	}


			    	if ( $prod_amount_saved == '' ) { echo '<p><strong>'.__('Save', 'azontables' ).': </strong> '.__('N/A', 'azontables' ).'</p>'; } 
			    	else {
						echo '<p><strong>'.__('Save', 'azontables' ).': </strong> '.$prod_amount_saved.' '.$prod_amount_saved_currency.' ('.$prod_percentage_saved.'%)</p>';
			    	}


			    	if ( $prod_warranty == '' ) { echo '<p><strong>'.__('Warranty', 'azontables' ).': </strong> '.__('N/A', 'azontables' ).'</p>'; } 
			    	else {
			    		echo '<p><strong>'.__('Warranty', 'azontables' ).':</strong> '.$prod_warranty.'</p>';
			    	}


			   //  	if ( $prod_features_array == '' ) { echo '<p><strong>'.__('Featutes', 'azontables' ).':</strong> '.__('N/A', 'azontables' ).'</p>'; }
			   //  	else {
				  //   	echo '<p class="features"><strong>'.__('Features', 'azontables' ).':</strong><br />';
				  //   	echo '<ul>';

				  //   	foreach ( $prod_features_array as $feature ) {
				  //   		echo '<li>'.$feature.'</li>';
				  //   	}

				  //   	echo '</ul>';

				  //   	$features_data_attr = array();

						// foreach ( (array) $prod_features_array as $index => $node )
						//     $features_data_attr[$index] = ( is_object ( $node ) ) ? xml2array ( $node ) : $node;

						// $prod_features_array_json = json_encode($prod_features_array);
	

				  //   }


			    	
			    	echo '<div class="clearfix"></div>';
			    echo '</div>';
			}

		echo '</div>';

		echo '';

		echo '<div class="amazon-search-pagination">';
			echo '<span class="amazon-search-page current" data-api-page="1" data-api-term="'.$term.'" data-api-locale="'.$locale.'">'.__('Page', 'azontables').' 1</span>';
			echo '<span class="amazon-search-page" data-api-page="2" data-api-term="'.$term.'" data-api-locale="'.$locale.'" data-api-specstype="'.$specstype.'">2</span>';
			echo '<span class="amazon-search-page" data-api-page="3" data-api-term="'.$term.'" data-api-locale="'.$locale.'" data-api-specstype="'.$specstype.'">3</span>';
			echo '<span class="amazon-search-page" data-api-page="4" data-api-term="'.$term.'" data-api-locale="'.$locale.'" data-api-specstype="'.$specstype.'">4</span>';
			echo '<span class="amazon-search-page" data-api-page="5" data-api-term="'.$term.'" data-api-locale="'.$locale.'" data-api-specstype="'.$specstype.'">5</span>';
			echo '<span class="amazon-search-page refine">'.__('... or refine your search!', 'azontables').'</span>';
		echo '</div>';


		// USE THIS To SEE FULL RESPONSE
		//echo '<hr /><pre>'.print_r($arr,1).'</pre>';

	}




	/** prepare our ajax call to the amazon product advertising api to load a new page of results
	 *
	 * @since    1.0.0
	 */
	public function azontables_load_amazon_products_newpage_ajax() {

		global $wpdb; // this is how you get access to the database

	    $term = $_POST['term'];
	    $locale = $_POST['locale'];
	    $page = $_POST['page'];
	    $specstype = $_POST['searchspecs'];

	    $api_response = $this->azontables_amazon_api_request( $term, $locale, $page );

	    echo $this->azontables_amazon_api_response_parse_new_page( $api_response, $term, $locale, $specstype );

	    die(); // this is required to return a proper result
	}





	/** parse our response form our ajax call to the amazon product advertising api
	 *
	 * @since    1.0.0
	 */
	function azontables_amazon_api_response_parse_new_page( $api_response, $term, $locale, $specstype ) {

		// convert xml response
		$arr = simplexml_load_string($api_response);

		if ( $arr->Items->Request->IsValid != 'True' ) {
			echo '<p class="amazon-api-failed-request">'.__('There was a problem retreiving the data from Amazon. Make sure your Amazon API key and Secret key are saved and correct', 'ultimateazon2' ).'.</p>';
			return;
		}
		
			// loop through our results and parse
			foreach($arr->Items->Item as $item)
			{

				$prod_title = htmlspecialchars( $item->ItemAttributes->Title );
				$prod_asin = $item->ASIN;
				$prod_url = $item->DetailPageURL;
				$prod_brand = $item->ItemAttributes->Brand;
				$prod_model = $item->ItemAttributes->Model;
				$prod_upc = $item->ItemAttributes->UPC;
				$prod_features_array = $item->ItemAttributes->Feature;
				$prod_warranty = $item->ItemAttributes->Warranty;

			    $prod_formatted_price = $item->ItemAttributes->ListPrice->FormattedPrice;

			    $prod_price_offer = $item->Offers->Offer->OfferListing->Price->FormattedPrice;

			    $prod_lowest_new_price = $item->OfferSummary->LowestNewPrice->FormattedPrice;
			    $prod_lowest_used_price = $item->OfferSummary->LowestUsedPrice->FormattedPrice;

			    $prod_lowest_new_price_currency = $item->OfferSummary->LowestNewPrice->CurrencyCode;
			    $prod_lowest_used_price_currency = $item->OfferSummary->LowestUsedPrice->CurrencyCode;

			    $prod_amount_saved = $item->Offers->Offer->OfferListing->AmountSaved->FormattedPrice;
			    $prod_amount_saved_currency = $item->Offers->Offer->OfferListing->AmountSaved->CurrencyCode;
			    $prod_percentage_saved = $item->Offers->Offer->OfferListing->PercentageSaved;

			    // $prod_total_new = $item->OfferSummary->TotalNew;
			    // $prod_total_used = $item->OfferSummary->TotalNew;

			    $prod_listprice_currency_code = $item->ItemAttributes->ListPrice->CurrencyCode;


			    $medium_img_url = $item->MediumImage->URL;
			    if ( $medium_img_url == '' ) {
			    	$image_set = $item->ImageSets;
			    	if ( is_array( $image_set ) ) {
			    		$medium_img_url = $item->ImageSets->ImageSet[0]->MediumImage->URL;
			    	}
			    	else {
			    		$medium_img_url = $item->ImageSets->ImageSet->MediumImage->URL;
			    	}
			    }

			    echo '<div class="amazon-api-prod-box">';

			    	echo '<img src="'.$medium_img_url.'" alt="'.$prod_title.'" class="amazon-api-prod-img" />';

			    	echo '<a href="'.$prod_url.'" target="_blank" class="amazon-api-result-title">';
			    		echo $prod_title;
			    	echo '</a>';

			    	echo '<p><strong>'.__('ASIN', 'ultimateazon2' ).':</strong> '.$prod_asin.'</p>';

			    	if ( $prod_brand == '' ) { echo '<p><strong>'.__('Brand', 'ultimateazon2' ).':</strong> '.__('N/A', 'ultimateazon2' ).'</p>'; }
			    	else {
				    	echo '<p><strong>'.__('Brand', 'ultimateazon2' ).':</strong> '.$prod_brand.'</p>';
				    }


				    if ( $prod_model == '' ) { echo '<p><strong>'.__('Model', 'ultimateazon2' ).':</strong> '.__('N/A', 'ultimateazon2' ).'</p>'; }
			    	else {
				    	echo '<p><strong>'.__('Model', 'ultimateazon2' ).':</strong> '.$prod_model.'</p>';
				    }


				    if ( $prod_upc == '' ) { echo '<p><strong>'.__('UPC', 'ultimateazon2' ).':</strong> '.__('N/A', 'ultimateazon2' ).'</p>'; }
			    	else {
				    	echo '<p><strong>'.__('UPC', 'ultimateazon2' ).':</strong> '.$prod_upc.'</p>';
				    }


				    if ( $prod_lowest_new_price == '' ) { echo '<p><strong>'.__('Lowest New Price', 'ultimateazon2' ).':</strong> '.__('N/A', 'ultimateazon2' ).'</p>'; }
			    	else {
			    		echo '<p><strong>'.__('Lowest New Price', 'ultimateazon2' ).':</strong> '.$prod_lowest_new_price.' '.$prod_lowest_new_price_currency.'</p>';
			    	}

			    	if ( $prod_lowest_used_price == '' ) { echo '<p><strong>'.__('Lowest Used Price', 'ultimateazon2' ).': </strong> '.__('N/A', 'ultimateazon2' ).'</p>'; } 
			    	else {
			    		echo '<p><strong>'.__('Lowest Used Price', 'ultimateazon2' ).':</strong> '.$prod_lowest_used_price.' '.$prod_lowest_used_price_currency.'</p>';
			    	}


			    	if ( $prod_formatted_price == '' ) { echo '<p><strong>'.__('List Price', 'ultimateazon2' ).':</strong> '.__('N/A', 'ultimateazon2' ).'</p>'; }
			    	else {
			    		echo '<p><strong>'.__('List Price', 'ultimateazon2' ).':</strong> '.$prod_formatted_price.' '.$prod_listprice_currency_code.'</p>';
			    	}
			    	

			    	if ( $prod_price_offer == '' ) { echo '<p><strong>'.__('Best Offer', 'ultimateazon2' ).': </strong> <span>'.__('N/A', 'ultimateazon2' ).'</span></p>'; } 
			    	else {
			    		echo '<p><strong>'.__('Best Offer', 'ultimateazon2' ).':</strong> <span>'.$prod_price_offer.' '.$prod_listprice_currencycode.'</span></p>';
			    	}


			    	if ( $prod_amount_saved == '' ) { echo '<p><strong>'.__('Save', 'ultimateazon2' ).': </strong> '.__('N/A', 'ultimateazon2' ).'</p>'; } 
			    	else {
						echo '<p><strong>'.__('Save', 'ultimateazon2' ).': </strong> '.$prod_amount_saved.' '.$prod_amount_saved_currency.' ('.$prod_percentage_saved.'%)</p>';
			    	}



			    	if ( $prod_warranty == '' ) { echo '<p><strong>'.__('Warranty', 'ultimateazon2' ).': </strong> '.__('N/A', 'ultimateazon2' ).'</p>'; } 
			    	else {
			    		echo '<p><strong>'.__('Warranty', 'ultimateazon2' ).':</strong> '.$prod_warranty.'</p>';
			    	}


			    	if ( $prod_features_array == '' ) { echo '<p><strong>'.__('Features', 'ultimateazon2' ).':</strong> '.__('N/A', 'ultimateazon2' ).'</p>'; }
			    	else {
				    	echo '<p class="features"><strong>'.__('Features', 'ultimateazon2' ).':</strong><br />';
				    	echo '<ul>';

				    	foreach ( $prod_features_array as $feature ) {
				    		echo '<li>'.$feature.'</li>';
				    	}

				    	$features_data_attr = array();

				    	// these is the data for the "choose item" button data attribute
				    	foreach ( $prod_features_array as $feature ) {
				    		$features_data_attr[] = '['.$feature.']';
				    	}


				    	echo '</ul>';
				    }
				    

			    	echo "<span class=\"insert-amazon-product\" data-prod-asin=\"".$prod_asin."\">Choose This Item</span>";

			    	echo '<div class="clearfix"></div>';
			    echo '</div>';
			}

		//echo '</div>';

		// USE THIS To SEE FULL RESPONSE
		//echo '<hr /><hr /><hr /><pre>'.print_r($arr,1).'</pre>';

	}




	/**
	 * Add Admin Column to List of CompEAR Lists
	 *
	 * @since    1.0.0
	 */
	public function azontables_admin_column( $columns ) {

		$screen = get_current_screen();
		$screentype = $screen->post_type;

		if( $screentype == 'azontable' ) {
			$columns['azontable_shortcode'] = 'Shortcode';
		}
		return $columns;

	}



	/**
	 * Add content to Admin Column to List of CompEAR Lists
	 *
	 * @since    1.0.0
	 */
	public function azontables_admin_column_content( $column, $id ) {

		$screen = get_current_screen();
		$screentype = $screen->post_type;

		if( 'azontable_shortcode' == $column && $screentype == 'azontable' ) {
			echo '[azontables id="'.$id.'" /]';
		}

	}



	/**
	 * Build our final table array on post save, to make the front end load faster.
	 * EXTENDS PRODUCT REVIEW PRO
	 *
	 * @since    1.0.0
	 */
	public function azontables_save_table_array( $post_id ) {

		$screen = get_current_screen( $post_id );
		// bail early if no ACF data or not on the correct screen
	    if( empty($_POST['acf']) || $screen->id != 'azontable' ) {
	        return;
	    }

	    $final_table = array();

    	// get our user chosen columns
		$columns = get_post_meta( $post_id, 'azontables_choose_columns', true );

		$j=0;

		// If the columns have been set
		if( $columns ) {

			for( $j = 0; $j < $columns; $j++ ) {

				// get the amazon column ID, it's the same as the column type
				$col_id = get_post_meta( $post_id, 'azontables_choose_columns_' . $j . '_column', true );

				// set up ourcolumn headervnames, since they need to be converted form the col type
				if ( $col_id == 'title' ) {
					$clean_name = __('Product', 'azontables');
				}
				elseif ( $col_id == 'asin' ) {
					$clean_name = __('ASIN', 'azontables');
				}
				elseif ( $col_id == 'brand' ) {
					$clean_name = __('Brand', 'azontables');
				}
				elseif ( $col_id == 'model' ) {
					$clean_name = __('Model', 'azontables');
				}
				elseif ( $col_id == 'upc' ) {
					$clean_name = __('UPC', 'azontables');
				}
				elseif ( $col_id == 'features' ) {
					$clean_name = __('Features', 'azontables');
				}
				elseif ( $col_id == 'warranty' ) {
					$clean_name = __('Warranty', 'azontables');
				}
				elseif ( $col_id == 'price' ) {
					$clean_name = __('Price', 'azontables');
				}
				elseif ( $col_id == 'lowest-new-price' ) {
					$clean_name = __('Lowest New Price', 'azontables');
				}
				elseif ( $col_id == 'lowest-used-price' ) {
					$clean_name = __('Lowest Used Price', 'azontables');
				}
				elseif ( $col_id == 'small-image' ) {
					$clean_name = __('Image', 'azontables');
				}
				elseif ( $col_id == 'medium-image' ) {
					$clean_name = __('Image', 'azontables');
				}
				elseif ( $col_id == 'large-image' ) {
					$clean_name = __('Image', 'azontables');
				}
				elseif ( $col_id == 'button-img-1' || $col_id == 'button-img-2' || $col_id == 'button-img-3' || $col_id == 'button-img-4' || $col_id == 'button-img-5' || $col_id == 'button-plain' | $col_id == 'text-link' ) {
					$clean_name = __('Link', 'azontables');
				}
				else {
					$clean_name = '';
				}

				// save our columns data into our final table array for layout
				$final_table['header'][$j]['col_type'] = $col_id;
				$final_table['header'][$j]['col_name'] = $clean_name;
				$final_table['header'][$j]['col_hidden_on'] = get_post_meta( $post_id, 'azontables_choose_columns_' . $j . '_responsive_settings',true );
				$final_table['header'][$j]['col_disable_sort'] = get_post_meta( $post_id, 'azontables_choose_columns_' . $j . '_disable_sorting', true );
				$final_table['header'][$j]['col_width'] = get_post_meta( $post_id, 'azontables_choose_columns_' . $j . '_column_width', true );
				$final_table['header'][$j]['col_width_type'] = get_post_meta( $post_id, 'azontables_choose_columns_' . $j . '_column_width_type', true );
				$final_table['header'][$j]['col_aff_link'] = get_post_meta( $post_id, 'azontables_choose_columns_' . $j . '_field_link', true );
				$final_table['header'][$j]['col_horz_align'] = get_post_meta( $post_id, 'azontables_choose_columns_' . $j . '_column_horz_align', true );
				$final_table['header'][$j]['col_vert_align'] = get_post_meta( $post_id, 'azontables_choose_columns_' . $j . '_column_vert_align', true );

			}

		}

		/* BUILD THE TABLE BODY */

		// get our user chosen products for this table
		$reviews = get_post_meta( $post_id, 'azontables_choose_products', true );
		// grab our table header sub-array
		$header_cols = $final_table['header'];

		$table_body = array();


		if( $reviews && $header_cols ) {

			for( $i = 0; $i < $reviews; $i++ ) {

				$m = 0;
				$prod_id = get_post_meta( $post_id, 'azontables_choose_products_' . $i . '_ama_asin', true );

				foreach ( $header_cols as $col ) {


					$final_table['body'][$i][$m]['prod_id'] = $prod_id;
					$final_table['body'][$i][$m]['col_type'] = $col['col_type'];
					$final_table['body'][$i][$m]['col_width'] = $col['col_width'];
					$final_table['body'][$i][$m]['col_width_type'] = $col['col_width_type'];
					$final_table['body'][$i][$m]['col_aff_link'] = $col['col_aff_link'];
					$final_table['body'][$i][$m]['col_horz_align'] = $col['col_horz_align'];
					$final_table['body'][$i][$m]['col_vert_align'] = $col['col_vert_align'];

					$m++;

				}

			}

		}

    	$prev_final_table = get_post_meta( $post_id, 'azontables_table_array', true );

    	if ( $final_table != $prev_final_table ) {
    		update_post_meta( $post_id, 'azontables_table_array', $final_table, $prev_final_table );
    	}

    	// echo '<pre>'.print_r($final_table,1).'</pre>';
    	// die();

	}








	/**
	 * // get the asin amazon info for displaying on the asontable creation page
	 *
	 * @since    1.0.0
	 */
	public function get_asins_info_for_admin(){

		global $wpdb; // this is how you get access to the database

	    $ama_asins_json = $_REQUEST['ama_asins_json'];


	    $ama_asins_json = stripslashes( $ama_asins_json );

	    // there will be an extra empty array value at the end. This is due to the acfcloneindex-field. just ignore it.
	    $ama_asins_arr = json_decode( $ama_asins_json );


	    $azontables_amazon_api = new azontables_amazon_api();
		$asins_info_arr = $azontables_amazon_api->azontables_get_admin_asins_info( $ama_asins_arr );

		echo json_encode( $asins_info_arr );

	    die(); // this is required to return a proper result

	}








	





	/**
	 * Include the ACF fields
	 *
	 * @since    1.0.0
	 */
	public function azontables_include_acf_fields() {


		// 5. Add Site Options Page
	    if( function_exists('acf_add_options_page') ) {

	        $option_page = acf_add_options_page(array(
	            'page_title'    => __('Azon Tables Settings','azontables'),
	            'menu_title'    => 'Azon Tables Settings',
	            'menu_slug'     => 'azontables-settings',
	            'capability'    => 'edit_posts',
	            'post_id'       => 'azontables-options',
	            'redirect'  => false
	        ));


	    }


		if( function_exists('acf_add_local_field_group') ):

			acf_add_local_field_group(array (
				'key' => 'group_azon_tables_settings',
				'title' => 'Azon Tables Settings',
				'fields' => array (
					array (
						'key' => 'field_t4e6yb36b5eyh53',
						'label' => __('Amazon API', 'azontables' ),
						'name' => '',
						'type' => 'tab',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'placement' => 'top',
						'endpoint' => 0,
					),
					array (
						'key' => 'field_8y7t6vrc5e6cr7v8',
						'label' => __('Amazon Product Advertising API Keys', 'azontables' ),
						'name' => '',
						'type' => 'message',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '',
							'class' => 'azontables-setting-heading',
							'id' => '',
						),
						'message' => __('For instructions on how to obtain your Amazon API keys', 'azontables') . ' <a href="https://azontables.com/amazon-api/" target="_blank">' . __('click here', 'azontables').'.</a>',
						'new_lines' => 'wpautop',
						'esc_html' => 0,
					),
					array (
						'key' => 'field_vr36y356y5eyv5yhb',
						'label' => __('Amazon API Access Key', 'azontables' ),
						'name' => 'amazon_api_key',
						'type' => 'text',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '50',
							'class' => '',
							'id' => '',
						),
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'maxlength' => '',
					),
					array (
						'key' => 'field_35ybh3y5hb46uj4yetv',
						'label' => __('Amazon API Secret Key', 'azontables' ),
						'name' => 'amazon_api_secret',
						'type' => 'text',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '50',
							'class' => '',
							'id' => '',
						),
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'maxlength' => '',
					),
					array (
						'key' => 'field_tye56yb35y6b35yh',
						'label' => __('Default Amazon Search Locale', 'azontables' ),
						'name' => 'default_amazon_search_locale',
						'type' => 'select',
						'instructions' => __('Please set your default search locale using the option below. When you use Product Review Pro, the links you create will default to this location so if most of your visitors come from the United States for example, you would set your default locale to United States.', 'azontables' ),
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'choices' => array (
							'US' => 'United States',
							'UK' => 'United Kingdom',
							'BR' => 'Brazil',
							'CA' => 'Canada',
							'CN' => 'China',
							'FR' => 'France',
							'DE' => 'Germany',
							'IN' => 'India',
							'IT' => 'Italy',
							'JP' => 'Japan',
							'MX' => 'Mexico',
							'ES' => 'Spain',
						),
						'default_value' => array (
						),
						'allow_null' => 0,
						'multiple' => 0,
						'ui' => 0,
						'ajax' => 0,
						'return_format' => 'value',
						'placeholder' => '',
					),
					array (
						'key' => 'field_5ybhtutevgwbyt',
						'label' => __('Amazon Affiliate IDs', 'azontables' ),
						'name' => '',
						'type' => 'message',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '',
							'class' => 'azontables-setting-heading',
							'id' => '',
						),
						'message' => __('Input your Amazon Affiliate ID\'s here. The only Amazon Affiliate ID that is required is the the one that matches your API keys.', 'azontables' ),
						'new_lines' => 'wpautop',
						'esc_html' => 0,
					),
					array (
						'key' => 'field_yb5e4w5vywrb7u5',
						'label' => __('Amazon Affiliate ID - United States', 'azontables' ),
						'name' => 'amazon_affiliate_id_US',
						'type' => 'text',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'maxlength' => '',
					),
					array (
						'key' => 'field_ecqow8yf3iry8b93',
						'label' => __('Amazon Affiliate ID - United Kingdom', 'azontables' ),
						'name' => 'amazon_affiliate_id_UK',
						'type' => 'text',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'maxlength' => '',
					),
					array (
						'key' => 'field_ecq8yfwb9r8r',
						'label' => __('Amazon Affiliate ID - Brazil', 'azontables' ),
						'name' => 'amazon_affiliate_id_BR',
						'type' => 'text',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'maxlength' => '',
					),
					array (
						'key' => 'field_rcf8yq9r38cunf',
						'label' => __('Amazon Affiliate ID - Canada', 'azontables' ),
						'name' => 'amazon_affiliate_id_CA',
						'type' => 'text',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'maxlength' => '',
					),
					array (
						'key' => 'field_cfiybre7cfybwery7c',
						'label' => __('Amazon Affiliate ID - China', 'azontables' ),
						'name' => 'amazon_affiliate_id_CN',
						'type' => 'text',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'maxlength' => '',
					),
					array (
						'key' => 'field_cfi7yr9eewrcgw',
						'label' => __('Amazon Affiliate ID - France', 'azontables' ),
						'name' => 'amazon_affiliate_id_FR',
						'type' => 'text',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'maxlength' => '',
					),
					array (
						'key' => 'field_ero8ucwgn9w9et8g',
						'label' => __('Amazon Affiliate ID - Germany', 'azontables' ),
						'name' => 'amazon_affiliate_id_DE',
						'type' => 'text',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'maxlength' => '',
					),
					array (
						'key' => 'field_egiuywn9gywtgnwe',
						'label' => __('Amazon Affiliate ID - India', 'azontables' ),
						'name' => 'amazon_affiliate_id_IN',
						'type' => 'text',
						'instructions' => __('You must be a resident of India to join this country\'s affiliate program', 'azontables' ),
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'maxlength' => '',
					),
					array (
						'key' => 'field_eguynew9tng9we',
						'label' => __('Amazon Affiliate ID - Italy', 'azontables' ),
						'name' => 'amazon_affiliate_id_IT',
						'type' => 'text',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'maxlength' => '',
					),
					array (
						'key' => 'field_eiry8gnw9eicnietw9',
						'label' => __('Amazon Affiliate ID - Japan', 'azontables' ),
						'name' => 'amazon_affiliate_id_JP',
						'type' => 'text',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'maxlength' => '',
					),
					array (
						'key' => 'field_wie7ycfq9r8nfeq8rgme',
						'label' => __('Amazon Affiliate ID - Mexico', 'azontables' ),
						'name' => 'amazon_affiliate_id_MX',
						'type' => 'text',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'maxlength' => '',
					),
					array (
						'key' => 'field_rfcqu8wi9cun9y78',
						'label' => __('Amazon Affiliate ID - Spain', 'azontables' ),
						'name' => 'amazon_affiliate_id_ES',
						'type' => 'text',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'maxlength' => '',
					),













					array (
						'key' => 'field_tty4erybh',
						'label' => __('Global Table Settings', 'azontables' ),
						'name' => '',
						'type' => 'tab',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'placement' => 'top',
						'endpoint' => 0,
					),
					array (
						'key' => 'field_j67bevgewtv',
						'label' => __('Table Heading', 'azontables'),
						'name' => '',
						'type' => 'message',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '',
							'class' => 'azontables-setting-heading',
							'id' => '',
						),
						'message' => '',
						'new_lines' => 'wpautop',
						'esc_html' => 0,
					),
					array (
						'key' => 'field_ub7tv75rv7t6yb',
						'label' => __('Hide the Table Header', 'azontables'),
						'name' => 'hide_the_table_header',
						'type' => 'true_false',
						'instructions' => __('Sortable rows are not possible if this is checked.', 'azontables'),
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'message' => __('Check this box to hide the table header row.', 'azontables'),
						'default_value' => 0,
					),
					array (
						'key' => 'field_78g6f5r7gt',
						'label' => __('Table Sorting', 'azontables'),
						'name' => '',
						'type' => 'message',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '',
							'class' => 'azontables-setting-heading',
							'id' => '',
						),
						'message' => '',
						'new_lines' => 'wpautop',
						'esc_html' => 0,
					),
					array (
						'key' => 'field_h7g6tfrtg8yh',
						'label' => __('Enable Table Sorting', 'azontables'),
						'name' => 'enable_table_sorting',
						'type' => 'true_false',
						'instructions' => __('(Table sorting will not work if you have the "Hide the Table Header" checkbox checked.)', 'azontables'),
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'message' => __('Check this box to make this table sortable', 'azontables'),
						'default_value' => 0,
					),
					array (
						'key' => 'field_oj9h87gt6gby',
						'label' => __('Table Filtering', 'azontables'),
						'name' => '',
						'type' => 'message',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '',
							'class' => 'azontables-setting-heading',
							'id' => '',
						),
						'message' => '',
						'new_lines' => 'wpautop',
						'esc_html' => 0,
					),
					array (
						'key' => 'field_4e556rt67by78n8u9',
						'label' => __('Enable Table Filtering', 'azontables'),
						'name' => 'enable_table_filtering',
						'type' => 'true_false',
						'instructions' => __('Enabling this will add a search box on top of the table that will filter the table rows by the value entered.', 'azontables'),
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '',
							'class' => 'djn-clear-all',
							'id' => '',
						),
						'message' => __('Check this box to enable table filtering', 'azontables'),
						'default_value' => 0,
					),
					array (
						'key' => 'field_5c6e6rv7trc5',
						'label' => __('No Results Message', 'azontables'),
						'name' => 'no_results_message',
						'type' => 'text',
						'instructions' => __('Enter the message you would like to display if the filter returns 0 results.', 'azontables'),
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_4e556rt67by78n8u9',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '50',
							'class' => '',
							'id' => '',
						),
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'maxlength' => '',
					),
					array (
						'key' => 'field_08rc9ru6cuyv',
						'label' => __('Filter Placeholder Text', 'azontables'),
						'name' => 'filter_placeholder_text',
						'type' => 'text',
						'instructions' => __('Enter the text that will display in the empty filter box.', 'azontables'),
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_4e556rt67by78n8u9',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '50',
							'class' => '',
							'id' => '',
						),
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'maxlength' => '',
					),
					array (
						'key' => 'field_g78gt678657rf56r7f',
						'label' => __('Filter Dropdown Heading', 'azontables'),
						'name' => 'filter_dropdown_heading',
						'type' => 'text',
						'instructions' => __('Enter a Heading for the filter dropdown for which columns are searchable. Leave this blank for no heading.', 'azontables'),
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_4e556rt67by78n8u9',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '50',
							'class' => 'djn-clear-all',
							'id' => '',
						),
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'maxlength' => '',
					),
					array (
						'key' => 'field_7v6c5erv7tb8y',
						'label' => __('Filter Position', 'azontables'),
						'name' => 'filter_position',
						'type' => 'select',
						'instructions' => __('The filter displays to the right by default. It can be placed to the left, center, or right.', 'azontables'),
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_4e556rt67by78n8u9',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '50',
							'class' => '',
							'id' => '',
						),
						'choices' => array (
							'left' => __('Left', 'azontables'),
							'center' => __('Center', 'azontables'),
							'right' => __('Right', 'azontables'),
						),
						'default_value' => array (
							0 => 'right',
						),
						'allow_null' => 0,
						'multiple' => 0,
						'ui' => 0,
						'ajax' => 0,
						'return_format' => 'value',
						'placeholder' => '',
					),
					array (
						'key' => 'field_niub87yvt6rc5v7t',
						'label' => __('Table Pagination', 'azontables'),
						'name' => '',
						'type' => 'message',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '',
							'class' => 'azontables-setting-heading',
							'id' => '',
						),
						'message' => '',
						'new_lines' => 'wpautop',
						'esc_html' => 0,
					),
					array (
						'key' => 'field_e38nu3e8uce47ycby7r',
						'label' => __('Enable Table Pagination', 'azontables'),
						'name' => 'enable_table_pagination',
						'type' => 'true_false',
						'instructions' => __('This should be used for tables with a large amount of rows.', 'azontables'),
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '',
							'class' => 'djn-clear-all',
							'id' => '',
						),
						'message' => __('Check this box to enable table pagination', 'azontables'),
						'default_value' => 0,
					),
					array (
						'key' => 'field_345yg356357j',
						'label' => __('Number of Rows Per Page', 'azontables'),
						'name' => 'number_of_rows_per_page',
						'type' => 'number',
						'instructions' => __('You don\'t want your table to be so long your visitor has trouble using it. Keep it reasonable.', 'azontables'),
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_e38nu3e8uce47ycby7r',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '50',
							'class' => '',
							'id' => '',
						),
						'default_value' => 10,
						'placeholder' => '',
						'prepend' => '',
						'append' => __('Rows', 'azontables'),
						'min' => '',
						'max' => '',
						'step' => '',
					),
					array (
						'key' => 'field_rth5bh3rhvrtwrtv',
						'label' => __('Pagination Position', 'azontables'),
						'name' => 'pagination_position',
						'type' => 'select',
						'instructions' => __('The pagination displays to the center by default. It can be placed to the left, center, or right.', 'azontables'),
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_e38nu3e8uce47ycby7r',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '50',
							'class' => '',
							'id' => '',
						),
						'choices' => array (
							'left' => __('Left', 'azontables'),
							'center' => __('Center', 'azontables'),
							'right' => __('Right', 'azontables'),
						),
						'default_value' => array (
							0 => 'center',
						),
						'allow_null' => 0,
						'multiple' => 0,
						'ui' => 0,
						'ajax' => 0,
						'return_format' => 'value',
						'placeholder' => '',
					),
					array (
						'key' => 'field_9n8u7b6tvr5tv67',
						'label' => __('Table Pagination - Pages Displayed Limit', 'azontables'),
						'name' => 'pages_displayed_limit',
						'type' => 'number',
						'instructions' => __('This setting does not limit the number of pages in your table, it only limits the number of pages that are displayed in the pagination at the same time. Users can still access every page of your full table. This is useful for tables a large amount of rows.', 'azontables'),
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_e38nu3e8uce47ycby7r',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => 'Pages',
						'min' => '',
						'max' => '',
						'step' => '',
					),
					array (
						'key' => 'field_n9u876v5c4778h3',
						'label' => __('Table Memory', 'azontables'),
						'name' => '',
						'type' => 'message',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '',
							'class' => 'azontables-setting-heading',
							'id' => '',
						),
						'message' => '',
						'new_lines' => 'wpautop',
						'esc_html' => 0,
					),
					array (
						'key' => 'field_998ybvt766rcx5e4',
						'label' => __('Enable Table Memory', 'azontables'),
						'name' => 'enable_table_memory',
						'type' => 'true_false',
						'instructions' => __('Enabling this option will store the state of your table in the visitor\'s browser local storage, so the next time they visit the page the table will remember the sorting, filtering, and pagination that the visitor left it as.', 'azontables'),
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'message' => __('Check this box to enable table memory for your visitors.', 'azontables'),
						'default_value' => 0,
					),
					array (
						'key' => 'field_344e5r56y7b8',
						'label' => __('Table Custom Responsive Settings', 'azontables'),
						'name' => '',
						'type' => 'message',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '',
							'class' => 'azontables-setting-heading',
							'id' => '',
						),
						'message' => '',
						'new_lines' => 'wpautop',
						'esc_html' => 0,
					),
					array (
						'key' => 'field_9889u67v564e5',
						'label' => __('Base Responsive Breakpoints off the Width of the Container', 'azontables'),
						'name' => 'breakpoints_off_container',
						'type' => 'true_false',
						'instructions' => __('By default the breakpoints are based off the width of the window. Check this box to base the breakpoints off the width of the container the table resides in.', 'azontables'),
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'message' => __('Check this box to base breakpoints off the parent container width.', 'azontables'),
						'default_value' => 0,
					),
					array (
						'key' => 'field_9j8u3e84cr7yrcfyb',
						'label' => __('Custom Breakpoints', 'azontables'),
						'name' => 'custom_breakpoints',
						'type' => 'true_false',
						'instructions' => __('This setting is if you want to use custom breakpoints for your responsive tables. If you are not familiar with responsive breakpoints, it is best to leave this unchecked.', 'azontables'),
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'message' => __('Check this box to use custom breakpoints for this table.', 'azontables'),
						'default_value' => 0,
					),
					array (
						'key' => 'field_n987t6vr57t',
						'label' => __('Extra Small Breakpoint', 'azontables'),
						'name' => 'extra_small_breakpoint',
						'type' => 'number',
						'instructions' => __('This breakpoint is for extra small screens.', 'azontables'),
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_9j8u3e84cr7yrcfyb',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '25',
							'class' => '',
							'id' => '',
						),
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => 'px',
						'min' => '',
						'max' => '',
						'step' => '',
					),
					array (
						'key' => 'field_8hy7gt6vrfv7t',
						'label' => __('Small Breakpoint', 'azontables'),
						'name' => 'small_breakpoint',
						'type' => 'number',
						'instructions' => __('This breakpoint is for small screens.', 'azontables'),
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_9j8u3e84cr7yrcfyb',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '25',
							'class' => '',
							'id' => '',
						),
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => 'px',
						'min' => '',
						'max' => '',
						'step' => '',
					),
					array (
						'key' => 'field_y7t6vr5ce5r6vt',
						'label' => __('Medium Breakpoint', 'azontables'),
						'name' => 'medium_breakpoint',
						'type' => 'number',
						'instructions' => __('This breakpoint is for medium screens.', 'azontables'),
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_9j8u3e84cr7yrcfyb',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '25',
							'class' => '',
							'id' => '',
						),
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => 'px',
						'min' => '',
						'max' => '',
						'step' => '',
					),
					array (
						'key' => 'field_96rexcttvyuiub',
						'label' => __('Large Breakpoint', 'azontables'),
						'name' => 'large_breakpoint',
						'type' => 'number',
						'instructions' => __('This breakpoint is for large screens.', 'azontables'),
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_9j8u3e84cr7yrcfyb',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '25',
							'class' => '',
							'id' => '',
						),
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => 'px',
						'min' => '',
						'max' => '',
						'step' => '',
					),
					array (
						'key' => 'field_67t6rr56e5e5x',
						'label' => __('Expand the First Row by Default', 'azontables'),
						'name' => 'expand_the_first_row_by_default',
						'type' => 'true_false',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '50',
							'class' => 'djn-clear-all',
							'id' => '',
						),
						'message' => __('Check this box to expand the first row by default', 'azontables'),
						'default_value' => 0,
					),
					array (
						'key' => 'field_6tv765rc65rec',
						'label' => __('Expand All Rows by Default', 'azontables'),
						'name' => 'expand_all_rows_by_default',
						'type' => 'true_false',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '50',
							'class' => '',
							'id' => '',
						),
						'message' => __('Check this box to expand all rows by default', 'azontables'),
						'default_value' => 0,
					),
					array (
						'key' => 'field_6rtvyr6ytrtvryv',
						'label' => __('Hide the Toggle Button', 'azontables'),
						'name' => 'hide_the_toggle_button',
						'type' => 'true_false',
						'instructions' => __('Check this box if you wish to hide the row toggle button. This will not prevent the rows form being opened and closed if clicked.', 'azontables'),
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'message' => __('Check this box to hide the toggle button on each row.', 'azontables'),
						'default_value' => 0,
					),



















					array (
						'key' => 'field_yutvubijhkbhbg',
						'label' => __('Global Table Styles', 'azontables' ),
						'name' => '',
						'type' => 'tab',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'placement' => 'top',
						'endpoint' => 0,
					),
					array (
						'key' => 'field_5897549c4c266123',
						'label' => __('Enable Table Wrapper Border', 'azontables'),
						'name' => 'enable_table_wrapper_border',
						'type' => 'true_false',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'message' => __('Check here to enable a border around the whole table', 'azontables'),
						'default_value' => 0,
					),
					array (
						'key' => 'field_r5c65er6t7b8bt',
						'label' => __('Table Wrapper Border Width', 'azontables'),
						'name' => 'table_wrapper_border_width',
						'type' => 'number',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_5897549c4c266123',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '34',
							'class' => '',
							'id' => '',
						),
						'default_value' => 1,
						'placeholder' => '',
						'prepend' => '',
						'append' => 'px',
						'min' => '',
						'max' => '',
						'step' => '',
					),
					array (
						'key' => 'field_7r56ex45e5tr7yyiub',
						'label' => __('Table Wrapper Border Style', 'azontables'),
						'name' => 'table_wrapper_border_style',
						'type' => 'select',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_5897549c4c266123',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '33',
							'class' => '',
							'id' => '',
						),
						'choices' => array (
							'solid' => __('solid', 'azontables'),
							'dotted' => __('dotted', 'azontables'),
							'dashed' => __('dashed', 'azontables'),
						),
						'default_value' => array (
							0 => 'solid',
						),
						'allow_null' => 0,
						'multiple' => 0,
						'ui' => 0,
						'ajax' => 0,
						'return_format' => 'value',
						'placeholder' => '',
					),
					array (
						'key' => 'field_v7t67r56445exe4x5e5x45',
						'label' => __('Table Wrapper Border Color', 'azontables'),
						'name' => 'table_wrapper_border_color',
						'type' => 'color_picker',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_5897549c4c266123',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '33',
							'class' => '',
							'id' => '',
						),
						'default_value' => '#dedede',
					),
					array (
						'key' => 'field_8byv7867556445xe4e5x',
						'label' => __('Table Header Styles', 'azontables'),
						'name' => '',
						'type' => 'message',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '',
							'class' => 'azontables-setting-heading',
							'id' => '',
						),
						'message' => '',
						'new_lines' => 'wpautop',
						'esc_html' => 0,
					),
					array (
						'key' => 'field_5893ea8c652ee456',
						'label' => __('Enable Table Header Styles', 'azontables'),
						'name' => 'enable_table_header_styles',
						'type' => 'true_false',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'message' => __('Check here to enable the table header styles', 'azontables'),
						'default_value' => 0,
					),
					array (
						'key' => 'field_7t6r55e44ww43w34z',
						'label' => __('Table Header Background Color', 'azontables'),
						'name' => 'table_header_bg_color',
						'type' => 'color_picker',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_5893ea8c652ee456',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '25',
							'class' => '',
							'id' => '',
						),
						'default_value' => '',
					),
					array (
						'key' => 'field_7tv76r56cr6vt7by',
						'label' => __('Table Header Color', 'azontables'),
						'name' => 'table_header_color',
						'type' => 'color_picker',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_5893ea8c652ee456',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '25',
							'class' => '',
							'id' => '',
						),
						'default_value' => '#000000',
					),
					array (
						'key' => 'field_67tv7654cx64xz54',
						'label' => __('Table Header Font Size', 'azontables'),
						'name' => 'table_header_font_size',
						'type' => 'number',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_5893ea8c652ee456',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '25',
							'class' => '',
							'id' => '',
						),
						'default_value' => 14,
						'placeholder' => '',
						'prepend' => '',
						'append' => 'px',
						'min' => '',
						'max' => '',
						'step' => '',
					),
					array (
						'key' => 'field_0i9n987byv876tc56rx',
						'label' => __('Table Header Line Height', 'azontables'),
						'name' => 'table_header_line_height',
						'type' => 'number',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_5893ea8c652ee456',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '25',
							'class' => '',
							'id' => '',
						),
						'default_value' => 16,
						'placeholder' => '',
						'prepend' => '',
						'append' => 'px',
						'min' => '',
						'max' => '',
						'step' => '',
					),
					array (
						'key' => 'field_65rc45ex54x45xe45x',
						'label' => __('Table Header Padding Top', 'azontables'),
						'name' => 'th_padding_top',
						'type' => 'number',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_5893ea8c652ee456',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '25',
							'class' => 'djn-clear-all',
							'id' => '',
						),
						'default_value' => 5,
						'placeholder' => '',
						'prepend' => '',
						'append' => 'px',
						'min' => '',
						'max' => '',
						'step' => '',
					),
					array (
						'key' => 'field_y887f6d5dsrdrvyhuhi',
						'label' => __('Table Header Padding Right', 'azontables'),
						'name' => 'th_padding_right',
						'type' => 'number',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_5893ea8c652ee456',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '25',
							'class' => '',
							'id' => '',
						),
						'default_value' => 5,
						'placeholder' => '',
						'prepend' => '',
						'append' => 'px',
						'min' => '',
						'max' => '',
						'step' => '',
					),
					array (
						'key' => 'field_b98v87c6c6xxrx',
						'label' => __('Table Header Padding Bottom', 'azontables'),
						'name' => 'th_padding_bottom',
						'type' => 'number',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_5893ea8c652ee456',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '25',
							'class' => '',
							'id' => '',
						),
						'default_value' => 5,
						'placeholder' => '',
						'prepend' => '',
						'append' => 'px',
						'min' => '',
						'max' => '',
						'step' => '',
					),
					array (
						'key' => 'field_c65x4x5c6v7b8n9ib',
						'label' => __('Table Header Padding Left', 'azontables'),
						'name' => 'th_padding_left',
						'type' => 'number',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_5893ea8c652ee456',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '25',
							'class' => '',
							'id' => '',
						),
						'default_value' => 5,
						'placeholder' => '',
						'prepend' => '',
						'append' => 'px',
						'min' => '',
						'max' => '',
						'step' => '',
					),
					array (
						'key' => 'field_589756aa0ac13789',
						'label' => __('Enable Table Header Border', 'azontables'),
						'name' => 'enable_table_header_border',
						'type' => 'true_false',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_5893ea8c652ee456',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'message' => __('Check here to enable a border on your table header', 'azontables'),
						'default_value' => 0,
					),
					array (
						'key' => 'field_87tv765c65rctuuibyuyvt',
						'label' => __('Table Header Border Width', 'azontables'),
						'name' => 'table_header_border_width',
						'type' => 'number',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_589756aa0ac13789',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_5893ea8c652ee456',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '25',
							'class' => '',
							'id' => '',
						),
						'default_value' => 5,
						'placeholder' => '',
						'prepend' => '',
						'append' => 'px',
						'min' => '',
						'max' => '',
						'step' => '',
					),
					array (
						'key' => 'field_8h76f54xx5c6v7b',
						'label' => __('Table Header Border Style', 'azontables'),
						'name' => 'table_header_border_style',
						'type' => 'select',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_589756aa0ac13789',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_5893ea8c652ee456',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '25',
							'class' => '',
							'id' => '',
						),
						'choices' => array (
							'solid' => 'solid',
							'dotted' => 'dotted',
							'dashed' => 'dashed',
						),
						'default_value' => array (
							0 => 'solid',
						),
						'allow_null' => 0,
						'multiple' => 0,
						'ui' => 0,
						'ajax' => 0,
						'return_format' => 'value',
						'placeholder' => '',
					),
					array (
						'key' => 'field_9876v5c4x3z4x5c6ubyt',
						'label' => __('Table Header Border Color', 'azontables'),
						'name' => 'table_header_border_color',
						'type' => 'color_picker',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_589756aa0ac13789',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_5893ea8c652ee456',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '25',
							'class' => '',
							'id' => '',
						),
						'default_value' => '#dedede',
					),
					array (
						'key' => 'field_9x2e9un39xeb38rybe8bei',
						'label' => __('Table Header Border Sides', 'azontables'),
						'name' => 'table_header_border_sides',
						'type' => 'checkbox',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_589756aa0ac13789',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_5893ea8c652ee456',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '25',
							'class' => '',
							'id' => '',
						),
						'choices' => array (
							'top' => __('top', 'azontables'),
							'right' => __('right', 'azontables'),
							'bottom' => __('bottom', 'azontables'),
							'left' => __('left', 'azontables'),
						),
						'default_value' => array (
							0 => 'bottom',
						),
						'layout' => 'vertical',
						'toggle' => 0,
						'return_format' => 'value',
					),
					array (
						'key' => 'field_98xw239n89eb397eb39e',
						'label' => __('Table Row Styles', 'azontables'),
						'name' => '',
						'type' => 'message',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '',
							'class' => 'azontables-setting-heading',
							'id' => '',
						),
						'message' => '',
						'new_lines' => 'wpautop',
						'esc_html' => 0,
					),
					array (
						'key' => 'field_589757eb3af1a999',
						'label' => __('Enable Table Row Styles', 'azontables'),
						'name' => 'enable_table_row_styles',
						'type' => 'true_false',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'message' => __('Check here to enable table row styles', 'azontables'),
						'default_value' => 0,
					),
					array (
						'key' => 'field_tyb4b5y65y4tyheyh3r',
						'label' => __('Odd Row Background Color', 'azontables'),
						'name' => 'odd_row_background_color',
						'type' => 'color_picker',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_589757eb3af1a999',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '50',
							'class' => '',
							'id' => '',
						),
						'default_value' => '',
					),
					array (
						'key' => 'field_by65tvr4c3e45vtbyun7uyb',
						'label' => __('Even Row Background Color', 'azontables'),
						'name' => 'even_row_background_color',
						'type' => 'color_picker',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_589757eb3af1a999',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '50',
							'class' => '',
							'id' => '',
						),
						'default_value' => '',
					),
					array (
						'key' => 'field_5899daad73092888',
						'label' => __('Enable Table Row Bottom Border', 'azontables'),
						'name' => 'enable_table_row_bottom_border',
						'type' => 'true_false',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_589757eb3af1a999',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'message' => __('Check Here to enable the table row bottom border styles', 'azontables'),
						'default_value' => 0,
					),
					array (
						'key' => 'field_5rwc4t4v6ye5rcwrgetvgr',
						'label' => __('Table Row Bottom Border Width', 'azontables'),
						'name' => 'table_row_bottom_border_width',
						'type' => 'number',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_589757eb3af1a999',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_5899daad73092888',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '33',
							'class' => '',
							'id' => '',
						),
						'default_value' => 1,
						'placeholder' => '',
						'prepend' => '',
						'append' => 'px',
						'min' => '',
						'max' => '',
						'step' => '',
					),
					array (
						'key' => 'field_rtgvrtbht7n7ujn7ik7iuy',
						'label' => __('Table Row Bottom Border Style', 'azontables'),
						'name' => 'table_row_bottom_border_style',
						'type' => 'select',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_589757eb3af1a999',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_5899daad73092888',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '33',
							'class' => '',
							'id' => '',
						),
						'choices' => array (
							'solid' => __('solid', 'azontables'),
							'dotted' => __('dotted', 'azontables'),
							'dashed' => __('dashed', 'azontables'),
						),
						'default_value' => array (
							0 => 'solid',
						),
						'allow_null' => 0,
						'multiple' => 0,
						'ui' => 0,
						'ajax' => 0,
						'return_format' => 'value',
						'placeholder' => '',
					),
					array (
						'key' => 'field_98xw98wn893e938be3e3',
						'label' => __('Table Row Bottom Border Color', 'azontables'),
						'name' => 'table_row_bottom_border_color',
						'type' => 'color_picker',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_589757eb3af1a999',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_5899daad73092888',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '34',
							'class' => '',
							'id' => '',
						),
						'default_value' => '#dedede',
					),
					array (
						'key' => 'field_5899df7967928777',
						'label' => __('Enable Table Row Font Styles', 'azontables'),
						'name' => 'enable_table_row_font_styles',
						'type' => 'true_false',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_589757eb3af1a999',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'message' => __('Check This to enable the table row font styles', 'azontables'),
						'default_value' => 0,
					),
					array (
						'key' => 'field_rv4t5ty6ty56y5tg',
						'label' => __('Table Row Global Font Size', 'azontables'),
						'name' => 'table_row_global_font_size',
						'type' => 'number',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_589757eb3af1a999',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_5899df7967928777',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '50',
							'class' => '',
							'id' => '',
						),
						'default_value' => 14,
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'min' => '',
						'max' => '',
						'step' => '',
					),
					array (
						'key' => 'field_r6by7un78u7brtv',
						'label' => __('Table Row Global Line Height', 'azontables'),
						'name' => 'table_row_global_line_height',
						'type' => 'number',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_589757eb3af1a999',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_5899df7967928777',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '50',
							'class' => '',
							'id' => '',
						),
						'default_value' => 16,
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'min' => '',
						'max' => '',
						'step' => '',
					),
					array (
						'key' => 'field_5gyh567ji79k7j8yiu7t6yrt',
						'label' => __('Table Row Font Color Odd Rows', 'azontables'),
						'name' => 'table_row_font_color_odd_rows',
						'type' => 'color_picker',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_589757eb3af1a999',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_5899df7967928777',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '25',
							'class' => '',
							'id' => '',
						),
						'default_value' => '#000000',
					),
					array (
						'key' => 'field_645d53s5465c7',
						'label' => __('Table Row Link Color Odd Rows', 'azontables'),
						'name' => 'table_row_link_color_odd_rows',
						'type' => 'color_picker',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_589757eb3af1a999',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_5899df7967928777',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '25',
							'class' => '',
							'id' => '',
						),
						'default_value' => '#000000',
					),
					array (
						'key' => 'field_9283n2938n293b287b87b738',
						'label' => __('Table Row Button Background Color Odd Rows', 'azontables'),
						'name' => 'table_row_button_bg_color_odd_rows',
						'type' => 'color_picker',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_589757eb3af1a999',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_5899df7967928777',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '25',
							'class' => '',
							'id' => '',
						),
						'default_value' => '#000000',
					),
					array (
						'key' => 'field_5e43s43567v8b99niiu',
						'label' => __('Table Row Button Text Color Odd Rows', 'azontables'),
						'name' => 'table_row_button_text_color_odd_rows',
						'type' => 'color_picker',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_589757eb3af1a999',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_5899df7967928777',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '25',
							'class' => '',
							'id' => '',
						),
						'default_value' => '#000000',
					),
					array (
						'key' => 'field_9x8w8n209e8n209e82en2',
						'label' => __('Table Row Font Color Even Rows', 'azontables'),
						'name' => 'table_row_font_color_even_rows',
						'type' => 'color_picker',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_589757eb3af1a999',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_5899df7967928777',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '25',
							'class' => '',
							'id' => '',
						),
						'default_value' => '#000000',
					),
					array (
						'key' => 'field_dyc4br948ryb89ru8u8r',
						'label' => __('Table Row Link Color Even Rows', 'azontables'),
						'name' => 'table_row_link_color_even_rows',
						'type' => 'color_picker',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_589757eb3af1a999',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_5899df7967928777',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '25',
							'class' => '',
							'id' => '',
						),
						'default_value' => '#000000',
					),
					array (
						'key' => 'field_98h7g6f54d6f7g8h976vt',
						'label' => __('Table Row Button Background Color Even Rows', 'azontables'),
						'name' => 'table_row_button_bg_color_even_rows',
						'type' => 'color_picker',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_589757eb3af1a999',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_5899df7967928777',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '25',
							'class' => '',
							'id' => '',
						),
						'default_value' => '#000000',
					),
					array (
						'key' => 'field_iuyvthu7yvtubvtc',
						'label' => __('Table Row Button Text Color Even Rows', 'azontables'),
						'name' => 'table_row_button_text_color_even_rows',
						'type' => 'color_picker',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_589757eb3af1a999',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_5899df7967928777',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '25',
							'class' => '',
							'id' => '',
						),
						'default_value' => '#000000',
					),
					array (
						'key' => 'field_589a173174147666',
						'label' => __('Enable Font Styles for Special Field Types', 'azontables'),
						'name' => 'enable_font_styles_for_field_types',
						'type' => 'true_false',
						'instructions' => __('These apply to these special field type: ASIN, Brand, Model, UPC, Features, Warranty, Price, Lowest Used Price, Lowest New Price, Star Rating, Yes/No', 'azontables'),
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_589757eb3af1a999',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_5899df7967928777',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'message' => '',
						'default_value' => 0,
					),
					array (
						'key' => 'field_589a24e769fcc555',
						'label' => __('Enable ASIN Field Styles', 'azontables'),
						'name' => 'enable_asin_field_styles',
						'type' => 'true_false',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_589757eb3af1a999',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_5899df7967928777',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_589a173174147666',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'message' => __('Check here to enable "ASIN" field styles', 'azontables'),
						'default_value' => 0,
					),
					array (
						'key' => 'field_cqer34vt4y56y5',
						'label' => __('ASIN Font Size', 'azontables'),
						'name' => 'asin_font_size',
						'type' => 'number',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_589757eb3af1a999',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_5899df7967928777',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_589a173174147666',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_589a24e769fcc555',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '33',
							'class' => '',
							'id' => '',
						),
						'default_value' => 14,
						'placeholder' => '',
						'prepend' => '',
						'append' => 'px',
						'min' => '',
						'max' => '',
						'step' => '',
					),
					array (
						'key' => 'field_etv56y5b6yt3c4r',
						'label' => __('ASIN Line Height', 'azontables'),
						'name' => 'asin_line_height',
						'type' => 'number',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_589757eb3af1a999',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_5899df7967928777',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_589a173174147666',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_589a24e769fcc555',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '33',
							'class' => '',
							'id' => '',
						),
						'default_value' => 16,
						'placeholder' => '',
						'prepend' => '',
						'append' => 'px',
						'min' => '',
						'max' => '',
						'step' => '',
					),
					array (
						'key' => 'field_ev5tyb6un6i8n4',
						'label' => __('ASIN Text Color', 'azontables'),
						'name' => 'asin_text_color',
						'type' => 'color_picker',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_589757eb3af1a999',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_5899df7967928777',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_589a173174147666',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_589a24e769fcc555',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '34',
							'class' => '',
							'id' => '',
						),
						'default_value' => '#000',
					),
					array (
						'key' => 'field_589a24fe69fcd222',
						'label' => __('Enable Brand Field Styles', 'azontables'),
						'name' => 'enable_brand_field_styles',
						'type' => 'true_false',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_589757eb3af1a999',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_5899df7967928777',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_589a173174147666',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'message' => __('Check here to enable "brand" field styles', 'azontables'),
						'default_value' => 0,
					),
					array (
						'key' => 'field_2345cwrtbryyh4by6y56yb56',
						'label' => __('Brand Font Size', 'azontables'),
						'name' => 'brand_font_size',
						'type' => 'number',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_589757eb3af1a999',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_5899df7967928777',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_589a173174147666',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_589a24fe69fcd222',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '33',
							'class' => '',
							'id' => '',
						),
						'default_value' => 14,
						'placeholder' => '',
						'prepend' => '',
						'append' => 'px',
						'min' => '',
						'max' => '',
						'step' => '',
					),
					array (
						'key' => 'field_rtby57u68u78u78i78',
						'label' => __('Brand Line Height', 'azontables'),
						'name' => 'brand_line_height',
						'type' => 'number',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_589757eb3af1a999',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_5899df7967928777',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_589a173174147666',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_589a24fe69fcd222',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '33',
							'class' => '',
							'id' => '',
						),
						'default_value' => 16,
						'placeholder' => '',
						'prepend' => '',
						'append' => 'px',
						'min' => '',
						'max' => '',
						'step' => '',
					),
					array (
						'key' => 'field_2cr3c5v35v35353v5',
						'label' => __('Brand Text Color', 'azontables'),
						'name' => 'brand_text_color',
						'type' => 'color_picker',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_589757eb3af1a999',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_5899df7967928777',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_589a173174147666',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_589a24fe69fcd222',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '34',
							'class' => '',
							'id' => '',
						),
						'default_value' => '#000',
					),
					array (
						'key' => 'field_589a250969fce111',
						'label' => __('Enable Model Field Styles', 'azontables'),
						'name' => 'enable_model_field_styles',
						'type' => 'true_false',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_589757eb3af1a999',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_5899df7967928777',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_589a173174147666',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'message' => __('Check here to enable "model" field styles', 'azontables'),
						'default_value' => 0,
					),
					array (
						'key' => 'field_rebyutnitrbyetvcawtveyrbutn',
						'label' => __('Model Font Size', 'azontables'),
						'name' => 'model_font_size',
						'type' => 'number',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_589757eb3af1a999',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_5899df7967928777',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_589a173174147666',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_589a250969fce111',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '33',
							'class' => '',
							'id' => '',
						),
						'default_value' => 14,
						'placeholder' => '',
						'prepend' => '',
						'append' => 'px',
						'min' => '',
						'max' => '',
						'step' => '',
					),
					array (
						'key' => 'field_by76cr5ex46cr7tvy8bu',
						'label' => __('Model Line Height', 'azontables'),
						'name' => 'model_line_height',
						'type' => 'number',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_589757eb3af1a999',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_5899df7967928777',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_589a173174147666',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_589a250969fce111',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '33',
							'class' => '',
							'id' => '',
						),
						'default_value' => 16,
						'placeholder' => '',
						'prepend' => '',
						'append' => 'px',
						'min' => '',
						'max' => '',
						'step' => '',
					),
					array (
						'key' => 'field_8h7g6f54xerctvybu',
						'label' => __('Model Text Color', 'azontables'),
						'name' => 'model_text_color',
						'type' => 'color_picker',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_589757eb3af1a999',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_5899df7967928777',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_589a173174147666',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_589a250969fce111',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '34',
							'class' => '',
							'id' => '',
						),
						'default_value' => '#000',
					),
					array (
						'key' => 'field_589a251069fcf999',
						'label' => __('Enable UPC Field Styles', 'azontables'),
						'name' => 'enable_upc_field_styles',
						'type' => 'true_false',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_589757eb3af1a999',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_5899df7967928777',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_589a173174147666',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'message' => __('Check here to enable "UPC" field styles', 'azontables'),
						'default_value' => 0,
					),
					array (
						'key' => 'field_345b7nutyb5v4rcw4vetbryt',
						'label' => __('UPC Font Size', 'azontables'),
						'name' => 'upc_font_size',
						'type' => 'number',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_589757eb3af1a999',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_5899df7967928777',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_589a173174147666',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_589a251069fcf999',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '33',
							'class' => '',
							'id' => '',
						),
						'default_value' => 14,
						'placeholder' => '',
						'prepend' => '',
						'append' => 'px',
						'min' => '',
						'max' => '',
						'step' => '',
					),
					array (
						'key' => 'field_uytv6rc8rcrynbd',
						'label' => __('UPC Line Height', 'azontables'),
						'name' => 'upc_line_height',
						'type' => 'number',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_589757eb3af1a999',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_5899df7967928777',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_589a173174147666',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_589a251069fcf999',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '33',
							'class' => '',
							'id' => '',
						),
						'default_value' => 16,
						'placeholder' => '',
						'prepend' => '',
						'append' => 'px',
						'min' => '',
						'max' => '',
						'step' => '',
					),
					array (
						'key' => 'field_6tv5454tyvuyuibhbj',
						'label' => __('UPC Text Color', 'azontables'),
						'name' => 'upc_text_color',
						'type' => 'color_picker',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_589757eb3af1a999',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_5899df7967928777',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_589a173174147666',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_589a251069fcf999',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '34',
							'class' => '',
							'id' => '',
						),
						'default_value' => '#000',
					),
					array (
						'key' => 'field_589a251b69fd0888',
						'label' => __('Enable Features Field Styles', 'azontables'),
						'name' => 'enable_features_field_styles',
						'type' => 'true_false',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_589757eb3af1a999',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_5899df7967928777',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_589a173174147666',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'message' => __('Check here to enable "features" field styles', 'azontables'),
						'default_value' => 0,
					),
					array (
						'key' => 'field_ev5t4v5tv4t45t',
						'label' => __('Features Font Size', 'azontables'),
						'name' => 'features_font_size',
						'type' => 'number',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_589757eb3af1a999',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_5899df7967928777',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_589a173174147666',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_589a251b69fd0888',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '33',
							'class' => '',
							'id' => '',
						),
						'default_value' => 12,
						'placeholder' => '',
						'prepend' => '',
						'append' => 'px',
						'min' => '',
						'max' => '',
						'step' => '',
					),
					array (
						'key' => 'field_rtyb57uy67u676',
						'label' => __('Features Line Height', 'azontables'),
						'name' => 'features_line_height',
						'type' => 'number',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_589757eb3af1a999',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_5899df7967928777',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_589a173174147666',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_589a251b69fd0888',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '33',
							'class' => '',
							'id' => '',
						),
						'default_value' => 14,
						'placeholder' => '',
						'prepend' => '',
						'append' => 'px',
						'min' => '',
						'max' => '',
						'step' => '',
					),
					array (
						'key' => 'field_rtyb67uby67b66',
						'label' => __('Features Text Color', 'azontables'),
						'name' => 'features_text_color',
						'type' => 'color_picker',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_589757eb3af1a999',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_5899df7967928777',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_589a173174147666',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_589a251b69fd0888',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '34',
							'class' => '',
							'id' => '',
						),
						'default_value' => '#000',
					),
					array (
						'key' => 'field_589a252469fd1777',
						'label' => __('Enable Warranty Field Styles', 'azontables'),
						'name' => 'enable_warranty_field_styles',
						'type' => 'true_false',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_589757eb3af1a999',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_5899df7967928777',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_589a173174147666',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'message' => __('Check here to enable "warranty" field styles', 'azontables'),
						'default_value' => 0,
					),
					array (
						'key' => 'field_76u6n7u7878u6uybyrt',
						'label' => __('Warranty Font Size', 'azontables'),
						'name' => 'warranty_font_size',
						'type' => 'number',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_589757eb3af1a999',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_5899df7967928777',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_589a173174147666',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_589a252469fd1777',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '33',
							'class' => '',
							'id' => '',
						),
						'default_value' => 14,
						'placeholder' => '',
						'prepend' => '',
						'append' => 'px',
						'min' => '',
						'max' => '',
						'step' => '',
					),
					array (
						'key' => 'field_i7bywbr73byr93bruc3rbei',
						'label' => __('Warranty Line Height', 'azontables'),
						'name' => 'warranty_line_height',
						'type' => 'number',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_589757eb3af1a999',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_5899df7967928777',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_589a173174147666',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_589a252469fd1777',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '33',
							'class' => '',
							'id' => '',
						),
						'default_value' => 16,
						'placeholder' => '',
						'prepend' => '',
						'append' => 'px',
						'min' => '',
						'max' => '',
						'step' => '',
					),
					array (
						'key' => 'field_67iu6y5fr4tyrgvbg',
						'label' => __('Warranty Text Color', 'azontables'),
						'name' => 'warranty_text_color',
						'type' => 'color_picker',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_589757eb3af1a999',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_5899df7967928777',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_589a173174147666',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_589a252469fd1777',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '34',
							'class' => '',
							'id' => '',
						),
						'default_value' => '#000',
					),
					array (
						'key' => 'field_589a253769fd2666',
						'label' => __('Enable Price Field Styles', 'azontables'),
						'name' => 'enable_price_field_styles',
						'type' => 'true_false',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_589757eb3af1a999',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_5899df7967928777',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_589a173174147666',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'message' => __('Check here to enable "price" field styles', 'azontables'),
						'default_value' => 0,
					),
					array (
						'key' => 'field_b5y6y56y5y656yb5by',
						'label' => __('Slashed Price Font Size', 'azontables'),
						'name' => 'slashed_price_font_size',
						'type' => 'number',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_589757eb3af1a999',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_5899df7967928777',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_589a173174147666',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_589a253769fd2666',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '50',
							'class' => '',
							'id' => '',
						),
						'default_value' => 14,
						'placeholder' => '',
						'prepend' => '',
						'append' => 'px',
						'min' => '',
						'max' => '',
						'step' => '',
					),
					array (
						'key' => 'field_erty456y5y56ye56ye',
						'label' => __('Slashed Price Color', 'azontables'),
						'name' => 'slashed_price_color',
						'type' => 'color_picker',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_589757eb3af1a999',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_5899df7967928777',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_589a173174147666',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_589a253769fd2666',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '50',
							'class' => '',
							'id' => '',
						),
						'default_value' => '#999999',
					),
					array (
						'key' => 'field_ery6b5yu67u6uj65u',
						'label' => __('Sale Price Font Size', 'azontables'),
						'name' => 'sale_price_font_size',
						'type' => 'number',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_589757eb3af1a999',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_5899df7967928777',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_589a173174147666',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_589a253769fd2666',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '50',
							'class' => '',
							'id' => '',
						),
						'default_value' => 18,
						'placeholder' => '',
						'prepend' => '',
						'append' => 'px',
						'min' => '',
						'max' => '',
						'step' => '',
					),
					array (
						'key' => 'field_5e67h6u767uy4g56y3',
						'label' => __('Sale Price Color', 'azontables'),
						'name' => 'sale_price_color',
						'type' => 'color_picker',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_589757eb3af1a999',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_5899df7967928777',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_589a173174147666',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_589a253769fd2666',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '50',
							'class' => '',
							'id' => '',
						),
						'default_value' => '#3ABB22',
					),
					array (
						'key' => 'field_e6yg5etgv5twtv35yv',
						'label' => __('Amount/Percentage Saved Font Size', 'azontables'),
						'name' => 'amountpercentage_saved_font_size',
						'type' => 'number',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_589757eb3af1a999',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_5899df7967928777',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_589a173174147666',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_589a253769fd2666',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '50',
							'class' => '',
							'id' => '',
						),
						'default_value' => 18,
						'placeholder' => '',
						'prepend' => '',
						'append' => 'px',
						'min' => '',
						'max' => '',
						'step' => '',
					),
					array (
						'key' => 'field_tvrtwtybr5tuybr6ujbtyhb',
						'label' => __('Amount/Percentage Saved Color', 'azontables'),
						'name' => 'amountpercentage_saved_color',
						'type' => 'color_picker',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_589757eb3af1a999',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_5899df7967928777',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_589a173174147666',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_589a253769fd2666',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '50',
							'class' => '',
							'id' => '',
						),
						'default_value' => '#000000',
					),
					array (
						'key' => 'field_589a254769fd3555',
						'label' => __('Enable Lowest New Price Field Styles', 'azontables'),
						'name' => 'enable_lowest_new_price_field_styles',
						'type' => 'true_false',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_589757eb3af1a999',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_5899df7967928777',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_589a173174147666',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'message' => __('Check here to enable "lowest new price" field styles', 'azontables'),
						'default_value' => 0,
					),
					array (
						'key' => 'field_tg5y6tve5t4ty4ge',
						'label' => __('Lowest New Price Font Size', 'azontables'),
						'name' => 'lowest_new_price_font_size',
						'type' => 'number',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_589757eb3af1a999',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_5899df7967928777',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_589a173174147666',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_589a254769fd3555',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '33',
							'class' => '',
							'id' => '',
						),
						'default_value' => 14,
						'placeholder' => '',
						'prepend' => '',
						'append' => 'px',
						'min' => '',
						'max' => '',
						'step' => '',
					),
					array (
						'key' => 'field_evtetetgetgetgvetgvetg',
						'label' => __('Lowest New Price Line Height', 'azontables'),
						'name' => 'lowest_new_price_line_height',
						'type' => 'number',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_589757eb3af1a999',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_5899df7967928777',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_589a173174147666',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_589a254769fd3555',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '33',
							'class' => '',
							'id' => '',
						),
						'default_value' => 16,
						'placeholder' => '',
						'prepend' => '',
						'append' => 'px',
						'min' => '',
						'max' => '',
						'step' => '',
					),
					array (
						'key' => 'field_evegfewy9ey87dey78e',
						'label' => __('Lowest New Price Text Color', 'azontables'),
						'name' => 'lowest_new_price_text_color',
						'type' => 'color_picker',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_589757eb3af1a999',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_5899df7967928777',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_589a173174147666',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_589a254769fd3555',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '34',
							'class' => '',
							'id' => '',
						),
						'default_value' => '#000',
					),
					array (
						'key' => 'field_589a254f69fd4444',
						'label' => __('Enable Lowest Used Price Field Styles', 'azontables'),
						'name' => 'enable_lowest_used_price_field_styles',
						'type' => 'true_false',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_589757eb3af1a999',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_5899df7967928777',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_589a173174147666',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'message' => __('Check here to enable "lowest used price" field styles', 'azontables'),
						'default_value' => 0,
					),
					array (
						'key' => 'field_e5tg465ybr6yb46gvetger',
						'label' => __('Lowest Used Price Font Size', 'azontables'),
						'name' => 'lowest_used_price_font_size',
						'type' => 'number',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_589757eb3af1a999',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_5899df7967928777',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_589a173174147666',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_589a254f69fd4444',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '33',
							'class' => '',
							'id' => '',
						),
						'default_value' => 14,
						'placeholder' => '',
						'prepend' => '',
						'append' => 'px',
						'min' => '',
						'max' => '',
						'step' => '',
					),
					array (
						'key' => 'field_8765c4xc6v7i7g65f7gd',
						'label' => __('Lowest Used Price Line Height', 'azontables'),
						'name' => 'lowest_used_price_line_height',
						'type' => 'number',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_589757eb3af1a999',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_5899df7967928777',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_589a173174147666',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_589a254f69fd4444',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '33',
							'class' => '',
							'id' => '',
						),
						'default_value' => 16,
						'placeholder' => '',
						'prepend' => '',
						'append' => 'px',
						'min' => '',
						'max' => '',
						'step' => '',
					),
					array (
						'key' => 'field_7tc76x7xutcitciutycuyt',
						'label' => __('Lowest Used Price Text Color', 'azontables'),
						'name' => 'lowest_used_price_text_color',
						'type' => 'color_picker',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_589757eb3af1a999',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_5899df7967928777',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_589a173174147666',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_589a254f69fd4444',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '34',
							'class' => '',
							'id' => '',
						),
						'default_value' => '#000',
					),
					array (
						'key' => 'field_67t67r55ee45e5cry',
						'label' => __('Table Footer Styles', 'azontables'),
						'name' => '',
						'type' => 'message',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '',
							'class' => 'azontables-setting-heading',
							'id' => '',
						),
						'message' => '',
						'new_lines' => 'wpautop',
						'esc_html' => 0,
					),
					array (
						'key' => 'field_o7yb6vt5c4vt7by8333',
						'label' => __('Enable Table Footer Styles', 'azontables'),
						'name' => 'enable_table_footer_styles',
						'type' => 'true_false',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'message' => __('Check here to enable the table footer styles', 'azontables'),
						'default_value' => 0,
					),
					array (
						'key' => 'field_u6tv76t7tviyvyivui',
						'label' => __('Table Footer Background Color', 'azontables'),
						'name' => 'table_footer_bg_color',
						'type' => 'color_picker',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_o7yb6vt5c4vt7by8333',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '25',
							'class' => '',
							'id' => '',
						),
						'default_value' => '',
					),

					array (
						'key' => 'field_ytrr56etxrterxrcytytuv',
						'label' => __('Table Footer Button Background Color', 'azontables'),
						'name' => 'table_footer_button_bg_color',
						'type' => 'color_picker',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_o7yb6vt5c4vt7by8333',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '25',
							'class' => '',
							'id' => '',
						),
						'default_value' => '#FFFFFF',
					),
					array (
						'key' => 'field_iuytv6cr5ex456f4xerxr',
						'label' => __('Table Footer Button Text Color', 'azontables'),
						'name' => 'table_footer_button_text_color',
						'type' => 'color_picker',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_o7yb6vt5c4vt7by8333',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '25',
							'class' => '',
							'id' => '',
						),
						'default_value' => '#FFFFFF',
					),
					array (
						'key' => 'field_76rrc565ex44e54w5z',
						'label' => __('Table Footer Text Color', 'azontables'),
						'name' => 'table_footer_text_color',
						'type' => 'color_picker',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_o7yb6vt5c4vt7by8333',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '25',
							'class' => '',
							'id' => '',
						),
						'default_value' => '#FFFFFF',
					),















					array (
						'key' => 'field_98h876765654e65rcvt7v',
						'label' => __('Help', 'azontables' ),
						'name' => '',
						'type' => 'tab',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'placement' => 'top',
						'endpoint' => 0,
					),

					array (
						'key' => 'field_h78h87tbutvyrctrecrtyvyu',
						'label' => __('Help Resources', 'azontables'),
						'name' => '',
						'type' => 'message',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '',
							'class' => 'azontables-setting-heading',
							'id' => '',
						),
						'message' => '<h4><a href="https://azontables.com/documentation/" target="_blank">'. __('Documentation', 'azontables') . '</a> - ' .  __('Learn how to use Azon Tables lightning fast.', 'azontables') . '</h4>' . '<h4><a href="https://azontables.com/faq/" target="_blank">'. __('Frequently Asked Questions', 'azontables') . '</a> - ' .  __('get fast answers to common quesitons.', 'azontables') . '</h4>' . '<h4><a href="https://azontables.com/support/" target="_blank">'. __('Support', 'azontables') . '</a> - ' .  __('Sometimes things happen. The nature of the internet. Need support? just make sure you have searched the documentation and FAQs first.', 'azontables') . '</h4>!<br />@<br />#<br />$<br />%<br />^<br />&<br />*<br /><h3>' .  __('Credits:', 'azontables') . '</h3><p>' .  __('Azon Tables is developed by Alchemy Coder. Learn more at', 'azontables') . ' <a href="https://AlchemyCoder.com" target="_blank">AlchemyCoder.com</a></p><pre style="font-size: 10px;line-height: 10px;">
    ___    __     __                            ______          __         
   /   |  / /____/ /_  ___  ____ ___  __  __   / ____/___  ____/ /__  _____
  / /| | / / ___/ __ \/ _ \/ __ `__ \/ / / /  / /   / __ \/ __  / _ \/ ___/
 / ___ |/ / /__/ / / /  __/ / / / / / /_/ /  / /___/ /_/ / /_/ /  __/ /    
/_/  |_/_/\___/_/ /_/\___/_/ /_/ /_/\__, /   \____/\____/\__,_/\___/_/     
                                   /____/                                  
</pre>',
						'new_lines' => 'wpautop',
						'esc_html' => 0,
					),
















				),
				'location' => array (
					array (
						array (
							'param' => 'options_page',
							'operator' => '==',
							'value' => 'azontables-settings',
						),
					),
				),
				'menu_order' => 0,
				'position' => 'normal',
				'style' => 'default',
				'label_placement' => 'top',
				'instruction_placement' => 'label',
				'hide_on_screen' => '',
				'active' => 1,
				'description' => '',
			));

			

		endif;






		acf_add_local_field_group(array (
				'key' => 'group_tybhthyhythyyth',
				'title' => __('Create your Table', 'azontables'),
				'fields' => array (

					array (
						'key' => 'field_utv76r5c76tv87yb',
						'label' => __('Step 1 - Create your Columns', 'azontables' ),
						'name' => '',
						'type' => 'message',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '',
							'class' => 'azontables-create-heading',
							'id' => '',
						),
						'message' => __('Click the Add Column button, choose your column data, and when you\'re done you can toggle each column open and closed with the triangle in the top left. You can also drag and drop your columns to change the order.', 'azontables'),
						'new_lines' => 'wpautop',
						'esc_html' => 0,
					),

					array (
						'key' => 'field_thbyhrtyhbtyhbthy',
						'label' => __('Create Columns', 'azontables'),
						'name' => 'azontables_choose_columns',
						'type' => 'repeater',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'collapsed' => 'field_wtvyve5ygw4ertv5ehrtv',
						'min' => '',
						'max' => '',
						'layout' => 'row',
						'button_label' => __('Add Column', 'azontables'),
						'sub_fields' => array (
							array (
								'key' => 'field_wtvyve5ygw4ertv5ehrtv',
								'label' => __('Column', 'azontables'),
								'name' => 'column',
								'type' => 'select',
								'instructions' => '',
								'required' => 0,
								'conditional_logic' => 0,
								'wrapper' => array (
									'width' => '',
									'class' => '',
									'id' => '',
								),
								'choices' => array (
									'title' => __('Product Title', 'azontables' ),
									'asin' => __('ASIN', 'azontables' ),
									'brand' => __('Brand', 'azontables' ),
									'model' => __('Model', 'azontables' ),
									'upc' => __('UPC', 'azontables' ),
									'features' => __('Features', 'azontables' ),
									'warranty' => __('Warranty', 'azontables' ),
									'price' => __('Price', 'azontables' ),
									'lowest-new-price' => __('Lowest New Price', 'azontables' ),
									'lowest-used-price' => __('Lowest Used Price', 'azontables' ),
									'small-image' => __('Small Image', 'azontables' ),
									'medium-image' => __('Medium Image', 'azontables' ),
									'large-image' => __('Large Image', 'azontables' ),
									'button-img-1' => __('Affiliate Link - Image #1 Button', 'azontables' ),
									'button-img-2' => __('Affiliate Link - Image #2 Button', 'azontables' ),
									'button-img-3' => __('Affiliate Link - Image #3 Button', 'azontables' ),
									'button-img-4' => __('Affiliate Link - Image #4 Button', 'azontables' ),
									'button-img-5' => __('Affiliate Link - Image #5 Button', 'azontables' ),
									'button-plain' => __('Affiliate Link - Plain Button', 'azontables' ),
									'text-link' => __('Affiliate Link - Text', 'azontables' ),
								),
								'default_value' => 'title',
								'placeholder' => '',
								'prepend' => '',
								'append' => '',
								'maxlength' => '',
							),

							array (
								'key' => 'field_ewrftwetgrtehbeyrbh5vetrgtv',
								'label' => __('Responsive Settings', 'azontables'),
								'name' => 'responsive_settings',
								'type' => 'checkbox',
								'instructions' => __('Check which screen sizes you would like to hide this column on.', 'azontables'),
								'required' => 0,
								'conditional_logic' => 0,
								'wrapper' => array (
									'width' => '',
									'class' => '',
									'id' => '',
								),
								'choices' => array(
									'xs' => __('Extra Small', 'azontables'),
									'sm' => __('Small', 'azontables'),
									'md' => __('Medium', 'azontables'),
									'lg' => __('Large', 'azontables'),
								),
								'default_value' => '',
								'layout' => 'horizontal',
								'placeholder' => '',
								'prepend' => '',
								'append' => '',
								'maxlength' => '',
							),

							array (
								'key' => 'field_wtvgeyhbyjbtyhertgvwtegv',
								'label' => __('Column Sorting', 'azontables'),
								'name' => 'disable_sorting',
								'type' => 'true_false',
								'instructions' => '',
								'required' => 0,
								'conditional_logic' => 0,
								'wrapper' => array (
									'width' => '',
									'class' => '',
									'id' => '',
								),
								'message' => __('Check this box to disable sorting on this column. (Image specification types are not sortable already)', 'azontables'),
								'default_value' => 0,
							),

							array (
								'key' => 'field_wvtgrthbtyjbrujn7tj',
								'label' => __('Column Width', 'azontables'),
								'name' => 'column_width',
								'type' => 'number',
								'instructions' => '',
								'required' => 0,
								'conditional_logic' => 0,
								'wrapper' => array (
									'width' => '70',
									'class' => '',
									'id' => '',
								),
								'default_value' => '',
								'placeholder' => '',
								'prepend' => '',
								'append' => '',
								'min' => '',
								'max' => '',
								'step' => '',
							),

							array (
								'key' => 'field_qetvyrbyejteyhvtweg',
								'label' => __('Column Width Type', 'azontables'),
								'name' => 'column_width_type',
								'type' => 'select',
								'instructions' => '',
								'required' => 0,
								'conditional_logic' => 0,
								'wrapper' => array (
									'width' => '30',
									'class' => '',
									'id' => '',
								),
								'choices' => array (
									'px' => 'px',
									'%' => '%',
								),
								'default_value' => array (
								),
								'allow_null' => 1,
								'multiple' => 0,
								'ui' => 0,
								'ajax' => 0,
								'return_format' => 'value',
								'placeholder' => '',
							),

							array(
								'key' => 'field_qe5yuhunknutyhbwrtg',
								'label' => __('Link', 'azontables'),
								'name' => 'field_link',
								'type' => 'select',
								'instructions' => '',
								'required' => 0,
								'conditional_logic' => 0,
								'wrapper' => array (
									'width' => '20',
									'class' => '',
									'id' => '',
								),
								'choices' => array (
									'field-link-none' => __('No Link', 'azontables'),
									'field-link-affiliate' => __('Affiliate Link', 'azontables'),
								),
								'default_value' => '',
								'placeholder' => '',
								'prepend' => '',
								'append' => '',
								'maxlength' => '',
							),


							array (
								'key' => 'field_wevthbtujrbutjhbrvtwg',
								'label' => __('Column Horizontal Alignment', 'azontables'),
								'name' => 'column_horz_align',
								'type' => 'select',
								'instructions' => '',
								'required' => 0,
								'conditional_logic' => 0,
								'wrapper' => array (
									'width' => '30',
									'class' => '',
									'id' => '',
								),
								'choices' => array (
									'left' => 'left',
									'center' => 'center',
									'right' => 'right',
								),
								'default_value' => array (
									0 => 'center',
								),
								'allow_null' => 1,
								'multiple' => 0,
								'ui' => 0,
								'ajax' => 0,
								'return_format' => 'value',
								'placeholder' => '',
							),

							array (
								'key' => 'field_wethbtunjiukmuiknjbetvgq',
								'label' => __('Column Vertical Alignment', 'azontables'),
								'name' => 'column_vert_align',
								'type' => 'select',
								'instructions' => '',
								'required' => 0,
								'conditional_logic' => 0,
								'wrapper' => array (
									'width' => '30',
									'class' => '',
									'id' => '',
								),
								'choices' => array (
									'top' => 'top',
									'middle' => 'middle',
									'bottom' => 'bottom',
								),
								'default_value' => array (
									0 => 'middle',
								),
								'allow_null' => 1,
								'multiple' => 0,
								'ui' => 0,
								'ajax' => 0,
								'return_format' => 'value',
								'placeholder' => '',
							),

						),
					),



					array (
						'key' => 'field_oiuvctrtvg7hy8bhyiubu',
						'label' => __('Step 2 - Add your Products', 'azontables' ),
						'name' => '',
						'type' => 'message',
						'instructions' => __('You must have your ', 'azontables') . '<a href="' . get_site_url() .'/wp-admin/admin.php?page=azontables-settings" target="_blank">' . __('Amazon API Settings', 'azontables') . '</a> ' .  __('saved to add products.', 'azontables'),
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '',
							'class' => 'azontables-create-heading',
							'id' => '',
						),
						'message' => __('Click the Add Product button, then search Amazon by keyword or ASIN. Browse the products returned by the API, and click "Choose this Item". That will automatically populate the field with the ASIN for that product. You can also manually ass the product ASIN if you know it.', 'azontables'),
						'new_lines' => 'wpautop',
						'esc_html' => 0,
					),


					array (
						'key' => 'field_7b6vt7r6c56cvtbyibi',
						'label' => __('Add Products from Amazon', 'azontables' ),
						'name' => 'azontables_choose_products',
						'type' => 'repeater',
						'instructions' => '<span id="generate_prod_previews">Generate Product Previews</span>',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'collapsed' => '',
						'min' => '',
						'max' => '',
						'layout' => 'row',
						'button_label' => __('Add Product', 'azontables' ),
						'sub_fields' => array (
							array (
								'key' => 'field_i76b67rc5rcv7t8bu',
								'label' => __('Chosen Amazon ASIN', 'azontables' ),
								'name' => 'ama_asin',
								'type' => 'text',
								'instructions' => '<a class="manual-search-amazon thickbox" href="#TB_inline?width=631&height=501&inlineId=azontables-amazon-lookup"><strong>'.__('Search Amazon', 'azontables' ).'</strong></a>'.'<div class="ama_asin_display"><img class="asin_loading" src="/wp-admin/images/loading.gif" alt="loading" /></div>',
								'required' => 0,
								'conditional_logic' => 0,
								'wrapper' => array (
									'width' => '',
									'class' => '',
									'id' => '',
								),
								'default_value' => '',
								'placeholder' => '',
								'prepend' => '',
								'append' => '',
								'maxlength' => '',
							),
						),
					),


					array (
						'key' => 'field_67r565rc67tv7yb',
						'label' => __('Step 3 - Embed your Table', 'azontables' ),
						'name' => '',
						'type' => 'message',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '',
							'class' => 'azontables-create-heading',
							'id' => '',
						),
						'message' => '<p>'.__('To embed your table in a page or post, publish this table, then you can use the 
							"Insert Azon Table" button in any WordPress editor, or copy and paste the shortcode found on the "All Azon Tables" admin page.', 'azontables').'</p><br /><br /><div class="acf-label"><label>'.__('Pro Tips', 'azontables').'</div><p>'.__('We also recommend using the Yoast SEO plugin, and setting the Azon Tables post type archive and single pages to "noindex. This is optional, but if you do not do this, all of your Amazon affiliate link heavy tables may get indexed in Google.', 'azontables').'<p>'.__('Make sure to experiment with the settings and view your table on multiple screen sizes.', 'azontables'),
						'new_lines' => 'wpautop',
						'esc_html' => 0,
					),


				),
				'location' => array (
					array (
						array (
							'param' => 'post_type',
							'operator' => '==',
							'value' => 'azontable',
						),
					),
				),
				'menu_order' => 0,
				'position' => 'acf_after_title',
				'style' => 'default',
				'label_placement' => 'top',
				'instruction_placement' => 'label',
				'hide_on_screen' => '',
				'active' => 1,
				'description' => '',
			));



























			$azontables_amazon_api = new azontables_amazon_api();
			$global_table_settings = $azontables_amazon_api->get_azontables_table_global_settings();


			// $global_table_settings['enable_table_memory']
			// 	$global_table_settings['hide_header']
			// 	$global_table_settings['enable_table_sorting']
			// 	$global_table_settings['enable_table_filtering']['enabled']
			// 	$global_table_settings['enable_table_filtering']['no_results_message']
			// 	$global_table_settings['enable_table_filtering']['filter_placeholder_text']
			// 	$global_table_settings['enable_table_filtering']['filter_dropdown_heading']
			// 	$global_table_settings['enable_table_filtering']['filter_position']
			// $global_table_settings['enable_table_pagination']['enabled']
			// $global_table_settings['enable_table_pagination']['number_of_rows_per_page']
			// $global_table_settings['enable_table_pagination']['pages_displayed_limit']
			// $global_table_settings['enable_table_pagination']['pagination_position']
			// $global_table_settings['breakpoints_off_container']
			// $global_table_settings['custom_breakpoints']['enabled']
			// $global_table_settings['custom_breakpoints']['points']['xs']
			// $global_table_settings['custom_breakpoints']['points']['sm']
			// $global_table_settings['custom_breakpoints']['points']['md']
			// $global_table_settings['custom_breakpoints']['points']['lg']
			// $global_table_settings['custom_breakpoints']['points']['xl']
			// $global_table_settings['expand_the_first_row_by_default']
			// $global_table_settings['expand_all_rows_by_default']
			// $global_table_settings['hide_the_toggle_button']



			// Add Override Table Settings Fields

			acf_add_local_field_group(array (
				'key' => 'group_7564c3x56c7f8g9h',
				'title' => __('Advanced Table Settings', 'azontables'),
				'fields' => array (
					array (
						'key' => 'field_583f359fc3be7666',
						'label' => __('Overide Global Table Settings', 'azontables'),
						'name' => 'overide_global_table_settings',
						'type' => 'true_false',
						'instructions' => __('When creating a brand new post, this starts prepopulated with your global settings. Adjust it from there.', 'azontables'),
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'message' => __('Check this box to override the global table settings', 'azontables'),
						'default_value' => 0,
					),
					array (
						'key' => 'field_765c567567c654576',
						'label' => __('Table Heading', 'azontables'),
						'name' => '',
						'type' => 'message',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_583f359fc3be7666',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '',
							'class' => 'azontables-create-heading',
							'id' => '',
						),
						'message' => '',
						'new_lines' => 'wpautop',
						'esc_html' => 0,
					),
					array (
						'key' => 'field_4v5t345eth5ethgevt',
						'label' => __('Hide the Table Header', 'azontables'),
						'name' => 'hide_the_table_header',
						'type' => 'true_false',
						'instructions' => __('Sortable rows are not possible if this is checked.', 'azontables'),
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_583f359fc3be7666',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'message' => __('Check this box to hide the table header row.', 'azontables'),
						'default_value' => $global_table_settings['hide_header'],
					),
					array (
						'key' => 'field_rtgvertgvbrthgbwrtgwr',
						'label' => __('Table Sorting', 'azontables'),
						'name' => '',
						'type' => 'message',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_583f359fc3be7666',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '',
							'class' => 'azontables-create-heading',
							'id' => '',
						),
						'message' => '',
						'new_lines' => 'wpautop',
						'esc_html' => 0,
					),
					array (
						'key' => 'field_etyb56yb356y35by6356',
						'label' => __('Enable Table Sorting', 'azontables'),
						'name' => 'enable_table_sorting',
						'type' => 'true_false',
						'instructions' => __('(Table sorting will not work if you have the "Hide the Table Header" checkbox checked.)', 'azontables'),
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_583f359fc3be7666',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'message' => __('Check this box to make this table sortable', 'azontables'),
						'default_value' => $global_table_settings['enable_table_sorting'],
					),
					array (
						'key' => 'field_583f3510c854a',
						'label' => __('Table Filtering', 'azontables'),
						'name' => '',
						'type' => 'message',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_583f359fc3be7666',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '',
							'class' => 'azontables-create-heading',
							'id' => '',
						),
						'message' => '',
						'new_lines' => 'wpautop',
						'esc_html' => 0,
					),
					array (
						'key' => 'field_583df7147590a555',
						'label' => __('Enable Table Filtering', 'azontables'),
						'name' => 'enable_table_filtering',
						'type' => 'true_false',
						'instructions' => __('Enabling this will add a search box on top of the table that will filter the table rows by the value entered.', 'azontables'),
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_583f359fc3be7666',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '',
							'class' => 'djn-clear-all',
							'id' => '',
						),
						'message' => __('Check this box to enable table filtering', 'azontables'),
						'default_value' => $global_table_settings['enable_table_filtering']['enabled'],
					),
					array (
						'key' => 'field_rgt4w5y3ty4t4tgevgwg',
						'label' => __('No Results Message', 'azontables'),
						'name' => 'no_results_message',
						'type' => 'text',
						'instructions' => __('Enter the message you would like to display if the filter returns 0 results.', 'azontables'),
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_583df7147590a555',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_583f359fc3be7666',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '50',
							'class' => '',
							'id' => '',
						),
						'default_value' => $global_table_settings['enable_table_filtering']['no_results_message'],
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'maxlength' => '',
					),
					array (
						'key' => 'field_esrtvgwbrthbertgv2ergrhtf',
						'label' => __('Filter Placeholder Text', 'azontables'),
						'name' => 'filter_placeholder_text',
						'type' => 'text',
						'instructions' => __('Enter the text that will display in the empty filter box.', 'azontables'),
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_583df7147590a555',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_583f359fc3be7666',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '50',
							'class' => '',
							'id' => '',
						),
						'default_value' => $global_table_settings['enable_table_filtering']['filter_placeholder_text'],
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'maxlength' => '',
					),
					array (
						'key' => 'field_98h7g6f54c6v7b8nu7bvt',
						'label' => __('Filter Dropdown Heading', 'azontables'),
						'name' => 'filter_dropdown_heading',
						'type' => 'text',
						'instructions' => __('Enter a Heading for the filter dropdown for which columns are searchable. Leave this blank for no heading.', 'azontables'),
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_583df7147590a555',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_583f359fc3be7666',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '50',
							'class' => 'djn-clear-all',
							'id' => '',
						),
						'default_value' => $global_table_settings['enable_table_filtering']['filter_dropdown_heading'],
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'maxlength' => '',
					),
					array (
						'key' => 'field_8b7657c4ertyvubi',
						'label' => __('Filter Position', 'azontables'),
						'name' => 'filter_position',
						'type' => 'select',
						'instructions' => __('The filter displays to the right by default. It can be placed to the left, center, or right.', 'azontables'),
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_583df7147590a555',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_583f359fc3be7666',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '50',
							'class' => '',
							'id' => '',
						),
						'choices' => array (
							'left' => __('Left', 'azontables'),
							'center' => __('Center', 'azontables'),
							'right' => __('Right', 'azontables'),
						),
						'default_value' => array (
							0 => $global_table_settings['enable_table_filtering']['filter_position'],
						),
						'allow_null' => 0,
						'multiple' => 0,
						'ui' => 0,
						'ajax' => 0,
						'return_format' => 'value',
						'placeholder' => '',
					),
					array (
						'key' => 'field_n8b76v57b86v5c76f5',
						'label' => __('Table Pagination', 'azontables'),
						'name' => '',
						'type' => 'message',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_583f359fc3be7666',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '',
							'class' => 'azontables-create-heading',
							'id' => '',
						),
						'message' => '',
						'new_lines' => 'wpautop',
						'esc_html' => 0,
					),
					array (
						'key' => 'field_583dfa558afbd444',
						'label' => __('Enable Table Pagination', 'azontables'),
						'name' => 'enable_table_pagination',
						'type' => 'true_false',
						'instructions' => __('This should be used for tables with a large amount of rows.', 'azontables'),
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_583f359fc3be7666',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '',
							'class' => 'djn-clear-all',
							'id' => '',
						),
						'message' => __('Check this box to enable table pagination', 'azontables'),
						'default_value' => $global_table_settings['enable_table_pagination']['enabled'],
					),
					array (
						'key' => 'field_765ecrv6t7brv767r6vce56',
						'label' => __('Number of Rows Per Page', 'azontables'),
						'name' => 'number_of_rows_per_page',
						'type' => 'number',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_583dfa558afbd444',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_583f359fc3be7666',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'default_value' => $global_table_settings['enable_table_pagination']['number_of_rows_per_page'],
						'placeholder' => '',
						'prepend' => '',
						'append' => 'Rows',
						'min' => '',
						'max' => '',
						'step' => '',
					),
					array (
						'key' => 'field_u6rcy4ex6r5c7',
						'label' => __('Pagination Position', 'azontables'),
						'name' => 'pagination_position',
						'type' => 'select',
						'instructions' => __('The pagination displays to the center by default. It can be placed to the left, center, or right.', 'azontables'),
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_583dfa558afbd444',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_583f359fc3be7666',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '50',
							'class' => '',
							'id' => '',
						),
						'choices' => array (
							'left' => __('Left', 'azontables'),
							'center' => __('Center', 'azontables'),
							'right' => __('Right', 'azontables'),
						),
						'default_value' => array (
							0 => $global_table_settings['enable_table_pagination']['pagination_position'],
						),
						'allow_null' => 0,
						'multiple' => 0,
						'ui' => 0,
						'ajax' => 0,
						'return_format' => 'value',
						'placeholder' => '',
					),
					array (
						'key' => 'field_iyt5rx5ecy6tvi',
						'label' => __('Table Pagination - Pages Displayed Limit', 'azontables'),
						'name' => 'pages_displayed_limit',
						'type' => 'number',
						'instructions' => __('This setting does not limit the number of pages in your table, it only limits the number of pages that are displayed in the pagination at the same time. Users can still access every page of your full table. This is useful for tables a large amount of rows.', 'azontables'),
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_583f359fc3be7666',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_583dfa558afbd444',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '50',
							'class' => '',
							'id' => '',
						),
						'default_value' => $global_table_settings['enable_table_pagination']['pages_displayed_limit'],
						'placeholder' => '',
						'prepend' => '',
						'append' => 'Pages',
						'min' => '',
						'max' => '',
						'step' => '',
					),
					array (
						'key' => 'field_u6rc756r6v87tbybuyg',
						'label' => __('Table Memory', 'azontables'),
						'name' => '',
						'type' => 'message',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_583f359fc3be7666',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '',
							'class' => 'azontables-create-heading',
							'id' => '',
						),
						'message' => '',
						'new_lines' => 'wpautop',
						'esc_html' => 0,
					),
					array (
						'key' => 'field_u6tv65rcv87tby987bi',
						'label' => __('Enable Table Memory', 'azontables'),
						'name' => 'enable_table_memory',
						'type' => 'true_false',
						'instructions' => __('Enabling this option will store the state of your table in the visitor\'s browser local storage, so the next time they visit the page the table will remember the sorting, filtering, and pagination that the visitor left it as.', 'azontables'),
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_583f359fc3be7666',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'message' => __('Check this box to enable table memory for your visitors.', 'azontables'),
						'default_value' => $global_table_settings['enable_table_memory'],
					),
					array (
						'key' => 'field_56t76rvf76rftgd',
						'label' => __('Table Custom Responsive Settings', 'azontables'),
						'name' => '',
						'type' => 'message',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_583f359fc3be7666',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '',
							'class' => 'azontables-create-heading',
							'id' => '',
						),
						'message' => '',
						'new_lines' => 'wpautop',
						'esc_html' => 0,
					),
					array (
						'key' => 'field_iytrcv6t7ggt6r5',
						'label' => __('Base Responsive Breakpoints off the Width of the Container', 'azontables'),
						'name' => 'breakpoints_off_container',
						'type' => 'true_false',
						'instructions' => __('By default the breakpoints are based off the width of the window. Check this box to base the breakpoints off the width of the container the table resides in.', 'azontables'),
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_583f359fc3be7666',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'message' => __('Check this box to base breakpoints off the parent container width.', 'azontables'),
						'default_value' => $global_table_settings['breakpoints_off_container'],
					),
					array (
						'key' => 'field_583df083fe439333',
						'label' => __('Custom Breakpoints', 'azontables'),
						'name' => 'custom_breakpoints',
						'type' => 'true_false',
						'instructions' => __('This setting is if you want to use custom breakpoints for your responsive tables. If you are not familiar with responsive breakpoints, it is best to leave this unchecked.', 'azontables'),
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_583f359fc3be7666',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'message' => __('Check this box to use custom breakpoints for this table.', 'azontables'),
						'default_value' => $global_table_settings['custom_breakpoints']['enabled'],
					),
					array (
						'key' => 'field_76vr7tb8yh78v6r5cv6tb',
						'label' => __('Extra Small Breakpoint', 'azontables'),
						'name' => 'extra_small_breakpoint',
						'type' => 'number',
						'instructions' => __('This breakpoint is for extra small screens.', 'azontables'),
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_583df083fe439333',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_583f359fc3be7666',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '25',
							'class' => '',
							'id' => '',
						),
						'default_value' => $global_table_settings['custom_breakpoints']['points']['xs'],
						'placeholder' => '',
						'prepend' => '',
						'append' => 'px',
						'min' => '',
						'max' => '',
						'step' => '',
					),
					array (
						'key' => 'field_876546v7b8y',
						'label' => __('Small Breakpoint', 'azontables'),
						'name' => 'small_breakpoint',
						'type' => 'number',
						'instructions' => __('This breakpoint is for small screens.', 'azontables'),
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_583df083fe439333',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_583f359fc3be7666',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '25',
							'class' => '',
							'id' => '',
						),
						'default_value' => $global_table_settings['custom_breakpoints']['points']['sm'],
						'placeholder' => '',
						'prepend' => '',
						'append' => 'px',
						'min' => '',
						'max' => '',
						'step' => '',
					),
					array (
						'key' => 'field_n87b6v5cv6b7n',
						'label' => __('Medium Breakpoint', 'azontables'),
						'name' => 'medium_breakpoint',
						'type' => 'number',
						'instructions' => __('This breakpoint is for medium screens.', 'azontables'),
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_583df083fe439333',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_583f359fc3be7666',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '25',
							'class' => '',
							'id' => '',
						),
						'default_value' => $global_table_settings['custom_breakpoints']['points']['md'],
						'placeholder' => '',
						'prepend' => '',
						'append' => 'px',
						'min' => '',
						'max' => '',
						'step' => '',
					),
					array (
						'key' => 'field_inub76v5c4v6t7by8',
						'label' => __('Large Breakpoint', 'azontables'),
						'name' => 'large_breakpoint',
						'type' => 'number',
						'instructions' => __('This breakpoint is for large screens.', 'azontables'),
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_583df083fe439333',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_583f359fc3be7666',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '25',
							'class' => '',
							'id' => '',
						),
						'default_value' => $global_table_settings['custom_breakpoints']['points']['lg'],
						'placeholder' => '',
						'prepend' => '',
						'append' => 'px',
						'min' => '',
						'max' => '',
						'step' => '',
					),
					array (
						'key' => 'field_oinubytvcrv6t7by8nu',
						'label' => __('Expand the First Row by Default', 'azontables'),
						'name' => 'expand_the_first_row_by_default',
						'type' => 'true_false',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_583f359fc3be7666',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '50',
							'class' => 'djn-clear-all',
							'id' => '',
						),
						'message' => __('Check this box to expand the first row by default', 'azontables'),
						'default_value' => $global_table_settings['expand_the_first_row_by_default'],
					),
					array (
						'key' => 'field_n98b76v5cerv6tb7y',
						'label' => __('Expand All Rows by Default', 'azontables'),
						'name' => 'expand_all_rows_by_default',
						'type' => 'true_false',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_583f359fc3be7666',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '50',
							'class' => '',
							'id' => '',
						),
						'message' => __('Check this box to expand all rows by default', 'azontables'),
						'default_value' => $global_table_settings['expand_all_rows_by_default'],
					),
					array (
						'key' => 'field_n8yb7t6vr5cv6tb7yn8u',
						'label' => __('Hide the Toggle Button', 'azontables'),
						'name' => 'hide_the_toggle_button',
						'type' => 'true_false',
						'instructions' => __('Check this box if you wish to hide the row toggle button. This will not prevent the rows form being opened and closed if clicked.', 'azontables'),
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_583f359fc3be7666',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'message' => __('Check this box to hide the toggle button on each row.', 'azontables'),
						'default_value' => $global_table_settings['hide_the_toggle_button'],
					),
				),
				'location' => array (
					array (
						array (
							'param' => 'post_type',
							'operator' => '==',
							'value' => 'azontable',
						),
					),
				),
				'menu_order' => 50,
				'position' => 'acf_after_title',
				'style' => 'default',
				'label_placement' => 'top',
				'instruction_placement' => 'label',
				'hide_on_screen' => '',
				'active' => 1,
				'description' => '',
			));


		

	}


}
