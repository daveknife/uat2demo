(function( $ ) {
	'use strict';

	/**
	 * All of the code for your admin-facing JavaScript source
	 * should reside in this file.
	 *
	 * Note: It has been assumed you will write jQuery code here, so the
	 * $ function reference has been prepared for usage within the scope
	 * of this function.
	 *
	 * This enables you to define handlers, for when the DOM is ready:
	 *
	 * $(function() {
	 *
	 * });
	 *
	 * When the window is loaded:
	 *
	 * $( window ).load(function() {
	 *
	 * });
	 *
	 * ...and/or other possibilities.
	 *
	 * Ideally, it is not considered best practise to attach more than a
	 * single DOM-ready or window-load handler for a particular page.
	 * Although scripts in the WordPress core, Plugins and Themes may be
	 * practising this, we should strive to set a better example in our own work.
	 */


	$(document).ready( function(){ 




		// submit amazon api search on review admin pedit screen
		$('body').on( 'click', '#azontables-api-search-submit', function(){

			$('.azontables-amazon-loading-overlay').fadeIn(100);

			var par = $(this).closest('#azontables-api-lookup');
			var term = par.find('#azontables-api-search-term').val();
			var locale = par.find('#azontables-search-locale').val();
			var searchspecs = par.find('#azontables-search-specs').val();

			//alert( locale );

			var data = {
	            action: 'azontables_find_amazon_products_ajax',
	            term: term,
	            locale: locale,
	            searchspecs: searchspecs
	        };

	        amazon_api_ajax_load_results(data);

			//console.log(term+' - '+locale);
		});

		// make our ajax call to do the amazon api call in php
		function amazon_api_ajax_load_results(data) {

			$.ajax({
	            type: 'POST',
	            url: ajaxurl,
	            data: data,
	            success: function(response) {
	                //console.log(response);
	                $('html, body').animate({
				        scrollTop: $("#azontables-amazon-api-results").offset().top - 50
				    }, 600);
	                $("#azontables-amazon-api-results").html(response);

	            },
	            complete: function(response){
	            	//console.log(response);

	            	$('.azontables-amazon-loading-overlay').fadeOut(100);
	            }
	        });

		}


		// make our ajax call to insert the chosen amazon product into the "chosen" box and save the info in the database for easy viewing in the admin.
		$('body').on( 'click', '.amazon-search-page:not(.refine)', function(){

			$('.azontables-amazon-loading-overlay').fadeIn(100);

			var page = $(this).attr('data-api-page');
			var term = $(this).attr('data-api-term');
			var locale = $(this).attr('data-api-locale');
			var searchspecs = $(this).attr('data-api-specstype');

			var data = {
	            action: 'azontables_load_amazon_products_newpage_ajax',
	            term: term,
	            locale: locale,
	            page: page,
	            searchspecs: searchspecs
	        };

	    	amazon_api_ajax_load_new_page(data);

			//console.log(term+' - '+locale);
		});

		// make our ajax call to do the amazon api call in php
		function amazon_api_ajax_load_new_page(data) {

			$.ajax({
	            type: 'POST',
	            url: ajaxurl,
	            data: data,
	            success: function(response) {
	                $("#azontables-amazon-api-results-inner").html(response);
	            },
	            complete: function(response){

				    $('.amazon-search-page').removeClass('current');
				    $('.amazon-search-page[data-api-page="'+data.page+'"]').addClass('current');

				    $('.azontables-amazon-loading-overlay').fadeOut(100);
	            }
	        });

		}




		// set our sliders repeater row empty. we'll update it to grab later
		var repeater_row_id = '';
		
		// open the "custom specs" pull form amazon search box
		$('body').on( 'click', '.manual-search-amazon', function(){

			if ( $('body').hasClass('post-type-azontable') ) {
				repeater_row_id = $(this).closest('.acf-field').find('input').attr('id');
				//console.log('::: '+repeater_row_id);
			}

		});




		// insert our chosen amazon product into the "chosen box, and save the choice in the page meta data in the ajax php function
		$('body').on( 'click', '.insert-amazon-product', function(){

			$('.azontables-amazon-loading-overlay').fadeIn(100);

			var asin = $(this).attr('data-prod-asin');
			if ( asin === '' ) { asin = 'No Data'; }

			if ( $('body').hasClass('post-type-azontable') ) {
	        	$('#'+repeater_row_id).attr('value', asin );
	        	$('.azontables-amazon-loading-overlay').fadeOut(100);
				$('.tb-close-icon').click();
	        }

		});




		// license activate button trigger
		jQuery('body').on('click', '.azontables-activate', function() {
			// identify current license box
			var current_license_box = jQuery(this).closest('.gf-option-box');
			// get ID of current license box
	        var current_license_box_ID = current_license_box.attr('id');
	        // get the license value in the license field
			var license = current_license_box.find('.azontables-license').attr('value');
	        // get the current product name
	        var azontables_product = current_license_box.find('.azontables-product').attr('value');

	        jQuery('.license-loading-gif').show();
	        // set our ajax call data
			var data = {
	            action: 'activate_azontables_license',
	            license_box_ID: current_license_box_ID,
	            azontables_product: azontables_product,
	            license: license
	        };


	        if ( license != '' ) {
	        	// show the loading spinner and add a class to identify it later to remove on completion
	        	current_license_box.find('.license-loading-gif').addClass('spinner-on').show();
	        	// call our deactivate ajax function and pass the data to it
	        	activate_azontables_license(data);
	        }
	        else {
	        	alert('Your license field does not contain a valid license. Please try again.');
	        }


		});

		function activate_azontables_license(data) {

			jQuery.ajax({
	            type: 'POST',
	            url: ajaxurl,
	            data: data,
	            dataType: 'json',
	            complete: function(response){
	                jQuery('.spinner-on').hide();
	                console.log(response);
	            },
	            success: function(response) {
	            	if ( response.result == 'valid' ) {
	                    var current_license_box = '#'+response.license_box_ID;
	                    jQuery(current_license_box).find('.license-message').html('Your license is active.').addClass('azontables-license-valid').removeClass('azontables-license-deactivated');
	                    jQuery(current_license_box).find('.azontables-activate').removeClass('azontables-activate').addClass('azontables-deactivate').attr('value', 'Dectivate');
	                    jQuery('.license-loading-gif').hide();
	                }
	                else if ( response.result == 'invalid' ) {
	                    alert('This license is invalid. Please try again.');
	                }
	            }
	        });
		}


	    // license deactivate button trigger
	    jQuery('body').on('click', '.azontables-deactivate', function() {
	        // identify current license box
	        var current_license_box = jQuery(this).closest('.gf-option-box');
	        // get ID of current license box
	        var current_license_box_ID = current_license_box.attr('id');
	        // get the license value in the license field
	        var license = current_license_box.find('.azontables-license').attr('value');
	        // get the current product name
	        var azontables_product = current_license_box.find('.azontables-product').attr('value');
	        // create our data for our ajax call
	        var data = {
	            action: 'deactivate_azontables_license',
	            license_box_ID: current_license_box_ID,
	            azontables_product: azontables_product,
	            license: license
	        };
	        // show the loading spinner and add a class to identify it later to remove on completion
	        current_license_box.find('.license-loading-gif').addClass('spinner-on').show();
	        // call our deactivate ajax function and pass the data to it
	        deactivate_azontables_license(data);
	    });

	    function deactivate_azontables_license(data) {
	        jQuery.ajax({
	            type: 'POST',
	            url: ajaxurl,
	            data: data,
	            dataType: 'json',
	            complete: function(response){
	                jQuery('.spinner-on').hide();
	            },
	            success: function(response){
	                console.log(data);
	                if ( response.result == 'deactivated' ) {
	                    var current_license_box = '#'+response.license_box_ID;
	                    jQuery(current_license_box).find('.license-message').html('The license key you entered has been deactivated on this website.').addClass('azontables-license-deactivated').removeClass('azontables-license-valid');
	                    jQuery(current_license_box).find('.azontables-deactivate').removeClass('azontables-deactivate').addClass('azontables-activate').attr('value', 'Activate');

	                } 
	            }
	        });
	    }




	    if ( jQuery('.post-type-azontable').length && jQuery('.ama_asin_display').length ) {



	    	jQuery('body').on( 'click', '#generate_prod_previews:not(.loaded)', function(){
	    		jQuery('.asin_loading').show();
	    		get_asins_previews();
	    	});

	    	function get_asins_previews() {

		    	var ama_asin_counter = 0;
		    	var ama_asins = new Array();

		    	jQuery('.ama_asin_display').each(function() {

		    		ama_asins[ama_asin_counter] = jQuery(this).closest('.acf-field').find('.acf-input').find('input[type=text]').val();
		    		
		    		ama_asin_counter++;

				});

				var ama_asins_json = JSON.stringify(ama_asins);

	    		//console.log(ama_asin_counter);

	    		var data = {
		            action: 'get_asins_info_for_admin',
		            ama_asins_json: ama_asins_json,
		        };

		        jQuery.ajax({
		            type: 'POST',
		            url: ajaxurl,
		            data: data,
		            dataType: 'json',
		            complete: function(response){
		                
		                var asin_info =  JSON.parse( response.responseText );

		                jQuery('.ama_asin_display').each(function() {

				    		var ama_asin = jQuery(this).closest('.acf-field').find('.acf-input').find('input[type=text]').val();

				    		if ( asin_info[ama_asin] ) {

				    			var title = asin_info[ama_asin].ItemAttributes.Title;
					    		var link = asin_info[ama_asin].DetailPageURL;
					    		var image = asin_info[ama_asin].SmallImage.URL;

					    		jQuery(this).append('<div class="ama-preview-img-wrap"><img src="'+image+'" alt="" /></div>');
					    		jQuery(this).append('<h5>'+title+'<br /><a href="'+link+'" target="_blank">view on Amazon</a></h5>');
					    		jQuery(this).closest('.acf-field').find('.asin_loading').remove();

					    		console.log( 'asin_info: ' );
					    		console.log( asin_info );
				    		}


						});

		            },
		            success: function(response){
		                console.log('success');

		                jQuery('#generate_prod_previews').addClass('loaded');

		            }
		        });


		    }



	    }



		

		

	});


})( jQuery );
