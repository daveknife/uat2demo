<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    azontables_Pluginname
 * @subpackage azontables_Pluginname/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    azontables_Pluginname
 * @subpackage azontables_Pluginname/public
 * @author     Your Name <email@example.com>
 */
class azontables_Public {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $azontables    The ID of this plugin.
	 */
	private $azontables;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $azontables       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $azontables, $version ) {

		$this->azontables = $azontables;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in azontables_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The azontables_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->azontables, plugin_dir_url( __FILE__ ) . 'css/style.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in azontables_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The azontables_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		//wp_enqueue_script( 'foundation-tabs', get_template_directory_uri() . '/bower_components/foundation-sites/js/foundation.tabs.js', array( 'jquery' ), $this->version, false );

		wp_enqueue_script( $this->azontables, plugin_dir_url( __FILE__ ) . 'js/azontables-public-min.js', array( 'jquery' ), $this->version, true );

		

	}




	public function add_azon_table_to_single($content) {

		if( is_singular('azontable') ) {

			 $content = $content . '[azontables id="'.get_the_ID().'" /]';

		}
		return $content;
	}






	// [uat2_table id="id-value"]
	public function azontables_layout_table( $atts ) {

	    // set our table ID variable form the id attribute of the shortcode
	    $table_id = $atts['id'];

	    // set our empty variable to return our table with at the end
	    $table_html = '';

	    // create a new instance of our tables class if it exists
		$azontables_tables_functions = new azontables_amazon_api(); 
			
		// get our Table Attributes
		$table_attributes = $azontables_tables_functions->get_azontables_table_attributes( $table_id );


		$final_table = get_post_meta( $table_id, 'azontables_table_array', true );


			if ( $final_table ) {

				$table_html .= '<div class="azontables-table-wrapper">';
				$table_html .= '<table class="azontables-table"'.$table_attributes.'>';


		        if ( $final_table['header'] ) {

		            $table_html .= '<thead>';
		                $table_html .= '<tr>';

		                $tbl=0;

		                    foreach ( $final_table['header'] as $th ) {

		                		$breakpoints = '';
		                		$breakpoints_string = '';

		                		$col_hidden_on = $th['col_hidden_on'];
		                		$col_hidden_on = $th['col_hidden_on'];

		                		if ( $col_hidden_on ) {
		                			foreach ( $col_hidden_on as $mq ) {
		                				$breakpoints_string .= $mq.' ';
		                			}
		                		}

		                		//USE data-filterable="false" for columns not suited for filtering like mages

		                		$breakpoints = ' data-breakpoints="'.$breakpoints_string.'"';
		                		$coltype = $th['col_type'];


		                		if ( $th['col_disable_sort'] == 1 ) {
		                			$disable_sort = ' data-sortable="false"';
		                		}
		                		else {
		                			$disable_sort = '';
		                		}
		             

		                		// amazon spec types
		                		if ( $coltype == 'features'  ) { 
		                			$coltype_string = ' data-value-type="'.$coltype.'" data-type="html"';
		                		}
		                		else if ( $coltype == 'asin' || $coltype == 'brand' || $coltype == 'upc' || $coltype == 'model' || $coltype == 'warranty' || $coltype == 'wp-title' || $coltype == 'amazon-title'  ) { 
		                			$coltype_string = ' data-value-type="'.$coltype.'" data-type="text"';
		                		}
		                		else if ( $coltype == 'price' || $coltype == 'lowest-new-price' || $coltype == 'lowest-used-price'  ) { 
		                			$coltype_string = ' data-value-type="'.$coltype.'" data-type="html"';
		                		}
		                		else if ( $coltype == 'editor-rating' || $coltype == 'small-image' || $coltype == 'medium-image' || $coltype == 'large-image'  ) { 
		                			$coltype_string = ' data-value-type="'.$coltype.'" data-type="html"';
		                		}

		                		elseif ( $coltype == 'button-img-1' || $coltype == 'button-img-2' || $coltype == 'button-img-3' || $coltype == 'button-img-4' || $coltype == 'button-img-5' ) {
									$coltype_string = ' data-value-type="'.$coltype.'" data-type="html"';
									$disable_sort = ' data-sortable="false"';
								}

								elseif ( $coltype == 'button-plain' ) {
									$coltype_string = ' data-value-type="'.$coltype.'" data-type="html"';
									$disable_sort = ' data-sortable="false"';
								}

								elseif ( $coltype == 'text-link' ) {
									$coltype_string = ' data-value-type="'.$coltype.'" data-type="html"';
									$disable_sort = ' data-sortable="false"';
								}


		                		

		                		// set a data type to html if an affiliate link is enabled for this column
		                		//echo $th['col_aff_link'];

		      //           		if ( $col_aff_link == 'field-link-affiliate' ) {
								// 	$table_html .= '<th'.$breakpoints.$coltype_string.$disable_sort.'>';
								// }
								// elseif ( $col_aff_link == 'field-link-review' ) {
								// 	$table_html .= '<th'.$breakpoints.$coltype_string.$disable_sort.'>';
								// }

		                		$col_link = $th['col_aff_link'];

		                		if ( $col_link == 'field-link-affiliate'  ) {
		                			$coltype_string = ' data-value-type="'.$coltype.'" data-type="html"';
		                		}

		                		$table_html .= '<th'.$breakpoints.$coltype_string.$disable_sort.'>';
		                    
		                        	$table_html .= $th['col_name'];

		                   		$table_html .= '</th>';

		                        $tbl++;
		                    }

		                $table_html .= '</tr>';

		            $table_html .= '</thead>';

		        } // end if ( $final_table['header'] ) 






// return $table_html;
// die();
			

				// finally let's lay out our table!!!
				if ( $final_table['body'] ) {

					// Let's build an array of the product asins to loop through below to make amazon api calls for ten products at a time
					$final_item_data = $azontables_tables_functions->azontables_table_get_asin_groups_data( $final_table, $table_id );

				} //  end if ( $final_table )



				if ( $final_table['body'] ) {

			        $table_html .= '<tbody>';

			        // MAKE SURE TO USE CLEAN FILTER VALUES data-filter-value="myFilterValue"

		        	$tr_counter=1;
		            foreach ( $final_table['body'] as $tr ) {

		            	if ( $tr_counter % 2 == 0 ) { $td_eo = 'even'; } 
		                else { $td_eo = 'odd'; }

		                $table_html .= '<tr data-table-row="Row '.$tr_counter.'" data-table-row-evenodd="'.$td_eo.'">';

		                	$td_counter=1;

		                    foreach ( $tr as $td ) {


		                    	// reset our td value for each iteration
		                    	$td_val = '';

		                    	$prod_id = $td['prod_id'];
		                    	$col_type = $td['col_type'];
		                    	$col_aff_link = $td['col_aff_link'];

								$azontables_ama_asin = $prod_id;

								// create an empty string for our table cell value
								$td_val = '';

								// set an empty data sort value by default
								$data_sort_value = '';

								


								if ( $col_type == 'title' ) {

									$api_val = $final_item_data[$azontables_ama_asin]->ItemAttributes->Title;
									if ( $api_val != '' ) {
										$td_val .= $api_val;
									}
								}


								elseif ( $col_type == 'features'  ) {

									$prod_features_array = $final_item_data[$azontables_ama_asin]->ItemAttributes->Feature;

									if ( is_object( $prod_features_array ) ) {
										$td_val .= '<ul class="chosen-api-product-features">';
							    		foreach ( $prod_features_array as $feature ) {
							    			$td_val .= '<li>'.$feature.'</li>';
							    		}
								    	$td_val .= '</ul>';
									}

								}

								

								elseif ( $col_type == 'asin' ) {
									$api_val = $final_item_data[$azontables_ama_asin]->ASIN;
									if ( $api_val != '' ) {
										$td_val .= $api_val;
									}
								}

								elseif ( $col_type == 'brand' ) {
									$api_val = $final_item_data[$azontables_ama_asin]->ItemAttributes->Brand;
									if ( $api_val != '' ) {
										$td_val .= $api_val;
									}
								}

								elseif ( $col_type == 'upc' ) {
									$api_val = $final_item_data[$azontables_ama_asin]->ItemAttributes->UPC;
									if ( $api_val != '' ) {
										$td_val .= $api_val;
									}
								}

								elseif ( $col_type == 'warranty' ) {
									$api_val = $final_item_data[$azontables_ama_asin]->ItemAttributes->Warranty;
									if ( $api_val != '' ) {
										$td_val .= $api_val;
									}
								}

								elseif ( $col_type == 'model' ) {
									$api_val = $final_item_data[$azontables_ama_asin]->ItemAttributes->Model;
									if ( $api_val != '' ) {
										$td_val .= $api_val;
									}
								}

								elseif ( $col_type == 'small-image' ) {
									$api_val = $final_item_data[$azontables_ama_asin]->ImageSets->ImageSet->SmallImage->URL;
									if ( $api_val ) {
										$td_val .= '<img src="'.$api_val.'" alt="" />';
									}
										
								}
								elseif ( $col_type == 'medium-image' ) {
									$api_val = $final_item_data[$azontables_ama_asin]->ImageSets->ImageSet->MediumImage->URL;
									if ( $api_val ) {
										$td_val .= '<img src="'.$api_val.'" alt="" />';
									}
								}
								elseif ( $col_type == 'large-image' ) {
									$api_val = $final_item_data[$azontables_ama_asin]->ImageSets->ImageSet->LargeImage->URL;
									if ( $api_val ) {
										$td_val .= '<img src="'.$api_val.'" alt="" />';
									}
								}

								elseif ( $col_type == 'lowest-new-price' ) {
									$td_val .= $final_item_data[$azontables_ama_asin]->OfferSummary->LowestNewPrice->FormattedPrice;
									$data_sort_value = ' data-sort-value="'.$final_item_data[$azontables_ama_asin]->OfferSummary->LowestUsedPrice->Amount.'"';
								}

								elseif ( $col_type == 'lowest-used-price' ) {
									$td_val .= $final_item_data[$azontables_ama_asin]->OfferSummary->LowestUsedPrice->FormattedPrice;
									$data_sort_value = ' data-sort-value="'.$final_item_data[$azontables_ama_asin]->OfferSummary->LowestUsedPrice->Amount.'"';
								}


								elseif ( $col_type == 'button-img-1' ) {
									$td_val .= '<a class="galfam-table-cell-image-link gtm-tablecell-imglink" href="'.$final_item_data[$azontables_ama_asin]->DetailPageURL.'" rel="nofollow" target="_blank">';
										$td_val .= '<img src="'.plugin_dir_url( __FILE__ ).'/images/amazon-btn-1.png" alt="Buy on Amazon" />';
									$td_val .= '</a>';
								}
								elseif ( $col_type == 'button-img-2' ) {
									$td_val .= '<a class="galfam-table-cell-image-link gtm-tablecell-imglink" href="'.$final_item_data[$azontables_ama_asin]->DetailPageURL.'" rel="nofollow" target="_blank">';
										$td_val .= '<img src="'.plugin_dir_url( __FILE__ ).'/images/amazon-btn-2.png" alt="Buy on Amazon" />';
									$td_val .= '</a>';
								}
								elseif ( $col_type == 'button-img-3' ) {
									$td_val .= '<a class="galfam-table-cell-image-link gtm-tablecell-imglink" href="'.$final_item_data[$azontables_ama_asin]->DetailPageURL.'" rel="nofollow" target="_blank">';
										$td_val .= '<img src="'.plugin_dir_url( __FILE__ ).'/images/amazon-btn-3.png" alt="Buy on Amazon" />';
									$td_val .= '</a>';
								}
								elseif ( $col_type == 'button-img-4' ) {
									$td_val .= '<a class="galfam-table-cell-image-link gtm-tablecell-imglink" href="'.$final_item_data[$azontables_ama_asin]->DetailPageURL.'" rel="nofollow" target="_blank">';
										$td_val .= '<img src="'.plugin_dir_url( __FILE__ ).'/images/amazon-btn-4.png" alt="Buy on Amazon" />';
									$td_val .= '</a>';
								}
								elseif ( $col_type == 'button-img-5' ) {
									$td_val .= '<a class="galfam-table-cell-image-link gtm-tablecell-imglink" href="'.$final_item_data[$azontables_ama_asin]->DetailPageURL.'" rel="nofollow" target="_blank">';
										$td_val .= '<img src="'.plugin_dir_url( __FILE__ ).'/images/amazon-btn-5.png" alt="Buy on Amazon" />';
									$td_val .= '</a>';
								}

								elseif ( $col_type == 'button-plain' ) {
									$td_val .= '<a class="galfam-table-cell-button-plain gtm-tablecell-butotnplain" href="'.$final_item_data[$azontables_ama_asin]->DetailPageURL.'" rel="nofollow" target="_blank">Buy on Amazon</a>';
								}

								elseif ( $col_type == 'text-link' ) {
									$td_val .= '<a class="galfam-table-cell-text-link gtm-tablecell-textlink" href="'.$final_item_data[$azontables_ama_asin]->DetailPageURL.'" rel="nofollow" target="_blank">Buy on Amazon</a>';
								}


								elseif ( $col_type == 'price'  ) {

									//echo '<pre>'.print_r($final_item_data,1).'</pre>';

									$price_fields = $td['price_fields'];

									//echo '<pre>'.print_r($price_fields,1).'</pre>';

									//echo '<pre>'.print_r($final_item_data[$azontables_ama_asin]->ItemAttributes,1).'</pre>';

									$listprice = $final_item_data[$azontables_ama_asin]->ItemAttributes->ListPrice->FormattedPrice;
									if ( $listprice ) {
										$listprice_num = $final_item_data[$azontables_ama_asin]->ItemAttributes->ListPrice->Amount;
										$data_sort_value_price = $listprice_num;
									}
									else { $listprice = 'off'; }

									$saleprice = $final_item_data[$azontables_ama_asin]->Offers->Offer->OfferListing->Price->FormattedPrice;
									if ( $saleprice ) {
										$saleprice_num = $final_item_data[$azontables_ama_asin]->Offers->Offer->OfferListing->Price->Amount;
										$data_sort_value_price = $saleprice_num;
									}
									else { $saleprice = 'off'; }

									$amountsaved = $final_item_data[$azontables_ama_asin]->Offers->Offer->OfferListing->AmountSaved->FormattedPrice;
									if ( !$amountsaved ) {
										$amountsaved = 'off'; 
									}

									$percentagesaved = $final_item_data[$azontables_ama_asin]->Offers->Offer->OfferListing->PercentageSaved;
									if ( !$percentagesaved ) {
										$percentagesaved = 'off';
									}

									// in case our list price matches the offer price and there really is no deal
									if ( trim($listprice_num) == trim($saleprice_num) ) {
										$saleprice = 'off';
										$amountsaved = 'off';
										$percentagesaved = 'off';
									}


									$td_prices = '<div class="specs-table-row price spec-price" itemprop="offers" itemscope itemtype="http://schema.org/Offer">';


									// if data for list price or lowest new price is set to display
									if ( $listprice != 'off' && $saleprice != 'off' ) {

										// display the slashed out list price and the lower new price
										$td_prices .= '<span class="slashed-price"><span>'.$listprice.'</span></span>';
										$td_prices .= '<br />';
										$td_prices .= '<span class="deal-price" itemprop="price">'.$saleprice.'</span>';

										if ( $percentagesaved != 'off' && $amountsaved != 'off' ) {
											$td_prices .= '<span class="prod-amount-saved">'.__('Save', 'ultimateazon2').' '.$percentagesaved.'% ('.$amountsaved.')</span>';
										}
										else if ( $percentagesaved != 'off' && $amountsaved == 'off' ) {
											$td_prices .= '<span class="prod-amount-saved">'.__('Save', 'ultimateazon2').' '.$percentagesaved.'%</span>';
										}
										else if ( $percentagesaved == 'off' && $amountsaved != 'off' ) {
											$td_prices .= '<span class="prod-amount-saved">'.__('Save', 'ultimateazon2').' '.$amountsaved.'</span>';
										}

									}

									// if list price data is set to display, but lowest new price is not
									else if ( $listprice != 'off' && $saleprice == 'off' ) {

										// display only the list price
										$td_prices .= '<span class="deal-price" itemprop="price">'.$listprice.'</span>';

									}
									// if list price data is NOT set to display, but lowest new price is
									else if ( $listprice == 'off' && $saleprice != 'off' ) {

										// display only the lowest new price
										$td_prices .= '<span class="deal-price" itemprop="price">'.$saleprice.'</span>';

									}

									$td_prices .= '</div>';



									if ( $listprice != 'off' || $saleprice != 'off' ) {
										$td_val .= $td_prices;
										$data_sort_value = ' data-sort-value="'.$data_sort_value_price.'"';
									}

									else {
										$td_val .= 'N/A';
										$data_sort_value = ' data-sort-value="0"';
									}

								}

								if ( $col_aff_link == 'field-link-affiliate' ) {
									$td_val .= '<a class="galfam-table-cell-cover-link gtm-tablecell-coverlink" href="'.$final_item_data[$azontables_ama_asin]->DetailPageURL.'" rel="nofollow" target="_blank">';
								}

		                    

		                    	$td_styles =' style="';

		                    		// get our column width settings
		                    		$col_width = $td['col_width'];
		                    		$col_width_type = $td['col_width_type'];

			                    	// if our col width setting are set, let's use them
			                    	if ( $col_width != '' && $col_width_type != '' ) {
			                    		$td_styles.= 'width: '.$col_width.$col_width_type.';';
			                    	}

			                    	$horz_align = $td['col_horz_align'];
			                    		if ( $horz_align ) { $td_styles.='text-align:'.$horz_align.';';}
			                    		
			                    	$vert_align = $td['col_vert_align'];
			                    		if ( $vert_align ) { $td_styles.='vertical-align:'.$vert_align.';';}

		                    	$td_styles.='"';

		                        $table_html .= '<td'.$data_sort_value.$td_styles.' class="azontables_'.$col_type.'" data-table-col="Column '.$td_counter.'">';
		                            $table_html .= $td_val;
		                        $table_html .= '</td>';

		                        $td_counter++;
		                    }

		                    

		                $table_html .= '</tr>';

		                $tr_counter++;
		            }

			        $table_html .= '</tbody>';


				} // END if ( $final_table['body'] )



				// close our table tag and outer container
				if ( $final_table ) {
					$table_html .= '</table>';
					$table_html .= '</div>'; // end .azontables-table-outer
				}


			} // end if ( $table_arr )



		return $table_html;

	}











	/** Add our custom styles to the Stylizer Child Theme
	 *
	 * @since    1.0.0
	 */
	public function azontables_add_custom_ext_css_tables() {


		//     .azontables-table-wrapper
		//     .azontables-table
		//     .azontables-table thead
		//     .azontables-table thead tr
		//     .azontables-table thead tr th

		//     .azontables-table tbody
		//     .azontables-table tbody tr
		//     .azontables-table tbody tr td

		ob_start();
		echo '<style type="text/css" id="azontables-css">';
		/*******************************
		// body user styles //
		*******************************/


		// table wrapper styles
		if ( get_field('enable_table_wrapper_border','azontables-options') ) :

			echo '.azontables-table-wrapper{';

				if ( get_field('table_wrapper_border_width','azontables-options') ) :
					echo 'border-width:'.get_field('table_wrapper_border_width','azontables-options').'px;';
					else:
					echo 'border-width:1px;';
					endif;

				if ( get_field('table_wrapper_border_style','azontables-options') ) :
					echo 'border-style:'.get_field('table_wrapper_border_style','azontables-options').';';
					else:
					echo 'border-style:solid;';
					endif;

				if ( get_field('table_wrapper_border_color','azontables-options') ) :
					echo 'border-color:'.get_field('table_wrapper_border_color','azontables-options').';';
					else:
					echo 'border-color:#dedede;';
					endif;

			echo '}';

		endif;


		// table header styles
		if ( get_field('enable_table_header_styles','azontables-options') ) :

			echo '.azontables-table thead tr th{';

				if ( get_field('table_header_bg_color','azontables-options') ) :
					echo 'background-color:'.get_field('table_header_bg_color','azontables-options').';';
					else:
					echo 'background-color:#fff;';
					endif;

				if ( get_field('table_header_color','azontables-options') ) :
					echo 'color:'.get_field('table_header_color','azontables-options').';';
					else:
					echo 'color:#000;';
					endif;

				if ( get_field('table_header_font_size','azontables-options') ) :
					echo 'font-size:'.get_field('table_header_font_size','azontables-options').'px;';
					else:
					echo 'font-size:14px;';
					endif;

				if ( get_field('table_header_line_height','azontables-options') ) :
					echo 'line-height:'.get_field('table_header_line_height','azontables-options').'px;';
					else:
					echo 'line-height:16px;';
					endif;

				if ( get_field('th_padding_top','azontables-options') ) :
					echo 'padding-top:'.get_field('th_padding_top','azontables-options').'px;';
					else:
					echo 'padding-top:10px;';
					endif;

				if ( get_field('th_padding_right','azontables-options') ) :
					echo 'padding-right:'.get_field('th_padding_right','azontables-options').'px!important;';
					else:
					echo 'padding-right:15px;';
					endif;

				if ( get_field('th_padding_bottom','azontables-options') ) :
					echo 'padding-bottom:'.get_field('th_padding_bottom','azontables-options').'px;';
					else:
					echo 'padding-bottom:10px;';
					endif;

				if ( get_field('th_padding_left','azontables-options') ) :
					echo 'padding-left:'.get_field('th_padding_left','azontables-options').'px;';
					else:
					echo 'padding-left:10px;';
					endif;

			echo '}';


			echo '.azontables-table .footable .btn-primary{';
				echo 'background-color:'.get_field('table_header_color','azontables-options').';';
				echo 'border-color:'.get_field('table_header_color','azontables-options').';';
			echo '}';


			// table header border styles
			if ( get_field('enable_table_header_border','azontables-options') ) :

				echo '.azontables-table thead{';

					if ( get_field('table_header_border_width','azontables-options') ) :
						echo 'border-width:'.get_field('table_header_border_width','azontables-options').'px;';
						else:
						echo 'border-width:1px;';
						endif;

					if ( get_field('table_header_border_style','azontables-options') ) :
						echo 'border-style:'.get_field('table_header_border_style','azontables-options').';';
						else:
						echo 'border-style:solid;';
						endif;

					if ( get_field('table_header_border_color','azontables-options') ) :
						echo 'border-color:'.get_field('table_header_border_color','azontables-options').';';
						else:
						echo 'border-color:#dedede;';
						endif;

					$sides = get_field('table_header_border_sides','azontables-options');

					if ( is_array( $sides ) ) : 

						if( !in_array( 'top', $sides ) ) :
							echo 'border-top: none;';
							endif;
						if( !in_array( 'right', $sides ) ) :
							echo 'border-right: none;';
							endif;
						if( !in_array( 'bottom', $sides ) ) :
							echo 'border-bottom: none;';
							endif;
						if( !in_array( 'left', $sides ) ) :
							echo 'border-left: none;';
							endif;

					else:

						if( 'top' != $sides ) :
							echo 'border-top: none;';
							endif;
						if( 'right' != $sides ) :
							echo 'border-right: none;';
							endif;
						if( 'bottom' != $sides ) :
							echo 'border-bottom: none;';
							endif;
						if( 'left' != $sides ) :
							echo 'border-left: none;';
							endif;

					endif;

				echo '}';

			endif;

		endif;



		/**********************/
		// table footer styles
		/**********************/


		// table footer styles
		if ( get_field('enable_table_footer_styles','azontables-options') ) :

				if ( get_field('table_footer_bg_color','azontables-options') ) :
					echo '.azontables-table tfoot, .footable .label-default {';
						echo 'background-color:'.get_field('table_header_bg_color','azontables-options').';';
						echo 'border-color:'.get_field('table_header_bg_color','azontables-options').';';
					echo '}';
				endif;

				if ( get_field('table_footer_text_color','azontables-options') ) :
					echo '.azontables-table tfoot, #content .azontables-table tfoot span, .azontables-table .footable .pagination > li > a {';
						echo 'color:'.get_field('table_footer_text_color','azontables-options').';';
					echo '}';
				endif;

				if ( get_field('table_footer_text_color','azontables-options') ) :
					echo '.footable .pagination > .active > a, .footable .pagination > .active > a:hover {';
						echo 'background-color:'.get_field('table_footer_text_color','azontables-options').';';
						echo 'border-color:'.get_field('table_footer_text_color','azontables-options').';';
					echo '}';
				endif;

		endif;





		/******************/
		// table row styles
		/******************/
		if ( get_field('enable_table_row_styles','azontables-options') ) :

				// table odd row styles
				if ( get_field('odd_row_background_color','azontables-options') ) :
					echo '.azontables-table tbody tr[data-table-row-evenodd="odd"], .azontables-table tbody tr[data-table-row-evenodd="odd"] + .footable-detail-row, .azontables-table tbody tr[data-table-row-evenodd="odd"] + .footable-detail-row tbody {';
						echo 'background-color:'.get_field('odd_row_background_color','azontables-options');
					echo '}';
				endif;

				// table even row styles
				if ( get_field('even_row_background_color','azontables-options') ) :
					echo '.azontables-table tbody tr[data-table-row-evenodd="even"], .azontables-table tbody tr[data-table-row-evenodd="even"] + .footable-detail-row, .azontables-table tbody tr[data-table-row-evenodd="even"] + .footable-detail-row tbody{';
						echo 'background-color:'.get_field('even_row_background_color','azontables-options');
					echo '}';
				endif;

				/*************************/
				// table row bottom border
				/*************************/
				if ( get_field('enable_table_row_bottom_border','azontables-options') ) :
					echo '.azontables-table tbody tr{';
						if ( get_field('table_row_bottom_border_width','azontables-options') ) :
							echo 'border-bottom-width:'.get_field('table_row_bottom_border_width','azontables-options').'px;';
							else:
							echo 'border-bottom-width:1px;';
							endif;
						if ( get_field('table_row_bottom_border_style','azontables-options') ) :
							echo 'border-bottom-style:'.get_field('table_row_bottom_border_style','azontables-options').';';
							else:
							echo 'border-bottom-style:solid;';
							endif;
						if ( get_field('table_row_bottom_border_color','azontables-options') ) :
							echo 'border-bottom-color:'.get_field('table_row_bottom_border_color','azontables-options').';';
							else:
							echo 'border-bottom-color:#dedede;';
							endif;
					echo '}';
					echo '.azontables-table tbody tr:last-child{border-bottom:none;}';
				endif;





				/***********************/
				// table row font styles
				/***********************/
				if ( get_field('enable_table_row_font_styles','azontables-options') ) :

						echo '.azontables-table tbody tr td,.azontables-table tbody tr td.azontables_features li,.azontables-table tbody tr td.azontables_price .deal-price,.azontables-table tbody tr td.azontables_price .slashed-price span,.azontables-table tbody tr td.azontables_price .prod-amount-saved{';
							// table row font styles
							if ( get_field('table_row_global_font_size','azontables-options') ) :
								echo 'font-size:'.get_field('table_row_global_font_size','azontables-options').'px;';
								else:
								echo 'font-size:14px;';
								endif;

							if ( get_field('table_row_global_line_height','azontables-options') ) :
								echo 'line-height:'.get_field('table_row_global_line_height','azontables-options').'px;';
								else:
								echo 'line-height:16px;';
								endif;
						echo '}';






						// table odd row font styles
						if ( get_field('table_row_font_color_odd_rows','azontables-options') ) :
							echo '.azontables-table tbody tr[data-table-row-evenodd="odd"] td,.azontables-table tbody tr[data-table-row-evenodd="odd"] td.azontables_features li,.azontables-table tbody tr[data-table-row-evenodd="odd"] td.azontables_price .deal-price,.azontables-table tbody tr[data-table-row-evenodd="odd"] td.azontables_price .slashed-price span,.azontables-table tbody tr[data-table-row-evenodd="odd"] td.azontables_price .prod-amount-saved{';
								echo 'color:'.get_field('table_row_font_color_odd_rows','azontables-options').';';
							echo '}';

							echo '.azontables-table tbody tr[data-table-row-evenodd="odd"] + .footable-detail-row td{';
								echo 'color:'.get_field('table_row_font_color_odd_rows','azontables-options').';';
							echo '}';

							echo '#content .azontables-table tbody tr[data-table-row-evenodd="odd"] td .footable-toggle{';
								echo 'color:'.get_field('table_row_font_color_odd_rows','azontables-options').';';
							echo '}';

						endif;

						if ( get_field('table_row_link_color_odd_rows','azontables-options') ) :
							echo '.azontables-table tbody tr[data-table-row-evenodd="odd"]) td a:not(.spec-button), .azontables-table tbody tr[data-table-row-evenodd="odd"]) + .footable-detail-row td a:not(.spec-button){';
								echo 'color:'.get_field('table_row_link_color_odd_rows','azontables-options').';';
							echo '}';
						endif;

						if ( get_field('table_row_button_bg_color_odd_rows','azontables-options') || get_field('table_row_button_text_color_odd_rows','azontables-options') ) :
							echo '.azontables-table tbody tr[data-table-row-evenodd="odd"] td a.spec-button, .azontables-table tbody tr[data-table-row-evenodd="odd"]) + .footable-detail-row td a.spec-button{';
								if ( get_field('table_row_button_bg_color_odd_rows','azontables-options') ) :
									echo 'background-color:'.get_field('table_row_button_bg_color_odd_rows','azontables-options').';';
								endif;
								if ( get_field('table_row_button_text_color_odd_rows','azontables-options') ) :
									echo 'color:'.get_field('table_row_button_text_color_odd_rows','azontables-options').';';
								endif;
							echo '}';
						endif;







						// table even row font styles
						if ( get_field('table_row_font_color_even_rows','azontables-options') ) :
							echo '.azontables-table tbody tr[data-table-row-evenodd="even"] td,.azontables-table tbody tr[data-table-row-evenodd="even"] td.azontables_features li,.azontables-table tbody tr[data-table-row-evenodd="even"] td.azontables_price .deal-price,.azontables-table tbody tr[data-table-row-evenodd="even"] td.azontables_price .slashed-price span,.azontables-table tbody tr[data-table-row-evenodd="even"] td.azontables_price .prod-amount-saved{';
								echo 'color:'.get_field('table_row_font_color_even_rows','azontables-options').';';
							echo '}';

							echo '.azontables-table tbody tr[data-table-row-evenodd="even"] + .footable-detail-row td{';
								echo 'color:'.get_field('table_row_font_color_even_rows','azontables-options').';';
							echo '}';

							echo '#content .azontables-table tbody tr[data-table-row-evenodd="even"] td .footable-toggle{';
								echo 'color:'.get_field('table_row_font_color_even_rows','azontables-options').';';
							echo '}';


						endif;

						if ( get_field('table_row_link_color_even_rows','azontables-options') ) :
							echo '.azontables-table tbody tr[data-table-row-evenodd="even"] td a:not(.spec-button), .azontables-table tbody tr[data-table-row-evenodd="even"]) + .footable-detail-row td a:not(.spec-button){';
								echo 'color:'.get_field('table_row_link_color_even_rows','azontables-options').';';
							echo '}';
						endif;

						if ( get_field('table_row_button_bg_color_even_rows','azontables-options') || get_field('table_row_button_text_color_even_rows','azontables-options') ) :
							echo '.azontables-table tbody tr[data-table-row-evenodd="even"] td a.spec-button, .azontables-table tbody tr[data-table-row-evenodd="even"]) + .footable-detail-row td aa.spec-button{';
								if ( get_field('table_row_button_bg_color_even_rows','azontables-options') ) :
									echo 'background-color:'.get_field('table_row_button_bg_color_even_rows','azontables-options').';';
								endif;
								if ( get_field('table_row_button_text_color_even_rows','azontables-options') ) :
									echo 'color:'.get_field('table_row_button_text_color_even_rows','azontables-options').';';
								endif;
							echo '}';
						endif;







						/*************************************/
						// table row font special field styles
						/*************************************/
						if ( get_field('enable_font_styles_for_field_types','azontables-options') ) :




								// ASIN special field styles
								if ( get_field('enable_asin_field_styles','azontables-options') ) :

									echo '.azontables-table tbody tr td.azontables_asin {';
										if ( get_field('asin_text_color','azontables-options') ) :
											echo 'color:'.get_field('asin_text_color','azontables-options').';';
											else:
											echo 'color:#000;';
											endif;

										if ( get_field('asin_font_size','azontables-options') ) :
											echo 'font-size:'.get_field('asin_font_size','azontables-options').'px;';
											else:
											echo 'font-size:14px;';
											endif;

										if ( get_field('asin_line_height','azontables-options') ) :
											echo 'line-height:'.get_field('asin_line_height','azontables-options').'px;';
											else:
											echo 'line-height:16px;';
											endif;
									echo '}';

								endif;




								// Brand special field styles
								if ( get_field('enable_brand_field_styles','azontables-options') ) :

									echo '.azontables-table tbody tr td.azontables_brand {';
										if ( get_field('brand_text_color','azontables-options') ) :
											echo 'color:'.get_field('brand_text_color','azontables-options').';';
											else:
											echo 'color:#000;';
											endif;

										if ( get_field('brand_font_size','azontables-options') ) :
											echo 'font-size:'.get_field('brand_font_size','azontables-options').'px;';
											else:
											echo 'font-size:14px;';
											endif;

										if ( get_field('brand_line_height','azontables-options') ) :
											echo 'line-height:'.get_field('brand_line_height','azontables-options').'px;';
											else:
											echo 'line-height:16px;';
											endif;
									echo '}';

								endif;




								// Model special field styles
								if ( get_field('enable_model_field_styles','azontables-options') ) :

									echo '.azontables-table tbody tr td.azontables_model {';
										if ( get_field('model_text_color','azontables-options') ) :
											echo 'color:'.get_field('model_text_color','azontables-options').';';
											else:
											echo 'color:#000;';
											endif;

										if ( get_field('model_font_size','azontables-options') ) :
											echo 'font-size:'.get_field('model_font_size','azontables-options').'px;';
											else:
											echo 'font-size:14px;';
											endif;

										if ( get_field('model_line_height','azontables-options') ) :
											echo 'line-height:'.get_field('model_line_height','azontables-options').'px;';
											else:
											echo 'line-height:16px;';
											endif;
									echo '}';

								endif;




								// UPC special field styles
								if ( get_field('enable_upc_field_styles','azontables-options') ) :

									echo '.azontables-table tbody tr td.azontables_upc {';
										if ( get_field('upc_text_color','azontables-options') ) :
											echo 'color:'.get_field('upc_text_color','azontables-options').';';
											else:
											echo 'color:#000;';
											endif;

										if ( get_field('upc_font_size','azontables-options') ) :
											echo 'font-size:'.get_field('upc_font_size','azontables-options').'px;';
											else:
											echo 'font-size:14px;';
											endif;

										if ( get_field('upc_line_height','azontables-options') ) :
											echo 'line-height:'.get_field('upc_line_height','azontables-options').'px;';
											else:
											echo 'line-height:16px;';
											endif;
									echo '}';

								endif;




								// Features special field styles
								if ( get_field('enable_features_field_styles','azontables-options') ) :

									echo '.azontables-table tbody tr td.azontables_features li,.azontables-table tbody tr:nth-child(odd) td.azontables_features li,.azontables-table tbody tr:nth-child(even) td.azontables_features li{';
										if ( get_field('features_text_color','azontables-options') ) :
											echo 'color:'.get_field('features_text_color','azontables-options').';';
											else:
											echo 'color:#000;';
											endif;

										if ( get_field('features_font_size','azontables-options') ) :
											echo 'font-size:'.get_field('features_font_size','azontables-options').'px;';
											else:
											echo 'font-size:14px;';
											endif;

										if ( get_field('features_line_height','azontables-options') ) :
											echo 'line-height:'.get_field('features_line_height','azontables-options').'px;';
											else:
											echo 'line-height:16px;';
											endif;
									echo '}';

								endif;




								// Warranty special field styles
								if ( get_field('enable_warranty_field_styles','azontables-options') ) :

									echo '.azontables-table tbody tr td.azontables_warranty {';
										if ( get_field('warranty_text_color','azontables-options') ) :
											echo 'color:'.get_field('warranty_text_color','azontables-options').';';
											else:
											echo 'color:#000;';
											endif;

										if ( get_field('warranty_font_size','azontables-options') ) :
											echo 'font-size:'.get_field('warranty_font_size','azontables-options').'px;';
											else:
											echo 'font-size:14px;';
											endif;

										if ( get_field('warranty_line_height','azontables-options') ) :
											echo 'line-height:'.get_field('warranty_line_height','azontables-options').'px;';
											else:
											echo 'line-height:16px;';
											endif;
									echo '}';

								endif;




								// Star Rating special field styles
								if ( get_field('enable_star_rating_field_styles','azontables-options') ) :

									echo '.azontables-table tbody tr td.azontables_asin {';
										
									echo '}';

								endif;




								// Yes/No special field styles
								if ( get_field('enable_yesno_rating_field_styles','azontables-options') ) :

									echo '.azontables-table tbody tr td.azontables_yesno {';
										
									echo '}';

								endif;




								// Price special field styles
								if ( get_field('enable_price_field_styles','azontables-options') ) :

									if ( get_field('sale_price_font_size','azontables-options') || get_field('sale_price_color','azontables-options') ) :

										echo '.azontables-table tbody tr:nth-child(odd) td.azontables_price .deal-price,.azontables-table tbody tr:nth-child(even) td.azontables_price .deal-price{';

											if ( get_field('sale_price_font_size','azontables-options') ) :
												echo 'font-size:'.get_field('sale_price_font_size','azontables-options').'px;';
												else:
												echo 'font-size:18px;';
												endif;

											if ( get_field('sale_price_color','azontables-options') ) :
												echo 'color:'.get_field('sale_price_color','azontables-options').';';
												else:
												echo 'color:#000;';
												endif;

										echo '}';

									endif;

									if ( get_field('slashed_price_font_size','azontables-options') || get_field('slashed_price_color','azontables-options') ) :

										echo '.azontables-table tbody tr:nth-child(odd) td.azontables_price .slashed-price span,.azontables-table tbody tr:nth-child(even) td.azontables_price .slashed-price span{';
									
											if ( get_field('slashed_price_font_size','azontables-options') ) :
												echo 'font-size:'.get_field('slashed_price_font_size','azontables-options').'px;';
												else:
												echo 'font-size:18px;';
												endif;

											if ( get_field('slashed_price_color','azontables-options') ) :
												echo 'color:'.get_field('slashed_price_color','azontables-options').';';
												else:
												echo 'color:#000;';
												endif;

										echo '}';

									endif;

									if ( get_field('amountpercentage_saved_font_size','azontables-options') || get_field('amountpercentage_saved_color','azontables-options') ) :

										echo '.azontables-table tbody tr:nth-child(odd) td.azontables_price .prod-amount-saved,.azontables-table tbody tr:nth-child(even) td.azontables_price .prod-amount-saved{';
									
											if ( get_field('amountpercentage_saved_font_size','azontables-options') ) :
												echo 'font-size:'.get_field('amountpercentage_saved_font_size','azontables-options').'px;';
												else:
												echo 'font-size:18px;';
												endif;

											if ( get_field('amountpercentage_saved_color','azontables-options') ) :
												echo 'color:'.get_field('amountpercentage_saved_color','azontables-options').';';
												else:
												echo 'color:#000;';
												endif;

										echo '}';

									endif;


								endif;




								// Lowest New Price special field styles
								if ( get_field('enable_lowest_new_price_field_styles','azontables-options') ) :

									echo '.azontables-table tbody tr td.azontables_lowest-new-price {';
										if ( get_field('lowest_new_price_text_color','azontables-options') ) :
											echo 'color:'.get_field('lowest_new_price_text_color','azontables-options').';';
											else:
											echo 'color:#000;';
											endif;

										if ( get_field('lowest_new_price_font_size','azontables-options') ) :
											echo 'font-size:'.get_field('lowest_new_price_font_size','azontables-options').'px;';
											else:
											echo 'font-size:14px;';
											endif;

										if ( get_field('lowest_new_price_line_height','azontables-options') ) :
											echo 'line-height:'.get_field('lowest_new_price_line_height','azontables-options').'px;';
											else:
											echo 'line-height:16px;';
											endif;
									echo '}';

								endif;




								// Lowest Used Price special field styles
								if ( get_field('enable_lowest_used_price_field_styles','azontables-options') ) :

									echo '.azontables-table tbody tr td.azontables_lowest-used-price {';
										if ( get_field('lowest_used_price_text_color','azontables-options') ) :
											echo 'color:'.get_field('lowest_used_price_text_color','azontables-options').';';
											else:
											echo 'color:#000;';
											endif;

										if ( get_field('lowest_used_price_font_size','azontables-options') ) :
											echo 'font-size:'.get_field('lowest_used_price_font_size','azontables-options').'px;';
											else:
											echo 'font-size:14px;';
											endif;

										if ( get_field('lowest_used_price_line_height','azontables-options') ) :
											echo 'line-height:'.get_field('lowest_used_price_line_height','azontables-options').'px;';
											else:
											echo 'line-height:16px;';
											endif;
									echo '}';

								endif;






						endif; // END if ( get_field('enable_font_styles_for_field_types','azontables-options') )

				endif; // end if ( get_field('enable_table_row_font_styles','azontables-options') )

		endif; // END if ( get_field('enable_table_row_styles','azontables-options') )


		
		echo '</style>';

		$buffer = ob_get_clean();

		// Minify CSS
		$buffer = preg_replace('!/\*[^*]*\*+([^/][^*]*\*+)*/!', '', $buffer);
		$buffer = str_replace(': ', ':', $buffer);
		$buffer = str_replace(array("\r\n", "\r", "\n", "\t", '  ', '    ', '    '), '', $buffer);

		echo $buffer;


	}




}
