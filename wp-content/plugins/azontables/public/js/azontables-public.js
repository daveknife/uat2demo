(function( $ ) {
	'use strict';

	/**
	 * All of the code for your public-facing JavaScript source
	 * should reside in this file.
	 *
	 * Note: It has been assumed you will write jQuery code here, so the
	 * $ function reference has been prepared for usage within the scope
	 * of this function.
	 *
	 * This enables you to define handlers, for when the DOM is ready:
	 *
	 * $(function() {
	 *
	 * });
	 *
	 * When the window is loaded:
	 *
	 * $( window ).load(function() {
	 *
	 * });
	 *
	 * ...and/or other possibilities.
	 *
	 * Ideally, it is not considered best practise to attach more than a
	 * single DOM-ready or window-load handler for a particular page.
	 * Although scripts in the WordPress core, Plugins and Themes may be
	 * practising this, we should strive to set a better example in our own work.
	 */


	 jQuery(document).ready(function() {

		/****************************************
		These functions are for front end styling
		****************************************/


		$('.azontables-table').footable();


		$('body').on('after.ft.sorting', '.azontables-table', function() {
			
			//console.log('sorted');
			var evenodd = 'odd';

			$('.azontables-table:not(.footable-details) > tbody > tr:not(.footable-detail-row)').each(function(i){

				
				if( evenodd === 'odd') {
					$(this).attr('data-table-row-evenodd','odd');
					evenodd = 'even';
				}
				else {
					$(this).attr('data-table-row-evenodd','even');
					evenodd = 'odd';
				}

			});

			// $('.azontables-table tbody > tr[data-table-row-evenodd]:nth-child(even)').not('.footable-detail-row').attr('data-table-row-evenodd','even');
			// $('.azontables-table tbody > tr[data-table-row-evenodd]:nth-child(odd)').not('.footable-detail-row').attr('data-table-row-evenodd','odd');

		});


		


	});



})( jQuery );
