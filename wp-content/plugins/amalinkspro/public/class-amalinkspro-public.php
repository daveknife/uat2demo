<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       http://amalinkspro.com
 * @since      1.0.0
 *
 * @package    Ama_Links_Pro
 * @subpackage Ama_Links_Pro/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the public-facing stylesheet and JavaScript.
 *
 * @package    Ama_Links_Pro
 * @subpackage Ama_Links_Pro/public
 * @author     Your Name <email@amalinkspro.com>
 */
class Ama_Links_Pro_Public {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $Ama_Links_Pro    The ID of this plugin.
	 */
	private $Ama_Links_Pro;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $Ama_Links_Pro       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $Ama_Links_Pro, $version ) {

		$this->Ama_Links_Pro = $Ama_Links_Pro;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Ama_Links_Pro_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Ama_Links_Pro_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->Ama_Links_Pro, plugin_dir_url( __FILE__ ) . 'css/amalinkspro-public.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Ama_Links_Pro_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Ama_Links_Pro_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->Ama_Links_Pro, plugin_dir_url( __FILE__ ) . 'js/amalinkspro-public.js', array( 'jquery' ), $this->version, false );





		


	}


	/**
	 * Register the JavaScript for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function amalinks_pro_footer_scripts() {

		$footer_scripts = '';

		$footer_scripts .= get_option('amalinkspro-options_amazon_onelink_script', true);

		echo $footer_scripts;

	} 




	// [amalinkspro /]
	public function amalinkspro_shortcode_functions( $atts, $content=null ) {

	    // set our table ID variable form the id attribute of the shortcode
	    //$table_id = $atts['id'];

	    //echo '<pre>'.print_r($atts,1).'</pre>';

	    $amalinkspro_html_output = '';

	    $asin = $atts['asin'];
	    $associate_id = $atts['associate_id'];

	    if ( $asin && $associate_id ) {


			/*
			http://amzn.com/0470560541/?tag=wpb09-20
			http://amazon.com/dp/0470560541/?tag=wpb09-20
			*/


			/* add to cart button */

			

			


		    $link_type = $atts['type'];

		    //$aws_access_key_id = trim( get_option( 'amalinkspro-options_amazon_api_access_key' ) );


		    if ( $link_type == 'text-link' ) {

		  //   	$amalinkspro_html_output .= '<a class="amalinkspro-text-link amalinkspro-addtocart" href="https://amazon.com/dp/' .$asin. '/?tag=' .$associate_id. '" target="blank">';
		  //   	$amalinkspro_html_output .= $content;
		  //   	$amalinkspro_html_output .= '<form class="amalinkspro-hidden-addtocartform" method="GET" action="http://www.amazon.com/gp/aws/cart/add.html">
				// 	<input type="hidden" name="AssociateTag" value="'.$associate_id.'" />
				// 	<input type="hidden" name="SubscriptionId" value="'.$aws_access_key_id.'" />
				// 	<input type="hidden" name="ASIN.1" value="'.$asin.'" />
				// 	<input type="hidden" name="Quantity.1" value="1"/>
				// 	<input type="image" name="add" value="Buy from Amazon.com" border="0" alt="Buy from Amazon.com" src="https://images-na.ssl-images-amazon.com/images/G/01/associates/remote-buy-box/buy5.gif">
				// </form>';
		  //   	$amalinkspro_html_output .= '</a>';

		    	$amalinkspro_html_output .= '<a class="amalinkspro-text-link" href="https://amazon.com/dp/' .$asin. '/?tag=' .$associate_id. '" target="blank">';
		    	$amalinkspro_html_output .= $content;
		    	$amalinkspro_html_output .= '</a>';

		    	


		    }

		    elseif ( $link_type == 'image-link' ) {

		    	$amalinkspro_html_output .= '<a href="https://amazon.com/dp/' .$asin. '/?tag=' .$associate_id. '" target="blank"><img src="' .$content. '" alt="" /></a>';
		    	
		    }

		    elseif ( $link_type == 'cta-link' ) {

		    	$cta_style = $atts['ctastyle'];
		    	$cta_class = $atts['ctaclass'];


		    	if ( $cta_style == 'cta-btn-text' ) {

		    		$amalinkspro_html_output .= '<div class="amalinkspro-cta-wrap ' .$cta_class. '">';

		    		$amalinkspro_html_output .= '<a href="https://amazon.com/dp/' .$asin. '/?tag=' .$associate_id. '" target="blank" class="amalinkspro-cta-link-link">' .$content. '</a> ';

		    		$amalinkspro_html_output .= '</div>';

		    	}

		    	elseif ( $cta_style == 'cta-btn-img' ) {

		    		$amalinkspro_html_output .= '<div class="amalinkspro-cta-wrap ' .$cta_class. '">';

		    		$amalinkspro_html_output .= '<a href="https://amazon.com/dp/' .$asin. '/?tag=' .$associate_id. '" target="blank" class="amalinkspro-cta-image-link"><img src="' .$content. '" alt="" /></a> ';

		    		$amalinkspro_html_output .= '</div>';

		    	}



		    	$amalinkspro_html_output .= '<a href="https://amazon.com/dp/' .$asin. '/?tag=' .$associate_id. '" target="blank"><img src="' .$content. '" alt="" /></a>';
		    	
		    }

		    elseif ( $link_type == 'info-block' ) {
		    	
		    }

		    else {
		    	$amalinkspro_html_output .= 'Error';
		    }

		}


	    return $amalinkspro_html_output;



	}




}
