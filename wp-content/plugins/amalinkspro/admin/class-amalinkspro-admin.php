<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       http://amalinkspro.com
 * @since      1.0.0
 *
 * @package    Ama_Links_Pro
 * @subpackage Ama_Links_Pro/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Ama_Links_Pro
 * @subpackage Ama_Links_Pro/admin
 * @author     Your Name <email@amalinkspro.com>
 */
class Ama_Links_Pro_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $Ama_Links_Pro    The ID of this plugin.
	 */
	private $Ama_Links_Pro;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $Ama_Links_Pro       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $Ama_Links_Pro, $version, $edd_product, $edd_store ) {

		$this->Ama_Links_Pro = $Ama_Links_Pro;
		$this->version = $version;
		$this->edd_product = $edd_product;
		$this->edd_store = $edd_store;

	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Ama_Links_Pro_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Ama_Links_Pro_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->Ama_Links_Pro, plugin_dir_url( __FILE__ ) . 'css/amalinkspro-admin.css', array(), $this->version, 'all' );

		wp_enqueue_style( 'jqueryui-css', '//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css', array(), $this->version, 'all' );

		$google_fonts_api = new amalinkspro_google_fonts();
		$google_fonts_css = $google_fonts_api->build_google_fonts_css_link();
		if ($google_fonts_css) {
			wp_enqueue_style( 'amalinkspro-googlefonts', $google_fonts_css, array(), null, 'all' );
		}
		

 


	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Ama_Links_Pro_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Ama_Links_Pro_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->Ama_Links_Pro, plugin_dir_url( __FILE__ ) . 'js/amalinkspro-admin-min.js', array( 'jquery' ), $this->version, false );

		$handle = 'jquery-ui-core';
		$list = 'enqueued';
		if ( !wp_script_is( $handle, $list )) {
			wp_enqueue_script( 'jquery-ui-core' );
		}

		wp_dequeue_script( 'jquery-ui-core' );



		$handle = 'jquery-ui-draggable';
		$list = 'enqueued';
		if ( !wp_script_is( $handle, $list )) {
			wp_enqueue_script( 'jquery-ui-draggable' );
		}


		$handle = 'jquery-ui-droppable';
		$list = 'enqueued';
		if ( !wp_script_is( $handle, $list )) {
			wp_enqueue_script( 'jquery-ui-droppable' );
		}


		$handle = 'jquery-ui-widget';
		$list = 'enqueued';
		if ( !wp_script_is( $handle, $list )) {
			wp_enqueue_script( 'jquery-ui-widget' );
		}


		$handle = 'jquery-ui-mouse';
		$list = 'enqueued';
		if ( !wp_script_is( $handle, $list )) {
			wp_enqueue_script( 'jquery-ui-mouse' );
		}


	}





	function amalinkspro_acf_settings_path( $path ) {
	    // update path
	    $path = plugin_dir_url( 'amalinkspro.php' ) . 'amalinkspro/includes/advanced-custom-fields-pro/';
	    // return
	    return $path;
	}


	function amalinkspro_acf_settings_dir( $dir ) {
	    // update path
	    $dir = plugin_dir_url( 'amalinkspro.php' ) . 'amalinkspro/includes/advanced-custom-fields-pro/';
	    // return
	    return $dir;
	}




	/**
	 * Include the Plugin Updater
	 *
	 * @since    1.0.0
	 */
	function amalinkspro_plugin_updater() {

		// retrieve our license key from the DB
		$license_key = trim( get_option( 'amalinkspro_license' ) );

		// echo '$license_key - <pre>'.print_r($license_key,1).'</pre>';
		// die();

		$plugin_file = ABSPATH . 'wp-content/plugins/amalinkspro/amalinkspro.php';

		// setup the updater
		$edd_updater = new AMALINKSPRO_Plugin_Updater( $this->edd_store, $plugin_file, array(
				'version' 	=> $this->version, 				// current version number
				'license' 	=> $license_key, 		// license key (used get_option above to retrieve from DB)
				'item_name' => $this->edd_product, 	// name of this plugin
				'author' 	=> 'Dave Nicosia',  // author of this plugin
				'url'       => home_url()
			)
		);

		// echo '<pre>'.print_r($edd_updater,1).'</pre>';
		// die();

	}






	// set up our default licensing values
	function amalinkspro_options_init() {
	    $amalinkspro_license = get_option( 'amalinkspro_license' );
	    // Are our options saved in the DB?
	    if ( false === $amalinkspro_license ) {
	        // If not, we'll save our default options
	        add_option( 'amalinkspro_license', '' );
	        add_option( 'amalinkspro_license_status', 'deactivated' );
	    }
	    // In other case we don't need to update the DB
	}



	// Add "Licenses" link to the "AmaLinks Pro" menu
	function amalinkspro_menu_options() {
	    // add_theme_page( $page_title, $menu_title, $capability, $menu_slug, $function);
	    add_submenu_page('amalinkspro-settings', 'AmaLinks Pro - Licensing', 'License', 'edit_theme_options', 'amalinkspro-license', 'amalinkspro_licensing_page', '' );


	    function amalinkspro_licensing_page () {
		?>


		    <div class="wrap settings-section">
		        <h2><?php _e( 'AmaLinks Pro Licensing Center', 'amalinkspro' ); ?></h2>
		        
		        <?php // the ID in the box must be the license option name: amalinkspro_CHILDTHEMEOREXTENSIONNAME_license ?>
		        <div class="gf-option-box" id="amalinkspro_license_wrap">


		            <div class="gf-option-box-inner">


		                <?php
		                $license    = get_option('amalinkspro_license');
		                $status     = get_option('amalinkspro_license_status');

		                if($status=='deactivated'):
		                echo '<div class="license-message amalinkspro-license-deactivated"><span>' . __( 'The license key you entered has been deactivated on this website.', 'amalinkspro' ) . '</span>';
		                //endif;

		                elseif($status=='invalid'):
		                echo '<div class="license-message amalinkspro-license-invalid"><span>' . __( 'The license key you entered is invalid. Please try again.', 'amalinkspro' ) . '</span>';
		                //endif;


		                elseif($status=='valid'):
		                echo '<div class="license-message amalinkspro-license-valid"><span>' . __( 'Your license is active.', 'amalinkspro' ) . '</span>';

		           		else:
		           			 echo '<div class="license-message"><span></span>';
		                endif;
		                ?>

			                <div class="license-form">

			                    <?php // the ID & name attributes must be the option name stored in the DB. It should follow this format:   amalinkspro_CHILDorEXTENSIONTITLE_license ?>
			                    <input id="amalinkspro_license" type="text" class="amalinkspro-license" name="amalinkspro_license" value="<?php echo $license; ?>" />

			                    <?php // the ID and name attributes must be "amalinkspro_product" and the value must be the name of the product in the amalinkspro.com store. The input type must be "hidden" ?>
			                    <input id="amalinkspro_product" type="hidden" class="amalinkspro-product" name="amalinkspro_product" value="AmaLinks Pro" />

			                    <?php if ( $license != '' && $status == 'valid' ) : ?>

			                        <?php // The deactivate button must have  amalinkspro-deactivate  as a class ?>
			                        <?php wp_nonce_field( 'edd_sample_nonce', 'edd_sample_nonce' ); ?>
			                            <input type="button" class="button button-primary button-large amalinkspro-deactivate" name="amalinksprodeactivate" value="<?php _e('Deactivate', 'amalinkspro'); ?>"/>

			                    <?php else: ?>

			                        <?php // The activate button must have  amalinkspro-activate  as a class ?>
			                        <?php wp_nonce_field( 'edd_sample_nonce', 'edd_sample_nonce' ); ?>
			                            <input type="button" class="button button-primary button-large amalinkspro-activate" name="amalinksproactivate" value="<?php _e('Activate', 'amalinkspro'); ?>"/>

			                    <?php endif; ?>

			                </div>

			                <img class="amalinkspro-license-loading-gif" src="/wp-admin/images/loading.gif" alt="loading" />

		                </div>

		                <div class="license-message-note">

		                	<?php 
		                	echo __('<p><span style="color: #dc5454;margin-bottom: 10px;display: block;">'.__('* This is not where you put your Amazon API Keys','amalinkspro').'.</span></p>
		                		<p>This is your AmaLinks Pro License. It is required for automatic updates, support and link creation. You can find your license in your email receipt, or by logging into','amalinkspro').' <a href="https://amalinkspro.com/my-account/" target="_blank">'. __( 'your account','amalinkspro') .'</a>..</p>';
		                	?>

		                </p>



		            </div>

		        </div>


		    </div>
		    <div class="clear"></div>

		<?php
		}





		add_submenu_page('amalinkspro-settings', 'AmaLinks Pro - Tools', 'Tools', 'edit_theme_options', 'amalinkspro-tools', 'amalinkspro_tools_page', '' );


	    function amalinkspro_tools_page () {


	    	

		?>


		    <div class="wrap settings-section">
		        <h2><?php _e( 'AmaLinks Pro Tools', 'amalinkspro' ); ?></h2>
		        
		        <?php // the ID in the box must be the license option name: amalinkspro_CHILDTHEMEOREXTENSIONNAME_license ?>
		        <div class="gf-option-box" id="amalinkspro_license_wrap">

		            <h3><?php _e( 'System Information', 'amalinkspro' ) ?></h3>
		            

		            <div class="gf-option-box-inner">

		            <form id="amalinkspro-system-info" action="<?php echo admin_url( 'admin.php?page=amalinkspro-tools' ); ?>" method="post" >

		            	<input type="hidden" name="action" value="amalinkspro_system_info_dwnld">

		            	<h4><?php _e( 'A copy of this information is required when submitting a support request. CLick in the box, copy and paste it when creating a spport ticket.', 'amalinkspro' ) ?></h4>


		                
		                <textarea readonly="readonly" onclick="this.focus(); this.select()" id="amalinkspro-system-info-textarea" name="amalinkspro-sysinfo"><?php
			                $return  = '### Begin System Info ###' . "\n\n";



			                $return .= "\n" . '-- AmaLinks Pro' . "\n\n";

							//$Ama_Links_Pro_Admin = New Ama_Links_Pro_Admin('',''); 

							if( ini_get('allow_url_fopen') ) {
								$return .=  'Making API call using file_get_contents()' . "\n";
							}
							else {
								$return .=  'Making API using WP_Curl - We recommend having your website hosting turn on "allow_url_fopen" in your server\'s PHP settings' . "\n";
							}

							$amalinkspro_amazon_api = new amalinkspro_amazon_api();
							$api_response = $amalinkspro_amazon_api->amazon_api_request( null, "Images,ItemAttributes,Offers,Reviews", "guitar" ) . "\n";

							if ( $api_response ) {

								$arr = simplexml_load_string($api_response);

								$return .=  'Amazon API Connection was Successful!' . "\n";


								$arguments = $arr->OperationRequest->Arguments->Argument;

								$api_key_name = $arguments[0]['Name'];
								$api_key_value = $arguments[0]['Value'];

								$return .=  $api_key_name . ': ' . $api_key_value . "\n";


								$api_key_name = $arguments[1]['Name'];
								$api_key_value = $arguments[1]['Value'];

								$return .=  $api_key_name . ': ' . $api_key_value.  "\n";


								$api_key_name = $arguments[6]['Name'];
								$api_key_value = $arguments[6]['Value'];

								$return .=  $api_key_name . ': ' . $api_key_value . "\n";

								$api_key_name = $arguments[6]['Name'];
								$api_key_value = $arguments[6]['Value'];

								$return .=  'Request Processing Time: ' . $arguments = $arr->OperationRequest->RequestProcessingTime . ' seconds' . "\n\n\n";

							}

							else {
								$return .=  'There was a problem connecting to Amazon' . "\n\n\n";
							}


							$return .= '-- Site Info' . "\n\n";
							$return .= 'Site URL:                 ' . site_url() . "\n";
							$return .= 'Home URL:                 ' . home_url() . "\n";
							$return .= 'Multisite:                ' . ( is_multisite() ? 'Yes' : 'No' ) . "\n";



							$host = false;

							if( defined( 'WPE_APIKEY' ) ) {
								$host = 'WP Engine';
							} elseif( defined( 'PAGELYBIN' ) ) {
								$host = 'Pagely';
							} elseif( DB_HOST == 'localhost:/tmp/mysql5.sock' ) {
								$host = 'ICDSoft';
							} elseif( DB_HOST == 'mysqlv5' ) {
								$host = 'NetworkSolutions';
							} elseif( strpos( DB_HOST, 'ipagemysql.com' ) !== false ) {
								$host = 'iPage';
							} elseif( strpos( DB_HOST, 'ipowermysql.com' ) !== false ) {
								$host = 'IPower';
							} elseif( strpos( DB_HOST, '.gridserver.com' ) !== false ) {
								$host = 'MediaTemple Grid';
							} elseif( strpos( DB_HOST, '.pair.com' ) !== false ) {
								$host = 'pair Networks';
							} elseif( strpos( DB_HOST, '.stabletransit.com' ) !== false ) {
								$host = 'Rackspace Cloud';
							} elseif( strpos( DB_HOST, '.sysfix.eu' ) !== false ) {
								$host = 'SysFix.eu Power Hosting';
							} elseif( strpos( $_SERVER['SERVER_NAME'], 'Flywheel' ) !== false ) {
								$host = 'Flywheel';
							} else {
								// Adding a general fallback for data gathering
								$host = 'DBH: ' . DB_HOST . ', SRV: ' . $_SERVER['SERVER_NAME'];
							}

							// Can we determine the site's host?
							if( $host ) {
								$return .= "\n" . '-- Hosting Provider' . "\n\n";
								$return .= 'Host:                     ' . $host . "\n";

								$return  = apply_filters( 'edd_sysinfo_after_host_info', $return );
							}





							// The local users' browser information, handled by the Browser class
							// $return .= "\n" . '-- User Browser' . "\n\n";
							// $return .= $browser;







							// Get theme info
							$theme_data   = wp_get_theme();
							$theme        = $theme_data->Name . ' ' . $theme_data->Version;
							$parent_theme = $theme_data->Template;
							if ( ! empty( $parent_theme ) ) {
								$parent_theme_data = wp_get_theme( $parent_theme );
								$parent_theme      = $parent_theme_data->Name . ' ' . $parent_theme_data->Version;
							}


							// WordPress configuration
							$return .= "\n" . '-- WordPress Configuration' . "\n\n";
							$return .= 'Version:                  ' . get_bloginfo( 'version' ) . "\n";
							$return .= 'Language:                 ' . ( !empty( $locale ) ? $locale : 'en_US' ) . "\n";
							$return .= 'Permalink Structure:      ' . ( get_option( 'permalink_structure' ) ? get_option( 'permalink_structure' ) : 'Default' ) . "\n";
							$return .= 'Active Theme:             ' . $theme . "\n";
							if ( $parent_theme !== $theme ) {
								$return .= 'Parent Theme:             ' . $parent_theme . "\n";
							}
							$return .= 'Show On Front:            ' . get_option( 'show_on_front' ) . "\n";

							// Only show page specs if frontpage is set to 'page'
							if( get_option( 'show_on_front' ) == 'page' ) {
								$front_page_id = get_option( 'page_on_front' );
								$blog_page_id = get_option( 'page_for_posts' );

								$return .= 'Page On Front:            ' . ( $front_page_id != 0 ? get_the_title( $front_page_id ) . ' (#' . $front_page_id . ')' : 'Unset' ) . "\n";
								$return .= 'Page For Posts:           ' . ( $blog_page_id != 0 ? get_the_title( $blog_page_id ) . ' (#' . $blog_page_id . ')' : 'Unset' ) . "\n";
							}

							$return .= 'ABSPATH:                  ' . ABSPATH . "\n";

							// Make sure wp_remote_post() is working
							$request['cmd'] = '_notify-validate';

							$params = array(
								'sslverify'     => false,
								'timeout'       => 60,
								'user-agent'    => 'EDD/' . EDD_VERSION,
								'body'          => $request
							);

							$response = wp_remote_post( 'https://www.paypal.com/cgi-bin/webscr', $params );

							if( !is_wp_error( $response ) && $response['response']['code'] >= 200 && $response['response']['code'] < 300 ) {
								$WP_REMOTE_POST = 'wp_remote_post() works';
							} else {
								$WP_REMOTE_POST = 'wp_remote_post() does not work';
							}

							$return .= 'Remote Post:              ' . $WP_REMOTE_POST . "\n";
							$return .= 'Table Prefix:             ' . 'Length: ' . strlen( $wpdb->prefix ) . '   Status: ' . ( strlen( $wpdb->prefix ) > 16 ? 'ERROR: Too long' : 'Acceptable' ) . "\n";
							// Commented out per https://github.com/easydigitaldownloads/Easy-Digital-Downloads/issues/3475
							//$return .= 'Admin AJAX:               ' . ( edd_test_ajax_works() ? 'Accessible' : 'Inaccessible' ) . "\n";
							$return .= 'WP_DEBUG:                 ' . ( defined( 'WP_DEBUG' ) ? WP_DEBUG ? 'Enabled' : 'Disabled' : 'Not set' ) . "\n";
							$return .= 'Memory Limit:             ' . WP_MEMORY_LIMIT . "\n";
							$return .= 'Registered Post Stat:    ' . implode( ', ', get_post_stati() ) . "\n";





							// WordPress active plugins
							$return .= "\n" . '-- WordPress Active Plugins' . "\n\n";

							// Get plugins that have an update
							$updates = get_plugin_updates();

							$plugins = get_plugins();
							$active_plugins = get_option( 'active_plugins', array() );

							foreach( $plugins as $plugin_path => $plugin ) {
								if( !in_array( $plugin_path, $active_plugins ) )
									continue;

								$update = ( array_key_exists( $plugin_path, $updates ) ) ? ' (needs update - ' . $updates[$plugin_path]->update->new_version . ')' : '';
								$return .= $plugin['Name'] . ': ' . $plugin['Version'] . $update . "\n";
							}







							// WordPress inactive plugins
							$return .= "\n" . '-- WordPress Inactive Plugins' . "\n\n";

							foreach( $plugins as $plugin_path => $plugin ) {
								if( in_array( $plugin_path, $active_plugins ) )
									continue;

								$update = ( array_key_exists( $plugin_path, $updates ) ) ? ' (needs update - ' . $updates[$plugin_path]->update->new_version . ')' : '';
								$return .= $plugin['Name'] . ': ' . $plugin['Version'] . $update . "\n";
							}







							if( is_multisite() ) {
								// WordPress Multisite active plugins
								$return .= "\n" . '-- Network Active Plugins' . "\n\n";

								$plugins = wp_get_active_network_plugins();
								$active_plugins = get_site_option( 'active_sitewide_plugins', array() );

								foreach( $plugins as $plugin_path ) {
									$plugin_base = plugin_basename( $plugin_path );

									if( !array_key_exists( $plugin_base, $active_plugins ) )
										continue;

									$update = ( array_key_exists( $plugin_path, $updates ) ) ? ' (needs update - ' . $updates[$plugin_path]->update->new_version . ')' : '';
									$plugin  = get_plugin_data( $plugin_path );
									$return .= $plugin['Name'] . ': ' . $plugin['Version'] . $update . "\n";
								}

							}

							global $wpdb;

							// Server configuration (really just versioning)
							$return .= "\n" . '-- Webserver Configuration' . "\n\n";
							$return .= 'PHP Version:              ' . PHP_VERSION . "\n";
							$return .= 'MySQL Version:            ' . $wpdb->db_version() . "\n";
							$return .= 'Webserver Info:           ' . $_SERVER['SERVER_SOFTWARE'] . "\n";



							// PHP configs... now we're getting to the important stuff
							$return .= "\n" . '-- PHP Configuration' . "\n\n";
							
							if( ini_get('allow_url_fopen') ) {
								$return .= 'Allow URL Fopen:          ' . 'allow_url_fopen is enabled' . "\n";
							}

							$return .= 'Memory Limit:             ' . ini_get( 'memory_limit' ) . "\n";
							$return .= 'Upload Max Size:          ' . ini_get( 'upload_max_filesize' ) . "\n";
							$return .= 'Post Max Size:            ' . ini_get( 'post_max_size' ) . "\n";
							$return .= 'Upload Max Filesize:      ' . ini_get( 'upload_max_filesize' ) . "\n";
							$return .= 'Time Limit:               ' . ini_get( 'max_execution_time' ) . "\n";
							$return .= 'Max Input Vars:           ' . ini_get( 'max_input_vars' ) . "\n";
							$return .= 'Display Errors:           ' . ( ini_get( 'display_errors' ) ? 'On (' . ini_get( 'display_errors' ) . ')' : 'N/A' ) . "\n";
							// $return .= 'PHP Arg Separator:        ' . edd_get_php_arg_separator_output() . "\n";



							// PHP extensions and such
							$return .= "\n" . '-- PHP Extensions' . "\n\n";
							$return .= 'cURL:                     ' . ( function_exists( 'curl_init' ) ? 'Supported' : 'Not Supported' ) . "\n";
							$return .= 'fsockopen:                ' . ( function_exists( 'fsockopen' ) ? 'Supported' : 'Not Supported' ) . "\n";
							$return .= 'SOAP Client:              ' . ( class_exists( 'SoapClient' ) ? 'Installed' : 'Not Installed' ) . "\n";
							$return .= 'Suhosin:                  ' . ( extension_loaded( 'suhosin' ) ? 'Installed' : 'Not Installed' ) . "\n";



							// Session stuff
							// $return .= "\n" . '-- Session Configuration' . "\n\n";
							// $return .= 'EDD Use Sessions:         ' . ( defined( 'EDD_USE_PHP_SESSIONS' ) && EDD_USE_PHP_SESSIONS ? 'Enforced' : ( EDD()->session->use_php_sessions() ? 'Enabled' : 'Disabled' ) ) . "\n";
							$return .= 'Session:                  ' . ( isset( $_SESSION ) ? 'Enabled' : 'Disabled' ) . "\n";

							// The rest of this is only relevant is session is enabled
							if( isset( $_SESSION ) ) {
								$return .= 'Session Name:             ' . esc_html( ini_get( 'session.name' ) ) . "\n";
								$return .= 'Cookie Path:              ' . esc_html( ini_get( 'session.cookie_path' ) ) . "\n";
								$return .= 'Save Path:                ' . esc_html( ini_get( 'session.save_path' ) ) . "\n";
								$return .= 'Use Cookies:              ' . ( ini_get( 'session.use_cookies' ) ? 'On' : 'Off' ) . "\n";
								$return .= 'Use Only Cookies:         ' . ( ini_get( 'session.use_only_cookies' ) ? 'On' : 'Off' ) . "\n";
							}


							



							$return .= "\n" . '### End System Info ###';





							echo $return;
							?></textarea>



		                <?php wp_nonce_field( 'edd_sample_nonce', 'edd_sample_nonce' ); ?>
		                <!-- <input type="submit" class="button-secondary amalinkspro-generate_system-info" name="amalinkspro-generate_system-info" value="<?php _e('Download System Info', 'amalinkspro'); ?>"/> -->




		            </div>

		        </div>


		    </div>
		    <div class="clear"></div>

		<?php
		}
	}







	/**
	 * Include the ACF fields
	 *
	 * @since    1.0.0
	 */
	public function amalinkspro_include_acf_fields() {


		// 5. Add Site Options Page
	    if( function_exists('acf_add_options_page') ) {

	        $option_page = acf_add_options_page(array(
	            'page_title'    => __('AmaLinks Pro Settings','amalinkspro'),
	            'menu_title'    => __('AmaLinks Pro','amalinkspro'),
	            'menu_slug'     => 'amalinkspro-settings',
	            'capability'    => 'edit_posts',
	            'post_id'       => 'amalinkspro-options',
	            'icon_url' => 'dashicons-admin-links',
	            'redirect'  => false
	        ));

	    }



	    if( function_exists('acf_add_local_field_group') ):

			acf_add_local_field_group(array (
				'key' => 'group_5a5ffec9ec0b1',
				'title' => 'AmaLinks Pro Settings',
				'fields' => array (
					array (
						'key' => 'field_5a5ffed2e1992',
						'label' => 'Amazon API',
						'name' => '',
						'type' => 'tab',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'placement' => 'top',
						'endpoint' => 0,
					),
					array (
						'key' => 'field_tgvh456gtc45ct54h45g',
						'label' => '',
						'name' => '',
						'type' => 'message',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'message' => 'You will only have to set up your Amazon Product Advertising API credentials once. For complete instructions on how to sign up for and obtain your API credentials, please view our <a href="https://amalinkspro.com/amazon-api-credentials/" target="_blank">Amazon Product Advertising API Tutorial</a>',
						'new_lines' => 'wpautop',
						'esc_html' => 0,
					),
					array (
						'key' => 'field_5a5fff12e1993',
						'label' => 'Amazon API Access Key',
						'name' => 'amazon_api_access_key',
						'type' => 'text',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '50',
							'class' => '',
							'id' => '',
						),
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'maxlength' => '',
					),
					array (
						'key' => 'field_5a5fff25e1994',
						'label' => 'Amazon API Secret Key',
						'name' => 'amazon_api_secret_key',
						'type' => 'text',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '50',
							'class' => '',
							'id' => '',
						),
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'maxlength' => '',
					),
					array (
						'key' => 'field_5a6001129b883',
						'label' => 'Default Amazon Search Locale',
						'name' => 'default_amazon_search_locale',
						'type' => 'select',
						'instructions' => 'Please set your default search locale using the option below. When you use Product Review Pro, the links you create will default to this location so if most of your visitors come from the United States for example, you would set your default locale to United States.',
						'required' => 1,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'choices' => array (
							'US' => 'United States',
							'UK' => 'United Kingdom',
							'BR' => 'Brazil',
							'CA' => 'Canada',
							'CN' => 'China',
							'FR' => 'France',
							'DE' => 'Germany',
							'IN' => 'India',
							'IT' => 'Italy',
							'JP' => 'Japan',
							'MX' => 'Mexico',
							'ES' => 'Spain',
						),
						'default_value' => array (
						),
						'allow_null' => 1,
						'multiple' => 0,
						'ui' => 0,
						'ajax' => 0,
						'return_format' => 'value',
						'placeholder' => '',
					),
					array (
						'key' => 'field_5a6001c7b597a',
						'label' => 'Amazon Associate IDs',
						'name' => 'US_amazon_associate_ids',
						'type' => 'repeater',
						'instructions' => '(United States)<br />More than one associate ID can be added in AmaLinks Pro. This can be very helpful if you want to use different IDs in different locations for tracking purposes. This is not used for link localization, for localization click the "Localization" tab.',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_5a6001129b883',
									'operator' => '==',
									'value' => 'US',
								),
							),
						),
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'collapsed' => '',
						'min' => 0,
						'max' => 0,
						'layout' => 'row',
						'button_label' => 'Add Associate ID',
						'sub_fields' => array (
							array (
								'key' => 'field_5a600200b597b',
								'label' => 'Associate ID',
								'name' => 'associate_id',
								'type' => 'text',
								'instructions' => '',
								'required' => 0,
								'conditional_logic' => 0,
								'wrapper' => array (
									'width' => '',
									'class' => '',
									'id' => '',
								),
								'default_value' => '',
								'placeholder' => '',
								'prepend' => '',
								'append' => '',
								'maxlength' => '',
							),
						),
					),
					array (
						'key' => 'field_5a60021db597c',
						'label' => 'Amazon Associate IDs',
						'name' => 'UK_amazon_associate_ids',
						'type' => 'repeater',
						'instructions' => '(United Kingdon)<br />More than one associate ID can be added in AmaLinks Pro. This can be very helpful if you want to use different IDs in different locations for tracking purposes. This is not used for link localization, for localization click the "Localization" tab.',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_5a6001129b883',
									'operator' => '==',
									'value' => 'UK',
								),
							),
						),
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'collapsed' => '',
						'min' => 0,
						'max' => 0,
						'layout' => 'table',
						'button_label' => 'Add Associate ID',
						'sub_fields' => array (
							array (
								'key' => 'field_5a60021db597d',
								'label' => 'Associate ID',
								'name' => 'associate_id',
								'type' => 'text',
								'instructions' => '',
								'required' => 0,
								'conditional_logic' => 0,
								'wrapper' => array (
									'width' => '',
									'class' => '',
									'id' => '',
								),
								'default_value' => '',
								'placeholder' => '',
								'prepend' => '',
								'append' => '',
								'maxlength' => '',
							),
						),
					),
					array (
						'key' => 'field_5a60023db597e',
						'label' => 'Amazon Associate IDs',
						'name' => 'BR_amazon_associate_ids',
						'type' => 'repeater',
						'instructions' => '(Brazil)<br />More than one associate ID can be added in AmaLinks Pro. This can be very helpful if you want to use different IDs in different locations for tracking purposes. This is not used for link localization, for localization click the "Localization" tab.',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_5a6001129b883',
									'operator' => '==',
									'value' => 'BR',
								),
							),
						),
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'collapsed' => '',
						'min' => 0,
						'max' => 0,
						'layout' => 'table',
						'button_label' => 'Add Associate ID',
						'sub_fields' => array (
							array (
								'key' => 'field_5a60023db597f',
								'label' => 'Associate ID',
								'name' => 'associate_id',
								'type' => 'text',
								'instructions' => '',
								'required' => 0,
								'conditional_logic' => 0,
								'wrapper' => array (
									'width' => '',
									'class' => '',
									'id' => '',
								),
								'default_value' => '',
								'placeholder' => '',
								'prepend' => '',
								'append' => '',
								'maxlength' => '',
							),
						),
					),
					array (
						'key' => 'field_5a600281b5980',
						'label' => 'Amazon Associate IDs',
						'name' => 'CA_amazon_associate_ids',
						'type' => 'repeater',
						'instructions' => '(Canada)<br />More than one associate ID can be added in AmaLinks Pro. This can be very helpful if you want to use different IDs in different locations for tracking purposes. This is not used for link localization, for localization click the "Localization" tab.',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_5a6001129b883',
									'operator' => '==',
									'value' => 'CA',
								),
							),
						),
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'collapsed' => '',
						'min' => 0,
						'max' => 0,
						'layout' => 'table',
						'button_label' => 'Add Associate ID',
						'sub_fields' => array (
							array (
								'key' => 'field_5a600281b5981',
								'label' => 'Associate ID',
								'name' => 'associate_id',
								'type' => 'text',
								'instructions' => '',
								'required' => 0,
								'conditional_logic' => 0,
								'wrapper' => array (
									'width' => '',
									'class' => '',
									'id' => '',
								),
								'default_value' => '',
								'placeholder' => '',
								'prepend' => '',
								'append' => '',
								'maxlength' => '',
							),
						),
					),
					array (
						'key' => 'field_5a600292b5982',
						'label' => 'Amazon Associate IDs',
						'name' => 'CN_amazon_associate_ids',
						'type' => 'repeater',
						'instructions' => '(China)<br />More than one associate ID can be added in AmaLinks Pro. This can be very helpful if you want to use different IDs in different locations for tracking purposes. This is not used for link localization, for localization click the "Localization" tab.',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_5a6001129b883',
									'operator' => '==',
									'value' => 'CN',
								),
							),
						),
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'collapsed' => '',
						'min' => 0,
						'max' => 0,
						'layout' => 'table',
						'button_label' => 'Add Associate ID',
						'sub_fields' => array (
							array (
								'key' => 'field_5a600292b5983',
								'label' => 'Associate ID',
								'name' => 'associate_id',
								'type' => 'text',
								'instructions' => '',
								'required' => 0,
								'conditional_logic' => 0,
								'wrapper' => array (
									'width' => '',
									'class' => '',
									'id' => '',
								),
								'default_value' => '',
								'placeholder' => '',
								'prepend' => '',
								'append' => '',
								'maxlength' => '',
							),
						),
					),
					array (
						'key' => 'field_5a6002d6b5984',
						'label' => 'Amazon Associate IDs',
						'name' => 'FR_amazon_associate_ids',
						'type' => 'repeater',
						'instructions' => '(France)<br />More than one associate ID can be added in AmaLinks Pro. This can be very helpful if you want to use different IDs in different locations for tracking purposes. This is not used for link localization, for localization click the "Localization" tab.',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_5a6001129b883',
									'operator' => '==',
									'value' => 'FR',
								),
							),
						),
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'collapsed' => '',
						'min' => 0,
						'max' => 0,
						'layout' => 'table',
						'button_label' => 'Add Associate ID',
						'sub_fields' => array (
							array (
								'key' => 'field_5a6002d6b5985',
								'label' => 'Associate ID',
								'name' => 'associate_id',
								'type' => 'text',
								'instructions' => '',
								'required' => 0,
								'conditional_logic' => 0,
								'wrapper' => array (
									'width' => '',
									'class' => '',
									'id' => '',
								),
								'default_value' => '',
								'placeholder' => '',
								'prepend' => '',
								'append' => '',
								'maxlength' => '',
							),
						),
					),
					array (
						'key' => 'field_5a6002f5b5986',
						'label' => 'Amazon Associate IDs',
						'name' => 'DE_amazon_associate_ids',
						'type' => 'repeater',
						'instructions' => '(Germany)<br />More than one associate ID can be added in AmaLinks Pro. This can be very helpful if you want to use different IDs in different locations for tracking purposes. This is not used for link localization, for localization click the "Localization" tab.',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_5a6001129b883',
									'operator' => '==',
									'value' => 'DE',
								),
							),
						),
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'collapsed' => '',
						'min' => 0,
						'max' => 0,
						'layout' => 'table',
						'button_label' => 'Add Associate ID',
						'sub_fields' => array (
							array (
								'key' => 'field_5a6002f5b5987',
								'label' => 'Associate ID',
								'name' => 'associate_id',
								'type' => 'text',
								'instructions' => '',
								'required' => 0,
								'conditional_logic' => 0,
								'wrapper' => array (
									'width' => '',
									'class' => '',
									'id' => '',
								),
								'default_value' => '',
								'placeholder' => '',
								'prepend' => '',
								'append' => '',
								'maxlength' => '',
							),
						),
					),
					array (
						'key' => 'field_5a600320b5988',
						'label' => 'Amazon Associate IDs',
						'name' => 'IN_amazon_associate_ids',
						'type' => 'repeater',
						'instructions' => '(India)<br />More than one associate ID can be added in AmaLinks Pro. This can be very helpful if you want to use different IDs in different locations for tracking purposes. This is not used for link localization, for localization click the "Localization" tab.',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_5a6001129b883',
									'operator' => '==',
									'value' => 'IN',
								),
							),
						),
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'collapsed' => '',
						'min' => 0,
						'max' => 0,
						'layout' => 'table',
						'button_label' => 'Add Associate ID',
						'sub_fields' => array (
							array (
								'key' => 'field_5a600320b5989',
								'label' => 'Associate ID',
								'name' => 'associate_id',
								'type' => 'text',
								'instructions' => '',
								'required' => 0,
								'conditional_logic' => 0,
								'wrapper' => array (
									'width' => '',
									'class' => '',
									'id' => '',
								),
								'default_value' => '',
								'placeholder' => '',
								'prepend' => '',
								'append' => '',
								'maxlength' => '',
							),
						),
					),
					array (
						'key' => 'field_5a6003782d260',
						'label' => 'Amazon Associate IDs',
						'name' => 'IT_amazon_associate_ids',
						'type' => 'repeater',
						'instructions' => '(Italy)<br />More than one associate ID can be added in AmaLinks Pro. This can be very helpful if you want to use different IDs in different locations for tracking purposes. This is not used for link localization, for localization click the "Localization" tab.',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_5a6001129b883',
									'operator' => '==',
									'value' => 'IT',
								),
							),
						),
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'collapsed' => '',
						'min' => 0,
						'max' => 0,
						'layout' => 'table',
						'button_label' => 'Add Associate ID',
						'sub_fields' => array (
							array (
								'key' => 'field_5a6003782d261',
								'label' => 'Associate ID',
								'name' => 'associate_id',
								'type' => 'text',
								'instructions' => '',
								'required' => 0,
								'conditional_logic' => 0,
								'wrapper' => array (
									'width' => '',
									'class' => '',
									'id' => '',
								),
								'default_value' => '',
								'placeholder' => '',
								'prepend' => '',
								'append' => '',
								'maxlength' => '',
							),
						),
					),
					array (
						'key' => 'field_5a60039d2d262',
						'label' => 'Amazon Associate IDs',
						'name' => 'JP_amazon_associate_ids',
						'type' => 'repeater',
						'instructions' => '(Japan)<br />More than one associate ID can be added in AmaLinks Pro. This can be very helpful if you want to use different IDs in different locations for tracking purposes. This is not used for link localization, for localization click the "Localization" tab.',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_5a6001129b883',
									'operator' => '==',
									'value' => 'JP',
								),
							),
						),
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'collapsed' => '',
						'min' => 0,
						'max' => 0,
						'layout' => 'table',
						'button_label' => 'Add Associate ID',
						'sub_fields' => array (
							array (
								'key' => 'field_5a60039d2d263',
								'label' => 'Associate ID',
								'name' => 'associate_id',
								'type' => 'text',
								'instructions' => '',
								'required' => 0,
								'conditional_logic' => 0,
								'wrapper' => array (
									'width' => '',
									'class' => '',
									'id' => '',
								),
								'default_value' => '',
								'placeholder' => '',
								'prepend' => '',
								'append' => '',
								'maxlength' => '',
							),
						),
					),
					array (
						'key' => 'field_5a6003b92d264',
						'label' => 'Amazon Associate IDs',
						'name' => 'MX_amazon_associate_ids',
						'type' => 'repeater',
						'instructions' => '(Mexico)<br />More than one associate ID can be added in AmaLinks Pro. This can be very helpful if you want to use different IDs in different locations for tracking purposes. This is not used for link localization, for localization click the "Localization" tab.',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_5a6001129b883',
									'operator' => '==',
									'value' => 'MX',
								),
							),
						),
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'collapsed' => '',
						'min' => 0,
						'max' => 0,
						'layout' => 'table',
						'button_label' => 'Add Associate ID',
						'sub_fields' => array (
							array (
								'key' => 'field_5a6003b92d265',
								'label' => 'Associate ID',
								'name' => 'associate_id',
								'type' => 'text',
								'instructions' => '',
								'required' => 0,
								'conditional_logic' => 0,
								'wrapper' => array (
									'width' => '',
									'class' => '',
									'id' => '',
								),
								'default_value' => '',
								'placeholder' => '',
								'prepend' => '',
								'append' => '',
								'maxlength' => '',
							),
						),
					),
					array (
						'key' => 'field_5a6003e32d266',
						'label' => 'Amazon Associate IDs',
						'name' => 'ES_amazon_associate_ids',
						'type' => 'repeater',
						'instructions' => '(Spain)<br />More than one associate ID can be added in AmaLinks Pro. This can be very helpful if you want to use different IDs in different locations for tracking purposes. This is not used for link localization, for localization click the "Localization" tab.',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_5a6001129b883',
									'operator' => '==',
									'value' => 'ES',
								),
							),
						),
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'collapsed' => '',
						'min' => 0,
						'max' => 0,
						'layout' => 'table',
						'button_label' => 'Add Associate ID',
						'sub_fields' => array (
							array (
								'key' => 'field_5a6003e32d267',
								'label' => 'Associate ID',
								'name' => 'associate_id',
								'type' => 'text',
								'instructions' => '',
								'required' => 0,
								'conditional_logic' => 0,
								'wrapper' => array (
									'width' => '',
									'class' => '',
									'id' => '',
								),
								'default_value' => '',
								'placeholder' => '',
								'prepend' => '',
								'append' => '',
								'maxlength' => '',
							),
						),
					),
					array (
						'key' => 'field_5a6009b5acae5',
						'label' => 'Test Amazon API Connection',
						'name' => '',
						'type' => 'message',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'message' => 'Click the button below to test your connection to the Amazon API. You must see a success message to use this plugin.

							<input type="button" class="js-amalinkspro-test-api button button-primary button-large" id="test-amazon-api-connection" value="Test Amazon API Connection" /> <img class="amalinkspro-apitest-loading-gif" src="/wp-admin/images/loading.gif" alt="loading" />
							<p class="amazon-api-test-message"></p>',
						'new_lines' => 'wpautop',
						'esc_html' => 0,
					),
					array (
						'key' => 'field_5a61769eaeb54',
						'label' => 'Global Link Settings',
						'name' => '',
						'type' => 'tab',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'placement' => 'top',
						'endpoint' => 0,
					),
					array (
						'key' => 'field_5a617e59aeb55',
						'label' => 'Open Links in a New Window',
						'name' => 'open_links_in_a_new_window',
						'type' => 'true_false',
						'instructions' => 'This setting can be overridden when creating links in the editor.',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'message' => 'Check this box to open all AmaLinks Pro links in a new window',
						'default_value' => 1,
						'ui' => 0,
						'ui_on_text' => '',
						'ui_off_text' => '',
					),
					array (
						'key' => 'field_5a617ea0aeb56',
						'label' => 'NoFollow Links',
						'name' => 'nofollow_links',
						'type' => 'true_false',
						'instructions' => 'This setting can be overridden when creating links in the editor.',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'message' => 'Check this box to open add a re="nofollow" tag to all AmaLinks Pro links',
						'default_value' => 1,
						'ui' => 0,
						'ui_on_text' => '',
						'ui_off_text' => '',
					),
					array (
						'key' => 'field_5a617f1db3445',
						'label' => 'Add to Cart',
						'name' => 'add_to_cart',
						'type' => 'true_false',
						'instructions' => 'By enabling this feature, all AmaLinks Pro links will become Amazon "Add to Cart" links. When a visitor adds an item to their cart after clicking your link, you get an extra 89 day cookie set on the visitor\'s browser, giving you 3 extra months to get the commission.',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'message' => 'I want AmalInks Pro to have my links add a product to a visitor\'s Amazon cart',
						'default_value' => 0,
						'ui' => 0,
						'ui_on_text' => '',
						'ui_off_text' => '',
					),
					array (
						'key' => 'field_5a6219c4c40e3',
						'label' => 'Localization',
						'name' => '',
						'type' => 'tab',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'placement' => 'top',
						'endpoint' => 0,
					),
					array (
						'key' => 'field_5a6219d5c40e4',
						'label' => 'Amazon OneLink',
						'name' => 'amazon_onelink_script',
						'type' => 'textarea',
						'instructions' => 'AmaLinks Pro uses Amazon\'s own link localization script to keep you in compliance and earning as much as you can from your website. Enter your Amazon OneLink script tag here. This will work on all Amazon links in your website, so if you already have your OneLink script installed elsewhere, you can ignore this setting.',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'default_value' => '',
						'placeholder' => 'Enter your Amazon OneLink Code here',
						'maxlength' => '',
						'rows' => '',
						'new_lines' => '',
					),
					

					array (
						'key' => 'field_5a61769eaeb5433',
						'label' => 'Styles',
						'name' => '',
						'type' => 'tab',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'placement' => 'top',
						'endpoint' => 0,
					),
					array (
						'key' => 'field_yg76vt87ibyi7vt8b7iyty',
						'label' => 'Google Fonts API Key',
						'name' => 'google_fonts_api_key',
						'type' => 'text',
						'instructions' => 'In order to use Google Fonts with AmaLinks Pro, you must get a free API key and save it here. To obtain your Google FInts API Key, visit <a href="https://developers.google.com/fonts/docs/developer_api#identifying_your_application_to_google" target="_blank">this page</a>. You must be signed in to Google to get your API key. Once you get it from Google, paste it here and save your options.',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'maxlength' => '',
					),
					array (
						'key' => 'field_5a6004498b936',
						'label' => 'Help',
						'name' => '',
						'type' => 'tab',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'placement' => 'top',
						'endpoint' => 0,
					),
					array (
						'key' => 'field_5a6004998b937',
						'label' => 'Support & Documentation',
						'name' => '',
						'type' => 'message',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'message' => 'To view the AmaLinks Pro documentation, please visit <a href="https://amalinkspro.com/my-account/" target="_blank">https://amalinkspro.com/my-account/</a>.

			For support, please visit <a href="https://amalinkspro.com/my-account/" target="_blank">https://amalinkspro.com/my-account/</a>.

			AmaLinks Pro is developed and maintained by <a href="https://dumbpassiveincome.com" target="_blank">Matthew Allen</a> and <a href="https://alchemycoder.com" target="_blank">Alchemy Coder</a>.',
						'new_lines' => 'br',
						'esc_html' => 0,
					),
				),
				'location' => array (
					array (
						array (
							'param' => 'options_page',
							'operator' => '==',
							'value' => 'amalinkspro-settings',
						),
					),
				),
				'menu_order' => 0,
				'position' => 'normal',
				'style' => 'default',
				'label_placement' => 'top',
				'instruction_placement' => 'label',
				'hide_on_screen' => '',
				'active' => 1,
				'description' => '',
			));

		endif;




	}





	// function for activating AmaLinks Pro themes and plugins licenses
	function activate_amalinkspro_license() {

	    global $wpdb;

	    $license        = $_POST['license'];

	    $old_license    = get_option( 'amalinkspro_license' );
	    $old_status     = get_option( 'amalinkspro_license_status' );

	    // if the license in the db is empty or matches the new valaue and is all numbers and letters
	    if ( ( $license != '' ) && ctype_alnum($license) ) {

	        // this is a fix to remove http from url to make api resonse not result in a 403 error
	        $full_home_url = get_home_url();
	        $find = array( 'http://', 'https://' );
	        $replace = '';
	        $short_home_url = str_replace( $find, $replace, $full_home_url );

	        // data to send in our API request
	        $api_params = array( 
	            'edd_action'=> 'activate_license', 
	            'license'   => $license, 
	            'item_name' => urlencode( $this->edd_product ), // the name of our product in EDD,
	            'url'       => $short_home_url
	        );
	        
	        // Call the custom API.
	        $response = wp_remote_get( add_query_arg( $api_params, 'http://amalinkspro.com' ) );

	        // make sure the response came back okay
	        if ( is_wp_error( $response ) ) {
	            $result = 'Sorry, there has been a connection error.';
	        }

	        else {

	            // decode the license data
	            $license_data = json_decode( wp_remote_retrieve_body( $response ) );

	            if ( $license_data->license == 'valid' ) {
	                // $license_data->license will be either "valid" or "invalid"
	                delete_option( 'amalinkspro_license_status' );
	                update_option( 'amalinkspro_license_status', $license_data->license );

	                if ( $license != $old_license ) {
	                    delete_option( 'amalinkspro_license' );
	                    update_option( 'amalinkspro_license', $license );
	                }
	            }
	            $result = $license_data->license;
	        }

	    }
	    else {
	        $result = 'Sorry, there has been a connection error.';
	    }

	    echo $result;
	    die(1);
	}


	// function for deactivating AmaLinks Pro themes and plugins licenses
	function deactivate_amalinkspro_license() {

	    global $wpdb;

	    $license        = $_POST['license'];

	    $old_license    = get_option( 'amalinkspro_license' );
	    $old_status     = get_option( 'amalinkspro_license_status' );

	    if ( $license == $old_license ) {

	        // this is a fix to remove http from url to make api resonse not result in a 403 error
	        $full_home_url = get_home_url();
	        $find = array( 'http://', 'https://' );
	        $replace = '';
	        $short_home_url = str_replace( $find, $replace, $full_home_url );


	        // data to send in our API request
	        $api_params = array( 
	            'edd_action'=> 'deactivate_license', 
	            'license'   => $license, 
	            'item_name' => urlencode( $this->edd_product ), // the name of our product in EDD,
	            'url'       => $short_home_url
	        );

	        
	        // Call the custom API.
	        $response = wp_remote_get( add_query_arg( $api_params, 'http://amalinkspro.com' ) );

	        // make sure the response came back okay
	        if ( is_wp_error( $response ) ) {
	            $result = 'Sorry, there has been a connection error.';
	        }

	        else {

	            // decode the license data
	            $license_data = json_decode( wp_remote_retrieve_body( $response ) );

	            // $license_data->license will be either "deactivated" or "failed"
	            if( $license_data->license == 'deactivated' ) {
	                delete_option( 'amalinkspro_license_status' );
	                update_option( 'amalinkspro_license_status', $license_data->license );
	            }

	            $result = $license_data->license;

	        }


	    }

	    else {
	        $result = 'Sorry, there has been a connection error.';
	    }

	    echo $result;
	    //str_replace("\u0022","\\\\\"",json_encode( $results,JSON_HEX_QUOT));
	    die(1);

	}



	// function for activating AmaLinks Pro themes and plugins licenses
	public function check_amalinkspro_license() {

		global $wpdb;

		$license = get_option('amalinkspro_license');

		// check if the license is completely empty, if so retun proper error message
		if ( $license == '' ) {
			$license_check_response = '<h4>You have not entered and activated your AMaLinks Pro License. Plese go <a href="'.get_admin_url().'admin.php?page=amalinkspro-license" target="_blank">here</a> to do so.</h4><p>You can find your license in your AmaLinks Pro email receipt, or by logging into <a href="https://amalinkspro.com/my-account/" target="_blank">your account</a></p>';

			return $license_check_response;
		}

		// check if license is alphanumeric, if not give proper error message
		if ( !ctype_alnum($license) ) {
			$license_check_response = '<h4>The AmaLinks Pro license you have enered is not valid. Plese go <a href="'.get_admin_url().'admin.php?page=amalinkspro-license" target="_blank">here</a> and doublecheck your license.</h4><p>You can find your license in your AmaLinks Pro email receipt, or by logging into <a href="https://amalinkspro.com/my-account/" target="_blank">your account</a></p>';

			return $license_check_response;
		}

		// our transient name
		$transient = 'amalinkspro_license_check_transient';
		// check for our transient
		$transient_check = get_transient( $transient );

		// if the transient is valid, return a valid response
		if ( $transient_check == 'amalinkspro_license_valid' ) {

			$license_check_response = 'valid-license';
			return $license_check_response;
		}

		// If transient is not set, lets set it and run our license check

		// set transient
		$value = 'amalinkspro_license_valid';
		$expiration = 60 * 60 * 24;
		set_transient( $transient, $value, $expiration );

		// this is a fix to remove http from url to make api resonse not result in a 403 error
        $full_home_url = get_home_url();
        $find = array( 'http://', 'https://' );
        $replace = '';
        $short_home_url = str_replace( $find, $replace, $full_home_url );

        // data to send in our API request
        $api_params = array( 
            'edd_action'=> 'check_license', 
            'license'   => $license, 
            'item_name' => urlencode( $this->edd_product ), // the name of our product in EDD,
            'url'       => $short_home_url
        );

        
        // Call the custom API.
        $response = wp_remote_get( add_query_arg( $api_params, 'https://amalinkspro.com' ) );

        //return $response;

        // make sure the response came back okay
        if ( is_wp_error( $response ) ) {
            $license_check_response = 'There was a problem connecting to the AmaLinks Pro API. Please try again in a few minutes. Thus is usually due to slower internet connections.';
            return $license_check_response;
        }

        if ( $response ) {

        	$response_body = json_decode( $response['body'] );

        	$license = $response_body->license;
        	$customer_name = $response_body->customer_name;
        	$customer_email = $response_body->customer_email;
        	$expires = $response_body->expires;

        	//return $expires;

        	if ( $expires ) {

        		$date = DateTime::createFromFormat('Y-m-d H:i:s', $expires);
	        	if ( $date ) {
	        		$date_formatted = $date->format('F d, Y');
	        	}
				else {
					$date_formatted = 'Unknown';
				}

        	}
        	else {
				$date_formatted = 'Unknown';
			}

			//return $date;

	        	


        	if ( $license == 'valid' ) {
        		$license_check_response = 'valid-license';
        	}
        	else if ( $license == 'expired' ) {

        		$license_check_response = '<p>Hi '.$customer_name.'. We hope you\'ve been having fun and making more money using AmaLinks Pro to insert Amazon links across your website!<p>';

        		$license_check_response .= '<h4>Unfortunately your AmaLinks Pro license expired on <span class="amalinkspro-expiration-date">'.$date_formatted.'</span>.</h4>';

        		$license_check_response .= '<p class="amalinkspro-renewal-discount">Renew your AmaLinks Pro Licence Now for 20% Off!<p>';

        		$license_check_response .= '<p>All of your links will still work, but you no longer create new links. You can renew your license by logging into your account <a href="https://amalinkspro.com/my-account/" target="_blank">here</a>.</p>';

        		

        	}

        	//return $response_body;


        	//$license_check_response = $response_body;
        }

	    return $license_check_response;


	}




	function add_amalinkspro_media_button() {
	    echo '<a id="insert-amalinkspro-media" class="button amalinkspro-insert-media-btn" title="Amalinks Pro" href="#TB_inline&inlineId=amalinkspro-media-window&width=753&height=185"><span class="dashicons dashicons-admin-links"></span> AmaLinks Pro</a>';
	}






	function amalinkspro_add_inline_popup_content() {


		echo '<div id="amalinkspro-close-modal" style="display: none;"></div>';

		$locale = trim( get_option( 'amalinkspro-options_default_amazon_search_locale' ) );
		?>

		<div id="amalinkspro-media-window" style="display: none;">

			<div class="amalinkspro-loading-overlay"><i class="icon-amalinkspro-spin3 animate-spin"></i></div>

			<div class="amalinkspro-media-window-title">
				<span class="amalinkspro-close-modal"></span>
				<h1><span class="dashicons dashicons-admin-links"></span> AmaLinks Pro</h1>
			</div>

			<div id="amalinkspro-button-generator-preview" class="amalinkspro-button-generator-preview">
				<?php 
				$user_meta = wp_get_current_user(); 
				$user_nicename = $user_meta->user_nicename;
				if ( !$user_nicename ) {
					$user_nicename = $user_meta->user_login;
				}
				?>

				<a class="amalinkspro-css-button" href="#">Hey <?php echo $user_meta->user_nicename; ?>, Here's Your Button!</a>


			</div>
		

			<div class="amalinkspro-steps-counter">
				<span class="amalinkspro-step amalinkspro-step-1 amalinkspro-step-active" data-amalinkspro-step="amalinkspro-step-1"><i class="icon-amalinkspro-search"></i> Step 1</span>
				<span class="amalinkspro-step amalinkspro-step-2 amalinkspro-step-disabled" data-amalinkspro-step="amalinkspro-step-2"><i class="icon-amalinkspro-edit"></i> Step 2</span>
				<span class="amalinkspro-step amalinkspro-step-3 amalinkspro-step-disabled" data-amalinkspro-step="amalinkspro-step-3"><i class="icon-amalinkspro-rocket"></i> Step 3</span>
				<div class="amalinkspro-clear"></div>
			</div>




			<div id="amalinkspro-media-window-content" class="amalinkspro-media-window-content">


			<div class="amalinkspro-button-generator">

				<!-- preview goes here -->


				<div class="amalinkspro-button-generator-controls">

					<div class="amalinkspro-button-generator-controls-section">
						<span class="toggle-indicator" aria-hidden="true"></span>
						<h3>Manage Buttons <i class="icon-amalinkspro-up-circle"></i></h3>

						<div class="amalinkspro-button-generator-controls-section-body">

							<ul>
								<li><a href="#">Save Current Settings</a></li>
								<li><a href="#">Load Saved Button</a></li>
							</ul>

						</div>
					</div>
				</div>


				<div class="amalinkspro-button-generator-controls">

					<div class="amalinkspro-button-generator-controls-section">
						<span class="toggle-indicator" aria-hidden="true"></span>
						<h3>Customize Text <span class="title-hover-label">(Hover State)</span> <i class="icon-amalinkspro-up-circle"></i></h3>

						<div class="amalinkspro-button-generator-controls-section-body">

							<div class="amalinkspro-button-generator-col amalinkspro-button-generator-col-50">
							
								<h4>Font Family</h4>

								<div class="system-select-wrapper">

									<select class="alp-control-font-family alp-control-font-family-system">
										<option></option>
										<option value="Helvetica">Helvetica</option>
										<option value="Arial" selected="selected">Arial</option>
										<option value="Times">Times</option>
										<option value="Times New Roman">Times New Roman</option>
										<option value="Courier">Courier</option>
										<option value="Courier New">Courier New</option>
										<option value="Verdana">Verdana</option>
										<option value="Tahoma">Tahoma</option>
									</select>

								</div>


								<div class="google-select-wrapper">

									<?php
									$amalinkspro_google_fonts = new amalinkspro_google_fonts();
									$build_google_fonts_select = $amalinkspro_google_fonts->build_google_fonts_select();

									//echo '<pre>'.print_r($build_google_fonts_select,1).'</pre>';

									echo $build_google_fonts_select;
									?>

								</div>

							</div>

							<div class="amalinkspro-button-generator-col amalinkspro-button-generator-col-50">
							
								<h4>Google Fonts</h4>
								<input name="alp-control-enable-googlefonts" class="alp-control-enable-googlefonts" type="checkbox" value="1" /> <label>Enable Google Fonts</label> <div class="amalinkspro-tooltip-wrap"><i class="amalinkspro-js-googlefont-info icon-amalinkspro-help"></i>
								<div class="amalinkspro-tooltip">To use Google Fonts in your CTA Buttons, you must have a Google Fonts API key. <a href="<?php echo admin_url( 'admin.php?page=amalinkspro-settings' ); ?>" target="_blank">Click Here</a> and view the "Styles" tab for instructions on setting up your API key. You must refresh this page after saving your API key to use Google Fonts.<span class="amalinkspro-close-tooltip"></span></div></div>

							</div>


						

							<div class="amalinkspro-button-generator-col amalinkspro-button-generator-col-clear amalinkspro-button-generator-col-50">
								<h4>Font Size</h4>
								<div class="amalinkspro-slider-wrap">
									<div class="alp-control-font-size"></div>
								</div>
							</div>


							<div class="amalinkspro-button-generator-col amalinkspro-button-generator-col-50">
							
								<h4>Font Color</h4>
								<div class="amalinkspro-slider-wrap">
									<input type="text" class="alp-control-font-color" value="#FFFFFF" />
								</div>

							</div>


							<div class="amalinkspro-button-generator-controls-section-body-bottom">


								<div class="amalinkspro-button-generator-col amalinkspro-button-generator-col-clear amalinkspro-button-generator-col-50">
								
									<h4>Text Shadow</h4>
									<label><input class="alp-control-textshadow-enable" name="alp-control-textshadow-enable" type="checkbox" value="1" checked="checked" /> Check to Enable Text Shadow</label>

								</div>


								<div class="amalinkspro-button-generator-col amalinkspro-button-generator-col-hidden amalinkspro-button-generator-col-50">
								
									<h4>Text Shadow Color</h4>
									<div class="amalinkspro-slider-wrap">
										<input type="text" class="alp-control-textshadow-color" value="#000000" />
									</div>

								</div>


								<div class="amalinkspro-button-generator-col amalinkspro-button-generator-col-hidden amalinkspro-button-generator-col-33">
								
									<h4>X Axis Offset</h4>
									<div class="amalinkspro-slider-wrap">
										<div class="alp-control-textshadow-x"></div>
									</div>

								</div>


								<div class="amalinkspro-button-generator-col amalinkspro-button-generator-col-hidden amalinkspro-button-generator-col-33">
								
									<h4>Y Axis Offset</h4>
									<div class="amalinkspro-slider-wrap">
										<div class="alp-control-textshadow-y"></div>
									</div>

								</div>


								<div class="amalinkspro-button-generator-col amalinkspro-button-generator-col-hidden amalinkspro-button-generator-col-33">
								
									<h4>Blur</h4>
									<div class="amalinkspro-slider-wrap">
										<div class="alp-control-textshadow-blur"></div>
									</div>

								</div>

								<div class="amalinkspro-clear"></div>

							</div>

							<div class="amalinkspro-button-generator-controls-section-body-hover">

								<span class="amalinkspro-hover-trigger"><i class="icon-amalinkspro-left-hand"></i></span>

								<div class="amalinkspro-button-generator-col amalinkspro-button-generator-col-clear amalinkspro-button-generator-col-50">
									<h4>Hover Styles</h4>
									<div class="amalinkspro-slider-wrap">
										&nbsp;
									</div>
								</div>

								<div class="amalinkspro-button-generator-col amalinkspro-button-generator-col-50">
									<h4>&nbsp;</h4>
									<div class="amalinkspro-slider-wrap">
										&nbsp;
									</div>
								</div>

								<div class="amalinkspro-button-generator-col amalinkspro-button-generator-col-clear amalinkspro-button-generator-col-50">
									<h4>Font Size (Hover)</h4>
									<div class="amalinkspro-slider-wrap">
										<div class="alp-control-font-size-hover"></div>
									</div>
								</div>


								<div class="amalinkspro-button-generator-col amalinkspro-button-generator-col-50">
								
									<h4>Font Color (Hover)</h4>
									<div class="amalinkspro-slider-wrap">
										<input type="text" class="alp-control-font-color-hover" value="#FFFFFF" />
									</div>

								</div>

								<div class="amalinkspro-button-generator-controls-section-body-hover-bottom">


									<div class="amalinkspro-button-generator-col amalinkspro-button-generator-col-clear amalinkspro-button-generator-col-50">
									
										<h4>&nbsp;</h4>
										&nbsp;

									</div>


									<div class="amalinkspro-button-generator-col amalinkspro-button-generator-col-hidden amalinkspro-button-generator-col-50">
									
										<h4>Text Shadow Color (Hover)</h4>
										<div class="amalinkspro-slider-wrap">
											<input type="text" class="alp-control-textshadow-color-hover" value="#000000" />
										</div>

									</div>


									<div class="amalinkspro-button-generator-col amalinkspro-button-generator-col-hidden amalinkspro-button-generator-col-33">
									
										<h4>X Axis Offset (Hover)</h4>
										<div class="amalinkspro-slider-wrap">
											<div class="alp-control-textshadow-x-hover"></div>
										</div>

									</div>


									<div class="amalinkspro-button-generator-col amalinkspro-button-generator-col-hidden amalinkspro-button-generator-col-33">
									
										<h4>Y Axis Offset (Hover)</h4>
										<div class="amalinkspro-slider-wrap">
											<div class="alp-control-textshadow-y-hover"></div>
										</div>

									</div>


									<div class="amalinkspro-button-generator-col amalinkspro-button-generator-col-hidden amalinkspro-button-generator-col-33">
									
										<h4>Blur (Hover)</h4>
										<div class="amalinkspro-slider-wrap">
											<div class="alp-control-textshadow-blur-hover"></div>
										</div>

									</div>

									<div class="amalinkspro-clear"></div>

								</div>

							</div>

							<div class="amalinkspro-clear"></div>


						</div>

					</div>







					<div class="amalinkspro-button-generator-controls-section">
						<span class="toggle-indicator" aria-hidden="true"></span>
						<h3>Customize Box <span class="title-hover-label">(Hover State)</span> <i class="icon-amalinkspro-up-circle"></i></h3>

						<div class="amalinkspro-button-generator-controls-section-body">

							<div class="amalinkspro-button-generator-col amalinkspro-button-generator-col-25">
							
								<h4>Padding Top</h4>
								<div class="amalinkspro-slider-wrap">
									<div class="alp-control-padding-top"></div>
								</div>

							</div>

							<div class="amalinkspro-button-generator-col amalinkspro-button-generator-col-25">
							
								<h4>Padding Right</h4>
								<div class="amalinkspro-slider-wrap">
									<div class="alp-control-padding-right"></div>
								</div>

							</div>

							<div class="amalinkspro-button-generator-col amalinkspro-button-generator-col-25">
							
								<h4>Padding Bottom</h4>
								<div class="amalinkspro-slider-wrap">
									<div class="alp-control-padding-bottom"></div>
								</div>

							</div>

							<div class="amalinkspro-button-generator-col amalinkspro-button-generator-col-25">
							
								<h4>Padding Left</h4>
								<div class="amalinkspro-slider-wrap">
									<div class="alp-control-padding-left"></div>
								</div>

							</div>


							<div class="amalinkspro-button-generator-controls-section-body-bottom">


								<div class="amalinkspro-button-generator-col amalinkspro-button-generator-col-clear amalinkspro-button-generator-col-50">
								
									<h4>Box Shadow</h4>
									<input class="alp-control-boxshadow-enable" name="alp-control-boxshadow-enable" type="checkbox" value="1" /> <label>Check to Enable Box Shadow</label>

								</div>


								<div class="amalinkspro-button-generator-col amalinkspro-button-generator-col-hidden amalinkspro-button-generator-col-50">
								
									<h4>Box Shadow Color</h4>
									<div class="amalinkspro-slider-wrap">
										<input type="text" class="alp-control-boxshadow-color" value="#000000" />
									</div>

								</div>


								<div class="amalinkspro-button-generator-col amalinkspro-button-generator-col-hidden amalinkspro-button-generator-col-33">
								
									<h4>X Axis Offset</h4>
									<div class="amalinkspro-slider-wrap">
										<div class="alp-control-boxshadow-x"></div>
									</div>

								</div>


								<div class="amalinkspro-button-generator-col amalinkspro-button-generator-col-hidden amalinkspro-button-generator-col-33">
								
									<h4>Y Axis Offset</h4>
									<div class="amalinkspro-slider-wrap">
										<div class="alp-control-boxshadow-y"></div>
									</div>

								</div>


								<div class="amalinkspro-button-generator-col amalinkspro-button-generator-col-hidden amalinkspro-button-generator-col-33">
								
									<h4>Blur</h4>
									<div class="amalinkspro-slider-wrap">
										<div class="alp-control-boxshadow-blur"></div>
									</div>

								</div>

								<div class="amalinkspro-clear"></div>

							</div>

							<div class="amalinkspro-button-generator-controls-section-body-hover">

								<span class="amalinkspro-hover-trigger"><i class="icon-amalinkspro-left-hand"></i></span>

								<div class="amalinkspro-clear"></div>

							</div>


						</div>

					</div>






					<div class="amalinkspro-button-generator-controls-section">

						<h3>Customize Border <span class="title-hover-label">(Hover State)</span> <i class="icon-amalinkspro-up-circle"></i></h3>

						<div class="amalinkspro-button-generator-controls-section-body">

							<div class="amalinkspro-button-generator-col amalinkspro-button-generator-col-33">
								<h4>Border Radius</h4>
								<div class="amalinkspro-slider-wrap">
									<div class="alp-control-border-radius"></div>
								</div>
							</div>

							<div class="amalinkspro-button-generator-col amalinkspro-button-generator-col-33">
								<h4>Border-Width</h4>
								<div class="amalinkspro-slider-wrap">
									<div class="alp-control-border-width"></div>
								</div>
							</div>

							<div class="amalinkspro-button-generator-col amalinkspro-button-generator-col-33">
								<h4>Border-Color</h4>
								<div class="amalinkspro-slider-wrap">
									<input type="text" class='alp-control-border-color' value="#0073aa" />
								</div>
							</div>

							<div class="amalinkspro-clear"></div>

							<div class="amalinkspro-button-generator-controls-section-body-hover">

								<span class="amalinkspro-hover-trigger"><i class="icon-amalinkspro-left-hand"></i></span>

								<div class="amalinkspro-clear"></div>

							</div>

						</div>




					</div>








					<div class="amalinkspro-button-generator-controls-section">
						<span class="toggle-indicator" aria-hidden="true"></span>
						<h3>Customize Background <span class="title-hover-label">(Hover State)</span> <i class="icon-amalinkspro-up-circle"></i></h3>

						<div class="amalinkspro-button-generator-controls-section-body">

							<div class="amalinkspro-button-generator-col amalinkspro-button-generator-col-33">
							
								<h4>Background Type</h4>
								<label><input name="alp-control-background-type" class='alp-control-background-type' type="radio" value="bg-solid" /> Solid </label> <label><input name="alp-control-background-type" class='alp-control-background-type' type="radio" value="bg-gradient" checked="checked" /> Gradient</label>

							</div>

							<div class="amalinkspro-button-generator-col amalinkspro-button-generator-col-33">
								<h4>Solid / Bottom Color</h4>
								<div class="amalinkspro-slider-wrap">
									<input type="text" class='alp-control-bg-solid' value="#1E5799" />
								</div>
							</div>

							<div class="amalinkspro-button-generator-col amalinkspro-button-generator-col-bg-top  amalinkspro-button-generator-col-33">
								<h4>Top Color</h4>
								<div class="amalinkspro-slider-wrap">
									<input type="text" class='alp-control-bg-top' value="#7db9e8" />
								</div>
							</div>

							<div class="amalinkspro-clear"></div>

							<div class="amalinkspro-button-generator-controls-section-body-hover">

								<span class="amalinkspro-hover-trigger"><i class="icon-amalinkspro-left-hand"></i></span>

								<div class="amalinkspro-clear"></div>

							</div>


						</div>

					</div>








				</div>

			</div>



				<div id="amalinkspro-step-1" class="amalinkspro-step-wrap amalinkspro-step-wrap-active">


					<?php 
					$license_check = $this->check_amalinkspro_license(); 
					?>

					<?php if ( $license_check == 'valid-license' ) : ?>

				    	<form class="amalinkspro-popup-form">
				    		<h2>Find Products by Keyword or ASIN</h2>
				    		<p>
				    			<input id="amalinkspro-search-keyword" name="amalinkspro-search-keyword" value="" type="text" placeholder="Enter your search term or ASIN here" />
				    			<input id="amalinkspro-search-locale" type="hidden" name="amalinkspro-search-locale" value="<?php echo $locale; ?>">
				    			<a class="button button-primary button-large js-amalinkspro-mediamodal-search amalinkspro-next-action">Search</a>
				    			<img class="amalinkspro-search-loading-gif" src="/wp-admin/images/loading.gif" alt="loading" />
				    		</p>
				    			
				    	</form>

				    	<div class="amalinkspro-search-results">

				    		<h2>Your Amazon Search Results</h2>

				    		<div class="amalinkspro-search-pagination">
								<span class="amalinkspro-search-page current" data-api-page="1" data-api-term="guitar" data-api-locale="US">Page 1</span>
								<span class="amalinkspro-search-page" data-api-page="2" data-api-term="guitar" data-api-locale="US">2</span>
								<span class="amalinkspro-search-page" data-api-page="3" data-api-term="guitar" data-api-locale="US">3</span>
								<span class="amalinkspro-search-page" data-api-page="4" data-api-term="guitar" data-api-locale="US">4</span>
								<span class="amalinkspro-search-page" data-api-page="5" data-api-term="guitar" data-api-locale="US">5</span>
							</div>

				    		<div class="amalinkspro-search-results-list"></div>

				    		<div class="amalinkspro-search-pagination">
								<span class="amalinkspro-search-page current" data-api-page="1" data-api-term="guitar" data-api-locale="US">Page 1</span>
								<span class="amalinkspro-search-page" data-api-page="2" data-api-term="guitar" data-api-locale="US">2</span>
								<span class="amalinkspro-search-page" data-api-page="3" data-api-term="guitar" data-api-locale="US">3</span>
								<span class="amalinkspro-search-page" data-api-page="4" data-api-term="guitar" data-api-locale="US">4</span>
								<span class="amalinkspro-search-page" data-api-page="5" data-api-term="guitar" data-api-locale="US">5</span>
							</div>

				    		
				    	</div>

				    <?php else : ?>

				    	<div class="amalinkspro-invalid-license-box">
				    		<?php echo $license_check; ?>
				    	</div>

				    <?php endif; ?>

			    </div>



			    <div id="amalinkspro-step-2" class="amalinkspro-step-wrap">


			    	<div class="amalinkspro-chosen-product"
			    	 data-amalinkspro-apidata-name=""
			    	 data-amalinkspro-apidata-asin=""
			    	 data-amalinkspro-apidata-brand=""
			    	 data-amalinkspro-apidata-model=""
			    	 data-amalinkspro-apidata-upc=""
			    	 data-amalinkspro-apidata-warranty=""
			    	 data-amalinkspro-apidata-lowestnewprice=""
			    	 data-amalinkspro-apidata-lowestusedprice=""
			    	 data-amalinkspro-apidata-listprice=""
			    	 data-amalinkspro-apidata-bestoffer=""
			    	 data-amalinkspro-apidata-percentageoff=""
			    	 data-amalinkspro-apidata-amountoff=""
			    	>

			    		<h2><a class="amalinkspro-chosen-item-previewlink" href="" target="_blank"><i class="icon-amalinkspro-link"></i> view on Amazon</a>Your Chosen Item</h2>
		    			
		    			<h3></h3>

		    			<img class="amalinkspro-chosen-product-main-img" src="" alt="" />

		    			<div class="amalinkspro-chosen-product-stats-col">
							<p class="amalinkspro-chosen-product-lowestnewprice"><strong>Lowest New Price:</strong> <span class=""><img class="amalinkspro-license-loading-gif" src="/wp-admin/images/loading.gif" alt="loading" /></span></p>
							<p class="amalinkspro-chosen-product-lowestusedprice"><strong>Lowest Used Price:</strong> <span class=""><img class="amalinkspro-license-loading-gif" src="/wp-admin/images/loading.gif" alt="loading" /></span></p>
							<p class="amalinkspro-chosen-product-listprice"><strong>List Price:</strong> <span class=""><img class="amalinkspro-license-loading-gif" src="/wp-admin/images/loading.gif" alt="loading" /></span></p>
							<p class="amalinkspro-chosen-product-bestoffer"><strong>Best Offer:</strong> <span class=""><img class="amalinkspro-license-loading-gif" src="/wp-admin/images/loading.gif" alt="loading" /></span></p>
							<p class="amalinkspro-chosen-product-save"><strong>Save: </strong> <span class=""><img class="amalinkspro-license-loading-gif" src="/wp-admin/images/loading.gif" alt="loading" /></span></p>
						</div>

		    			<div class="amalinkspro-chosen-product-stats-col">
			    			<p class="amalinkspro-chosen-product-asin"><strong>ASIN:</strong> <span class=""><img class="amalinkspro-license-loading-gif" src="/wp-admin/images/loading.gif" alt="loading" /></span></p>
							<p class="amalinkspro-chosen-product-brand"><strong>Brand:</strong> <span class=""><img class="amalinkspro-license-loading-gif" src="/wp-admin/images/loading.gif" alt="loading" /></span></p>
							<p class="amalinkspro-chosen-product-model"><strong>Model:</strong> <span class=""><img class="amalinkspro-license-loading-gif" src="/wp-admin/images/loading.gif" alt="loading" /></span></p>
							<p class="amalinkspro-chosen-product-upc"><strong>UPC:</strong> <span class=""><img class="amalinkspro-license-loading-gif" src="/wp-admin/images/loading.gif" alt="loading" /></span></p>
							<p class="amalinkspro-chosen-product-warranty"><strong>Warranty: </strong> <span class=""><img class="amalinkspro-license-loading-gif" src="/wp-admin/images/loading.gif" alt="loading" /></span></p>
						</div>

						<div class="amalinkspro-clear"></div>

		    		</div>



		    		

		    		<div class="amalinkspro-choose-link-type-wrap">

		    			<h2 class="amalinkspro-choose-link-type-heading">Choose Link Type</h2>

			    		<a href="#" class="js-amalinkspro-choose-link-type" data-link-type="text-link"><i class="icon-amalinkspro-text-link"></i> Text Link</a>
			    		<a href="#" class="js-amalinkspro-choose-link-type" data-link-type="image-link"><i class="icon-amalinkspro-image-link"></i> Image Link</a>
			    		<a href="#" class="js-amalinkspro-choose-link-type" data-link-type="cta-link"><i class="icon-amalinkspro-cta-link"></i> CTA Link</a>
			    		<a href="#" class="js-amalinkspro-choose-link-type" data-link-type="info-block"><i class="icon-amalinkspro-info-block"></i> Info Block</a>
			    	</div>


			    	<div class="amalinkspro-clear"></div>



		    		<div class="amalinkspro-linktype-preview amalinkspro-linktype-preview-text-link"
		    			data-amalinkspro-linktype="text-link" 
		    			data-amalinkspro-linktext="" 
		    			data-amalinkspro-asin="">

		    			<h3>Final Text Link Preview <i class="js-amalinkspro-edit-link amalinkspro-edit-link icon-amalinkspro-edit"></i></h3>

		    			<p class="amalinkspro-text-link-preview-p">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.  <a href="#" target="_blank"></a> Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
		    			</p>

		    			<div class="amalinkspro-linktype-preview-edit amalinkspro-linktype-preview-text-link-edit">

			    			<h3>Edit your Link Text</h3>

			    			<div class="amalinkspro-clear"></div>

			    			<input type="text" name="amalinkspro-edit-textlink-text" value="" />

			    		</div>

		    		</div>




		    		<div class="amalinkspro-linktype-preview amalinkspro-linktype-preview-image-link"
		    			data-amalinkspro-linktype="image-link" 
		    			data-amalinkspro-linkimage="" 
		    			data-amalinkspro-asin="">

		    			<h3>Final Image Link Preview <i class="js-amalinkspro-edit-link amalinkspro-edit-link icon-amalinkspro-edit"></i></h3>

		    			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>

		    			<a class="amalinkspro-image-link-link" href="#" target="_blank">
		    				<img class="amalinkspro-image-link-img" style="float: left;margin: 0 15px 15px 0;" src="" alt="" />
		    			</a>

		    			<p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>

		    			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>

		    			<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>

		    			<div class="amalinkspro-clear"></div>

		    			<div class="amalinkspro-linktype-preview-edit amalinkspro-linktype-preview-image-link-edit">

		    				<h3>These are the available images on Amazon for this item</h3>
		    				
		    				<div class="amalinkspro-clear"></div>

		    				<div class="amalinkspro-linktype-preview-image-link-edit-imagechoices">
		    					
		    				</div>

			    				<!-- <div class="amalinkspro-choose-api-image amalinkspro-chosen-api-img">
				    				<img src="https://images-na.ssl-images-amazon.com/images/I/51WZPRhFECL._SL110_.jpg" alt="" />
				    				<a class="amalinkspro-choose-api-image-cover js-amalinkspro-select-image" href="#"></a>
				    			</div>
			    			 -->

			    			<div class="amalinkspro-clear"></div>

		    			</div>


		    		</div>

		    

		    		<div class="amalinkspro-linktype-preview amalinkspro-linktype-preview-cta-link"	
		    			data-amalinkspro-linktype="cta-link" 
		    			data-amalinkspro-ctatext="" 
		    			data-amalinkspro-ctaimage="" 
		    			data-amalinkspro-ctastyle="cta-btn-text" 
		    			data-amalinkspro-ctaclass="amalinkspro-cta-button-plain-small amalinkspro-green" 
		    			data-amalinkspro-asin="">

		    			<h3>Final CTA Link Preview <i class="js-amalinkspro-edit-link amalinkspro-edit-link icon-amalinkspro-edit"></i></h3>

			    		<div class="amalinkspro-cta-link-preview-p">

			    			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
			    			
			    			<div class="amalinkspro-cta-wrap-preview amalinkspro-cta-wrap amalinkspro-cta-button-plain-small amalinkspro-green">
			    				<a class="amalinkspro-cta-link-link" href="#" target="_blank"></a> 
			    			</div>

			    			<p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>

		    			</div>

		    			<div class="amalinkspro-linktype-preview-edit amalinkspro-linktype-preview-cta-link-edit">

		    				<h3>Edit Your CTA Button Text</h3>
			    			<div class="amalinkspro-clear"></div>
			    			<input type="text" name="amalinkspro-edit-ctalink-text" value="" />

		    				<h3>Click to Choose One of the Available CTA Buttons</h3>
		    				<div class="amalinkspro-clear"></div>

		    				<div class="amalinkspro-cta-button-choices">

			    				

			    				<div class="amalinkspro-cta-wrap amalinkspro-cta-button-plain-small amalinkspro-green">
				    				<a class="js-select-cta-style amalinkspro-cta-link-link" target="_blank" data-amalinkspro-cta-class="amalinkspro-cta-button-plain-small amalinkspro-green"></a> 
				    			</div>

				    			<div class="amalinkspro-cta-wrap amalinkspro-cta-button-plain-small amalinkspro-red">
				    				<a class="js-select-cta-style amalinkspro-cta-link-link" target="_blank" data-amalinkspro-cta-class="amalinkspro-cta-button-plain-small amalinkspro-red"></a> 
				    			</div>




				    			

				    			<div class="amalinkspro-cta-wrap amalinkspro-cta-button-img">
				    				<a class="js-select-cta-style amalinkspro-cta-image-link" target="_blank" data-amalinkspro-cta-class="amalinkspro-cta-button-img"><img src="<?php echo plugin_dir_url( __DIR__ ) . 'includes/images/cta-buttons/buy1.gif'; ?>" alt="" /></a> 
				    			</div>


								<div class="amalinkspro-cta-wrap amalinkspro-cta-button-img">
				    				<a class="js-select-cta-style amalinkspro-cta-image-link" target="_blank" data-amalinkspro-cta-class="amalinkspro-cta-button-img"><img src="<?php echo plugin_dir_url( __DIR__ ) . 'includes/images/cta-buttons/buy2.gif'; ?>" alt="" /></a> 
				    			</div>


								<div class="amalinkspro-cta-wrap amalinkspro-cta-button-img">
				    				<a class="js-select-cta-style amalinkspro-cta-image-link" target="_blank" data-amalinkspro-cta-class="amalinkspro-cta-button-img"><img src="<?php echo plugin_dir_url( __DIR__ ) . 'includes/images/cta-buttons/buy3.gif'; ?>" alt="" /></a> 
				    			</div>


								<div class="amalinkspro-cta-wrap amalinkspro-cta-button-img">
				    				<a class="js-select-cta-style amalinkspro-cta-image-link" target="_blank" data-amalinkspro-cta-class="amalinkspro-cta-button-img"><img src="<?php echo plugin_dir_url( __DIR__ ) . 'includes/images/cta-buttons/buy4.gif'; ?>" alt="" /></a> 
				    			</div>


								<div class="amalinkspro-cta-wrap amalinkspro-cta-button-img">
				    				<a class="js-select-cta-style amalinkspro-cta-image-link" target="_blank" data-amalinkspro-cta-class="amalinkspro-cta-button-img"><img src="<?php echo plugin_dir_url( __DIR__ ) . 'includes/images/cta-buttons/buy5.gif'; ?>" alt="" /></a> 
				    			</div>

				    			<div class="amalinkspro-cta-wrap amalinkspro-cta-button-img">
				    				<a class="js-select-cta-style amalinkspro-cta-image-link" target="_blank" data-amalinkspro-cta-class="amalinkspro-cta-button-img"><img src="<?php echo plugin_dir_url( __DIR__ ) . 'includes/images/cta-buttons/buy6.gif'; ?>" alt="" /></a> 
				    			</div>









				    			
			    			</div>

		    				

			    		</div>


		    		</div>

			    		


		    		<div class="amalinkspro-linktype-preview amalinkspro-linktype-preview-info-block">

		    			<h3>Configure your Info Box <i class="js-amalinkspro-edit-link amalinkspro-edit-link icon-amalinkspro-edit edit-box-open"></i></h3>

		    			


		    			<div class="amalinkspro-linktype-preview-edit amalinkspro-linktype-preview-info-block-edit" style="display:block;">

		    				<div class="amalinkspro-info-box-builder">

			    				<div class="amalinkspro-info-box-builder-options">

			    					<h4>Choose Info Box Layout</h4>

			    					<div class="amalinkspro-infobox-layout-options">



			    						
			    						<div class="amalinkspro-infobox-option infobox-layout-1 js-amalinkspro-infobox-option" data-amalinkspro-infobox-type="infobox-layout-1">
			    							<img src="<?php echo  plugin_dir_url( 'amalinkspro.php' ); ?>amalinkspro/includes/images/amalinkspro-box-layout1.png" alt="" />
			    							<a href="" class="amalinkspro-infobox-option-cover"></a>
			    						</div>




			    						<div class="amalinkspro-infobox-option infobox-layout-2 js-amalinkspro-infobox-option" data-amalinkspro-infobox-type="infobox-layout-2">
			    							<img src="<?php echo  plugin_dir_url( 'amalinkspro.php' ); ?>amalinkspro/includes/images/amalinkspro-box-layout2.png" alt="" />
			    							<a href="" class="amalinkspro-infobox-option-cover"></a>
			    						</div>





			    						<div class="amalinkspro-infobox-option infobox-layout-3 js-amalinkspro-infobox-option" data-amalinkspro-infobox-type="infobox-layout-3">
			    							<img src="<?php echo  plugin_dir_url( 'amalinkspro.php' ); ?>amalinkspro/includes/images/amalinkspro-box-layout3.png" alt="" />
			    							<a href="" class="amalinkspro-infobox-option-cover"></a>
			    						</div>



			    						<div class="amalinkspro-infobox-option infobox-layout-4 js-amalinkspro-infobox-option" data-amalinkspro-infobox-type="infobox-layout-4">
			    							<img src="<?php echo  plugin_dir_url( 'amalinkspro.php' ); ?>amalinkspro/includes/images/amalinkspro-box-layout3.png" alt="" />
			    							<a href="" class="amalinkspro-infobox-option-cover"></a>
			    						</div>


			    						<div class="amalinkspro-infobox-option infobox-layout-5 js-amalinkspro-infobox-option" data-amalinkspro-infobox-type="infobox-layout-5">
			    							<img src="<?php echo  plugin_dir_url( 'amalinkspro.php' ); ?>amalinkspro/includes/images/amalinkspro-box-layout5.png" alt="" />
			    							<a href="" class="amalinkspro-infobox-option-cover"></a>
			    						</div>




			    						<div class="amalinkspro-clear"></div>



			    						<div class="amalinkspro-infobox-option amalinkspro-infobox-option-wide infobox-layout-5 js-amalinkspro-infobox-option" data-amalinkspro-infobox-type="infobox-layout-5">

			    							<a href="" class="amalinkspro-infobox-option-cover"></a>

			    							<div class="amalinkspro-row amalinkspro-row-header amalinkspro-row-align-left">
				    							<div class="amalinkspro-column amalinkspro-column-12">
				    								<i class="icon-amalinkspro-image-link"></i>
				    								<span class="amalinkspro-item-title">PRODUCT TITLE</span>
				    								<span class="amalinkspro-item-price">$$$</span>
				    							</div>
			    							</div>

			    							<div class="amalinkspro-row amalinkspro-row-align-left">
				    							<div class="amalinkspro-column amalinkspro-column-6">
				    								<span class="amalinkspro-column-data">xxxxxxxxxx</span>
				    							</div>
				    							<div class="amalinkspro-column amalinkspro-column-6">
				    								<span class="amalinkspro-column-data">xxxxxxxxxx</span>
				    							</div>
			    							</div>

			    							<div class="amalinkspro-row amalinkspro-row-align-left">
				    							<div class="amalinkspro-column amalinkspro-column-6">
				    								<span class="amalinkspro-column-data">xxxxxxxxxx</span>
				    							</div>
				    							<div class="amalinkspro-column amalinkspro-column-6">
				    								<span class="amalinkspro-column-data">xxxxxxxxxx</span>
				    							</div>
			    							</div>

			    							<div class="amalinkspro-row amalinkspro-row-align-left">
				    							<div class="amalinkspro-column amalinkspro-column-6">
				    								<span class="amalinkspro-column-data">xxxxxxxxxx</span>
				    							</div>
				    							<div class="amalinkspro-column amalinkspro-column-6">
				    								<span class="amalinkspro-column-data">xxxxxxxxxx</span>
				    							</div>
			    							</div>

			    							<div class="amalinkspro-row amalinkspro-row-align-left">
				    							<div class="amalinkspro-column amalinkspro-column-6">
				    								<span class="amalinkspro-column-data">xxxxxxxxxx</span>
				    							</div>
				    							<div class="amalinkspro-column amalinkspro-column-6">
				    								<span class="amalinkspro-column-data">xxxxxxxxxx</span>
				    							</div>
			    							</div>

			    						</div>



			    						<div class="amalinkspro-infobox-option amalinkspro-infobox-option-wide infobox-layout-6 js-amalinkspro-infobox-option" data-amalinkspro-infobox-type="infobox-layout-6">

			    							<a href="" class="amalinkspro-infobox-option-cover"></a>

			    							<div class="amalinkspro-row amalinkspro-row-header amalinkspro-row-align-left">
				    							<div class="amalinkspro-column amalinkspro-column-12">
				    								<i class="icon-amalinkspro-image-link"></i>
				    								<span class="amalinkspro-item-title">PRODUCT TITLE</span>
				    								<span class="amalinkspro-item-price">$$$</span>
				    							</div>
			    							</div>

			    							<div class="amalinkspro-row amalinkspro-row-align-left">
				    							<div class="amalinkspro-column amalinkspro-column-4">
				    								<span class="amalinkspro-column-data">xxxxxxxxxx</span>
				    							</div>
				    							<div class="amalinkspro-column amalinkspro-column-4">
				    								<span class="amalinkspro-column-data">xxxxxxxxxx</span>
				    							</div>
				    							<div class="amalinkspro-column amalinkspro-column-4">
				    								<span class="amalinkspro-column-data">xxxxxxxxxx</span>
				    							</div>
			    							</div>

			    							<div class="amalinkspro-row amalinkspro-row-align-left">
				    							<div class="amalinkspro-column amalinkspro-column-4">
				    								<span class="amalinkspro-column-data">xxxxxxxxxx</span>
				    							</div>
				    							<div class="amalinkspro-column amalinkspro-column-4">
				    								<span class="amalinkspro-column-data">xxxxxxxxxx</span>
				    							</div>
				    							<div class="amalinkspro-column amalinkspro-column-4">
				    								<span class="amalinkspro-column-data">xxxxxxxxxx</span>
				    							</div>
			    							</div>

			    							<div class="amalinkspro-row amalinkspro-row-align-left">
				    							<div class="amalinkspro-column amalinkspro-column-4">
				    								<span class="amalinkspro-column-data">xxxxxxxxxx</span>
				    							</div>
				    							<div class="amalinkspro-column amalinkspro-column-4">
				    								<span class="amalinkspro-column-data">xxxxxxxxxx</span>
				    							</div>
				    							<div class="amalinkspro-column amalinkspro-column-4">
				    								<span class="amalinkspro-column-data">xxxxxxxxxx</span>
				    							</div>
			    							</div>

			    							<div class="amalinkspro-row amalinkspro-row-align-left">
				    							<div class="amalinkspro-column amalinkspro-column-4">
				    								<span class="amalinkspro-column-data">xxxxxxxxxx</span>
				    							</div>
				    							<div class="amalinkspro-column amalinkspro-column-4">
				    								<span class="amalinkspro-column-data">xxxxxxxxxx</span>
				    							</div>
				    							<div class="amalinkspro-column amalinkspro-column-4">
				    								<span class="amalinkspro-column-data">xxxxxxxxxx</span>
				    							</div>
			    							</div>

			    						</div>



			    						<div class="amalinkspro-clear"></div>

			    					</div>






			    					<div class="amalinkspro-infobox-layout-data-infobox">

			    						<h4>Choose Info Box Data - Drag-n-Drop options into the Info Block below.</h4>

			    						<div class="amalinkspro-infobox-layout-data-options">

			    							<ul>
			    								<li class="amalinkspro-draggable-data" data-amalinkspro-datapoint="title" data-amalinkspro-data="">Title</li>
			    								<li class="amalinkspro-draggable-data" data-amalinkspro-datapoint="prime" data-amalinkspro-data="">Prime</li>
			    								<li class="amalinkspro-draggable-data" data-amalinkspro-datapoint="brand" data-amalinkspro-data="">Brand</li>
			    								<li class="amalinkspro-draggable-data" data-amalinkspro-datapoint="model" data-amalinkspro-data="">Model</li>
			    								<li class="amalinkspro-draggable-data" data-amalinkspro-datapoint="image" data-amalinkspro-data="">Image</li>
			    								<li class="amalinkspro-draggable-data" data-amalinkspro-datapoint="features" data-amalinkspro-data="">Features</li>
			    								<li class="amalinkspro-draggable-data" data-amalinkspro-datapoint="lowest-new-price" data-amalinkspro-data="">Lowest New Price</li>
			    								<li class="amalinkspro-draggable-data" data-amalinkspro-datapoint="lowest-used-price" data-amalinkspro-data="">Lowest Used Price</li>
			    							</ul>

			    						</div>

			    						<div class="amalinkspro-infobox-layout-boxes-empty">
			    						
				    						<div class="amalinkspro-infobox-option infobox-layout-1 js-amalinkspro-infobox-option" id="infobox-layout-1">

				    							<div class="amalinkspro-row amalinkspro-row-header amalinkspro-row-align-center">
					    							<div class="amalinkspro-column amalinkspro-column-12">
					    								
					    								<div class="amalinkspro-column-data amalinkspro-drop-zone">
					    									<i class="icon-amalinkspro-edit"></i>
					    									<img class="amalinkspro-image-link" src="" alt="" />
					    								</div>
					    								<div class="amalinkspro-edit-data"></div>
					    								
					    								<div class="amalinkspro-column-data amalinkspro-drop-zone">
					    									<i class="icon-amalinkspro-edit"></i>
					    									<span class="amalinkspro-item-label">Label:</span>
					    									<span class="amalinkspro-item-title amalinkspro-item-drop">PRODUCT TITLE</span>
					    								</div>
					    								<div class="amalinkspro-edit-data"></div>
					    							</div>
				    							</div>

				    							<div class="amalinkspro-row amalinkspro-row-align-center">
					    							<div class="amalinkspro-column amalinkspro-column-12">
					    								<div class="amalinkspro-column-data amalinkspro-drop-zone">
					    									<i class="icon-amalinkspro-edit"></i>
					    									<span class="amalinkspro-item-label">Label:</span>
					    									<span class="amalinkspro-item-drop">(drop here)</span>
					    								</div>
					    								<div class="amalinkspro-edit-data"></div>
					    							</div>
				    							</div>

				    							<div class="amalinkspro-row amalinkspro-row-align-center">
					    							<div class="amalinkspro-column amalinkspro-column-12">
					    								<div class="amalinkspro-column-data amalinkspro-drop-zone">
					    									<i class="icon-amalinkspro-edit"></i>
					    									<span class="amalinkspro-item-label">Label:</span>
					    									<span class="amalinkspro-item-drop">(drop here)</span>
					    								</div>
					    								<div class="amalinkspro-edit-data"></div>
					    							</div>
				    							</div>

				    							<div class="amalinkspro-row amalinkspro-row-align-center">
					    							<div class="amalinkspro-column amalinkspro-column-12">
					    								<div class="amalinkspro-column-data amalinkspro-drop-zone">
					    									<i class="icon-amalinkspro-edit"></i>
					    									<span class="amalinkspro-item-label">Label:</span>
					    									<span class="amalinkspro-item-drop">(drop here)</span>
					    								</div>
					    								<div class="amalinkspro-edit-data"></div>
					    							</div>
				    							</div>

				    							<div class="amalinkspro-row amalinkspro-row-align-center">
					    							<div class="amalinkspro-column amalinkspro-column-12">
					    								<div class="amalinkspro-column-data amalinkspro-drop-zone">
					    									<i class="icon-amalinkspro-edit"></i>
					    									<span class="amalinkspro-item-label">Label:</span>
					    									<span class="amalinkspro-item-drop">(drop here)</span>
					    								</div>
					    								<div class="amalinkspro-edit-data"></div>
					    							</div>
				    							</div>

				    						</div>




				    						<div class="amalinkspro-infobox-option infobox-layout-2 js-amalinkspro-infobox-option" id="infobox-layout-2">

				    							<div class="amalinkspro-row amalinkspro-row-header amalinkspro-row-align-center">
					    							<div class="amalinkspro-column amalinkspro-column-12">
					    								
					    								<div class="amalinkspro-column-data amalinkspro-drop-zone">
					    									<i class="icon-amalinkspro-edit"></i>
					    									<img class="amalinkspro-image-link" src="" alt="" />
					    								</div>
					    								<div class="amalinkspro-edit-data"></div>
					    								
					    								<div class="amalinkspro-column-data amalinkspro-drop-zone">
					    									<i class="icon-amalinkspro-edit"></i>
					    									<span class="amalinkspro-item-label">Label:</span>
					    									<span class="amalinkspro-item-title amalinkspro-item-drop">PRODUCT TITLE</span>
					    								</div>
					    								<div class="amalinkspro-edit-data"></div>
					    							</div>
				    							</div>

				    							<div class="amalinkspro-row amalinkspro-row-align-center">
					    							<div class="amalinkspro-column amalinkspro-column-6">
					    								<div class="amalinkspro-column-data amalinkspro-drop-zone">
					    									<i class="icon-amalinkspro-edit"></i>
					    									<span class="amalinkspro-item-label">Label:</span>
					    									<span class="amalinkspro-item-drop">(drop here)</span>
					    								</div>
					    								<div class="amalinkspro-edit-data"></div>
					    							</div>
					    							<div class="amalinkspro-column amalinkspro-column-6">
					    								<div class="amalinkspro-column-data amalinkspro-drop-zone">
					    									<i class="icon-amalinkspro-edit"></i>
					    									<span class="amalinkspro-item-label">Label:</span>
					    									<span class="amalinkspro-item-drop">(drop here)</span>
					    								</div>
					    								<div class="amalinkspro-edit-data"></div>
					    							</div>
				    							</div>

				    							<div class="amalinkspro-row amalinkspro-row-align-center">
					    							<div class="amalinkspro-column amalinkspro-column-6">
					    								<div class="amalinkspro-column-data amalinkspro-drop-zone">
					    									<i class="icon-amalinkspro-edit"></i>
					    									<span class="amalinkspro-item-label">Label:</span>
					    									<span class="amalinkspro-item-drop">(drop here)</span>
					    								</div>
					    								<div class="amalinkspro-edit-data"></div>
					    							</div>
					    							<div class="amalinkspro-column amalinkspro-column-6">
					    								<div class="amalinkspro-column-data amalinkspro-drop-zone">
					    									<i class="icon-amalinkspro-edit"></i>
					    									<span class="amalinkspro-item-label">Label:</span>
					    									<span class="amalinkspro-item-drop">(drop here)</span>
					    								</div>
					    								<div class="amalinkspro-edit-data"></div>
					    							</div>
				    							</div>

				    							<div class="amalinkspro-row amalinkspro-row-align-center">
					    							<div class="amalinkspro-column amalinkspro-column-6">
					    								<div class="amalinkspro-column-data amalinkspro-drop-zone">
					    									<i class="icon-amalinkspro-edit"></i>
					    									<span class="amalinkspro-item-label">Label:</span>
					    									<span class="amalinkspro-item-drop">(drop here)</span>
					    								</div>
					    								<div class="amalinkspro-edit-data"></div>
					    							</div>
					    							<div class="amalinkspro-column amalinkspro-column-6">
					    								<div class="amalinkspro-column-data amalinkspro-drop-zone">
					    									<i class="icon-amalinkspro-edit"></i>
					    									<span class="amalinkspro-item-label">Label:</span>
					    									<span class="amalinkspro-item-drop">(drop here)</span>
					    								</div>
					    								<div class="amalinkspro-edit-data"></div>
					    							</div>
				    							</div>

				    							<div class="amalinkspro-row amalinkspro-row-align-center">
					    							<div class="amalinkspro-column amalinkspro-column-6">
					    								<div class="amalinkspro-column-data amalinkspro-drop-zone">
					    									<i class="icon-amalinkspro-edit"></i>
					    									<span class="amalinkspro-item-label">Label:</span>
					    									<span class="amalinkspro-item-drop">(drop here)</span>
					    								</div>
					    								<div class="amalinkspro-edit-data"></div>
					    							</div>
					    							<div class="amalinkspro-column amalinkspro-column-6">
					    								<div class="amalinkspro-column-data amalinkspro-drop-zone">
					    									<i class="icon-amalinkspro-edit"></i>
					    									<span class="amalinkspro-item-label">Label:</span>
					    									<span class="amalinkspro-item-drop">(drop here)</span>
					    								</div>
					    								<div class="amalinkspro-edit-data"></div>
					    							</div>
				    							</div>

				    						</div>





				    						<div class="amalinkspro-infobox-option infobox-layout-3 js-amalinkspro-infobox-option" id="infobox-layout-3">

				    							<div class="amalinkspro-row amalinkspro-row-header amalinkspro-row-align-center">
					    							<div class="amalinkspro-column amalinkspro-column-12">
					    								
					    								<div class="amalinkspro-column-data amalinkspro-drop-zone">
					    									<i class="icon-amalinkspro-edit"></i>
					    									<img class="amalinkspro-image-link" src="" alt="" />
					    								</div>
					    								<div class="amalinkspro-edit-data"></div>
					    								
					    								<div class="amalinkspro-column-data amalinkspro-drop-zone">
					    									<i class="icon-amalinkspro-edit"></i>
					    									<span class="amalinkspro-item-label">Label:</span>
					    									<span class="amalinkspro-item-title amalinkspro-item-drop">PRODUCT TITLE</span>
					    								</div>
					    								<div class="amalinkspro-edit-data"></div>
					    							</div>
				    							</div>

				    							<div class="amalinkspro-row amalinkspro-row-align-center">
					    							<div class="amalinkspro-column amalinkspro-column-4">
					    								<div class="amalinkspro-column-data amalinkspro-drop-zone">
					    									<i class="icon-amalinkspro-edit"></i>
					    									<span class="amalinkspro-item-label">Label:</span>
					    									<span class="amalinkspro-item-drop">(drop here)</span>
					    								</div>
					    								<div class="amalinkspro-edit-data"></div>
					    							</div>
					    							<div class="amalinkspro-column amalinkspro-column-4">
					    								<div class="amalinkspro-column-data amalinkspro-drop-zone">
					    									<i class="icon-amalinkspro-edit"></i>
					    									<span class="amalinkspro-item-label">Label:</span>
					    									<span class="amalinkspro-item-drop">(drop here)</span>
					    								</div>
					    								<div class="amalinkspro-edit-data"></div>
					    							</div>
					    							<div class="amalinkspro-column amalinkspro-column-4">
					    								<div class="amalinkspro-column-data amalinkspro-drop-zone">
					    									<i class="icon-amalinkspro-edit"></i>
					    									<span class="amalinkspro-item-label">Label:</span>
					    									<span class="amalinkspro-item-drop">(drop here)</span>
					    								</div>
					    								<div class="amalinkspro-edit-data"></div>
					    							</div>
				    							</div>

				    							<div class="amalinkspro-row amalinkspro-row-align-center">
					    							<div class="amalinkspro-column amalinkspro-column-4">
					    								<div class="amalinkspro-column-data amalinkspro-drop-zone">
					    									<i class="icon-amalinkspro-edit"></i>
					    									<span class="amalinkspro-item-label">Label:</span>
					    									<span class="amalinkspro-item-drop">(drop here)</span>
					    								</div>
					    								<div class="amalinkspro-edit-data"></div>
					    							</div>
					    							<div class="amalinkspro-column amalinkspro-column-4">
					    								<div class="amalinkspro-column-data amalinkspro-drop-zone">
					    									<i class="icon-amalinkspro-edit"></i>
					    									<span class="amalinkspro-item-label">Label:</span>
					    									<span class="amalinkspro-item-drop">(drop here)</span>
					    								</div>
					    								<div class="amalinkspro-edit-data"></div>
					    							</div>
					    							<div class="amalinkspro-column amalinkspro-column-4">
					    								<div class="amalinkspro-column-data amalinkspro-drop-zone">
					    									<i class="icon-amalinkspro-edit"></i>
					    									<span class="amalinkspro-item-label">Label:</span>
					    									<span class="amalinkspro-item-drop">(drop here)</span>
					    								</div>
					    								<div class="amalinkspro-edit-data"></div>
					    							</div>
				    							</div>

				    							<div class="amalinkspro-row amalinkspro-row-align-center">
					    							<div class="amalinkspro-column amalinkspro-column-4">
					    								<div class="amalinkspro-column-data amalinkspro-drop-zone">
					    									<i class="icon-amalinkspro-edit"></i>
					    									<span class="amalinkspro-item-label">Label:</span>
					    									<span class="amalinkspro-item-drop">(drop here)</span>
					    								</div>
					    								<div class="amalinkspro-edit-data"></div>
					    							</div>
					    							<div class="amalinkspro-column amalinkspro-column-4">
					    								<div class="amalinkspro-column-data amalinkspro-drop-zone">
					    									<i class="icon-amalinkspro-edit"></i>
					    									<span class="amalinkspro-item-label">Label:</span>
					    									<span class="amalinkspro-item-drop">(drop here)</span>
					    								</div>
					    								<div class="amalinkspro-edit-data"></div>
					    							</div>
					    							<div class="amalinkspro-column amalinkspro-column-4">
					    								<div class="amalinkspro-column-data amalinkspro-drop-zone">
					    									<i class="icon-amalinkspro-edit"></i>
					    									<span class="amalinkspro-item-label">Label:</span>
					    									<span class="amalinkspro-item-drop">(drop here)</span>
					    								</div>
					    								<div class="amalinkspro-edit-data"></div>
					    							</div>
				    							</div>

				    							<div class="amalinkspro-row amalinkspro-row-align-center">
					    							<div class="amalinkspro-column amalinkspro-column-4">
					    								<div class="amalinkspro-column-data amalinkspro-drop-zone">
					    									<i class="icon-amalinkspro-edit"></i>
					    									<span class="amalinkspro-item-label">Label:</span>
					    									<span class="amalinkspro-item-drop">(drop here)</span>
					    								</div>
					    								<div class="amalinkspro-edit-data"></div>
					    							</div>
					    							<div class="amalinkspro-column amalinkspro-column-4">
					    								<div class="amalinkspro-column-data amalinkspro-drop-zone">
					    									<i class="icon-amalinkspro-edit"></i>
					    									<span class="amalinkspro-item-label">Label:</span>
					    									<span class="amalinkspro-item-drop">(drop here)</span>
					    								</div>
					    								<div class="amalinkspro-edit-data"></div>
					    							</div>
					    							<div class="amalinkspro-column amalinkspro-column-4">
					    								<div class="amalinkspro-column-data amalinkspro-drop-zone">
					    									<i class="icon-amalinkspro-edit"></i>
					    									<span class="amalinkspro-item-label">Label:</span>
					    									<span class="amalinkspro-item-drop">(drop here)</span>
					    								</div>
					    								<div class="amalinkspro-edit-data"></div>
					    							</div>
				    							</div>

				    						</div>



				    						<div class="amalinkspro-infobox-option infobox-layout-4 js-amalinkspro-infobox-option" id="infobox-layout-4">

				    							<div class="amalinkspro-row amalinkspro-row-header amalinkspro-row-align-left">
					    							<div class="amalinkspro-column amalinkspro-column-6">
					    								<img class="amalinkspro-image-link" src="" alt="" />
					    							</div>
					    							<div class="amalinkspro-column amalinkspro-column-6">
					    								<span class="amalinkspro-item-title">PRODUCT TITLE</span>
					    							</div>
				    							</div>

				    							<div class="amalinkspro-row amalinkspro-row-align-left">
					    							<div class="amalinkspro-column amalinkspro-column-6">
					    								<div class="amalinkspro-column-data amalinkspro-drop-zone">
					    									<i class="icon-amalinkspro-edit"></i>
					    									<span class="amalinkspro-item-label">Label:</span>
					    									<span class="amalinkspro-item-drop">(drop here)</span>
					    								</div>
					    								<div class="amalinkspro-edit-data"></div>
					    							</div>
					    							<div class="amalinkspro-column amalinkspro-column-6">
					    								<div class="amalinkspro-column-data amalinkspro-drop-zone">
					    									<i class="icon-amalinkspro-edit"></i>
					    									<span class="amalinkspro-item-label">Label:</span>
					    									<span class="amalinkspro-item-drop">(drop here)</span>
					    								</div>
					    								<div class="amalinkspro-edit-data"></div>
					    							</div>
				    							</div>

				    							<div class="amalinkspro-row amalinkspro-row-align-left">
					    							<div class="amalinkspro-column amalinkspro-column-6">
					    								<div class="amalinkspro-column-data amalinkspro-drop-zone">
					    									<i class="icon-amalinkspro-edit"></i>
					    									<span class="amalinkspro-item-label">Label:</span>
					    									<span class="amalinkspro-item-drop">(drop here)</span>
					    								</div>
					    								<div class="amalinkspro-edit-data"></div>
					    							</div>
					    							<div class="amalinkspro-column amalinkspro-column-6">
					    								<div class="amalinkspro-column-data amalinkspro-drop-zone">
					    									<i class="icon-amalinkspro-edit"></i>
					    									<span class="amalinkspro-item-label">Label:</span>
					    									<span class="amalinkspro-item-drop">(drop here)</span>
					    								</div>
					    								<div class="amalinkspro-edit-data"></div>
					    							</div>
				    							</div>

				    							<div class="amalinkspro-row amalinkspro-row-align-left">
					    							<div class="amalinkspro-column amalinkspro-column-6">
					    								<div class="amalinkspro-column-data amalinkspro-drop-zone">
					    									<i class="icon-amalinkspro-edit"></i>
					    									<span class="amalinkspro-item-label">Label:</span>
					    									<span class="amalinkspro-item-drop">(drop here)</span>
					    								</div>
					    								<div class="amalinkspro-edit-data"></div>
					    							</div>
					    							<div class="amalinkspro-column amalinkspro-column-6">
					    								<div class="amalinkspro-column-data amalinkspro-drop-zone">
					    									<i class="icon-amalinkspro-edit"></i>
					    									<span class="amalinkspro-item-label">Label:</span>
					    									<span class="amalinkspro-item-drop">(drop here)</span>
					    								</div>
					    								<div class="amalinkspro-edit-data"></div>
					    							</div>
				    							</div>

				    							<div class="amalinkspro-row amalinkspro-row-align-left">
					    							<div class="amalinkspro-column amalinkspro-column-6">
					    								<div class="amalinkspro-column-data amalinkspro-drop-zone">
					    									<i class="icon-amalinkspro-edit"></i>
					    									<span class="amalinkspro-item-label">Label:</span>
					    									<span class="amalinkspro-item-drop">(drop here)</span>
					    								</div>
					    								<div class="amalinkspro-edit-data"></div>
					    							</div>
					    							<div class="amalinkspro-column amalinkspro-column-6">
					    								<div class="amalinkspro-column-data amalinkspro-drop-zone">
					    									<i class="icon-amalinkspro-edit"></i>
					    									<span class="amalinkspro-item-label">Label:</span>
					    									<span class="amalinkspro-item-drop">(drop here)</span>
					    								</div>
					    								<div class="amalinkspro-edit-data"></div>
					    							</div>
				    							</div>

				    						</div>




				    						<div class="amalinkspro-clear"></div>



				    						<div class="amalinkspro-infobox-option amalinkspro-infobox-option-wide infobox-layout-5 js-amalinkspro-infobox-option" id="infobox-layout-5">

				    							<div class="amalinkspro-row amalinkspro-row-header amalinkspro-row-align-left">
					    							<div class="amalinkspro-column amalinkspro-column-12">
					    								<img class="amalinkspro-image-link" src="" alt="" />
					    								<span class="amalinkspro-item-title">PRODUCT TITLE</span>
					    								<span class="amalinkspro-item-price">$$$</span>
					    							</div>
				    							</div>

				    							<div class="amalinkspro-row amalinkspro-row-align-left">
					    							<div class="amalinkspro-column amalinkspro-column-6">
					    								<div class="amalinkspro-column-data amalinkspro-drop-zone">
					    									<i class="icon-amalinkspro-edit"></i>
					    									<span class="amalinkspro-item-label">Label:</span>
					    									<span class="amalinkspro-item-drop">(drop here)</span>
					    								</div>
					    								<div class="amalinkspro-edit-data"></div>
					    							</div>
					    							<div class="amalinkspro-column amalinkspro-column-6">
					    								<div class="amalinkspro-column-data amalinkspro-drop-zone">
					    									<i class="icon-amalinkspro-edit"></i>
					    									<span class="amalinkspro-item-label">Label:</span>
					    									<span class="amalinkspro-item-drop">(drop here)</span>
					    								</div>
					    								<div class="amalinkspro-edit-data"></div>
					    							</div>
				    							</div>

				    							<div class="amalinkspro-row amalinkspro-row-align-left">
					    							<div class="amalinkspro-column amalinkspro-column-6">
					    								<div class="amalinkspro-column-data amalinkspro-drop-zone">
					    									<i class="icon-amalinkspro-edit"></i>
					    									<span class="amalinkspro-item-label">Label:</span>
					    									<span class="amalinkspro-item-drop">(drop here)</span>
					    								</div>
					    								<div class="amalinkspro-edit-data"></div>
					    							</div>
					    							<div class="amalinkspro-column amalinkspro-column-6">
					    								<div class="amalinkspro-column-data amalinkspro-drop-zone">
					    									<i class="icon-amalinkspro-edit"></i>
					    									<span class="amalinkspro-item-label">Label:</span>
					    									<span class="amalinkspro-item-drop">(drop here)</span>
					    								</div>
					    								<div class="amalinkspro-edit-data"></div>
					    							</div>
				    							</div>

				    							<div class="amalinkspro-row amalinkspro-row-align-left">
					    							<div class="amalinkspro-column amalinkspro-column-6">
					    								<div class="amalinkspro-column-data amalinkspro-drop-zone">
					    									<i class="icon-amalinkspro-edit"></i>
					    									<span class="amalinkspro-item-label">Label:</span>
					    									<span class="amalinkspro-item-drop">(drop here)</span>
					    								</div>
					    								<div class="amalinkspro-edit-data"></div>
					    							</div>
					    							<div class="amalinkspro-column amalinkspro-column-6">
					    								<div class="amalinkspro-column-data amalinkspro-drop-zone">
					    									<i class="icon-amalinkspro-edit"></i>
					    									<span class="amalinkspro-item-label">Label:</span>
					    									<span class="amalinkspro-item-drop">(drop here)</span>
					    								</div>
					    								<div class="amalinkspro-edit-data"></div>
					    							</div>
				    							</div>

				    							<div class="amalinkspro-row amalinkspro-row-align-left">
					    							<div class="amalinkspro-column amalinkspro-column-6">
					    								<div class="amalinkspro-column-data amalinkspro-drop-zone">
					    									<i class="icon-amalinkspro-edit"></i>
					    									<span class="amalinkspro-item-label">Label:</span>
					    									<span class="amalinkspro-item-drop">(drop here)</span>
					    								</div>
					    								<div class="amalinkspro-edit-data"></div>
					    							</div>
					    							<div class="amalinkspro-column amalinkspro-column-6">
					    								<div class="amalinkspro-column-data amalinkspro-drop-zone">
					    									<i class="icon-amalinkspro-edit"></i>
					    									<span class="amalinkspro-item-label">Label:</span>
					    									<span class="amalinkspro-item-drop">(drop here)</span>
					    								</div>
					    								<div class="amalinkspro-edit-data"></div>
					    							</div>
				    							</div>

				    						</div>



				    						<div class="amalinkspro-infobox-option amalinkspro-infobox-option-wide infobox-layout-6 js-amalinkspro-infobox-option" id="infobox-layout-6">

				    							<div class="amalinkspro-row amalinkspro-row-header amalinkspro-row-align-left">
					    							<div class="amalinkspro-column amalinkspro-column-12">
					    								<img class="amalinkspro-image-link" src="" alt="" />
					    								<span class="amalinkspro-item-title">PRODUCT TITLE</span>
					    								<span class="amalinkspro-item-price">$$$</span>
					    							</div>
				    							</div>

				    							<div class="amalinkspro-row amalinkspro-row-align-left">
					    							<div class="amalinkspro-column amalinkspro-column-4">
					    								<div class="amalinkspro-column-data amalinkspro-drop-zone">
					    									<i class="icon-amalinkspro-edit"></i>
					    									<span class="amalinkspro-item-label">Label:</span>
					    									<span class="amalinkspro-item-drop">(drop here)</span>
					    								</div>
					    								<div class="amalinkspro-edit-data"></div>
					    							</div>
					    							<div class="amalinkspro-column amalinkspro-column-4">
					    								<div class="amalinkspro-column-data amalinkspro-drop-zone">
					    									<i class="icon-amalinkspro-edit"></i>
					    									<span class="amalinkspro-item-label">Label:</span>
					    									<span class="amalinkspro-item-drop">(drop here)</span>
					    								</div>
					    								<div class="amalinkspro-edit-data"></div>
					    							</div>
					    							<div class="amalinkspro-column amalinkspro-column-4">
					    								<div class="amalinkspro-column-data amalinkspro-drop-zone">
					    									<i class="icon-amalinkspro-edit"></i>
					    									<span class="amalinkspro-item-label">Label:</span>
					    									<span class="amalinkspro-item-drop">(drop here)</span>
					    								</div>
					    								<div class="amalinkspro-edit-data"></div>
					    							</div>
				    							</div>

				    							<div class="amalinkspro-row amalinkspro-row-align-left">
					    							<div class="amalinkspro-column amalinkspro-column-4">
					    								<div class="amalinkspro-column-data amalinkspro-drop-zone">
					    									<i class="icon-amalinkspro-edit"></i>
					    									<span class="amalinkspro-item-label">Label:</span>
					    									<span class="amalinkspro-item-drop">(drop here)</span>
					    								</div>
					    								<div class="amalinkspro-edit-data"></div>
					    							</div>
					    							<div class="amalinkspro-column amalinkspro-column-4">
					    								<div class="amalinkspro-column-data amalinkspro-drop-zone">
					    									<i class="icon-amalinkspro-edit"></i>
					    									<span class="amalinkspro-item-label">Label:</span>
					    									<span class="amalinkspro-item-drop">(drop here)</span>
					    								</div>
					    								<div class="amalinkspro-edit-data"></div>
					    							</div>
					    							<div class="amalinkspro-column amalinkspro-column-4">
					    								<div class="amalinkspro-column-data amalinkspro-drop-zone">
					    									<i class="icon-amalinkspro-edit"></i>
					    									<span class="amalinkspro-item-label">Label:</span>
					    									<span class="amalinkspro-item-drop">(drop here)</span>
					    								</div>
					    								<div class="amalinkspro-edit-data"></div>
					    							</div>
				    							</div>

				    							<div class="amalinkspro-row amalinkspro-row-align-left">
					    							<div class="amalinkspro-column amalinkspro-column-4">
					    								<div class="amalinkspro-column-data amalinkspro-drop-zone">
					    									<i class="icon-amalinkspro-edit"></i>
					    									<span class="amalinkspro-item-label">Label:</span>
					    									<span class="amalinkspro-item-drop">(drop here)</span>
					    								</div>
					    								<div class="amalinkspro-edit-data"></div>
					    							</div>
					    							<div class="amalinkspro-column amalinkspro-column-4">
					    								<div class="amalinkspro-column-data amalinkspro-drop-zone">
					    									<i class="icon-amalinkspro-edit"></i>
					    									<span class="amalinkspro-item-label">Label:</span>
					    									<span class="amalinkspro-item-drop">(drop here)</span>
					    								</div>
					    								<div class="amalinkspro-edit-data"></div>
					    							</div>
					    							<div class="amalinkspro-column amalinkspro-column-4">
					    								<div class="amalinkspro-column-data amalinkspro-drop-zone">
					    									<i class="icon-amalinkspro-edit"></i>
					    									<span class="amalinkspro-item-label">Label:</span>
					    									<span class="amalinkspro-item-drop">(drop here)</span>
					    								</div>
					    								<div class="amalinkspro-edit-data"></div>
					    							</div>
				    							</div>

				    							<div class="amalinkspro-row amalinkspro-row-align-left">
					    							<div class="amalinkspro-column amalinkspro-column-4">
					    								<div class="amalinkspro-column-data amalinkspro-drop-zone">
					    									<i class="icon-amalinkspro-edit"></i>
					    									<span class="amalinkspro-item-label">Label:</span>
					    									<span class="amalinkspro-item-drop">(drop here)</span>
					    								</div>
					    								<div class="amalinkspro-edit-data"></div>
					    							</div>
					    							<div class="amalinkspro-column amalinkspro-column-4">
					    								<div class="amalinkspro-column-data amalinkspro-drop-zone">
					    									<i class="icon-amalinkspro-edit"></i>
					    									<span class="amalinkspro-item-label">Label:</span>
					    									<span class="amalinkspro-item-drop">(drop here)</span>
					    								</div>
					    								<div class="amalinkspro-edit-data"></div>
					    							</div>
					    							<div class="amalinkspro-column amalinkspro-column-4">
					    								<div class="amalinkspro-column-data amalinkspro-drop-zone">
					    									<i class="icon-amalinkspro-edit"></i>
					    									<span class="amalinkspro-item-label">Label:</span>
					    									<span class="amalinkspro-item-drop">(drop here)</span>
					    								</div>
					    								<div class="amalinkspro-edit-data"></div>
					    							</div>
				    							</div>

				    						</div>


				    					</div>


			    					</div>





				    				<div class="amalinkspro-info-box-builder-settings">

				    					<h4>Change your Default Global Info Box Settings</h4>
				    					<p>Your global Info Box settings have been loaded for your convenience. Have a nice day.</p>

				    				</div>

			    				</div>




			    			</div>

		    				<div class="amalinkspro-clear"></div>

			    		</div>


		    		</div>

		    		<div class="amalinkspro-goto-step3">

		    			<h2 class="amalinkspro-choose-link-type-heading">Are you happy with your AmaLink Configuration?</h2>
		    			<a class="js-amalinkspro-final-step amalinkspro-final-step-btn" href="#">Go to Step 3</a>

		    		</div>

			    		




			    	

			    </div>






			    <div id="amalinkspro-step-3" class="amalinkspro-step-wrap">

			    	<div class="amalinkspro-insert-final-link">

			    		<a href="#" class="button button-primary js-amalinkspro-insert-final-link-shortcode"><i class="icon-amalinkspro-rocket"></i> Insert Shortcode</a>
			    		<a href="#" class="button button-secondary js-amalinkspro-insert-final-link-html">Insert HTML</a>

			    	</div>


				    <div class="amalinkspro-step3-final-preview"></div>


		    		<h2 class="amalinkspro-choose-link-type-heading">Link Settings</h2>

		    		<p>Your Global Link Settings have been automatically loaded. You can override them here. Your final link will be built using these options.</p>

		    		<form class="amalinkspro-associate-ids-form">

		    			<div class="amalinkspro-step3-setting-wrap amalinkspro-choose-associate-id-wrap">

		    			<h3>Choose Your Affiliate ID</h3>

		    				<?php 
		    				$locale = trim( get_option( 'amalinkspro-options_default_amazon_search_locale' ) );
		    				$associate_ids_field = 'amalinkspro-options_'.$locale.'_amazon_associate_ids';
		    				$associate_ids = get_option( 'amalinkspro-options_'.$locale.'_amazon_associate_ids' );

		    				echo '<p>Your chosen Locale is: <strong>'.$locale.'</strong>. You have associated these Amazon Associate IDs with this locale.</p>';

							if( $associate_ids ) {
								echo '<select class="amalinkspro-choose-associate-id-select">';
								for( $i = 0; $i < $associate_ids; $i++ ) {
									$id = get_option( 'amalinkspro-options_'.$locale.'_amazon_associate_ids_'.$i.'_associate_id' );
									echo '<option value="'.$id.'">'.$id.'</option>';
								}
								echo '</select>';
							}
		    				?>
		    				
		    			</div>

			    		<div class="amalinkspro-step3-setting-wrap">
			    			<h3>Open Links in a New Window</h3>
			    			<?php 
			    			$new_window = get_option('amalinkspro-options_open_links_in_a_new_window', true);
			    			if ( $new_window ) {
			    				$checked = ' checked="checked"';
			    			} else {
			    				$checked = '';
			    			}
			    			?>
							<input type="checkbox" value=""<?php echo $checked; ?> /><label>Check this box to open all AmaLinks Pro links in a new window</label>
						</div>

						<div class="amalinkspro-step3-setting-wrap">
			    			<h3>NoFollow Links</h3>
			    			<?php 
			    			$new_window = get_option('amalinkspro-options_nofollow_links', true);
			    			if ( $new_window ) {
			    				$checked = ' checked="checked"';
			    			} else {
			    				$checked = '';
			    			}
			    			?>
							<input type="checkbox" value=""<?php echo $checked; ?> /><label>Check this box to open add a re="nofollow" tag to all AmaLinks Pro links</label>
						</div>

						<div class="amalinkspro-step3-setting-wrap">
			    			<h3>Add to Cart</h3>
			    			<?php 
			    			$new_window = get_option('amalinkspro-options_add_to_cart', true);
			    			if ( $new_window ) {
			    				$checked = ' checked="checked"';
			    			} else {
			    				$checked = '';
			    			}
			    			?>
			    			<p>By enabling this feature, all AmaLinks Pro links will become Amazon "Add to Cart" links. When a visitor adds an item to their cart after clicking your link, you get an extra 89 day cookie set on the visitor's browser, giving you 3 extra months to get the commission.</p>
							<input type="checkbox" value=""<?php echo $checked; ?> /><label>I want AmalInks Pro to have my links add a product to a visitor's Amazon cart</label>
						</div>


					</form>



					<div class="amaalinkspro-insert-final-link">

			    	

			    </div>


			</div> <?php // end #amalinkspro-media-window-content ?>

		
	    </div>



	   <?php
	}  


	





	public function amazon_api_connection_test() {

		// make our amazon api call

		echo '<div class="amazon-api-test-message-inner">';


		if( ini_get('allow_url_fopen') ) {
			echo '<p>Making API call using file_get_contents()</p>';
		}
		else {
			echo '<p>Making API using WP_Curl - We recommend having your website hosting turn on "allow_url_fopen" in your server\'s PHP settings';
		}

		$amalinkspro_amazon_api = new amalinkspro_amazon_api();
		$api_response = $amalinkspro_amazon_api->amazon_api_request( null, "Images,ItemAttributes,Offers,Reviews", "guitar" );

		if ( $api_response ) {

			

			$arr = simplexml_load_string($api_response);

			echo '<p class="amalinkspro-success-message">Amazon API Connection was Successful!</p>';


			$arguments = $arr->OperationRequest->Arguments->Argument;

			$api_key_name = $arguments[0]['Name'];
			$api_key_value = $arguments[0]['Value'];

			echo '<strong>' . $api_key_name . ': </strong> ' . $api_key_value . '<br />';


			$api_key_name = $arguments[1]['Name'];
			$api_key_value = $arguments[1]['Value'];

			echo '<strong>' . $api_key_name . ':</strong> ' . $api_key_value. '<br />';


			$api_key_name = $arguments[6]['Name'];
			$api_key_value = $arguments[6]['Value'];

			echo '<strong>' . $api_key_name . ':</strong> ' . $api_key_value. '<br />';

			$api_key_name = $arguments[6]['Name'];
			$api_key_value = $arguments[6]['Value'];

			echo '<strong>' . 'Request Processing Time:</strong> ' . $arguments = $arr->OperationRequest->RequestProcessingTime . ' seconds';

		}

		else {
			echo '<p class="amalinkspro-error-message">There was a problem connecting to Amazon. Please check your Amazon credentials.</p>';

		}

		echo '</div>';

		die();




	}






	/** prepare our ajax call to the amazon product advertising api
	 *
	 * @since    1.0.0
	 */
	public function amalinkspro_find_amazon_products_ajax() {

		global $wpdb; // this is how you get access to the database

	    $term = $_POST['term'];
	    $locale = $_POST['locale'];
	    if ( $_POST['page'] ) {
	    	$page = $_POST['page'];
	    }
	    else {
	    	$page = '1';
	    }

	    // make our amazon api call
	    $api_response = $this->amalinkspro_amazon_api_request( $term, $locale, $page );

	    if(substr($api_response, 0, 5) == "<?xml") {
		    // parse our amazon api call response
	    	echo $this->amalinkspro_amazon_api_response_parse( $api_response );
		} 
		else {
		    echo  $api_response;
		}

	    die(); // this is required to return a proper result

	}




	/** make our ajax call to the amazon product advertising api
	 *
	 * @since    1.0.0
	 */
	public function amalinkspro_amazon_api_request( $term, $locale, $page ) {


		// http://webservices.amazon.com/scratchpad/index.html

		// Your AWS Access Key ID, as taken from the AWS Your Account page
		$aws_access_key_id = trim( get_option( 'amalinkspro-options_amazon_api_access_key' ) );

		// Your AWS Secret Key corresponding to the above ID, as taken from the AWS Your Account page
		$aws_secret_key = trim( get_option( 'amalinkspro-options_amazon_api_secret_key' ) );

		$affiliate_id = trim( get_option( 'amalinkspro-options_'.$locale.'_amazon_associate_ids_0_associate_id' ) );

		$error_message = '';

		if ( !$aws_access_key_id ) {
			$error_message .= '<p>You have not entered your Amazon API Access Key</p>';
		}

		if ( !$aws_secret_key ) {
			$error_message .= '<p>You have not entered your Amazon API Secret Key</p>';
		}

		if ( !$affiliate_id ) {
			$error_message .= '<p>You have not entered your Amazon Associate ID</p>';
		}

		if ( $error_message != '' ) {
			$api_response = $error_message;
			return $api_response;
		}

		if ( $aws_access_key_id && $aws_secret_key && $affiliate_id ) {

			$amalinkspro_amazon_api = new amalinkspro_amazon_api();
			$endpoint = $amalinkspro_amazon_api->amazon_api_get_endpoint();

			$uri = "/onca/xml";

			$params = array(
			    "Service" => "AWSECommerceService",
			    "Operation" => "ItemSearch",
			    "AWSAccessKeyId" => $aws_access_key_id,
			    "AssociateTag" => $affiliate_id,
			    "SearchIndex" => "All",
			    "Keywords" => $term,
			    "ItemPage" => $page,
			    "ResponseGroup" => "Images,ItemAttributes,Offers,Reviews"
			);

			// Set current timestamp if not set
			if (!isset($params["Timestamp"])) {
			    $params["Timestamp"] = gmdate('Y-m-d\TH:i:s\Z');
			}

			// Sort the parameters by key
			ksort($params);

			$pairs = array();

			foreach ($params as $key => $value) {
			    array_push($pairs, rawurlencode($key)."=".rawurlencode($value));
			}

			// Generate the canonical query
			$canonical_query_string = join("&", $pairs);

			// Generate the string to be signed
			$string_to_sign = "GET\n".$endpoint."\n".$uri."\n".$canonical_query_string;

			// Generate the signature required by the Product Advertising API
			$signature = base64_encode(hash_hmac("sha256", $string_to_sign, $aws_secret_key, true));

			// Generate the signed URL
			$request_url = 'https://'.$endpoint.$uri.'?'.$canonical_query_string.'&Signature='.rawurlencode($signature);

		


			if( ini_get('allow_url_fopen') ) {

				$api_response = file_get_contents($request_url);

			}

			else {

				$amazon_request_return = wp_remote_get($request_url);

				if ( is_wp_error($amazon_request_return) ) {
					$errors_arr = $amazon_request_return->errors;

					foreach( $errors_arr as $error ) {
						$api_response = $error[0];
					}
				}

				else {

					$request_response = $amazon_request_return['response']['code'];

					if ( $request_response == '200' ) {
						$api_response = wp_remote_retrieve_body($amazon_request_return);
					}
					else {
						$api_response = 'NOT 200 - Request Denied ny Amazon';
					}

				}

			}


		}

		return $api_response;


	}





	/** parse our response form our ajax call to the amazon product advertising api
	 *
	 * @since    1.0.0
	 */
	function amalinkspro_amazon_api_response_parse( $api_response ) {

		// convert xml response
		$arr = simplexml_load_string($api_response);

		// echo '<pre>'.print_r($arr,1).'</pre>';
		// die();

		if ( $arr->Items->Request->IsValid != 'True' ) {
			echo 'There was a problem retreiving the data from Amazon.';
			return;
		}

		?>


		


		<!-- <div class="amalinkspro-search-pagination">
			<span class="amalinkspro-search-page current" data-api-page="1" data-api-term="guitar" data-api-locale="US">Page 1</span>
			<span class="amalinkspro-search-page" data-api-page="2" data-api-term="guitar" data-api-locale="US">2</span>
			<span class="amalinkspro-search-page" data-api-page="3" data-api-term="guitar" data-api-locale="US">3</span>
			<span class="amalinkspro-search-page" data-api-page="4" data-api-term="guitar" data-api-locale="US">4</span>
			<span class="amalinkspro-search-page" data-api-page="5" data-api-term="guitar" data-api-locale="US">5</span>
		</div> -->

		<?php foreach($arr->Items->Item as $item) : // loop through our results and parse ?>

			<?php
			$Title = $item->ItemAttributes->Title;
			$SmallImage = $item->ImageSets->ImageSet->SmallImage->URL;
			$MediumImage = $item->ImageSets->ImageSet->MediumImage->URL;
			$FormattedPrice = $item->ItemAttributes->ListPrice->FormattedPrice;
			$AmountSavedPrice = $item->Offers->Offer->OfferListing->Price->FormattedPrice;
			$AmountSaveCurrency = $item->Offers->Offer->OfferListing->AmountSaved->FormattedPrice;
			$AmountSavedCurrency = $item->Offers->Offer->OfferListing->AmountSaved->CurrencyCode;
			$PercentageSaved = $item->Offers->Offer->OfferListing->PercentageSaved;
			$PrimeAvailable = $item->Offers->Offer->OfferListing->IsEligibleForPrime;

			$asin = $item->ASIN;
			$prod_url = $item->DetailPageURL;



			if ( $AmountSavedPrice ) {
				$price = $AmountSavedPrice;
			}
			elseif ( $FormattedPrice ) {
				$price = $FormattedPrice;
			}

			if ( $PercentageSaved ) {
				$price .= '<br />'.$PercentageSaved.'% Off';
			}

			if ( $PrimeAvailable == 1 ) {
				$prime_logo = '<span class="prime-logo"><img src="' . plugin_dir_url( 'amalinkspro.php' ) . 'amalinkspro/includes/images/amazon-prime.png" alt="Amazon Prime Available" /></span>';
			}
			else {
				$prime_logo = 'X';
			}



			?>

			<div class="amalinkspro-search-result">
				<div class="amalinkspro-search-result-inner">
					<div class="amalinkspro-search-result-col amalinkspro-search-result-col-img">
						<img src="<?php echo $SmallImage; ?>" alt="<?php echo $Title; ?>" />
					</div>
					<div class="amalinkspro-search-result-col amalinkspro-search-result-col-title">
						<?php echo $Title; ?>
					</div>
					<div class="amalinkspro-search-result-col amalinkspro-search-result-col-prime">
						<?php echo $prime_logo; ?>
					</div>
					<div class="amalinkspro-search-result-col amalinkspro-search-result-col-price"><?php echo $price; ?></div>
					<div class="amalinkspro-search-result-col amalinkspro-search-result-col-choose">
						<a class="button button-secondary button-large js-amalinkspro-choose-product"
						data-amalinkspro-item-title="<?php echo $Title; ?>"
						data-amalinkspro-item-img-med="<?php echo $MediumImage; ?>"
						data-amalinkspro-item-asin="<?php echo $asin; ?>"
						>Choose</a>
						<a class="amalinkspro-view-link" href="<?php echo $prod_url; ?>" target="_blank">view</a>
					</div>
				</div>
			</div>

		<?php endforeach; ?>

		<!-- <div class="amalinkspro-search-pagination">
			<span class="amalinkspro-search-page current" data-api-page="1" data-api-term="guitar" data-api-locale="US">Page 1</span>
			<span class="amalinkspro-search-page" data-api-page="2" data-api-term="guitar" data-api-locale="US">2</span>
			<span class="amalinkspro-search-page" data-api-page="3" data-api-term="guitar" data-api-locale="US">3</span>
			<span class="amalinkspro-search-page" data-api-page="4" data-api-term="guitar" data-api-locale="US">4</span>
			<span class="amalinkspro-search-page" data-api-page="5" data-api-term="guitar" data-api-locale="US">5</span>
		</div> -->




	<?php
		// USE THIS To SEE FULL RESPONSE
		//echo '<hr /><pre>'.print_r($arr,1).'</pre>';

	}




	








	/** parse our response form our ajax call to the amazon product advertising api
	 *
	 * @since    1.0.0
	 */
	function amazon_api_ajax_lookup_chosen_item(  ) {

		global $wpdb;

		$asin = $_POST['asin'];

		$amalinkspro_amazon_api = new amalinkspro_amazon_api();
		$api_response = $amalinkspro_amazon_api->amazon_api_request( null, "Images,ItemAttributes,Offers,Reviews", $asin );

		if ( $api_response ) {
			$arr = simplexml_load_string($api_response);
			$item = $arr->Items->Item;
			$item_json = json_encode($item);
			echo $item_json;
		}
		else {
			echo 'no api results';
		}
	    
		die();

	}








}
