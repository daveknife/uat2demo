(function( $ ) {
	'use strict';

	/**
	 * All of the code for your admin-facing JavaScript source
	 * should reside in this file.
	 *
	 * Note: It has been assumed you will write $ code here, so the
	 * $ function reference has been prepared for usage within the scope
	 * of this function.
	 *
	 * This enables you to define handlers, for when the DOM is ready:
	 *
	 * $(function() {
	 *
	 * });
	 *
	 * When the window is loaded:
	 *
	 * $( window ).load(function() {
	 *
	 * });
	 *
	 * ...and/or other possibilities.
	 *
	 * Ideally, it is not considered best practise to attach more than a
	 * single DOM-ready or window-load handler for a particular page.
	 * Although scripts in the WordPress core, Plugins and Themes may be
	 * practising this, we should strive to set a better example in our own work.
	 */


	/**
	* jquery.matchHeight-min.js master
	* http://brm.io/jquery-match-height/
	* License: MIT
	*/
	(function(c){var n=-1,f=-1,g=function(a){return parseFloat(a)||0},r=function(a){var b=null,d=[];c(a).each(function(){var a=c(this),k=a.offset().top-g(a.css("margin-top")),l=0<d.length?d[d.length-1]:null;null===l?d.push(a):1>=Math.floor(Math.abs(b-k))?d[d.length-1]=l.add(a):d.push(a);b=k});return d},p=function(a){var b={byRow:!0,property:"height",target:null,remove:!1};if("object"===typeof a)return c.extend(b,a);"boolean"===typeof a?b.byRow=a:"remove"===a&&(b.remove=!0);return b},b=c.fn.matchHeight=
	function(a){a=p(a);if(a.remove){var e=this;this.css(a.property,"");c.each(b._groups,function(a,b){b.elements=b.elements.not(e)});return this}if(1>=this.length&&!a.target)return this;b._groups.push({elements:this,options:a});b._apply(this,a);return this};b._groups=[];b._throttle=80;b._maintainScroll=!1;b._beforeUpdate=null;b._afterUpdate=null;b._apply=function(a,e){var d=p(e),h=c(a),k=[h],l=c(window).scrollTop(),f=c("html").outerHeight(!0),m=h.parents().filter(":hidden");m.each(function(){var a=c(this);
	a.data("style-cache",a.attr("style"))});m.css("display","block");d.byRow&&!d.target&&(h.each(function(){var a=c(this),b="inline-block"===a.css("display")?"inline-block":"block";a.data("style-cache",a.attr("style"));a.css({display:b,"padding-top":"0","padding-bottom":"0","margin-top":"0","margin-bottom":"0","border-top-width":"0","border-bottom-width":"0",height:"100px"})}),k=r(h),h.each(function(){var a=c(this);a.attr("style",a.data("style-cache")||"")}));c.each(k,function(a,b){var e=c(b),f=0;if(d.target)f=
	d.target.outerHeight(!1);else{if(d.byRow&&1>=e.length){e.css(d.property,"");return}e.each(function(){var a=c(this),b={display:"inline-block"===a.css("display")?"inline-block":"block"};b[d.property]="";a.css(b);a.outerHeight(!1)>f&&(f=a.outerHeight(!1));a.css("display","")})}e.each(function(){var a=c(this),b=0;d.target&&a.is(d.target)||("border-box"!==a.css("box-sizing")&&(b+=g(a.css("border-top-width"))+g(a.css("border-bottom-width")),b+=g(a.css("padding-top"))+g(a.css("padding-bottom"))),a.css(d.property,
	f-b))})});m.each(function(){var a=c(this);a.attr("style",a.data("style-cache")||null)});b._maintainScroll&&c(window).scrollTop(l/f*c("html").outerHeight(!0));return this};b._applyDataApi=function(){var a={};c("[data-match-height], [data-mh]").each(function(){var b=c(this),d=b.attr("data-mh")||b.attr("data-match-height");a[d]=d in a?a[d].add(b):b});c.each(a,function(){this.matchHeight(!0)})};var q=function(a){b._beforeUpdate&&b._beforeUpdate(a,b._groups);c.each(b._groups,function(){b._apply(this.elements,
	this.options)});b._afterUpdate&&b._afterUpdate(a,b._groups)};b._update=function(a,e){if(e&&"resize"===e.type){var d=c(window).width();if(d===n)return;n=d}a?-1===f&&(f=setTimeout(function(){q(e);f=-1},b._throttle)):q(e)};c(b._applyDataApi);c(window).bind("load",function(a){b._update(!1,a)});c(window).bind("resize orientationchange",function(a){b._update(!0,a)})})(jQuery);

	/*!
	 * jQuery UI Touch Punch 0.2.3
	 *
	 * Copyright 2011–2014, Dave Furfero
	 * Dual licensed under the MIT or GPL Version 2 licenses.
	 *
	 * Depends:
	 *  jquery.ui.widget.js
	 *  jquery.ui.mouse.js
	 */
	!function(a){function f(a,b){if(!(a.originalEvent.touches.length>1)){a.preventDefault();var c=a.originalEvent.changedTouches[0],d=document.createEvent("MouseEvents");d.initMouseEvent(b,!0,!0,window,1,c.screenX,c.screenY,c.clientX,c.clientY,!1,!1,!1,!1,0,null),a.target.dispatchEvent(d)}}if(a.support.touch="ontouchend"in document,a.support.touch){var e,b=a.ui.mouse.prototype,c=b._mouseInit,d=b._mouseDestroy;b._touchStart=function(a){var b=this;!e&&b._mouseCapture(a.originalEvent.changedTouches[0])&&(e=!0,b._touchMoved=!1,f(a,"mouseover"),f(a,"mousemove"),f(a,"mousedown"))},b._touchMove=function(a){e&&(this._touchMoved=!0,f(a,"mousemove"))},b._touchEnd=function(a){e&&(f(a,"mouseup"),f(a,"mouseout"),this._touchMoved||f(a,"click"),e=!1)},b._mouseInit=function(){var b=this;b.element.bind({touchstart:a.proxy(b,"_touchStart"),touchmove:a.proxy(b,"_touchMove"),touchend:a.proxy(b,"_touchEnd")}),c.call(b)},b._mouseDestroy=function(){var b=this;b.element.unbind({touchstart:a.proxy(b,"_touchStart"),touchmove:a.proxy(b,"_touchMove"),touchend:a.proxy(b,"_touchEnd")}),d.call(b)}}}(jQuery);


	jQuery(document).ready( function($){ 

		

		var amalinksproChosenItem = '';


		// license activate button trigger
		$('body').on('click', '.amalinkspro-activate', function() {

	        // get the license value in the license field
			var license = $(this).closest('.gf-option-box').find('.amalinkspro-license').val();

			// show the loading spinner and add a class to identify it later to remove on completion
	        $('.amalinkspro-license-loading-gif').fadeIn(300);
	        $('.amalinkspro-media-window-content').addClass('scroll-locked');
	        
	        // set our ajax call data
			var data = {
	            action: 'activate_amalinkspro_license',
	            license: license
	        };

	        if ( license != '' ) {
	        	// call our deactivate ajax function and pass the data to it
	        	activate_amalinkspro_license(data);
	        }
	        else {
	        	alert('Your license field does not contain a valid license. Please try again.');
	        }


		});

		function activate_amalinkspro_license(data) {

			$.ajax({
	            type: 'POST',
	            url: ajaxurl,
	            data: data,
	            //dataType: 'json',
	            complete: function(response){
	                
	            },
	            success: function(response) {

	            	console.log('activate response: ' + response);

	            	if ( response == 'valid' ) {

	                    var current_license_box = $('#amalinkspro_license_wrap');

	                    current_license_box.find('.license-message span')
	                    	.html('Your license is active.');
	                    current_license_box.find('.license-message')
	                    	.addClass('amalinkspro-license-valid')
	                    	.removeClass('amalinkspro-license-deactivated'
	                    );

	                    current_license_box.find('.amalinkspro-activate')
	                    	.removeClass('amalinkspro-activate')
	                    	.addClass('amalinkspro-deactivate')
	                    	.attr('value', 'Dectivate'
	                    );

	                }
	                else if ( response == 'invalid' ) {
	                    alert('This license is invalid. Please try again.');
	                }

	                $('.amalinkspro-media-window-content').removeClass('scroll-locked');
	                $('.amalinkspro-license-loading-gif').fadeOut(300);

	            }
	        });
		}


		// license deactivate button trigger
	    $('body').on('click', '.amalinkspro-deactivate', function() {

	        // get the license value in the license field
			var license = $(this).closest('.gf-option-box').find('.amalinkspro-license').val();

			// show the loading spinner and add a class to identify it later to remove on completion
	        $('.amalinkspro-license-loading-gif').fadeIn(300);
	        $('.amalinkspro-media-window-content').addClass('scroll-locked');
	        
	        // set our ajax call data
			var data = {
	            action: 'deactivate_amalinkspro_license',
	            license: license
	        };

	        if ( license != '' ) {
	        	// call our deactivate ajax function and pass the data to it
	        	deactivate_amalinkspro_license(data);
	        }
	        else {
	        	alert('Your license field does not contain a valid license. Please try again.');
	        }
	    });

	    function deactivate_amalinkspro_license(data) {
	        $.ajax({
	            type: 'POST',
	            url: ajaxurl,
	            data: data,
	            //dataType: 'json',
	            complete: function(response){

	            	console.log('deactivate response: ' + response);
	                
	            },
	            success: function(response){


	            	console.log('deactivate response: ' + response);

	            	if ( response == 'deactivated' ) {

	                    var current_license_box = $('#amalinkspro_license_wrap');

	                    current_license_box.find('.license-message span')
	                    	.html('The license key you entered has been deactivated on this website.');
	                    current_license_box.find('.license-message')
	                    	.addClass('amalinkspro-license-deactivated')
	                    	.removeClass('amalinkspro-license-valid'
	                    );

	                    current_license_box.find('.amalinkspro-deactivate')
	                    	.removeClass('amalinkspro-deactivate')
	                    	.addClass('amalinkspro-activate')
	                    	.attr('value', 'Activate'
	                    );

	                }
	                else if ( response == 'invalid' ) {
	                    alert('There has been an error. Console Message');
	                }

	                $('.amalinkspro-media-window-content').removeClass('scroll-locked');
	                $('.amalinkspro-license-loading-gif').fadeOut(300);

	            }
	        });
	    }



	    // submit amazon api search on review admin pedit screen
		$('body').on( 'click', '.js-amalinkspro-test-api', function(){

			$('.amalinkspro-apitest-loading-gif').show();

			var data = {
	            action: 'amazon_api_connection_test',
	        };

	        amazon_api_connection_test_ajax(data);

			//console.log(term+' - '+locale);
		});

		// make our ajax call to do the amazon api call in php
		function amazon_api_connection_test_ajax(data) {

			$.ajax({
	            type: 'POST',
	            url: ajaxurl,
	            data: data,
	            success: function(response) {

	            	$('.amalinkspro-apitest-loading-gif').hide();

	                $(".amazon-api-test-message").html(response);

	            },
	            complete: function(response){

	            	//$(".amazon-api-test-message").html('response 2');
	            	
	            }
	        });

		}


























		$('body').on('click', '#insert-amalinkspro-media', function(e){
			e.preventDefault();
			$('#amalinkspro-media-window').fadeIn(100);
			$('#amalinkspro-close-modal').fadeIn(100);
			$('body').addClass('scroll-locked');
		});



		$('body').on('click', '#amalinkspro-close-modal, .amalinkspro-close-modal', function(e){
			e.preventDefault();
			$('#amalinkspro-media-window').fadeOut(100);
			$('#amalinkspro-close-modal').fadeOut(100);
			$('body').removeClass('scroll-locked');
		});



		


		var amalinkspro_shortcode = '';
		var amalinkspro_html = '';

		// insert our shortcode
		$('body').on('click', '.js-amalinkspro-insert-final-link-shortcode', function(e) {
			var amalinkspro_shortcode = amalinkspro_build_shortcode();
			tinymce.activeEditor.execCommand('mceInsertContent', false, amalinkspro_shortcode);
			$('#amalinkspro-media-window').fadeOut(100);
			$('#amalinkspro-close-modal').fadeOut(100);
			$('body').removeClass('scroll-locked');
		});


		function amalinkspro_build_shortcode() {

			var preview = $('#amalinkspro-step-3').find('.amalinkspro-linktype-preview');
			
			var link_type = preview.attr('data-amalinkspro-linktype');
			var asin = preview.attr('data-amalinkspro-asin');
			var associate_id = $('#amalinkspro-step-3').find('.amalinkspro-choose-associate-id-select').val();


			if ( link_type == 'text-link' ) {
				var link_text =  preview.attr('data-amalinkspro-linktext');
				var amalinkspro_shortcode = '[amalinkspro type="text-link" asin="'+asin+'" associate_id="'+associate_id+'"]'+link_text+'[/amalinkspro]';
			}

			else if ( link_type == 'image-link' ) {
				var link_image =  preview.attr('data-amalinkspro-linkimage');
				var amalinkspro_shortcode = '[amalinkspro type="image-link" asin="'+asin+'" associate_id="'+associate_id+'"]'+link_image+'[/amalinkspro]';
			}

			else if ( link_type == 'cta-link' ) {

				var cta_text = preview.attr('data-amalinkspro-ctatext');
				
				var cta_style = preview.attr('data-amalinkspro-ctastyle');

				var cta_class = preview.attr('data-amalinkspro-ctaclass');

				if ( cta_style === 'cta-btn-text' ) {

					var amalinkspro_shortcode = '[amalinkspro type="cta-link" ctaclass="'+cta_class+'" ctastyle="'+cta_style+'" asin="'+asin+'" associate_id="'+associate_id+'"]'+cta_text+'[/amalinkspro]';

				}
				else if ( cta_style === 'cta-btn-image' ) {

					var cta_image = preview.attr('data-amalinkspro-ctaimage');

					var amalinkspro_shortcode = '[amalinkspro type="cta-link" ctaclass="'+cta_class+'" ctastyle="'+cta_style+'" cta_alt="'+cta_text+'" asin="'+asin+'" associate_id="'+associate_id+'"]'+cta_image+'[/amalinkspro]';

				}
				else {
					var amalinkspro_shortcode = 'The cta_style value is not valid.';
				}
				
				
			}


			else {
				var amalinkspro_shortcode = 'The link_type value is not valid.';
			}


			
			return amalinkspro_shortcode;

		}








		// insert our shortcode
		$('body').on('click', '.js-amalinkspro-insert-final-link-html', function(e) {
			var amalinkspro_html = amalinkspro_build_html();
			vartinymce.activeEditor.execCommand('mceInsertContent', false, amalinkspro_html);
			$('#amalinkspro-media-window').fadeOut(100);
			$('#amalinkspro-close-modal').fadeOut(100);
			$('body').removeClass('scroll-locked');
		});


		var amalinkspro_html = '';
		function amalinkspro_build_html() {

			var amalinkspro_html = '<a href="#">test html link</a>';


			return amalinkspro_html;
			
		}






		// 
		$('body').on('click', '.amalinkspro-step:not(.amalinkspro-step-active, .amalinkspro-step-disabled)', function() {
			console.log('tab clicked');

			$('.amalinkspro-step').removeClass('amalinkspro-step-active');
	        $(this).addClass('amalinkspro-step-active');

	        $('.amalinkspro-step-wrap').hide();

	        var div = $(this).attr( 'data-amalinkspro-step' );

	        $( '#'+div ).show();

	    	$('#amalinkspro-media-window-content').animate({ scrollTop: 0 }, 0);

		});





		// submit amazon api search on review admin pedit screen
		$('body').on( 'click', '.js-amalinkspro-mediamodal-search', function(){

			console.log('Amazon API search initiated');

			//$('.amalinkspro-search-loading-gif').show();
			$('.amalinkspro-search-results-list').animate({
				opacity: 0.15,
				}, 100, function() {
					// Animation complete.tegrth
			});

			$('.amalinkspro-loading-overlay').fadeIn(300);

			

			var par = $(this).closest('p');
			var term = par.find('#amalinkspro-search-keyword').val();
			var locale = par.find('#amalinkspro-search-locale').val();

			//console.log(term,locale);

			var data = {
	            action: 'amalinkspro_find_amazon_products_ajax',
	            term: term,
	            locale: locale,
	        };

	        amazon_api_ajax_load_results(data);

			//console.log(term+' - '+locale);
		});

		// make our ajax call to do the amazon api call in php
		function amazon_api_ajax_load_results(data) {

			$.ajax({
	            type: 'POST',
	            url: ajaxurl,
	            data: data,
	            success: function(response) {
	                console.log('Successful Ajax Response');
	                $('.amalinkspro-search-results h2').fadeIn(100);
	                $(".amalinkspro-search-results-list").html(response);
	            },
	            complete: function(response){
	            	$('.amalinkspro-search-loading-gif').hide();
	            	$('.amalinkspro-search-results-list').animate({
						opacity: 1,
						}, 100, function() {
							// Animation complete.tegrth
					});

					$('.js-amalinkspro-mediamodal-search').removeClass('amalinkspro-next-action');
					$('.js-amalinkspro-choose-product').addClass('amalinkspro-next-action');
					$('.amalinkspro-loading-overlay').fadeOut(300);
					$('.amalinkspro-search-pagination').fadeIn(600);

	            }
	        });

		}



		// make our ajax call to insert the chosen amazon product into the "chosen" box and save the info in the database for easy viewing in the admin.
		$('body').on( 'click', '.amalinkspro-search-page', function(){

			console.log('New Amazon Results Page Selected');

			//$('.amalinkspro-search-loading-gif').show();
			$('.amalinkspro-search-results-list').animate({
				opacity: 0.15,
				}, 100, function() {
					// Animation complete.tegrth
			});

			var par = $(this).closest('.amalinkspro-step-wrap').find('.amalinkspro-popup-form p');
			var term = par.find('#amalinkspro-search-keyword').val();
			var locale = par.find('#amalinkspro-search-locale').val();
			var page = $(this).attr('data-api-page');

			var data = {
	            action: 'amalinkspro_find_amazon_products_ajax',
	            term: term,
	            locale: locale,
	            page: page,
	        };


	        //console.log(data);

	        $('.amalinkspro-loading-overlay').fadeIn(300);
	        

	    	amazon_api_ajax_load_new_page(data);

		});

		//make our ajax call to do the amazon api call in php
		function amazon_api_ajax_load_new_page(data) {

			$.ajax({
	            type: 'POST',
	            url: ajaxurl,
	            data: data,
	            success: function(response) {
	                console.log('Successful Ajax Response New Page');
	                $(".amalinkspro-search-results-list").html(response);
	            },
	            complete: function(response){
				    //$('.amalinkspro-search-loading-gif').hide();
	            	$('.amalinkspro-search-results-list').animate({
						opacity: 1,
						}, 100, function() {
							// Animation complete.tegrth
					});

					$('.amalinkspro-loading-overlay').fadeOut(300);
	            }
	        });

		}




		// 
		$('body').on('click', '.js-amalinkspro-choose-product', function() {
			
			console.log('chose this product');


			var title = $(this).attr('data-amalinkspro-item-title');
			var imgMed = $(this).attr('data-amalinkspro-item-img-med');
			var asin = $(this).attr('data-amalinkspro-item-asin');

			var data = {
	            action: 'amazon_api_ajax_lookup_chosen_item',
	            asin: asin,
	            imgMed: imgMed,
	            title: title
	        };

	        $('.amalinkspro-loading-overlay').fadeIn(300);

			amazon_api_ajax_lookup_single_asin(data);

		});



		// make our ajax call to do the amazon api call in php
		function amazon_api_ajax_lookup_single_asin(data) {

			$.ajax({
	            type: 'POST',
	            url: ajaxurl,
	            dataType: "json",
	            data: data,
	            success: function(response) {

	            	// console.log('response: ');
	            	// console.log(response);


	            	$('.amalinkspro-step-2').addClass('amalinkspro-step-active');
	            	$('#amalinkspro-step-1').hide();

	            	$('.amalinkspro-step-1').removeClass('amalinkspro-step-active');
	            	$('#amalinkspro-step-2').show();

	            	$('html, body').animate({ scrollTop: 0 }, 0);

	            	$('.amalinkspro-step-2').removeClass('amalinkspro-step-disabled');


	            
	            	amalinksproChosenItem = response;


	                //console.log(response);

	                $('.amalinkspro-chosen-product').find('h3').text(data.title);
	            	$('.amalinkspro-chosen-product').find('.amalinkspro-chosen-product-main-img').attr('src', data.imgMed);
	            	$('.amalinkspro-chosen-item-previewlink').attr('href', amalinksproChosenItem.DetailPageURL);

	            	$('.amalinkspro-chosen-product').fadeIn(100);


	            	if ( amalinksproChosenItem ) {

		            	if ( amalinksproChosenItem.ASIN ) {
		            		$('.amalinkspro-chosen-product-asin').find('span').html(amalinksproChosenItem.ASIN);
		            	}
		            	else {
		            		$('.amalinkspro-chosen-product-asin').find('span').html('No Data');
		            	}



		            	if ( amalinksproChosenItem.ItemAttributes ) {

			            	if ( amalinksproChosenItem.ItemAttributes.Brand ) {
			            		$('.amalinkspro-chosen-product-brand').find('span').html(amalinksproChosenItem.ItemAttributes.Brand);
			            	}
			            	else {
			            		$('.amalinkspro-chosen-product-brand').find('span').html('No Data');
			            	}

			            	if ( amalinksproChosenItem.ItemAttributes.Model ) {
			            		$('.amalinkspro-chosen-product-model').find('span').html(amalinksproChosenItem.ItemAttributes.Model);
			            	}
			            	else {
			            		$('.amalinkspro-chosen-product-model').find('span').html('No Data');
			            	}

			            	if ( amalinksproChosenItem.ItemAttributes.UPC ) {
			            		$('.amalinkspro-chosen-product-upc').find('span').html(amalinksproChosenItem.ItemAttributes.UPC);
			            	}
			            	else {
			            		$('.amalinkspro-chosen-product-upc').find('span').html('No Data');
			            	}

			            	if ( amalinksproChosenItem.ItemAttributes.Warranty ) {
			            		$('.amalinkspro-chosen-product-warranty').find('span').html(amalinksproChosenItem.ItemAttributes.Warranty);
			            	}
			            	else {
			            		$('.amalinkspro-chosen-product-warranty').find('span').html('No Data');
			            	}


			            	if ( amalinksproChosenItem.ItemAttributes.ListPrice ) {

				            	if ( amalinksproChosenItem.ItemAttributes.ListPrice.FormattedPrice ) {
				            		$('.amalinkspro-chosen-product-listprice').find('span').html(amalinksproChosenItem.ItemAttributes.ListPrice.FormattedPrice);
				            	}
				            	else {
				            		$('.amalinkspro-chosen-product-listprice').find('span').html('No Data');
				            	}

				            }
				            else {
				            	$('.amalinkspro-chosen-product-listprice').find('span').html('No Data');
				            }

			            }
			            else {
		            		$('.amalinkspro-chosen-product-brand').find('span').html('No Data');
		            		$('.amalinkspro-chosen-product-model').find('span').html('No Data');
		            		$('.amalinkspro-chosen-product-upc').find('span').html('No Data');
		            		$('.amalinkspro-chosen-product-warranty').find('span').html('No Data');
		            		$('.amalinkspro-chosen-product-listprice').find('span').html('No Data');
			            }




			            if ( amalinksproChosenItem.OfferSummary ) {

			            	if ( amalinksproChosenItem.OfferSummary.LowestNewPrice ) {

				            	if ( amalinksproChosenItem.OfferSummary.LowestNewPrice.FormattedPrice ) {
				            		$('.amalinkspro-chosen-product-lowestnewprice').find('span').html(amalinksproChosenItem.OfferSummary.LowestNewPrice.FormattedPrice);
				            	}
				            	else {
				            		$('.amalinkspro-chosen-product-lowestnewprice').find('span').html('No Data');
				            	}

				            }

				            if ( amalinksproChosenItem.OfferSummary.LowestUsedPrice ) {

				            	if ( amalinksproChosenItem.OfferSummary.LowestUsedPrice.FormattedPrice ) {
				            		$('.amalinkspro-chosen-product-lowestusedprice').find('span').html(amalinksproChosenItem.OfferSummary.LowestUsedPrice.FormattedPrice);
				            	}
				            	else {
				            		$('.amalinkspro-chosen-product-lowestusedprice').find('span').html('No Data');

				            	}

				            }
				            else {
				            	$('.amalinkspro-chosen-product-lowestusedprice').find('span').html('No Data');
				            }

			            }

			            else {
		            		$('.amalinkspro-chosen-product-lowestnewprice').find('span').html('No Data');
	            			$('.amalinkspro-chosen-product-lowestusedprice').find('span').html('No Data');
			            }

			            // console.log('amalinksproChosenItem.Offers.Offer');
			            //console.log(amalinksproChosenItem);


			            if ( amalinksproChosenItem.Offers ) {

			            	if ( amalinksproChosenItem.Offers.Offer ) {

			            		if ( amalinksproChosenItem.Offers.Offer.OfferListing ) {

			            			if ( amalinksproChosenItem.Offers.Offer.OfferListing.Price ) {

						            	if ( amalinksproChosenItem.Offers.Offer.OfferListing.Price.FormattedPrice ) {
						            		$('.amalinkspro-chosen-product-bestoffer').find('span').html(amalinksproChosenItem.Offers.Offer.OfferListing.Price.FormattedPrice);
						            	}
						            	else {
						            		$('.amalinkspro-chosen-product-bestoffer').find('span').html('No Data');
						            	}

						            }
						            else {

						            }




					            	if ( amalinksproChosenItem.Offers.Offer.OfferListing.PercentageSaved ) {
					            		console.log(amalinksproChosenItem.Offers.Offer.OfferListing);
					            		$('.amalinkspro-chosen-product-save').find('span').html(amalinksproChosenItem.Offers.Offer.OfferListing.PercentageSaved+'%');
					            	}
					            	else {
					            		$('.amalinkspro-chosen-product-save').find('span').html('No Data');
					            	}

					            }
					            else {
					            	$('.amalinkspro-chosen-product-bestoffer').find('span').html('No Data');
			            			$('.amalinkspro-chosen-product-save').find('span').html('No Data');
					            }

				            }
				            else {
				            	$('.amalinkspro-chosen-product-bestoffer').find('span').html('No Data');
		            			$('.amalinkspro-chosen-product-save').find('span').html('No Data');
				            }

			            }
			            else {
			            	$('.amalinkspro-chosen-product-bestoffer').find('span').html('No Data');
	            			$('.amalinkspro-chosen-product-save').find('span').html('No Data');
			            }


			        }

			        else {

			        	$('.amalinkspro-chosen-product-asin').find('span').html('No Data');
	            		$('.amalinkspro-chosen-product-brand').find('span').html('No Data');
	            		$('.amalinkspro-chosen-product-model').find('span').html('No Data');
	            		$('.amalinkspro-chosen-product-upc').find('span').html('No Data');
	            		$('.amalinkspro-chosen-product-warranty').find('span').html('No Data');
	            		$('.amalinkspro-chosen-product-listprice').find('span').html('No Data');
	            		$('.amalinkspro-chosen-product-lowestnewprice').find('span').html('No Data');
	            		$('.amalinkspro-chosen-product-lowestusedprice').find('span').html('No Data');
	            		$('.amalinkspro-chosen-product-bestoffer').find('span').html('No Data');
	            		$('.amalinkspro-chosen-product-save').find('span').html('No Data');


			        }

			        $(".amalinkspro-chosen-product p:contains('No Data')").css("color", "#ffb2b2");


			        /* Set text Link Values */

	            	var par = $('.amalinkspro-linktype-preview-text-link');
					par.find('.amalinkspro-text-link-preview-p').find('a').text( amalinksproChosenItem.ItemAttributes.Title );
					par.find('.amalinkspro-text-link-preview-p').find('a').attr( 'href', amalinksproChosenItem.DetailPageURL );
					par.find('input[name=amalinkspro-edit-textlink-text]').val(amalinksproChosenItem.ItemAttributes.Title);

					par.attr( 'data-amalinkspro-linktext', amalinksproChosenItem.ItemAttributes.Title );
					par.attr( 'data-amalinkspro-asin', amalinksproChosenItem.ASIN );

					


					/* Set Image Link Values */

					var par = $('.amalinkspro-linktype-preview-image-link');
	            	par.find('.amalinkspro-image-link-img').attr('src', amalinksproChosenItem.MediumImage.URL );
	            	par.find('.amalinkspro-image-link-link').attr('href', amalinksproChosenItem.DetailPageURL );

	            	par.attr( 'data-amalinkspro-asin', amalinksproChosenItem.ASIN );
	            	par.attr( 'data-amalinkspro-linkimage', amalinksproChosenItem.MediumImage.URL );

	            	var chosen_img_url = amalinksproChosenItem.MediumImage.URL;


	            	var new_image_choices = '';

	            	if ( amalinksproChosenItem.ImageSets ) {

	            		if ( amalinksproChosenItem.ImageSets.ImageSet ) {

			            	var image_set = amalinksproChosenItem.ImageSets.ImageSet;

			            	for (var key in image_set) {

			            		if ( image_set[key].MediumImage ) {
			            			var img_src = image_set[key].MediumImage.URL;
			            		}
			            		else if ( image_set[key].SmallImage ) {
			            			var img_src = image_set[key].SmallImage.URL;
			            		}


								if ( img_src === amalinksproChosenItem.MediumImage.URL ) {
									new_image_choices += '<div class="amalinkspro-choose-api-image amalinkspro-chosen-api-img">';
									new_image_choices += '<span class="amalinkspro-chosen-size" style="display:inline-block;">medium</span>';
								}
								else {
									new_image_choices += '<div class="amalinkspro-choose-api-image">';
									new_image_choices += '<span class="amalinkspro-chosen-size"></span>';
								}

								

				    			new_image_choices += '<img src="'+img_src+'" alt="" />';
				    			new_image_choices += '<div class="amalinkspro-choose-api-image-cover js-amalinkspro-select-image">';

				    			if ( image_set[key].LargeImage ) {
					    			new_image_choices += '<span class="js-amalinkspro-select-image-size" data-img-size-url="'+image_set[key].LargeImage.URL+'">large</span>';
					    		}
					    		if ( image_set[key].MediumImage ) {
					    			new_image_choices += '<span class="js-amalinkspro-select-image-size" data-img-size-url="'+image_set[key].MediumImage.URL+'">medium</span>';
					    		}
					    		if ( image_set[key].SmallImage ) {
					    			new_image_choices += '<span class="js-amalinkspro-select-image-size" data-img-size-url="'+image_set[key].SmallImage.URL+'">small</span>';
					    		}
					    		if ( image_set[key].SwatchImage ) {
					    			new_image_choices += '<span class="js-amalinkspro-select-image-size" data-img-size-url="'+image_set[key].SwatchImage.URL+'">tiny</span>';
					    		}
				    			new_image_choices += '</div class="js-amalinkspro-select-image-size">';

				    			new_image_choices += '</div>';

							}
							new_image_choices += '<div class="amalinkspro-clear"></div>';

						}
						else {
							new_image_choices += '<div class="amalinkspro-clear"></div>';
						}

					}
					else {
						new_image_choices += '<h3 class="amalinkspro-clear">Sorry, there are no image sets</h3>';
					}

	            	$('.amalinkspro-linktype-preview-image-link-edit-imagechoices').html(new_image_choices);





	            	/* Set CAT Link Values */

					var par = $('.amalinkspro-linktype-preview-cta-link');
					par.find('.amalinkspro-cta-link-link').text( amalinksproChosenItem.ItemAttributes.Title );
					par.find('.amalinkspro-cta-image-link').attr( 'alt', amalinksproChosenItem.ItemAttributes.Title );
	            	par.find('.amalinkspro-cta-link-link').attr( 'href', amalinksproChosenItem.DetailPageURL );
	            	par.find('input[name=amalinkspro-edit-ctalink-text]').val( amalinksproChosenItem.ItemAttributes.Title );

	            	par.attr( 'data-amalinkspro-asin', amalinksproChosenItem.ASIN );
	            	par.attr( 'data-amalinkspro-ctatext', amalinksproChosenItem.ItemAttributes.Title );
					

	            	


	            },
	            complete: function(response){

	            	$('.js-amalinkspro-choose-product').removeClass('amalinkspro-next-action');
					$('.js-amalinkspro-choose-link-type').addClass('amalinkspro-next-action');

					$('.amalinkspro-loading-overlay').fadeOut(300);


	    //         	var item = $('.amalinkspro-chosen-product');

	    //         	var xxx = 'test';

					// item.attr('data-amalinkspro-apidata-name',xxx );
					// item.attr('data-amalinkspro-apidata-asin',xxx );
					// item.attr('data-amalinkspro-apidata-brand',xxx );
					// item.attr('data-amalinkspro-apidata-model',xxx );
					// item.attr('data-amalinkspro-apidata-upc',xxx );
					// item.attr('data-amalinkspro-apidata-warranty',xxx );
					// item.attr('data-amalinkspro-apidata-lowestnewprice',xxx );
					// item.attr('data-amalinkspro-apidata-lowestusedprice',xxx );
					// item.attr('data-amalinkspro-apidata-listprice',xxx );
					// item.attr('data-amalinkspro-apidata-bestoffer',xxx );
					// item.attr('data-amalinkspro-apidata-percentageoff',xxx );
					// item.attr('data-amalinkspro-apidata-amountoff',xxx );

					//console.log(amalinksproChosenItem);
			    	

				    
	            }
	        });

		}









		// 
		$('body').on('click', '.js-amalinkspro-choose-link-type:not(.amalinkspro-linktype-active)', function(e) {
			console.log('chose this product');

			e.preventDefault();

			$('.js-amalinkspro-choose-link-type').removeClass('amalinkspro-linktype-active');
			$(this).addClass('amalinkspro-linktype-active');

			var link_type = $(this).attr('data-link-type');

			$('.amalinkspro-linktype-preview').removeClass('amalinkspro-chosen-linktype').fadeOut(100);

			$('.amalinkspro-linktype-preview-'+link_type).addClass('amalinkspro-chosen-linktype').fadeIn(100);

			$('.amalinkspro-goto-step3').fadeIn(100);

			
			$('.js-amalinkspro-choose-link-type').removeClass('amalinkspro-next-action');
			$('.js-amalinkspro-final-step').addClass('amalinkspro-next-action');


			$('.amalinkspro-step-3').addClass('amalinkspro-step-disabled');

			$('#amalinkspro-step-3').find('.amalinkspro-step3-final-preview').html('');


		});





		

		// function get_text_link_data() {

		// 	var par = $('.amalinkspro-linktype-preview-text-link');

		// 	par.find('.amalinkspro-text-link-preview-p').find('a').text( amalinksproChosenItem.ItemAttributes.Title );
		// 	par.find('.amalinkspro-text-link-preview-p').find('a').attr( 'href', amalinksproChosenItem.DetailPageURL );
		// 	par.find('input[name=amalinkspro-edit-textlink-text]').val(amalinksproChosenItem.ItemAttributes.Title);
			

		// }


		// 
		$('body').on('click', '.js-amalinkspro-edit-link', function() {
			console.log('worked fool');
			if ( !$(this).hasClass('edit-box-open') ) {

				$('.amalinkspro-choose-api-image').matchHeight();
				console.log('height matched?');

				$(this).addClass('edit-box-open');
				$(this).closest('.amalinkspro-linktype-preview').find('.amalinkspro-linktype-preview-edit').slideDown(100);

				
			}
			else {
				
				$(this).removeClass('edit-box-open');
				$(this).closest('.amalinkspro-linktype-preview').find('.amalinkspro-linktype-preview-edit').slideUp(100);
			}
			
		});






		$('input[name="amalinkspro-edit-textlink-text"]').keyup(function() {
			var new_textlink_val = $(this).val();
			$(this).closest('.amalinkspro-linktype-preview').find('.amalinkspro-text-link-preview-p a').text(new_textlink_val);
			$(this).closest('.amalinkspro-linktype-preview').attr( 'data-amalinkspro-linktext', new_textlink_val );
		});
		
		

		

		


		// 
		$('body').on('click', '.js-amalinkspro-select-image-size', function(e) {
			e.preventDefault();
			console.log('chose this image');

			$('.amalinkspro-choose-api-image').removeClass('amalinkspro-chosen-api-img');
			$(this).closest('.amalinkspro-choose-api-image').addClass('amalinkspro-chosen-api-img');  

			var chosen_img = $(this).attr('data-img-size-url');

			$(this).closest('.amalinkspro-linktype-preview').find('.amalinkspro-image-link-img').attr('src', chosen_img );

			$(this).closest('.amalinkspro-linktype-preview-edit').find('.js-amalinkspro-edit-image-link').attr('data-chosen-img-src',chosen_img );

			var size_text = $(this).text();
			$('.amalinkspro-chosen-size').text( '' ).css('display', 'none');
			$(this).closest('.amalinkspro-choose-api-image').find('.amalinkspro-chosen-size').text( size_text ).css('display', 'inline-block');


		});

		// 
		// $('body').on('click', '.js-amalinkspro-edit-image-link', function() {
		// 	console.log('Text link in preview updated');
			
		// 	var new_url = $(this).attr('data-chosen-img-src');

		// 	$(this).closest('.amalinkspro-linktype-preview').find('.amalinkspro-image-link-img').attr('src', new_url );

		// 	//$(this).closest('.amalinkspro-linktype-preview-edit').slideUp(100);
		// 	//$(this).closest('.amalinkspro-linktype-preview').find('.amalinkspro-edit-link').removeClass('edit-box-open');

		// 	$('.amalinkspro-choose-api-image').matchHeight();

		// 	$(this).closest('.amalinkspro-linktype-preview').attr( 'data-amalinkspro-linkimage', new_url );
			
		// });



		// 
		$('body').on('click', '.js-amalinkspro-edit-cta-link', function() {
			console.log('CTA link in preview updated');

			var new_textlink_val = $(this).closest('.amalinkspro-linktype-preview-edit').find('input[name=amalinkspro-edit-ctalink-text]').val();

			var cta_wrap = $(this).closest('.amalinkspro-linktype-preview').find('.amalinkspro-cta-wrap-preview');

			if ( !cta_wrap.hasClass('amalinkspro-cta-button-img') ) {
				$(this).closest('.amalinkspro-linktype-preview').find('.amalinkspro-cta-link-link').text(new_textlink_val);
			}

			$(this).closest('.amalinkspro-linktype-preview').attr('data-amalinkspro-ctatext',new_textlink_val );

			$(this).closest('.amalinkspro-linktype-preview-edit').slideUp(100);
			$(this).closest('.amalinkspro-linktype-preview').find('.amalinkspro-edit-link').removeClass('edit-box-open');

			
		});


		$('input[name="amalinkspro-edit-ctalink-text"]').keyup(function() {
			var new_textlink_val = $(this).val();
			var cta_wrap = $(this).closest('.amalinkspro-linktype-preview').find('.amalinkspro-cta-wrap-preview');

			if ( !cta_wrap.hasClass('amalinkspro-cta-button-img') ) {
				$(this).closest('.amalinkspro-linktype-preview').find('.amalinkspro-cta-link-link').text(new_textlink_val);
			}
			$(this).closest('.amalinkspro-linktype-preview').attr( 'data-amalinkspro-ctatext', new_textlink_val );
		});




		

		// 
		$('body').on('click', '.js-select-cta-style', function(e) {
			e.preventDefault();
			console.log('CTA link in preview updated');

			

			if ( $(this).hasClass('amalinkspro-cta-image-link') ) {
				var new_cta_val = $(this).find('img').clone();
				$(this).closest('.amalinkspro-linktype-preview').attr('data-amalinkspro-ctastyle', 'cta-btn-image');
				$(this).closest('.amalinkspro-linktype-preview').attr('data-amalinkspro-ctaimage', );
				$(this).closest('.amalinkspro-linktype-preview').attr('data-amalinkspro-ctaimage', new_cta_val.attr('src') );
			}
			else {
				var new_cta_val = $(this).closest('.amalinkspro-linktype-preview-edit').find('input[name=amalinkspro-edit-ctalink-text]').val();
				$(this).closest('.amalinkspro-linktype-preview').attr('data-amalinkspro-ctastyle', 'cta-btn-text');
				$(this).closest('.amalinkspro-linktype-preview').attr('data-amalinkspro-ctaimage', '' );
			}
			$('.amalinkspro-cta-wrap-preview a').html( new_cta_val );

			var new_cta_class = 'amalinkspro-cta-wrap-preview amalinkspro-cta-wrap ' + $(this).attr('data-amalinkspro-cta-class');
			$('.amalinkspro-cta-wrap-preview').attr('class',new_cta_class);

			$(this).closest('.amalinkspro-linktype-preview').attr('data-amalinkspro-ctaclass', $(this).attr('data-amalinkspro-cta-class') );

			// $(this).closest('.amalinkspro-linktype-preview').attr('data-amalinkspro-ctaimage', $(this).find('img').attr('src') );
			
		});







		// 
		$('body').on('click', '.js-amalinkspro-final-step', function(e) {
			
			e.preventDefault();
			console.log('Finalizing link in step 3');

			$('.amalinkspro-step-3').addClass('amalinkspro-step-active');
	        $('#amalinkspro-step-2').hide();

        	$('.amalinkspro-step-2').removeClass('amalinkspro-step-active');
        	$('#amalinkspro-step-3').show();

        	$('#amalinkspro-media-window-content').animate({ scrollTop: 0 }, 0);


        	$('.amalinkspro-step-3').removeClass('amalinkspro-step-disabled');

        	var final_preview = $(this).closest('.amalinkspro-step-wrap').find('.amalinkspro-chosen-linktype').clone();
        	

        	final_preview.find('.js-amalinkspro-edit-link').remove().end();
        	final_preview.find('.amalinkspro-linktype-preview-edit').remove()


        	$('.amalinkspro-step3-final-preview').html(final_preview);

        	$(this).removeClass('amalinkspro-next-action');
			$('.js-amalinkspro-insert-final-link-shortcode').addClass('amalinkspro-next-action');


		});



		// 
		$('body').on('click', '.amalinkspro-infobox-option-cover', function(e) {
			
			e.preventDefault();
			console.log('Chose Info Block Layout');

			console.log(amalinksproChosenItem);

			var clone_type = $(this).closest('.amalinkspro-infobox-option').attr('data-amalinkspro-infobox-type');
			var element = '#'+clone_type;
			// var clone = $(element).clone();

			$('.amalinkspro-infobox-layout-data-infobox').find('.amalinkspro-infobox-option').hide();
			$('.amalinkspro-infobox-layout-data-infobox').find(element).show();
			$('.amalinkspro-infobox-layout-data-infobox').fadeIn(200);

			if ( amalinksproChosenItem ) {

				var Title = amalinksproChosenItem.ItemAttributes.Title;
				var SmallImage = amalinksproChosenItem.SmallImage.URL;
				var MediumImage = amalinksproChosenItem.MediumImage.URL;
				var FormattedPrice = amalinksproChosenItem.ItemAttributes.ListPrice.FormattedPrice;
				var AmountSavedPrice = amalinksproChosenItem.Offers.Offer.OfferListing.Price.FormattedPrice;
				var AmountSaveCurrency = amalinksproChosenItem.Offers.Offer.OfferListing.AmountSaved.FormattedPrice;
				var AmountSavedCurrency = amalinksproChosenItem.Offers.Offer.OfferListing.AmountSaved.CurrencyCode;
				var PercentageSaved = amalinksproChosenItem.Offers.Offer.OfferListing.PercentageSaved;
				var PrimeAvailable = amalinksproChosenItem.Offers.Offer.OfferListing.IsEligibleForPrime;

				var asin = amalinksproChosenItem.ASIN;
				var prod_url = amalinksproChosenItem.DetailPageURL;

				var price = '';
				var prime_logo = '';

				if ( AmountSavedPrice ) {
					price = AmountSavedPrice;
				}
				else if ( FormattedPrice ) {
					price = FormattedPrice;
				}

				if ( PercentageSaved ) {
					price += '<br />'+PercentageSaved+'% Off';
				}

				if ( PrimeAvailable == 1 ) {
					//prime_logo = '<span class="prime-logo"><img src="' + plugins_url() + '/amalinkspro/includes/images/amazon-prime.png" alt="Amazon Prime Available" /></span>';
					prime_logo = 'prime';
				}
				else {
					prime_logo = 'X';
				}

				$('.amalinkspro-infobox-layout-data-infobox').find('.amalinkspro-item-title').text(Title);
				$('.amalinkspro-infobox-layout-data-infobox').find('.amalinkspro-item-price').html(price);
				$('.amalinkspro-infobox-layout-data-infobox').find('.amalinkspro-image-link').attr('src', SmallImage );

			}


		});



		$( ".amalinkspro-infobox-layout-data-options ul li" ).draggable({ 
	        helper: "clone",
	        cursor: "crosshair"
	    });


	    $( ".amalinkspro-drop-zone" ).droppable({ 
	        drop: Drop,
	        tolerance: "pointer"
	    });

	    function Drop(event, ui) {
	    	//console.log('test');

	    	var dataString = '';

	    	var dataType =  ui.draggable.attr("data-amalinkspro-datapoint");

	    	var dataString = get_amazon_api_data(dataType);

	    	console.log(dataString);

	    	// if ( dataType == 'title' ) {
	    	// 	dataString = Title;
	    	// }

	    	var item_value_target = $(this).find('.amalinkspro-item-drop');
	    	item_value_target.html(dataString['value']);

	    	var item_value_label = $(this).find('.amalinkspro-item-label');
	    	item_value_label.html(dataString['label']);
	    }

	    function get_amazon_api_data(dataType) {

	    	var dataString = '';

	    	if ( amalinksproChosenItem ) {

				var Title = amalinksproChosenItem.ItemAttributes.Title;


				
				var PrimeAvailable = amalinksproChosenItem.Offers.Offer.OfferListing.IsEligibleForPrime;
				var prime_logo = 'prime logo';

				if ( PrimeAvailable == 1 ) {
					//prime_logo = '<span class="prime-logo"><img src="' + plugins_url() + '/amalinkspro/includes/images/amazon-prime.png" alt="Amazon Prime Available" /></span>';
					prime_logo = 'prime logo';
				}
				else {
					prime_logo = 'X';
				}

				var Brand = amalinksproChosenItem.ItemAttributes.Brand;
				var Model = amalinksproChosenItem.ItemAttributes.Model;


				var SmallImage = amalinksproChosenItem.SmallImage.URL;

				var Features = amalinksproChosenItem.ItemAttributes.Feature;
				var featuresString = '';

				if ( Features ) {

					featuresString += '<ul>';

						for (var i = 0, len = Features.length; i < len; i++) {

							featuresString += '<li>';
							featuresString += Features[i];
							featuresString += '</li>';
						}

					featuresString += '</ul>';

				}

					



				var MediumImage = amalinksproChosenItem.MediumImage.URL;

				var FormattedPrice = amalinksproChosenItem.ItemAttributes.ListPrice.FormattedPrice;
				var AmountSavedPrice = amalinksproChosenItem.Offers.Offer.OfferListing.Price.FormattedPrice;
				var AmountSaveCurrency = amalinksproChosenItem.Offers.Offer.OfferListing.AmountSaved.FormattedPrice;
				var AmountSavedCurrency = amalinksproChosenItem.Offers.Offer.OfferListing.AmountSaved.CurrencyCode;
				var PercentageSaved = amalinksproChosenItem.Offers.Offer.OfferListing.PercentageSaved;
				

				var asin = amalinksproChosenItem.ASIN;
				var prod_url = amalinksproChosenItem.DetailPageURL;

				var price = '';
				

				if ( AmountSavedPrice ) {
					price = AmountSavedPrice;
				}
				else if ( FormattedPrice ) {
					price = FormattedPrice;
				}

				if ( PercentageSaved ) {
					price += '<br />'+PercentageSaved+'% Off';
				}

				
				dataString = [];
				
				if ( dataType == 'title' ) { 
					dataString['value'] = Title; 
					dataString['label'] = ''; 
				}
				else if ( dataType == 'prime' ) { 
					dataString['value'] = prime_logo; 
					dataString['label'] = ''; 
				}
				else if ( dataType == 'brand' ) { 
					dataString['value'] = Brand; 
					dataString['label'] = 'Brand:'; 
				}
				else if ( dataType == 'model' ) { 
					dataString['value'] = Model; 
					dataString['label'] = 'Model:'; 
				}
				else if ( dataType == 'image' ) { 
					dataString['value'] = '<img src="'+SmallImage+'" alt="'+Title+'" />'; 
					dataString['label'] = Title; 
				}
				else if ( dataType == 'features' ) { 
					dataString['value'] = featuresString; 
					dataString['label'] = 'Features:'; 
				}
				else if ( dataType == 'lowest-new-price' ) { 
					dataString['value'] = Title; 
					dataString['label'] = 'Lowest New Price:'; 
				}
				else if ( dataType == 'lowest-used-price' ) { 
					dataString['value'] = Title; 
					dataString['label'] = 'Lowest Used price'; 
				}

				else { 
					dataString['value'] = 'invalid api data type'; 
					dataString['label'] = '';
				}

			}

			else {
				dataString['value'] = 'Error: no Amazon API item';
				dataString['label'] = '';
			}

	    	return dataString;

	    }




	    

	    // submit amazon api search on review admin pedit screen
		$('body').on( 'click', '.amalinkspro-column-data .icon-amalinkspro-edit', function(){

			if ( !$(this).hasClass('edit-box-open') ) {

				$(this).addClass('edit-box-open');
				$(this).closest('.amalinkspro-column-data').next('.amalinkspro-edit-data').slideDown(100);

			}
			else {

				$(this).removeClass('edit-box-open');
				$(this).closest('.amalinkspro-column-data').next('.amalinkspro-edit-data').slideUp(100);

			}

		});











		/************** CTA BUTTON GENERATOR ******************/




		$('body').on('click', '.amalinkspro-hover-trigger', function() {
			if ( $(this).hasClass('hover-panel-open') ) {
				$(this).closest('.amalinkspro-button-generator-controls-section-body-hover').css('transform', 'translateX( 100% )');
				$(this).removeClass('hover-panel-open');
				$(this).find('i').removeClass('icon-amalinkspro-right-hand').addClass('icon-amalinkspro-left-hand');
				$(this).animate({
					lineHeight: '35px',
					left: '-35',
					width: '35px',
					height: '35px',
					}, 300, function() {
					// Animation complete.
				});
				$(this).closest('.amalinkspro-button-generator-controls-section').find('.title-hover-label').removeClass('amalinkspro-visible');
			}
			else {
				$(this).closest('.amalinkspro-button-generator-controls-section-body-hover').css('transform', 'translateX( 0% )');
				$(this).addClass('hover-panel-open');
				$(this).find('i').removeClass('icon-amalinkspro-left-hand').addClass('icon-amalinkspro-right-hand');
				$(this).animate({
					lineHeight: '25px',
					left: '0',
					width: '25px',
					height: '25px',
					}, 300, function() {
					// Animation complete.
				});
				$(this).closest('.amalinkspro-button-generator-controls-section').find('.title-hover-label').addClass('amalinkspro-visible');

			}

		});


		$('body').on('click', '.amalinkspro-button-generator-controls-section h3', function() {

			var controls_body =  $(this).closest('.amalinkspro-button-generator-controls-section').find('.amalinkspro-button-generator-controls-section-body');

			if ( $(this).hasClass('cta-btn-box-open') ) {
				controls_body.slideUp(300);
				$(this).removeClass('cta-btn-box-open');
			}
			else {
				controls_body.slideDown(300);
				$(this).addClass('cta-btn-box-open');
			}

			$(this).find('i').toggleClass('icon-amalinkspro-up-circle');
			$(this).find('i').toggleClass('icon-amalinkspro-down-circle');
				
		});



		$('.alp-control-font-family-system').select2({
			placeholder: 'Select a Font',
			allowClear: true,
			containerCssClass: "alp-control-font-family-system-select2"
		});

		$( ".alp-control-font-family-system" ).on( "change", function(  ) {
			var font_family_selection = $(this).val();
			$('#amalinkspro-button-generator-preview').find('.amalinkspro-css-button').css('fontFamily', font_family_selection );
			
		} );

		$('body').on('click', '.alp-control-enable-googlefonts', function() {
			
			if ( $(this).is(':checked') ) {
				$('.system-select-wrapper').hide();
				$('.google-select-wrapper').show();
				console.log('checked');
			}
			else {
				$('.google-select-wrapper').hide();
				$('.system-select-wrapper').show();
				console.log('not checked');
			}
		});

		$('.alp-control-font-family-google').select2({
			placeholder: 'Select a Google Font',
			allowClear: true,
			containerCssClass: "alp-control-font-family-google-select2"
		});

		$( ".alp-control-font-family-google" ).on( "change", function(  ) {
			var font_family_selection = $(this).val();
			$('#amalinkspro-button-generator-preview').find('.amalinkspro-css-button').css('fontFamily', font_family_selection );
			
		} );



	


		$( ".alp-control-font-size" ).slider({
		  "min": 10,
		  "max": 60,
		  "value": 16,
		  "step": 1
		});

		$( ".alp-control-font-size" ).find('.ui-slider-handle').text( '16px' );
		
		$( ".alp-control-font-size" ).on( "slide", function( event, ui ) {
			$('#amalinkspro-button-generator-preview').find('.amalinkspro-css-button').css('fontSize', ui.value+'px');
			$(this).find('.ui-slider-handle').text(ui.value+'px');
		} );



		$('.alp-control-font-color').wpColorPicker({
			change: function(event, ui) {
		        // change the headline color
		        $('#amalinkspro-button-generator-preview').find('.amalinkspro-css-button').css('color', ui.color.toString() );
		        
		    }
		});


		$( ".alp-control-font-size-hover" ).slider({
		  "min": 10,
		  "max": 60,
		  "value": 16,
		  "step": 1
		});

		$( ".alp-control-font-size-hover" ).find('.ui-slider-handle').text( '16px' );
		
		$( ".alp-control-font-size-hover" ).on( "slide", function( event, ui ) {
			//$('#amalinkspro-button-generator-preview').find('.amalinkspro-css-button').css('fontSize', ui.value+'px');
			$(this).find('.ui-slider-handle').text(ui.value+'px');
		} );



		$('.alp-control-font-color-hover').wpColorPicker({
			change: function(event, ui) {
		        // change the headline color
		        //$('#amalinkspro-button-generator-preview').find('.amalinkspro-css-button').css('color', ui.color.toString() );
		        
		    }
		});


		



		// $( ".alp-control-textshadow-enable" ).on( "change", function(  ) {
		// 	var text_shadow_selection = $(this).val();
		// 	$(this).closest('.amalinkspro-button-generator-controls-section-body-bottom').find('.amalinkspro-button-generator-col-hidden').css('fontFamily', text_shadow_selection );
			
		// } );

		$('body').on('click', '.alp-control-textshadow-enable', function() {
			
			if ( $(this).is(':checked') ) {
				$('.amalinkspro-button-generator-controls-section-body-bottom').find('.amalinkspro-button-generator-col-hidden').show();
				$('.amalinkspro-button-generator-controls-section-body-hover-bottom').find('.amalinkspro-button-generator-col-hidden').show();

				var color = $( "input.alp-control-textshadow-color" ).val();
		        var x = $( ".alp-control-textshadow-x" ).slider("option", "value");
		        var y = $( ".alp-control-textshadow-y" ).slider("option", "value");
		        var blur = $( ".alp-control-textshadow-blur" ).slider("option", "value");

		        var text_shadow_css = x + 'px ' + y + 'px ' + blur + 'px ' + color;

				$('#amalinkspro-button-generator-preview').find('.amalinkspro-css-button').css('text-shadow', text_shadow_css );
			}
			else {
				$('.amalinkspro-button-generator-controls-section-body-bottom').find('.amalinkspro-button-generator-col-hidden').hide();
				$('.amalinkspro-button-generator-controls-section-body-hover-bottom').find('.amalinkspro-button-generator-col-hidden').hide();
				$('#amalinkspro-button-generator-preview').find('.amalinkspro-css-button').css('text-shadow', '' );
			}
		});




		$('.alp-control-textshadow-color').wpColorPicker({
			change: function(event, ui) {

		        var color = ui.color.toString();
		        var x = $( ".alp-control-textshadow-x" ).slider("option", "value");
		        var y = $( ".alp-control-textshadow-y" ).slider("option", "value");
		        var blur = $( ".alp-control-textshadow-blur" ).slider("option", "value");

		        var text_shadow_css = x + 'px ' + y + 'px ' + blur + 'px ' + color;
		        console.log(text_shadow_css);
		        // change the headline color
		        $('#amalinkspro-button-generator-preview').find('.amalinkspro-css-button').css('text-shadow', text_shadow_css );
		        
		    }
		});


		$( ".alp-control-textshadow-x" ).slider({
		  "min": -30,
		  "max": 30,
		  "value": 2,
		  animate: "fast"
		});

		$( ".alp-control-textshadow-x" ).find('.ui-slider-handle').text( '2px' );

		$( ".alp-control-textshadow-x" ).on( "slide", function( event, ui ) {
			//$(this).closest('.amalinkspro-button-generator').find('.amalinkspro-css-button').css('borderRadius', ui.value+'px');
			$(this).find('.ui-slider-handle').text(ui.value+'px');
			
			var color = $( "input.alp-control-textshadow-color" ).val();
	        var x = ui.value;
	        var y = $( ".alp-control-textshadow-y" ).slider("option", "value");
	        var blur = $( ".alp-control-textshadow-blur" ).slider("option", "value");

	        var text_shadow_css = x + 'px ' + y + 'px ' + blur + 'px ' + color;

			$('#amalinkspro-button-generator-preview').find('.amalinkspro-css-button').css('text-shadow', text_shadow_css );
		} );


		$( ".alp-control-textshadow-y" ).slider({
		  "min": -30,
		  "max": 30,
		  "value": 2,
		  animate: "fast"
		});

		$( ".alp-control-textshadow-y" ).find('.ui-slider-handle').text( '2px' );

		$( ".alp-control-textshadow-y" ).on( "slide", function( event, ui ) {
			//$(this).closest('.amalinkspro-button-generator').find('.amalinkspro-css-button').css('borderRadius', ui.value+'px');
			$(this).find('.ui-slider-handle').text(ui.value+'px');
			var color = $( "input.alp-control-textshadow-color" ).val();
	        var x = $( ".alp-control-textshadow-x" ).slider("option", "value");
	        var y = ui.value;
	        var blur = $( ".alp-control-textshadow-blur" ).slider("option", "value");

	        var text_shadow_css = x + 'px ' + y + 'px ' + blur + 'px ' + color;

			$('#amalinkspro-button-generator-preview').find('.amalinkspro-css-button').css('text-shadow', text_shadow_css );
		} );


		$( ".alp-control-textshadow-blur" ).slider({
		  "min": 0,
		  "max": 30,
		  "value": 3,
		  animate: "fast"
		});

		$( ".alp-control-textshadow-blur" ).find('.ui-slider-handle').text( '3px' );

		$( ".alp-control-textshadow-blur" ).on( "slide", function( event, ui ) {
			//$(this).closest('.amalinkspro-button-generator').find('.amalinkspro-css-button').css('borderRadius', ui.value+'px');
			$(this).find('.ui-slider-handle').text(ui.value+'px');
			var color = $( "input.alp-control-textshadow-color" ).val();
	        var x = $( ".alp-control-textshadow-x" ).slider("option", "value");
	        var y = $( ".alp-control-textshadow-y" ).slider("option", "value");
	        var blur = ui.value;

	        var text_shadow_css = x + 'px ' + y + 'px ' + blur + 'px ' + color;

			$('#amalinkspro-button-generator-preview').find('.amalinkspro-css-button').css('text-shadow', text_shadow_css );
		} );






		$('.alp-control-textshadow-color-hover').wpColorPicker({
			change: function(event, ui) {

		        var color = ui.color.toString();
		        var x = $( ".alp-control-textshadow-x" ).slider("option", "value");
		        var y = $( ".alp-control-textshadow-y" ).slider("option", "value");
		        var blur = $( ".alp-control-textshadow-blur" ).slider("option", "value");

		        var text_shadow_css = x + 'px ' + y + 'px ' + blur + 'px ' + color;
		        console.log(text_shadow_css);
		        // change the headline color
		        //$('#amalinkspro-button-generator-preview').find('.amalinkspro-css-button').css('text-shadow', text_shadow_css );
		        
		    }
		});


		$( ".alp-control-textshadow-x-hover" ).slider({
		  "min": -30,
		  "max": 30,
		  "value": 2,
		  animate: "fast"
		});

		$( ".alp-control-textshadow-x-hover" ).find('.ui-slider-handle').text( '2px' );

		$( ".alp-control-textshadow-x-hover" ).on( "slide", function( event, ui ) {
			//$(this).closest('.amalinkspro-button-generator').find('.amalinkspro-css-button').css('borderRadius', ui.value+'px');
			$(this).find('.ui-slider-handle').text(ui.value+'px');
			
			var color = $( "input.alp-control-textshadow-color" ).val();
	        var x = ui.value;
	        var y = $( ".alp-control-textshadow-y" ).slider("option", "value");
	        var blur = $( ".alp-control-textshadow-blur" ).slider("option", "value");

	        var text_shadow_css = x + 'px ' + y + 'px ' + blur + 'px ' + color;

			//$('#amalinkspro-button-generator-preview').find('.amalinkspro-css-button').css('text-shadow', text_shadow_css );
		} );


		$( ".alp-control-textshadow-y-hover" ).slider({
		  "min": -30,
		  "max": 30,
		  "value": 2,
		  animate: "fast"
		});

		$( ".alp-control-textshadow-y-hover" ).find('.ui-slider-handle').text( '2px' );

		$( ".alp-control-textshadow-y-hover" ).on( "slide", function( event, ui ) {
			//$(this).closest('.amalinkspro-button-generator').find('.amalinkspro-css-button').css('borderRadius', ui.value+'px');
			$(this).find('.ui-slider-handle').text(ui.value+'px');
			var color = $( "input.alp-control-textshadow-color" ).val();
	        var x = $( ".alp-control-textshadow-x" ).slider("option", "value");
	        var y = ui.value;
	        var blur = $( ".alp-control-textshadow-blur" ).slider("option", "value");

	        var text_shadow_css = x + 'px ' + y + 'px ' + blur + 'px ' + color;

			//$('#amalinkspro-button-generator-preview').find('.amalinkspro-css-button').css('text-shadow', text_shadow_css );
		} );


		$( ".alp-control-textshadow-blur-hover" ).slider({
		  "min": 0,
		  "max": 30,
		  "value": 3,
		  animate: "fast"
		});

		$( ".alp-control-textshadow-blur-hover" ).find('.ui-slider-handle').text( '3px' );

		$( ".alp-control-textshadow-blur-hover" ).on( "slide", function( event, ui ) {
			//$(this).closest('.amalinkspro-button-generator').find('.amalinkspro-css-button').css('borderRadius', ui.value+'px');
			$(this).find('.ui-slider-handle').text(ui.value+'px');
			var color = $( "input.alp-control-textshadow-color" ).val();
	        var x = $( ".alp-control-textshadow-x" ).slider("option", "value");
	        var y = $( ".alp-control-textshadow-y" ).slider("option", "value");
	        var blur = ui.value;

	        var text_shadow_css = x + 'px ' + y + 'px ' + blur + 'px ' + color;

			//$('#amalinkspro-button-generator-preview').find('.amalinkspro-css-button').css('text-shadow', text_shadow_css );
		} );












		$( ".alp-control-padding-top" ).slider({
		  "min": 5,
		  "max": 60,
		  "value": 15,
		  animate: "fast"
		});

		$( ".alp-control-padding-top" ).find('.ui-slider-handle').text( '15px' );

		$( ".alp-control-padding-top" ).on( "slide", function( event, ui ) {
			//$(this).closest('.amalinkspro-button-generator').find('.amalinkspro-css-button').css('borderRadius', ui.value+'px');
			$(this).find('.ui-slider-handle').text(ui.value+'px');
			$('#amalinkspro-button-generator-preview').find('.amalinkspro-css-button').css('padding-top', ui.value );
		} );

		$( ".alp-control-padding-right" ).slider({
		  "min": 5,
		  "max": 60,
		  "value": 15,
		  animate: "fast"
		});

		$( ".alp-control-padding-right" ).find('.ui-slider-handle').text( '15px' );

		$( ".alp-control-padding-right" ).on( "slide", function( event, ui ) {
			//$(this).closest('.amalinkspro-button-generator').find('.amalinkspro-css-button').css('borderRadius', ui.value+'px');
			$(this).find('.ui-slider-handle').text(ui.value+'px');
			$('#amalinkspro-button-generator-preview').find('.amalinkspro-css-button').css('padding-right', ui.value );
		} );

		$( ".alp-control-padding-bottom" ).slider({
		  "min": 5,
		  "max": 60,
		  "value": 15,
		  animate: "fast"
		});

		$( ".alp-control-padding-bottom" ).find('.ui-slider-handle').text( '15px' );

		$( ".alp-control-padding-bottom" ).on( "slide", function( event, ui ) {
			//$(this).closest('.amalinkspro-button-generator').find('.amalinkspro-css-button').css('borderRadius', ui.value+'px');
			$(this).find('.ui-slider-handle').text(ui.value+'px');
			$('#amalinkspro-button-generator-preview').find('.amalinkspro-css-button').css('padding-bottom', ui.value );
		} );

		$( ".alp-control-padding-left" ).slider({
		  "min": 5,
		  "max": 60,
		  "value": 15,
		  animate: "fast"
		});

		$( ".alp-control-padding-left" ).find('.ui-slider-handle').text( '15px' );

		$( ".alp-control-padding-left" ).on( "slide", function( event, ui ) {
			//$(this).closest('.amalinkspro-button-generator').find('.amalinkspro-css-button').css('borderRadius', ui.value+'px');
			$(this).find('.ui-slider-handle').text(ui.value+'px');
			$('#amalinkspro-button-generator-preview').find('.amalinkspro-css-button').css('padding-left', ui.value );
		} );




		// $( ".alp-control-textshadow-enable" ).on( "change", function(  ) {
		// 	var text_shadow_selection = $(this).val();
		// 	$(this).closest('.amalinkspro-button-generator-controls-section-body-bottom').find('.amalinkspro-button-generator-col-hidden').css('fontFamily', text_shadow_selection );
			
		// } );

		$('body').on('click', '.alp-control-boxshadow-enable', function() {
			
			if ( $(this).is(':checked') ) {
				$(this).closest('.amalinkspro-button-generator-controls-section-body-bottom').find('.amalinkspro-button-generator-col-hidden').show();

				var color = $( "input.alp-control-boxshadow-color" ).val();
		        var x = $( ".alp-control-boxshadow-x" ).slider("option", "value");
		        var y = $( ".alp-control-boxshadow-y" ).slider("option", "value");
		        var blur = $( ".alp-control-boxshadow-blur" ).slider("option", "value");

		        var box_shadow_css = x + 'px ' + y + 'px ' + blur + 'px ' + color;

				$('#amalinkspro-button-generator-preview').find('.amalinkspro-css-button').css('boxShadow', box_shadow_css );
			}
			else {
				$('.amalinkspro-button-generator-controls-section-body-bottom').find('.amalinkspro-button-generator-col-hidden').hide();
				$('#amalinkspro-button-generator-preview').find('.amalinkspro-css-button').css('boxShadow', '' );
			}
		});





		$('.alp-control-boxshadow-color').wpColorPicker({
			change: function(event, ui) {

		        var color = ui.color.toString();
		        var x = $( ".alp-control-boxshadow-x" ).slider("option", "value");
		        var y = $( ".alp-control-boxshadow-y" ).slider("option", "value");
		        var blur = $( ".alp-control-boxshadow-blur" ).slider("option", "value");

		        var box_shadow_css = x + 'px ' + y + 'px ' + blur + 'px ' + color;

				$('#amalinkspro-button-generator-preview').find('.amalinkspro-css-button').css('boxShadow', box_shadow_css );
		        
		    }
		});


		


		$( ".alp-control-boxshadow-x" ).slider({
		  "min": 0,
		  "max": 30,
		  "value": 2,
		  animate: "fast"
		});

		$( ".alp-control-boxshadow-x" ).find('.ui-slider-handle').text( '2px' );

		$( ".alp-control-boxshadow-x" ).on( "slide", function( event, ui ) {
			//$(this).closest('.amalinkspro-button-generator').find('.amalinkspro-css-button').css('borderRadius', ui.value+'px');
			$(this).find('.ui-slider-handle').text(ui.value+'px');
			var color = $( "input.alp-control-boxshadow-color" ).val();
	        var x = ui.value;
	        var y = $( ".alp-control-boxshadow-y" ).slider("option", "value");
	        var blur = $( ".alp-control-boxshadow-blur" ).slider("option", "value");

	        var box_shadow_css = x + 'px ' + y + 'px ' + blur + 'px ' + color;

			$('#amalinkspro-button-generator-preview').find('.amalinkspro-css-button').css('boxShadow', box_shadow_css );
		} );


		$( ".alp-control-boxshadow-y" ).slider({
		  "min": 0,
		  "max": 30,
		  "value": 2,
		  animate: "fast"
		});

		$( ".alp-control-boxshadow-y" ).find('.ui-slider-handle').text( '2px' );

		$( ".alp-control-boxshadow-y" ).on( "slide", function( event, ui ) {
			//$(this).closest('.amalinkspro-button-generator').find('.amalinkspro-css-button').css('borderRadius', ui.value+'px');
			$(this).find('.ui-slider-handle').text(ui.value+'px');
			var color = $( "input.alp-control-boxshadow-color" ).val();
	        var x = $( ".alp-control-boxshadow-x" ).slider("option", "value");
	        var y = ui.value
	        var blur = $( ".alp-control-boxshadow-blur" ).slider("option", "value");

	        var box_shadow_css = x + 'px ' + y + 'px ' + blur + 'px ' + color;

			$('#amalinkspro-button-generator-preview').find('.amalinkspro-css-button').css('boxShadow', box_shadow_css );
		} );


		$( ".alp-control-boxshadow-blur" ).slider({
		  "min": 0,
		  "max": 30,
		  "value": 6,
		  animate: "fast"
		});

		$( ".alp-control-boxshadow-blur" ).find('.ui-slider-handle').text( '3px' );

		$( ".alp-control-boxshadow-blur" ).on( "slide", function( event, ui ) {
			//$(this).closest('.amalinkspro-button-generator').find('.amalinkspro-css-button').css('borderRadius', ui.value+'px');
			$(this).find('.ui-slider-handle').text(ui.value+'px');
			var color = $( "input.alp-control-boxshadow-color" ).val();
	        var x = $( ".alp-control-boxshadow-x" ).slider("option", "value");
	        var y = $( ".alp-control-boxshadow-y" ).slider("option", "value");
	        var blur = ui.value;

	        var box_shadow_css = x + 'px ' + y + 'px ' + blur + 'px ' + color;

			$('#amalinkspro-button-generator-preview').find('.amalinkspro-css-button').css('boxShadow', box_shadow_css );
		} );











		$( ".alp-control-border-radius" ).slider({
		  "min": 1,
		  "max": 360,
		  "value": 15,
		  animate: "fast"
		});

		$( ".alp-control-border-radius" ).find('.ui-slider-handle').text( '15px' );

		$( ".alp-control-border-radius" ).on( "slide", function( event, ui ) {
			$('#amalinkspro-button-generator-preview').find('.amalinkspro-css-button').css('borderRadius', ui.value+'px');
			$(this).find('.ui-slider-handle').text(ui.value+'px');
		} );





		$( ".alp-control-border-width" ).slider({
		  "min": 1,
		  "max": 10,
		  "value": 1,
		  animate: "fast"
		});

		$( ".alp-control-border-width" ).find('.ui-slider-handle').text( '1px');



		$( ".alp-control-border-width" ).on( "slide", function( event, ui ) {
			$('#amalinkspro-button-generator-preview').find('.amalinkspro-css-button').css('borderWidth', ui.value+'px');
			$(this).find('.ui-slider-handle').text(ui.value+'px');
		} );


		$('.amalinkspro-border-picker').iris({
			palettes: true,
			change: function(event, ui) {
		        // event = standard jQuery event, produced by whichever control was changed.
		        // ui = standard jQuery UI object, with a color member containing a Color.js object

		        // change the headline color
		        $(this).closest('#amalinkspro-button-generator-preview').find('.amalinkspro-css-button').css('borderColor', ui.color.toString() );
		        
		    }
		});



		$('.alp-control-border-color').wpColorPicker({
			change: function(event, ui) {
		        // event = standard jQuery event, produced by whichever control was changed.
		        // ui = standard jQuery UI object, with a color member containing a Color.js object

		        // change the headline color
		        $('#amalinkspro-button-generator-preview').find('.amalinkspro-css-button').css('borderColor', ui.color.toString() );
		        
		    }
		});





		


		$('body').on('click', '.alp-control-background-type', function() {

			var bg_type = $(this).val();

			// console.log('chose bg type: ');
			// console.log(bg_type);
			
			if ( bg_type == 'bg-solid' ) {
				var bg_color = $( "input.alp-control-bg-solid" ).val();
				$('#amalinkspro-button-generator-preview').find('.amalinkspro-css-button').css('background', bg_color );

				$('.amalinkspro-button-generator-col-bg-top').animate({
					opacity: 0,
					}, 300, function() {
					// Animation complete.
				});
			}
			else {

				$('.amalinkspro-button-generator-col-bg-top').animate({
					opacity: 1,
					}, 300, function() {
					// Animation complete.
				});

				var bg_color_bottom = $( "input.alp-control-bg-solid" ).val();
				var bg_color_top = $( "input.alp-control-bg-top" ).val();
				var bg_gradient = 'linear-gradient(to bottom, ' + bg_color_top + ' 0%,' + bg_color_bottom + ' 100%)';
				$('#amalinkspro-button-generator-preview').find('.amalinkspro-css-button').css('background', bg_gradient );
	
			}
		});



		
		$('.alp-control-bg-solid').wpColorPicker({
			change: function(event, ui) {
		        // event = standard jQuery event, produced by whichever control was changed.
		        // ui = standard jQuery UI object, with a color member containing a Color.js object

		        var bg_type = $('input[name=alp-control-background-type]:checked').val();

		        console.log(bg_type);

		        if ( bg_type == 'bg-solid' ) {
					$('#amalinkspro-button-generator-preview').find('.amalinkspro-css-button').css('background', ui.color.toString() );
				}
				else {

					var bg_color_bottom = ui.color.toString();
					var bg_color_top = $( "input.alp-control-bg-top" ).val();
					var bg_gradient = 'linear-gradient(to bottom, ' + bg_color_top + ' 0%,' + bg_color_bottom + ' 100%)';
					console.log(bg_gradient);
					$('#amalinkspro-button-generator-preview').find('.amalinkspro-css-button').css('background', bg_gradient );
					
				}

		        
		    }
		});

		$('.alp-control-bg-top').wpColorPicker({
			change: function(event, ui) {
		        // event = standard jQuery event, produced by whichever control was changed.
		        // ui = standard jQuery UI object, with a color member containing a Color.js object

		        // change the headline color
		        var bg_type = $('input[name=alp-control-background-type]:checked').val();


		        console.log(bg_type);

		        if ( bg_type == 'bg-gradient' ) {
					var bg_color_bottom = $( "input.alp-control-bg-solid" ).val();
					var bg_color_top = ui.color.toString();
					var bg_gradient = 'linear-gradient(to bottom, ' + bg_color_top + ' 0%,' + bg_color_bottom + ' 100%)';
					console.log(bg_gradient);
					$('#amalinkspro-button-generator-preview').find('.amalinkspro-css-button').css('background', bg_gradient );
				}
		        
		    }
		});




		/************** end CTA BUTTON GENERATOR ******************/



		$( "body" ).on( "click", ".amalinkspro-js-googlefont-info, .amalinkspro-close-tooltip", function( event, ui ) {
			$(this).closest('.amalinkspro-tooltip-wrap').find('.amalinkspro-tooltip').toggleClass('amalinkspro-tooltip-open');
		} );
		



		/*


		http://automattic.github.io/Iris/


		*/





		
		// function update_bordr_radius( event, ui ) {
		// 	console.log(event, ui);
		// }



		
















	});


})( $ );
