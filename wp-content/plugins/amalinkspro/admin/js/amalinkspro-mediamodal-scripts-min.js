(function( $ ) {
	'use strict';


	var amalinksproChosenItem;

	/**
	 * All of the code for your admin-facing JavaScript source
	 * should reside in this file.
	 *
	 * Note: It has been assumed you will write $ code here, so the
	 * $ function reference has been prepared for usage within the scope
	 * of this function.
	 *
	 * This enables you to define handlers, for when the DOM is ready:
	 *
	 * $(function() {
	 *
	 * });
	 *
	 * When the window is loaded:
	 *
	 * $( window ).load(function() {
	 *
	 * });
	 *
	 * ...and/or other possibilities.
	 *
	 * Ideally, it is not considered best practise to attach more than a
	 * single DOM-ready or window-load handler for a particular page.
	 * Although scripts in the WordPress core, Plugins and Themes may be
	 * practising this, we should strive to set a better example in our own work.
	 */

	jQuery(document).ready( function($){ 





		

		// 
		$('body').on('click', '.amalinkspro-step:not(.amalinkspro-step-active, .amalinkspro-step-disabled)', function() {
			console.log('tab clicked');

			$('.amalinkspro-step').removeClass('amalinkspro-step-active');
	        $(this).addClass('amalinkspro-step-active');

	        $('.amalinkspro-step-wrap').hide();

	        var div = $(this).attr( 'data-amalinkspro-step' );

	        $( '#'+div ).show();

	    	$('html, body').animate({ scrollTop: 0 }, 0);
		});





		// submit amazon api search on review admin pedit screen
		$('body').on( 'click', '.js-amalinkspro-mediamodal-search', function(){

			console.log('Amazon API search initiated');

			$('.amalinkspro-search-loading-gif').show();
			$('.amalinkspro-search-results-list').animate({
				opacity: 0.15,
				}, 100, function() {
					// Animation complete.tegrth
			});

			

			var par = $(this).closest('p');
			var term = par.find('#amalinkspro-search-keyword').val();
			var locale = par.find('#amalinkspro-search-locale').val();

			console.log(term,locale);

			var data = {
	            action: 'amalinkspro_find_amazon_products_ajax',
	            term: term,
	            locale: locale,
	        };

	        amazon_api_ajax_load_results(data);

			//console.log(term+' - '+locale);
		});

		// make our ajax call to do the amazon api call in php
		function amazon_api_ajax_load_results(data) {

			$.ajax({
	            type: 'POST',
	            url: ajaxurl,
	            data: data,
	            success: function(response) {
	                console.log('Successful Ajax Response');
	                $('.amalinkspro-search-results h2').fadeIn(100);
	                $(".amalinkspro-search-results-list").html(response);
	            },
	            complete: function(response){
	            	$('.amalinkspro-search-loading-gif').hide();
	            	$('.amalinkspro-search-results-list').animate({
						opacity: 1,
						}, 100, function() {
							// Animation complete.tegrth
					});
	            }
	        });

		}



		// make our ajax call to insert the chosen amazon product into the "chosen" box and save the info in the database for easy viewing in the admin.
		$('body').on( 'click', '.amalinkspro-search-page', function(){

			console.log('New Amazon Results Page Selected');

			$('.amalinkspro-search-loading-gif').show();
			$('.amalinkspro-search-results-list').animate({
				opacity: 0.15,
				}, 100, function() {
					// Animation complete.tegrth
			});

			var par = $(this).closest('.amalinkspro-step-wrap').find('.amalinkspro-popup-form p');
			var term = par.find('#amalinkspro-search-keyword').val();
			var locale = par.find('#amalinkspro-search-locale').val();
			var page = $(this).attr('data-api-page');

			var data = {
	            action: 'amalinkspro_find_amazon_products_ajax',
	            term: term,
	            locale: locale,
	            page: page,
	        };


	        console.log(data);
	        

	    	amazon_api_ajax_load_new_page(data);

		});

		//make our ajax call to do the amazon api call in php
		function amazon_api_ajax_load_new_page(data) {

			$.ajax({
	            type: 'POST',
	            url: ajaxurl,
	            data: data,
	            success: function(response) {
	                console.log('Successful Ajax Response New Page');
	                $(".amalinkspro-search-results-list").html(response);
	            },
	            complete: function(response){
				    $('.amalinkspro-search-loading-gif').hide();
	            	$('.amalinkspro-search-results-list').animate({
						opacity: 1,
						}, 100, function() {
							// Animation complete.tegrth
					});
	            }
	        });

		}




		// 
		$('body').on('click', '.js-amalinkspro-choose-product', function() {
			
			console.log('chose this product');


			var title = $(this).attr('data-amalinkspro-item-title');
			var imgMed = $(this).attr('data-amalinkspro-item-img-med');
			var asin = $(this).attr('data-amalinkspro-item-asin');

			var data = {
	            action: 'amazon_api_ajax_lookup_chosen_item',
	            asin: asin,
	            imgMed: imgMed,
	            title: title
	        };

			amazon_api_ajax_lookup_single_asin(data);

		});



		// make our ajax call to do the amazon api call in php
		function amazon_api_ajax_lookup_single_asin(data) {

			$.ajax({
	            type: 'POST',
	            url: ajaxurl,
	            dataType: "json",
	            data: data,
	            success: function(response) {


	            	$('.amalinkspro-step-2').addClass('amalinkspro-step-active');
	            	$('#amalinkspro-step-1').hide();

	            	$('.amalinkspro-step-1').removeClass('amalinkspro-step-active');
	            	$('#amalinkspro-step-2').show();

	            	$('html, body').animate({ scrollTop: 0 }, 0);

	            	$('.amalinkspro-step-2').removeClass('amalinkspro-step-disabled');


	            
	            	amalinksproChosenItem = response;


	                //console.log(response);

	                $('.amalinkspro-chosen-product').find('h3').text(data.title);
	            	$('.amalinkspro-chosen-product').find('.amalinkspro-chosen-product-main-img').attr('src', data.imgMed);
	            	$('.amalinkspro-chosen-item-previewlink').attr('href', amalinksproChosenItem.DetailPageURL);

	            	$('.amalinkspro-chosen-product').fadeIn(100);


	            	if ( amalinksproChosenItem ) {

		            	if ( amalinksproChosenItem.ASIN ) {
		            		$('.amalinkspro-chosen-product-asin').find('span').html(amalinksproChosenItem.ASIN);
		            	}
		            	else {
		            		$('.amalinkspro-chosen-product-asin').find('span').html('No Data');
		            	}



		            	if ( amalinksproChosenItem.ItemAttributes ) {

			            	if ( amalinksproChosenItem.ItemAttributes.Brand ) {
			            		$('.amalinkspro-chosen-product-brand').find('span').html(amalinksproChosenItem.ItemAttributes.Brand);
			            	}
			            	else {
			            		$('.amalinkspro-chosen-product-brand').find('span').html('No Data');
			            	}

			            	if ( amalinksproChosenItem.ItemAttributes.Model ) {
			            		$('.amalinkspro-chosen-product-model').find('span').html(amalinksproChosenItem.ItemAttributes.Model);
			            	}
			            	else {
			            		$('.amalinkspro-chosen-product-model').find('span').html('No Data');
			            	}

			            	if ( amalinksproChosenItem.ItemAttributes.UPC ) {
			            		$('.amalinkspro-chosen-product-upc').find('span').html(amalinksproChosenItem.ItemAttributes.UPC);
			            	}
			            	else {
			            		$('.amalinkspro-chosen-product-upc').find('span').html('No Data');
			            	}

			            	if ( amalinksproChosenItem.ItemAttributes.Warranty ) {
			            		$('.amalinkspro-chosen-product-warranty').find('span').html(amalinksproChosenItem.ItemAttributes.Warranty);
			            	}
			            	else {
			            		$('.amalinkspro-chosen-product-warranty').find('span').html('No Data');
			            	}


			            	if ( amalinksproChosenItem.ItemAttributes.ListPrice ) {

				            	if ( amalinksproChosenItem.ItemAttributes.ListPrice.FormattedPrice ) {
				            		$('.amalinkspro-chosen-product-listprice').find('span').html(amalinksproChosenItem.ItemAttributes.ListPrice.FormattedPrice);
				            	}
				            	else {
				            		$('.amalinkspro-chosen-product-listprice').find('span').html('No Data');
				            	}

				            }
				            else {
				            	$('.amalinkspro-chosen-product-listprice').find('span').html('No Data');
				            }

			            }
			            else {
		            		$('.amalinkspro-chosen-product-brand').find('span').html('No Data');
		            		$('.amalinkspro-chosen-product-model').find('span').html('No Data');
		            		$('.amalinkspro-chosen-product-upc').find('span').html('No Data');
		            		$('.amalinkspro-chosen-product-warranty').find('span').html('No Data');
		            		$('.amalinkspro-chosen-product-listprice').find('span').html('No Data');
			            }




			            if ( amalinksproChosenItem.OfferSummary ) {

			            	if ( amalinksproChosenItem.OfferSummary.LowestNewPrice ) {

				            	if ( amalinksproChosenItem.OfferSummary.LowestNewPrice.FormattedPrice ) {
				            		$('.amalinkspro-chosen-product-lowestnewprice').find('span').html(amalinksproChosenItem.OfferSummary.LowestNewPrice.FormattedPrice);
				            	}
				            	else {
				            		$('.amalinkspro-chosen-product-lowestnewprice').find('span').html('No Data');
				            	}

				            }

				            if ( amalinksproChosenItem.OfferSummary.LowestUsedPrice ) {

				            	if ( amalinksproChosenItem.OfferSummary.LowestUsedPrice.FormattedPrice ) {
				            		$('.amalinkspro-chosen-product-lowestusedprice').find('span').html(amalinksproChosenItem.OfferSummary.LowestUsedPrice.FormattedPrice);
				            	}
				            	else {
				            		$('.amalinkspro-chosen-product-lowestusedprice').find('span').html('No Data');

				            	}

				            }
				            else {
				            	$('.amalinkspro-chosen-product-lowestusedprice').find('span').html('No Data');
				            }

			            }

			            else {
		            		$('.amalinkspro-chosen-product-lowestnewprice').find('span').html('No Data');
	            			$('.amalinkspro-chosen-product-lowestusedprice').find('span').html('No Data');
			            }

			            // console.log('amalinksproChosenItem.Offers.Offer');
			            console.log(amalinksproChosenItem);


			            if ( amalinksproChosenItem.Offers ) {

			            	if ( amalinksproChosenItem.Offers.Offer ) {

			            		if ( amalinksproChosenItem.Offers.Offer.OfferListing ) {

			            			if ( amalinksproChosenItem.Offers.Offer.OfferListing.Price ) {

						            	if ( amalinksproChosenItem.Offers.Offer.OfferListing.Price.FormattedPrice ) {
						            		$('.amalinkspro-chosen-product-bestoffer').find('span').html(amalinksproChosenItem.Offers.Offer.OfferListing.Price.FormattedPrice);
						            	}
						            	else {
						            		$('.amalinkspro-chosen-product-bestoffer').find('span').html('No Data');
						            	}

						            }
						            else {

						            }




					            	if ( amalinksproChosenItem.Offers.Offer.OfferListing.PercentageSaved ) {
					            		console.log(amalinksproChosenItem.Offers.Offer.OfferListing);
					            		$('.amalinkspro-chosen-product-save').find('span').html(amalinksproChosenItem.Offers.Offer.OfferListing.PercentageSaved+'%');
					            	}
					            	else {
					            		$('.amalinkspro-chosen-product-save').find('span').html('No Data');
					            	}

					            }
					            else {
					            	$('.amalinkspro-chosen-product-bestoffer').find('span').html('No Data');
			            			$('.amalinkspro-chosen-product-save').find('span').html('No Data');
					            }

				            }
				            else {
				            	$('.amalinkspro-chosen-product-bestoffer').find('span').html('No Data');
		            			$('.amalinkspro-chosen-product-save').find('span').html('No Data');
				            }

			            }
			            else {
			            	$('.amalinkspro-chosen-product-bestoffer').find('span').html('No Data');
	            			$('.amalinkspro-chosen-product-save').find('span').html('No Data');
			            }


			        }

			        else {

			        	$('.amalinkspro-chosen-product-asin').find('span').html('No Data');
	            		$('.amalinkspro-chosen-product-brand').find('span').html('No Data');
	            		$('.amalinkspro-chosen-product-model').find('span').html('No Data');
	            		$('.amalinkspro-chosen-product-upc').find('span').html('No Data');
	            		$('.amalinkspro-chosen-product-warranty').find('span').html('No Data');
	            		$('.amalinkspro-chosen-product-listprice').find('span').html('No Data');
	            		$('.amalinkspro-chosen-product-lowestnewprice').find('span').html('No Data');
	            		$('.amalinkspro-chosen-product-lowestusedprice').find('span').html('No Data');
	            		$('.amalinkspro-chosen-product-bestoffer').find('span').html('No Data');
	            		$('.amalinkspro-chosen-product-save').find('span').html('No Data');


			        }

			        $(".amalinkspro-chosen-product p:contains('No Data')").css("color", "#ffb2b2");


			        /* Set text Link Values */

	            	var par = $('.amalinkspro-linktype-preview-text-link');
					par.find('.amalinkspro-text-link-preview-p').find('a').text( amalinksproChosenItem.ItemAttributes.Title );
					par.find('.amalinkspro-text-link-preview-p').find('a').attr( 'href', amalinksproChosenItem.DetailPageURL );
					par.find('input[name=amalinkspro-edit-textlink-text]').val(amalinksproChosenItem.ItemAttributes.Title);

					/* Set Image Link Values */

					var par = $('.amalinkspro-linktype-preview-image-link');
	            	par.find('.amalinkspro-image-link-img').attr('src', amalinksproChosenItem.MediumImage.URL );
	            	par.find('.amalinkspro-image-link-link').attr('href', amalinksproChosenItem.DetailPageURL );


	            	var new_image_choices = '';

	            	if ( amalinksproChosenItem.ImageSets ) {

	            		if ( amalinksproChosenItem.ImageSets.ImageSet ) {

			            	var image_set = amalinksproChosenItem.ImageSets.ImageSet;

			            	for (var key in image_set) {

			            		if ( image_set[key].MediumImage ) {
			            			var img_src = image_set[key].MediumImage.URL;
			            		}
			            		else if ( image_set[key].SmallImage ) {
			            			var img_src = image_set[key].SmallImage.URL;
			            		}


								if ( img_src === amalinksproChosenItem.MediumImage.URL ) {
									new_image_choices += '<div class="amalinkspro-choose-api-image amalinkspro-chosen-api-img">';
								}
								else {
									new_image_choices += '<div class="amalinkspro-choose-api-image">';
								}

								new_image_choices += '<span class="amalinkspro-chosen-size"></span>';

				    			new_image_choices += '<img src="'+img_src+'" alt="" />';
				    			new_image_choices += '<div class="amalinkspro-choose-api-image-cover js-amalinkspro-select-image">';

				    			if ( image_set[key].LargeImage ) {
					    			new_image_choices += '<span class="js-amalinkspro-select-image-size" data-img-size-url="'+image_set[key].LargeImage.URL+'">large</span>';
					    		}
					    		if ( image_set[key].MediumImage ) {
					    			new_image_choices += '<span class="js-amalinkspro-select-image-size" data-img-size-url="'+image_set[key].MediumImage.URL+'">medium</span>';
					    		}
					    		if ( image_set[key].SmallImage ) {
					    			new_image_choices += '<span class="js-amalinkspro-select-image-size" data-img-size-url="'+image_set[key].SmallImage.URL+'">small</span>';
					    		}
					    		if ( image_set[key].SwatchImage ) {
					    			new_image_choices += '<span class="js-amalinkspro-select-image-size" data-img-size-url="'+image_set[key].SwatchImage.URL+'">tiny</span>';
					    		}
				    			new_image_choices += '</div class="js-amalinkspro-select-image-size">';

				    			new_image_choices += '</div>';

							}
							new_image_choices += '<div class="amalinkspro-clear"></div>';

						}
						else {
							new_image_choices += '<div class="amalinkspro-clear"></div>';
						}

					}
					else {
						new_image_choices += '<h3 class="amalinkspro-clear">Sorry, there are no image sets</h3>';
					}

	            	$('.amalinkspro-linktype-preview-image-link-edit-imagechoices').html(new_image_choices);





	            	/* Set CAT Link Values */

					var par = $('.amalinkspro-linktype-preview-cta-link');
					par.find('.amalinkspro-cta-link-link').text( amalinksproChosenItem.ItemAttributes.Title );
					par.find('.amalinkspro-cta-image-link').attr( 'alt', amalinksproChosenItem.ItemAttributes.Title );
	            	par.find('.amalinkspro-cta-link-link').attr( 'href', amalinksproChosenItem.DetailPageURL );
	            	par.find('input[name=amalinkspro-edit-ctalink-text]').val( amalinksproChosenItem.ItemAttributes.Title );
					

	            	


	            },
	            complete: function(response){

	            	var item = $('.amalinkspro-chosen-product');

	            	var xxx = 'test';

					item.attr('data-amalinkspro-apidata-name',xxx );
					item.attr('data-amalinkspro-apidata-asin',xxx );
					item.attr('data-amalinkspro-apidata-brand',xxx );
					item.attr('data-amalinkspro-apidata-model',xxx );
					item.attr('data-amalinkspro-apidata-upc',xxx );
					item.attr('data-amalinkspro-apidata-warranty',xxx );
					item.attr('data-amalinkspro-apidata-lowestnewprice',xxx );
					item.attr('data-amalinkspro-apidata-lowestusedprice',xxx );
					item.attr('data-amalinkspro-apidata-listprice',xxx );
					item.attr('data-amalinkspro-apidata-bestoffer',xxx );
					item.attr('data-amalinkspro-apidata-percentageoff',xxx );
					item.attr('data-amalinkspro-apidata-amountoff',xxx );

					//console.log(amalinksproChosenItem);
			    	

				    
	            }
	        });

		}









		// 
		$('body').on('click', '.js-amalinkpro-choose-link-type', function(e) {
			console.log('chose this product');

			e.preventDefault();

			$('.js-amalinkpro-choose-link-type').removeClass('amalinkspro-linktype-active');
			$(this).addClass('amalinkspro-linktype-active');

			var link_type = $(this).attr('data-link-type');

			$('.amalinkspro-linktype-preview').removeClass('amalinkspro-chosen-linktype').fadeOut(100);

			$('.amalinkspro-linktype-preview-'+link_type).addClass('amalinkspro-chosen-linktype').fadeIn(100);

			$('.amalinkspro-goto-step3').fadeIn(100);


			// if ( link_type === 'text-link' ) {
			// 	get_text_link_data();
			// }


		});

		// function get_text_link_data() {

		// 	var par = $('.amalinkspro-linktype-preview-text-link');

		// 	par.find('.amalinkspro-text-link-preview-p').find('a').text( amalinksproChosenItem.ItemAttributes.Title );
		// 	par.find('.amalinkspro-text-link-preview-p').find('a').attr( 'href', amalinksproChosenItem.DetailPageURL );
		// 	par.find('input[name=amalinkspro-edit-textlink-text]').val(amalinksproChosenItem.ItemAttributes.Title);
			

		// }


		// 
		$('body').on('click', '.js-amalinkspro-edit-link', function() {
			console.log('worked fool');
			if ( !$(this).hasClass('edit-box-open') ) {

				$(this).addClass('edit-box-open');
				$(this).closest('.amalinkspro-linktype-preview').find('.amalinkspro-linktype-preview-edit').slideDown(100);

				
			}
			else {
				
				$(this).removeClass('edit-box-open');
				$(this).closest('.amalinkspro-linktype-preview').find('.amalinkspro-linktype-preview-edit').slideUp(100);
			}
			
		});




		// 
		$('body').on('click', '.js-amalinkspro-edit-text-link', function() {
			console.log('Text link in preview updated');
			
			var new_textlink_val = $(this).closest('.amalinkspro-linktype-preview-edit').find('input[name=amalinkspro-edit-textlink-text]').val();

			$(this).closest('.amalinkspro-linktype-preview').find('.amalinkspro-text-link-preview-p a').text(new_textlink_val);

			$(this).closest('.amalinkspro-linktype-preview-edit').slideUp(100);

			$(this).closest('.amalinkspro-linktype-preview').find('.amalinkspro-edit-link').removeClass('edit-box-open');
			
		});
		


		


		// 
		$('body').on('click', '.js-amalinkspro-select-image-size', function(e) {
			e.preventDefault();
			console.log('chose this image');

			$('.amalinkspro-choose-api-image').removeClass('amalinkspro-chosen-api-img');
			$(this).closest('.amalinkspro-choose-api-image').addClass('amalinkspro-chosen-api-img');  

			var chosen_img = $(this).attr('data-img-size-url');

			$(this).closest('.amalinkspro-linktype-preview-edit').find('.js-amalinkspro-edit-image-link').attr('data-chosen-img-src',chosen_img );

			var size_text = $(this).text();
			$(this).closest('.amalinkspro-choose-api-image').find('.amalinkspro-chosen-size').text( size_text ).css('display', 'inline-block');


		});

		// 
		$('body').on('click', '.js-amalinkspro-edit-image-link', function() {
			console.log('Text link in preview updated');
			
			var new_url = $(this).attr('data-chosen-img-src');

			$(this).closest('.amalinkspro-linktype-preview').find('.amalinkspro-image-link-img').attr('src', new_url );

			$(this).closest('.amalinkspro-linktype-preview-edit').slideUp(100);
			$(this).closest('.amalinkspro-linktype-preview').find('.amalinkspro-edit-link').removeClass('edit-box-open');
			
		});



		// 
		$('body').on('click', '.js-amalinkspro-edit-cta-link', function() {
			console.log('CTA link in preview updated');

			var new_textlink_val = $(this).closest('.amalinkspro-linktype-preview-edit').find('input[name=amalinkspro-edit-ctalink-text]').val();

			$(this).closest('.amalinkspro-linktype-preview').find('.amalinkspro-cta-link-link').text(new_textlink_val);

			$(this).closest('.amalinkspro-linktype-preview-edit').slideUp(100);
			$(this).closest('.amalinkspro-linktype-preview').find('.amalinkspro-edit-link').removeClass('edit-box-open');
			
		});



		

		// 
		$('body').on('click', '.js-select-cta-style', function(e) {
			e.preventDefault();
			console.log('CTA link in preview updated');

			

			if ( $(this).hasClass('amalinkspro-cta-image-link') ) {
				var new_cta_class = 'amalinkspro-cta-wrap-preview amalinkspro-cta-wrap ' + $(this).attr('data-amalinkspro-cta-class');
				var new_cta_img = $(this).find('img').clone();
				$('.amalinkspro-cta-wrap-preview a').html( new_cta_img );
			}
			else {
				var new_cta_class = 'amalinkspro-cta-wrap-preview amalinkspro-cta-wrap ' + $(this).attr('data-amalinkspro-cta-class');
			}

			$('.amalinkspro-cta-wrap-preview').attr('class',new_cta_class);

			// $(this).closest('.amalinkspro-linktype-preview-edit').slideUp(100);
			// $(this).closest('.amalinkspro-linktype-preview').find('.amalinkspro-edit-link').removeClass('edit-box-open');
			
		});







		// 
		$('body').on('click', '.js-amalinkspro-final-step', function(e) {
			e.preventDefault();
			console.log('Finalizing link in step 3');

			$('.amalinkspro-step-3').addClass('amalinkspro-step-active');
	        $('#amalinkspro-step-2').hide();

        	$('.amalinkspro-step-2').removeClass('amalinkspro-step-active');
        	$('#amalinkspro-step-3').show();

        	$('html, body').animate({ scrollTop: 0 }, 0);

        	$('.amalinkspro-step-3').removeClass('amalinkspro-step-disabled');

        	var final_preview = $(this).closest('.amalinkspro-step-wrap').find('.amalinkspro-chosen-linktype').clone();
        	

        	final_preview.find('.js-amalinkspro-edit-link').remove().end();
        	final_preview.find('.amalinkspro-linktype-preview-edit').remove()

        		//console.log( $(final_preview_cleaned) );



        	//console.log('final_preview: ',final_preview);

        	$('.amalinkspro-step3-final-preview').html(final_preview);


		});



		

		
		// 
		$('body').on('click', '.js-amalinkspro-insert-final-link-html', function(e) {
			e.preventDefault();

			console.log(parent.amalinkspro_global_editor_id);


			var myText = '<a href="#">test link</a>';

			// tinyMCE.execCommand('setActive',false, parent.amalinkspro_global_editor_id);
			// tinyMCE.get('id_of_textarea').focus()

			tinyMCE.get( 'content' ).execCommand('mceInsertContent', false, myText);
			
			window.parent.tb_remove();


		});


		// 
		$('body').on('click', '.js-amalinkspro-insert-final-link-shortcode', function(e) {
			e.preventDefault();

			var myText = '[amalinkspro /]';
			tinyMCE.activeEditor.execCommand('mceInsertContent', false, myText);

		});

		



		 



		





		



		

	

	});


})( $ );


