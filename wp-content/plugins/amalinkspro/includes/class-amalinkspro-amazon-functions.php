<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       http://amalinkspro.com
 * @since      1.0.0
 *
 * @package    Ama_Links_Pro
 * @subpackage Ama_Links_Pro/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Ama_Links_Pro
 * @subpackage Ama_Links_Pro/admin
 * @author     Your Name <email@amalinkspro.com>
 */
class amalinkspro_amazon_api {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */



	public function amazon_api_get_endpoint() {


		$locale = trim( get_option( 'amalinkspro-options_default_amazon_search_locale' ) );

		// The region you are interested in
		if ( $locale == 'US' ) {
			$endpoint = "webservices.amazon.com";
		}
		else if ( $locale == 'UK' ) {
			$endpoint = "webservices.amazon.co.uk";
		}
		else if ( $locale == 'BR' ) {
			$endpoint = "webservices.amazon.com.br";
		}
		else if ( $locale == 'CA' ) {
			$endpoint = "webservices.amazon.ca";
		}
		else if ( $locale == 'CN' ) {
			$endpoint = "webservices.amazon.cn";
		}
		else if ( $locale == 'FR' ) {
			$endpoint = "webservices.amazon.fr";
		}
		else if ( $locale == 'DE' ) {
			$endpoint = "webservices.amazon.de";
		}
		else if ( $locale == 'IN' ) {
			$endpoint = "webservices.amazon.in";
		}
		else if ( $locale == 'IT' ) {
			$endpoint = "webservices.amazon.it";
		}
		else if ( $locale == 'JP' ) {
			$endpoint = "webservices.amazon.co.jp";
		}
		else if ( $locale == 'ES' ) {
			$endpoint = "webservices.amazon.es";
		}
		else if ( $locale == 'MX' ) {
			$endpoint = "webservices.amazon.com.mx";
		}

		return $endpoint;

		// echo $endpoint;;
		// die();

	}




	public function amazon_api_request( $post_id=null, $response_group, $keywords ) {


		/****************************************
		*** Get our product info from the api ***
		****************************************/

		// http://webservices.amazon.com/scratchpad/index.html

		$locale = trim( get_option( 'amalinkspro-options_default_amazon_search_locale' ) );

		// Your AWS Access Key ID, as taken from the AWS Your Account page
		$aws_access_key_id = trim( get_option( 'amalinkspro-options_amazon_api_access_key' ) );

		// Your AWS Secret Key corresponding to the above ID, as taken from the AWS Your Account page
		$aws_secret_key = trim( get_option( 'amalinkspro-options_amazon_api_secret_key' ) );

		$affiliate_id = trim( get_option( 'amalinkspro-options_'.$locale.'_amazon_associate_ids_0_associate_id' ) );
					    

		if ( $aws_access_key_id && $aws_secret_key && $affiliate_id ) {

			$endpoint = $this->amazon_api_get_endpoint();
			

			$uri = "/onca/xml";

			$params = array(
			    "Service" => "AWSECommerceService",
			    "Operation" => "ItemSearch",
			    "AWSAccessKeyId" => $aws_access_key_id,
			    "AssociateTag" => $affiliate_id,
			    "SearchIndex" => "All",
			    "Keywords" => $keywords,
			    "ResponseGroup" => $response_group
			);

			// Set current timestamp if not set
			if (!isset($params["Timestamp"])) {
			    $params["Timestamp"] = gmdate('Y-m-d\TH:i:s\Z');
			}

			// Sort the parameters by key
			ksort($params);

			$pairs = array();

			foreach ($params as $key => $value) {
			    array_push($pairs, rawurlencode($key)."=".rawurlencode($value));
			}

			// Generate the canonical query
			$canonical_query_string = join("&", $pairs);

			// Generate the string to be signed
			$string_to_sign = "GET\n".$endpoint."\n".$uri."\n".$canonical_query_string;

			// Generate the signature required by the Product Advertising API
			$signature = base64_encode(hash_hmac("sha256", $string_to_sign, $aws_secret_key, true));

			// Generate the signed URL
			$request_url = 'https://'.$endpoint.$uri.'?'.$canonical_query_string.'&Signature='.rawurlencode($signature);

			
			if( ini_get('allow_url_fopen') ) {

				$api_response = file_get_contents($request_url);

			}

			else {

				$amazon_request_return = wp_remote_get($request_url);

				if ( is_wp_error($amazon_request_return) ) {
					$errors_arr = $amazon_request_return->errors;

					foreach( $errors_arr as $error ) {
						$api_response = $error[0];
					}
				}

				else {

					$request_response = $amazon_request_return['response']['code'];

					if ( $request_response == '200' ) {
						$api_response = wp_remote_retrieve_body($amazon_request_return);
					}
					else {
						$api_response = 'NOT 200 - Request Denied ny Amazon';
					}

				}

			}

				

			

		}
		else {

			$api_response = 'There is a problem with your ';

			if ( $aws_access_key_id ) {
				$api_response .= 'Access Key';
			}

			if ( $aws_secret_key ) {
				$api_response .= ', Secret Key';
			}

			if ( $affiliate_id ) {
				$api_response .= ', Affiliate ID';
			}
			
		}

		// echo '<pre>'.print_r($api_response,1).'</pre>';
		// die();

		return $api_response;

		// echo '::: '.$api_response;
		// die();


	}



	



}