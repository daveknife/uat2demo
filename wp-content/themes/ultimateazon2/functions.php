<?php
/**
 * ultimateazon2
 *
 * WARNING: This file is part of the core Ultimate Azon Theme 2 Theme. DO NOT edit this file under any circumstances. Do all modifications in a child theme. http://codex.wordpress.org/Child_Themes
 *
 * @package ultimateazon2
 * @author  Dave Nicosia
 * @license GPL-2.0+
 * @link    https://ultimateazontheme.com
 */

// Let's get things started by initializing our theme framework
require_once( dirname( __FILE__ ) . '/lib/init.php' );