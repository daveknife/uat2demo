<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              https://incomegalaxy.com
 * @since             1.0.0
 * @package           galfram_productreviewpro
 *
 * @wordpress-plugin
 * Plugin Name:       Galaxy Framework - Product Review Pro
 * Plugin URI:        https://incomegalaxy.com/downloads/product-review-plus/
 * Description:       Add product reviews to your website with ease. Create custom product specifications, pull smoe information form the Amazon API, or pull all product info fomr the Amazon API.
 * Version:           0.3.1
 * Author:            Income Galaxy
 * Author URI:        https://incomegalaxy.com
 * License:           GPL-2.0+
 * License URI:       https://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       galaxyframework-prp
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-galaxyframework-productreviewpro-activator.php
 */
function activate_galfram_productreviewpro() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-galaxyframework-productreviewpro-activator.php';
	galfram_productreviewpro_Activator::activate();
}






// REMOVE THIS FOR PRODUCTION !!!!!!!!!!!!!!!

// 3. Hide ACF field group menu item
//add_filter('acf/settings/show_admin', '__return_true', 100);







/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-galaxyframework-productreviewpro-deactivator.php
 */
function deactivate_galfram_productreviewpro() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-galaxyframework-productreviewpro-deactivator.php';
	galfram_productreviewpro_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_galfram_productreviewpro' );
register_deactivation_hook( __FILE__, 'deactivate_galfram_productreviewpro' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-galaxyframework-productreviewpro.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_galfram_productreviewpro() {

	$plugin = new galfram_productreviewpro();
	$plugin->run();

}
run_galfram_productreviewpro();
