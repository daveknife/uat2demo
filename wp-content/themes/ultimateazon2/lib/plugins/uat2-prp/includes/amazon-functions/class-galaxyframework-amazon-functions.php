<?php

/**
 * Hold all Amazon Product Advertising API functions
 *
 * @link       https://incomegalaxy.com
 * @since      1.0.0
 *
 * @package    galfram_productreviewpro
 * @subpackage galfram_productreviewpro/includes
 */

/**
 * Hold all Amazon Product Advertising API functions
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    galfram_productreviewpro
 * @subpackage galfram_productreviewpro/includes
 * @author     Your Name <email@example.com>
 */
class galfram_productreviewpro_amazon_api {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */



	public function amazon_api_get_endpoint() {


		$locale = trim( get_option( 'extension-prp-settings_default_amazon_search_locale' ) );

		// The region you are interested in
		if ( $locale == 'US' ) {
			$endpoint = "webservices.amazon.com";
		}
		else if ( $locale == 'UK' ) {
			$endpoint = "webservices.amazon.co.uk";
		}
		else if ( $locale == 'BR' ) {
			$endpoint = "webservices.amazon.com.br";
		}
		else if ( $locale == 'CA' ) {
			$endpoint = "webservices.amazon.ca";
		}
		else if ( $locale == 'CN' ) {
			$endpoint = "webservices.amazon.cn";
		}
		else if ( $locale == 'FR' ) {
			$endpoint = "webservices.amazon.fr";
		}
		else if ( $locale == 'DE' ) {
			$endpoint = "webservices.amazon.de";
		}
		else if ( $locale == 'IN' ) {
			$endpoint = "webservices.amazon.in";
		}
		else if ( $locale == 'IT' ) {
			$endpoint = "webservices.amazon.it";
		}
		else if ( $locale == 'JP' ) {
			$endpoint = "webservices.amazon.co.jp";
		}
		else if ( $locale == 'ES' ) {
			$endpoint = "webservices.amazon.es";
		}
		else if ( $locale == 'MX' ) {
			$endpoint = "webservices.amazon.com.mx";
		}

		return $endpoint;

	}




	public function amazon_api_request( $post_id, $response_group, $keywords ) {


		/****************************************
		*** Get our product info from the api ***
		****************************************/

		// http://webservices.amazon.com/scratchpad/index.html

		$locale = trim( get_option( 'extension-prp-settings_default_amazon_search_locale' ) );

		// Your AWS Access Key ID, as taken from the AWS Your Account page
		$aws_access_key_id = trim( get_option( 'extension-prp-settings_amazon_api_key' ) );

		// Your AWS Secret Key corresponding to the above ID, as taken from the AWS Your Account page
		$aws_secret_key = trim( get_option( 'extension-prp-settings_amazon_api_secret' ) );

		$affiliate_id = trim( get_option( 'extension-prp-settings_amazon_affiliate_id_'.$locale ) );

		if ( $aws_access_key_id && $aws_secret_key && $affiliate_id ) {

			$endpoint = $this->amazon_api_get_endpoint();
			

			$uri = "/onca/xml";

			$params = array(
			    "Service" => "AWSECommerceService",
			    "Operation" => "ItemSearch",
			    "AWSAccessKeyId" => $aws_access_key_id,
			    "AssociateTag" => $affiliate_id,
			    "SearchIndex" => "All",
			    "Keywords" => $keywords,
			    "ResponseGroup" => $response_group
			);

			// Set current timestamp if not set
			if (!isset($params["Timestamp"])) {
			    $params["Timestamp"] = gmdate('Y-m-d\TH:i:s\Z');
			}

			// Sort the parameters by key
			ksort($params);

			$pairs = array();

			foreach ($params as $key => $value) {
			    array_push($pairs, rawurlencode($key)."=".rawurlencode($value));
			}

			// Generate the canonical query
			$canonical_query_string = join("&", $pairs);

			// Generate the string to be signed
			$string_to_sign = "GET\n".$endpoint."\n".$uri."\n".$canonical_query_string;

			// Generate the signature required by the Product Advertising API
			$signature = base64_encode(hash_hmac("sha256", $string_to_sign, $aws_secret_key, true));

			// Generate the signed URL
			$request_url = 'https://'.$endpoint.$uri.'?'.$canonical_query_string.'&Signature='.rawurlencode($signature);

			// get our xml file contents
			$amazon_request_return = wp_remote_get($request_url);
			$request_response = $amazon_request_return['response']['code'];

			if ( $request_response == '200' ) {
				$api_response = wp_remote_retrieve_body($amazon_request_return);
			}
			else {
				$api_response = 'NOT 200 - Request Denied ny Amazon';
			}


			return $api_response;

		}
		else {
			return 'No Credentials';
		}


	}





	public function amazon_api_item_lookup( $post_id, $response_group, $asin ) {

		/****************************************
		*** Get our product info from the api ***
		****************************************/

		// http://webservices.amazon.com/scratchpad/index.html

		$locale = trim( get_option( 'extension-prp-settings_default_amazon_search_locale' ) );

		// Your AWS Access Key ID, as taken from the AWS Your Account page
		$aws_access_key_id = trim( get_option( 'extension-prp-settings_amazon_api_key' ) );

		// Your AWS Secret Key corresponding to the above ID, as taken from the AWS Your Account page
		$aws_secret_key = trim( get_option( 'extension-prp-settings_amazon_api_secret' ) );

		$affiliate_id = trim( get_option( 'extension-prp-settings_amazon_affiliate_id_'.$locale ) );

		$endpoint = $this->amazon_api_get_endpoint();
		

		$uri = "/onca/xml";

		$params = array(
		    "Service" => "AWSECommerceService",
		    "Operation" => "ItemLookup",
		    "IdType" => "ASIN",
		    "ItemId" => $asin,
		    "AWSAccessKeyId" => $aws_access_key_id,
		    "AssociateTag" => $affiliate_id,
		    "ResponseGroup" => $response_group
		);

		//http://webservices.amazon.com/onca/xml?


		//print_r($params);

		// Set current timestamp if not set
		if (!isset($params["Timestamp"])) {
		    $params["Timestamp"] = gmdate('Y-m-d\TH:i:s\Z');
		}

		// Sort the parameters by key
		ksort($params);

		$pairs = array();

		foreach ($params as $key => $value) {
		    array_push($pairs, rawurlencode($key)."=".rawurlencode($value));
		}

		// Generate the canonical query
		$canonical_query_string = join("&", $pairs);

		// Generate the string to be signed
		$string_to_sign = "GET\n".$endpoint."\n".$uri."\n".$canonical_query_string;

		// Generate the signature required by the Product Advertising API
		$signature = base64_encode(hash_hmac("sha256", $string_to_sign, $aws_secret_key, true));

		// Generate the signed URL
		$request_url = 'https://'.$endpoint.$uri.'?'.$canonical_query_string.'&Signature='.rawurlencode($signature);

		// get our xml file contents
		$amazon_request_return = wp_remote_get($request_url);
		$request_response = $amazon_request_return['response']['code'];

		if ( $request_response == '200' ) {
			$api_response = wp_remote_retrieve_body($amazon_request_return);
		}
		else {
			$api_response = 'NOT 200 - Request Denied ny Amazon';
		}

		return $api_response;


	}









	public function get_custom_review_specs( $post_id ) {

		// get our chosen specs group unique id
		$specs_group_id = get_post_meta( $post_id, 'prod_specs_group', true);
		// get our array of all specs groups
		$user_specs_groups = get_option('galfram_ext_prp_all_userspecs_groups');

		// save only the chosen spec group to a new array
		foreach ( $user_specs_groups as $user_spec_group ) {
			if ( $user_spec_group['field_57c35f7e8031b'] == $specs_group_id ) {
				$current_chosen_spec_group = $user_spec_group;
			}
		}

		// save only the specs themselves into a new array
		$specs = $current_chosen_spec_group['field_57c06aa9622b9'];

		// start a new array for our spec names, types, and values
		$specs_array = array();

		$i=0;
		$amazon_spec_present = false;

		// let's set the spec title and type into the array
		foreach ( $specs as $single_spec ) {

			$specs_array[$i]['spec_title'] = $single_spec['field_57c06afc622ba'];

			if ( isset( $single_spec['field_5824dc0feb5f7'] ) ) {
				$specs_array[$i]['spec_title_hide'] = $single_spec['field_5824dc0feb5f7'];
			}
			else {
				$specs_array[$i]['spec_title_hide'] = 0;
			}
			$specs_array[$i]['spec_type'] = $single_spec['field_57c06b19622bb'];
			$specs_array[$i]['spec_id'] = $single_spec['field_57c6bfb62908a'];
			if ( $single_spec['field_57c06b19622bb'] == 'amazon' ) {
				$specs_array[$i]['spec_amazon_field'] = $single_spec['field_57c7052ff242d'];

				if ( $single_spec['field_57c7052ff242d'] == 'price' ) {
					$specs_array[$i]['spec_amazon_price_fields'] = $single_spec['field_58061e2c0864b'];
				}
				$amazon_spec_present = true;
			}
			else {
				$specs_array[$i]['spec_amazon_field'] = 'asin';
				$specs_array[$i]['spec_amazon_price_fields'] = array();
			}
			

			$i++;
			
		}

		$custom_specs['amazon_spec_present'] = $amazon_spec_present;
		$custom_specs['specs_group_id'] = $specs_group_id;
		$custom_specs['specs_array'] = $specs_array;

		return $custom_specs;


	}






	public function get_amazon_review_specs( $xml_response ) {

		$amazon_spec_val['prod_title'] = htmlspecialchars( $xml_response->Items->Item->ItemAttributes->Title );
		$amazon_spec_val['prod_asin'] = $xml_response->Items->Item->ASIN;
		//$prod_url = $xml_response->Items->Item->DetailPageURL; // pulled in above for featured image
		$amazon_spec_val['prod_brand'] = $xml_response->Items->Item->ItemAttributes->Brand;
		$amazon_spec_val['prod_model'] = $xml_response->Items->Item->ItemAttributes->Model;
		$amazon_spec_val['prod_upc'] = $xml_response->Items->Item->ItemAttributes->UPC;
		$amazon_spec_val['prod_features_array'] = $xml_response->Items->Item->ItemAttributes->Feature;
		$amazon_spec_val['prod_warranty'] = $xml_response->Items->Item->ItemAttributes->Warranty;

	    $amazon_spec_val['prod_formatted_price'] = $xml_response->Items->Item->ItemAttributes->ListPrice->FormattedPrice;

	    $amazon_spec_val['prod_lowest_new_price'] = $xml_response->Items->Item->OfferSummary->LowestNewPrice->FormattedPrice;
	    $amazon_spec_val['prod_lowest_used_price'] = $xml_response->Items->Item->OfferSummary->LowestUsedPrice->FormattedPrice;

	    $amazon_spec_val['prod_lowest_new_price_currency'] = $xml_response->Items->Item->OfferSummary->LowestNewPrice->CurrencyCode;
	    $amazon_spec_val['prod_lowest_used_price_currency'] = $xml_response->Items->Item->OfferSummary->LowestUsedPrice->CurrencyCode;

	    $amazon_spec_val['prod_price_offer'] = $xml_response->Items->Item->Offers->Offer->OfferListing->Price->FormattedPrice;
	    $amazon_spec_val['prod_amount_saved'] = $xml_response->Items->Item->Offers->Offer->OfferListing->AmountSaved->FormattedPrice;
	    $amazon_spec_val['prod_amount_saved_currency'] = $xml_response->Items->Item->Offers->Offer->OfferListing->AmountSaved->CurrencyCode;
	    $amazon_spec_val['prod_percentage_saved'] = $xml_response->Items->Item->Offers->Offer->OfferListing->PercentageSaved;

	    $amazon_spec_val['prod_total_new'] = $xml_response->Items->Item->OfferSummary->TotalNew;
	    $amazon_spec_val['prod_total_used'] = $xml_response->Items->Item->OfferSummary->TotalNew;

	    $amazon_spec_val['prod_listprice_currencycode'] = $xml_response->Items->Item->ItemAttributes->ListPrice->CurrencyCode;

	    $amazon_spec_val['small-img'] = $xml_response->Items->Item->ImageSets->ImageSet->SmallImage->URL;
	    $amazon_spec_val['medium-img'] = $xml_response->Items->Item->ImageSets->ImageSet->MediumImage->URL;
	    $amazon_spec_val['large-img'] = $xml_response->Items->Item->ImageSets->ImageSet->LargeImage->URL;

	    return $amazon_spec_val; 


	}



	public function get_star_rating_html( $editor_rating_num ) {

		if ( $editor_rating_num == 0.0 ) {
			$stars = '<span class="editor-rating" title="'.$editor_rating_num.' Star Rating"><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i></span>';
		}
		else if ( $editor_rating_num >= 0.1 && $editor_rating_num <= 0.9 ) {
			$stars = '<span class="editor-rating" title="'.$editor_rating_num.' Star Rating"><i class="fa fa-star-half-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i></span>';
		}
		else if ( $editor_rating_num == 1.0 ) {
			$stars = '<span class="editor-rating" title="'.$editor_rating_num.' Star Rating"><i class="fa fa-star"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i></span>';
		}
		else if ( $editor_rating_num >= 1.1 && $editor_rating_num <= 1.9 ) {
			$stars = '<span class="editor-rating" title="'.$editor_rating_num.' Star Rating"><i class="fa fa-star"></i><i class="fa fa-star-half-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i></span>';
		}
		else if ( $editor_rating_num == 2.0 ) {
			$stars = '<span class="editor-rating" title="'.$editor_rating_num.' Star Rating"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i></span>';
		}
		else if ( $editor_rating_num >= 2.1 && $editor_rating_num <= 2.9 ) {
			$stars = '<span class="editor-rating" title="'.$editor_rating_num.' Star Rating"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star-half-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i></span>';
		}
		else if ( $editor_rating_num == 3.0 ) {
			$stars = '<span class="editor-rating" title="'.$editor_rating_num.' Star Rating"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i></span>';
		}
		else if ( $editor_rating_num >= 3.1 && $editor_rating_num <= 3.9 ) {
			$stars = '<span class="editor-rating" title="'.$editor_rating_num.' Star Rating"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star-half-o"></i><i class="fa fa-star-o"></i></span>';
		}
		else if ( $editor_rating_num == 4.0 ) {
			$stars = '<span class="editor-rating" title="'.$editor_rating_num.' Star Rating"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star-o"></i></span>';
		}
		else if ( $editor_rating_num >= 4.1 && $editor_rating_num <= 4.9 ) {
			$stars = '<span class="editor-rating" title="'.$editor_rating_num.' Star Rating"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star-half-o"></i></span>';
		}
		else if ( $editor_rating_num == 5.0 ) {
			$stars = '<span class="editor-rating" title="'.$editor_rating_num.' Star Rating"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i></span>';
		}

		return $stars;

	}





	/** Add our custom styles to the Stylizer Child Theme
	 *
	 * @since    1.0.0
	 */
	public function galfram_get_spec_button_html( $post_id, $spec_id ) {


		
		$button_html = '';


		$button_type = get_post_meta( $post_id, $spec_id.'_buttontype' , true);

		if ( $button_type == 'button-aff' ) {
			$link_url = get_post_meta( $post_id, 'manual_affiliate_link', true);

			$new_tab = get_option( 'extension-prp-settings_open_aff_links_in_new_window', true );
			if ( $new_tab == 1 ) { $new_tab_html = ' target="_blank"'; }
			else { $new_tab_html = ''; }

			$no_follow = get_option( 'extension-prp-settings_no-follow_affiliate_links', true );
			if ( $no_follow == 1 ) { $no_follow_html = ' rel="nofollow"'; }
			else { $no_follow_html = ''; }
		}
		else if ( $button_type == 'button-full' ) {
			$link_url = get_the_permalink( $post_id );
			$new_tab_html = '';
			$no_follow_html = '';
		}
		else if ( $button_type == 'button-custom' ) {
			$link_url = get_post_meta( $post_id, $spec_id.'_buttonurl' , true);
			$new_tab_html = '';
			$no_follow_html = '';
		}
		else {
			$link_url = '#';
			$new_tab_html = '';
			$no_follow_html = '';
		}



		$button_style = '';
		$button_style = get_post_meta( $post_id, $spec_id.'_buttonstyle' , true);

		if ( $button_style == 'button-custom' ) {
			$button_img = get_post_meta( $post_id, $spec_id.'_buttonimg' , true);
			$button_img = wp_get_attachment_image_src( $button_img, 'full' );
			$button_img = $button_img[0];

			$button_html .= '<a class="button-custom-img spec-button gtm-spec-button" href="'.$link_url.'"'.$no_follow_html.$new_tab_html.'>';
			$button_html .= '<img src="'.$button_img.'" alt="" />';
			$button_html .= '</a>';
		}

		else if ( $button_style == 'button-ama-1' ) {
			$button_html .= '<a class="button-custom-img spec-button gtm-spec-button" href="'.$link_url.'"'.$no_follow_html.$new_tab_html.'>';
			$button_html .= '<img src="'.get_template_directory_uri().'/lib/plugins/uat2-prp/public/images/amazon-btn-1.png" alt="" />';
			$button_html .= '</a>';
		}
		else if ( $button_style == 'button-ama-2' ) {
			$button_html .= '<a class="button-custom-img spec-button gtm-spec-button" href="'.$link_url.'"'.$no_follow_html.$new_tab_html.'>';
			$button_html .= '<img src="'.get_template_directory_uri().'/lib/plugins/uat2-prp/public/images/amazon-btn-2.png" alt="" />';
			$button_html .= '</a>';
		}
		else if ( $button_style == 'button-ama-3' ) {
			$button_html .= '<a class="button-custom-img spec-button gtm-spec-button" href="'.$link_url.'"'.$no_follow_html.$new_tab_html.'>';
			$button_html .= '<img src="'.get_template_directory_uri().'/lib/plugins/uat2-prp/public/images/amazon-btn-3.png" alt="" />';
			$button_html .= '</a>';
		}
		else if ( $button_style == 'button-ama-4' ) {
			$button_html .= '<a class="button-custom-img spec-button gtm-spec-button" href="'.$link_url.'"'.$no_follow_html.$new_tab_html.'>';
			$button_html .= '<img src="'.get_template_directory_uri().'/lib/plugins/uat2-prp/public/images/amazon-btn-4.png" alt="" />';
			$button_html .= '</a>';
		}
		else if ( $button_style == 'button-ama-5' ) {
			$button_html .= '<a class="button-custom-img spec-button gtm-spec-button" href="'.$link_url.'"'.$no_follow_html.$new_tab_html.'>';
			$button_html .= '<img src="'.get_template_directory_uri().'/lib/plugins/uat2-prp/public/images/amazon-btn-5.png" alt="" />';
			$button_html .= '</a>';
		}

		else {

			$button_text = get_post_meta( $post_id, $spec_id.'_buttontext' , true);
			if ( !$button_text ) {
				if ( $button_type == 'button-aff' ) {
					$button_text = 'Buy on Amazon';
				}
				else if ( $button_type == 'button-full' ) {
					$button_text = 'Full Review';
				}
			}


			$button_html .= '<a class="spec-button '.$button_style.' gtm-spec-button" href="'.$link_url.'"'.$no_follow_html.$new_tab_html.'>';
				$button_html .= $button_text;
			$button_html .= '</a>';

			

		}



		//$button_html .= $button_style;

		return $button_html;

	}







}