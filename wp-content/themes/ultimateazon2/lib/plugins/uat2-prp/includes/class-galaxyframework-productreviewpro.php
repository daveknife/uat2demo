<?php

/**
 * The file that defines the core plugin class
 *
 * A class definition that includes attributes and functions used across both the
 * public-facing side of the site and the admin area.
 *
 * @link       https://incomegalaxy.com
 * @since      1.0.0
 *
 * @package    galfram_productreviewpro
 * @subpackage galfram_productreviewpro/includes
 */

/**
 * The core plugin class.
 *
 * This is used to define internationalization, admin-specific hooks, and
 * public-facing site hooks.
 *
 * Also maintains the unique identifier of this plugin as well as the current
 * version of the plugin.
 *
 * @since      1.0.0
 * @package    galfram_productreviewpro
 * @subpackage galfram_productreviewpro/includes
 * @author     Your Name <email@example.com>
 */
class galfram_productreviewpro {

	/**
	 * The loader that's responsible for maintaining and registering all hooks that power
	 * the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      galfram_productreviewpro_Loader    $loader    Maintains and registers all hooks for the plugin.
	 */
	protected $loader;

	/**
	 * The unique identifier of this plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $galfram_productreviewpro    The string used to uniquely identify this plugin.
	 */
	protected $galfram_productreviewpro;

	/**
	 * The current version of the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $version    The current version of the plugin.
	 */
	protected $version;

	/**
	 * Define the core functionality of the plugin.
	 *
	 * Set the plugin name and the plugin version that can be used throughout the plugin.
	 * Load the dependencies, define the locale, and set the hooks for the admin area and
	 * the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function __construct() {

		$this->galfram_productreviewpro = 'galaxyframework-prp';
		$this->version = '0.3.1';
		$this->edd_product = 'Product Review Pro';
		$this->edd_store = 'https://incomegalaxy.com';

		$this->load_dependencies();
		$this->set_locale();
		$this->define_admin_hooks();
		$this->define_public_hooks();

	}

	/**
	 * Load the required dependencies for this plugin.
	 *
	 * Include the following files that make up the plugin:
	 *
	 * - galfram_productreviewpro_Loader. Orchestrates the hooks of the plugin.
	 * - galfram_productreviewpro_i18n. Defines internationalization functionality.
	 * - galfram_productreviewpro_Admin. Defines all hooks for the admin area.
	 * - galfram_productreviewpro_Public. Defines all hooks for the public side of the site.
	 *
	 * Create an instance of the loader which will be used to register the hooks
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function load_dependencies() {

		/**
		 * The class responsible for orchestrating the actions and filters of the
		 * core plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-galaxyframework-productreviewpro-loader.php';

		/**
		 * The class responsible for defining internationalization functionality
		 * of the plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-galaxyframework-productreviewpro-i18n.php';

		/**
		 * The class responsible for defining all actions that occur in the admin area.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/class-galaxyframework-productreviewpro-admin.php';

		/**
		 * 
		 *
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/amazon-functions/class-galaxyframework-amazon-functions.php';


		/** 
		 * 
		 */
		//require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/page-templater/pagetemplater.php';

		/**
		 * 
		 * 
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'public/class-galaxyframework-productreviewpro-public.php';


		if( !class_exists( 'GALFRAM_PRP_Plugin_Updater' ) ) {
			// load our custom updater
			require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/partials/GALFRAM_PRODUCTREVIEWPRO_Plugin_Updater.php';
		}

		$this->loader = new galfram_productreviewpro_Loader();

	}

	/**
	 * Define the locale for this plugin for internationalization.
	 *
	 * Uses the galfram_productreviewpro_i18n class in order to set the domain and to register the hook
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function set_locale() {

		$plugin_i18n = new galfram_productreviewpro_i18n();

		$this->loader->add_action( 'plugins_loaded', $plugin_i18n, 'load_plugin_textdomain' );

	}

	/**
	 * Register all of the hooks related to the admin area functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_admin_hooks() {

		$plugin_admin = new galfram_productreviewpro_Admin( $this->get_galfram_productreviewpro(), $this->get_version(), $this->edd_product, $this->edd_store );

		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_styles' );
		
		// load updater function
		$this->loader->add_action( 'admin_init', $plugin_admin, 'galfram_productreviewproshort_plugin_updater', 0 );

		// load ACF fields for this plugin
		$this->loader->add_action( 'acf/init', $plugin_admin, 'include_acf_fields' );

		// load licence managament function
		$this->loader->add_action( 'galfram_add_extension_license', $plugin_admin, 'galfram_productreviewproshort_license_management' );

		// Add our custom image sizes
		$this->loader->add_action( 'after_setup_theme', $plugin_admin, 'gf_ext_prp_add_image_sizes' );

		// Add our post types and taxonomies
		$this->loader->add_action( 'init', $plugin_admin, 'gf_ext_prp_add_post_types' );
		
		// Add our plugin options page
		$this->loader->add_action( 'after_setup_theme', $plugin_admin, 'gf_ext_prp_add_style_options_page' );


		// Add user generated spec groups to option to reference later from post
		$this->loader->add_action( 'acf/save_post', $plugin_admin, 'gf_ext_prp_acf_save_specs', 1 );

		// save a random field id value
		$this->loader->add_action( 'acf/save_post', $plugin_admin, 'save_spec_unique_id', 20 );

		// Add user generated spec groups to review single select field
		$this->loader->add_action( 'acf/load_field/name=prod_specs_group', $plugin_admin, 'gf_ext_prp_add_spec_groups' );

		// Add our metabox for the amazon api is selected by user for post
		$this->loader->add_action( 'add_meta_boxes', $plugin_admin, 'galfram_ext_prp_add_amazon_api_metabox' );


		// Add user generated spec groups to option to reference later
		// earliest hook to get post id in post admin
		$this->loader->add_action( 'acf/init', $plugin_admin, 'galfram_ext_prp_add_specs_fields' );


		// Add amazon api lookup ajax function
		$this->loader->add_action( 'wp_ajax_galfram_find_amazon_products_ajax', $plugin_admin, 'galfram_find_amazon_products_ajax' );

		// Add amazon api lookup ajax function to load new pages of results
		$this->loader->add_action( 'wp_ajax_galfram_load_amazon_products_newpage_ajax', $plugin_admin, 'galfram_load_amazon_products_newpage_ajax' );

		// Add insert product from api lookup ajax function (amazon specs only)
		$this->loader->add_action( 'wp_ajax_galfram_amazon_api_insert_chosen_ajax', $plugin_admin, 'galfram_amazon_api_insert_chosen_ajax' );

		// Add insert product from api lookup ajax function (custom specs)
		$this->loader->add_action( 'wp_ajax_galfram_amazon_api_insert_chosen_customspecs_ajax', $plugin_admin, 'galfram_amazon_api_insert_chosen_customspecs_ajax' );

		// Save the user chosen amazon specs to display on frontend review
		$this->loader->add_action( 'wp_ajax_galfram_save_amazon_specs_to_display_ajax', $plugin_admin, 'galfram_save_amazon_specs_to_display_ajax' );

		// add the popup amazon search box
		$this->loader->add_action( 'admin_footer', $plugin_admin, 'amazon_search_thickbox' );

		// saves the chosen spec group to the taxonomy upon saving the review
		$this->loader->add_action( 'save_post', $plugin_admin, 'galfram_save_spec_group_taxonomy' );


		$this->loader->add_filter( 'admin_post_thumbnail_html', $plugin_admin, 'galfram_add_featured_image_note' );

		

	}

	/**
	 * Register all of the hooks related to the public-facing functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_public_hooks() {

		$plugin_public = new galfram_productreviewpro_Public( $this->get_galfram_productreviewpro(), $this->get_version() );

		// add product review post type single loop function
		$this->loader->add_action( 'wp', $plugin_public, 'get_galfram_review_post_type_template' );

		// add product review post type archive loop function
		$this->loader->add_action( 'wp', $plugin_public, 'galfram_review_standard_archive' );

		// add product review post type archive loop function
		$this->loader->add_action( 'wp', $plugin_public, 'galfram_review_grid_archive' );

		// add the popup div for our product review ajax specs quick view
		$this->loader->add_action( 'wp_footer', $plugin_public, 'galfram_quickview_modal' );


		// check that the STylizer child theme is active.
		$current_theme = wp_get_theme();
		$current_theme_slug = $current_theme->get( 'TextDomain' );
		// If it is active
		//if( $current_theme_slug == 'galaxyframework-child-stylizer' ) {
			// add our custom Stylizer styles to that child theme
			$this->loader->add_action( 'galfram_add_custom_ext_css', $plugin_public, 'galfram_add_custom_ext_css_prp' );

		//}

		
		// add the ajax url to the head tag for front end ajax useage
		$this->loader->add_action( 'wp_head', $plugin_public, 'galfram_wp_ajax_url' );

		// populate our quickview info box via ajax
		$this->loader->add_action( 'wp_ajax_nopriv_galfram_quickview_modal_getinfo', $plugin_public, 'galfram_quickview_modal_getinfo' );

		// populate our quickview info box via ajax
		$this->loader->add_action( 'wp_ajax_galfram_quickview_modal_getinfo', $plugin_public, 'galfram_quickview_modal_getinfo' );
		


	}

	/**
	 * Run the loader to execute all of the hooks with WordPress.
	 *
	 * @since    1.0.0
	 */
	public function run() {
		$this->loader->run();
	}

	/**
	 * The name of the plugin used to uniquely identify it within the context of
	 * WordPress and to define internationalization functionality.
	 *
	 * @since     1.0.0
	 * @return    string    The name of the plugin.
	 */
	public function get_galfram_productreviewpro() {
		return $this->galfram_productreviewpro;
	}

	/**
	 * The reference to the class that orchestrates the hooks with the plugin.
	 *
	 * @since     1.0.0
	 * @return    galfram_productreviewpro_Loader    Orchestrates the hooks of the plugin.
	 */
	public function get_loader() {
		return $this->loader;
	}

	/**
	 * Retrieve the version number of the plugin.
	 *
	 * @since     1.0.0
	 * @return    string    The version number of the plugin.
	 */
	public function get_version() {
		return $this->version;
	}

}
