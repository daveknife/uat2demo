<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    galfram_productreviewpro
 * @subpackage galfram_productreviewpro/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    galfram_productreviewpro
 * @subpackage galfram_productreviewpro/admin
 * @author     Your Name <email@example.com>
 */
class galfram_productreviewpro_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $galfram_productreviewpro    The ID of this plugin.
	 */
	private $galfram_productreviewpro;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $galfram_productreviewpro       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $galfram_productreviewpro, $version, $edd_product, $edd_store ) {

		$this->galfram_productreviewpro = $galfram_productreviewpro;
		$this->version = $version;
		$this->edd_product = $edd_product;
		$this->edd_store = $edd_store;

	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in galfram_productreviewpro_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The galfram_productreviewpro_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->galfram_productreviewpro, get_stylesheet_directory_uri() . '/lib/plugins/uat2-prp/admin/css/galaxyframework-productreviewpro-admin.css', array(), $this->version, 'all' );

	}





	

	function galfram_add_featured_image_note( $content ) {
	    return $content .= '<p>'.__('If you want to pull the featured image directly from Amazon, check the box to pull all images from Amazon in the "Additional Images" section below on the left.', 'ultimateazon2').'</p>';
	}


	


	
	/**
	 * Include the Plugin Updater
	 *
	 * @since    1.0.0
	 */
	function galfram_productreviewproshort_plugin_updater() {

		// retrieve our license key from the DB
		$license_key = trim( get_option( 'galfram_plugin_prp_license' ) );

		$plugin_file = ABSPATH . 'wp-content/plugins/galaxyframework-productreviewpro/galaxyframework-prp.php';

		// setup the updater
		$edd_updater = new GALFRAM_PRP_Plugin_Updater( $this->edd_store, $plugin_file, array(
				'version' 	=> $this->version, 				// current version number
				'license' 	=> $license_key, 		// license key (used get_option above to retrieve from DB)
				'item_name' => $this->edd_product, 	// name of this plugin
				'author' 	=> 'Dave Nicosia',  // author of this plugin
				'url'       => home_url()
			)
		);

	}


	/**
	 * Create ACF Styles option page after theme setup
	 *
	 * @since    1.0.0
	 */
	function gf_ext_prp_add_style_options_page() {
		// Add Site Styles Page
		if( function_exists('acf_add_options_page') ) {
			$option_page = acf_add_options_sub_page(array(
				'page_title' 	=> __('Product Reviews - Settings', 'ultimateazon2'),
				'menu_title' 	=> __('Reviews & Amazon', 'ultimateazon2'),
				'menu_slug' 	=> 'extension-prp-settings',
				'capability' 	=> 'edit_posts',
				'post_id'       => 'extension-prp-settings',
				'parent_slug' 	=> 'theme-settings',
				'position'      => 200,
				'redirect' 	=> false
			));
		}
	}













    /**
	 * Create review post type
	 *
	 * @since    1.0.0
	 */
	function gf_ext_prp_add_post_types() {


		// sets main product slug to main product plural field

	    if ( get_field('product_review_pt_plural', 'extension-prp-settings') ) {
	        $prp_rewrite_slug = get_field('product_review_pt_plural', 'extension-prp-settings');
	        // sanitizes and cleans up the slug
	        $prp_rewrite_slug = sanitize_title($prp_rewrite_slug);
	    }
	    else {
	        $prp_rewrite_slug = "reviews";
	    }

	    if ( get_field('product_review_pt_singular', 'extension-prp-settings') && get_field('product_review_pt_plural', 'extension-prp-settings') ) {
	        $singular_name = get_field('product_review_pt_singular', 'extension-prp-settings');
	        $plural_name = get_field('product_review_pt_plural', 'extension-prp-settings');
	    }
	    else {
	        $plural_name = __('Reviews', 'ultimateazon2');
	        $singular_name = __('Review', 'ultimateazon2');
	    }

	    register_post_type( 'galfram_review',
	        array(
	            'labels' => array(
	                'name' => $plural_name,
	                'singular_name' => $singular_name,
	                'add_new' => __('Add New', 'ultimateazon2'),
	                'add_new_item' => __('Add New','ultimateazon2').' '.$singular_name,
	                'edit' => __('Edit','ultimateazon2'),
	                'edit_item' => __('Edit','ultimateazon2').' '.$singular_name,
	                'new_item' => __('New','ultimateazon2').' '.$singular_name,
	                'view' => __('View','ultimateazon2'),
	                'view_item' => __('View','ultimateazon2').' '.$singular_name,
	                'search_items' => __('Search','ultimateazon2').' '.$plural_name,
	                'not_found' => __('No','ultimateazon2').' '.$plural_name.__('found','ultimateazon2'),
	                'not_found_in_trash' => __('No','ultimateazon2').' '.$plural_name.' '.__('found in Trash','ultimateazon2'),
	                'parent' => __('Parent','ultimateazon2').' '.$singular_name
	            ),
	            'public' => true,
	            'show_in_menu' => true,
	            'menu_position' => 7,
	            'rewrite' => array( 'slug' => $prp_rewrite_slug ),
	            'supports' => array( 'title', 'editor', 'author', 'comments', 'thumbnail', 'excerpt', 'trackbacks', 'revisions', 'page-attributes' ),
	            'taxonomies' => array( '' ),
	            'menu_icon' => 'dashicons-star-filled',
	            'has_archive' => true
	        )
	    );


		// Add new taxonomy, make it hierarchical (like categories)
		$labels = array(
			'name'              => _x( 'Review Category', 'taxonomy general name', 'ultimateazon2' ),
			'singular_name'     => _x( 'Review Category', 'taxonomy singular name', 'ultimateazon2' ),
			'search_items'      => __( 'Search Review Categories', 'ultimateazon2' ),
			'all_items'         => __( 'All Review Categories', 'ultimateazon2' ),
			'parent_item'       => __( 'Parent Review Category', 'ultimateazon2' ),
			'parent_item_colon' => __( 'Parent Review Category:', 'ultimateazon2' ),
			'edit_item'         => __( 'Edit Review Category', 'ultimateazon2' ),
			'update_item'       => __( 'Update Review Category', 'ultimateazon2' ),
			'add_new_item'      => __( 'Add New Review Category', 'ultimateazon2' ),
			'new_item_name'     => __( 'New Review Category Name', 'ultimateazon2' ),
			'menu_name'         => __( 'Review Category', 'ultimateazon2' ),
		);
		$args = array(
			'hierarchical'      => true,
			'labels'            => $labels,
			'show_ui'           => true,
			'show_admin_column' => true,
			'query_var'         => true,
			'rewrite'           => array( 'slug' => 'review-category' ),
		);
		register_taxonomy( 'galfram_review_cat', array( 'galfram_review' ), $args );





		$labels = array(
			"name" => __( 'Chosen Spec Group', 'ultimateazon2' ),
			"singular_name" => __( 'Chosen Spec Groups', 'ultimateazon2' ),
		);
		$args = array(
			"label" => __( 'Chosen Spec Group', 'ultimateazon2' ),
			"labels" => $labels,
			"public" => true,
			"hierarchical" => true,
			"label" => "Chosen Spec Group",
			"show_ui" => false,
			"show_in_menu" => false,
			"show_in_nav_menus" => false,
			"query_var" => true,
			"rewrite" => array( 'slug' => 'galfram_spec_group', 'with_front' => true, ),
			"show_admin_column" => false,
			"show_in_rest" => false,
			"rest_base" => "",
			"show_in_quick_edit" => false,
		);
		register_taxonomy( "galfram_spec_group", array( "galfram_review" ), $args );

		
	}









	/**
	 * Save a unique(random) product spec ID for each new spec.
	 *
	 * @since    1.0.0
	 */
	public function save_spec_unique_id( $post_id ) {

		$screen = get_current_screen($post_id);


		// bail early if no ACF data or not on the correct screen
	    if( empty($_POST['acf']) || $screen->id != 'ultimate-azon_page_extension-prp-settings' ) {
	        return;
	    }

	    // do I nee dthis? I don't think so.
	    $spec_groups = get_field('product_specifications_groups', $post_id);

	    if( have_rows('product_specifications_groups', $post_id) ) {

			$i = 0;

			while( have_rows('product_specifications_groups', $post_id) ) {

				the_row();

				if ( get_sub_field('specification_group_id') == 'noid' ) {

					$random_spec_id = 'spec_'.substr(md5(microtime()),rand(0,26),10);
					update_sub_field( 'specification_group_id', $random_spec_id );

				}

				if( have_rows('custom_product_specifications') ):
				
					while ( have_rows('custom_product_specifications') ) : the_row();

						$spec_id = get_sub_field('spec_id');

						if ( $spec_id == 'noid' ) {
							$random_spec_id = 'spec_'.substr(md5(microtime()),rand(0,26),10);
							update_sub_field( 'spec_id', $random_spec_id );
						}
				
					endwhile;
				endif;

				$i++;

			}

		}

		//die();

	}





	/**
	 * Save th chosen spec group as a tax term and set this term for this post
	 *
	 * @since    1.0.0
	 */
	public function galfram_save_spec_group_taxonomy( $post_id ) {

		// If this is just a revision, exitthe function
		if ( wp_is_post_revision( $post_id ) ) {
			return;
		}
		// if it is not a galfram_review post, exit the function
		if ( get_post_type() != 'galfram_review' ) {
			return;
		}

		// if the spec group is not chosen, exit the function
		if ( !isset( $_POST['acf']['field_57c1fcf9ce98a'] ) ) {
			return;
		}

		// set our hidden custom specs group taxonomy
		$taxonomy = 'galfram_spec_group';

		$chosen_specs_group = $_POST['acf']['field_57c1fcf9ce98a'];


		// get our list of galfram_spec_group terms
		$terms = get_terms( $taxonomy, array('hide_empty' => false ) );

		// if the term already exists, save the term to the post then exit
		if ( term_exists( $chosen_specs_group, $taxonomy ) ) {
			$terms = array( $chosen_specs_group );
			// save the term to our review post - false makes it so this will override the previously saved terms
			wp_set_object_terms( $post_id, $terms, $taxonomy, false );
			return;
		}

		// since the term does not exists based off the check above, let's add it
		wp_insert_term( $chosen_specs_group, $taxonomy );

		$terms = array( $chosen_specs_group );
		// save the term to our review post - false makes it so this will override the previously saved terms
		wp_set_object_terms( $post_id, $terms, $taxonomy, false );

		// get our chosen specs group after the review post saves
		$chosen_specs_group = get_post_meta( $post_id, 'prod_specs_group', true );

	}










	/**
	 * Include the ACF fields
	 *
	 * @since    1.0.0
	 */
	public function include_acf_fields() {


		if( function_exists('acf_add_local_field_group') ):

			



			// The main Product Review Pro Fields

			acf_add_local_field_group(array (
				'key' => 'group_57c0654775bf7',
				'title' => 'Product Review Pro',
				'fields' => array (
					array (
						'key' => 'field_57c066543ff54',
						'label' => __('Global Settings', 'ultimateazon2' ),
						'name' => '',
						'type' => 'tab',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'placement' => 'left',
						'endpoint' => 0,
					),
					array (
						'key' => 'field_57c0948518058',
						'label' => __('Product Review Post Type', 'ultimateazon2' ),
						'name' => '',
						'type' => 'message',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'message' => __('<p>Product Review Pro adds a new post type to your website that is used to create your reviews on. Here you will create the name for it.</p>
							<p>If you are going to be reviewing multiple types of products, we recommend using something like these examples in the fields below:</p>
							<p>Singular: Review<br />Plural: Reviews</p>
							<p>or</p>
							<p>Singular: Product<br />Plural: Products</p>
							<p>URL examples:</p>
							<p>http://website.com/reviews/my-great-review/<br />http://website.com/products/my-great-review/</p>
							<p>If you are only going to be reviewing one type of product, you can use the product name if you wish.</p>
							<p>Singular: Camping Tent<br />Plural: Camping Tents</p>
							<p>http://website.com/camping-tents/my-great-review/</p>', 'ultimateazon2' ),
						'new_lines' => 'wpautop',
						'esc_html' => 0,
					),
					array (
						'key' => 'field_57c0937d18056',
						'label' => __('Product Review Post Type Singular Name', 'ultimateazon2' ),
						'name' => 'product_review_pt_singular',
						'type' => 'text',
						'instructions' => __('Add your singular name for your Product Review Post Type here.', 'ultimateazon2' ),
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '50',
							'class' => '',
							'id' => '',
						),
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'maxlength' => '',
					),
					array (
						'key' => 'field_765765f654f65d4d',
						'label' => __('Product Review Post Type Plural Name', 'ultimateazon2' ),
						'name' => 'product_review_pt_plural',
						'type' => 'text',
						'instructions' => __('Add your plural name for your Product Review Post Type here.', 'ultimateazon2' ),
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '50',
							'class' => '',
							'id' => '',
						),
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'maxlength' => '',
					),
					array (
						'key' => 'field_57c4f5e3cc45f',
						'label' => __('Important Notice', 'ultimateazon2' ),
						'name' => '',
						'type' => 'message',
						'instructions' => __('Add your plural name for your Product Review Post Type here. This will be displayed in the URL of your product reviews. WordPress requires this part to be present in the URL for custom post types such as this.', 'ultimateazon2' ),
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'message' => __('If you add a singular and plural rewrite here for your review post type, you must visit your', 'ultimateazon2').' <a href="/wp-admin/options-permalink.php">'.__('permalinks page settings page', 'ultimateazon2').'</a>'.__(' and click "Save Changes" to reset your permalinks cache. You do not have to change anything on this page, just click the button.', 'ultimateazon2' ),
						'new_lines' => 'wpautop',
						'esc_html' => 0,
					),
					array (
						'key' => 'field_57c7258fec92e',
						'label' => __('Open Affiliate Links in New Window', 'ultimateazon2' ),
						'name' => 'open_aff_links_in_new_window',
						'type' => 'true_false',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'message' => __('Check this box if you want all auto-generated affiliate links to open in a new window', 'ultimateazon2' ),
						'default_value' => 1,
					),
					array (
						'key' => 'field_57c725d2ec92f',
						'label' => __('No-Follow Affiliate Links', 'ultimateazon2' ),
						'name' => 'no-follow_affiliate_links',
						'type' => 'true_false',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'message' => __('Check this box if you want all auto-generated affiliate links to be no-followed', 'ultimateazon2' ),
						'default_value' => 0,
					),
					array (
						'key' => 'field_57c06834622b4',
						'label' => __('Product Specifications', 'ultimateazon2' ),
						'name' => '',
						'type' => 'tab',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'placement' => 'left',
						'endpoint' => 0,
					),
					array (
						'key' => 'field_57c068c8622b5',
						'label' => __('Product Specifications Groups', 'ultimateazon2' ),
						'name' => 'product_specifications_groups',
						'type' => 'repeater',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'collapsed' => 'field_57c068df622b6',
						'min' => 1,
						'max' => '',
						'layout' => 'row',
						'button_label' => __('Add Product Specification Group', 'ultimateazon2' ),
						'sub_fields' => array (
							array (
								'key' => 'field_57c35f7e8031b',
								'label' => __('Specification Group ID', 'ultimateazon2' ),
								'name' => 'specification_group_id',
								'type' => 'text',
								'instructions' => '',
								'required' => 0,
								'conditional_logic' => 0,
								'wrapper' => array (
									'width' => '',
									'class' => '',
									'id' => '',
								),
								'default_value' => 'noid',
								'placeholder' => '',
								'prepend' => '',
								'append' => '',
								'maxlength' => '',
							),
							array (
								'key' => 'field_57c068df622b6',
								'label' => __('Specification Group Name', 'ultimateazon2' ),
								'name' => 'specification_group_name',
								'type' => 'text',
								'instructions' => '',
								'required' => 0,
								'conditional_logic' => 0,
								'wrapper' => array (
									'width' => '',
									'class' => '',
									'id' => '',
								),
								'default_value' => '',
								'placeholder' => '',
								'prepend' => '',
								'append' => '',
								'maxlength' => '',
							),
							array (
								'key' => 'field_57c068f1622b7',
								'label' => __('Specification Group Type', 'ultimateazon2' ),
								'name' => 'specification_set',
								'type' => 'select',
								'instructions' => __('Choose what type of product specifications group type you would like to use for this group.', 'ultimateazon2' ),
								'required' => 0,
								'conditional_logic' => 0,
								'wrapper' => array (
									'width' => '',
									'class' => '',
									'id' => '',
								),
								'choices' => array (
									'create-my-own' => __('Create My Own', 'ultimateazon2' ),
								),
								'default_value' => array (
								),
								'allow_null' => 0,
								'multiple' => 0,
								'ui' => 0,
								'ajax' => 0,
								'return_format' => 'value',
								'placeholder' => '',
							),
							array (
								'key' => 'field_57c06aa9622b9',
								'label' => __('Custom Product Specifications', 'ultimateazon2' ),
								'name' => 'custom_product_specifications',
								'type' => 'repeater',
								'instructions' => '',
								'required' => 0,
								'conditional_logic' => array (
									array (
										array (
											'field' => 'field_57c068f1622b7',
											'operator' => '==',
											'value' => 'create-my-own',
										),
									),
								),
								'wrapper' => array (
									'width' => '',
									'class' => '',
									'id' => '',
								),
								'collapsed' => 'field_57c06afc622ba',
								'min' => 1,
								'max' => '',
								'layout' => 'block',
								'button_label' => __('Add Specification', 'ultimateazon2' ),
								'sub_fields' => array (
									array (
										'key' => 'field_57c6bfb62908a',
										'label' => __('Spec ID', 'ultimateazon2' ),
										'name' => 'spec_id',
										'type' => 'text',
										'instructions' => '',
										'required' => 0,
										'conditional_logic' => 0,
										'wrapper' => array (
											'width' => '',
											'class' => '',
											'id' => '',
										),
										'default_value' => 'noid',
										'placeholder' => '',
										'prepend' => '',
										'append' => '',
										'maxlength' => '',
									),
									array (
										'key' => 'field_57c06afc622ba',
										'label' => __('Specification Name', 'ultimateazon2' ),
										'name' => 'specification_name',
										'type' => 'text',
										'instructions' => __('This will be shown on your website', 'ultimateazon2' ),
										'required' => 0,
										'conditional_logic' => 0,
										'wrapper' => array (
											'width' => '',
											'class' => '',
											'id' => '',
										),
										'default_value' => '',
										'placeholder' => '',
										'prepend' => '',
										'append' => '',
										'maxlength' => '',
									),
									array (
										'key' => 'field_5824dc0feb5f7',
										'label' => __('Hide This Label', 'ultimateazon2' ),
										'name' => 'hide_this_label',
										'type' => 'true_false',
										'instructions' => '',
										'required' => 0,
										'conditional_logic' => array (
											array (
												array (
													'field' => 'field_57c06b19622bb',
													'operator' => '!=',
													'value' => 'star-rating',
												),
												array (
													'field' => 'field_57c06b19622bb',
													'operator' => '!=',
													'value' => 'yes-no',
												),
												array (
													'field' => 'field_57c06b19622bb',
													'operator' => '!=',
													'value' => 'price',
												),
												array (
													'field' => 'field_57c06b19622bb',
													'operator' => '!=',
													'value' => 'spec-button',
												),
											),
										),
										'wrapper' => array (
											'width' => '',
											'class' => '',
											'id' => '',
										),
										'message' => __('Check this box to hide the label for this specification.', 'ultimateazon2' ),
										'default_value' => 0,
									),
									array (
										'key' => 'field_57c06b19622bb',
										'label' => __('Specification Type', 'ultimateazon2' ),
										'name' => 'specification_type',
										'type' => 'select',
										'instructions' => __('Choose which type of data this specification will use.', 'ultimateazon2' ),
										'required' => 0,
										'conditional_logic' => 0,
										'wrapper' => array (
											'width' => '',
											'class' => '',
											'id' => '',
										),
										'choices' => array (
											'text' => 'Plain Text',
											'p-text' => __('Paragraph Text', 'ultimateazon2' ),
											'star-rating' => __('Star Rating', 'ultimateazon2' ),
											'yes-no' => __('Yes / No', 'ultimateazon2' ),
											'image' => __('Image', 'ultimateazon2' ),
											'price' => __('Price', 'ultimateazon2' ),
											'spec-button' => __('Button', 'ultimateazon2' ),
											'spec-shortcode' => __('Shortcode', 'ultimateazon2' ),
											'amazon' => __('Pull from Amazon', 'ultimateazon2' ),
										),
										'default_value' => array (
										),
										'allow_null' => 0,
										'multiple' => 0,
										'ui' => 0,
										'ajax' => 0,
										'return_format' => 'value',
										'placeholder' => '',
									),
									array (
										'key' => 'field_57c7052ff242d',
										'label' => __('Choose Amazon Field to Import', 'ultimateazon2' ),
										'name' => 'amazon_api_spec_import',
										'type' => 'radio',
										'instructions' => __('Choose which Amazon API field you would like to import into this product specification.', 'ultimateazon2' ),
										'required' => 0,
										'conditional_logic' => array (
											array (
												array (
													'field' => 'field_57c06b19622bb',
													'operator' => '==',
													'value' => 'amazon',
												),
											),
										),
										'wrapper' => array (
											'width' => '',
											'class' => '',
											'id' => '',
										),
										'choices' => array (
											'asin' => __('ASIN', 'ultimateazon2' ),
											'brand' => __('Brand', 'ultimateazon2' ),
											'model' => __('Model', 'ultimateazon2' ),
											'upc' => __('UPC', 'ultimateazon2' ),
											'features' => __('Features', 'ultimateazon2' ),
											'warranty' => __('Warranty', 'ultimateazon2' ),
											'price' => __('Price', 'ultimateazon2' ),
											'lowest-new-price' => __('Lowest New Price', 'ultimateazon2' ),
											'lowest-used-price' => __('Lowest Used Price', 'ultimateazon2' ),
											'small-image' => __('Small Image', 'ultimateazon2' ),
											'medium-image' => __('Medium Image', 'ultimateazon2' ),
											'large-image' => __('Large Image', 'ultimateazon2' ),
										),
										'allow_null' => 0,
										'other_choice' => 0,
										'save_other_choice' => 0,
										'default_value' => '',
										'layout' => 'horizontal',
										'return_format' => 'value',
									),
									array (
										'key' => 'field_58061e2c0864b',
										'label' => __('Price Fields', 'ultimateazon2' ),
										'name' => 'price_fields',
										'type' => 'checkbox',
										'instructions' => '',
										'required' => 0,
										'conditional_logic' => array (
											array (
												array (
													'field' => 'field_57c7052ff242d',
													'operator' => '==',
													'value' => 'price',
												),
											),
										),
										'wrapper' => array (
											'width' => '',
											'class' => '',
											'id' => '',
										),
										'choices' => array (
											'listprice' => __('List Price', 'ultimateazon2' ),
											'discountprice' => __('Discounted Price', 'ultimateazon2' ),
											'amountsaved' => __('Amount Saved', 'ultimateazon2' ),
											'percentagesaved' => __('Percentage Saved', 'ultimateazon2' ),
										),
										'default_value' => array (
										),
										'layout' => 'horizontal',
										'toggle' => 0,
										'return_format' => 'value',
									),
								),
							),
						),
					),
					array (
						'key' => 'field_57c065679fbbe',
						'label' => __('Amazon API', 'ultimateazon2' ),
						'name' => '',
						'type' => 'tab',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'placement' => 'left',
						'endpoint' => 0,
					),
					array (
						'key' => 'field_57c728da230ed',
						'label' => __('Enable Amazon API Integration', 'ultimateazon2' ),
						'name' => 'enable_amazon_api_integration',
						'type' => 'true_false',
						'instructions' => __('Product Review Pro can easily pull product information, images, and data form the Amazon API.', 'ultimateazon2' ).'<br /><br /><a href="https://ultimateazontheme.com/amazon-api-instructions/" target="_blank">' . __('Click Here for instructions on setting up your Amazon AWS Api key and Secret Key.','ultimateazon2') .'</a>',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'message' => __('Check this box to enable the Amazon API Integration.', 'ultimateazon2' ),
						'default_value' => 0,
					),
					array (
						'key' => 'field_57c065809fbbf',
						'label' => __('Amazon API Access Key', 'ultimateazon2' ),
						'name' => 'amazon_api_key',
						'type' => 'text',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_57c728da230ed',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'maxlength' => '',
					),
					array (
						'key' => 'field_57c724340dd7c',
						'label' => __('Amazon Search Default Locale', 'ultimateazon2' ),
						'name' => '',
						'type' => 'message',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_57c728da230ed',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'message' => __('Please set your default search locale using the option below. When you use Product Review Pro, the links you create will default to this location so if most of your visitors come from the United States for example, you would set your default locale to United States. LINK LOCALIZATION IS NOT AVAILABLE AT THIS TIME.', 'ultimateazon2' ),
						'new_lines' => 'wpautop',
						'esc_html' => 0,
					),
					array (
						'key' => 'field_57c724860dd7d',
						'label' => __('Default Amazon Search Locale', 'ultimateazon2' ),
						'name' => 'default_amazon_search_locale',
						'type' => 'select',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_57c728da230ed',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'choices' => array (
							'US' => 'United States',
							'UK' => 'United Kingdom',
							'BR' => 'Brazil',
							'CA' => 'Canada',
							'CN' => 'China',
							'FR' => 'France',
							'DE' => 'Germany',
							'IN' => 'India',
							'IT' => 'Italy',
							'JP' => 'Japan',
							'MX' => 'Mexico',
							'ES' => 'Spain',
						),
						'default_value' => array (
						),
						'allow_null' => 0,
						'multiple' => 0,
						'ui' => 0,
						'ajax' => 0,
						'return_format' => 'value',
						'placeholder' => '',
					),
					array (
						'key' => 'field_57c065899fbc0',
						'label' => __('Amazon API Secret Key', 'ultimateazon2' ),
						'name' => 'amazon_api_secret',
						'type' => 'text',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_57c728da230ed',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'maxlength' => '',
					),
					array (
						'key' => 'field_57c721f97b2b7',
						'label' => __('Amazon Affiliate IDs', 'ultimateazon2' ),
						'name' => '',
						'type' => 'message',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_57c728da230ed',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'message' => __('Input your Amazon Affiliate ID\'s here.', 'ultimateazon2' ),
						'new_lines' => 'wpautop',
						'esc_html' => 0,
					),
					array (
						'key' => 'field_57c720fe7b2ac',
						'label' => __('Amazon Affiliate ID - United States', 'ultimateazon2' ),
						'name' => 'amazon_affiliate_id_US',
						'type' => 'text',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_57c728da230ed',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'maxlength' => '',
					),
					array (
						'key' => 'field_57c721e87b2b6',
						'label' => __('Amazon Affiliate ID - United Kingdom', 'ultimateazon2' ),
						'name' => 'amazon_affiliate_id_UK',
						'type' => 'text',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_57c728da230ed',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'maxlength' => '',
					),
					array (
						'key' => 'field_57c721617b2ad',
						'label' => __('Amazon Affiliate ID - Brazil', 'ultimateazon2' ),
						'name' => 'amazon_affiliate_id_BR',
						'type' => 'text',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_57c728da230ed',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'maxlength' => '',
					),
					array (
						'key' => 'field_57c7216b7b2ae',
						'label' => __('Amazon Affiliate ID - Canada', 'ultimateazon2' ),
						'name' => 'amazon_affiliate_id_CA',
						'type' => 'text',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_57c728da230ed',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'maxlength' => '',
					),
					array (
						'key' => 'field_57c721767b2af',
						'label' => __('Amazon Affiliate ID - China', 'ultimateazon2' ),
						'name' => 'amazon_affiliate_id_CN',
						'type' => 'text',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_57c728da230ed',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'maxlength' => '',
					),
					array (
						'key' => 'field_57c721877b2b0',
						'label' => __('Amazon Affiliate ID - France', 'ultimateazon2' ),
						'name' => 'amazon_affiliate_id_FR',
						'type' => 'text',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_57c728da230ed',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'maxlength' => '',
					),
					array (
						'key' => 'field_57c721927b2b1',
						'label' => __('Amazon Affiliate ID - Germany', 'ultimateazon2' ),
						'name' => 'amazon_affiliate_id_DE',
						'type' => 'text',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_57c728da230ed',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'maxlength' => '',
					),
					array (
						'key' => 'field_57c7219e7b2b2',
						'label' => __('Amazon Affiliate ID - India', 'ultimateazon2' ),
						'name' => 'amazon_affiliate_id_IN',
						'type' => 'text',
						'instructions' => __('You must be a resident of India to join this country\'s affiliate program', 'ultimateazon2' ),
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_57c728da230ed',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'maxlength' => '',
					),
					array (
						'key' => 'field_57c721c47b2b3',
						'label' => __('Amazon Affiliate ID - Italy', 'ultimateazon2' ),
						'name' => 'amazon_affiliate_id_IT',
						'type' => 'text',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_57c728da230ed',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'maxlength' => '',
					),
					array (
						'key' => 'field_57c721cf7b2b4',
						'label' => __('Amazon Affiliate ID - Japan', 'ultimateazon2' ),
						'name' => 'amazon_affiliate_id_JP',
						'type' => 'text',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_57c728da230ed',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'maxlength' => '',
					),
					array (
						'key' => 'field_58155565b0d7d',
						'label' => __('Amazon Affiliate ID - Mexico', 'ultimateazon2' ),
						'name' => 'amazon_affiliate_id_MX',
						'type' => 'text',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_57c728da230ed',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'maxlength' => '',
					),
					array (
						'key' => 'field_57c721d97b2b5',
						'label' => __('Amazon Affiliate ID - Spain', 'ultimateazon2' ),
						'name' => 'amazon_affiliate_id_ES',
						'type' => 'text',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_57c728da230ed',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'maxlength' => '',
					),
					array (
						'key' => 'field_57c6bff32908b',
						'label' => __('Single Review Page', 'ultimateazon2' ),
						'name' => '',
						'type' => 'tab',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'placement' => 'left',
						'endpoint' => 0,
					),
					array (
						'key' => 'field_7b6856v7r76tb8',
						'label' => __( 'Review Single - Layout Type', 'ultimateazon2' ),
						'name' => 'single_layout_type_prp',
						'type' => 'select',
						'instructions' => __( 'Choose which layout you would like to use for your single "reviews".', 'ultimateazon2' ),
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'choices' => array (
							'content-sidebar' => __( 'Content / Sidebar', 'ultimateazon2' ),
							'sidebar-content' => __( 'Sidebar / Content', 'ultimateazon2' ),
						),
						'default_value' => array (
							0 => 'content-sidebar',
						),
						'allow_null' => 0,
						'multiple' => 0,
						'ui' => 0,
						'ajax' => 0,
						'placeholder' => '',
						'disabled' => 0,
						'readonly' => 0,
					),
					array (
						'key' => 'field_57c6c28f2908c',
						'label' => __('Hide or Show Sidebar Product Link Button', 'ultimateazon2' ),
						'name' => 'hide_sidebar_prod_link_btn',
						'type' => 'radio',
						'instructions' => __('This option can be overridden on each Review post', 'ultimateazon2' ),
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'choices' => array (
							'show-sidebar-btn' => __('Show all Sidebar Buttons on Reviews', 'ultimateazon2' ),
							'hide-sidebar-btn' => __('Hide all Sidebar Buttons on Reviews', 'ultimateazon2' ),
						),
						'allow_null' => 0,
						'other_choice' => 0,
						'save_other_choice' => 0,
						'default_value' => __('show-sidebar-btn : Show all Sidebar Buttons on Reviews', 'ultimateazon2' ),
						'layout' => 'vertical',
						'return_format' => 'value',
					),
					array (
						'key' => 'field_57c0947418057',
						'label' => __('Sidebar Product Link Button Text', 'ultimateazon2' ),
						'name' => 'sidebar_product_button_text',
						'type' => 'text',
						'instructions' => __('Add the text you want to display in the affiliate link button in the product review sidebar. This can be overridden on a per post basis.', 'ultimateazon2' ),
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'maxlength' => '',
					),
					array (
						'key' => 'field_57d5fc2bca155',
						'label' => __('Link or Unlink Top Feature Image', 'ultimateazon2' ),
						'name' => 'no_image_link',
						'type' => 'radio',
						'instructions' => __('This option can be overridden on each Review post', 'ultimateazon2' ),
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'choices' => array (
							'link-top-img' => __('Link all Review\'s Featured Images', 'ultimateazon2' ),
							'unlink-top-img' => __('Do not Link all Review\'s featured Images', 'ultimateazon2' ),
						),
						'allow_null' => 0,
						'other_choice' => 0,
						'save_other_choice' => 0,
						'default_value' => __('link-top-img : Link all Review\'s Featured Images', 'ultimateazon2' ),
						'layout' => 'vertical',
						'return_format' => 'value',
					),
					array (
						'key' => 'field_57d603373dc1f',
						'label' => __('Hide or Show Top Link Bar', 'ultimateazon2' ),
						'name' => 'hide_top_link_bar',
						'type' => 'radio',
						'instructions' => __('(Underneath Featured Images) This option can be overridden on each Review post', 'ultimateazon2' ),
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '50',
							'class' => '',
							'id' => '',
						),
						'choices' => array (
							'show-top-link-bar' => __('Show Top Link Bar on All Reviews', 'ultimateazon2' ),
							'hide-top-link-bar' => __('Hide Top Link Bar on All Reviews', 'ultimateazon2' ),
						),
						'allow_null' => 0,
						'other_choice' => 0,
						'save_other_choice' => 0,
						'default_value' => __('show-top-link-bar : Show Top Link Bar on All Reviews', 'ultimateazon2' ),
						'layout' => 'vertical',
						'return_format' => 'value',
					),
					array (
						'key' => 'field_57d71bdeb381b',
						'label' => __('Hide or Show Bottom Link Bar', 'ultimateazon2' ),
						'name' => 'hide_bottom_link_bar',
						'type' => 'radio',
						'instructions' => __('(Underneath Full Review Body) This option can be overridden on each Review post', 'ultimateazon2' ),
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '50',
							'class' => '',
							'id' => '',
						),
						'choices' => array (
							'show-bottom-link-bar' => __('Show Bottom Link Bar on All Reviews', 'ultimateazon2' ),
							'hide-bottom-link-bar' => __('Hide Bottom Link Bar on All Reviews', 'ultimateazon2' ),
						),
						'allow_null' => 0,
						'other_choice' => 0,
						'save_other_choice' => 0,
						'default_value' => __('show-bottom-link-bar : Show Bottom Link Bar on All Reviews', 'ultimateazon2' ),
						'layout' => 'vertical',
						'return_format' => 'value',
					),
					array (
						'key' => 'field_57d604e32d090',
						'label' => __('Top Link Bar Text', 'ultimateazon2' ),
						'name' => 'top_link_bar_text',
						'type' => 'text',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '50',
							'class' => '',
							'id' => '',
						),
						'default_value' => __('Buy on Amazon', 'ultimateazon2' ),
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'maxlength' => '',
					),
					array (
						'key' => 'field_57d604ee2d091',
						'label' => __('Bottom Link Bar Text', 'ultimateazon2' ),
						'name' => 'bottom_link_bar_text',
						'type' => 'text',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '50',
							'class' => '',
							'id' => '',
						),
						'default_value' => __('Buy on Amazon', 'ultimateazon2' ),
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'maxlength' => '',
					),
					array (
						'key' => 'field_58225a8313778',
						'label' => __('Review Archive Page', 'ultimateazon2' ),
						'name' => '',
						'type' => 'tab',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'placement' => 'left',
						'endpoint' => 0,
					),
					array (
						'key' => 'field_erg4vy6grefce5gt',
						'label' => __( 'Review Archive - Layout Type', 'ultimateazon2' ),
						'name' => 'blog_layout_type_prp',
						'type' => 'select',
						'instructions' => __( 'What you select here will affect all archive pages, including the blog archive, the category archives, and the tag archives.', 'ultimateazon2' ),
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'choices' => array (
							'content-sidebar' => __( 'Content / Sidebar', 'ultimateazon2' ),
							'sidebar-content' => __( 'Sidebar / Content', 'ultimateazon2' ),
							'content-grid-sidebar' => __( 'Content Grid / Sidebar', 'ultimateazon2' ),
							'sidebar-content-grid' => __( 'Sidebar / Content Grid', 'ultimateazon2' ),
							'content-grid' => __( 'Content Grid', 'ultimateazon2' ),
						),
						'default_value' => array (
							0 => 'content-sidebar',
						),
						'allow_null' => 0,
						'multiple' => 0,
						'ui' => 0,
						'ajax' => 0,
						'placeholder' => '',
						'disabled' => 0,
						'readonly' => 0,
					),
					array (
						'key' => 'field_876g5f7v8gbh9',
						'label' => __( 'Grid Columns ( Desktop )', 'ultimateazon2' ),
						'name' => 'grid_columns_prp',
						'type' => 'select',
						'instructions' => __( 'Choose how many columns you want in your grid on desktop views.', 'ultimateazon2' ),
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_erg4vy6grefce5gt',
									'operator' => '==',
									'value' => 'content-grid-sidebar',
								),
							),
							array (
								array (
									'field' => 'field_erg4vy6grefce5gt',
									'operator' => '==',
									'value' => 'sidebar-content-grid',
								),
							),
							array (
								array (
									'field' => 'field_erg4vy6grefce5gt',
									'operator' => '==',
									'value' => 'content-grid',
								),
							),
						),
						'wrapper' => array (
							'width' => 50,
							'class' => '',
							'id' => '',
						),
						'choices' => array (
							'large-6' => 2,
							'large-4' => 3,
							'large-3' => 4,
							'large-col5' => 5,
							'large-2' => 6,
						),
						'default_value' => array (
							0 => 'large-4',
						),
						'allow_null' => 0,
						'multiple' => 0,
						'ui' => 0,
						'ajax' => 0,
						'placeholder' => '',
						'disabled' => 0,
						'readonly' => 0,
					),
					array (
						'key' => 'field_7865vc4rv7tb8y',
						'label' => __( 'Grid Columns ( Tablets )', 'ultimateazon2' ),
						'name' => 'grid_columns_tablets_prp',
						'type' => 'select',
						'instructions' => __( 'Choose how many columns you want in your grid on tablet views.', 'ultimateazon2' ),
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_erg4vy6grefce5gt',
									'operator' => '==',
									'value' => 'content-grid-sidebar',
								),
							),
							array (
								array (
									'field' => 'field_erg4vy6grefce5gt',
									'operator' => '==',
									'value' => 'sidebar-content-grid',
								),
							),
							array (
								array (
									'field' => 'field_erg4vy6grefce5gt',
									'operator' => '==',
									'value' => 'content-grid',
								),
							),
						),
						'wrapper' => array (
							'width' => 50,
							'class' => '',
							'id' => '',
						),
						'choices' => array (
							'medium-6' => 2,
							'medium-4' => 3,
							'medium-3' => 4,
							'medium-col5' => 5,
							'medium-2' => 6,
						),
						'default_value' => array (
							0 => 'medium-6',
						),
						'allow_null' => 0,
						'multiple' => 0,
						'ui' => 0,
						'ajax' => 0,
						'placeholder' => '',
						'disabled' => 0,
						'readonly' => 0,
					),
					array (
						'key' => 'field_r7u68u78ubh7tv6gr',
						'label' => __( 'Review Archive Page Heading (H1 Tag)', 'ultimateazon2' ),
						'name' => 'h1_tag_prp',
						'type' => 'text',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'maxlength' => '',
						'readonly' => 0,
						'disabled' => 0,
					),
					array (
						'key' => 'field_5vt46t56t56y56',
						'label' => __( 'Review Archive Page Intro', 'ultimateazon2' ),
						'name' => 'index_page_intro_prp',
						'type' => 'wysiwyg',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'default_value' => '',
						'tabs' => 'all',
						'toolbar' => 'full',
						'media_upload' => 1,
					),
					array (
						'key' => 'field_58225a9613779',
						'label' => __('Disable "Quick View"', 'ultimateazon2' ),
						'name' => 'disable_quick_view',
						'type' => 'true_false',
						'instructions' => __('Check this box to disable the "Quick View" slideup link bar tat when clicked, shows the visitor the product specifications, image, full review link, and main review image.', 'ultimateazon2' ),
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'message' => __('Check this to disable the "Quick View" slideup link bar.', 'ultimateazon2' ),
						'default_value' => 0,
					),
					array (
						'key' => 'field_58249dabdad89',
						'label' => __('Full Review Button Text', 'ultimateazon2' ),
						'name' => 'full_review_button_text',
						'type' => 'text',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '50',
							'class' => '',
							'id' => '',
						),
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'maxlength' => '',
					),
					array (
						'key' => 'field_58225b371377a',
						'label' => __('Disable Full Review Button in the "Quick View" popup modal.', 'ultimateazon2' ),
						'name' => 'disable_full_review_btn',
						'type' => 'true_false',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '50',
							'class' => '',
							'id' => '',
						),
						'message' => __('Check this box to disable the Full Review Button in the "Quick View" popup modal.', 'ultimateazon2' ),
						'default_value' => 0,
					),
					array (
						'key' => 'field_58249db7dad8a',
						'label' => __('Affiliate Link Button Text', 'ultimateazon2' ),
						'name' => 'aff_link_button_text',
						'type' => 'text',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '50',
							'class' => 'djn-clear-all',
							'id' => '',
						),
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'maxlength' => '',
					),
					array (
						'key' => 'field_58225bad1377b',
						'label' => __('Disable Affiliate Button in the "Quick View" popup modal.', 'ultimateazon2' ),
						'name' => 'disable_affiliate_btn',
						'type' => 'true_false',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '50',
							'class' => '',
							'id' => '',
						),
						'message' => __('Check this box to disable the Affiliate Link Button in the "Quick View" popup modal.', 'ultimateazon2' ),
						'default_value' => 0,
					),
					array (
						'key' => 'field_5823577ce97cc',
						'label' => __('Disable Product Specs in the "Quick View" popup modal.', 'ultimateazon2' ),
						'name' => 'disable_prod_specs',
						'type' => 'true_false',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'message' => __('Check this box to disable the Product Specifications in the "Quick View" popup modal.', 'ultimateazon2' ),
						'default_value' => 0,
					),
					array (
						'key' => 'field_58238992e94ba',
						'label' => __('Disable Bottom Affiliate Link in the "Quick View" popup modal.', 'ultimateazon2' ),
						'name' => 'disable_bottom_aff_link',
						'type' => 'true_false',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'message' => __('Bottom Affiliate Link in the "Quick View" popup modal.', 'ultimateazon2' ),
						'default_value' => 0,
					),
					array (
						'key' => 'field_58238ae8fd3ea',
						'label' => __('Disable Top Product Image in the "Quick View" popup modal.', 'ultimateazon2' ),
						'name' => 'disable_top_prod_img',
						'type' => 'true_false',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'message' => __('Check this to disable the Top Product Image in the "Quick View" popup modal.', 'ultimateazon2' ),
						'default_value' => 0,
					),
					array (
						'key' => 'field_58238b1ffd3eb',
						'label' => __('Disable the Affiliate Link on the Top Product Image in the "Quick View" popup modal.', 'ultimateazon2' ),
						'name' => 'disable_top_prod_img_link',
						'type' => 'true_false',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'message' => __('Check this to disable the Top Product Image Link in the "Quick View" popup modal.', 'ultimateazon2' ),
						'default_value' => 0,
					),
				),
				'location' => array (
					array (
						array (
							'param' => 'options_page',
							'operator' => '==',
							'value' => 'extension-prp-settings',
						),
					),
				),
				'menu_order' => 0,
				'position' => 'normal',
				'style' => 'default',
				'label_placement' => 'top',
				'instruction_placement' => 'label',
				'hide_on_screen' => '',
				'active' => 1,
				'description' => '',
			));


















			acf_add_local_field_group(array (
				'key' => 'group_57c5d9eeb7ae5',
				'title' => __('Product Review Settings', 'ultimateazon2' ),
				'fields' => array (
					array (
						'key' => 'field_57d6c721100e1',
						'label' => __('Review Product Specifications Type', 'ultimateazon2' ),
						'name' => 'review_specs_type',
						'type' => 'select',
						'instructions' => __('Choose whether you are pulling all of your data from Amazon automatically, or using your own custom product specifications.', 'ultimateazon2' ),
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'choices' => array (
							'custom-specs' => __('I am Setting up my own custom product specifications', 'ultimateazon2' ),
							'amazon-specs' => __('Pull all Data from Amazon', 'ultimateazon2' ),
						),
						'default_value' => array (
						),
						'allow_null' => 0,
						'multiple' => 0,
						'ui' => 0,
						'ajax' => 0,
						'return_format' => 'value',
						'placeholder' => '',
					),
					array (
						'key' => 'field_57c5da15bf817',
						'label' => __('Manual Affiliate Link', 'ultimateazon2' ),
						'name' => 'manual_affiliate_link',
						'type' => 'text',
						'instructions' => __('Add your affiliate link here if not using the Amazon API to pull your data. If you pull ALL of your data from the Amazon API, your affiliate link will be created automatically for you by Amazon.', 'ultimateazon2' ),
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_57d6c721100e1',
									'operator' => '==',
									'value' => 'custom-specs',
								),
							),
						),
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'maxlength' => '',
					),
					array (
						'key' => 'field_57f7b64e2ccad',
						'label' => __('Amazon Product ASIN', 'ultimateazon2' ),
						'name' => 'amazon_manual_product_asin',
						'type' => 'text',
						'instructions' => __('If you are pulling any Amazon product information in your custom product specifications, enter the Amazon Product ASIN here. If you are not pulling any data from Amazon, you can leave this blank. <a class="manual-search-amazon thickbox" href="#TB_inline?width=630&height=500&inlineId=galfram-amazon-lookup"><strong>Click here to Search Amazon</strong></a> for your product.', 'ultimateazon2' ),
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_57d6c721100e1',
									'operator' => '==',
									'value' => 'custom-specs',
								),
							),
						),
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'maxlength' => '',
					),
					array (
						'key' => 'field_57d6c661100e0',
						'label' => __('Editor\'s Rating', 'ultimateazon2' ),
						'name' => 'editor_rating',
						'type' => 'select',
						'instructions' => __('This is your own manually set product rating. Amazon API does not allow the display of the Amazon Star Rating or number of reviews any more.', 'ultimateazon2' ),
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_57d6c721100e1',
									'operator' => '==',
									'value' => 'amazon-specs',
								),
							),
						),
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'choices' => array (
							'no-rating' => 'No rating',
							'0.0' => '0.0',
							'0.1' => '0.1',
							'0.2' => '0.2',
							'0.3' => '0.3',
							'0.4' => '0.4',
							'0.5' => '0.5',
							'0.6' => '0.6',
							'0.7' => '0.7',
							'0.8' => '0.8',
							'0.9' => '0.9',
							'1.0' => '1.0',
							'1.1' => '1.1',
							'1.2' => '1.2',
							'1.3' => '1.3',
							'1.4' => '1.4',
							'1.5' => '1.5',
							'1.6' => '1.6',
							'1.7' => '1.7',
							'1.8' => '1.8',
							'1.9' => '1.9',
							'2.0' => '2.0',
							'2.1' => '2.1',
							'2.2' => '2.2',
							'2.3' => '2.3',
							'2.4' => '2.4',
							'2.5' => '2.5',
							'2.6' => '2.6',
							'2.7' => '2.7',
							'2.8' => '2.8',
							'2.9' => '2.9',
							'3.0' => '3.0',
							'3.1' => '3.1',
							'3.2' => '3.2',
							'3.3' => '3.3',
							'3.4' => '3.4',
							'3.5' => '3.5',
							'3.6' => '3.6',
							'3.7' => '3.7',
							'3.8' => '3.8',
							'3.9' => '3.9',
							'4.0' => '4.0',
							'4.1' => '4.1',
							'4.2' => '4.2',
							'4.3' => '4.3',
							'4.4' => '4.4',
							'4.5' => '4.5',
							'4.6' => '4.6',
							'4.7' => '4.7',
							'4.8' => '4.8',
							'4.9' => '4.9',
							'5.0' => '5.0',
						),
						'default_value' => array (
						),
						'allow_null' => 0,
						'multiple' => 0,
						'ui' => 0,
						'ajax' => 0,
						'return_format' => 'value',
						'placeholder' => '',
					),
					array (
						'key' => 'field_57d6c7da100e3',
						'label' => __('Open all affiliate links in a new window', 'ultimateazon2' ),
						'name' => 'open_aff_links_blank',
						'type' => 'true_false',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'message' => __('(This will override the global setting for this post, and only affects the automatically generated affiliate links.)', 'ultimateazon2' ),
						'default_value' => 0,
					),
					array (
						'key' => 'field_57d6c812100e4',
						'label' => __('Nofollow all Affiliate links', 'ultimateazon2' ),
						'name' => 'nofollow_aff_links',
						'type' => 'true_false',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'message' => __('(This will override the global setting for this post, and only affects the automatically generated links.)', 'ultimateazon2' ),
						'default_value' => 0,
					),
					array (
						'key' => 'field_57d6c8d67122a',
						'label' => __('Link or Unlink Top Featured Image', 'ultimateazon2' ),
						'name' => 'do_not_link_top_large_image',
						'type' => 'true_false',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'message' => __('Check this box to override the global setting.', 'ultimateazon2' ),
						'default_value' => 0,
					),
					array (
						'key' => 'field_57d6cbc1fc0d1',
						'label' => __('Hide or Show Sidebar Product Link Button', 'ultimateazon2' ),
						'name' => 'hide_sidebar_product_link_button',
						'type' => 'true_false',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'message' => __('Check this box to override the global setting.', 'ultimateazon2' ),
						'default_value' => 0,
					),
					array (
						'key' => 'field_582412661ef49',
						'label' => __('Sidebar Product Link Button Text', 'ultimateazon2' ),
						'name' => 'aff_button_sidebar_text',
						'type' => 'text',
						'instructions' => __('This text will be displayed instead of the global setting if filled out. Leave empty to use the global setting.', 'ultimateazon2' ),
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'maxlength' => '',
					),
					array (
						'key' => 'field_57d6cce1a169d',
						'label' => __('Hide or Show Top Link Bar', 'ultimateazon2' ),
						'name' => 'hide_top_link_bar',
						'type' => 'true_false',
						'instructions' => __('(Underneath Featured Images)', 'ultimateazon2' ),
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '50',
							'class' => '',
							'id' => '',
						),
						'message' => __('Check this box to override the global setting.', 'ultimateazon2' ),
						'default_value' => 0,
					),
					array (
						'key' => 'field_57d71b8a7ea8a',
						'label' => __('Hide or Show Bottom Link Bar', 'ultimateazon2' ),
						'name' => 'hide_bottom_link_bar',
						'type' => 'true_false',
						'instructions' => __('(Underneath the Full Review Body)', 'ultimateazon2' ),
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '50',
							'class' => '',
							'id' => '',
						),
						'message' => __('Check this box to override the global setting.', 'ultimateazon2' ),
						'default_value' => 0,
					),
					array (
						'key' => 'field_57d719831dedb',
						'label' => __('Top Link Bar Text', 'ultimateazon2' ),
						'name' => 'top_link_bar_text',
						'type' => 'text',
						'instructions' => __('This will override the global setting.', 'ultimateazon2' ),
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '50',
							'class' => 'djn-clear-all',
							'id' => '',
						),
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'maxlength' => '',
					),
					array (
						'key' => 'field_57d719a31dedc',
						'label' => __('Bottom Link Bar Text', 'ultimateazon2' ),
						'name' => 'bottom_link_bar_text',
						'type' => 'text',
						'instructions' => __('This will override the global setting.', 'ultimateazon2' ),
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '50',
							'class' => '',
							'id' => '',
						),
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'maxlength' => '',
					),
				),
				'location' => array (
					array (
						array (
							'param' => 'post_type',
							'operator' => '==',
							'value' => 'galfram_review',
						),
					),
				),
				'menu_order' => 0,
				'position' => 'acf_after_title',
				'style' => 'default',
				'label_placement' => 'top',
				'instruction_placement' => 'label',
				'hide_on_screen' => '',
				'active' => 1,
				'description' => '',
			));

















			acf_add_local_field_group(array (
				'key' => 'group_57c5d61c8e846',
				'title' => __('Additional Images', 'ultimateazon2' ),
				'fields' => array (
					array (
						'key' => 'field_iuyb7tb7tb_ama_imgs',
						'label' => __('Pull All Images from Amazon', 'ultimateazon2' ),
						'name' => 'pull_images_from_amazon',
						'type' => 'true_false',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'message' => __('Check here to pull all images from the Amazon API.', 'ultimateazon2' ),
						'default_value' => 0,
					),
					array (
						'key' => 'field_iybu7tv68tb7n78y8',
						'label' => __('Important Message', 'ultimateazon2' ),
						'name' => '',
						'type' => 'message',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_iuyb7tb7tb_ama_imgs',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '',
							'class' => 'djn-section-heading',
							'id' => '',
						),
						'message' => __('To pull all images from Amazon, make sure you have your Amazon API connection set up, and you have choosen the Amazon product ASIN above.', 'ultimateazon2'),
						'new_lines' => 'wpautop',
						'esc_html' => 0,
					),
					array (
						'key' => 'field_57c5d6394c85d',
						'label' => __('Additional Images', 'ultimateazon2' ),
						'name' => 'additional_images',
						'type' => 'repeater',
						'instructions' => __('Add additional images here that will be displayed as thumbnails underneath your Featured Image.', 'ultimateazon2' ),
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_iuyb7tb7tb_ama_imgs',
									'operator' => '!=',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'collapsed' => 'field_57c5d6654c85e',
						'min' => '',
						'max' => '',
						'layout' => 'row',
						'button_label' => __('Add Image', 'ultimateazon2' ),
						'sub_fields' => array (
							array (
								'key' => 'field_57c5d6654c85e',
								'label' => __('Image', 'ultimateazon2' ),
								'name' => 'image',
								'type' => 'image',
								'instructions' => '',
								'required' => 0,
								'conditional_logic' => 0,
								'wrapper' => array (
									'width' => '',
									'class' => '',
									'id' => '',
								),
								'return_format' => 'id',
								'preview_size' => 'medium',
								'library' => 'all',
								'min_width' => '',
								'min_height' => '',
								'min_size' => '',
								'max_width' => '',
								'max_height' => '',
								'max_size' => '',
								'mime_types' => '',
							),
							array (
								'key' => 'field_57c5dd77adb50',
								'label' => __('Alt Tag', 'ultimateazon2' ),
								'name' => 'alt_tag',
								'type' => 'text',
								'instructions' => '',
								'required' => 0,
								'conditional_logic' => 0,
								'wrapper' => array (
									'width' => '',
									'class' => '',
									'id' => '',
								),
								'default_value' => '',
								'placeholder' => '',
								'prepend' => '',
								'append' => '',
								'maxlength' => '',
							),
						),
					),
				),
				'location' => array (
					array (
						array (
							'param' => 'post_type',
							'operator' => '==',
							'value' => 'galfram_review',
						),
					),
				),
				'menu_order' => 100,
				'position' => 'normal',
				'style' => 'default',
				'label_placement' => 'top',
				'instruction_placement' => 'label',
				'hide_on_screen' => '',
				'active' => 1,
				'description' => '',
			));

















			acf_add_local_field_group(array (
				'key' => 'group_57c1fca7e9e86',
				'title' => __('Custom Specifications Group', 'ultimateazon2' ),
				'fields' => array (
					array (
						'key' => 'field_57c1fcf9ce98a',
						'label' => __('Choose Custom Product Specifications Group', 'ultimateazon2' ),
						'name' => 'prod_specs_group',
						'type' => 'select',
						'instructions' => __('Choose which product specifications group you would like to use for this review. <strong style="color: red;">You must save your post to make the specifications group available on this edit screen</strong>.', 'ultimateazon2' ),
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'choices' => array (
							'pull-from-amazon' => __('Pull All Data from Amazon', 'ultimateazon2' ),
						),
						'default_value' => array (
						),
						'allow_null' => 0,
						'multiple' => 0,
						'ui' => 0,
						'ajax' => 0,
						'return_format' => 'value',
						'placeholder' => '',
					),
				),
				'location' => array (
					array (
						array (
							'param' => 'post_type',
							'operator' => '==',
							'value' => 'galfram_review',
						),
					),
				),
				'menu_order' => 0,
				'position' => 'side',
				'style' => 'default',
				'label_placement' => 'top',
				'instruction_placement' => 'label',
				'hide_on_screen' => '',
				'active' => 1,
				'description' => '',
			));















			acf_add_local_field_group(array (
				'key' => 'group_57d2bfe8688f0',
				'title' => 'Intro Content',
				'fields' => array (
					array (
						'key' => 'field_57d2c02a3dd21',
						'label' => __('Enable Intro Content', 'ultimateazon2' ),
						'name' => 'enable_intro_content',
						'type' => 'true_false',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'message' => __('Check here to enable intro content that gets displayed above the images in your review.', 'ultimateazon2' ),
						'default_value' => 0,
					),
					array (
						'key' => 'field_57d2c0023dd20',
						'label' => __('Intro Content', 'ultimateazon2' ),
						'name' => 'intro_content',
						'type' => 'wysiwyg',
						'instructions' => __('This will be displayed above the images in your review. This field is optional.', 'ultimateazon2' ),
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_57d2c02a3dd21',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'default_value' => '',
						'tabs' => 'all',
						'toolbar' => 'full',
						'media_upload' => 1,
					),
				),
				'location' => array (
					array (
						array (
							'param' => 'post_type',
							'operator' => '==',
							'value' => 'galfram_review',
						),
					),
				),
				'menu_order' => 0,
				'position' => 'acf_after_title',
				'style' => 'default',
				'label_placement' => 'top',
				'instruction_placement' => 'label',
				'hide_on_screen' => '',
				'active' => 1,
				'description' => '',
			));
















			/*********************************
			* Add Stylizer Fields ************
			*********************************/

			//$stylizer_active = galfram_extension_checker('galaxyframework-child-stylizer');

			//if ( $stylizer_active ) :


				acf_add_local_field_group(array (
					'key' => 'group_5824a73d312fa',
					'title' => __('Product Review Pro Styles', 'ultimateazon2' ),
					'fields' => array (
						array (
							'key' => 'field_58262a3d4d9be',
							'label' => __('Archive Page - Product Review Pro', 'ultimateazon2' ),
							'name' => '',
							'type' => 'message',
							'instructions' => '',
							'required' => 0,
							'conditional_logic' => 0,
							'wrapper' => array (
								'width' => '',
								'class' => 'djn-section-heading',
								'id' => '',
							),
							'message' => '',
							'new_lines' => 'wpautop',
							'esc_html' => 0,
						),
						array (
							'key' => 'field_5825f0af157d1',
							'label' => __('Quick View Slideup Bar Background Color', 'ultimateazon2' ),
							'name' => 'quickview_bar_bg_color',
							'type' => 'color_picker',
							'instructions' => '',
							'required' => 0,
							'conditional_logic' => 0,
							'wrapper' => array (
								'width' => '50',
								'class' => '',
								'id' => '',
							),
							'default_value' => '#3abb22',
						),
						array (
							'key' => 'field_5825f0cc157d2',
							'label' => __('Quick View Slideup Bar Text Color', 'ultimateazon2' ),
							'name' => 'quickview_bar_text_color',
							'type' => 'color_picker',
							'instructions' => '',
							'required' => 0,
							'conditional_logic' => 0,
							'wrapper' => array (
								'width' => '50',
								'class' => '',
								'id' => '',
							),
							'default_value' => '',
						),
						array (
							'key' => 'field_58266de3ebd2d',
							'label' => __('Quick View Slideup Bar Background Color Hover', 'ultimateazon2' ),
							'name' => 'quickview_bar_bg_color_hover',
							'type' => 'color_picker',
							'instructions' => '',
							'required' => 0,
							'conditional_logic' => 0,
							'wrapper' => array (
								'width' => '50',
								'class' => 'djn-clear-all',
								'id' => '',
							),
							'default_value' => '#3abb22',
						),
						array (
							'key' => 'field_58266df4ebd2e',
							'label' => __('Quick View Slideup Bar Text Color Hover', 'ultimateazon2' ),
							'name' => 'quickview_bar_text_color_hover',
							'type' => 'color_picker',
							'instructions' => '',
							'required' => 0,
							'conditional_logic' => 0,
							'wrapper' => array (
								'width' => '50',
								'class' => '',
								'id' => '',
							),
							'default_value' => '',
						),
						array (
							'key' => 'field_58262a634d9bf',
							'label' => __('Quick View Modal - Product Review Pro Archive Page', 'ultimateazon2' ),
							'name' => '',
							'type' => 'message',
							'instructions' => '',
							'required' => 0,
							'conditional_logic' => 0,
							'wrapper' => array (
								'width' => '',
								'class' => 'djn-section-heading',
								'id' => '',
							),
							'message' => '',
							'new_lines' => 'wpautop',
							'esc_html' => 0,
						),
						array (
							'key' => 'field_582626fcc8eec',
							'label' => __('Quick View Modal - Product Title Color', 'ultimateazon2' ),
							'name' => 'quickview_modal_prod_title_color',
							'type' => 'color_picker',
							'instructions' => '',
							'required' => 0,
							'conditional_logic' => 0,
							'wrapper' => array (
								'width' => '50',
								'class' => 'djn-clear-all',
								'id' => '',
							),
							'default_value' => '#0a0a0a',
						),
						array (
							'key' => 'field_58263400c54a8',
							'label' => __('Quick View Modal - Product Title Font Size', 'ultimateazon2' ),
							'name' => 'quickview_modal_prod_title_font_size',
							'type' => 'number',
							'instructions' => '',
							'required' => 0,
							'conditional_logic' => 0,
							'wrapper' => array (
								'width' => '50',
								'class' => '',
								'id' => '',
							),
							'default_value' => 24,
							'placeholder' => '',
							'prepend' => '',
							'append' => 'px',
							'min' => '',
							'max' => '',
							'step' => '',
						),
						array (
							'key' => 'field_58262789069f9',
							'label' => __('Quick View Modal - Affiliate Button Background Color', 'ultimateazon2' ),
							'name' => 'quickview_modal_aff_btn_bg',
							'type' => 'color_picker',
							'instructions' => '',
							'required' => 0,
							'conditional_logic' => 0,
							'wrapper' => array (
								'width' => '50',
								'class' => 'djn-clear-all',
								'id' => '',
							),
							'default_value' => '#3abb22',
						),
						array (
							'key' => 'field_582627d8069fa',
							'label' => __('Quick View Modal - Affiliate Button Text Color', 'ultimateazon2' ),
							'name' => 'quickview_modal_aff_btn_txt',
							'type' => 'color_picker',
							'instructions' => '',
							'required' => 0,
							'conditional_logic' => 0,
							'wrapper' => array (
								'width' => '50',
								'class' => '',
								'id' => '',
							),
							'default_value' => '#FFFFFF',
						),
						array (
							'key' => 'field_58262884069fd',
							'label' => __('Quick View Modal - Affiliate Button Background Color Hover', 'ultimateazon2' ),
							'name' => 'quickview_modal_aff_btn_bg_hover',
							'type' => 'color_picker',
							'instructions' => '',
							'required' => 0,
							'conditional_logic' => 0,
							'wrapper' => array (
								'width' => '50',
								'class' => 'djn-clear-all',
								'id' => '',
							),
							'default_value' => '#3abb22',
						),
						array (
							'key' => 'field_58262893069fe',
							'label' => __('Quick View Modal - Affiliate Button Text Color Hover', 'ultimateazon2' ),
							'name' => 'quickview_modal_aff_btn_txt_hover',
							'type' => 'color_picker',
							'instructions' => '',
							'required' => 0,
							'conditional_logic' => 0,
							'wrapper' => array (
								'width' => '50',
								'class' => '',
								'id' => '',
							),
							'default_value' => '#FFFFFF',
						),
						array (
							'key' => 'field_58262830069fb',
							'label' => __('Quick View Modal - Full Review Button Background Color', 'ultimateazon2' ),
							'name' => 'quickview_modal_full_btn_bg',
							'type' => 'color_picker',
							'instructions' => '',
							'required' => 0,
							'conditional_logic' => 0,
							'wrapper' => array (
								'width' => '50',
								'class' => '',
								'id' => '',
							),
							'default_value' => '#f1f1f1',
						),
						array (
							'key' => 'field_5826295806a00',
							'label' => __('Quick View Modal - Full Review Button Text Color', 'ultimateazon2' ),
							'name' => 'quickview_modal_full_btn_txt',
							'type' => 'color_picker',
							'instructions' => '',
							'required' => 0,
							'conditional_logic' => 0,
							'wrapper' => array (
								'width' => '50',
								'class' => '',
								'id' => '',
							),
							'default_value' => '#aeaeae',
						),
						array (
							'key' => 'field_582628bf069ff',
							'label' => __('Quick View Modal - Full Review Button Background Color Hover', 'ultimateazon2' ),
							'name' => 'quickview_modal_full_btn_bg_hover',
							'type' => 'color_picker',
							'instructions' => '',
							'required' => 0,
							'conditional_logic' => 0,
							'wrapper' => array (
								'width' => '50',
								'class' => 'djn-clear-all',
								'id' => '',
							),
							'default_value' => '#e9e9e9',
						),
						array (
							'key' => 'field_5826297406a01',
							'label' => __('Quick View Modal - Full Review Button Text Color Hover', 'ultimateazon2' ),
							'name' => 'quickview_modal_full_btn_txt_hover',
							'type' => 'color_picker',
							'instructions' => '',
							'required' => 0,
							'conditional_logic' => 0,
							'wrapper' => array (
								'width' => '50',
								'class' => '',
								'id' => '',
							),
							'default_value' => '#919191',
						),
						array (
							'key' => 'field_58262f5268f3c',
							'label' => __('Single Review - Product Review Pro', 'ultimateazon2' ),
							'name' => '',
							'type' => 'message',
							'instructions' => '',
							'required' => 0,
							'conditional_logic' => 0,
							'wrapper' => array (
								'width' => '',
								'class' => 'djn-section-heading',
								'id' => '',
							),
							'message' => '',
							'new_lines' => 'wpautop',
							'esc_html' => 0,
						),
						array (
							'key' => 'field_58262f7468f3d',
							'label' => __('Product Specification Label Color', 'ultimateazon2' ),
							'name' => 'product_specs_label_color',
							'type' => 'color_picker',
							'instructions' => __('(This also affects the Quick View modal on the archives page)', 'ultimateazon2' ),
							'required' => 0,
							'conditional_logic' => 0,
							'wrapper' => array (
								'width' => '50',
								'class' => '',
								'id' => '',
							),
							'default_value' => '#aeaeae',
						),
						array (
							'key' => 'field_58263304809a5',
							'label' => __('Product Specification Label Font Size', 'ultimateazon2' ),
							'name' => 'product_specification_label_font_size',
							'type' => 'number',
							'instructions' => __('(This also affects the Quick View modal on the archives page)', 'ultimateazon2' ),
							'required' => 0,
							'conditional_logic' => 0,
							'wrapper' => array (
								'width' => '50',
								'class' => '',
								'id' => '',
							),
							'default_value' => 16,
							'placeholder' => '',
							'prepend' => '',
							'append' => 'px',
							'min' => '',
							'max' => '',
							'step' => '',
						),
						array (
							'key' => 'field_5826351c0c4dd',
							'label' => __('Product Specification Value - Font Size', 'ultimateazon2' ),
							'name' => 'product_specification_value_font_size',
							'type' => 'number',
							'instructions' => __('(This also affects the Quick View modal on the archives page)', 'ultimateazon2' ),
							'required' => 0,
							'conditional_logic' => 0,
							'wrapper' => array (
								'width' => '33',
								'class' => 'djn-clear-all',
								'id' => '',
							),
							'default_value' => 16,
							'placeholder' => '',
							'prepend' => '',
							'append' => 'px',
							'min' => '',
							'max' => '',
							'step' => '',
						),
						array (
							'key' => 'field_582635520c4de',
							'label' => __('Product Specification Value - Line Height', 'ultimateazon2' ),
							'name' => 'product_specification_value_line_height',
							'type' => 'number',
							'instructions' => __('(This also affects the Quick View modal on the archives page)', 'ultimateazon2' ),
							'required' => 0,
							'conditional_logic' => 0,
							'wrapper' => array (
								'width' => '33',
								'class' => '',
								'id' => '',
							),
							'default_value' => 16,
							'placeholder' => '',
							'prepend' => '',
							'append' => 'px',
							'min' => '',
							'max' => '',
							'step' => '',
						),
						array (
							'key' => 'field_5826357567af5',
							'label' => __('Product Specification Value - Font Color', 'ultimateazon2' ),
							'name' => 'product_spec_value_font_color',
							'type' => 'color_picker',
							'instructions' => __('(This also affects the Quick View modal on the archives page)', 'ultimateazon2' ),
							'required' => 0,
							'conditional_logic' => 0,
							'wrapper' => array (
								'width' => '34',
								'class' => '',
								'id' => '',
							),
							'default_value' => '#aeaeae',
						),
						array (
							'key' => 'field_582636605dbcd',
							'label' => __('Link Bar Background Color', 'ultimateazon2' ),
							'name' => 'link_bar_background_color',
							'type' => 'color_picker',
							'instructions' => '',
							'required' => 0,
							'conditional_logic' => 0,
							'wrapper' => array (
								'width' => '50',
								'class' => '',
								'id' => '',
							),
							'default_value' => '#3abb22',
						),
						array (
							'key' => 'field_5826367b5dbce',
							'label' => __('Link Bar Text Color', 'ultimateazon2' ),
							'name' => 'link_bar_text_color',
							'type' => 'color_picker',
							'instructions' => '',
							'required' => 0,
							'conditional_logic' => 0,
							'wrapper' => array (
								'width' => '50',
								'class' => '',
								'id' => '',
							),
							'default_value' => '#3abb22',
						),
						array (
							'key' => 'field_582636955dbcf',
							'label' => __('Link Bar Background Color Hover', 'ultimateazon2' ),
							'name' => 'link_bar_background_color_hover',
							'type' => 'color_picker',
							'instructions' => '',
							'required' => 0,
							'conditional_logic' => 0,
							'wrapper' => array (
								'width' => '50',
								'class' => 'djn-clear-all',
								'id' => '',
							),
							'default_value' => '#3abb22',
						),
						array (
							'key' => 'field_582636a15dbd0',
							'label' => __('Link Bar Text Color Hover', 'ultimateazon2' ),
							'name' => 'link_bar_text_color_hover',
							'type' => 'color_picker',
							'instructions' => '',
							'required' => 0,
							'conditional_logic' => 0,
							'wrapper' => array (
								'width' => '50',
								'class' => '',
								'id' => '',
							),
							'default_value' => '#3abb22',
						),
						array (
							'key' => 'field_58267e70a2d4d',
							'label' => __('Sidebar Affiliate Button Background Color', 'ultimateazon2' ),
							'name' => 'sidebar_aff_btn_bg_color',
							'type' => 'color_picker',
							'instructions' => '',
							'required' => 0,
							'conditional_logic' => 0,
							'wrapper' => array (
								'width' => '50',
								'class' => 'djn-clear-all',
								'id' => '',
							),
							'default_value' => '#3abb22',
						),
						array (
							'key' => 'field_58267e8ea2d4e',
							'label' => __('Sidebar Affiliate Button Text Color', 'ultimateazon2' ),
							'name' => 'sidebar_aff_btn_text_color',
							'type' => 'color_picker',
							'instructions' => '',
							'required' => 0,
							'conditional_logic' => 0,
							'wrapper' => array (
								'width' => '50',
								'class' => '',
								'id' => '',
							),
							'default_value' => '#ffffff',
						),
						array (
							'key' => 'field_58267eaba2d4f',
							'label' => __('Sidebar Affiliate Button Background Color Hover', 'ultimateazon2' ),
							'name' => 'sidebar_aff_btn_bg_color_hover',
							'type' => 'color_picker',
							'instructions' => '',
							'required' => 0,
							'conditional_logic' => 0,
							'wrapper' => array (
								'width' => '50',
								'class' => 'djn-clear-all',
								'id' => '',
							),
							'default_value' => '#3abb22',
						),
						array (
							'key' => 'field_58267ee9a2d51',
							'label' => __('Sidebar Affiliate Button Text Color Hover', 'ultimateazon2' ),
							'name' => 'sidebar_aff_btn_text_color_hover',
							'type' => 'color_picker',
							'instructions' => '',
							'required' => 0,
							'conditional_logic' => 0,
							'wrapper' => array (
								'width' => '50',
								'class' => '',
								'id' => '',
							),
							'default_value' => '#ffffff',
						),
					),
					'location' => array (
						array (
							array (
								'param' => 'options_page',
								'operator' => '==',
								'value' => 'theme-styles',
							),
						),
					),
					'menu_order' => 180,
					'position' => 'normal',
					'style' => 'default',
					'label_placement' => 'top',
					'instruction_placement' => 'label',
					'hide_on_screen' => '',
					'active' => 1,
					'description' => '',
				));


			//endif;


		endif;


	}







	/**
	 * Include the Plugin Updater
	 *
	 * @since    1.0.0
	 */
	function galfram_productreviewproshort_license_management() {

		// the ID in the box must be the license option name: galfram_CHILDTHEMEOREXTENSIONNAME_license - This must also match the ID of the license text input below ?>
	    <div class="gf-option-box" id="galfram_plugin_prp_license">

	        <h3><?php _e('Galaxy Framework Extension - Product Review Pro License Key', 'ultimateazon2'); ?></h3>

	        <div class="gf-option-box-inner">

	            <?php
	            $license    = trim( get_option('galfram_plugin_prp_license') );
	            $status     = trim( get_option('galfram_plugin_prp_license_status') );

	            if($status=='deactivated'):
	            echo '<p><span class="license-message galaxyframework-license-deactivated">'.__('The license key you entered has been', 'ultimateazon2').' '.$status.' '.__('on this website', 'ultimateazon2').'</span> <img class="license-loading-gif" src="/wp-admin/images/loading.gif" alt="loading" /></p>';
	            //endif;

	            elseif($status=='invalid'):
	            echo '<p><span class="license-message galaxyframework-license-invalid">'.__('The license key you entered is', 'ultimateazon2').' '.$status.'. '.__('Please try again', 'ultimateazon2').'</span> <img class="license-loading-gif" src="/wp-admin/images/loading.gif" alt="loading" /></p>';
	            //endif;


	            elseif($status=='valid'):
	            echo '<p><span class="license-message galaxyframework-license-valid">'.__('Your license is active', 'ultimateazon2').'</span> <img class="license-loading-gif" src="/wp-admin/images/loading.gif" alt="loading" /></p>';

	       		else:
	            echo '<p><span class="license-message galaxyframework-license-deactivated">'.__('Please activate your license for updates and support', 'ultimateazon2').'</span> <img class="license-loading-gif" src="/wp-admin/images/loading.gif" alt="loading" /></p>';

	            endif;
	            ?>

	            <div class="license-form">

	                <?php // the ID & name attributes must be the option name stored in the DB. It should follow this format:   galfram_CHILDorEXTENSIONTITLE_license ?>
	                <input id="galfram_plugin_prp_license" type="text" class="regular-text incomegalaxy-license" name="galfram_plugin_prp_license" value="<?php echo $license; ?>" />

	                <?php // the ID and name attributes must be "incomegalaxy_product" and the value must be the name of the product in the IncomeGalaxy.com store. The input type must be "hidden" ?>
	                <input id="incomegalaxy_product" type="hidden" class="incomegalaxy-product" name="incomegalaxy_product" value="Product Review Pro" />

	                <?php if ( $license != '' && $status == 'valid' ) : ?>

	                    <?php // The deactivate button must have  incomegalaxy-deactivate  as a class ?>
	                    <?php wp_nonce_field( 'edd_sample_nonce', 'edd_sample_nonce' ); ?>
	                        <input type="button" class="button-secondary incomegalaxy-deactivate" name="incomegalaxydeactivate" value="<?php _e('Deactivate', 'ultimateazon2'); ?>"/>

	                <?php else: ?>

	                    <?php // The activate button must have  incomegalaxy-activate  as a class ?>
	                    <?php wp_nonce_field( 'edd_sample_nonce', 'edd_sample_nonce' ); ?>
	                        <input type="button" class="button-secondary incomegalaxy-activate" name="incomegalaxyactivate" value="<?php _e('Activate', 'ultimateazon2'); ?>"/>

	                <?php endif; ?>

	            </div>



	        </div>

	    </div>

    <?php }






    /**
	 * Add custom image sizes
	 *
	 * @since    1.0.0
	 */
	function gf_ext_prp_add_image_sizes() {


		add_image_size( 'review-additional-image-thumb', '75', '75', false );

		
	}
















	/**
	 * Save user generated product specs array to option
	 *
	 * @since    1.0.0
	 */
	public function gf_ext_prp_acf_save_specs( ) {


		if ( get_current_screen()->id != 'ultimate-azon_page_extension-prp-settings' ) {
			return;
		}

		// bail early if no ACF data
	    if( empty($_POST['acf']) ) {
	        return;
	    }

		// array of field values
		$fields = $_POST['acf'];
		$user_specs = $fields['field_57c068c8622b5'];
		update_option( 'galfram_ext_prp_all_userspecs_groups', $user_specs );

	}


















	/**
	 * Populate Product Review Specs select box with choices created in PRP settings
	 *
	 * @since    1.0.0
	 */
	function gf_ext_prp_add_spec_groups( $field ) {

		// if ( get_post_type() != 'galfram_review' ) {
		// 	return;
		// }

		// get user saved spefs fields array
		$user_specs_groups = get_option( 'galfram_ext_prp_all_userspecs_groups' );

		//echo '<pre>'.print_r($user_specs_groups,1).'</pre>';

		// set empty choices
    	$field['choices'] = array();
    	$choices = array();

    	$choices['pull-from-amazon'] = __('Pull All Data from Amazon', 'ultimateazon2' );

    	if ( $user_specs_groups ) {
    		foreach ( $user_specs_groups as $group ) {

				$option_val = $group['field_57c35f7e8031b'];
				$option_txt = $group['field_57c068df622b6'];

				$choices[$option_val] = $option_txt;

			}
    	}

		// echo '<pre>'.print_r($choices,1).'</pre>';
		// die();

		// remove any unwanted white space
	    $choices = array_map('trim', $choices);

	    // loop through array and add to field 'choices'
	    if( is_array($choices) ) {
	        foreach( $choices as $value => $choice ) {
	            $field['choices'][ $value ] = $choice;
	        }
	    }
	    
	    // return the field
	    return $field;

	}






	











	/**
	 * add the user specs fields to the review post
	 *
	 * @since    1.0.0
	 */
	public function galfram_ext_prp_add_specs_fields() {

		if ( !isset( $_GET ) || !isset( $_POST ) ) {
			return;
		}


		if ( isset( $_GET['post'] ) ) {
			$post_id = $_GET['post'];
		}
		elseif ( isset( $_POST['post_ID'] ) ) {
			$post_id = $_POST['post_ID'];
		}
		else {
			return;
		}
		

		if ( get_post_type($post_id) != 'galfram_review' ) {
			return;
		}

		$specs_group_id = get_post_meta($post_id, 'prod_specs_group', true);
		$user_specs_groups = get_option('galfram_ext_prp_all_userspecs_groups');

		//echo '<pre>'.print_r($user_specs_groups,1).'</pre>';

		$current_chosen_spec_group = array();

		if ( $user_specs_groups ) {
			foreach ( $user_specs_groups as $user_spec_group ) {
				if ( $user_spec_group['field_57c35f7e8031b'] == $specs_group_id ) {
					$current_chosen_spec_group = $user_spec_group;
				}
			}
		}



		if ( isset($current_chosen_spec_group['field_57c068f1622b7']) && $current_chosen_spec_group['field_57c068f1622b7'] == 'create-my-own' ) {

			// add our new product specifications field group
			if( function_exists('acf_add_local_field_group') ) {

				acf_add_local_field_group(array(
					'key' => 'review_custom_specs',
					'title' => __('Custom Product Specifications', 'ultimateazon2' ),
					'fields' => array (),
					'location' => array (
						array (
							array (
								'param' => 'post_type',
								'operator' => '==',
								'value' => 'galfram_review',
							),
						),
					),
				));

				$current_chosen_spec_group_fields = $current_chosen_spec_group['field_57c06aa9622b9'];

				foreach ( $current_chosen_spec_group_fields as $field ) {

					//print_r($field['field_57c06afc622ba']);

					$spec_type = $field['field_57c06b19622bb'];
					$spec_name = $field['field_57c06afc622ba'];
					$spec_id   = $field['field_57c6bfb62908a'];

					switch ( $spec_type ) {

						case 'text' :

							acf_add_local_field(array (
								'key' => $specs_group_id.'_'.$spec_id,
								'label' => $spec_name,
								'name' => $specs_group_id.'_'.$spec_id,
								'type' => 'text',
								'instructions' => '',
								'required' => 0,
								'conditional_logic' => 0,
								'wrapper' => array (
									'width' => '',
									'class' => '',
									'id' => '',
								),
								'default_value' => '',
								'placeholder' => '',
								'prepend' => '',
								'append' => '',
								'maxlength' => '',
								'parent' => 'review_custom_specs',
							));

							break;

						case 'p-text' :

							acf_add_local_field(array (
								'key' => $specs_group_id.'_'.$spec_id,
								'label' => $spec_name,
								'name' => $specs_group_id.'_'.$spec_id,
								'type' => 'textarea',
								'instructions' => '',
								'required' => 0,
								'conditional_logic' => 0,
								'wrapper' => array (
									'width' => '',
									'class' => '',
									'id' => '',
								),
								'default_value' => '',
								'placeholder' => '',
								'maxlength' => '',
								'rows' => '',
								'new_lines' => 'wpautop',
								'parent' => 'review_custom_specs',
							));

							break;

						case 'star-rating' :

							acf_add_local_field(array (
								'key' => $specs_group_id.'_'.$spec_id,
								'label' => $spec_name,
								'name' => $specs_group_id.'_'.$spec_id,
								'type' => 'select',
								'instructions' => '',
								'required' => 0,
								'conditional_logic' => 0,
								'wrapper' => array (
									'width' => '',
									'class' => '',
									'id' => '',
								),
								'choices' => array (
									'0.0' => '0.0',
									'0.1' => '0.1',
									'0.2' => '0.2',
									'0.3' => '0.3',
									'0.4' => '0.4',
									'0.5' => '0.5',
									'0.6' => '0.6',
									'0.7' => '0.7',
									'0.8' => '0.8',
									'0.9' => '0.9',
									'1.0' => '1.0',
									'1.1' => '1.1',
									'1.2' => '1.2',
									'1.3' => '1.3',
									'1.4' => '1.4',
									'1.5' => '1.5',
									'1.6' => '1.6',
									'1.7' => '1.7',
									'1.8' => '1.8',
									'1.9' => '1.9',
									'2.0' => '2.0',
									'2.1' => '2.1',
									'2.2' => '2.2',
									'2.3' => '2.3',
									'2.4' => '2.4',
									'2.5' => '2.5',
									'2.6' => '2.6',
									'2.7' => '2.7',
									'2.8' => '2.8',
									'2.9' => '2.9',
									'3.0' => '3.0',
									'3.1' => '3.1',
									'3.2' => '3.2',
									'3.3' => '3.3',
									'3.4' => '3.4',
									'3.5' => '3.5',
									'3.6' => '3.6',
									'3.7' => '3.7',
									'3.8' => '3.8',
									'3.9' => '3.9',
									'4.0' => '4.0',
									'4.1' => '4.1',
									'4.2' => '4.2',
									'4.3' => '4.3',
									'4.4' => '4.4',
									'4.5' => '4.5',
									'4.6' => '4.6',
									'4.7' => '4.7',
									'4.8' => '4.8',
									'4.9' => '4.9',
									'5.0' => '5.0',
								),
								'default_value' => array (
								),
								'allow_null' => 0,
								'multiple' => 0,
								'ui' => 0,
								'ajax' => 0,
								'return_format' => 'value',
								'placeholder' => '',
								'parent' => 'review_custom_specs',
							));

							break;

						case 'yes-no' :

							acf_add_local_field(array (
								'key' => $specs_group_id.'_'.$spec_id,
								'label' => $spec_name,
								'name' => $specs_group_id.'_'.$spec_id,
								'type' => 'radio',
								'instructions' => '',
								'required' => 0,
								'conditional_logic' => 0,
								'wrapper' => array (
									'width' => '',
									'class' => '',
									'id' => '',
								),
								'choices' => array (
									'yes' => 'yes',
									'no' => 'No',
								),
								'allow_null' => 0,
								'other_choice' => 0,
								'save_other_choice' => 0,
								'default_value' => 'yes : Yes',
								'layout' => 'horizontal',
								'return_format' => 'value',
								'parent' => 'review_custom_specs',
							));

							break;

						case 'image' :

							acf_add_local_field(array (
								'key' => $specs_group_id.'_'.$spec_id,
								'label' => $spec_name,
								'name' => $specs_group_id.'_'.$spec_id,
								'type' => 'image',
								'instructions' => '',
								'required' => 0,
								'conditional_logic' => 0,
								'wrapper' => array (
									'width' => '',
									'class' => '',
									'id' => '',
								),
								'return_format' => 'id',
								'preview_size' => 'medium',
								'library' => 'all',
								'min_width' => '',
								'min_height' => '',
								'min_size' => '',
								'max_width' => '',
								'max_height' => '',
								'max_size' => '',
								'mime_types' => '',
								'parent' => 'review_custom_specs',
							));

							break;


						case 'price' :

							acf_add_local_field(array (
								'key' => $specs_group_id.'_'.$spec_id.'_listprice',
								'label' => __('List Price', 'ultimateazon2' ),
								'name' => $specs_group_id.'_'.$spec_id.'_listprice',
								'type' => 'text',
								'instructions' => '',
								'required' => 0,
								'conditional_logic' => 0,
								'wrapper' => array (
									'width' => 50,
									'class' => '',
									'id' => '',
								),
								'default_value' => '',
								'placeholder' => '',
								'prepend' => '',
								'append' => '',
								'maxlength' => '',
								'parent' => 'review_custom_specs',
							));

							acf_add_local_field(array (
								'key' => $specs_group_id.'_'.$spec_id.'_saleprice',
								'label' => __('Sale Price', 'ultimateazon2' ),
								'name' => $specs_group_id.'_'.$spec_id.'_saleprice',
								'type' => 'text',
								'instructions' => '',
								'required' => 0,
								'conditional_logic' => 0,
								'wrapper' => array (
									'width' => 50,
									'class' => '',
									'id' => '',
								),
								'default_value' => '',
								'placeholder' => '',
								'prepend' => '',
								'append' => '',
								'maxlength' => '',
								'parent' => 'review_custom_specs',
							));

							acf_add_local_field(array (
								'key' => $specs_group_id.'_'.$spec_id.'_percentagediscounted',
								'label' => __('Percentage Saved', 'ultimateazon2' ),
								'name' => $specs_group_id.'_'.$spec_id.'_percentagediscounted',
								'type' => 'text',
								'instructions' => '',
								'required' => 0,
								'conditional_logic' => 0,
								'wrapper' => array (
									'width' => 50,
									'class' => '',
									'id' => '',
								),
								'default_value' => '',
								'placeholder' => '',
								'prepend' => '',
								'append' => '',
								'maxlength' => '',
								'parent' => 'review_custom_specs',
							));

							acf_add_local_field(array (
								'key' => $specs_group_id.'_'.$spec_id.'_amountdiscounted',
								'label' => __('Amount Saved', 'ultimateazon2' ),
								'name' => $specs_group_id.'_'.$spec_id.'_amountdiscounted',
								'type' => 'text',
								'instructions' => '',
								'required' => 0,
								'conditional_logic' => 0,
								'wrapper' => array (
									'width' => 50,
									'class' => '',
									'id' => '',
								),
								'default_value' => '',
								'placeholder' => '',
								'prepend' => '',
								'append' => '',
								'maxlength' => '',
								'parent' => 'review_custom_specs',
							));

							break;

						case 'spec-button' :

							acf_add_local_field(array (
								'key' => $specs_group_id.'_'.$spec_id.'_buttonstyle',
								'label' => __('Button Style', 'ultimateazon2' ),
								'name' => $specs_group_id.'_'.$spec_id.'_buttonstyle',
								'type' => 'select',
								'instructions' => '',
								'required' => 0,
								'conditional_logic' => 0,
								'wrapper' => array (
									'width' => '50',
									'class' => '',
									'id' => '',
								),
								'choices' => array (
									'button-rounded' => __('Rounded Corner Button', 'ultimateazon2' ),
									'button-square' => __('Square Corner Button', 'ultimateazon2' ),
									'button-custom' => __('Custom Button Image', 'ultimateazon2' ),
									'button-ama-1' => __('Buy on Amazon Button #1', 'ultimateazon2' ),
									'button-ama-2' => __('Buy on Amazon Button #2', 'ultimateazon2' ),
									'button-ama-3' => __('Buy on Amazon Button #3', 'ultimateazon2' ),
									'button-ama-4' => __('Buy on Amazon Button #4', 'ultimateazon2' ),
									'button-ama-5' => __('Buy on Amazon Button #5', 'ultimateazon2' ),
								),
								'default_value' => array (
								),
								'allow_null' => 0,
								'multiple' => 0,
								'ui' => 0,
								'ajax' => 0,
								'return_format' => 'value',
								'placeholder' => '',
								'parent' => 'review_custom_specs',
							));

							acf_add_local_field(array (
								'key' => $specs_group_id.'_'.$spec_id.'_buttontype',
								'label' => $spec_name.__(' (Button)', 'ultimateazon2' ),
								'name' => $specs_group_id.'_'.$spec_id.'_buttontype',
								'type' => 'select',
								'instructions' => __('Choose whether you want to link the button to your affiliate link, the full review link, or a custom link.', 'ultimateazon2' ),
								'required' => 0,
								'conditional_logic' => 0,
								'wrapper' => array (
									'width' => '50',
									'class' => '',
									'id' => '',
								),
								'choices' => array (
									'button-aff' => __('Affiliate Link', 'ultimateazon2' ),
									'button-full' => __('Full Review Link', 'ultimateazon2' ),
									'button-custom' => __('Custom Link', 'ultimateazon2' ),
								),
								'default_value' => array (
								),
								'allow_null' => 0,
								'multiple' => 0,
								'ui' => 0,
								'ajax' => 0,
								'return_format' => 'value',
								'placeholder' => '',
								'parent' => 'review_custom_specs',
							));

							acf_add_local_field(array (
								'key' => $specs_group_id.'_'.$spec_id.'_buttonimg',
								'label' => __('Button Image', 'ultimateazon2' ),
								'name' => $specs_group_id.'_'.$spec_id.'_buttonimg',
								'type' => 'image',
								'instructions' => '',
								'required' => 0,
								'conditional_logic' => array (
									array (
										array (
											'field' => $specs_group_id.'_'.$spec_id.'_buttonstyle',
											'operator' => '==',
											'value' => 'button-custom',
										),
									),
								),
								'wrapper' => array (
									'width' => '',
									'class' => '',
									'id' => '',
								),
								'return_format' => 'array',
								'preview_size' => 'medium',
								'library' => 'all',
								'min_width' => '',
								'min_height' => '',
								'min_size' => '',
								'max_width' => '',
								'max_height' => '',
								'max_size' => '',
								'mime_types' => '',
								'parent' => 'review_custom_specs',
							));


							acf_add_local_field(array (
								'key' => $specs_group_id.'_'.$spec_id.'_buttontext',
								'label' => __('Button Text', 'ultimateazon2' ),
								'name' => $specs_group_id.'_'.$spec_id.'_buttontext',
								'type' => 'text',
								'instructions' => '',
								'required' => 0,
								'conditional_logic' => array (
									array (
										array (
											'field' => $specs_group_id.'_'.$spec_id.'_buttonstyle',
											'operator' => '==',
											'value' => 'button-rounded',
										),
									),
									array (
										array (
											'field' => $specs_group_id.'_'.$spec_id.'_buttonstyle',
											'operator' => '==',
											'value' => 'button-square',
										),
									)
								),
								'wrapper' => array (
									'width' => '',
									'class' => '',
									'id' => '',
								),
								'default_value' => '',
								'placeholder' => '',
								'prepend' => '',
								'append' => '',
								'maxlength' => '',
								'parent' => 'review_custom_specs',
							));

							acf_add_local_field(array (
								'key' => $specs_group_id.'_'.$spec_id.'_buttonurl',
								'label' => __('Custom Button Link', 'ultimateazon2' ),
								'name' => $specs_group_id.'_'.$spec_id.'_buttonurl',
								'type' => 'text',
								'instructions' => '',
								'required' => 0,
								'conditional_logic' => array (
									array (
										array (
											'field' => $specs_group_id.'_'.$spec_id.'_buttontype',
											'operator' => '==',
											'value' => 'button-custom',
										),
									),
								),
								'wrapper' => array (
									'width' => '',
									'class' => '',
									'id' => '',
								),
								'default_value' => '',
								'placeholder' => '',
								'prepend' => '',
								'append' => '',
								'maxlength' => '',
								'parent' => 'review_custom_specs',
							));

							break;


						case 'spec-shortcode' :

							acf_add_local_field(array (
								'key' => $specs_group_id.'_'.$spec_id,
								'label' => $spec_name,
								'name' => $specs_group_id.'_'.$spec_id,
								'type' => 'text',
								'instructions' => 'Add your shortcode here.',
								'required' => 0,
								'conditional_logic' => 0,
								'wrapper' => array (
									'width' => '',
									'class' => '',
									'id' => '',
								),
								'default_value' => '',
								'placeholder' => '',
								'prepend' => '',
								'append' => '',
								'maxlength' => '',
								'parent' => 'review_custom_specs',
							));

							break;


						case 'amazon' :

						acf_add_local_field(array (
							'key' => $specs_group_id.'_'.$spec_id,
							'label' => $spec_name,
							'name' => '',
							'type' => 'message',
							'instructions' => '',
							'required' => 0,
							'conditional_logic' => 0,
							'wrapper' => array (
								'width' => '',
								'class' => '',
								'id' => '',
							),
							'message' => __('This field will be pulled from Amazon in the front end of your website.', 'ultimateazon2' ),
							'new_lines' => 'wpautop',
							'esc_html' => 0,
							'parent' => 'review_custom_specs',
						));

			    	} // end switch ( $spec_type )

			    } // end foreach ( $current_chosen_spec_group_fields as $field ) {

			} // end if( function_exists('acf_add_local_field_group') )

		} // end if ( $current_chosen_spec_group['field_57c068f1622b7'] == 'create-my-own' )


	}



	







	/**
	 * add the user specs fields to the review post
	 *
	 * @since    1.0.0
	 */
	public function galfram_ext_prp_add_amazon_api_metabox() {

		$amazon_api_enabled = get_option( 'extension-prp-settings_enable_amazon_api_integration' );

		if ( !isset( $_GET ) || !isset( $_POST ) ) {
			return;
		}


			if ( isset( $_GET['post'] ) ) {
				$post_id = $_GET['post'];
			}
			elseif ( isset( $_POST['post_ID'] ) ) {
				$post_id = $_POST['post_ID'];
			}
			else {
				return;
			}
		

		if ( get_post_type($post_id) != 'galfram_review' ) {
			return;
		}

		if ( !$amazon_api_enabled ) {
			return;
		}

		$specs_group_id = get_post_meta($post_id, 'prod_specs_group', true);

		if ( $specs_group_id != 'pull-from-amazon' ) {
			return;
		}

		$user_specs_groups = get_option('galfram_ext_prp_all_userspecs_groups');

		//echo '<pre>'.print_r($user_specs_groups,1).'</pre>';

		$current_chosen_spec_group = array();

		foreach ( $user_specs_groups as $user_spec_group ) {
			if ( $user_spec_group['field_57c35f7e8031b'] == $specs_group_id ) {
				$current_chosen_spec_group = $user_spec_group;
			}
		}


		add_meta_box( 'galfram_amazon_api_metabox', __('Amazon API Integration', 'ultimateazon2' ), 'galfram_add_amazon_api_metabox_info', 'galfram_review', 'advanced', 'high' );


		// Function for out[putting info into the Amazon API Integration Section
		function galfram_add_amazon_api_metabox_info() {

			global $post;
			$post_id = $post->ID;
			?>

			<div id="amazon-api-chosen-asin" class="amazon-api-search-results" data-post-id="<?php echo $post_id; ?>">

				<div id="amazon-api-chosen-asin-inner">

					<?php 
					$chosen_review = get_post_meta( $post_id, 'galfram_chosen_amazon_prod', true);

					if ( !$chosen_review ) :

						echo '<p class="no-amazon-prod-chosen">'.__('You have not chosen a product yet. Please search for one below', 'ultimateazon2' ).'</p>';

					else :




						$galfram_prp_amazon_api = new galfram_productreviewpro_amazon_api();
						$api_response = $galfram_prp_amazon_api->amazon_api_request( get_the_ID(), 'Images,ItemAttributes,Offers,Reviews', $chosen_review );

						/*************************************
						*** Parse our Results form the api ***
						*************************************/

						// convert xml response, we'll use lower while echoing out the custom amazpon specs data
						$arr = simplexml_load_string($api_response);
						

						if ( $arr->Items->Request->IsValid != 'True' ) {
							echo '<p class="amazon-api-failed-request">'.__('There was a problem retreiving the data from Amazon. Make sure your Amazon API key and Secret key are saved and correct', 'ultimateazon2' ).'</p>';
							return;
						}



						$prod_title = htmlspecialchars( $arr->Items->Item->ItemAttributes->Title );
						$prod_asin = $arr->Items->Item->ASIN;
						$prod_url = $arr->Items->Item->DetailPageURL;
						$prod_brand = $arr->Items->Item->ItemAttributes->Brand;
						$prod_model = $arr->Items->Item->ItemAttributes->Model;
						$prod_upc = $arr->Items->Item->ItemAttributes->UPC;
						$prod_features_array = $arr->Items->Item->ItemAttributes->Feature;
						$prod_warranty = $arr->Items->Item->ItemAttributes->Warranty;

					    $prod_formatted_price = $arr->Items->Item->ItemAttributes->ListPrice->FormattedPrice;

					    $prod_lowest_new_price = $arr->Items->Item->OfferSummary->LowestNewPrice->FormattedPrice;
					    $prod_lowest_used_price = $arr->Items->Item->OfferSummary->LowestUsedPrice->FormattedPrice;

					    $prod_lowest_new_price_currency = $arr->Items->Item->OfferSummary->LowestNewPrice->CurrencyCode;
					    $prod_lowest_used_price_currency = $arr->Items->Item->OfferSummary->LowestUsedPrice->CurrencyCode;

					    $prod_price_offer = $arr->Items->Item->Offers->Offer->OfferListing->Price->FormattedPrice;

					    $prod_amount_saved = $arr->Items->Item->Offers->Offer->OfferListing->AmountSaved->FormattedPrice;
					    $prod_amount_saved_currency = $arr->Items->Item->Offers->Offer->OfferListing->AmountSaved->CurrencyCode;
					    $prod_percentage_saved = $arr->Items->Item->Offers->Offer->OfferListing->PercentageSaved;

					    $prod_total_new = $arr->Items->Item->OfferSummary->TotalNew;
					    $prod_total_used = $arr->Items->Item->OfferSummary->TotalNew;

					    $prod_listprice_currencycode = $arr->Items->Item->ItemAttributes->ListPrice->CurrencyCode;


					    $medium_img_url = $arr->Items->Item->MediumImage->URL;
					    if ( $medium_img_url == '' ) {
					    	$image_set = $item->ImageSets;
					    	if ( is_array( $image_set ) ) {
					    		$medium_img_url = $arr->Items->Item->ImageSets->ImageSet[0]->MediumImage->URL;
					    	}
					    	else {
					    		$medium_img_url = $arr->Items->Item->ImageSets->ImageSet->MediumImage->URL;
					    	}
					    }


					    echo '<img id="chosen-api-product-image" src="'.$medium_img_url.'" alt="'.$prod_title.'" class="amazon-api-prod-img" />';

					    echo '<h3>'.__('Your chosen product for this product for this review is', 'ultimateazon2' ).':</h3>';


					    echo '<a id="chosen-api-product-name" href="'.$prod_url.'" target="_blank">';
				    		echo $prod_title;
				    	echo '</a>';


				    	echo '<p id="chosen-api-product-asin"><strong>'.__('ASIN', 'ultimateazon2' ).':</strong> <span>'.$prod_asin.'</span></p>';

				    	if ( $prod_brand == '' ) { echo '<p id="chosen-api-product-brand"><strong>'.__('Brand', 'ultimateazon2' ).':</strong> <span>N/A</span></p>'; }
				    	else {
					    	echo '<p id="chosen-api-product-brand"><strong>'.__('Brand', 'ultimateazon2' ).':</strong> <span>'.$prod_brand.'</span></p>';
					    }


					    if ( $prod_model == '' ) { echo '<p id="chosen-api-product-model"><strong>Model:</strong> <span>'.__('N/A', 'ultimateazon2' ).'</span></p>'; }
				    	else {
					    	echo '<p id="chosen-api-product-model"><strong>'.__('Model', 'ultimateazon2' ).':</strong> <span>'.$prod_model.'</span></p>';
					    }


					    if ( $prod_upc == '' ) { echo '<p id="chosen-api-product-upc"><strong>'.__('UPC', 'ultimateazon2' ).':</strong> <span>N/A</span></p>'; }
				    	else {
					    	echo '<p id="chosen-api-product-upc"><strong>'.__('UPC', 'ultimateazon2' ).':</strong> <span>'.$prod_upc.'</span></p>';
					    }


					    if ( $prod_lowest_new_price == '' ) { echo '<p id="chosen-api-product-lowestnewprice3"><strong>'.__('Lowest New Price', 'ultimateazon2' ).':</strong> <span>N/A</span></p>'; }
				    	else {
				    		echo '<p id="chosen-api-product-lowestnewprice"><strong>'.__('Lowest New Price', 'ultimateazon2' ).':</strong> <span>'.$prod_lowest_new_price.' '.$prod_lowest_new_price_currency.'</span></p>';
				    	}


				    	if ( $prod_lowest_used_price == '' ) { echo '<p id="chosen-api-product-lowestusedprice"><strong>'.__('Lowest Used Price:', 'ultimateazon2' ).'</strong> <span>'.__('N/A', 'ultimateazon2' ).'</span></p>'; } 
				    	else {
				    		echo '<p id="chosen-api-product-lowestusedprice"><strong>Lowest Used Price:</strong> <span>'.$prod_lowest_used_price.' '.$prod_lowest_used_price_currency.'</span></p>';
				    	}


				    	if ( $prod_formatted_price == '' ) { echo '<p id="chosen-api-product-listprice"><strong>'.__('List Price', 'ultimateazon2' ).':</strong> <span>N/A</span></p>'; }
				    	else {
				    		echo '<p id="chosen-api-product-"><strong>'.__('List Price', 'ultimateazon2' ).':</strong> <span>'.$prod_formatted_price.' '.$prod_listprice_currencycode.'</span></p>';
				    	}
				    	

				    	if ( $prod_price_offer == '' ) { echo '<p id="chosen-api-product-bestoffer"><strong>'.__('Best Offer', 'ultimateazon2' ).': </strong> <span>N/A</span></p>'; } 
				    	else {
				    		echo '<p id="chosen-api-product-lowestusedprice"><strong>'.__('Best Offer', 'ultimateazon2' ).':</strong> <span>'.$prod_price_offer.' '.$prod_listprice_currencycode.'</span></p>';
				    	}


				    	if ( $prod_amount_saved == '' ) { echo '<p id="chosen-api-product-ampountsaved"><strong>'.__('Save', 'ultimateazon2' ).': </strong> <span>N/A</span></p>'; } 
				    	else {
							echo '<p id="chosen-api-product-ampountsaved"><strong>'.__('Save', 'ultimateazon2' ).': </strong> <span>'.$prod_amount_saved.' '.$prod_amount_saved_currency.' ('.$prod_percentage_saved.'%)</span></p>';
				    	}


				    	if ( $prod_lowest_used_price == '' ) { echo '<p id="chosen-api-product-lowestusedprice"><strong>'.__('Lowest Used Price', 'ultimateazon2' ).': </strong> <span>N/A</span></p>'; } 
				    	else {
				    		echo '<p id="chosen-api-product-lowestusedprice"><strong>Lowest Used Price:</strong> <span>'.$prod_lowest_used_price.' '.$prod_lowest_used_price_currency.'</span></p>';
				    	}


				    	if ( $prod_warranty == '' ) { echo '<p id="chosen-api-product-warranty"><strong>'.__('Warranty', 'ultimateazon2' ).': </strong> <span>N/A</span></p>'; } 
				    	else {
				    		echo '<p id="chosen-api-product-warranty"><strong>'.__('Warranty', 'ultimateazon2' ).':</strong> '.$prod_warranty.'</span></p>';
				    	}


				    	if ( $prod_features_array == '' ) { echo '<p><strong>'.__('Featutes', 'ultimateazon2' ).':</strong> <span>N/A</span></p>'; }
				    	else {
					    	echo '<p class="features"><strong>'.__('Features', 'ultimateazon2' ).':</strong><br />';
					    	echo '<ul id="chosen-api-product-features">';

						    	foreach ( $prod_features_array as $feature ) {
						    		echo '<li>'.$feature.'</li>';
						    	}

					    	echo '</ul>';
					    }


						echo '<p class="change-prod">('.__('To change this product, search for a new one below', 'ultimateazon2' ).'.)</p>';


					endif; ?>

				</div>



				<div id="choose-amazin-fields-for-display" class="visible">
					<h4><?php _E('Choose which fields you would like to display to your visitors', 'ultimateazon2' ); ?></h4>

					<?php 
					$chosen_specs = get_post_meta( $post_id, 'galfram_chosen_amazon_prod_specs_display', true); 

					if ( !$chosen_specs ) 
						$chosen_specs = array();
					?>

					<div id="choose-display-fields-checkboxes">
						<input type="checkbox" name="choose-display-fields" value="asin" <?php if (in_array('asin', $chosen_specs)) echo 'checked=checked'; ?> /> <?php _e('ASIN', 'ultimateazon2' ); ?><br />
						<input type="checkbox" name="choose-display-fields" value="main-img" <?php if (in_array('main-img', $chosen_specs)) echo 'checked=checked'; ?> /> <?php _e('Main Image (this will override the featured image)', 'ultimateazon2' ); ?><br />
						<input type="checkbox" name="choose-display-fields" value="add-imgs" <?php if (in_array('add-imgs', $chosen_specs)) echo 'checked=checked'; ?> /> <?php _e('Additional Images (this will override the additional images field)', 'ultimateazon2' ); ?><br />
						<input type="checkbox" name="choose-display-fields" value="brand" <?php if (in_array('brand', $chosen_specs)) echo 'checked=checked'; ?> /> <?php _e('Brand', 'ultimateazon2' ); ?><br />
						<input type="checkbox" name="choose-display-fields" value="model" <?php if (in_array('model', $chosen_specs)) echo 'checked=checked'; ?> /> <?php _e('Model', 'ultimateazon2' ); ?><br />
						<input type="checkbox" name="choose-display-fields" value="features" <?php if (in_array('features', $chosen_specs)) echo 'checked=checked'; ?> /> <?php _e('Features', 'ultimateazon2' ); ?><br />
						<input type="checkbox" name="choose-display-fields" value="upc" <?php if (in_array('upc', $chosen_specs)) echo 'checked=checked'; ?> /> <?php _e('UPC', 'ultimateazon2' ); ?><br />
						<input type="checkbox" name="choose-display-fields" value="list-price" <?php if (in_array('list-price', $chosen_specs)) echo 'checked=checked'; ?> /> <?php _e('List Price', 'ultimateazon2' ); ?><br />
						<input type="checkbox" name="choose-display-fields" value="price-offer" <?php if (in_array('price-offer', $chosen_specs)) echo 'checked=checked'; ?> /> <?php _e('Offered Price', 'ultimateazon2' ); ?><br />
						<input type="checkbox" name="choose-display-fields" value="lowest-new-price" <?php if (in_array('lowest-new-price', $chosen_specs)) echo 'checked=checked'; ?> /> <?php _e('Lowest New Price', 'ultimateazon2' ); ?><br />
						<input type="checkbox" name="choose-display-fields" value="amount-saved" <?php if (in_array('amount-saved', $chosen_specs)) echo 'checked=checked'; ?> /> <?php _e('Amount Saved (total and percentage)', 'ultimateazon2' ); ?><br />
						<input type="checkbox" name="choose-display-fields" value="lowest-used-price" <?php if (in_array('lowest-used-price', $chosen_specs)) echo 'checked=checked'; ?> /> <?php _e('Lowest Used Price', 'ultimateazon2' ); ?><br />
						<input type="checkbox" name="choose-display-fields" value="warranty" <?php if (in_array('warranty', $chosen_specs)) echo 'checked=checked'; ?> /> <?php _e('Warranty', 'ultimateazon2' ); ?><br />
						<span id="galfram-save-amazon-spec-display-choices" class="button button-primary" /><?php _e('Save These Choices', 'ultimateazon2' ); ?></span>
					</div>
				</div>




			</div>





			<div id="galfram-api-lookup">

				<form>

					<label>Enter your Keywords or the product ASIN here</label>
					<input id="galfram-api-search-term" type="text" value="">

					<?php $chosen_locale = get_option('extension-prp-settings_default_amazon_search_locale', true); ?>

					<input id="galfram-search-locale" type="hidden" name="galfram-search-locale" value="<?php echo $chosen_locale; ?>">
					<input id="galfram-search-specs" type="hidden" name="galfram-search-specs" value="amazon-only">

					<?php 
					if ( $chosen_locale == 'US') { echo '<span class="search-locale-label">'.__('Locale: United States', 'ultimateazon2' ).'</span>'; }
					else if ( $chosen_locale == 'UK') { echo '<span class="search-locale-label">'.__('Locale: United Kingdom', 'ultimateazon2' ).'</span>'; }
					else if ( $chosen_locale == 'BR') { echo '<span class="search-locale-label">'.__('Locale: Brazil', 'ultimateazon2' ).'</span>'; }
					else if ( $chosen_locale == 'CA') { echo '<span class="search-locale-label">'.__('Locale: Canada', 'ultimateazon2' ).'</span>'; }
					else if ( $chosen_locale == 'CN') { echo '<span class="search-locale-label">'.__('Locale: China', 'ultimateazon2' ).'</span>'; }
					else if ( $chosen_locale == 'FR') { echo '<span class="search-locale-label">'.__('Locale: France', 'ultimateazon2' ).'</span>'; }
					else if ( $chosen_locale == 'DE') { echo '<span class="search-locale-label">'.__('Locale: Germany', 'ultimateazon2' ).'</span>'; }
					else if ( $chosen_locale == 'IN') { echo '<span class="search-locale-label">'.__('Locale: India', 'ultimateazon2' ).'</span>'; }
					else if ( $chosen_locale == 'MX') { echo '<span class="search-locale-label">'.__('Locale: Mexico', 'ultimateazon2' ).'</span>'; }
					else if ( $chosen_locale == 'IT') { echo '<span class="search-locale-label">'.__('Locale: Italy', 'ultimateazon2' ).'</span>'; }
					else if ( $chosen_locale == 'JP') { echo '<span class="search-locale-label">'.__('Locale: Japan', 'ultimateazon2' ).'</span>'; }
					else if ( $chosen_locale == 'ES') { echo '<span class="search-locale-label">'.__('Locale: Spain', 'ultimateazon2' ).'</span>'; }
					else { _e('Locale: United States', 'ultimateazon2' ); }
					?>

					<input id="galfram-api-search-submit" type="button" class="button button-primary button-large" value="<?php _e('Search', 'ultimateazon2' ); ?>" />

				</form>

			</div>

			<div id="galfram-amazon-api-results" class="amazon-api-search-results"></div>

			<div class="galfram-amazon-loading-overlay test yo">
				<img src="<?php echo get_template_directory_uri( __FILE__ ) . '/lib/plugins/uat2-prp/admin/img/loading-circle.gif'; ?>" alt="loading" />
			</div>

			<?php
			
		}



	}




	/** prepare our ajax call to the amazon product advertising api
	 *
	 * @since    1.0.0
	 */
	public function galfram_find_amazon_products_ajax() {

		global $wpdb; // this is how you get access to the database

	    $term = $_POST['term'];
	    $locale = $_POST['locale'];
	    $specstype = $_POST['searchspecs'];
	    $page = '1';

	    // make our amazon api call
	    $api_response = $this->galfram_amazon_api_request( $term, $locale, $page );

	    // parse our amazon api call response
	    echo $this->galfram_amazon_api_response_parse( $api_response, $term, $locale, $specstype );

	    die(); // this is required to return a proper result

	}






	/** make our ajax call to the amazon product advertising api
	 *
	 * @since    1.0.0
	 */
	public function galfram_amazon_api_request( $term, $locale, $page ) {


		// http://webservices.amazon.com/scratchpad/index.html

		// Your AWS Access Key ID, as taken from the AWS Your Account page
		$aws_access_key_id = trim( get_option( 'extension-prp-settings_amazon_api_key' ) );

		// Your AWS Secret Key corresponding to the above ID, as taken from the AWS Your Account page
		$aws_secret_key = trim( get_option( 'extension-prp-settings_amazon_api_secret' ) );

		$affiliate_id = trim( get_option( 'extension-prp-settings_amazon_affiliate_id_'.$locale ) );

		if ( $locale == 'US') {
			$endpoint = "webservices.amazon.com";
		}
		elseif ( $locale == 'UK') {
			$endpoint = "webservices.amazon.co.uk";
		}
		elseif ( $locale == 'BR') {
			$endpoint = "webservices.amazon.com.br";
		}
		elseif ( $locale == 'CA') {
			$endpoint = "webservices.amazon.ca";
		}
		elseif ( $locale == 'CN') {
			$endpoint = "webservices.amazon.cn";
		}
		elseif ( $locale == 'DE') {
			$endpoint = "webservices.amazon.de";
		}
		elseif ( $locale == 'ES') {
			$endpoint = "webservices.amazon.es";
		}
		elseif ( $locale == 'FR') {
			$endpoint = "webservices.amazon.fr";
		}
		elseif ( $locale == 'IT') {
			$endpoint = "webservices.amazon.it";
		}
		elseif ( $locale == 'IN') {
			$endpoint = "webservices.amazon.in";
		}
		elseif ( $locale == 'JP') {
			$endpoint = "webservices.amazon.co.jp";
		}

		$uri = "/onca/xml";

		$params = array(
		    "Service" => "AWSECommerceService",
		    "Operation" => "ItemSearch",
		    "AWSAccessKeyId" => $aws_access_key_id,
		    "AssociateTag" => $affiliate_id,
		    "SearchIndex" => "All",
		    "Keywords" => $term,
		    "ItemPage" => $page,
		    "ResponseGroup" => "Images,ItemAttributes,Offers,Reviews"
		);

		// Set current timestamp if not set
		if (!isset($params["Timestamp"])) {
		    $params["Timestamp"] = gmdate('Y-m-d\TH:i:s\Z');
		}

		// Sort the parameters by key
		ksort($params);

		$pairs = array();

		foreach ($params as $key => $value) {
		    array_push($pairs, rawurlencode($key)."=".rawurlencode($value));
		}

		// Generate the canonical query
		$canonical_query_string = join("&", $pairs);

		// Generate the string to be signed
		$string_to_sign = "GET\n".$endpoint."\n".$uri."\n".$canonical_query_string;

		// Generate the signature required by the Product Advertising API
		$signature = base64_encode(hash_hmac("sha256", $string_to_sign, $aws_secret_key, true));

		// Generate the signed URL
		$request_url = 'https://'.$endpoint.$uri.'?'.$canonical_query_string.'&Signature='.rawurlencode($signature);

		// get our xml file contents
		$amazon_request_return = wp_remote_get($request_url);
		$request_response = $amazon_request_return['response']['code'];

		if ( $request_response == '200' ) {
			$response = wp_remote_retrieve_body($amazon_request_return);
		}
		else {
			$response = 'NOT 200 - Request Denied ny Amazon';
		}

		return $response;

	}




	/** parse our response form our ajax call to the amazon product advertising api
	 *
	 * @since    1.0.0
	 */
	function galfram_amazon_api_response_parse( $api_response, $term, $locale, $specstype ) {

		// convert xml response
		$arr = simplexml_load_string($api_response);

		if ( $arr->Items->Request->IsValid != 'True' ) {
			echo '<p class="amazon-api-failed-request">'.__('There was a problem retreiving the data from Amazon. Make sure your Amazon API key, Secret key, and amazon id for the locale you are searching are saved and correct in the', 'ultimateazon2' ).' <a href="/wp-admin/admin.php?page=extension-prp-settings" target="_blank">'.__('settings', 'ultimateazon2' ).'</a>.</p>';
			return;
		}

		echo '<div class="amazon-api-search-header">';

			echo '<h3>'.__('Search Results', 'ultimateazon2' ).':</h3>';
			//$results = (int)$arr->Items->TotalResults;
			$formatted_results = number_format( (int)$arr->Items->TotalResults );
			echo '<p>'.$formatted_results.' '.__('Results', 'ultimateazon2' ).'</p>';

		echo '</div>';

		echo '<div class="amazon-search-pagination">';
			echo '<span class="amazon-search-page current" data-api-page="1" data-api-term="'.$term.'" data-api-locale="'.$locale.'" data-api-specstype="'.$specstype.'">Page 1</span>';
			echo '<span class="amazon-search-page" data-api-page="2" data-api-term="'.$term.'" data-api-locale="'.$locale.'" data-api-specstype="'.$specstype.'">2</span>';
			echo '<span class="amazon-search-page" data-api-page="3" data-api-term="'.$term.'" data-api-locale="'.$locale.'" data-api-specstype="'.$specstype.'">3</span>';
			echo '<span class="amazon-search-page" data-api-page="4" data-api-term="'.$term.'" data-api-locale="'.$locale.'" data-api-specstype="'.$specstype.'">4</span>';
			echo '<span class="amazon-search-page" data-api-page="5" data-api-term="'.$term.'" data-api-locale="'.$locale.'" data-api-specstype="'.$specstype.'">5</span>';
			echo '<span class="amazon-search-page refine">... or refine your search!</span>';
		echo '</div>';

		echo '<div id="galfram-amazon-api-results-inner">';
		
			// loop through our results and parse
			foreach($arr->Items->Item as $item)
			{

				$prod_title = htmlspecialchars( $item->ItemAttributes->Title );
				$prod_asin = $item->ASIN;
				$prod_url = $item->DetailPageURL;
				$prod_brand = $item->ItemAttributes->Brand;
				$prod_model = $item->ItemAttributes->Model;
				$prod_upc = $item->ItemAttributes->UPC;
				$prod_features_array = $item->ItemAttributes->Feature;
				$prod_warranty = $item->ItemAttributes->Warranty;

			    $prod_formatted_price = $item->ItemAttributes->ListPrice->FormattedPrice;

			    $prod_lowest_new_price = $item->OfferSummary->LowestNewPrice->FormattedPrice;
			    $prod_lowest_used_price = $item->OfferSummary->LowestUsedPrice->FormattedPrice;

			    $prod_lowest_new_price_currency = $item->OfferSummary->LowestNewPrice->CurrencyCode;
			    $prod_lowest_used_price_currency = $item->OfferSummary->LowestUsedPrice->CurrencyCode;

			    $prod_price_offer = $item->Offers->Offer->OfferListing->Price->FormattedPrice;

			    $prod_amount_saved = $item->Offers->Offer->OfferListing->AmountSaved->FormattedPrice;
			    $prod_amount_saved_currency = $item->Offers->Offer->OfferListing->AmountSaved->CurrencyCode;
			    $prod_percentage_saved = $item->Offers->Offer->OfferListing->PercentageSaved;

			    $prod_total_new = $item->OfferSummary->TotalNew;
			    $prod_total_used = $item->OfferSummary->TotalNew;

			    $prod_listprice_currency_code = $item->ItemAttributes->ListPrice->CurrencyCode;


			    $medium_img_url = $item->MediumImage->URL;
			    if ( $medium_img_url == '' ) {
			    	$image_set = $item->ImageSets;
			    	if ( is_array( $image_set ) ) {
			    		$medium_img_url = $item->ImageSets->ImageSet[0]->MediumImage->URL;
			    	}
			    	else {
			    		$medium_img_url = $item->ImageSets->ImageSet->MediumImage->URL;
			    	}
			    }

			    echo '<div class="amazon-api-prod-box">';

			    	echo '<img src="'.$medium_img_url.'" alt="'.$prod_title.'" class="amazon-api-prod-img" />';

			    	echo '<a href="'.$prod_url.'" target="_blank" class="amazon-api-result-title">';
			    		echo $prod_title;
			    	echo '</a>';

			    	echo '<p><strong>'.__('ASIN', 'ultimateazon2' ).':</strong> '.$prod_asin.'</p>';

			    	if ( $prod_brand == '' ) { echo '<p><strong>'.__('Brand', 'ultimateazon2' ).':</strong> N/A</p>'; }
			    	else {
				    	echo '<p><strong>'.__('Brand', 'ultimateazon2' ).':</strong> '.$prod_brand.'</p>';
				    }


				    if ( $prod_model == '' ) { echo '<p><strong>'.__('Model', 'ultimateazon2' ).':</strong> '.__('N/A', 'ultimateazon2' ).'</p>'; }
			    	else {
				    	echo '<p><strong>'.__('Model', 'ultimateazon2' ).':</strong> '.$prod_model.'</p>';
				    }


				    if ( $prod_upc == '' ) { echo '<p><strong>'.__('UPC', 'ultimateazon2' ).':</strong> N/A</p>'; }
			    	else {
				    	echo '<p><strong>'.__('UPC', 'ultimateazon2' ).':</strong> '.$prod_upc.'</p>';
				    }


				    if ( $prod_lowest_new_price == '' ) { echo '<p><strong>'.__('Lowest New Price', 'ultimateazon2' ).':</strong> '.__('N/A', 'ultimateazon2' ).'</p>'; }
			    	else {
			    		echo '<p><strong>'.__('Lowest New Price', 'ultimateazon2' ).':</strong> '.$prod_lowest_new_price.' '.$prod_lowest_new_price_currency.'</p>';
			    	}


			    	if ( $prod_lowest_used_price == '' ) { echo '<p><strong>'.__('Lowest Used Price', 'ultimateazon2' ).': </strong>'.__('N/A', 'ultimateazon2' ).'</p>'; } 
			    	else {
			    		echo '<p><strong>'.__('Lowest Used Price', 'ultimateazon2' ).':</strong> '.$prod_lowest_used_price.' '.$prod_lowest_used_price_currency.'</p>';
			    	}


			    	if ( $prod_formatted_price == '' ) { echo '<p><strong>'.__('List Price', 'ultimateazon2' ).':</strong> '.__('N/A', 'ultimateazon2' ).'</p>'; }
			    	else {
			    		echo '<p><strong>'.__('List Price', 'ultimateazon2' ).':</strong> '.$prod_formatted_price.' '.$prod_listprice_currency_code.'</p>';
			    	}


		    		if ( $prod_price_offer == '' ) { echo '<p id="chosen-api-product-bestoffer"><strong>'.__('Best Offer', 'ultimateazon2' ).': </strong> <span>'.__('N/A', 'ultimateazon2' ).'</span></p>'; } 
			    	else {
			    		echo '<p id="chosen-api-product-bestoffer"><strong>'.__('Best Offer', 'ultimateazon2' ).':</strong> <span>'.$prod_price_offer.' '.$prod_listprice_currencycode.'</span></p>';
			    	}


			    	if ( $prod_amount_saved == '' ) { echo '<p><strong>'.__('Save', 'ultimateazon2' ).': </strong> '.__('N/A', 'ultimateazon2' ).'</p>'; } 
			    	else {
						echo '<p><strong>'.__('Save', 'ultimateazon2' ).': </strong> '.$prod_amount_saved.' '.$prod_amount_saved_currency.' ('.$prod_percentage_saved.'%)</p>';
			    	}


			    	if ( $prod_warranty == '' ) { echo '<p><strong>'.__('Warranty', 'ultimateazon2' ).': </strong> '.__('N/A', 'ultimateazon2' ).'</p>'; } 
			    	else {
			    		echo '<p><strong>'.__('Warranty', 'ultimateazon2' ).':</strong> '.$prod_warranty.'</p>';
			    	}


			    	if ( $prod_features_array == '' ) { echo '<p><strong>'.__('Featutes', 'ultimateazon2' ).':</strong> '.__('N/A', 'ultimateazon2' ).'</p>'; }
			    	else {
				    	echo '<p class="features"><strong>'.__('Features', 'ultimateazon2' ).':</strong><br />';
				    	echo '<ul>';

				    	foreach ( $prod_features_array as $feature ) {
				    		echo '<li>'.$feature.'</li>';
				    	}

				    	echo '</ul>';

				    	$features_data_attr = array();

						foreach ( (array) $prod_features_array as $index => $node )
						    $features_data_attr[$index] = ( is_object ( $node ) ) ? xml2array ( $node ) : $node;

						$prod_features_array_json = json_encode($prod_features_array);
	

				    }


			    	echo "<span class=\"insert-amazon-product\" 
			    		data-prod-asin=\"".$prod_asin."\" 
			    		data-prod-name=\"".$prod_title."\" 
			    		data-prod-brand=\"".$prod_brand."\" 
			    		data-prod-model=\"".$prod_model."\" 
			    		data-prod-upc=\"".$prod_upc."\" 
			    		data-prod-url=\"".$prod_url."\" 
			    		data-prod-image=\"".$medium_img_url."\" 
			    		data-prod-listprice=\"".$prod_formatted_price."\" 
			    		data-prod-listpricecurrencycode=\"".$prod_listprice_currencycode."\" 
			    		data-prod-bestoffer=\"".$prod_price_offer."\" 
			    		data-prod-lowestnewprice=\"".$prod_lowest_new_price."\" 
			    		data-prod-lowestusedprice=\"".$prod_lowest_new_price."\" 
			    		data-prod-lowestnewpricecurrency=\"".$prod_lowest_new_price_currency."\" 
			    		data-prod-lowestusedpricecurrency=\"".$prod_lowest_new_price_currency."\" 
			    		data-prod-amountsaved=\"".$prod_amount_saved."\" 
			    		data-prod-amountsavedcurrency=\"".$prod_amount_saved_currency."\" 
			    		data-prod-amountsavespercentage=\"".$prod_percentage_saved."\" 
			    		data-prod-features=\"".htmlspecialchars(json_encode($features_data_attr))."\" 
			    		data-prod-warranty=\"".$prod_warranty."\" 
			    		data-prod-specstype=\"".$specstype."\"
			    	>Choose This Item</span>";

			    	echo '<div class="clearfix"></div>';
			    echo '</div>';
			}

		echo '</div>';

		echo '';

		echo '<div class="amazon-search-pagination">';
			echo '<span class="amazon-search-page current" data-api-page="1" data-api-term="'.$term.'" data-api-locale="'.$locale.'">'.__('Page', 'ultimateazon2').' 1</span>';
			echo '<span class="amazon-search-page" data-api-page="2" data-api-term="'.$term.'" data-api-locale="'.$locale.'" data-api-specstype="'.$specstype.'">2</span>';
			echo '<span class="amazon-search-page" data-api-page="3" data-api-term="'.$term.'" data-api-locale="'.$locale.'" data-api-specstype="'.$specstype.'">3</span>';
			echo '<span class="amazon-search-page" data-api-page="4" data-api-term="'.$term.'" data-api-locale="'.$locale.'" data-api-specstype="'.$specstype.'">4</span>';
			echo '<span class="amazon-search-page" data-api-page="5" data-api-term="'.$term.'" data-api-locale="'.$locale.'" data-api-specstype="'.$specstype.'">5</span>';
			echo '<span class="amazon-search-page refine">'.__('... or refine your search!', 'ultimateazon2').'</span>';
		echo '</div>';


		// USE THIS To SEE FULL RESPONSE
		//echo '<hr /><pre>'.print_r($arr,1).'</pre>';

	}







	/** prepare our ajax call to the amazon product advertising api to load a new page of results
	 *
	 * @since    1.0.0
	 */
	public function galfram_load_amazon_products_newpage_ajax() {

		global $wpdb; // this is how you get access to the database

	    $term = $_POST['term'];
	    $locale = $_POST['locale'];
	    $page = $_POST['page'];
	    $specstype = $_POST['searchspecs'];

	    $api_response = $this->galfram_amazon_api_request( $term, $locale, $page );

	    echo $this->galfram_amazon_api_response_parse_new_page( $api_response, $term, $locale, $specstype );

	    die(); // this is required to return a proper result
	}





	/** parse our response form our ajax call to the amazon product advertising api
	 *
	 * @since    1.0.0
	 */
	function galfram_amazon_api_response_parse_new_page( $api_response, $term, $locale, $specstype ) {

		// convert xml response
		$arr = simplexml_load_string($api_response);

		if ( $arr->Items->Request->IsValid != 'True' ) {
			echo '<p class="amazon-api-failed-request">'.__('There was a problem retreiving the data from Amazon. Make sure your Amazon API key and Secret key are saved and correct', 'ultimateazon2' ).'.</p>';
			return;
		}
		
			// loop through our results and parse
			foreach($arr->Items->Item as $item)
			{

				$prod_title = htmlspecialchars( $item->ItemAttributes->Title );
				$prod_asin = $item->ASIN;
				$prod_url = $item->DetailPageURL;
				$prod_brand = $item->ItemAttributes->Brand;
				$prod_model = $item->ItemAttributes->Model;
				$prod_upc = $item->ItemAttributes->UPC;
				$prod_features_array = $item->ItemAttributes->Feature;
				$prod_warranty = $item->ItemAttributes->Warranty;

			    $prod_formatted_price = $item->ItemAttributes->ListPrice->FormattedPrice;

			    $prod_price_offer = $item->Offers->Offer->OfferListing->Price->FormattedPrice;

			    $prod_lowest_new_price = $item->OfferSummary->LowestNewPrice->FormattedPrice;
			    $prod_lowest_used_price = $item->OfferSummary->LowestUsedPrice->FormattedPrice;

			    $prod_lowest_new_price_currency = $item->OfferSummary->LowestNewPrice->CurrencyCode;
			    $prod_lowest_used_price_currency = $item->OfferSummary->LowestUsedPrice->CurrencyCode;

			    $prod_amount_saved = $item->Offers->Offer->OfferListing->AmountSaved->FormattedPrice;
			    $prod_amount_saved_currency = $item->Offers->Offer->OfferListing->AmountSaved->CurrencyCode;
			    $prod_percentage_saved = $item->Offers->Offer->OfferListing->PercentageSaved;

			    // $prod_total_new = $item->OfferSummary->TotalNew;
			    // $prod_total_used = $item->OfferSummary->TotalNew;

			    $prod_listprice_currency_code = $item->ItemAttributes->ListPrice->CurrencyCode;


			    $medium_img_url = $item->MediumImage->URL;
			    if ( $medium_img_url == '' ) {
			    	$image_set = $item->ImageSets;
			    	if ( is_array( $image_set ) ) {
			    		$medium_img_url = $item->ImageSets->ImageSet[0]->MediumImage->URL;
			    	}
			    	else {
			    		$medium_img_url = $item->ImageSets->ImageSet->MediumImage->URL;
			    	}
			    }

			    echo '<div class="amazon-api-prod-box">';

			    	echo '<img src="'.$medium_img_url.'" alt="'.$prod_title.'" class="amazon-api-prod-img" />';

			    	echo '<a href="'.$prod_url.'" target="_blank" class="amazon-api-result-title">';
			    		echo $prod_title;
			    	echo '</a>';

			    	echo '<p><strong>'.__('ASIN', 'ultimateazon2' ).':</strong> '.$prod_asin.'</p>';

			    	if ( $prod_brand == '' ) { echo '<p><strong>'.__('Brand', 'ultimateazon2' ).':</strong> '.__('N/A', 'ultimateazon2' ).'</p>'; }
			    	else {
				    	echo '<p><strong>'.__('Brand', 'ultimateazon2' ).':</strong> '.$prod_brand.'</p>';
				    }


				    if ( $prod_model == '' ) { echo '<p><strong>'.__('Model', 'ultimateazon2' ).':</strong> '.__('N/A', 'ultimateazon2' ).'</p>'; }
			    	else {
				    	echo '<p><strong>'.__('Model', 'ultimateazon2' ).':</strong> '.$prod_model.'</p>';
				    }


				    if ( $prod_upc == '' ) { echo '<p><strong>'.__('UPC', 'ultimateazon2' ).':</strong> '.__('N/A', 'ultimateazon2' ).'</p>'; }
			    	else {
				    	echo '<p><strong>'.__('UPC', 'ultimateazon2' ).':</strong> '.$prod_upc.'</p>';
				    }


				    if ( $prod_lowest_new_price == '' ) { echo '<p><strong>'.__('Lowest New Price', 'ultimateazon2' ).':</strong> '.__('N/A', 'ultimateazon2' ).'</p>'; }
			    	else {
			    		echo '<p><strong>'.__('Lowest New Price', 'ultimateazon2' ).':</strong> '.$prod_lowest_new_price.' '.$prod_lowest_new_price_currency.'</p>';
			    	}

			    	if ( $prod_lowest_used_price == '' ) { echo '<p><strong>'.__('Lowest Used Price', 'ultimateazon2' ).': </strong> '.__('N/A', 'ultimateazon2' ).'</p>'; } 
			    	else {
			    		echo '<p><strong>'.__('Lowest Used Price', 'ultimateazon2' ).':</strong> '.$prod_lowest_used_price.' '.$prod_lowest_used_price_currency.'</p>';
			    	}


			    	if ( $prod_formatted_price == '' ) { echo '<p><strong>'.__('List Price', 'ultimateazon2' ).':</strong> '.__('N/A', 'ultimateazon2' ).'</p>'; }
			    	else {
			    		echo '<p><strong>'.__('List Price', 'ultimateazon2' ).':</strong> '.$prod_formatted_price.' '.$prod_listprice_currency_code.'</p>';
			    	}
			    	

			    	if ( $prod_price_offer == '' ) { echo '<p><strong>'.__('Best Offer', 'ultimateazon2' ).': </strong> <span>'.__('N/A', 'ultimateazon2' ).'</span></p>'; } 
			    	else {
			    		echo '<p><strong>'.__('Best Offer', 'ultimateazon2' ).':</strong> <span>'.$prod_price_offer.' '.$prod_listprice_currencycode.'</span></p>';
			    	}


			    	if ( $prod_amount_saved == '' ) { echo '<p><strong>'.__('Save', 'ultimateazon2' ).': </strong> '.__('N/A', 'ultimateazon2' ).'</p>'; } 
			    	else {
						echo '<p><strong>'.__('Save', 'ultimateazon2' ).': </strong> '.$prod_amount_saved.' '.$prod_amount_saved_currency.' ('.$prod_percentage_saved.'%)</p>';
			    	}



			    	if ( $prod_warranty == '' ) { echo '<p><strong>'.__('Warranty', 'ultimateazon2' ).': </strong> '.__('N/A', 'ultimateazon2' ).'</p>'; } 
			    	else {
			    		echo '<p><strong>'.__('Warranty', 'ultimateazon2' ).':</strong> '.$prod_warranty.'</p>';
			    	}


			    	if ( $prod_features_array == '' ) { echo '<p><strong>'.__('Features', 'ultimateazon2' ).':</strong> '.__('N/A', 'ultimateazon2' ).'</p>'; }
			    	else {
				    	echo '<p class="features"><strong>'.__('Features', 'ultimateazon2' ).':</strong><br />';
				    	echo '<ul>';

				    	foreach ( $prod_features_array as $feature ) {
				    		echo '<li>'.$feature.'</li>';
				    	}

				    	$features_data_attr = array();

				    	// these is the data for the "choose item" button data attribute
				    	foreach ( $prod_features_array as $feature ) {
				    		$features_data_attr[] = '['.$feature.']';
				    	}


				    	echo '</ul>';
				    }
				    

			    	echo "<span class=\"insert-amazon-product\" 
			    		data-prod-asin=\"".$prod_asin."\" 
			    		data-prod-name=\"".$prod_title."\" 
			    		data-prod-brand=\"".$prod_brand."\" 
			    		data-prod-model=\"".$prod_model."\" 
			    		data-prod-upc=\"".$prod_upc."\" 
			    		data-prod-url=\"".$prod_url."\" 
			    		data-prod-image=\"".$medium_img_url."\" 
			    		data-prod-listprice=\"".$prod_formatted_price."\" 
			    		data-prod-listpricecurrencycode=\"".$prod_listprice_currencycode."\" 
			    		data-prod-bestoffer=\"".$prod_price_offer."\" 
			    		data-prod-lowestnewprice=\"".$prod_lowest_new_price."\" 
			    		data-prod-lowestusedprice=\"".$prod_lowest_new_price."\" 
			    		data-prod-lowestnewpricecurrency=\"".$prod_lowest_new_price_currency."\" 
			    		data-prod-lowestusedpricecurrency=\"".$prod_lowest_new_price_currency."\" 
			    		data-prod-amountsaved=\"".$prod_amount_saved."\" 
			    		data-prod-amountsavedcurrency=\"".$prod_amount_saved_currency."\" 
			    		data-prod-amountsavespercentage=\"".$prod_percentage_saved."\" 
			    		data-prod-features=\"".htmlspecialchars(json_encode($features_data_attr))."\" 
			    		data-prod-warranty=\"".$prod_warranty."\" 
			    		data-prod-specstype=\"".$specstype."\"
			    	>Choose This Item</span>";

			    	echo '<div class="clearfix"></div>';
			    echo '</div>';
			}

		//echo '</div>';

		// USE THIS To SEE FULL RESPONSE
		//echo '<hr /><hr /><hr /><pre>'.print_r($arr,1).'</pre>';

	}






	/** save our chosen amazon product in the database. Displaying of the data takes place in teh javascript.
	 *
	 * @since    1.0.0
	 */
	function galfram_amazon_api_insert_chosen_ajax() {

		global $wpdb; // this is how you get access to the database
		global $post;

		$post_id = $_POST['post_id'];
		$asin = $_POST['asin'];

		update_post_meta( $post_id, 'galfram_chosen_amazon_prod', $asin );

		$response = 'success';

		echo $response;
		die();


	}



	/** save our chosen amazon product in the database. Displaying of the data takes place in teh javascript.
	 *
	 * @since    1.0.0
	 */
	function galfram_amazon_api_insert_chosen_customspecs_ajax() {

		global $wpdb; // this is how you get access to the database
		global $post;

		$post_id = $_POST['post_id'];
		$asin = $_POST['asin'];
		$prod_url = $_POST['prod_url'];

		update_post_meta( $post_id, 'amazon_manual_product_asin', $asin );
		update_post_meta( $post_id, 'manual_affiliate_link', $prod_url );

		$response = $post_id.' - '.$asin;

		echo $response;
		die();


	}







	/** save our chosen amazon specs to display in the review frontend
	 *
	 * @since    1.0.0
	 */
	function galfram_save_amazon_specs_to_display_ajax() {

		global $wpdb; // this is how you get access to the database
		global $post;

		$post_id = $_POST['post_id'];
		$displayed_fields = $_POST['displayed_fields'];

		update_post_meta( $post_id, 'galfram_chosen_amazon_prod_specs_display', $displayed_fields );


		$response = 'success';

		echo $response;
		die();

	}



	public function amazon_search_thickbox() {
	?>

		<?php add_thickbox(); ?>
		<div id="galfram-amazon-lookup" style="display:none;">
		    

			<div id="galfram-api-lookup">

			<form>

				<label>Enter your Keywords or the product ASIN here</label>
				<input id="galfram-api-search-term" type="text" value="">

				<?php $chosen_locale = get_option('extension-prp-settings_default_amazon_search_locale', true); ?>

				<input id="galfram-search-locale" type="hidden" name="galfram-search-locale" value="<?php echo $chosen_locale; ?>">
				<input id="galfram-search-specs" type="hidden" name="galfram-search-specs" value="custom-specs">

				<?php 
				if ( $chosen_locale == 'US') { echo '<span class="search-locale-label">'.__('Locale: United States', 'ultimateazon2' ).'</span>'; }
				else if ( $chosen_locale == 'UK') { echo '<span class="search-locale-label">'.__('Locale: United Kingdom', 'ultimateazon2' ).'</span>'; }
				else if ( $chosen_locale == 'BR') { echo '<span class="search-locale-label">'.__('Locale: Brazil', 'ultimateazon2' ).'</span>'; }
				else if ( $chosen_locale == 'CA') { echo '<span class="search-locale-label">'.__('Locale: Canada', 'ultimateazon2' ).'</span>'; }
				else if ( $chosen_locale == 'CN') { echo '<span class="search-locale-label">'.__('Locale: China', 'ultimateazon2' ).'</span>'; }
				else if ( $chosen_locale == 'FR') { echo '<span class="search-locale-label">'.__('Locale: France', 'ultimateazon2' ).'</span>'; }
				else if ( $chosen_locale == 'DE') { echo '<span class="search-locale-label">'.__('Locale: Germany', 'ultimateazon2' ).'</span>'; }
				else if ( $chosen_locale == 'IN') { echo '<span class="search-locale-label">'.__('Locale: India', 'ultimateazon2' ).'</span>'; }
				else if ( $chosen_locale == 'MX') { echo '<span class="search-locale-label">'.__('Locale: Mexico', 'ultimateazon2' ).'</span>'; }
				else if ( $chosen_locale == 'IT') { echo '<span class="search-locale-label">'.__('Locale: Italy', 'ultimateazon2' ).'</span>'; }
				else if ( $chosen_locale == 'JP') { echo '<span class="search-locale-label">'.__('Locale: Japan', 'ultimateazon2' ).'</span>'; }
				else if ( $chosen_locale == 'ES') { echo '<span class="search-locale-label">'.__('Locale: Spain', 'ultimateazon2' ).'</span>'; }
				else { _e('Locale: United States', 'ultimateazon2' ); }
				?>

				<input id="galfram-api-search-submit" type="button" class="button button-primary button-large" value="<?php _e('Search', 'ultimateazon2' ); ?>" />

				</form>

			</div>

			<div id="galfram-amazon-api-results" class="amazon-api-search-results"></div>

			<div class="galfram-amazon-loading-overlay test">
				<img src="<?php echo get_template_directory_uri( __FILE__ ) . '/lib/plugins/uat2-prp/admin/img/loading-circle.gif'; ?>" alt="loading" />
			</div>

		</div>



		<?php
	}

	





























}
