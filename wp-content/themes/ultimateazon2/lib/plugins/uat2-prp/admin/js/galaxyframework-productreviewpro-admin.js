(function( $ ) {
	'use strict';

	/**
	 * All of the code for your admin-facing JavaScript source
	 * should reside in this file.
	 *
	 * Note: It has been assumed you will write jQuery code here, so the
	 * $ function reference has been prepared for usage within the scope
	 * of this function.
	 *
	 * This enables you to define handlers, for when the DOM is ready:
	 *
	 * $(function() {
	 *
	 * });
	 *
	 * When the window is loaded:
	 *
	 * $( window ).load(function() {
	 *
	 * });
	 *
	 * ...and/or other possibilities.
	 *
	 * Ideally, it is not considered best practise to attach more than a
	 * single DOM-ready or window-load handler for a particular page.
	 * Although scripts in the WordPress core, Plugins and Themes may be
	 * practising this, we should strive to set a better example in our own work.
	 */


	$(document).ready( function(){ 


		
		// submit amazon api search on review admin pedit screen
		$('body').on( 'click', '#galfram-api-search-submit', function(){

			$('.galfram-amazon-loading-overlay').fadeIn(100);

			var par = $(this).closest('#galfram-api-lookup');
			var term = par.find('#galfram-api-search-term').val();
			var locale = par.find('#galfram-search-locale').val();
			var searchspecs = par.find('#galfram-search-specs').val();

			//alert( locale );

			var data = {
	            action: 'galfram_find_amazon_products_ajax',
	            term: term,
	            locale: locale,
	            searchspecs: searchspecs
	        };

	        amazon_api_ajax_load_results(data);

			//console.log(term+' - '+locale);
		});

		// make our ajax call to do the amazon api call in php
		function amazon_api_ajax_load_results(data) {

			$.ajax({
	            type: 'POST',
	            url: ajaxurl,
	            data: data,
	            success: function(response) {
	                //console.log(response);
	                $('html, body').animate({
				        scrollTop: $("#galfram-amazon-api-results").offset().top - 50
				    }, 600);
	                $("#galfram-amazon-api-results").html(response);

	            },
	            complete: function(response){
	            	//console.log(response);

	            	$('.galfram-amazon-loading-overlay').fadeOut(100);
	            }
	        });

		}









		// make our ajax call to insert the chosen amazon product into the "chosen" box and save the info in the database for easy viewing in the admin.
		$('body').on( 'click', '.amazon-search-page', function(){

			$('.galfram-amazon-loading-overlay').fadeIn(100);

			var page = $(this).attr('data-api-page');
			var term = $(this).attr('data-api-term');
			var locale = $(this).attr('data-api-locale');
			var searchspecs = $(this).attr('data-api-specstype');

			var data = {
	            action: 'galfram_load_amazon_products_newpage_ajax',
	            term: term,
	            locale: locale,
	            page: page,
	            searchspecs: searchspecs
	        };

	    	amazon_api_ajax_load_new_page(data);

			//console.log(term+' - '+locale);
		});

		// make our ajax call to do the amazon api call in php
		function amazon_api_ajax_load_new_page(data) {

			$.ajax({
	            type: 'POST',
	            url: ajaxurl,
	            data: data,
	            success: function(response) {
	            	$('html, body').animate({
				        scrollTop: $("#galfram-amazon-api-results").offset().top - 50
				    }, 600);
	                $("#galfram-amazon-api-results-inner").html(response);
	            },
	            complete: function(response){

				    $('.amazon-search-page').removeClass('current');
				    $('.amazon-search-page[data-api-page="'+data.page+'"]').addClass('current');

				    $('.galfram-amazon-loading-overlay').fadeOut(100);
	            }
	        });

		}



		// insert our chosen amazon product into the "chosen box, and save the choice in the page meta data in the ajax php function
		$('body').on( 'click', '.insert-amazon-product', function(){

			$('.galfram-amazon-loading-overlay').fadeIn(100);


			var asin = $(this).attr('data-prod-asin');
			if ( asin === '' ) { asin = 'No Data'; }


			if ( $('body').hasClass('post-type-galfram_slider') ) {
	        	
	        	
	        	$('#'+repeater_row_id).attr('value', asin );
				//asin_field.val(asin);

				//console.log( asin+', '+asin_field );

	        	$('.galfram-amazon-loading-overlay').fadeOut(100);
				$('.tb-close-icon').click();
	        }
	        else {
	        	

					var specstype = $(this).attr('data-prod-specstype');

					var post_id = $('#post_ID').val();

					var prod_url = $(this).attr('data-prod-url');
					if ( prod_url === '' ) { prod_url = ''; }


					if ( specstype === 'amazon-only' ) {

						// var prod_url = $(this).attr('data-prod-url');
						// if ( prod_url === '' ) { prod_url = ''; }

						var name = $(this).attr('data-prod-name');
						if ( name === '' ) { name = 'No Data'; }

						var brand = $(this).attr('data-prod-brand');
						if ( brand === '' ) { brand = 'N/A'; }

						var model = $(this).attr('data-prod-model');
						if ( model === '' ) { model = 'N/A'; }

						var upc = $(this).attr('data-prod-upc');
						if ( upc ==='' ) { upc = 'N/A'; }

						var url = $(this).attr('data-prod-url');

						var image = $(this).attr('data-prod-image');

						var listprice = $(this).attr('data-prod-listprice');
						if ( listprice === '' ) { listprice = 'N/A'; }

						var listpricecurrency = $(this).attr('data-prod-listpricecurrencycode');

						var lowestnewprice = $(this).attr('data-prod-lowestnewprice');
						if ( lowestnewprice === '' ) { lowestnewprice = 'N/A'; }

						var lowestusedprice = $(this).attr('data-prod-lowestusedprice');
						if ( lowestusedprice === '' ) { lowestusedprice = 'N/A'; }

						var lowestnewpricecurrency = $(this).attr('data-prod-lowestnewpricecurrency');

						var lowestusedpricecurrency = $(this).attr('data-prod-lowestusedpricecurrency');

						var bestoffer = $(this).attr('data-prod-bestoffer');
						if ( bestoffer === '' ) { bestoffer = 'N/A'; }

						var amountsaved = $(this).attr('data-prod-amountsaved');
						if ( amountsaved === '' ) { amountsaved = 'N/A'; }

						var amountsavedcurrency = $(this).attr('data-prod-amountsavedcurrency');

						var amountsavespercentage = $(this).attr('data-prod-amountsavespercentage')+'%';
						if ( amountsavespercentage === '' ) { amountsavespercentage = ''; }

						var features = $(this).attr('data-prod-features');

						var warranty = $(this).attr('data-prod-warranty');
						if ( warranty === '' ) { warranty = 'N/A'; }

						var data = {
				            action: 'galfram_amazon_api_insert_chosen_ajax',
				            asin: asin,
				            post_id: post_id
				        };

				        console.log


				    	amazon_api_ajax_insert_product( data, asin, name, brand, model, upc, url, image, listprice, listpricecurrency, lowestnewprice, lowestusedprice, lowestnewpricecurrency, lowestusedpricecurrency, bestoffer, amountsaved, amountsavedcurrency, amountsavespercentage, features, warranty );

				    }

				    else if ( specstype === 'custom-specs' ) {
				    	
				    	var data = {
				            action: 'galfram_amazon_api_insert_chosen_customspecs_ajax',
				            asin: asin,
				            prod_url: prod_url,
				            post_id: post_id
				        };


				    	amazon_api_ajax_insert_product_customspecs( data, asin, prod_url );

				    }

			}

			//console.log(term+' - '+locale);
		});



		function amazon_api_ajax_insert_product( data, asin, name, brand, model, upc, url, image, listprice, listpricecurrency, lowestnewprice, lowestusedprice, lowestnewpricecurrency, lowestusedpricecurrency, bestoffer, amountsaved, amountsavedcurrency, amountsavespercentage, features, warranty ) {


			$.ajax({
	            type: 'POST',
	            url: ajaxurl,
	            data: data,
	            success: function(response) {
	                //console.log(response);
	                $('html, body').animate({
				        scrollTop: $("#galfram_amazon_api_metabox").offset().top - 50
				    }, 600);
	            },
	            complete: function(response){
	            	
	            	var chosen_box = $('#amazon-api-chosen-asin-inner');

	            	var chosen_prod_html = '';

	            	chosen_prod_html += '<img id="chosen-api-product-image" src="'+image+'" alt="" class="amazon-api-prod-img" />';

					chosen_prod_html += '<h3>Your chosen product for this product for this review is:</h3>';
					chosen_prod_html += '<a id="chosen-api-product-name" href="'+url+'" target="_blank">'+name+'</a>';
					chosen_prod_html += '<p id="chosen-api-product-asin"><strong>ASIN:</strong> <span>'+asin+'</span></p>';
					chosen_prod_html += '<p id="chosen-api-product-brand"><strong>Brand:</strong> <span">'+brand+'</span></p>';
					chosen_prod_html += '<p id="chosen-api-product-model"><strong>Model:</strong> '+model+'</p>';
					chosen_prod_html += '<p id="chosen-api-product-upc"><strong>UPC:</strong> '+upc+'</p>';
					chosen_prod_html += '<p id="chosen-api-product-lowestnewprice"><strong>Lowest New Price:</strong> <span>'+lowestnewprice+' '+lowestnewpricecurrency+'</span></p>';
					chosen_prod_html += '<p id="chosen-api-product-lowestusedprice"><strong>Lowest Used Price:</strong> '+lowestusedprice+' '+lowestusedpricecurrency+'</p>';
					chosen_prod_html += '<p id="chosen-api-product-listprice"><strong>List Price:</strong> <span>'+listprice+' '+listpricecurrency+'</span></p>';
					
					chosen_prod_html += '<p id="chosen-api-product-bestoffer"><strong>Best Offer:</strong> <span>'+bestoffer+' '+listpricecurrency+'</span></p>';
					
					chosen_prod_html += '<p><strong>Save:</strong> '+amountsaved+' '+amountsavedcurrency+' ('+amountsavespercentage+')</p>';
					
					chosen_prod_html += '<p><strong>Warranty:</strong> '+warranty+'</p>';
					chosen_prod_html += '<p class="features"><strong>Features:</strong><br />';
				    chosen_prod_html += '<ul>';

				    var obj = jQuery.parseJSON(features);
					$.each(obj, function(key,value) {
						chosen_prod_html += '<li>'+value+'</li>';
					}); 

				    chosen_prod_html += '</ul>';

					chosen_prod_html += '<p class="change-prod">(To change this product, search for a new one below.)</p>';

	            	chosen_box.html(chosen_prod_html);

				    $('.galfram-amazon-loading-overlay').fadeOut(100);

	            }
	        });


		}



		function amazon_api_ajax_insert_product_customspecs( data, asin, prod_url ) {

			$.ajax({
	            type: 'POST',
	            url: ajaxurl,
	            data: data,
	            success: function(response) {
	                console.log(response);
	            },
	            complete: function(response){

	            	//console.log(response);

	            	$('#acf-field_57f7b64e2ccad').val(asin);
	            	$('#acf-field_57c5da15bf817').val(prod_url);
				    $('.galfram-amazon-loading-overlay').fadeOut(100);
				    $('.tb-close-icon').click();

				    $('html, body').animate({
				        scrollTop: $("#acf-field_57c5da15bf817").offset().top - 120
				    }, 600);

	            }
	        });



		}







		// save amazon specs to display on review frontend
		$('body').on( 'click', '#galfram-save-amazon-spec-display-choices', function(){

			$('.galfram-amazon-loading-overlay').fadeIn(100);

			var post_id = $('#amazon-api-chosen-asin').attr('data-post-id');

			var displayed_fields = [];

			$('#choose-display-fields-checkboxes :checked').each(function() {
				displayed_fields.push($(this).val());
			});

			var data = {
	            action: 'galfram_save_amazon_specs_to_display_ajax',
	            displayed_fields: displayed_fields,
	            post_id: post_id
	        };

	        save_amazon_specs_to_display(data);

			//console.log(term+' - '+locale);
		});

		// make our ajax call to do the amazon api call in php
		function save_amazon_specs_to_display(data) {

			$.ajax({
	            type: 'POST',
	            url: ajaxurl,
	            data: data,
	            success: function(response) {
	                

	            },
	            complete: function(response){
	            	//console.log(response);

	            	$('.galfram-amazon-loading-overlay').fadeOut(100);
	            }
	        });

		}



		// let's hijack the enter button when the user has the amazon search box in focus
		$('#galfram-api-search-term').keypress(function(e){
			if(event.keyCode == 13){
				$('#galfram-api-search-submit').click();
				e.preventDefault();
			}
		});



		// set our sliders repeater row empty. we'll update it to grab later
		var repeater_row_id = '';
		

		// open the "custom specs" pull form amazon search box
		$('body').on( 'click', '.manual-search-amazon', function(){

			if ( $('body').hasClass('post-type-galfram_slider') ) {
				repeater_row_id = $(this).closest('.acf-field').find('input').attr('id');
				console.log('::: '+repeater_row_id);
			}

		});


		


		


		

		

	});


})( jQuery );
