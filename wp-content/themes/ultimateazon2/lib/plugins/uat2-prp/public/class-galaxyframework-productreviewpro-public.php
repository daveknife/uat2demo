<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    galfram_productreviewpro
 * @subpackage galfram_productreviewpro/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    galfram_productreviewpro
 * @subpackage galfram_productreviewpro/public
 * @author     Your Name <email@example.com>
 */
class galfram_productreviewpro_Public {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $galfram_productreviewpro    The ID of this plugin.
	 */
	private $galfram_productreviewpro;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $galfram_productreviewpro       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $galfram_productreviewpro, $version ) {

		$this->galfram_productreviewpro = $galfram_productreviewpro;
		$this->version = $version;

	}





	/**Set our custom loop to use with our post type galfram_review
	 *
	 * @since    1.0.0
	 */
	function get_galfram_review_post_type_template() {

		global $post;

		if ( is_singular('galfram_review') ) {

			remove_action( 'uat2_post_loop', 'uat2_post_loop_function', 10 );
			add_action( 'uat2_post_loop', 'galfram_review_loop_function', 10 );

		}

		function galfram_review_loop_function() {

			global $post;

			$chosen_specgroup = get_post_meta( $post->ID, 'prod_specs_group', true );
			
			if ( $chosen_specgroup != 'pull-from-amazon' ) :


					echo '<div class="content-inner-row" itemscope itemtype="http://schema.org/Review">';
						echo '<div class="content-inner-row-main">';
							do_action( 'galfram_before_loop' );

							if (have_posts()) : 
								while (have_posts()) : 
									the_post();
										?>
									<article id="post-<?php the_ID(); ?>" <?php post_class(''); ?> role="review">

										<header class="article-header">

											<h1 class="page-title" itemprop="itemReviewed"><?php the_title(); ?></h1>

											<?php
											$fname = get_the_author_meta('first_name');
											$lname = get_the_author_meta('last_name');

											echo '<meta itemprop="author" content="'.$fname.' '.$lname.'">';

											$prod_url = get_post_meta( get_the_ID(), 'manual_affiliate_link', true);

											$intro_enabled = get_post_meta( get_the_ID(), 'enable_intro_content', true);
											if ( $intro_enabled ) {
												$intro_content = get_post_meta( get_the_ID(), 'intro_content', true);
												echo '<div class="review-intro-content">'.apply_filters( 'the_content', $intro_content ).'</div>';
											}
											?>

											<div class="single-post-featured-image">

												<?php 
												if ( has_post_thumbnail() ) {

													$thumb_id = get_post_thumbnail_id();
													$thumb_url_array = wp_get_attachment_image_src($thumb_id, 'archive-default-featured-image', true);
													$thumb_url = $thumb_url_array[0];

													$thumb_url_array_sm = wp_get_attachment_image_src($thumb_id, 'archive-default-featured-image-sm', true);
													$thumb_url_sm = $thumb_url_array_sm[0];

												}
												else {


													$display_ama_images = get_post_meta( get_the_ID(), 'pull_images_from_amazon', true);

													if ( $display_ama_images ) {

														/****************************************
														*** Get our product info from the api ***
														****************************************/

														// http://webservices.amazon.com/scratchpad/index.html

														$chosen_review = get_post_meta( get_the_ID(), 'amazon_manual_product_asin', true);

														$galfram_prp_amazon_api = new galfram_productreviewpro_amazon_api();
														$api_response = $galfram_prp_amazon_api->amazon_api_request( get_the_ID(), 'Images,ItemAttributes,Offers,Reviews', $chosen_review );


														/*************************************
														*** Parse our Results form the api ***
														*************************************/


														if ( $api_response != 'No Credentials' ) {
															// convert xml response, we'll use lower while echoing out the custom amazpon specs data
															$xml_response = simplexml_load_string($api_response);

															// get api images
															$image_sets = $xml_response->Items->Item->ImageSets;

															//echo '<pre>'.print_r($image_sets,1).'</pre>';

															if ( $image_sets ) {

																$i=0;

																// loop through our results and parse
																foreach($image_sets as $image_set) {

																	if ($i==0) {

																		$j=0;

																		foreach($image_set as $set) {

																			if ($j==0) {
																				$thumb_url_sm = $set->SmallImage->URL;
																				$thumb_url = $set->LargeImage->URL;
																			}
																			$j++;
																				
																		}

																	}
																	$i++;

																}

																if (!$thumb_url_sm && !$thumb_url) {

																	$thumb_url = get_template_directory_uri().'/lib/images/default-featured-image.png';
																	$thumb_url_sm = get_template_directory_uri().'/lib/images/default-featured-image.png';
																}

																

															}
															else {
																$thumb_url = get_template_directory_uri().'/lib/images/default-featured-image.png';
																$thumb_url_sm = get_template_directory_uri().'/lib/images/default-featured-image.png';
															}
														}



													}
													else {

														if(get_field('default_featured_image', 'theme-options')) {
															$img = get_field('default_featured_image','theme-options');
															$img_array = wp_get_attachment_image_src( $img, 'archive-default-featured-image' );
															$thumb_url = $img_array[0];

															$thumb_url_array_sm = wp_get_attachment_image_src($img, 'archive-default-featured-image-sm', true);
															$thumb_url_sm = $thumb_url_array_sm[0];
														}
														else {
															$thumb_url = get_template_directory_uri().'/lib/images/default-featured-image.png';
															$thumb_url_sm = get_template_directory_uri().'/lib/images/default-featured-image.png';
														}

													}

													

													

												}
												?>

												<?php $images = get_post_meta( get_the_ID(), 'additional_images', true ); ?>

												<?php if ( $images || $image_sets ) : ?>
													<div id="single-post-featured-image-change" class="has-additional">
												<?php else : ?>
													<div id="single-post-featured-image-change">
												<?php endif; ?>

												

												<?php
												$remove_prod_link_global = get_option( 'extension-prp-settings_no_image_link', true ); 
												$remove_prod_link_post = get_post_meta( get_the_ID(), 'do_not_link_top_large_image', true );
												?>

												<?php 
												if ( $remove_prod_link_global == 'link-top-img' && !$remove_prod_link_post ) {
													echo '<a class="gtm-review-featured-img-link" href="'.$prod_url.'" target="_blank">';
												}
												else if ( $remove_prod_link_global == 'unlink-top-img' && $remove_prod_link_post ) {
													echo '<a class="gtm-review-featured-img-link" href="'.$prod_url.'" target="_blank">';
												}
												?>


													<img id="galfram-review-main-img" data-interchange="[<?php echo $thumb_url; ?>, small], [<?php echo $thumb_url; ?>, medium]" class="wp-post-image" alt="<?php the_title(); ?>" itemprop="image" />

												<?php 
												if ( $remove_prod_link_global == 'link-top-img' && !$remove_prod_link_post ) {
													echo '</a>';
												}
												else if ( $remove_prod_link_global == 'unlink-top-img' && $remove_prod_link_post ) {
													echo '</a>';
												}
												?>

													

												</div>


												<?php

												if ( $display_ama_images && $image_sets ) { ?>

													<div class="galfram-review-additional-images">

														<?php
														// loop through our results and parse
														foreach($image_sets as $image_set) {

															foreach($image_set as $set) {

																$thumb_url = $set->SmallImage->URL;
																$thumb_url_full = $set->LargeImage->URL;

																// NEED TO ADD ALT TAG IN STILL !!!

																// Build the image box
																echo '<div class="galfram-review-additional-img-thumb-box" data-thumb-full="'.$thumb_url_full.'">';
																	echo '<img class="galfram-review-additional-img-switch gtm-swap-review-img" src="'.$thumb_url.'" alt="" itemprop="image" />';
																echo '</div>';

															}

														} ?>
													</div>

													<?php
												}
												else {

													if( $images ) { ?>

														<div class="galfram-review-additional-images">

															<?php for( $i = 0; $i < $images; $i++ ) {

																$thumb_id = (int) get_post_meta( get_the_ID(), 'additional_images_' . $i . '_image', true );

																// Thumbnail field returns image ID, so grab image. If none provided, use default image?
																$thumb_url_array = wp_get_attachment_image_src($thumb_id, 'review-additional-image-thumb', true);
																$thumb_url = $thumb_url_array[0];

																$thumb_url_array_full = wp_get_attachment_image_src($thumb_id, 'large', true);
																$thumb_url_full = $thumb_url_array_full[0];

																// NEED TO ADD ALT TAG IN STILL !!!

																// Build the video box
																echo '<div class="galfram-review-additional-img-thumb-box" data-thumb-full="'.$thumb_url_full.'">';
																	echo '<img class="galfram-review-additional-img-switch gtm-swap-review-img" src="'.$thumb_url.'" alt="" />';
																echo '</div>';

															} ?>

														</div>

													<?php } ?>

												<?php } ?>

												<div class="clearfix"></div>

											</div>

										</header> <!-- end article header -->


										<?php 
										$remove_top_link_bar_global = get_option('extension-prp-settings_hide_top_link_bar', true); 
										$remove_bottom_link_bar_global = get_option('extension-prp-settings_hide_bottom_link_bar', true);

										$remove_top_link_bar_post = get_post_meta( get_the_ID(), 'hide_top_link_bar', true );
										$remove_bottom_link_bar_post = get_post_meta( get_the_ID(), 'hide_bottom_link_bar', true );

										$top_link_bar_text_global = get_option('extension-prp-settings_top_link_bar_text', true); 
										$bottom_link_bar_text_global = get_option('extension-prp-settings_bottom_link_bar_text', true); 

										$top_link_bar_text_post = get_post_meta( get_the_ID(), 'top_link_bar_text', true );
										if ( $top_link_bar_text_post != '' ) {
											$top_link_text = $top_link_bar_text_post;
										}
										else if ( $top_link_bar_text_post == '' && ( $top_link_bar_text_global != '' && $top_link_bar_text_global != 1 ) ) {
											$top_link_text = $top_link_bar_text_global;
										}
										else {
											$top_link_text = __('Buy on Amazon', 'ultimateazon2');
										}

										$bottom_link_bar_text_post = get_post_meta( get_the_ID(), 'bottom_link_bar_text', true );
										if ( $bottom_link_bar_text_post != '' ) {
											$bottom_link_text = $bottom_link_bar_text_post;
										}
										else if ( $bottom_link_bar_text_post == ''  && ( $bottom_link_bar_text_global != '' && $bottom_link_bar_text_global != 1 ) ) {
											$bottom_link_text = $bottom_link_bar_text_global;
										}
										else {
											$bottom_link_text = __('Buy on Amazon', 'ultimateazon2');
										}
										?>

										<?php
										$new_tab = get_option( 'extension-prp-settings_open_aff_links_in_new_window', true );
										if ( $new_tab == 1 ) { $new_tab_html = ' target="_blank"'; }
										else { $new_tab_html = ''; }

										$no_follow = get_option( 'extension-prp-settings_no-follow_affiliate_links', true );
										if ( $no_follow == 1 ) { $no_follow_html = ' rel="nofollow"'; }
										else { $no_follow_html = ''; }
										?>



										<?php 
										if ( $remove_top_link_bar_global == 'show-top-link-bar' && !$remove_top_link_bar_post ) {
											echo '<a href="'.$prod_url.'" class="button link-bar gtm-top-linkbar"'.$no_follow_html.$new_tab_html.'>'.$top_link_text.'</a>';
										}
										else if ( $remove_top_link_bar_global == 'hide-top-link-bar' && $remove_top_link_bar_post ) {
											echo '<a href="'.$prod_url.'" class="button link-bar gtm-top-linkbar"'.$no_follow_html.$new_tab_html.'>'.$top_link_text.'</a>';
										}
										?>
													
										<section class="entry-content" itemprop="reviewBody">
											<?php the_content(); ?>
										</section> <!-- end article section -->

										<?php 
										if ( $remove_bottom_link_bar_global == 'show-bottom-link-bar' && !$remove_bottom_link_bar_post ) {
											echo '<a href="'.$prod_url.'" class="button link-bar gtm-bottom-linkbar"'.$no_follow_html.$new_tab_html.'>'.$bottom_link_text.'</a>';
										}
										else if ( $remove_bottom_link_bar_global == 'hide-bottom-link-bar' && $remove_bottom_link_bar_post ) {
											echo '<a href="'.$prod_url.'" class="button link-bar gtm-bottom-linkbar"'.$no_follow_html.$new_tab_html.'>'.$bottom_link_text.'</a>';
										}
										?>

										<?php 
										$checked = get_field('hide_comments_pages', 'theme-options');
										if ( $checked != true ) :
											comments_template();
										endif;
										?>

									</article> <!-- end article -->

								<?php
								endwhile;
							else:
								galfram_missing_loop();
							endif;

							do_action( 'galfram_after_loop' );
						echo '</div>';
						?>
						

						<div id="sidebar1" class="sidebar" role="complementary">

						

							<?php 
							$galfram_prp_amazon_api = new galfram_productreviewpro_amazon_api();
							$get_custom_specs = $galfram_prp_amazon_api->get_custom_review_specs( get_the_id() );

							//print_r($get_custom_specs);

							$amazon_spec_present = $get_custom_specs['amazon_spec_present'];
							$specs_group_id = $get_custom_specs['specs_group_id'];
							$specs_array = $get_custom_specs['specs_array'];


							// if an amazon spec is present, let's make our api call
							if ( $amazon_spec_present ) {

								// get the ASIN saved to this post
								$chosen_review = trim( get_post_meta( get_the_ID(), 'amazon_manual_product_asin', true ) );

								$galfram_prp_amazon_api = new galfram_productreviewpro_amazon_api();
								$api_response = $galfram_prp_amazon_api->amazon_api_request( get_the_ID(), 'Images,ItemAttributes,Offers,Reviews', $chosen_review );

								/*************************************
								*** Parse our Results form the api ***
								*************************************/

								// convert xml response, we'll use lower while echoing out the custom amazpon specs data
								$arr = simplexml_load_string($api_response);


							}


							echo '<div class="product-specs-group">';

								echo '<h2>'.__('Specifications', 'ultimateazon2').'</h2>';

								echo '<div class="specs-table">';

									foreach ( $specs_array as  $spec ) {

										$spec_id = $specs_group_id.'_'.$spec['spec_id'];
										$spec_type = $spec['spec_type'];
										$spec_title = $spec['spec_title'];
										$spec_title_hide = $spec['spec_title_hide'];

										switch ( $spec_type ) {

											case 'text' :

												echo '<div class="specs-table-row spec-text">';
													if ( !$spec_title_hide ) {
														echo '<strong>'.$spec_title.':</strong> ';
													}
													echo '<span>'.get_post_meta( get_the_ID(), $spec_id , true).'</span>';
												echo '</div>';

												break;


											case 'p-text' :

												echo '<div class="specs-table-row spec-p-text">';
													if ( !$spec_title_hide ) {
														echo '<strong>'.$spec_title.':</strong><br />';
													}
													echo '<span>'.get_post_meta( get_the_ID(), $spec_id , true).'</span>';
												echo '</div>';

												break;

											case 'star-rating' :

												$editor_rating = get_post_meta( get_the_ID(), $spec_id , true);

												if ( $editor_rating && $editor_rating != 'no-rating' ) {

													$editor_rating_num = (float) $editor_rating;

													$galfram_prp_amazon_api = new galfram_productreviewpro_amazon_api();
													$stars = $galfram_prp_amazon_api->get_star_rating_html( $editor_rating_num );


													if ( $stars != '' ) {

														echo '<div class="specs-table-row editor-rating custom-rating spec-star-rating" itemprop="reviewRating" itemscope itemtype="http://schema.org/Rating">';
															echo '<span><strong>'.__('Editor\'s Rating', 'ultimateazon2').': <span itemprop="ratingValue">'.$editor_rating.'</span> '.__('stars', 'ultimateazon2').'</strong> '.$stars.'</span>';

															$fname = get_the_author_meta('first_name');
															$lname = get_the_author_meta('last_name');

															echo '<meta itemprop="author" content="'.$fname.' '.$lname.'">';
															echo '<meta itemprop="bestRating" content="5">';
															echo '<meta itemprop="worstRating" content="0">';

														echo '</div>';

													}

												}


												break;

											case 'yes-no' :

												$yesno = get_post_meta( get_the_ID(), $spec_id , true);

												echo '<div class="specs-table-row yes-no spec-yes-no">';

													echo '<strong>'.$spec_title.':</strong> ';
													
													if ( $yesno == 'yes' ) {
														echo '<i class="fa fa-check"></i>';
													}
													elseif ( $yesno == 'no' ) {
														echo '<i class="fa fa-close"></i>';
													}

												echo '</div>';

												break;

											case 'image' :

												$thumb_id = get_post_meta( get_the_ID(), $spec_id , true);
												$thumb_url_array = wp_get_attachment_image_src($thumb_id, 'large', true);
												$thumb_url = $thumb_url_array[0];

												echo '<div class="specs-table-row spec-image">';
													// if ( !$spec_title_hide ) {
													// 	echo '<strong>'.$spec_title.':</strong><br />';
													// }
													echo '<img class="galfram-review-spec-img" src="'.$thumb_url.'" alt="" />';
												echo '</div>';

												break;


											case 'price' :


												$price_fields = trim( get_option( '' ) );

												$listprice = get_post_meta( get_the_ID(), $spec_id.'_listprice' , true);
												$saleprice = get_post_meta( get_the_ID(), $spec_id.'_saleprice' , true);
												$percentagesaved = get_post_meta( get_the_ID(), $spec_id.'_percentagediscounted' , true);
												$amountsaved = get_post_meta( get_the_ID(), $spec_id.'_amountdiscounted' , true);

												echo '<div class="specs-table-row price spec-price" itemprop="offers" itemscope itemtype="http://schema.org/Offer">';

													// if data for list price or lowest new price is set to display
													if ( $listprice != '' && $saleprice != '' ) {


														// display the slashed out list price and the lower new price
														echo '<span class="slashed-price"><span>'.$listprice.'</span></span>';
														echo '<br />';
														echo '<span class="deal-price" itemprop="price">'.$saleprice.'</span>';



														if ( $percentagesaved != '' && $amountsaved != '' ) {
															echo '<span class="prod-amount-saved">Save '.$percentagesaved.' ('.$amountsaved.')</span>';
														}
														else if ( $percentagesaved != '' && $amountsaved == '' ) {
															echo '<span class="prod-amount-saved">Save '.$percentagesaved.'</span>';
														}
														else if ( $percentagesaved == '' && $amountsaved != '' ) {
															echo '<span class="prod-amount-saved">Save '.$amountsaved.'</span>';
														}


													}

													// if list price data is set to display, but lowest new price is not
													else if ( $listprice != '' && $saleprice == '' ) {

														// display only the list price
														echo '<span class="deal-price" itemprop="price">'.$listprice.'</span>';

													}
													// if list price data is NOT set to display, but lowest new price is
													else if ( $listprice == '' && $saleprice != '' ) {

														// display only the lowest new price
														echo '<span class="deal-price" itemprop="price">'.$saleprice.'</span>';

													}


												echo '</div>';


												break;


											case 'spec-button' :


												echo '<div class="specs-table-row spec-button">';
													$public_this = new galfram_productreviewpro_amazon_api();
													echo $public_this->galfram_get_spec_button_html( get_the_ID(), $spec_id );
												echo '</div>';

												break;




											case 'spec-shortcode' :

												echo '<div class="specs-table-row spec-shortcode">';
													$spec_shortcode = get_post_meta( get_the_ID(), $spec_id , true);
													echo do_shortcode($spec_shortcode);
												echo '</div>';

												break;



											case 'amazon' :

												$ama_spec = $spec['spec_amazon_field'];


												if ( $arr->Items->Request->IsValid != 'True' ) {
													echo '<p class="amazon-api-failed-request">'.__('There was a problem retreiving the data from Amazon', 'ultimateazon2').'.</p>';
													return;
												}



												else if ( $ama_spec == 'asin' ) {

													$spec_val = $arr->Items->Item->ASIN;

													echo '<div class="specs-table-row spec-asin">';
														if ( !$spec_title_hide ) {
															echo '<strong>'.__('ASIN', 'ultimateazon2').':</strong> ';
														}
														echo '<span>'.$spec_val.'</span>';
													echo '</div>';

												}

												else if ( $ama_spec == 'brand' ) {

													$spec_val = $arr->Items->Item->ItemAttributes->Brand;

													echo '<div class="specs-table-row spec-brand">';
														if ( !$spec_title_hide ) {
															echo '<strong>'.__('Brand', 'ultimateazon2').':</strong> ';
														}
														echo '<span>'.$spec_val.'</span>';
													echo '</div>';

												}

												else if ( $ama_spec == 'model' ) {

													$spec_val = $arr->Items->Item->ItemAttributes->Model;

													echo '<div class="specs-table-row spec-model">';
														if ( !$spec_title_hide ) {
															echo '<strong>'.__('Model', 'ultimateazon2').':</strong> ';
														}
														echo '<span>'.$spec_val.'</span>';
													echo '</div>';

												}

												else if ( $ama_spec == 'upc' ) {

													$spec_val = $arr->Items->Item->ItemAttributes->UPC;

													echo '<div class="specs-table-row spec-upc">';
														if ( !$spec_title_hide ) {
															echo '<strong>'.__('UPC', 'ultimateazon2').':</strong> ';
														}
														echo '<span>'.$spec_val.'</span>';
													echo '</div>';

												}

												else if ( $ama_spec == 'features' ) {

													$prod_features_array = $arr->Items->Item->ItemAttributes->Feature;

													echo '<div class="specs-table-row features spec-features">';

														if ( $prod_features_array == '' && !$spec_title_hide ) { echo '<p><strong>'.__('Featutes', 'ultimateazon2').':</strong> <span>'.__('N/A', 'ultimateazon2').'</span></p>'; }
												    	else {
													    	echo '<p class="features"><strong>'.__('Features', 'ultimateazon2').':</strong><br />';
													    	echo '<ul id="chosen-api-product-features">';

														    	foreach ( $prod_features_array as $feature ) {
														    		echo '<li>'.$feature.'</li>';
														    	}

													    	echo '</ul>';
													    }

												    echo '</div>';

												}

												else if ( $ama_spec == 'warranty' ) {

													$spec_val = $arr->Items->Item->ItemAttributes->Warranty;

													echo '<div class="specs-table-row spec-warranty">';
														if ( !$spec_title_hide ) {
															echo '<strong>'.__('Warranty', 'ultimateazon2').':</strong> ';
														}
														echo '<span>'.$spec_val.'</span>';
													echo '</div>';

												}

												else if ( $ama_spec == 'price' ) {

													$ama_price_fields = $spec['spec_amazon_price_fields'];

													//print_r($ama_price_fields);

													//echo '<pre>'.print_r($arr,1).'</pre>';

													$prod_formatted_price = $arr->Items->Item->ItemAttributes->ListPrice->FormattedPrice;
													$prod_listprice_currencycode = $arr->Items->Item->ItemAttributes->ListPrice->CurrencyCode;
												    $prod_price_offer = $arr->Items->Item->Offers->Offer->OfferListing->SalePrice->FormattedPrice;
												    $prod_amount_saved = $arr->Items->Item->Offers->Offer->OfferListing->AmountSaved->FormattedPrice;
												    $prod_amount_saved_currency = $arr->Items->Item->Offers->Offer->OfferListing->AmountSaved->CurrencyCode;
												    $prod_percentage_saved = $arr->Items->Item->Offers->Offer->OfferListing->PercentageSaved;


													//if data for list price or lowest new price is set to display
													if ( in_array( 'listprice', $ama_price_fields ) || in_array( 'discountprice', $ama_price_fields ) ) {

														// if list price data is set to display, and lowest new price is set to diaplay also
														if ( in_array( 'listprice', $ama_price_fields ) && in_array( 'discountprice', $ama_price_fields ) ) {

															// display the slashed out list price and the lower new price
															echo '<div class="specs-table-row price spec-price" itemprop="offers" itemscope itemtype="http://schema.org/Offer">';

																if ($prod_formatted_price && $prod_price_offer ) {
																	echo '<span class="slashed-price"><span>'.$prod_formatted_price.' '.$prod_listprice_currencycode.'</span></span>';
																	echo '<br />';
																	echo '<span class="deal-price" itemprop="price">'.$prod_price_offer.' '.$prod_listprice_currencycode.'</span>';
																}
																else {
																	echo '<span class="deal-price" itemprop="price">'.$prod_formatted_price.' '.$prod_listprice_currencycode.'</span>';
																}
																	

																if ( in_array( 'amountsaved', $ama_price_fields ) ) {

																	if ( $prod_percentage_saved != '' && $prod_amount_saved != '' ) {
																		echo '<span class="prod-amount-saved">Save '.$prod_percentage_saved.'% ('.$prod_amount_saved.')</span>';
																	}
																	else if ( $prod_percentage_saved != '' && $prod_amount_saved == '' ) {
																		echo '<span class="prod-amount-saved">'.__('Save', 'ultimateazon2').' '.$prod_percentage_saved.'%</span>';
																	}
																	else if ( $prod_percentage_saved == '' && $prod_amount_saved != '' ) {
																		echo '<span class="prod-amount-saved">'.__('Save', 'ultimateazon2').' '.$prod_amount_saved.'</span>';
																	}

																	

																}

															echo '</div>';

														}

														// if list price data is set to display, but lowest new price is not
														else if ( in_array( 'listprice', $ama_price_fields ) && !in_array( 'discountprice', $ama_price_fields ) ) {

															// display only the list price
															echo '<div class="specs-table-row price spec-lisprice">';
																echo '<span class="deal-price" itemprop="price">'.$prod_formatted_price.' '.$prod_listprice_currencycode.'</span>';
															echo '</div>';

														}
														// if list price data is NOT set to display, but lowest new price is
														else if ( !in_array( 'listprice', $ama_price_fields ) && in_array( 'discountprice', $ama_price_fields ) ) {

															// display only the lowest new price
															echo '<div class="specs-table-row price spec-listprice" itemprop="offers" itemscope itemtype="http://schema.org/Offer">';
																echo '<span class="deal-price" itemprop="price">'.$prod_price_offer.' '.$prod_listprice_currencycode.'</span>';
															echo '</div>';

														}


													}


												}

												else if ( $ama_spec == 'lowest-new-price' ) {

													$spec_val = $arr->Items->Item->OfferSummary->LowestNewPrice->FormattedPrice;
													$spec_val_cur = $arr->Items->Item->OfferSummary->LowestNewPrice->CurrencyCode;

													echo '<div class="specs-table-row spec-lowest-new-price">';
														if ( !$spec_title_hide ) {
															echo '<strong>'.__('Lowest New Price', 'ultimateazon2').':</strong> ';
														}
														echo '<span>'.$spec_val.' '.$spec_val_cur.'</span>';
													echo '</div>';

												}

												else if ( $ama_spec == 'lowest-used-price' ) {

													$spec_val = $arr->Items->Item->OfferSummary->LowestUsedPrice->FormattedPrice;
													$spec_val_cur = $arr->Items->Item->OfferSummary->LowestUsedPrice->CurrencyCode;

													echo '<div class="specs-table-row spec-lowest-used-price">';
														if ( !$spec_title_hide ) {
															echo '<strong>'.__('Lowest Used Price', 'ultimateazon2').':</strong> ';
														}
														echo '<span>'.$spec_val.' '.$spec_val_cur.'</span>';
													echo '</div>';

												}


												else if ( $ama_spec == 'small-image' ) {

													$thumb_url = $image_sets = $arr->Items->Item->ImageSets->ImageSet->SmallImage->URL;

													// $thumb_url = $image_sets = $arr->Items->Item->ImageSets;
													// echo '<pre>'.print_r($thumb_url,1).'</pre>';

													echo '<div class="specs-table-row spec-image spec-image-sm">';
														// if ( !$spec_title_hide ) {
														// 	echo '<strong>'.$spec_title.':</strong><br />';
														// }
														echo '<img class="galfram-review-spec-img" src="'.$thumb_url.'" alt="" />';
													echo '</div>';

												}

												else if ( $ama_spec == 'medium-image' ) {

													$thumb_url = $image_sets = $arr->Items->Item->ImageSets->ImageSet->MediumImage->URL;

													echo '<div class="specs-table-row spec-image spec-image-md">';
														// if ( !$spec_title_hide ) {
														// 	echo '<strong>'.$spec_title.':</strong><br />';
														// }
														echo '<img class="galfram-review-spec-img" src="'.$thumb_url.'" alt="" />';
													echo '</div>';

												}

												else if ( $ama_spec == 'large-image' ) {

													$thumb_url = $image_sets = $arr->Items->Item->ImageSets->ImageSet->LargeImage->URL;

													echo '<div class="specs-table-row spec-image spec-image-lg">';
														// if ( !$spec_title_hide ) {
														// 	echo '<strong>'.$spec_title.':</strong><br />';
														// }
														echo '<img class="galfram-review-spec-img" src="'.$thumb_url.'" alt="" />';
													echo '</div>';

												}



												break;

										} // end switch ( $spec_type )



									}



									$hide_sb_btn_global = get_option( 'extension-prp-settings_hide_sidebar_prod_link_btn', true );
									$hide_sb_btn_post = get_post_meta( get_the_ID(), 'hide_sidebar_product_link_button', true );


									$txt_prod_link_global = get_option( 'extension-prp-settings_sidebar_product_button_text', true ); 
									$txt_prod_link_post = get_post_meta( get_the_ID(), 'aff_button_sidebar_text', true );

									$new_tab = get_option( 'extension-prp-settings_open_aff_links_in_new_window', true );
									if ( $new_tab == 1 ) { $new_tab_html = ' target="_blank"'; }
									else { $new_tab_html = ''; }

									$no_follow = get_option( 'extension-prp-settings_no-follow_affiliate_links', true );
									if ( $no_follow == 1 ) { $no_follow_html = ' rel="nofollow"'; }
									else { $no_follow_html = ''; }

									if ( $txt_prod_link_post != '' ) { $btn_txt = $txt_prod_link_post; }
									else if ( $txt_prod_link_global != '' ) { $btn_txt = $txt_prod_link_global; }
									else { $btn_txt = __('Buy on Amazon', 'ultimateazon2' ); }

									if ( $hide_sb_btn_global == 'show-sidebar-btn' && !$hide_sb_btn_post ) {
										echo '<a class="button buy-on-amazon gtm-sidebar-ama-btn" href="'.$prod_url.'"'.$no_follow_html.$new_tab_html.'>'.$btn_txt.'</a>';
									}
									else if ( $hide_sb_btn_global == 'hide-sidebar-btn' && $hide_sb_btn_post )  {
										echo '<a class="button buy-on-amazon gtm-sidebar-ama-btn" href="'.$prod_url.'"'.$no_follow_html.$new_tab_html.'>'.$btn_txt.'</a>';
									}


								echo '</div>';

							echo '</div>';


							?>

						</div>

					<?php
					echo '</div>'; // end .content-inner-row











			elseif ( $chosen_specgroup == 'pull-from-amazon' ) : 









					/****************************************
					*** Get our product info from the api ***
					****************************************/

					// http://webservices.amazon.com/scratchpad/index.html

					$chosen_review = get_post_meta( get_the_ID(), 'galfram_chosen_amazon_prod', true);

					$galfram_prp_amazon_api = new galfram_productreviewpro_amazon_api();
					$api_response = $galfram_prp_amazon_api->amazon_api_request( get_the_ID(), 'Images,ItemAttributes,Offers,Reviews', $chosen_review );

					/*************************************
					*** Parse our Results form the api ***
					*************************************/


					if ( $api_response != 'No Credentials' ) {
						// convert xml response, we'll use lower while echoing out the custom amazpon specs data
						$xml_response = simplexml_load_string($api_response);
					}
					


					echo '<div class="content-inner-row" itemscope itemtype="http://schema.org/Review">';
						echo '<div class="content-inner-row-main">';
							do_action( 'galfram_before_loop' );

							if (have_posts()) : 
								while (have_posts()) : 
									the_post();
										?>
									<article id="post-<?php the_ID(); ?>" <?php post_class(''); ?> role="review">

										<header class="article-header">

											<h1 class="page-title" itemprop="itemReviewed"><?php the_title(); ?></h1>

											<?php 

											$fname = get_the_author_meta('first_name');
											$lname = get_the_author_meta('last_name');

											echo '<meta itemprop="author" content="'.$fname.' '.$lname.'">';

											$intro_enabled = get_post_meta( get_the_ID(), 'enable_intro_content', true);
											if ( $intro_enabled ) {
												$intro_content = get_post_meta( get_the_ID(), 'intro_content', true);
												echo '<div class="review-intro-content">'.apply_filters( 'the_content', $intro_content ).'</div>';
											}
											?>

											<div class="galfram-review-images">

												<div class="galfram-review-additional-images">

													<?php

													$chosen_specs = get_post_meta( get_the_ID(), 'galfram_chosen_amazon_prod_specs_display', true);

													if ( is_array($chosen_specs) && in_array( 'add-imgs', $chosen_specs ) ) {

														// get api images
														$image_sets = $xml_response->Items->Item->ImageSets;

														//echo '<pre>'.print_r($image_sets,1).'</pre>';

														if ( $image_sets ) {

															// loop through our results and parse
															foreach($image_sets as $image_set) {

																foreach($image_set as $set) {

																	$thumb_url = $set->SmallImage->URL;
																	$thumb_url_full = $set->LargeImage->URL;

																	// NEED TO ADD ALT TAG IN STILL !!!

																	// Build the image box
																	echo '<div class="galfram-review-additional-img-thumb-box" data-thumb-full="'.$thumb_url_full.'">';
																		echo '<img class="galfram-review-additional-img-switch gtm-swap-review-img" src="'.$thumb_url.'" alt="" itemprop="image" />';
																	echo '</div>';

																}

															}

														}
														else {
															echo 'There was a problem retreiving the data from Amazon.';
														}

													}

													else {

														$images = get_post_meta( get_the_ID(), 'additional_images', true );

														if( $images ) {

															for( $i = 0; $i < $images; $i++ ) {

																$thumb_id = (int) get_post_meta( get_the_ID(), 'additional_images_' . $i . '_image', true );

																// Thumbnail field returns image ID, so grab image. If none provided, use default image?
																$thumb_url_array = wp_get_attachment_image_src($thumb_id, 'review-additional-image-thumb', true);
																$thumb_url = $thumb_url_array[0];

																$thumb_url_array_full = wp_get_attachment_image_src($thumb_id, 'large', true);
																$thumb_url_full = $thumb_url_array_full[0];

																// NEED TO ADD ALT TAG IN STILL !!!

																// Build the image box
																echo '<div class="galfram-review-additional-img-thumb-box" data-thumb-full="'.$thumb_url_full.'">';
																	echo '<img class="galfram-review-additional-img-switch gtm-swap-review-img" src="'.$thumb_url.'" alt="" />';
																echo '</div>';

															}
														}

													} // end if ( in_array( 'add-imgs', $chosen_specs ) )
													?>

													<div class="clearfix"></div>

												</div>


												<?php 

												//$chosen_specs = get_post_meta( get_the_ID(), 'galfram_chosen_amazon_prod_specs_display', true);

												if ( is_array($chosen_specs) && in_array( 'main-img', $chosen_specs ) ) {

													$thumb_url = $xml_response->Items->Item->LargeImage->URL;
												    if ( $thumb_url == '' ) {
												    	$image_set = $xml_response->Items->Item->ImageSets;
												    	if ( is_array( $image_set ) ) {
												    		$thumb_url = $xml_response->Items->Item->ImageSets->ImageSet[0]->LargeImage->URL;
												    	}
												    	else {
												    		$thumb_url = $xml_response->Items->Item->ImageSets->ImageSet->LargeImage->URL;
												    	}
												    }

													$thumb_url_sm = $xml_response->Items->Item->LargeImage->URL;
													if ( $thumb_url_sm == '' ) {
												    	$image_set = $xml_response->Items->Item->ImageSets;
												    	if ( is_array( $image_set ) ) {
												    		$thumb_url_sm = $xml_response->Items->Item->ImageSets->ImageSet[0]->LargeImage->URL;
												    	}
												    	else {
												    		$thumb_url_sm = $xml_response->Items->Item->ImageSets->ImageSet->LargeImage->URL;
												    	}
												    }

												}

												else {

													if ( has_post_thumbnail() ) {

														$thumb_id = get_post_thumbnail_id();
														$thumb_url_array = wp_get_attachment_image_src($thumb_id, 'archive-default-featured-image', true);
														$thumb_url = $thumb_url_array[0];

														$thumb_url_array_sm = wp_get_attachment_image_src($thumb_id, 'archive-default-featured-image-sm', true);
														$thumb_url_sm = $thumb_url_array_sm[0];

													}
													else {

														if(get_field('default_featured_image', 'theme-options')) {
															$img = get_field('default_featured_image','theme-options');
															$img_array = wp_get_attachment_image_src( $img, 'archive-default-featured-image' );
															$thumb_url = $img_array[0];

															$thumb_url_array_sm = wp_get_attachment_image_src($img, 'archive-default-featured-image-sm', true);
															$thumb_url_sm = $thumb_url_array_sm[0];
														}
														else {
															$thumb_url = get_template_directory_uri().'/lib/images/default-featured-image.png';
															$thumb_url_sm = get_template_directory_uri().'/lib/images/default-featured-image.png';
														}

													}

												}

												?>

												<?php 
												$remove_prod_link_global = get_option( 'extension-prp-settings_no_image_link', true ); 
												$remove_prod_link_post = get_post_meta( get_the_ID(), 'do_not_link_top_large_image', true );
												$prod_url = $xml_response->Items->Item->DetailPageURL;
												?>

												
												<div id="single-post-featured-image-change">

													<?php 
													if ( $remove_prod_link_global == 'link-top-img' && !$remove_prod_link_post ) {
														echo '<a class="gtm-review-featured-img-link" href="'.$prod_url.'" target="_blank">';
													}
													else if ( $remove_prod_link_global == 'unlink-top-img' && $remove_prod_link_post ) {
														echo '<a class="gtm-review-featured-img-link" href="'.$prod_url.'" target="_blank">';
													}
													?>


														<img id="galfram-review-main-img" data-interchange="[<?php echo $thumb_url; ?>, small], [<?php echo $thumb_url; ?>, medium]" class="wp-post-image" alt="<?php the_title(); ?>">

													<?php 
													if ( $remove_prod_link_global == 'link-top-img' && !$remove_prod_link_post ) {
														echo '</a>';
													}
													else if ( $remove_prod_link_global == 'unlink-top-img' && $remove_prod_link_post ) {
														echo '</a>';
													}
													?>


												</div>

												

												<div class="clearfix"></div>


											</div> <!-- end .galfram-review-images -->

											
											

										</header> <!-- end article header -->
													

										<?php 
										$remove_top_link_bar_global = get_option('extension-prp-settings_hide_top_link_bar', true); 
										$remove_bottom_link_bar_global = get_option('extension-prp-settings_hide_bottom_link_bar', true);

										$remove_top_link_bar_post = get_post_meta( get_the_ID(), 'hide_top_link_bar', true );
										$remove_bottom_link_bar_post = get_post_meta( get_the_ID(), 'hide_bottom_link_bar', true );

										$top_link_bar_text_global = get_option('extension-prp-settings_top_link_bar_text', true); 
										$bottom_link_bar_text_global = get_option('extension-prp-settings_bottom_link_bar_text', true); 

										$top_link_bar_text_post = get_post_meta( get_the_ID(), 'top_link_bar_text', true );
										if ( $top_link_bar_text_post != '' ) {
											$top_link_text = $top_link_bar_text_post;
										}
										else if ( $top_link_bar_text_post == '' && ( $top_link_bar_text_global != '' && $top_link_bar_text_global != 1 ) ) {
											$top_link_text = $top_link_bar_text_global;
										}
										else {
											$top_link_text = __('Buy on Amazon', 'ultimateazon2');
										}

										$bottom_link_bar_text_post = get_post_meta( get_the_ID(), 'bottom_link_bar_text', true );
										if ( $bottom_link_bar_text_post != '' ) {
											$bottom_link_text = $bottom_link_bar_text_post;
										}
										else if ( $bottom_link_bar_text_post == ''  && ( $bottom_link_bar_text_global != '' && $bottom_link_bar_text_global != 1 ) ) {
											$bottom_link_text = $bottom_link_bar_text_global;
										}
										else {
											$bottom_link_text = __('Buy on Amazon', 'ultimateazon2');
										}
										?>



										<?php 
										if ( $remove_top_link_bar_global == 'show-top-link-bar' && !$remove_top_link_bar_post ) {
											echo '<a href="'.$prod_url.'" class="button link-bar gtm-top-linkbar" target="_blank">'.$top_link_text.'</a>';
										}
										else if ( $remove_top_link_bar_global == 'hide-top-link-bar' && $remove_top_link_bar_post ) {
											echo '<a href="'.$prod_url.'" class="button link-bar gtm-top-linkbar" target="_blank">'.$top_link_text.'</a>';
										}
										?>

										<section class="entry-content" itemprop="reviewBody">
											<?php the_content(); ?>
										</section> <!-- end article section -->

										<?php 
										if ( $remove_bottom_link_bar_global == 'show-bottom-link-bar' && !$remove_bottom_link_bar_post ) {
											echo '<a href="'.$prod_url.'" class="button link-bar gtm-bottom-linkbar" target="_blank">'.$bottom_link_text.'</a>';
										}
										else if ( $remove_bottom_link_bar_global == 'hide-bottom-link-bar' && $remove_bottom_link_bar_post ) {
											echo '<a href="'.$prod_url.'" class="button link-bar gtm-bottom-linkbar" target="_blank">'.$bottom_link_text.'</a>';
										}
										?>


										<?php 
										$checked = get_field('hide_comments_pages', 'theme-options');
										if ( $checked != true ) :
											comments_template();
										endif;
										?>

									</article> <!-- end article -->

								<?php
								endwhile;
							else:
								galfram_missing_loop();
							endif;

							do_action( 'galfram_after_loop' );
						echo '</div>';
						?>



						<div id="sidebar1" class="sidebar" role="complementary">

						<?php

						if ( !$xml_response ) {
							echo '<p class="amazon-api-failed-request">'.__('There was a problem retreiving the data from Amazon. You may need to check your API keys', 'ultimateazon2').'.</p>';
						}
						else if ( $xml_response->Items->Request->IsValid != 'True' ) {
							echo '<p class="amazon-api-failed-request">'.__('There was a problem retreiving the data from Amazon', 'ultimateazon2').'.</p>';
						}

						else { // search for ifxmlresponse to find closing tag 

							$get_amazon_review_specs = new galfram_productreviewpro_amazon_api();
							$amazon_spec_val = $get_amazon_review_specs->get_amazon_review_specs( $xml_response );


							echo '<div class="product-specs-group amazon-only-specs">';

								echo '<h2>'.__('Specifications', 'ultimateazon2').'</h2>';

								//echo '<pre>'.print_r($chosen_specs,1).'</pre>';

								echo '<div class="specs-table">';

									if ( in_array( 'asin', $chosen_specs ) && $amazon_spec_val['prod_asin'] != '' ) {

										echo '<div class="specs-table-row spec-asin">';
											echo '<strong>'.__('ASIN', 'ultimateazon2').':</strong> <span>'.$amazon_spec_val['prod_asin'].'</span>';
										echo '</div>';

									}

									if ( in_array( 'brand', $chosen_specs ) && $amazon_spec_val['prod_brand'] != '' ) {

										echo '<div class="specs-table-row spec-brand">';
											echo '<strong>'.__('Brand', 'ultimateazon2').':</strong> <span>'.$amazon_spec_val['prod_brand'].'</span>';
										echo '</div>';

									}

									if ( in_array( 'model', $chosen_specs ) && $amazon_spec_val['prod_model'] != '' ) {

										echo '<div class="specs-table-row spec-model">';
											echo '<strong>'.__('Model', 'ultimateazon2').':</strong> <span>'.$amazon_spec_val['prod_model'].'</span>';
										echo '</div>';

									}

									if ( in_array( 'upc', $chosen_specs ) && $amazon_spec_val['prod_upc'] != '' ) {

										echo '<div class="specs-table-row spec-upc">';
											echo '<strong>'.__('UPC', 'ultimateazon2').':</strong> <span>'.$amazon_spec_val['prod_upc'].'</span>';
										echo '</div>';

									}

									if ( in_array( 'warranty', $chosen_specs ) && $amazon_spec_val['prod_warranty'] != '' ) {

										echo '<div class="specs-table-row spec-warranty">';
											echo '<strong>'.__('Warranty', 'ultimateazon2').':</strong> <span>'.$amazon_spec_val['prod_warranty'].'</span>';
										echo '</div>';

									}

									if ( in_array( 'lowest-used-price', $chosen_specs ) && $amazon_spec_val['prod_lowest_used_price'] != '' ) {

										echo '<div class="specs-table-row spec-lowest-used-price">';
											echo '<strong>'.__('Lowest Used Price', 'ultimateazon2').':</strong> <span>'.$amazon_spec_val['prod_lowest_used_price'].' '.$amazon_spec_val['prod_lowest_used_price_currency'].'</span>';
										echo '</div>';

									}

									if ( in_array( 'lowest-new-price', $chosen_specs ) && $amazon_spec_val['prod_lowest_used_price'] != '' ) {

										echo '<div class="specs-table-row spec-lowest-new-price">';
											echo '<strong>'.__('Lowest New Price', 'ultimateazon2').':</strong> <span>'.$amazon_spec_val['prod_lowest_new_price'].' '.$amazon_spec_val['prod_lowest_new_price_currency'].'</span>';
										echo '</div>';

									}

									$editor_rating = get_post_meta( get_the_ID(), 'editor_rating', true );

									if ( $editor_rating && $editor_rating != 'no-rating' ) {

										$editor_rating_num = (float) $editor_rating;

										$galfram_prp_amazon_api = new galfram_productreviewpro_amazon_api();
										$stars = $galfram_prp_amazon_api->get_star_rating_html( $editor_rating_num );


										if ( $stars != '' ) {

											echo '<div class="specs-table-row editor-rating custom-rating spec-star-rating" itemprop="reviewRating" itemscope itemtype="http://schema.org/Rating">';
												echo '<span><strong>'.__('Editor\'s Rating', 'ultimateazon2').': <span itemprop="ratingValue">'.$editor_rating.'</span> '.__('stars', 'ultimateazon2').'</strong> '.$stars.'</span>';

												$fname = get_the_author_meta('first_name');
												$lname = get_the_author_meta('last_name');

												echo '<meta itemprop="author" content="'.$fname.' '.$lname.'">';
												echo '<meta itemprop="bestRating" content="5">';
												echo '<meta itemprop="worstRating" content="0">';

											echo '</div>';

										}

									}

									// if data for list price or lowest new price is set to display
									if ( in_array( 'list-price', $chosen_specs ) || in_array( 'price-offer', $chosen_specs ) ) {

										// if list price data is set to display, and lowest new price is set to diaplay also
										if ( in_array( 'list-price', $chosen_specs ) && in_array( 'price-offer', $chosen_specs ) ) {

											// display the slashed out list price and the lower new price
											echo '<div class="specs-table-row price spec-listprice" itemprop="offers" itemscope itemtype="http://schema.org/Offer">';
												echo '<span class="slashed-price"><span>'.$amazon_spec_val['prod_formatted_price'].' '.$amazon_spec_val['prod_listprice_currencycode'].'</span></span>';
												echo '<br />';
												echo '<span class="deal-price" itemprop="price">'.$amazon_spec_val['prod_price_offer'].' '.$amazon_spec_val['prod_listprice_currencycode'].'</span>';

												if ( in_array( 'amount-saved', $chosen_specs ) ) {

													if ( $amazon_spec_val['prod_percentage_saved'] != '' && $amazon_spec_val['prod_amount_saved'] != '' ) {
														echo '<span class="prod-amount-saved">'.__('Save', 'ultimateazon2').' '.$amazon_spec_val['prod_percentage_saved'].'% ('.$amazon_spec_val['prod_amount_saved'].')</span>';
													}
													else if ( $amazon_spec_val['prod_percentage_saved'] != '' && $amazon_spec_val['prod_amount_saved'] == '' ) {
														echo '<span class="prod-amount-saved">'.__('Save', 'ultimateazon2').' '.$amazon_spec_val['prod_percentage_saved'].'%</span>';
													}
													else if ( $amazon_spec_val['prod_percentage_saved'] == '' && $amazon_spec_val['prod_amount_saved'] != '' ) {
														echo '<span class="prod-amount-saved">'.__('Save', 'ultimateazon2').' '.$amazon_spec_val['prod_amount_saved'].'</span>';
													}

													

												}

											echo '</div>';

										}

										// if list price data is set to display, but lowest new price is not
										else if ( in_array( 'list-price', $chosen_specs ) && !in_array( 'price-offer', $chosen_specs ) ) {

											// display only the list price
											echo '<div class="specs-table-row price spec-listprice">';
												echo '<span class="deal-price" itemprop="price">'.$amazon_spec_val['prod_formatted_price'].' '.$amazon_spec_val['prod_listprice_currencycode'].'</span>';
											echo '</div>';

										}
										// if list price data is NOT set to display, but lowest new price is
										else if ( !in_array( 'list-price', $chosen_specs ) && in_array( 'price-offer', $chosen_specs ) ) {

											// display only the lowest new price
											echo '<div class="specs-table-row price spec-listprice" itemprop="offers" itemscope itemtype="http://schema.org/Offer">';
												echo '<span class="deal-price" itemprop="price">'.$xamazon_spec_val['prod_price_offer'].' '.$amazon_spec_val['prod_listprice_currencycode'].'</span>';
											echo '</div>';

										}


									}


									$hide_sb_btn_global = get_option( 'extension-prp-settings_hide_sidebar_prod_link_btn', true );
									$hide_sb_btn_post = get_post_meta( get_the_ID(), 'hide_sidebar_product_link_button', true );

									$txt_prod_link_global = get_option( 'extension-prp-settings_sidebar_product_button_text', true ); 
									$txt_prod_link_post = get_post_meta( get_the_ID(), 'aff_button_sidebar_text', true );

									if ( $txt_prod_link_post != '' ) { $btn_txt = $txt_prod_link_post; }
									else if ( $txt_prod_link_global != '' ) { $btn_txt = $txt_prod_link_global; }
									else { $btn_txt = __('Buy on Amazon', 'ultimateazon2' ); }

									$new_tab = get_option( 'extension-prp-settings_open_aff_links_in_new_window', true );
									if ( $new_tab == 1 ) { $new_tab_html = ' target="_blank"'; }
									else { $new_tab_html = ''; }

									$no_follow = get_option( 'extension-prp-settings_no-follow_affiliate_links', true );
									if ( $no_follow == 1 ) { $no_follow_html = ' rel="nofollow"'; }
									else { $no_follow_html = ''; }

									if ( $hide_sb_btn_global == 'show-sidebar-btn' && !$hide_sb_btn_post ) {
										echo '<a class="button buy-on-amazon gtm-sidebar-ama-btn" href="'.$prod_url.'"'.$no_follow_html.$new_tab_html.'>'.$btn_txt.'</a>';
									}
									else if ( $hide_sb_btn_global == 'hide-sidebar-btn' && $hide_sb_btn_post )  {
										echo '<a class="button buy-on-amazon gtm-sidebar-ama-btn" href="'.$prod_url.'"'.$no_follow_html.$new_tab_html.'>'.$btn_txt.'</a>';
									}

									

									if ( in_array( 'features', $chosen_specs ) && $amazon_spec_val['prod_features_array'] != '' ) {

										echo '<div class="specs-table-row features spec-features">';
										echo '<h5>'.__('Features', 'ultimateazon2').'</h5>';
									    	echo '<ul id="chosen-api-product-features" itemprop="description">';

										    	foreach ( $amazon_spec_val['prod_features_array'] as $feature ) {
										    		echo '<li>'.$feature.'</li>';
										    	}

									    	echo '</ul>';
								    	echo '</div>';

								    }


								echo '</div>';


							echo '</div>';



							?>

							
							</div>

						<?php } // END ifxmlresponse ?>

					<?php
					echo '</div>'; // end .content-inner-row



			endif;

			
		} // end function galfram_review_loop_function()



	}

















	// new hijacked loop for the standard archive loop
	public function galfram_review_standard_archive() {

		global $post;

		if ( is_post_type_archive() == 'galfram_review' || is_tax( 'galfram_review_cat' ) ) {
			remove_action('uat2_archive_standard_loop', 'uat2_archive_standard_loop_function', 10);
			add_action('uat2_archive_standard_loop', 'galfram_prp_archive_loop_function', 10);
		}

		function galfram_prp_archive_loop_function() {
			
				uat2_archive_title();

				uat2_before_archive_loop();

					if (have_posts()) : $prod_count=1;
						while (have_posts()) : 
							the_post();
							?>

							<article id="post-<?php the_ID(); ?>" <?php post_class(''); ?> role="article" itemscope itemtype="http://schema.org/Review">	

								<?php 
								if ( has_post_thumbnail() ) {

									$thumb_id = get_post_thumbnail_id();
									$thumb_url_array = wp_get_attachment_image_src($thumb_id, 'archive-default-featured-image', true);
									$thumb_url = $thumb_url_array[0];

									$thumb_url_array_sm = wp_get_attachment_image_src($thumb_id, 'archive-default-featured-image-sm', true);
									$thumb_url_sm = $thumb_url_array_sm[0];

								}
								else {


									$chosen_specgroup = get_post_meta( get_the_ID(), 'prod_specs_group', true );

			    					if ( $chosen_specgroup == 'pull-from-amazon' ) {

			    						$chosen_review = get_post_meta( get_the_ID(), 'galfram_chosen_amazon_prod', true);

										$galfram_prp_amazon_api = new galfram_productreviewpro_amazon_api();
										$api_response = $galfram_prp_amazon_api->amazon_api_request( get_the_ID(), 'Images,ItemAttributes,Offers,Reviews', $chosen_review );

										/*************************************
										*** Parse our Results form the api ***
										*************************************/

										// convert xml response, we'll use lower while echoing out the custom amazpon specs data
										$xml_response = simplexml_load_string($api_response);

										$prod_url = $xml_response->Items->Item->DetailPageURL;

										$thumb_url = $xml_response->Items->Item->LargeImage->URL;
									    if ( $thumb_url == '' ) {
									    	$image_set = $xml_response->Items->Item->ImageSets;
									    	if ( is_array( $image_set ) ) {
									    		$thumb_url = $xml_response->Items->Item->ImageSets->ImageSet[0]->LargeImage->URL;
									    	}
									    	else {
									    		$thumb_url = $xml_response->Items->Item->ImageSets->ImageSet->LargeImage->URL;
									    	}
									    }

										$thumb_url_sm = $xml_response->Items->Item->LargeImage->URL;
										if ( $thumb_url_sm == '' ) {
									    	$image_set = $xml_response->Items->Item->ImageSets;
									    	if ( is_array( $image_set ) ) {
									    		$thumb_url_sm = $xml_response->Items->Item->ImageSets->ImageSet[0]->LargeImage->URL;
									    	}
									    	else {
									    		$thumb_url_sm = $xml_response->Items->Item->ImageSets->ImageSet->LargeImage->URL;
									    	}
									    }
			    					}

			    					else {

										$display_ama_images = get_post_meta( get_the_ID(), 'pull_images_from_amazon', true);

										if ( $display_ama_images ) {

											/****************************************
											*** Get our product info from the api ***
											****************************************/

											// http://webservices.amazon.com/scratchpad/index.html

											$chosen_review = get_post_meta( get_the_ID(), 'amazon_manual_product_asin', true);

											$galfram_prp_amazon_api = new galfram_productreviewpro_amazon_api();
											$api_response = $galfram_prp_amazon_api->amazon_api_request( get_the_ID(), 'Images,ItemAttributes,Offers,Reviews', $chosen_review );


											/*************************************
											*** Parse our Results form the api ***
											*************************************/


											if ( $api_response != 'No Credentials' ) {
												// convert xml response, we'll use lower while echoing out the custom amazpon specs data
												$xml_response = simplexml_load_string($api_response);

												// get api images
												$image_sets = $xml_response->Items->Item->ImageSets;

												//echo '<pre>'.print_r($image_sets,1).'</pre>';

												if ( $image_sets ) {

													$i=0;

													// loop through our results and parse
													foreach($image_sets as $image_set) {

														if ($i==0) {

															$j=0;

															foreach($image_set as $set) {

																if ($j==0) {
																	$thumb_url_sm = $set->SmallImage->URL;
																	$thumb_url = $set->LargeImage->URL;
																}
																$j++;
																	
															}

														}
														$i++;

													}

													if (!$thumb_url_sm && !$thumb_url) {

														$thumb_url = get_template_directory_uri().'/lib/images/default-featured-image.png';
														$thumb_url_sm = get_template_directory_uri().'/lib/images/default-featured-image.png';
													}

													

												}
												else {
													$thumb_url = get_template_directory_uri().'/lib/images/default-featured-image.png';
													$thumb_url_sm = get_template_directory_uri().'/lib/images/default-featured-image.png';
												}
											}



										}
										else {

											if(get_field('default_featured_image', 'theme-options')) {
												$img = get_field('default_featured_image','theme-options');
												$img_array = wp_get_attachment_image_src( $img, 'archive-default-featured-image' );
												$thumb_url = $img_array[0];

												$thumb_url_array_sm = wp_get_attachment_image_src($img, 'archive-default-featured-image-sm', true);
												$thumb_url_sm = $thumb_url_array_sm[0];
											}
											else {
												$thumb_url = get_template_directory_uri().'/lib/images/default-featured-image.png';
												$thumb_url_sm = get_template_directory_uri().'/lib/images/default-featured-image.png';
											}


										} // END if ( $display_ama_images )

									} // END if ( $chosen_specgroup != 'pull-from-amazon' )

								}
								?>

								<div class="archive-post-featured-image">

									<img data-interchange="[<?php echo $thumb_url; ?>, small], [<?php echo $thumb_url; ?>, medium]" class="wp-post-image galfram-review-main-img" alt="<?php the_title(); ?>" itemprop="image" />

								</div>				


								<section class="entry-content">

									<h2 itemprop="itemReviewed"><a class="gtm-review-title" href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h2>
									
									<?php
									$review_post_id = get_the_ID();
									$fname = get_the_author_meta('first_name');
									$lname = get_the_author_meta('last_name');

									echo '<meta itemprop="author" content="'.$fname.' '.$lname.'">';
									?>

									<?php
									$args = array('orderby' => 'name', 'order' => 'ASC', 'fields' => 'all');
									$terms = wp_get_post_terms( get_the_ID(), 'galfram_review_cat', $args ); 
									?>

									


									<?php 
									global $post;
									if ( $post->post_excerpt ) :
										echo '<p class="post-archive-excerpt" itemprop="articleBody">'.get_the_excerpt();
									echo ' <a href="'.get_the_permalink().'"><button class="tiny">'. __( 'Read more...', 'ultimateazon2' ) .'</button></a></p>';
									elseif ( get_field('display_post_excerpt', 'theme-options' ) ) :

										if(strstr($post->post_content,'<!--more-->')) : 
											echo '<div class="more-excerpt" itemprop="articleBody">';
												the_content('<button class="tiny">' . __( 'Read more...', 'ultimateazon2' ) . '</button>'); 
											echo '</div>';
										else: 
											echo get_the_excerpt();
										endif;

									else:
										if(strstr($post->post_content,'<!--more-->')) : echo '<div class="more-excerpt">'; endif;
										the_content('<button class="tiny">' . __( 'Read more...', 'ultimateazon2' ) . '</button>'); 
										if(strstr($post->post_content,'<!--more-->')) : echo '</div>'; endif;
									endif;
									?>



									<?php if ( $terms ) : ?>
										<p class="byline categories">
											<span class="categories-title"><?php _e('Categories','ultimateazon2'); ?>:</span>
											<?php foreach ( $terms as $term ) : ?>
												<a class="gtm-category-tag" href="/review-category/<?php echo $term->slug; ?>/"><?php echo $term->name; ?></a>
											<?php endforeach; ?>
										</p>
									<?php endif; ?>



									<?php
									$hide_quickview = get_option( 'extension-prp-settings_disable_quick_view', true );
									?>

									<?php if ( !$hide_quickview ) : ?>

										<button class="md-trigger galfram-quickview-trigger gtm-quickview-trigger" data-modal="galfram-quickview-modal" data-galfram-id="<?php echo $review_post_id; ?>"><?php _e('Quick View', 'ultimateazon2'); ?></button>

									<?php endif; ?>


								</section> <!-- end article section -->

								

							</article>

						<?php
						$prod_count++;
						endwhile;
						uat2_page_navi();
					else:
						uat2_missing_loop();
					endif;

				uat2_after_archive_loop();

		}

	}





	// new hijacked loop for the grid archive loop
	public function galfram_review_grid_archive() {

		global $post;

		if ( is_post_type_archive() == 'galfram_review' || is_tax( 'galfram_review_cat' ) ) {
			remove_action('uat2_archive_grid_loop', 'uat2_archive_grid_loop_function', 10);
			add_action('uat2_archive_grid_loop', 'galfram_prp_archive_loop_grid_function', 10);
		}

		function galfram_prp_archive_loop_grid_function() {
			

			uat2_archive_title();

			uat2_before_archive_loop();

			echo '<div class="row archive-grid" data-equalizer="review-archive-box" data-equalize-on="medium" data-equalize-by-row="true"> <!--Begin Row:-->';

			if (have_posts()) : 
				$i = 0;
				while (have_posts()) : 
					the_post();

					$blog_page_type = get_blog_page_type();
					$grid_cols = $blog_page_type['grid_cols'];
					$grid_cols_tablet = $blog_page_type['grid_cols_tablet'];
					?>

					<!--Item: -->
					<div class="panel column <?php echo $grid_cols.' '.$grid_cols_tablet; ?>">
					
						<article id="post-<?php the_ID(); ?>" <?php post_class(''); ?> role="article" data-equalizer-watch="review-archive-box" itemscope itemtype="http://schema.org/Review">					
								<header class="article-header">

									<?php 
									if ( has_post_thumbnail() ) {

										$thumb_id = get_post_thumbnail_id();
										$thumb_url_array = wp_get_attachment_image_src($thumb_id, 'archive-default-featured-image', true);
										$thumb_url = $thumb_url_array[0];

										$thumb_url_array_sm = wp_get_attachment_image_src($thumb_id, 'archive-default-featured-image-sm', true);
										$thumb_url_sm = $thumb_url_array_sm[0];

									}
									else {


										$display_ama_images = get_post_meta( get_the_ID(), 'pull_images_from_amazon', true);

										if ( $display_ama_images ) {

											/****************************************
											*** Get our product info from the api ***
											****************************************/

											// http://webservices.amazon.com/scratchpad/index.html

											$chosen_review = get_post_meta( get_the_ID(), 'amazon_manual_product_asin', true);

											$galfram_prp_amazon_api = new galfram_productreviewpro_amazon_api();
											$api_response = $galfram_prp_amazon_api->amazon_api_request( get_the_ID(), 'Images,ItemAttributes,Offers,Reviews', $chosen_review );


											/*************************************
											*** Parse our Results form the api ***
											*************************************/


											if ( $api_response != 'No Credentials' ) {
												// convert xml response, we'll use lower while echoing out the custom amazpon specs data
												$xml_response = simplexml_load_string($api_response);

												// get api images
												$image_sets = $xml_response->Items->Item->ImageSets;

												//echo '<pre>'.print_r($image_sets,1).'</pre>';

												if ( $image_sets ) {

													$i=0;

													// loop through our results and parse
													foreach($image_sets as $image_set) {

														if ($i==0) {

															$j=0;

															foreach($image_set as $set) {

																if ($j==0) {
																	$thumb_url_sm = $set->SmallImage->URL;
																	$thumb_url = $set->LargeImage->URL;
																}
																$j++;
																	
															}

														}
														$i++;

													}

													if (!$thumb_url_sm && !$thumb_url) {

														$thumb_url = get_template_directory_uri().'/lib/images/default-featured-image.png';
														$thumb_url_sm = get_template_directory_uri().'/lib/images/default-featured-image.png';
													}

													

												}
												else {
													$thumb_url = get_template_directory_uri().'/lib/images/default-featured-image.png';
													$thumb_url_sm = get_template_directory_uri().'/lib/images/default-featured-image.png';
												}
											}



										}
										else {

											if(get_field('default_featured_image', 'theme-options')) {
												$img = get_field('default_featured_image','theme-options');
												$img_array = wp_get_attachment_image_src( $img, 'archive-default-featured-image' );
												$thumb_url = $img_array[0];

												$thumb_url_array_sm = wp_get_attachment_image_src($img, 'archive-default-featured-image-sm', true);
												$thumb_url_sm = $thumb_url_array_sm[0];
											}
											else {
												$thumb_url = get_template_directory_uri().'/lib/images/default-featured-image.png';
												$thumb_url_sm = get_template_directory_uri().'/lib/images/default-featured-image.png';
											}


										}

									}
									?>

									<div class="archive-post-featured-image">

										<img class="galfram-review-main-img" data-interchange="[<?php echo $thumb_url; ?>, small], [<?php echo $thumb_url; ?>, medium]" class="wp-post-image" alt="<?php the_title(); ?>" itemprop="image" />

									</div>

									<h2 itemprop="itemReviewed"><a class="gtm-review-title" href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h2>
									
									<?php
									$review_post_id = get_the_ID();
									$fname = get_the_author_meta('first_name');
									$lname = get_the_author_meta('last_name');

									echo '<meta itemprop="author" content="'.$fname.' '.$lname.'">';
									?>

									<div class="clearfix"></div>

								</header>

								<section class="entry-content">

									<?php
									$hide_quickview = get_option( 'extension-prp-settings_disable_quick_view', true );
									?>

									<?php if ( !$hide_quickview ) : ?>

										<button class="md-trigger galfram-quickview-trigger gtm-quickview-trigger" data-modal="galfram-quickview-modal" data-galfram-id="<?php echo $review_post_id; ?>"><?php _e('Quick View', 'ultimateazon2'); ?></button>

									<?php endif; ?>

								</section> <!-- end article section -->

								<?php 
								$args = array('orderby' => 'name', 'order' => 'ASC', 'fields' => 'all');
								$terms = wp_get_post_terms( get_the_ID(), 'galfram_review_cat', $args ); 
								?>

								<?php if ( $terms ) : ?>
									<footer class="article-footer">
										<p class="byline categories">
											<span class="categories-title"><?php _e('Categories','ultimateazon2'); ?>:</span>
											<?php foreach ( $terms as $term ) : ?>
												<a class="gtm-category-tag" href="/review-category/<?php echo $term->slug; ?>/"><?php echo $term->name; ?></a>
											<?php endforeach; ?>
										</p>
									</footer> <!-- end article footer -->
								<?php endif; ?>

							</article>
			
					</div>

					<?php
					$i++;
				endwhile;
				echo '</div>  <!--End Row: --> ';
				uat2_page_navi();
			else:
				uat2_missing_loop();
			endif;

			uat2_after_archive_loop();



		}

	}


	public function galfram_quickview_modal() {

		echo '<div class="md-modal md-effect-8" id="galfram-quickview-modal">
			<div class="md-content">
			</div>
		</div>';

		echo '<div id="prp-page-overlay" class="prp-page-overlay"></div>';

	}



	public function galfram_wp_ajax_url() {
		echo '<script type="text/javascript"> var ajaxurl = "'.admin_url('admin-ajax.php').'";</script>';
	}






	public function galfram_quickview_modal_getinfo() {


		global $wpdb; // this is how you get access to the database

	    $galfram_review_id = $_POST['galfram_review_id'];

	    $galfram_review_post = get_post($galfram_review_id);
	    $galfram_post_ID = $galfram_review_post->ID;

	    


			    $chosen_specgroup = get_post_meta( $galfram_post_ID, 'prod_specs_group', true );

			    if ( $chosen_specgroup != 'pull-from-amazon' ) :

			    	$response = '';

					$response .= '<div class="galfram-quickview-top">';

						$response  .= '<h3>'.$galfram_review_post->post_title.'</h3>';

			    		$prod_url = get_post_meta( $galfram_post_ID, 'manual_affiliate_link', true);

			    		$remove_prod_img_top = get_option( 'extension-prp-settings_disable_top_prod_img', true ); 


			    			if ( !$remove_prod_img_top ) {
							
									if ( has_post_thumbnail( $galfram_post_ID ) ) {

										$thumb_id = get_post_thumbnail_id($galfram_post_ID);
										$thumb_url_array = wp_get_attachment_image_src($thumb_id, 'archive-default-featured-image', true);
										$thumb_url = $thumb_url_array[0];

										$thumb_url_array_sm = wp_get_attachment_image_src($thumb_id, 'archive-default-featured-image-sm', true);
										$thumb_url_sm = $thumb_url_array_sm[0];

									}
									else {

										$display_ama_images = get_post_meta( $galfram_post_ID, 'pull_images_from_amazon', true);

										if ( $display_ama_images ) {

											/****************************************
											*** Get our product info from the api ***
											****************************************/

											// http://webservices.amazon.com/scratchpad/index.html

											$chosen_review = get_post_meta( $galfram_post_ID, 'amazon_manual_product_asin', true);

											$galfram_prp_amazon_api = new galfram_productreviewpro_amazon_api();
											$api_response = $galfram_prp_amazon_api->amazon_api_request( $galfram_post_ID, 'Images,ItemAttributes,Offers,Reviews', $chosen_review );


											/*************************************
											*** Parse our Results form the api ***
											*************************************/


											if ( $api_response != 'No Credentials' ) {
												// convert xml response, we'll use lower while echoing out the custom amazpon specs data
												$xml_response = simplexml_load_string($api_response);

												// get api images
												$image_sets = $xml_response->Items->Item->ImageSets;

												//echo '<pre>'.print_r($image_sets,1).'</pre>';

												if ( $image_sets ) {

													$i=0;

													// loop through our results and parse
													foreach($image_sets as $image_set) {

														if ($i==0) {

															$j=0;

															foreach($image_set as $set) {

																if ($j==0) {
																	$thumb_url_sm = $set->LargeImage->URL;
																	$thumb_url = $set->LargeImage->URL;
																}
																$j++;
																	
															}

														}
														$i++;

													}

													if (!$thumb_url_sm && !$thumb_url) {

														$thumb_url = get_template_directory_uri().'/lib/images/default-featured-image.png';
														$thumb_url_sm = get_template_directory_uri().'/lib/images/default-featured-image.png';
													}

													

												}
												else {
													$thumb_url = get_template_directory_uri().'/lib/images/default-featured-image.png';
													$thumb_url_sm = get_template_directory_uri().'/lib/images/default-featured-image.png';
												}
											}



										}
										else {


											if(get_field('default_featured_image', 'theme-options')) {
												$img = get_field('default_featured_image','theme-options');
												$img_array = wp_get_attachment_image_src( $img, 'archive-default-featured-image' );
												$thumb_url = $img_array[0];

												$thumb_url_array_sm = wp_get_attachment_image_src($img, 'archive-default-featured-image-sm', true);
												$thumb_url_sm = $thumb_url_array_sm[0];
											}
											else {
												$thumb_url = get_template_directory_uri().'/lib/images/default-featured-image.png';
												$thumb_url_sm = get_template_directory_uri().'/lib/images/default-featured-image.png';
											}

										}

									}


									$response .= '<div class="galfram-quickview-img">';


										$remove_prod_link_top = get_option( 'extension-prp-settings_disable_top_prod_img_link', true ); 
										
										if ( !$remove_prod_link_top ) {
											$response .= '<a class="gtm-quickview-top-imglink" href="'.$prod_url.'" target="_blank">';
										}

											$response .= '<img id="galfram-quickview-img" src="'.$thumb_url_sm.'" class="wp-post-image" alt="'.$galfram_review_post->post_title.'" itemprop="image" />';

										
										if ( !$remove_prod_link_top ) {
											$response .= '</a>';
										}


										
									$response .= '</div>';

							}


							$hide_review_link = get_option( 'extension-prp-settings_disable_full_review_btn', true );
							$hide_aff_link = get_option( 'extension-prp-settings_disable_affiliate_btn', true );

							$new_tab = get_option( 'extension-prp-settings_open_aff_links_in_new_window', true );
							if ( $new_tab == 1 ) { $new_tab_html = ' target="_blank"'; }
							else { $new_tab_html = ''; }

							$no_follow = get_option( 'extension-prp-settings_no-follow_affiliate_links', true );
							if ( $no_follow == 1 ) { $no_follow_html = ' rel="nofollow"'; }
							else { $no_follow_html = ''; }

							if ( !$hide_review_link && !$hide_aff_link ) {
								$response .= '<a class="button buy-on-amazon top-btn gtm-quickview-top-afflink" href="'.$prod_url.'"'.$no_follow_html.$new_tab_html.'><span>Buy on Amazon</span></a>';
								$response .= '<a class="button full-galfram-review gtm-quickview-top-fulllink" href="'.get_the_permalink($galfram_post_ID).'">Full Review</a>';
							}
							elseif ( !$hide_review_link && $hide_aff_link ) {
								$response .= '<a class="button full-galfram-review review-only gtm-quickview-top-fulllink" href="'.get_the_permalink($galfram_post_ID).'">Full Review</a>';
							}
							elseif ( $hide_review_link && !$hide_aff_link ) {
								$response .= '<a class="button buy-on-amazon top-btn aff-only gtm-quickview-top-afflink" href="'.$prod_url.'"'.$no_follow_html.$new_tab_html.'><span>Buy on Amazon</span></a>';
							}
							

							

							$response .= '<div class="galfram-clear"></div>';

						$response .= '</div>';

						

						$response .= '<div class="md-content-inner">';



						$hide_specs = get_option( 'extension-prp-settings_disable_prod_specs', true );

						if ( !$hide_specs ) :



								$response .= '<div id="sidebar1" class="sidebar" role="complementary">';

								

								$galfram_prp_amazon_api = new galfram_productreviewpro_amazon_api();
								$get_custom_specs = $galfram_prp_amazon_api->get_custom_review_specs( $galfram_post_ID );

								//print_r($get_custom_specs);

								$amazon_spec_present = $get_custom_specs['amazon_spec_present'];
								$specs_group_id = $get_custom_specs['specs_group_id'];
								$specs_array = $get_custom_specs['specs_array'];



								// if an amazon spec is present, let's make our api call
								if ( $amazon_spec_present ) {

									$chosen_review = trim( get_post_meta( $galfram_review_id, 'amazon_manual_product_asin', true ) );

									$galfram_prp_amazon_api = new galfram_productreviewpro_amazon_api();
									$api_response = $galfram_prp_amazon_api->amazon_api_request( $galfram_review_id, 'Images,ItemAttributes,Offers', $chosen_review );

									/*************************************
									*** Parse our Results form the api ***
									*************************************/

									// convert xml response, we'll use lower while echoing out the custom amazpon specs data
									$arr = simplexml_load_string($api_response);


									
								}


								$response .= '<div class="product-specs-group">';

									$response .= '<h2>'.__('Specifications', 'ultimateazon2').'</h2>';

									$response .= '<div class="specs-table">';

										foreach ( $specs_array as  $spec ) {

											$spec_id = $specs_group_id.'_'.$spec['spec_id'];
											$spec_type = $spec['spec_type'];
											$spec_title = $spec['spec_title'];
											$spec_title_hide = $spec['spec_title_hide'];

											switch ( $spec_type ) {

												case 'text' :

													$response .= '<div class="specs-table-row spec-text">';
														if ( !$spec_title_hide ) {
															$response .= '<strong>'.$spec_title.':</strong> ';
														}
														$response .= '<span>'.get_post_meta( $galfram_post_ID, $spec_id , true).'</span>';
													$response .= '</div>';

													break;


												case 'p-text' :

													$response .= '<div class="specs-table-row spec-p-text">';
														if ( !$spec_title_hide ) {
															$response .= '<strong>'.$spec_title.':</strong><br />';
														}
														$response .= '<span>'.get_post_meta( $galfram_post_ID, $spec_id , true).'</span>';
													$response .= '</div>';

													break;

												case 'star-rating' :

													$editor_rating = get_post_meta( $galfram_post_ID, $spec_id , true);


													if ( $editor_rating && $editor_rating != 'no-rating' ) {

														$editor_rating_num = (float) $editor_rating;

														$galfram_prp_amazon_api = new galfram_productreviewpro_amazon_api();
														$stars = $galfram_prp_amazon_api->get_star_rating_html( $editor_rating_num );

														

														if ( $stars != '' ) {

															$response .= '<div class="specs-table-row editor-rating custom-rating spec-star-rating" itemprop="reviewRating" itemscope itemtype="http://schema.org/Rating">';
																$response .= '<span><strong>'.$spec_title.': <span itemprop="ratingValue">'.$editor_rating.'</span> '.__('stars', 'ultimateazon2').'</strong> '.$stars.'</span>';

																$fname = get_the_author_meta('first_name');
																$lname = get_the_author_meta('last_name');

																$response .= '<meta itemprop="author" content="'.$fname.' '.$lname.'">';
																$response .= '<meta itemprop="bestRating" content="5">';
																$response .= '<meta itemprop="worstRating" content="0">';

															$response .= '</div>';

														}

														

													}


													break;

												case 'yes-no' :

													$yesno = get_post_meta( $galfram_post_ID, $spec_id , true);

													$response .= '<div class="specs-table-row yes-no spec-yes-no">';

														$response .= '<strong>'.$spec_title.':</strong> ';
														
														if ( $yesno == 'yes' ) {
															$response .= '<i class="fa fa-check"></i>';
														}
														elseif ( $yesno == 'no' ) {
															$response .= '<i class="fa fa-close"></i>';
														}

													$response .= '</div>';

													break;

												case 'image' :

													$thumb_id = get_post_meta( $galfram_post_ID, $spec_id , true);
													$thumb_url_array = wp_get_attachment_image_src($thumb_id, 'large', true);
													$thumb_url = $thumb_url_array[0];

													$image_alt = get_post_meta( $thumb_id, '_wp_attachment_image_alt', true);

													$response .= '<div class="specs-table-row spec-image">';
														if ( !$spec_title_hide ) {
															$response .= '<strong>'.$spec_title.':</strong><br />';
														}
														$response .= '<img class="galfram-review-spec-img" src="'.$thumb_url.'" alt="'.$image_alt.'" />';
													$response .= '</div>';

													break;


												case 'price' :


													$price_fields = trim( get_option( '' ) );

													$listprice = get_post_meta( $galfram_post_ID, $spec_id.'_listprice' , true);
													$saleprice = get_post_meta( $galfram_post_ID, $spec_id.'_saleprice' , true);
													$percentagesaved = get_post_meta( $galfram_post_ID, $spec_id.'_percentagediscounted' , true);
													$amountsaved = get_post_meta( $galfram_post_ID, $spec_id.'_amountdiscounted' , true);

													$response .= '<div class="specs-table-row price spec-price" itemprop="offers" itemscope itemtype="http://schema.org/Offer">';

														// if data for list price or lowest new price is set to display
														if ( $listprice != '' && $saleprice != '' ) {

															if (  $listprice != $saleprice ) {

																// display the slashed out list price and the lower new price
																$response .= '<span class="slashed-price"><span>'.$listprice.'</span></span>';
																$response .= '<br />';
																$response .= '<span class="deal-price" itemprop="price">'.$saleprice.'</span>';

															}
															else {
																$response .= '<span class="deal-price" itemprop="price">'.$listprice.'</span>';
															}


															if ( $percentagesaved != '' && $amountsaved != '' ) {
																$response .= '<span class="prod-amount-saved">'.__('Save', 'ultimateazon2').' '.$percentagesaved.' ('.$amountsaved.')</span>';
															}
															else if ( $percentagesaved != '' && $amountsaved == '' ) {
																$response .= '<span class="prod-amount-saved">'.__('Save', 'ultimateazon2').' '.$percentagesaved.'</span>';
															}
															else if ( $percentagesaved == '' && $amountsaved != '' ) {
																$response .= '<span class="prod-amount-saved">'.__('Save', 'ultimateazon2').' '.$amountsaved.'</span>';
															}


														}

														// if list price data is set to display, but lowest new price is not
														else if ( $listprice != '' && $saleprice == '' ) {

															// display only the list price
															$response .= '<span class="deal-price" itemprop="price">'.$listprice.'</span>';

														}
														// if list price data is NOT set to display, but lowest new price is
														else if ( $listprice == '' && $saleprice != '' ) {

															// display only the lowest new price
															$response .= '<span class="deal-price" itemprop="price">'.$saleprice.'</span>';

														}


													$response .= '</div>';


													break;



												case 'spec-button' :


													$response .= '<div class="specs-table-row spec-button">';
														$public_this = new galfram_productreviewpro_amazon_api();
														$response .= $public_this->galfram_get_spec_button_html( $galfram_post_ID, $spec_id );
													$response .= '</div>';

													break;




												case 'spec-shortcode' :

													$response .= '<div class="specs-table-row spec-shortcode">';
														$spec_shortcode = get_post_meta( $galfram_post_ID, $spec_id , true);
														$response .= do_shortcode( $spec_shortcode );
													$response .= '</div>';

													break;



												case 'amazon' :

													$ama_spec = $spec['spec_amazon_field'];


													if ( $arr->Items->Request->IsValid != 'True' ) {
														$response .= '<p class="amazon-api-failed-request">'.__('There was a problem retreiving the data from Amazon', 'ultimateazon2').'</p>';
														return;
													}



													else if ( $ama_spec == 'asin' ) {

														$spec_val = $arr->Items->Item->ASIN;

														$response .= '<div class="specs-table-row spec-asin">';
															if ( !$spec_title_hide ) {
																$response .= '<strong>'.__('ASIN', 'ultimateazon2').':</strong> ';
															}
															$response .= '<span>'.$spec_val.'</span>';
														$response .= '</div>';

													}

													else if ( $ama_spec == 'brand' ) {

														$spec_val = $arr->Items->Item->ItemAttributes->Brand;

														$response .= '<div class="specs-table-row spec-brand">';
															if ( !$spec_title_hide ) {
																$response .= '<strong>'.__('Brand', 'ultimateazon2').':</strong> ';
															}
															$response .= '<span>'.$spec_val.'</span>';
														$response .= '</div>';

													}

													else if ( $ama_spec == 'model' ) {

														$spec_val = $arr->Items->Item->ItemAttributes->Model;

														$response .= '<div class="specs-table-row spec-model">';
															if ( !$spec_title_hide ) {
																$response .= '<strong>'.__('Model', 'ultimateazon2').':</strong> ';
															}
															$response .= '<span>'.$spec_val.'</span>';
														$response .= '</div>';

													}

													else if ( $ama_spec == 'upc' ) {

														$spec_val = $arr->Items->Item->ItemAttributes->UPC;

														$response .= '<div class="specs-table-row spec-upc">';
															if ( !$spec_title_hide ) {
																$response .= '<strong>'.__('UPC', 'ultimateazon2').':</strong> ';
															}
															$response .= '<span>'.$spec_val.'</span>';
														$response .= '</div>';

													}

													else if ( $ama_spec == 'features' ) {

														$prod_features_array = $arr->Items->Item->ItemAttributes->Feature;

														$response .= '<div class="specs-table-row spec-features">';

														if ( $prod_features_array == '' && !$spec_title_hide ) { $response .= '<p><strong>'.__('Features', 'ultimateazon2').':</strong> <span>'.__('N/A', 'ultimateazon2').'</span></p>'; }
												    	else {
												    		if ( !$spec_title_hide ) {
														    	$response .= '<p class="features"><strong>Features:</strong><br />';
														    }
													    	$response .= '<ul id="chosen-api-product-features">';

														    	foreach ( $prod_features_array as $feature ) {
														    		$response .= '<li>'.$feature.'</li>';
														    	}

													    	$response .= '</ul>';
													    }

													    $response .= '</div>';

													}

													else if ( $ama_spec == 'warranty' ) {

														$spec_val = $arr->Items->Item->ItemAttributes->Warranty;

														$response .= '<div class="specs-table-row spec-warranty">';
															if ( !$spec_title_hide ) {
																$response .= '<strong>'.__('Warranty', 'ultimateazon2').':</strong> ';
															}
															$response .= '<span>'.$spec_val.'</span>';
														$response .= '</div>';

													}

													else if ( $ama_spec == 'price' ) {

														$ama_price_fields = $spec['spec_amazon_price_fields'];

														//print_r($ama_price_fields);

														$prod_formatted_price = $arr->Items->Item->ItemAttributes->ListPrice->FormattedPrice;
														$prod_listprice_currencycode = $arr->Items->Item->ItemAttributes->ListPrice->CurrencyCode;
													    $prod_price_offer = $arr->Items->Item->Offers->Offer->OfferListing->Price->FormattedPrice;
													    $prod_amount_saved = $arr->Items->Item->Offers->Offer->OfferListing->AmountSaved->FormattedPrice;
													    $prod_amount_saved_currency = $arr->Items->Item->Offers->Offer->OfferListing->AmountSaved->CurrencyCode;
													    $prod_percentage_saved = $arr->Items->Item->Offers->Offer->OfferListing->PercentageSaved;


														//if data for list price or lowest new price is set to display
														if ( in_array( 'listprice', $ama_price_fields ) || in_array( 'discountprice', $ama_price_fields ) ) {

															// if list price data is set to display, and lowest new price is set to diaplay also
															if ( in_array( 'listprice', $ama_price_fields ) && in_array( 'discountprice', $ama_price_fields ) ) {

																// display the slashed out list price and the lower new price
																$response .= '<div class="specs-table-row price spec-price" itemprop="offers" itemscope itemtype="http://schema.org/Offer">';




																	if (  settype($prod_formatted_price, 'string') != settype($prod_price_offer, 'string') ) {

																		// display the slashed out list price and the lower new price
																		$response .= '<span class="slashed-price"><span>'.$prod_formatted_price.' '.$prod_listprice_currencycode.'</span></span>';
																		$response .= '<br />';
																		$response .= '<span class="deal-price" itemprop="price">'.$prod_price_offer.' '.$prod_listprice_currencycode.'</span>';

																	}
																	else {
																		$response .= '<span class="deal-price" itemprop="price">'.$prod_formatted_price.' '.$prod_listprice_currencycode.'</span>';
																	}
																		

																	if ( in_array( 'amountsaved', $ama_price_fields ) ) {

																		if ( $prod_percentage_saved != '' && $prod_amount_saved != '' ) {
																			$response .= '<span class="prod-amount-saved">'.__('Save', 'ultimateazon2').' '.$prod_percentage_saved.'% ('.$prod_amount_saved.')</span>';
																		}
																		else if ( $prod_percentage_saved != '' && $prod_amount_saved == '' ) {
																			$response .= '<span class="prod-amount-saved">'.__('Save', 'ultimateazon2').' '.$prod_percentage_saved.'%</span>';
																		}
																		else if ( $prod_percentage_saved == '' && $prod_amount_saved != '' ) {
																			$response .= '<span class="prod-amount-saved">'.__('Save', 'ultimateazon2').' '.$prod_amount_saved.'</span>';
																		}

																		

																	}

																$response .= '</div>';

															}

															// if list price data is set to display, but lowest new price is not
															else if ( in_array( 'listprice', $ama_price_fields ) && !in_array( 'discountprice', $ama_price_fields ) ) {

																// display only the list price
																$response .= '<div class="specs-table-row price">';
																	$response .= '<span class="deal-price spec-listprice" itemprop="price">'.$prod_formatted_price.' '.$prod_listprice_currencycode.'</span>';
																$response .= '</div>';

															}
															// if list price data is NOT set to display, but lowest new price is
															else if ( !in_array( 'listprice', $ama_price_fields ) && in_array( 'discountprice', $ama_price_fields ) ) {

																// display only the lowest new price
																$response .= '<div class="specs-table-row price spec-listprice" itemprop="offers" itemscope itemtype="http://schema.org/Offer">';
																	$response .= '<span class="deal-price" itemprop="price">'.$prod_price_offer.' '.$prod_listprice_currencycode.'</span>';
																$response .= '</div>';

															}


														}


													}

													else if ( $ama_spec == 'lowest-new-price' ) {

														$spec_val = $arr->Items->Item->OfferSummary->LowestNewPrice->FormattedPrice;
														$spec_val_cur = $arr->Items->Item->OfferSummary->LowestNewPrice->CurrencyCode;

														$response .= '<div class="specs-table-row spec-lowest-new-price">';
															if ( !$spec_title_hide ) {
																$response .= '<strong>'.__('Lowest New Price', 'ultimateazon2').':</strong> ';
															}
															$response .= '<span>'.$spec_val.' '.$spec_val_cur.'</span>';
														$response .= '</div>';

													}

													else if ( $ama_spec == 'lowest-used-price' ) {

														$spec_val = $arr->Items->Item->OfferSummary->LowestUsedPrice->FormattedPrice;
														$spec_val_cur = $arr->Items->Item->OfferSummary->LowestUsedPrice->CurrencyCode;

														$response .= '<div class="specs-table-row spec-lowest-used-price">';
															if ( !$spec_title_hide ) {
																$response .= '<strong>'.__('Lowest Used Price', 'ultimateazon2').':</strong> ';
															}
															$response .= '<span>'.$spec_val.' '.$spec_val_cur.'</span>';
														$response .= '</div>';

													}


													else if ( $ama_spec == 'small-image' ) {

														$thumb_url = $image_sets = $arr->Items->Item->ImageSets->ImageSet->SmallImage->URL;

														// $thumb_url = $image_sets = $arr->Items->Item->ImageSets;
														// echo '<pre>'.print_r($thumb_url,1).'</pre>';

														$response .= '<div class="specs-table-row spec-image spec-image-sm">';
															if ( !$spec_title_hide ) {
																$response .= '<strong>'.$spec_title.':</strong><br />';
															}
															$response .= '<img class="galfram-review-spec-img" src="'.$thumb_url.'" alt="" />';
														$response .= '</div>';

													}

													else if ( $ama_spec == 'medium-image' ) {

														$thumb_url = $image_sets = $arr->Items->Item->ImageSets->ImageSet->MediumImage->URL;

														$response .= '<div class="specs-table-row spec-image spec-image-md">';
															if ( !$spec_title_hide ) {
																$response .= '<strong>'.$spec_title.':</strong><br />';
															}
															$response .= '<img class="galfram-review-spec-img" src="'.$thumb_url.'" alt="" />';
														$response .= '</div>';

													}

													else if ( $ama_spec == 'large-image' ) {

														$thumb_url = $image_sets = $arr->Items->Item->ImageSets->ImageSet->LargeImage->URL;

														$response .= '<div class="specs-table-row spec-image spec-image-lg">';
															if ( !$spec_title_hide ) {
																$response .= '<strong>'.$spec_title.':</strong><br />';
															}
															$response .= '<img class="galfram-review-spec-img" src="'.$thumb_url.'" alt="" />';
														$response .= '</div>';

													}



													break;

											} // end switch ( $spec_type )



										}


									$response .= '</div>';

								$response .= '</div>';

							endif; // end if prod specs not hidden in PRP settings

						$response .= '</div>';


					$response .= '</div>';

					$hide_btm_afflink = get_option( 'extension-prp-settings_disable_bottom_aff_link', true );

					$txt_prod_link_global = get_option( 'extension-prp-settings_sidebar_product_button_text', true ); 
					$txt_prod_link_post = get_post_meta( $galfram_post_ID, 'aff_button_sidebar_text', true );

					$new_tab = get_option( 'extension-prp-settings_open_aff_links_in_new_window', true );
					if ( $new_tab == 1 ) { $new_tab_html = ' target="_blank"'; }
					else { $new_tab_html = ''; }

					$no_follow = get_option( 'extension-prp-settings_no-follow_affiliate_links', true );
					if ( $no_follow == 1 ) { $no_follow_html = ' rel="nofollow"'; }
					else { $no_follow_html = ''; }

					if ( $txt_prod_link_post != '' ) { $btn_txt = $txt_prod_link_post; }
					else if ( $txt_prod_link_global != '' ) { $btn_txt = $txt_prod_link_global; }
					else { $btn_txt = __('Buy on Amazon', 'ultimateazon2' ); }

					if ( !$hide_btm_afflink ) :
						$response .= '<a class="button buy-on-amazon buy-bottom gtm-quickview-bottom-afflink" href="'.$prod_url.'"'.$no_follow_html.$new_tab_html.'><span>'.$btn_txt.'</span></a>';
					endif;


					


			    elseif ( $chosen_specgroup == 'pull-from-amazon' ) :





			    	/****************************************
					*** Get our product info from the api ***
					****************************************/

					// http://webservices.amazon.com/scratchpad/index.html

					$chosen_review = get_post_meta( $galfram_post_ID, 'galfram_chosen_amazon_prod', true);

					$galfram_prp_amazon_api = new galfram_productreviewpro_amazon_api();
					$api_response = $galfram_prp_amazon_api->amazon_api_request( $galfram_post_ID, 'Images,ItemAttributes,Offers,Reviews', $chosen_review );

					/*************************************
					*** Parse our Results form the api ***
					*************************************/

					// convert xml response, we'll use lower while echoing out the custom amazpon specs data
					$xml_response = simplexml_load_string($api_response);

					$prod_url = $xml_response->Items->Item->DetailPageURL;



					$response = '';

						$response .= '<div class="galfram-quickview-top">';

							$response  .= '<h3>'.$galfram_review_post->post_title.'</h3>';

							$chosen_specs = get_post_meta( $galfram_post_ID, 'galfram_chosen_amazon_prod_specs_display', true);

							$remove_prod_img_top = get_option( 'extension-prp-settings_disable_top_prod_img', true ); 
			    		

				    		if ( !$remove_prod_img_top ) {

									if ( in_array( 'main-img', $chosen_specs ) ) {

										$thumb_url = $xml_response->Items->Item->LargeImage->URL;
									    if ( $thumb_url == '' ) {
									    	$image_set = $xml_response->Items->Item->ImageSets;
									    	if ( is_array( $image_set ) ) {
									    		$thumb_url = $xml_response->Items->Item->ImageSets->ImageSet[0]->LargeImage->URL;
									    	}
									    	else {
									    		$thumb_url = $xml_response->Items->Item->ImageSets->ImageSet->LargeImage->URL;
									    	}
									    }

										$thumb_url_sm = $xml_response->Items->Item->LargeImage->URL;
										if ( $thumb_url_sm == '' ) {
									    	$image_set = $xml_response->Items->Item->ImageSets;
									    	if ( is_array( $image_set ) ) {
									    		$thumb_url_sm = $xml_response->Items->Item->ImageSets->ImageSet[0]->LargeImage->URL;
									    	}
									    	else {
									    		$thumb_url_sm = $xml_response->Items->Item->ImageSets->ImageSet->LargeImage->URL;
									    	}
									    }

									}

									else {

										if ( has_post_thumbnail() ) {

											$thumb_id = get_post_thumbnail_id($galfram_post_ID);
											$thumb_url_array = wp_get_attachment_image_src($thumb_id, 'archive-default-featured-image', true);
											$thumb_url = $thumb_url_array[0];

											$thumb_url_array_sm = wp_get_attachment_image_src($thumb_id, 'archive-default-featured-image-sm', true);
											$thumb_url_sm = $thumb_url_array_sm[0];

										}
										else {

											if(get_field('default_featured_image', 'theme-options')) {
												$img = get_field('default_featured_image','theme-options');
												$img_array = wp_get_attachment_image_src( $img, 'archive-default-featured-image' );
												$thumb_url = $img_array[0];

												$thumb_url_array_sm = wp_get_attachment_image_src($img, 'archive-default-featured-image-sm', true);
												$thumb_url_sm = $thumb_url_array_sm[0];
											}
											else {
												$thumb_url = get_template_directory_uri().'/lib/images/default-featured-image.png';
												$thumb_url_sm = get_template_directory_uri().'/lib/images/default-featured-image.png';
											}

										}

									}


								

									$response .= '<div class="galfram-quickview-img">';

									
										$remove_prod_link_top = get_option( 'extension-prp-settings_disable_top_prod_img_link', true ); 
										
										if ( !$remove_prod_link_top ) {
											$response .= '<a class="gtm-quickview-top-imglink" href="'.$prod_url.'" target="_blank">';
										}

											$response .= '<img id="galfram-quickview-img" src="'.$thumb_url_sm.'" class="wp-post-image" alt="'.$galfram_review_post->post_title.'" itemprop="image" />';

										if ( !$remove_prod_link_top ) {
											$response .= '</a>';
										}


									$response .= '</div>';

							}


							

							$hide_review_link = get_option( 'extension-prp-settings_disable_full_review_btn', true );
							$hide_aff_link = get_option( 'extension-prp-settings_disable_affiliate_btn', true );

							$new_tab = get_option( 'extension-prp-settings_open_aff_links_in_new_window', true );
							if ( $new_tab == 1 ) { $new_tab_html = ' target="_blank"'; }
							else { $new_tab_html = ''; }

							$no_follow = get_option( 'extension-prp-settings_no-follow_affiliate_links', true );
							if ( $no_follow == 1 ) { $no_follow_html = ' rel="nofollow"'; }
							else { $no_follow_html = ''; }

							if ( !$hide_review_link && !$hide_aff_link ) {
								$response .= '<a class="button buy-on-amazon top-btn gtm-quickview-top-afflink" href="'.$prod_url.'"'.$no_follow_html.$new_tab_html.'><span>Buy on Amazon</span></a>';
								$response .= '<a class="button full-galfram-review gtm-quickview-top-fulllink" href="'.get_the_permalink($galfram_post_ID).'">Full Review</a>';
							}
							elseif ( !$hide_review_link && $hide_aff_link ) {
								$response .= '<a class="button full-galfram-review review-only gtm-quickview-top-fulllink" href="'.get_the_permalink($galfram_post_ID).'">Full Review</a>';
							}
							elseif ( $hide_review_link && !$hide_aff_link ) {
								$response .= '<a class="button buy-on-amazon top-btn aff-only gtm-quickview-top-afflink" href="'.$prod_url.'"'.$no_follow_html.$new_tab_html.'><span>Buy on Amazon</span></a>';
							}


							$response .= '<div class="galfram-clear"></div>';

						$response .= '</div>';

						

						$response .= '<div class="md-content-inner">';


							$hide_specs = get_option( 'extension-prp-settings_disable_prod_specs', true );

							if ( !$hide_specs ) :


								$response .= '<div id="sidebar1" class="sidebar" role="complementary">';


									if ( $xml_response->Items->Request->IsValid != 'True' ) {
										$response .= '<p class="amazon-api-failed-request">'.__('There was a problem retreiving the data from Amazon', 'ultimateazon2').'.</p>';
									}

									else { // search for ifxmlresponse to find closing tag 

										$get_amazon_review_specs = new galfram_productreviewpro_amazon_api();
										$amazon_spec_val = $get_amazon_review_specs->get_amazon_review_specs( $xml_response );


										$response .= '<div class="product-specs-group amazon-only-specs">';

											$response .= '<h2>'.__('Specifications', 'ultimateazon2').'</h2>';

											//echo '<pre>'.print_r($chosen_specs,1).'</pre>';

											$response .= '<div class="specs-table">';


												// if data for list price or lowest new price is set to display
												if ( in_array( 'list-price', $chosen_specs ) || in_array( 'price-offer', $chosen_specs ) ) {

													// if list price data is set to display, and lowest new price is set to diaplay also
													if ( in_array( 'list-price', $chosen_specs ) && in_array( 'price-offer', $chosen_specs ) ) {

														// display the slashed out list price and the lower new price
														$response .= '<div class="specs-table-row price spec-listprice" itemprop="offers" itemscope itemtype="http://schema.org/Offer">';
															$response .= '<span class="slashed-price"><span>'.$amazon_spec_val['prod_formatted_price'].' '.$amazon_spec_val['prod_listprice_currencycode'].'</span></span>';
															$response .= '<br />';
															$response .= '<span class="deal-price" itemprop="price">'.$amazon_spec_val['prod_price_offer'].' '.$amazon_spec_val['prod_listprice_currencycode'].'</span>';

															if ( in_array( 'amount-saved', $chosen_specs ) ) {

																if ( $amazon_spec_val['prod_percentage_saved'] != '' && $amazon_spec_val['prod_amount_saved'] != '' ) {
																	$response .= '<span class="prod-amount-saved">'.__('Save', 'ultimateazon2').' '.$amazon_spec_val['prod_percentage_saved'].'% ('.$amazon_spec_val['prod_amount_saved'].')</span>';
																}
																else if ( $amazon_spec_val['prod_percentage_saved'] != '' && $amazon_spec_val['prod_amount_saved'] == '' ) {
																	$response .= '<span class="prod-amount-saved">'.__('Save', 'ultimateazon2').' '.$amazon_spec_val['prod_percentage_saved'].'%</span>';
																}
																else if ( $amazon_spec_val['prod_percentage_saved'] == '' && $amazon_spec_val['prod_amount_saved'] != '' ) {
																	$response .= '<span class="prod-amount-saved">'.__('Save', 'ultimateazon2').' '.$amazon_spec_val['prod_amount_saved'].'</span>';
																}

																

															}

														$response .= '</div>';

													}

													// if list price data is set to display, but lowest new price is not
													else if ( in_array( 'list-price', $chosen_specs ) && !in_array( 'price-offer', $chosen_specs ) ) {

														// display only the list price
														$response .= '<div class="specs-table-row price spec-listprice">';
															$response .= '<span class="deal-price" itemprop="price">'.$amazon_spec_val['prod_formatted_price'].' '.$amazon_spec_val['prod_listprice_currencycode'].'</span>';
														$response .= '</div>';

													}
													// if list price data is NOT set to display, but lowest new price is
													else if ( !in_array( 'list-price', $chosen_specs ) && in_array( 'price-offer', $chosen_specs ) ) {

														// display only the lowest new price
														$response .= '<div class="specs-table-row price spec-listprice" itemprop="offers" itemscope itemtype="http://schema.org/Offer">';
															$response .= '<span class="deal-price" itemprop="price">'.$xamazon_spec_val['prod_price_offer'].' '.$amazon_spec_val['prod_listprice_currencycode'].'</span>';
														$response .= '</div>';

													}


												}

												if ( in_array( 'asin', $chosen_specs ) && $amazon_spec_val['prod_asin'] != '' ) {

													$response .= '<div class="specs-table-row spec-asin">';
														$response .= '<strong>'.__('ASIN', 'ultimateazon2').':</strong> <span>'.$amazon_spec_val['prod_asin'].'</span>';
													$response .= '</div>';

												}

												if ( in_array( 'brand', $chosen_specs ) && $amazon_spec_val['prod_brand'] != '' ) {

													$response .= '<div class="specs-table-row spec-brand">';
														$response .= '<strong>'.__('Brand', 'ultimateazon2').':</strong> <span>'.$amazon_spec_val['prod_brand'].'</span>';
													$response .= '</div>';

												}

												if ( in_array( 'model', $chosen_specs ) && $amazon_spec_val['prod_model'] != '' ) {

													$response .= '<div class="specs-table-row spec-model">';
														$response .= '<strong>'.__('Model', 'ultimateazon2').':</strong> <span>'.$amazon_spec_val['prod_model'].'</span>';
													$response .= '</div>';

												}

												if ( in_array( 'upc', $chosen_specs ) && $amazon_spec_val['prod_upc'] != '' ) {

													$response .= '<div class="specs-table-row spec-upc">';
														$response .= '<strong>'.__('UPC', 'ultimateazon2').':</strong> <span>'.$amazon_spec_val['prod_upc'].'</span>';
													$response .= '</div>';

												}

												if ( in_array( 'warranty', $chosen_specs ) && $amazon_spec_val['prod_warranty'] != '' ) {

													$response .= '<div class="specs-table-row spec-warranty">';
														$response .= '<strong>'.__('Warranty', 'ultimateazon2').':</strong> <span>'.$amazon_spec_val['prod_warranty'].'</span>';
													$response .= '</div>';

												}

												if ( in_array( 'lowest-used-price', $chosen_specs ) && $amazon_spec_val['prod_lowest_used_price'] != '' ) {

													$response .= '<div class="specs-table-row spec-lowest-used-price">';
														$response .= '<strong>'.__('Lowest Used Price', 'ultimateazon2').':</strong> <span>'.$amazon_spec_val['prod_lowest_used_price'].' '.$amazon_spec_val['prod_lowest_used_price_currency'].'</span>';
													$response .= '</div>';

												}

												if ( in_array( 'lowest-new-price', $chosen_specs ) && $amazon_spec_val['prod_lowest_used_price'] != '' ) {

													$response .= '<div class="specs-table-row spec-lowest-new-price">';
														$response .= '<strong>'.__('Lowest New Price', 'ultimateazon2').':</strong> <span>'.$amazon_spec_val['prod_lowest_new_price'].' '.$amazon_spec_val['prod_lowest_new_price_currency'].'</span>';
													$response .= '</div>';

												}

												$editor_rating = get_post_meta( $galfram_post_ID, 'editor_rating', true );

												// echo gettype( (float) $editor_rating );
												//echo ' - '.$editor_rating; die();

												if ( $editor_rating && $editor_rating != 'no-rating' ) {

													$editor_rating_num = (float) $editor_rating;

													$galfram_prp_amazon_api = new galfram_productreviewpro_amazon_api();
													$stars = $galfram_prp_amazon_api->get_star_rating_html( $editor_rating_num );


													if ( $stars != '' ) {

														$response .= '<div class="specs-table-row editor-rating custom-rating spec-star-rating" itemprop="reviewRating" itemscope itemtype="http://schema.org/Rating">';
															$response .= '<span><strong>'.__('Editor\'s Rating', 'ultimateazon2').': <span itemprop="ratingValue">'.$editor_rating.'</span> '.__('stars', 'ultimateazon2').'</strong> '.$stars.'</span>';

															$fname = get_the_author_meta('first_name');
															$lname = get_the_author_meta('last_name');

															$response .= '<meta itemprop="author" content="'.$fname.' '.$lname.'">';
															$response .= '<meta itemprop="bestRating" content="5">';
															$response .= '<meta itemprop="worstRating" content="0">';

														$response .= '</div>';

													}

												}

												if ( in_array( 'features', $chosen_specs ) && $amazon_spec_val['prod_features_array'] != '' ) {

													$response .= '<div class="specs-table-row features spec-features">';
													$response .= '<h5>'.__('Features', 'ultimateazon2').'</h5>';
												    	$response .= '<ul id="chosen-api-product-features" itemprop="description">';

													    	foreach ( $amazon_spec_val['prod_features_array'] as $feature ) {
													    		$response .= '<li>'.$feature.'</li>';
													    	}

												    	$response .= '</ul>';
											    	$response .= '</div>';

											    }

											$response .= '</div>';


										$response .= '</div>';



										?>

										
										</div>


									<?php } // END ifxmlresponse



								$response .= '</div>';

							endif; // end if product specs set to not display in PRP settings


						$response .= '</div>';

						$hide_btm_afflink = get_option( 'extension-prp-settings_disable_bottom_aff_link', true );

						$txt_prod_link_global = get_option( 'extension-prp-settings_sidebar_product_button_text', true ); 
						$txt_prod_link_post = get_post_meta( $galfram_post_ID, 'aff_button_sidebar_text', true );

						$new_tab = get_option( 'extension-prp-settings_open_aff_links_in_new_window', true );
						if ( $new_tab == 1 ) { $new_tab_html = ' target="_blank"'; }
						else { $new_tab_html = ''; }

						$no_follow = get_option( 'extension-prp-settings_no-follow_affiliate_links', true );
						if ( $no_follow == 1 ) { $no_follow_html = ' rel="nofollow"'; }
						else { $no_follow_html = ''; }

						if ( $txt_prod_link_post != '' ) { $btn_txt = $txt_prod_link_post; }
						else if ( $txt_prod_link_global != '' ) { $btn_txt = $txt_prod_link_global; }
						else { $btn_txt = __('Buy on Amazon', 'ultimateazon2' ); }

						if ( !$hide_btm_afflink ) {
							$response .= '<a class="button buy-on-amazon buy-bottom gtm-quickview-bottom-afflink" href="'.$prod_url.'"'.$no_follow_html.$new_tab_html.'><span>'.$btn_txt.'</span></a>';
						}


			   	endif;

			
		$response .= '</div>';


	    echo $response;

	    die(); // this is required to return a proper result


	}



	/** Add our custom styles to the Stylizer Child Theme
	 *
	 * @since    1.0.0
	 */
	public function galfram_add_custom_ext_css_prp() {

		



		if ( get_field('quickview_bar_bg_color','theme-styles') || get_field('quickview_bar_text_color','theme-styles') ) :
			echo 'body.post-type-archive-galfram_review .galfram-quickview-trigger, body.tax-galfram_review_cat .galfram-quickview-trigger{';
			if ( get_field('quickview_bar_bg_color','theme-styles') ) :
				echo 'background-color:'.get_field('quickview_bar_bg_color','theme-styles').';';
			endif;
			if ( get_field('quickview_bar_text_color','theme-styles') ) :
				echo 'color:'.get_field('quickview_bar_text_color','theme-styles').';';
			endif;
			echo '}';
		endif;

		if ( get_field('quickview_bar_bg_color_hover','theme-styles') || get_field('quickview_bar_text_color_hover','theme-styles') ) :
			echo 'body.post-type-archive-galfram_review .galfram-quickview-trigger:hover, body.tax-galfram_review_cat .galfram-quickview-trigger:hover{';
			if ( get_field('quickview_bar_bg_color_hover','theme-styles') ) :
				echo 'background-color:'.get_field('quickview_bar_bg_color_hover','theme-styles').';';
			endif;
			if ( get_field('quickview_bar_text_color_hover','theme-styles') ) :
				echo 'color:'.get_field('quickview_bar_text_color_hover','theme-styles').';';
			endif;
			echo '}';
		endif;




		if ( get_field('quickview_modal_prod_title_color','theme-styles') || get_field('quickview_modal_prod_title_font_size','theme-styles') ) :

			echo 'body.post-type-archive-galfram_review .md-content h3, body.tax-galfram_review_cat .md-content h3{';
			if ( get_field('quickview_modal_prod_title_color','theme-styles') ) :
				echo 'color:'.get_field('quickview_modal_prod_title_color','theme-styles').';';
			endif;
			if ( get_field('quickview_modal_prod_title_font_size','theme-styles') ) :
				echo 'font-size:'.get_field('quickview_modal_prod_title_font_size','theme-styles').'px;';
			endif;
			echo '}';
		endif;








		if ( get_field('quickview_modal_aff_btn_bg','theme-styles') || get_field('quickview_modal_aff_btn_txt','theme-styles') ) :
			echo 'body.post-type-archive-galfram_review .md-content a.buy-on-amazon, body.tax-galfram_review_cat .md-content a.buy-on-amazon,body.post-type-archive-galfram_review .md-content a.buy-on-amazon span, body.tax-galfram_review_cat .md-content a.buy-on-amazon span{';
			if ( get_field('quickview_modal_aff_btn_bg','theme-styles') ) :
				echo 'background-color:'.get_field('quickview_modal_aff_btn_bg','theme-styles').';';
			endif;

			if ( get_field('quickview_modal_aff_btn_txt','theme-styles') ) :
				echo 'color:'.get_field('quickview_modal_aff_btn_txt','theme-styles').';';
			endif;
			echo '}';
		endif;

		if ( get_field('quickview_modal_aff_btn_bg_hover','theme-styles') || get_field('quickview_modal_aff_btn_txt_hover','theme-styles') ) :
			echo 'body.post-type-archive-galfram_review .md-content a.buy-on-amazon:hover, body.tax-galfram_review_cat .md-content a.buy-on-amazon:hover,body.post-type-archive-galfram_review .md-content a.buy-on-amazon:hover span, body.tax-galfram_review_cat .md-content a.buy-on-amazon:hover span{';
			if ( get_field('quickview_modal_aff_btn_bg_hover','theme-styles') ) :
				echo 'background-color:'.get_field('quickview_modal_aff_btn_bg_hover','theme-styles').';';
			endif;

			if ( get_field('quickview_modal_aff_btn_txt_hover','theme-styles') ) :
				echo 'color:'.get_field('quickview_modal_aff_btn_txt_hover','theme-styles').';';
			endif;
			echo '}';
		endif;





		if ( get_field('quickview_modal_full_btn_bg','theme-styles') || get_field('quickview_modal_full_btn_txt','theme-styles') ) :
			echo 'body.post-type-archive-galfram_review .md-content a.full-galfram-review, body.tax-galfram_review_cat .md-content a.full-galfram-review,body.post-type-archive-galfram_review .md-content a.full-galfram-review span, body.tax-galfram_review_cat .md-content a.full-galfram-review span{';
			if ( get_field('quickview_modal_full_btn_bg','theme-styles') ) :
				echo 'background-color:'.get_field('quickview_modal_full_btn_bg','theme-styles').';';
			endif;

			if ( get_field('quickview_modal_full_btn_txt','theme-styles') ) :
				echo 'color:'.get_field('quickview_modal_full_btn_txt','theme-styles').';';
			endif;
			echo '}';
		endif;

		if ( get_field('quickview_modal_full_btn_bg_hover','theme-styles') || get_field('quickview_modal_full_btn_txt_hover','theme-styles') ) :
			echo 'body.post-type-archive-galfram_review .md-content a.full-galfram-review:hover, body.tax-galfram_review_cat .md-content a.full-galfram-review:hover,body.post-type-archive-galfram_review .md-content a.full-galfram-review:hover span, body.tax-galfram_review_cat .md-content a.full-galfram-review:hover span{';
			if ( get_field('quickview_modal_full_btn_bg_hover','theme-styles') ) :
				echo 'background-color:'.get_field('quickview_modal_full_btn_bg_hover','theme-styles').';';
			endif;

			if ( get_field('quickview_modal_full_btn_txt_hover','theme-styles') ) :
				echo 'color:'.get_field('quickview_modal_full_btn_txt_hover','theme-styles').';';
			endif;
			echo '}';
		endif;



		if ( get_field('product_specs_label_color','theme-styles') || get_field('product_specification_label_font_size','theme-styles') ) :
			echo '.specs-table .specs-table-row strong,.specs-table .specs-table-row.editor-rating strong span,.specs-table p.features strong{';
			if ( get_field('product_specs_label_color','theme-styles') ) :
				echo 'color:'.get_field('product_specs_label_color','theme-styles').';';
			endif;
			if ( get_field('product_specification_label_font_size','theme-styles') ) :
				echo 'font-size:'.get_field('product_specification_label_font_size','theme-styles').'px;';
			endif;
			echo '}';
		endif;





		if ( get_field('product_specification_value_font_size','theme-styles') || get_field('product_specification_value_line_height','theme-styles') || get_field('product_spec_value_font_color','theme-styles') ) :
			echo '.specs-table-row.spec-text span,.specs-table-row.spec-p-text span,.specs-table-row.spec-listprice span.slashed-price span,.specs-table-row.spec-listprice span.prod-amount-saved,.specs-table-row.spec-asin span,.specs-table-row.spec-brand span,.specs-table-row.spec-model span,.specs-table-row.spec-upc span,.specs-table-row.spec-lowest-used-price span,.specs-table-row.spec-lowest-new-price span,body.single-galfram_review .product-specs-group .specs-table-row.features ul#chosen-api-product-features li,#chosen-api-product-features li,body.post-type-archive-galfram_review .md-content .specs-table-row.features ul#chosen-api-product-features li{';
			if ( get_field('product_specification_value_font_size','theme-styles') ) :
				echo 'font-size:'.get_field('product_specification_value_font_size','theme-styles').'px;';
			endif;
			if ( get_field('product_specification_value_line_height','theme-styles') ) :
				echo 'line-height:'.get_field('product_specification_value_line_height','theme-styles').'px;';
			endif;
			if ( get_field('product_spec_value_font_color','theme-styles') ) :
				echo 'color:'.get_field('product_spec_value_font_color','theme-styles').';';
			endif;
			echo '}';
		endif;






		if ( get_field('link_bar_background_color','theme-styles') || get_field('link_bar_text_color','theme-styles') ) :
			echo '.button.link-bar{';
			if ( get_field('link_bar_background_color','theme-styles') ) :
				echo 'background-color:'.get_field('link_bar_background_color','theme-styles').';';
			endif;
			if ( get_field('link_bar_text_color','theme-styles')) :
				echo 'color:'.get_field('link_bar_text_color','theme-styles').';';
			endif;
			echo '}';
		endif;


		if ( get_field('link_bar_background_color_hover','theme-styles') || get_field('link_bar_text_color_hover','theme-styles') ) :
			echo '.button.link-bar:hover{';
			if ( get_field('link_bar_background_color_hover','theme-styles') ) :
				echo 'background-color:'.get_field('link_bar_background_color_hover','theme-styles').';';
			endif;
			if ( get_field('link_bar_text_color_hover','theme-styles')) :
				echo 'color:'.get_field('link_bar_text_color_hover','theme-styles').';';
			endif;
			echo '}';
		endif;




		if ( get_field('sidebar_aff_btn_bg_color','theme-styles') || get_field('sidebar_aff_btn_text_color','theme-styles') ) :
			echo 'a.button.buy-on-amazon{';
			if ( get_field('sidebar_aff_btn_bg_color','theme-styles') ) :
				echo 'background-color:'.get_field('sidebar_aff_btn_bg_color','theme-styles').';';
			endif;
			if ( get_field('sidebar_aff_btn_text_color','theme-styles')) :
				echo 'color:'.get_field('sidebar_aff_btn_text_color','theme-styles').';';
			endif;
			echo '}';
		endif;


		if ( get_field('sidebar_aff_btn_bg_color_hover','theme-styles') || get_field('sidebar_aff_btn_text_color_hover','theme-styles') ) :
			echo 'a.button.buy-on-amazon:hover{';
			if ( get_field('lsidebar_aff_btn_bg_color_hover','theme-styles') ) :
				echo 'background-color:'.get_field('sidebar_aff_btn_bg_color_hover','theme-styles').';';
			endif;
			if ( get_field('sidebar_aff_btn_text_color_hover','theme-styles')) :
				echo 'color:'.get_field('sidebar_aff_btn_text_color_hover','theme-styles').';';
			endif;
			echo '}';
		endif;



	}








}
