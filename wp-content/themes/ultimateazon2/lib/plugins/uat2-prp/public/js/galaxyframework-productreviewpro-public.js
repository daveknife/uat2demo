(function( $ ) {
	'use strict';

	/**
	 * All of the code for your public-facing JavaScript source
	 * should reside in this file.
	 *
	 * Note: It has been assumed you will write jQuery code here, so the
	 * $ function reference has been prepared for usage within the scope
	 * of this function.
	 *
	 * This enables you to define handlers, for when the DOM is ready:
	 *
	 * $(function() {
	 *
	 * });
	 *
	 * When the window is loaded:
	 *
	 * $( window ).load(function() {
	 *
	 * });
	 *
	 * ...and/or other possibilities.
	 *
	 * Ideally, it is not considered best practise to attach more than a
	 * single DOM-ready or window-load handler for a particular page.
	 * Although scripts in the WordPress core, Plugins and Themes may be
	 * practising this, we should strive to set a better example in our own work.
	 */


	 jQuery(document).ready(function() {

		/****************************************
		These functions are for front end styling
		****************************************/


		function preloadImages(srcs) {
		    if (!preloadImages.cache) {
		        preloadImages.cache = [];
		    }
		    var img;
		    for (var i = 0; i < srcs.length; i++) {
		        img = new Image();
		        img.src = srcs[i];
		        preloadImages.cache.push(img);
		    }
		}

		// then to call it, you would use this
		var imageSrcs = [];

		$( '.galfram-review-additional-img-thumb-box' ).each(function( index ) {
			imageSrcs.push( $(this).attr('data-thumb-full') );
		});

		preloadImages(imageSrcs);


	



		// swap out product images
		$('body').on( 'click', '.galfram-review-additional-img-thumb-box', function(){

			$('.galfram-review-additional-img-thumb-box').removeClass('active');
			$(this).addClass('active');

			var new_img_src = $(this).attr('data-thumb-full');

			$('#single-post-featured-image-change img').animate({
					opacity: 0
				}, 300, function() {
					$(this).attr('src',new_img_src).animate({
					opacity: 1
				}, 300, function() {
					
				});
			});
		});



		$('body').on( 'click', '.md-trigger', function(){

			var galfram_review_id = $(this).attr('data-galfram-id');
			var galfram_review_lastloaded = $(this).attr('data-galfram-id-last');

			$('.md-modal .md-content').addClass('loaded');

			if ( galfram_review_lastloaded != galfram_review_id ) {

				$('.md-modal .md-content').removeClass('loaded');

				$('#galfram-quickview-modal .md-content').html('<div class="loader"><span></span><span></span><span></span></div>');

				var data = {
		            action: 'galfram_quickview_modal_getinfo',
		            galfram_review_id: galfram_review_id,
		        };

		        galfram_review_load_quickview(data,galfram_review_id);

			}

			
			$('#prp-page-overlay').fadeIn(100);

			$('#galfram-quickview-modal').addClass('md-show');

			$('body').addClass('body-locked');

			$('.md-trigger').attr('data-galfram-id-last', galfram_review_id);

			


		});

		function galfram_review_load_quickview(data, galfram_review_id) {

			$.ajax({
	            type: 'POST',
	            url: ajaxurl,
	            data: data,
	            success: function(response) {

	                $('#galfram-quickview-modal .md-content').html(response);
	                $('#galfram-quickview-modal').append('<button class="md-close"><i class="fa fa-close"></i></button>');

	            },
	            complete: function(response){
	            	//console.log(response);

	            	$('.md-modal .md-content').delay(300).addClass('loaded');

	            	//$('#galfram-quickview-img').foundation();
	            }
	        });

		}




		$('body').on( 'click', '#prp-page-overlay, .md-close', function(){

			$('#galfram-quickview-modal').removeClass('md-show');
			$('#prp-page-overlay').fadeOut(100);
			$('body').removeClass('body-locked');

		});




		


		

		


	});



})( jQuery );
