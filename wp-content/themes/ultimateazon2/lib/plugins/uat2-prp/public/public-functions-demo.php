<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    galfram_productreviewpro
 * @subpackage galfram_productreviewpro/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    galfram_productreviewpro
 * @subpackage galfram_productreviewpro/public
 * @author     Your Name <email@example.com>
 */
class galfram_productreviewpro_Public {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $galfram_productreviewpro    The ID of this plugin.
	 */
	private $galfram_productreviewpro;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $galfram_productreviewpro       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $galfram_productreviewpro, $version ) {

		$this->galfram_productreviewpro = $galfram_productreviewpro;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in galfram_productreviewpro_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The galfram_productreviewpro_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->galfram_productreviewpro, plugin_dir_url( __FILE__ ) . 'css/style.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in galfram_productreviewpro_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The galfram_productreviewpro_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		//wp_enqueue_script( 'foundation-tabs', get_template_directory_uri() . '/bower_components/foundation-sites/js/foundation.tabs.js', array( 'jquery' ), $this->version, false );

		wp_enqueue_script( $this->galfram_productreviewpro, plugin_dir_url( __FILE__ ) . 'js/galaxyframework-productreviewpro-public.js', array( 'jquery' ), $this->version, true );

		

	}






	/**Set our custom loop to use with our post type galfram_review
	 *
	 * @since    1.0.0
	 */
	function get_galfram_review_post_type_template() {

		global $post;

		if ( is_singular('galfram_review') ) {

			remove_action( 'galfram_post_loop', 'galfram_post_loop_function', 10 );
			add_action( 'galfram_post_loop', 'galfram_review_loop_function', 10 );

		}

		function galfram_review_loop_function() {

			global $post;

			$chosen_specgroup = get_post_meta( $post->ID, 'prod_specs_group', true );
			
			if ( $chosen_specgroup != 'pull-from-amazon' ) :


					echo '<div class="content-inner-row" itemscope itemtype="http://schema.org/Review">';
						echo '<div class="content-inner-row-main">';
							do_action( 'galfram_before_loop' );

							if (have_posts()) : 
								while (have_posts()) : 
									the_post();
										?>
									<article id="post-<?php the_ID(); ?>" <?php post_class(''); ?> role="review">

										<header class="article-header">

											<h1 class="page-title" itemprop="itemReviewed"><?php the_title(); ?></h1>

											<?php
											$fname = get_the_author_meta('first_name');
											$lname = get_the_author_meta('last_name');

											echo '<meta itemprop="author" content="'.$fname.' '.$lname.'">';

											$prod_url = get_post_meta( get_the_ID(), 'manual_affiliate_link', true);

											$intro_enabled = get_post_meta( get_the_ID(), 'enable_intro_content', true);
											if ( $intro_enabled ) {
												$intro_content = get_post_meta( get_the_ID(), 'intro_content', true);
												echo '<div class="review-intro-content">'.apply_filters( 'the_content', $intro_content ).'</div>';
											}
											?>

											<div class="single-post-featured-image">

												<?php 
												if ( has_post_thumbnail() ) {

													$thumb_id = get_post_thumbnail_id();
													$thumb_url_array = wp_get_attachment_image_src($thumb_id, 'archive-default-featured-image', true);
													$thumb_url = $thumb_url_array[0];

													$thumb_url_array_sm = wp_get_attachment_image_src($thumb_id, 'archive-default-featured-image-sm', true);
													$thumb_url_sm = $thumb_url_array_sm[0];

												}
												else {

													if(get_field('default_featured_image', 'theme-options')) {
														$img = get_field('default_featured_image','theme-options');
														$img_array = wp_get_attachment_image_src( $img, 'archive-default-featured-image' );
														$thumb_url = $img_array[0];

														$thumb_url_array_sm = wp_get_attachment_image_src($img, 'archive-default-featured-image-sm', true);
														$thumb_url_sm = $thumb_url_array_sm[0];
													}
													else {
														$thumb_url = get_template_directory_uri().'/lib/images/default-featured-image.png';
														$thumb_url_sm = get_template_directory_uri().'/lib/images/default-featured-image.png';
													}

												}
												?>

												<div id="single-post-featured-image-change">

													<img id="galfram-review-main-img" data-interchange="[<?php echo $thumb_url_sm; ?>, small], [<?php echo $thumb_url; ?>, medium]" class="wp-post-image" alt="<?php the_title(); ?>" itemprop="image" />

												</div>

												<div class="galfram-review-additional-images">

													<?php

													$images = get_post_meta( get_the_ID(), 'additional_images', true );

													if( $images ) {

														for( $i = 0; $i < $images; $i++ ) {

															$thumb_id = (int) get_post_meta( get_the_ID(), 'additional_images_' . $i . '_image', true );

															// Thumbnail field returns image ID, so grab image. If none provided, use default image?
															$thumb_url_array = wp_get_attachment_image_src($thumb_id, 'review-additional-image-thumb', true);
															$thumb_url = $thumb_url_array[0];

															$thumb_url_array_full = wp_get_attachment_image_src($thumb_id, 'large', true);
															$thumb_url_full = $thumb_url_array_full[0];

															// NEED TO ADD ALT TAG IN STILL !!!

															// Build the video box
															echo '<div class="galfram-review-additional-img-thumb-box" data-thumb-full="'.$thumb_url_full.'">';
																echo '<img class="galfram-review-additional-img-switch" src="'.$thumb_url.'" alt="" />';
															echo '</div>';

														}
													}
													?>

												</div>

												<div class="clearfix"></div>

											</div>

										</header> <!-- end article header -->


										<?php 
										$remove_top_link_bar_global = get_option('extension-prp-settings_hide_top_link_bar', true); 
										$remove_bottom_link_bar_global = get_option('extension-prp-settings_hide_bottom_link_bar', true);

										$remove_top_link_bar_post = get_post_meta( get_the_ID(), 'hide_top_link_bar', true );
										$remove_bottom_link_bar_post = get_post_meta( get_the_ID(), 'hide_bottom_link_bar', true );

										$top_link_bar_text_global = get_option('extension-prp-settings_top_link_bar_text', true); 
										$bottom_link_bar_text_global = get_option('extension-prp-settings_bottom_link_bar_text', true); 

										$top_link_bar_text_post = get_post_meta( get_the_ID(), 'top_link_bar_text', true );
										if ( $top_link_bar_text_post != '' ) {
											$top_link_text = $top_link_bar_text_post;
										}
										else if ( $top_link_bar_text_post == '' && $top_link_bar_text_global != '' ) {
											$top_link_text = $top_link_bar_text_global;
										}
										else {
											$top_link_text = 'Buy on Amazon';
										}

										$bottom_link_bar_text_post = get_post_meta( get_the_ID(), 'bottom_link_bar_text', true );
										if ( $bottom_link_bar_text_post != '' ) {
											$bottom_link_text = $bottom_link_bar_text_post;
										}
										else if ( $bottom_link_bar_text_post == '' && $bottom_link_bar_text_global != '' ) {
											$bottom_link_text = $bottom_link_bar_text_global;
										}
										else {
											$bottom_link_text = 'Buy on Amazon';
										}
										?>



										<?php 
										if ( $remove_top_link_bar_global == 'show-top-link-bar' && !$remove_top_link_bar_post ) {
											echo '<a href="'.$prod_url.'" class="button link-bar" target="_blank">'.$top_link_text.'</a>';
										}
										else if ( $remove_top_link_bar_global == 'hide-top-link-bar' && $remove_top_link_bar_post ) {
											echo '<a href="'.$prod_url.'" class="button link-bar" target="_blank">'.$top_link_text.'</a>';
										}
										?>
													
										<section class="entry-content" itemprop="reviewBody">
											<?php the_content(); ?>
										</section> <!-- end article section -->

										<?php 
										if ( $remove_bottom_link_bar_global == 'show-bottom-link-bar' && !$remove_bottom_link_bar_post ) {
											echo '<a href="'.$prod_url.'" class="button link-bar" target="_blank">'.$bottom_link_text.'</a>';
										}
										else if ( $remove_bottom_link_bar_global == 'hide-bottom-link-bar' && $remove_bottom_link_bar_post ) {
											echo '<a href="'.$prod_url.'" class="button link-bar" target="_blank">'.$bottom_link_text.'</a>';
										}
										?>

										<?php 
										$checked = get_field('hide_comments_pages', 'theme-options');
										if ( $checked != true ) :
											comments_template();
										endif;
										?>

									</article> <!-- end article -->

								<?php
								endwhile;
							else:
								galfram_missing_loop();
							endif;

							do_action( 'galfram_after_loop' );
						echo '</div>';
						?>
						

						<div id="sidebar1" class="sidebar" role="complementary">

						

							<?php 

							// get our chosen specs group unique id
							$specs_group_id = get_post_meta( get_the_ID(), 'prod_specs_group', true);
							// get our array of all specs groups
							$user_specs_groups = get_option('galfram_ext_prp_all_userspecs_groups');

							// save only the chosen spec group to a new array
							foreach ( $user_specs_groups as $user_spec_group ) {
								if ( $user_spec_group['field_57c35f7e8031b'] == $specs_group_id ) {
									$current_chosen_spec_group = $user_spec_group;
								}
							}

							// save only the specs themselves into a new array
							$specs = $current_chosen_spec_group['field_57c06aa9622b9'];

							// echo '<pre>'.print_r($specs,1).'</pre>';
							// die();

							// start a new array for our spec names, types, and values
							$specs_array = array();

							$i=0;
							$amazon_spec_present = false;

							// let's set the spec title and type into the array
							foreach ( $specs as $single_spec ) {

								$specs_array[$i]['spec_title'] = $single_spec['field_57c06afc622ba'];
								$specs_array[$i]['spec_type'] = $single_spec['field_57c06b19622bb'];
								$specs_array[$i]['spec_id'] = $single_spec['field_57c6bfb62908a'];
								if ( $single_spec['field_57c06b19622bb'] == 'amazon' ) {
									$specs_array[$i]['spec_amazon_field'] = $single_spec['field_57c7052ff242d'];

									if ( $single_spec['field_57c7052ff242d'] == 'price' ) {
										$specs_array[$i]['spec_amazon_price_fields'] = $single_spec['field_58061e2c0864b'];
									}
									$amazon_spec_present = true;
								}
								else {
									$specs_array[$i]['spec_amazon_field'] = 'asin';
									$specs_array[$i]['spec_amazon_price_fields'] = array();
								}
								

								$i++;
								
							}


							/****************************************
							*** Get our product info from the api ***
							****************************************/

							// http://webservices.amazon.com/scratchpad/index.html

							$locale = trim( get_option( 'extension-prp-settings_default_amazon_search_locale' ) );

							$chosen_review = get_post_meta( get_the_ID(), 'galfram_chosen_amazon_prod', true);

							// Your AWS Access Key ID, as taken from the AWS Your Account page
							$aws_access_key_id = trim( get_option( 'extension-prp-settings_amazon_api_key' ) );

							// Your AWS Secret Key corresponding to the above ID, as taken from the AWS Your Account page
							$aws_secret_key = trim( get_option( 'extension-prp-settings_amazon_api_secret' ) );

							$affiliate_id = trim( get_option( 'extension-prp-settings_amazon_affiliate_id_us' ) );

							// The region you are interested in
							if ( $locale == 'US' ) {
								$endpoint = "webservices.amazon.com";
							}
							else if ( $locale == 'UK' ) {
								$endpoint = "webservices.amazon.co.uk";
							}
							else if ( $locale == 'BR' ) {
								$endpoint = "webservices.amazon.com.br";
							}
							else if ( $locale == 'CA' ) {
								$endpoint = "webservices.amazon.ca";
							}
							else if ( $locale == 'CN' ) {
								$endpoint = "webservices.amazon.cn";
							}
							else if ( $locale == 'FR' ) {
								$endpoint = "webservices.amazon.fr";
							}
							else if ( $locale == 'DE' ) {
								$endpoint = "webservices.amazon.de";
							}
							else if ( $locale == 'IN' ) {
								$endpoint = "webservices.amazon.in";
							}
							else if ( $locale == 'IT' ) {
								$endpoint = "webservices.amazon.it";
							}
							else if ( $locale == 'JP' ) {
								$endpoint = "webservices.amazon.co.jp";
							}
							else if ( $locale == 'ES' ) {
								$endpoint = "webservices.amazon.es";
							}
							else if ( $locale == 'MX' ) {
								$endpoint = "webservices.amazon.com.mx";
							}

							$uri = "/onca/xml";


							// if an amazon spec is present, let's make our api call
							if ( $amazon_spec_present ) {
								

								$params = array(
								    "Service" => "AWSECommerceService",
								    "Operation" => "ItemSearch",
								    "AWSAccessKeyId" => $aws_access_key_id,
								    "AssociateTag" => $affiliate_id,
								    "SearchIndex" => "All",
								    "Keywords" => $chosen_review,
								    "ResponseGroup" => "Images,ItemAttributes,Offers,Reviews"
								);

								// Set current timestamp if not set
								if (!isset($params["Timestamp"])) {
								    $params["Timestamp"] = gmdate('Y-m-d\TH:i:s\Z');
								}

								// Sort the parameters by key
								ksort($params);

								$pairs = array();

								foreach ($params as $key => $value) {
								    array_push($pairs, rawurlencode($key)."=".rawurlencode($value));
								}

								// Generate the canonical query
								$canonical_query_string = join("&", $pairs);

								// Generate the string to be signed
								$string_to_sign = "GET\n".$endpoint."\n".$uri."\n".$canonical_query_string;

								// Generate the signature required by the Product Advertising API
								$signature = base64_encode(hash_hmac("sha256", $string_to_sign, $aws_secret_key, true));

								// Generate the signed URL
								$request_url = 'http://'.$endpoint.$uri.'?'.$canonical_query_string.'&Signature='.rawurlencode($signature);

								// get our xml file contents
								$api_response = file_get_contents($request_url);




								/*************************************
								*** Parse our Results form the api ***
								*************************************/

								// convert xml response, we'll use lower while echoing out the custom amazpon specs data
								$arr = simplexml_load_string($api_response);


								
							}


							echo '<div class="product-specs-group">';

								echo '<h2>Specifications</h2>';

								echo '<div class="specs-table">';

									foreach ( $specs_array as  $spec ) {

										$spec_id = $specs_group_id.'_'.$spec['spec_id'];
										$spec_type = $spec['spec_type'];
										$spec_title = $spec['spec_title'];

										switch ( $spec_type ) {

											case 'text' :

												echo '<div class="specs-table-row">';
													echo '<strong>'.$spec_title.':</strong> ';
													echo '<span>'.get_post_meta( get_the_ID(), $spec_id , true).'</span>';
												echo '</div>';

												break;


											case 'p-text' :

												echo '<div class="specs-table-row">';
													echo '<strong>'.$spec_title.':</strong><br />';
													echo '<span>'.get_post_meta( get_the_ID(), $spec_id , true).'</span>';
												echo '</div>';

												break;

											case 'star-rating' :

												$editor_rating = get_post_meta( get_the_ID(), $spec_id , true);


												if ( $editor_rating && $editor_rating != 'no-rating' ) {

													$editor_rating_num = (float) $editor_rating;

													if ( $editor_rating_num == 0.0 ) {
														$stars = '<span class="editor-rating" title="'.$editor_rating_num.' Star Rating"><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i></span>';
													}
													else if ( $editor_rating_num >= 0.1 && $editor_rating_num <= 0.9 ) {
														$stars = '<span class="editor-rating" title="'.$editor_rating_num.' Star Rating"><i class="fa fa-star-half-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i></span>';
													}
													else if ( $editor_rating_num == 1.0 ) {
														$stars = '<span class="editor-rating" title="'.$editor_rating_num.' Star Rating"><i class="fa fa-star"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i></span>';
													}
													else if ( $editor_rating_num >= 1.1 && $editor_rating_num <= 1.9 ) {
														$stars = '<span class="editor-rating" title="'.$editor_rating_num.' Star Rating"><i class="fa fa-star"></i><i class="fa fa-star-half-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i></span>';
													}
													else if ( $editor_rating_num == 2.0 ) {
														$stars = '<span class="editor-rating" title="'.$editor_rating_num.' Star Rating"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i></span>';
													}
													else if ( $editor_rating_num >= 2.1 && $editor_rating_num <= 2.9 ) {
														$stars = '<span class="editor-rating" title="'.$editor_rating_num.' Star Rating"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star-half-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i></span>';
													}
													else if ( $editor_rating_num == 3.0 ) {
														$stars = '<span class="editor-rating" title="'.$editor_rating_num.' Star Rating"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i></span>';
													}
													else if ( $editor_rating_num >= 3.1 && $editor_rating_num <= 3.9 ) {
														$stars = '<span class="editor-rating" title="'.$editor_rating_num.' Star Rating"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star-half-o"></i><i class="fa fa-star-o"></i></span>';
													}
													else if ( $editor_rating_num == 4.0 ) {
														$stars = '<span class="editor-rating" title="'.$editor_rating_num.' Star Rating"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star-o"></i></span>';
													}
													else if ( $editor_rating_num >= 4.1 && $editor_rating_num <= 4.9 ) {
														$stars = '<span class="editor-rating" title="'.$editor_rating_num.' Star Rating"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star-half-o"></i></span>';
													}
													else if ( $editor_rating_num == 5.0 ) {
														$stars = '<span class="editor-rating" title="'.$editor_rating_num.' Star Rating"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i></span>';
													}

													//echo '<strong>'.$spec_title.':</strong> ';

													echo '<div class="specs-table-row editor-rating custom-rating" itemprop="reviewRating" itemscope itemtype="http://schema.org/Rating">';
														echo '<span><strong>'.$spec_title.': <span itemprop="ratingValue">'.$editor_rating.'</span> stars</strong> '.$stars.'</span>';

														$fname = get_the_author_meta('first_name');
														$lname = get_the_author_meta('last_name');

														echo '<meta itemprop="author" content="'.$fname.' '.$lname.'">';
														echo '<meta itemprop="bestRating" content="5">';
														echo '<meta itemprop="worstRating" content="0">';

													echo '</div>';

												}


												break;

											case 'yes-no' :

												$yesno = get_post_meta( get_the_ID(), $spec_id , true);

												echo '<div class="specs-table-row yes-no">';

													echo '<strong>'.$spec_title.':</strong> ';
													
													if ( $yesno == 'yes' ) {
														echo '<i class="fa fa-check"></i>';
													}
													elseif ( $yesno == 'no' ) {
														echo '<i class="fa fa-close"></i>';
													}

												echo '</div>';

												break;

											case 'image' :

												$thumb_id = get_post_meta( get_the_ID(), $spec_id , true);
												$thumb_url_array = wp_get_attachment_image_src($thumb_id, 'large', true);
												$thumb_url = $thumb_url_array[0];

												echo '<div class="specs-table-row">';
													echo '<strong>'.$spec_title.':</strong><br />';
													echo '<img class="galfram-review-spec-img" src="'.$thumb_url.'" alt="" />';
												echo '</div>';

												break;


											case 'price' :


												$price_fields = trim( get_option( '' ) );

												$listprice = get_post_meta( get_the_ID(), $spec_id.'_listprice' , true);
												$saleprice = get_post_meta( get_the_ID(), $spec_id.'_saleprice' , true);
												$percentagesaved = get_post_meta( get_the_ID(), $spec_id.'_percentagediscounted' , true);
												$amountsaved = get_post_meta( get_the_ID(), $spec_id.'_amountdiscounted' , true);

												echo '<div class="specs-table-row price" itemprop="offers" itemscope itemtype="http://schema.org/Offer">';

													// if data for list price or lowest new price is set to display
													if ( $listprice != '' && $saleprice != '' ) {


														// display the slashed out list price and the lower new price
														echo '<span class="slashed-price"><span>'.$listprice.'</span></span>';
														echo '<br />';
														echo '<span class="deal-price" itemprop="price">'.$saleprice.'</span>';



														if ( $percentagesaved != '' && $amountsaved != '' ) {
															echo '<span class="prod-amount-saved">Save '.$percentagesaved.' ('.$amountsaved.')</span>';
														}
														else if ( $percentagesaved != '' && $amountsaved == '' ) {
															echo '<span class="prod-amount-saved">Save '.$percentagesaved.'</span>';
														}
														else if ( $percentagesaved == '' && $amountsaved != '' ) {
															echo '<span class="prod-amount-saved">Save '.$amountsaved.'</span>';
														}


													}

													// if list price data is set to display, but lowest new price is not
													else if ( $listprice != '' && $saleprice == '' ) {

														// display only the list price
														echo '<span class="deal-price" itemprop="price">'.$listprice.'</span>';

													}
													// if list price data is NOT set to display, but lowest new price is
													else if ( $listprice == '' && $saleprice != '' ) {

														// display only the lowest new price
														echo '<span class="deal-price" itemprop="price">'.$saleprice.'</span>';

													}


												echo '</div>';


												break;



											case 'amazon' :

												$ama_spec = $spec['spec_amazon_field'];


												if ( $arr->Items->Request->IsValid != 'True' ) {
													echo '<p class="amazon-api-failed-request">There was a problem retreiving the data from Amazon.</p>';
													return;
												}



												else if ( $ama_spec == 'asin' ) {

													$spec_val = $arr->Items->Item->ASIN;

													echo '<div class="specs-table-row">';
														echo '<strong>ASIN:</strong> ';
														echo '<span>'.$spec_val.'</span>';
													echo '</div>';

												}

												else if ( $ama_spec == 'brand' ) {

													$spec_val = $arr->Items->Item->ItemAttributes->Brand;

													echo '<div class="specs-table-row">';
														echo '<strong>Brand:</strong> ';
														echo '<span>'.$spec_val.'</span>';
													echo '</div>';

												}

												else if ( $ama_spec == 'model' ) {

													$spec_val = $arr->Items->Item->ItemAttributes->Model;

													echo '<div class="specs-table-row">';
														echo '<strong>Model:</strong> ';
														echo '<span>'.$spec_val.'</span>';
													echo '</div>';

												}

												else if ( $ama_spec == 'upc' ) {

													$spec_val = $arr->Items->Item->ItemAttributes->UPC;

													echo '<div class="specs-table-row">';
														echo '<strong>UPC:</strong> ';
														echo '<span>'.$spec_val.'</span>';
													echo '</div>';

												}

												else if ( $ama_spec == 'features' ) {

													$prod_features_array = $arr->Items->Item->ItemAttributes->Feature;

													if ( $prod_features_array == '' ) { echo '<p><strong>Featutes:</strong> <span>N/A</span></p>'; }
											    	else {
												    	echo '<p class="features"><strong>Features:</strong><br />';
												    	echo '<ul id="chosen-api-product-features">';

													    	foreach ( $prod_features_array as $feature ) {
													    		echo '<li>'.$feature.'</li>';
													    	}

												    	echo '</ul>';
												    }

												}

												else if ( $ama_spec == 'warranty' ) {

													$spec_val = $arr->Items->Item->ItemAttributes->Warranty;

													echo '<div class="specs-table-row">';
														echo '<strong>Warranty:</strong> ';
														echo '<span>'.$spec_val.'</span>';
													echo '</div>';

												}

												else if ( $ama_spec == 'price' ) {

													$ama_price_fields = $spec['spec_amazon_price_fields'];

													//print_r($ama_price_fields);

													$prod_formatted_price = $arr->Items->Item->ItemAttributes->ListPrice->FormattedPrice;
													$prod_listprice_currencycode = $arr->Items->Item->ItemAttributes->ListPrice->CurrencyCode;
												    $prod_price_offer = $arr->Items->Item->Offers->Offer->OfferListing->Price->FormattedPrice;
												    $prod_amount_saved = $arr->Items->Item->Offers->Offer->OfferListing->AmountSaved->FormattedPrice;
												    $prod_amount_saved_currency = $arr->Items->Item->Offers->Offer->OfferListing->AmountSaved->CurrencyCode;
												    $prod_percentage_saved = $arr->Items->Item->Offers->Offer->OfferListing->PercentageSaved;


													//if data for list price or lowest new price is set to display
													if ( in_array( 'listprice', $ama_price_fields ) || in_array( 'discountprice', $ama_price_fields ) ) {

														// if list price data is set to display, and lowest new price is set to diaplay also
														if ( in_array( 'listprice', $ama_price_fields ) && in_array( 'discountprice', $ama_price_fields ) ) {

															// display the slashed out list price and the lower new price
															echo '<div class="specs-table-row price" itemprop="offers" itemscope itemtype="http://schema.org/Offer">';
																echo '<span class="slashed-price"><span>'.$prod_formatted_price.' '.$prod_listprice_currencycode.'</span></span>';
																echo '<br />';
																echo '<span class="deal-price" itemprop="price">'.$prod_price_offer.' '.$prod_listprice_currencycode.'</span>';

																if ( in_array( 'amountsaved', $ama_price_fields ) ) {

																	if ( $prod_percentage_saved != '' && $prod_amount_saved != '' ) {
																		echo '<span class="prod-amount-saved">Save '.$prod_percentage_saved.'% ('.$prod_amount_saved.')</span>';
																	}
																	else if ( $prod_percentage_saved != '' && $prod_amount_saved == '' ) {
																		echo '<span class="prod-amount-saved">Save '.$prod_percentage_saved.'%</span>';
																	}
																	else if ( $prod_percentage_saved == '' && $prod_amount_saved != '' ) {
																		echo '<span class="prod-amount-saved">Save '.$prod_amount_saved.'</span>';
																	}

																	

																}

															echo '</div>';

														}

														// if list price data is set to display, but lowest new price is not
														else if ( in_array( 'listprice', $ama_price_fields ) && !in_array( 'discountprice', $ama_price_fields ) ) {

															// display only the list price
															echo '<div class="specs-table-row price">';
																echo '<span class="deal-price" itemprop="price">'.$prod_formatted_price.' '.$prod_listprice_currencycode.'</span>';
															echo '</div>';

														}
														// if list price data is NOT set to display, but lowest new price is
														else if ( !in_array( 'listprice', $ama_price_fields ) && in_array( 'discountprice', $ama_price_fields ) ) {

															// display only the lowest new price
															echo '<div class="specs-table-row price" itemprop="offers" itemscope itemtype="http://schema.org/Offer">';
																echo '<span class="deal-price" itemprop="price">'.$prod_price_offer.' '.$prod_listprice_currencycode.'</span>';
															echo '</div>';

														}


													}


												}

												else if ( $ama_spec == 'lowest-new-price' ) {

													$spec_val = $arr->Items->Item->OfferSummary->LowestNewPrice->FormattedPrice;
													$spec_val_cur = $arr->Items->Item->OfferSummary->LowestNewPrice->CurrencyCode;

													echo '<div class="specs-table-row">';
														echo '<strong>Lowest New Price:</strong> ';
														echo '<span>'.$spec_val.' '.$spec_val_cur.'</span>';
													echo '</div>';

												}

												else if ( $ama_spec == 'lowest-used-price' ) {

													$spec_val = $arr->Items->Item->OfferSummary->LowestUsedPrice->FormattedPrice;
													$spec_val_cur = $arr->Items->Item->OfferSummary->LowestUsedPrice->CurrencyCode;

													echo '<div class="specs-table-row">';
														echo '<strong>Lowest Used Price:</strong> ';
														echo '<span>'.$spec_val.' '.$spec_val_cur.'</span>';
													echo '</div>';

												}


												else if ( $ama_spec == 'small-image' ) {

													$thumb_url = $image_sets = $arr->Items->Item->ImageSets->ImageSet->SmallImage->URL;

													// $thumb_url = $image_sets = $arr->Items->Item->ImageSets;
													// echo '<pre>'.print_r($thumb_url,1).'</pre>';

													echo '<div class="specs-table-row">';
														echo '<strong>'.$spec_title.':</strong><br />';
														echo '<img class="galfram-review-spec-img" src="'.$thumb_url.'" alt="" />';
													echo '</div>';

												}

												else if ( $ama_spec == 'medium-image' ) {

													$thumb_url = $image_sets = $arr->Items->Item->ImageSets->ImageSet->MediumImage->URL;

													echo '<div class="specs-table-row">';
														echo '<strong>'.$spec_title.':</strong><br />';
														echo '<img class="galfram-review-spec-img" src="'.$thumb_url.'" alt="" />';
													echo '</div>';

												}

												else if ( $ama_spec == 'large-image' ) {

													$thumb_url = $image_sets = $arr->Items->Item->ImageSets->ImageSet->LargeImage->URL;

													echo '<div class="specs-table-row">';
														echo '<strong>'.$spec_title.':</strong><br />';
														echo '<img class="galfram-review-spec-img" src="'.$thumb_url.'" alt="" />';
													echo '</div>';

												}



												break;

										} // end switch ( $spec_type )



									}



									$hide_sb_btn_global = get_option( 'extension-prp-settings_hide_sidebar_prod_link_btn', true );
									$hide_sb_btn_post = get_post_meta( get_the_ID(), 'hide_sidebar_product_link_button', true );

									//echo $hide_sidebar_prod_link_btn_global.' - '.$hide_sidebar_prod_link_btn_post;

									if ( $hide_sb_btn_global == 'show-sidebar-btn' && !$hide_sb_btn_post ) {
										echo '<a class="button buy-on-amazon" href="'.$prod_url.'" target="_blank">Buy on Amazon</a>';
									}
									else if ( $hide_sb_btn_global == 'hide-sidebar-btn' && $hide_sb_btn_post )  {
										echo '<a class="button buy-on-amazon" href="'.$prod_url.'" target="_blank">Buy on Amazon</a>';
									}


								echo '</div><!-- should be end specs table -->';

							echo '</div><!-- should be ... ... -->';


							?>

						</div>

					<?php
					echo '</div>'; // end .content-inner-row











			elseif ( $chosen_specgroup == 'pull-from-amazon' ) : 











					$params = array(
					    "Service" => "AWSECommerceService",
					    "Operation" => "ItemSearch",
					    "AWSAccessKeyId" => $aws_access_key_id,
					    "AssociateTag" => $affiliate_id,
					    "SearchIndex" => "All",
					    "Keywords" => $chosen_review,
					    "ResponseGroup" => "Images,ItemAttributes,Offers"
					);

					// Set current timestamp if not set
					if (!isset($params["Timestamp"])) {
					    $params["Timestamp"] = gmdate('Y-m-d\TH:i:s\Z');
					}

					// Sort the parameters by key
					ksort($params);

					$pairs = array();

					foreach ($params as $key => $value) {
					    array_push($pairs, rawurlencode($key)."=".rawurlencode($value));
					}

					// Generate the canonical query
					$canonical_query_string = join("&", $pairs);

					// Generate the string to be signed
					$string_to_sign = "GET\n".$endpoint."\n".$uri."\n".$canonical_query_string;

					// Generate the signature required by the Product Advertising API
					$signature = base64_encode(hash_hmac("sha256", $string_to_sign, $aws_secret_key, true));

					// Generate the signed URL
					$request_url = 'http://'.$endpoint.$uri.'?'.$canonical_query_string.'&Signature='.rawurlencode($signature);

					// get our xml file contents
					$api_response = file_get_contents($request_url);




					/*************************************
					*** Parse our Results form the api ***
					*************************************/

					// convert xml response
					$xml_response = simplexml_load_string($api_response);

					if ( $xml_response->Items->Request->IsValid != 'True' ) {
						$api_call_success = 1;
					}

					//echo '<pre>'.print_r($xml_response,1).'</pre>';





					

					echo '<div class="content-inner-row" itemscope itemtype="http://schema.org/Review">';
						echo '<div class="content-inner-row-main">';
							do_action( 'galfram_before_loop' );

							if (have_posts()) : 
								while (have_posts()) : 
									the_post();
										?>
									<article id="post-<?php the_ID(); ?>" <?php post_class(''); ?> role="review">

										<header class="article-header">

											<h1 class="page-title" itemprop="itemReviewed"><?php the_title(); ?></h1>

											<?php 

											$fname = get_the_author_meta('first_name');
											$lname = get_the_author_meta('last_name');

											echo '<meta itemprop="author" content="'.$fname.' '.$lname.'">';

											$intro_enabled = get_post_meta( get_the_ID(), 'enable_intro_content', true);
											if ( $intro_enabled ) {
												$intro_content = get_post_meta( get_the_ID(), 'intro_content', true);
												echo '<div class="review-intro-content">'.apply_filters( 'the_content', $intro_content ).'</div>';
											}
											?>

											<div class="galfram-review-images">

												<div class="galfram-review-additional-images">

													<?php

													$chosen_specs = get_post_meta( get_the_ID(), 'galfram_chosen_amazon_prod_specs_display', true);

													if ( in_array( 'add-imgs', $chosen_specs ) ) {

														// get api images
														$image_sets = $xml_response->Items->Item->ImageSets;

														//echo '<pre>'.print_r($image_sets,1).'</pre>';

														// loop through our results and parse
														foreach($image_sets as $image_set) {

															foreach($image_set as $set) {

																$thumb_url = $set->SmallImage->URL;
																$thumb_url_full = $set->LargeImage->URL;

																// NEED TO ADD ALT TAG IN STILL !!!

																// Build the image box
																echo '<div class="galfram-review-additional-img-thumb-box" data-thumb-full="'.$thumb_url_full.'">';
																	echo '<img class="galfram-review-additional-img-switch" src="'.$thumb_url.'" alt="" itemprop="image" />';
																echo '</div>';


															}


														}

													}

													else {

														$images = get_post_meta( get_the_ID(), 'additional_images', true );

														if( $images ) {

															for( $i = 0; $i < $images; $i++ ) {

																$thumb_id = (int) get_post_meta( get_the_ID(), 'additional_images_' . $i . '_image', true );

																// Thumbnail field returns image ID, so grab image. If none provided, use default image?
																$thumb_url_array = wp_get_attachment_image_src($thumb_id, 'review-additional-image-thumb', true);
																$thumb_url = $thumb_url_array[0];

																$thumb_url_array_full = wp_get_attachment_image_src($thumb_id, 'large', true);
																$thumb_url_full = $thumb_url_array_full[0];

																// NEED TO ADD ALT TAG IN STILL !!!

																// Build the image box
																echo '<div class="galfram-review-additional-img-thumb-box" data-thumb-full="'.$thumb_url_full.'">';
																	echo '<img class="galfram-review-additional-img-switch" src="'.$thumb_url.'" alt="" />';
																echo '</div>';

															}
														}

													} // end if ( in_array( 'add-imgs', $chosen_specs ) )
													?>

													<div class="clearfix"></div>

												</div>


												<?php 

												//$chosen_specs = get_post_meta( get_the_ID(), 'galfram_chosen_amazon_prod_specs_display', true);

												if ( in_array( 'main-img', $chosen_specs ) ) {

													$thumb_url = $xml_response->Items->Item->LargeImage->URL;
												    if ( $thumb_url == '' ) {
												    	$image_set = $xml_response->Items->Item->ImageSets;
												    	if ( is_array( $image_set ) ) {
												    		$thumb_url = $xml_response->Items->Item->ImageSets->ImageSet[0]->LargeImage->URL;
												    	}
												    	else {
												    		$thumb_url = $xml_response->Items->Item->ImageSets->ImageSet->LargeImage->URL;
												    	}
												    }

													$thumb_url_sm = $xml_response->Items->Item->LargeImage->URL;
													if ( $thumb_url_sm == '' ) {
												    	$image_set = $xml_response->Items->Item->ImageSets;
												    	if ( is_array( $image_set ) ) {
												    		$thumb_url_sm = $xml_response->Items->Item->ImageSets->ImageSet[0]->LargeImage->URL;
												    	}
												    	else {
												    		$thumb_url_sm = $xml_response->Items->Item->ImageSets->ImageSet->LargeImage->URL;
												    	}
												    }

												}

												else {

													if ( has_post_thumbnail() ) {

														$thumb_id = get_post_thumbnail_id();
														$thumb_url_array = wp_get_attachment_image_src($thumb_id, 'archive-default-featured-image', true);
														$thumb_url = $thumb_url_array[0];

														$thumb_url_array_sm = wp_get_attachment_image_src($thumb_id, 'archive-default-featured-image-sm', true);
														$thumb_url_sm = $thumb_url_array_sm[0];

													}
													else {

														if(get_field('default_featured_image', 'theme-options')) {
															$img = get_field('default_featured_image','theme-options');
															$img_array = wp_get_attachment_image_src( $img, 'archive-default-featured-image' );
															$thumb_url = $img_array[0];

															$thumb_url_array_sm = wp_get_attachment_image_src($img, 'archive-default-featured-image-sm', true);
															$thumb_url_sm = $thumb_url_array_sm[0];
														}
														else {
															$thumb_url = get_template_directory_uri().'/lib/images/default-featured-image.png';
															$thumb_url_sm = get_template_directory_uri().'/lib/images/default-featured-image.png';
														}

													}

												}

												?>

												<?php 
												$remove_prod_link_global = get_option( 'extension-prp-settings_no_image_link', true ); 
												$remove_prod_link_post = get_post_meta( get_the_ID(), 'do_not_link_top_large_image', true );
												$prod_url = $xml_response->Items->Item->DetailPageURL;
												?>

												
												<div id="single-post-featured-image-change">

													<?php 
													if ( $remove_prod_link_global == 'link-top-img' && !$remove_prod_link_post ) {
														echo '<a href="'.$prod_url.'" target="_blank">';
													}
													else if ( $remove_prod_link_global == 'unlink-top-img' && $remove_prod_link_post ) {
														echo '<a href="'.$prod_url.'" target="_blank">';
													}
													?>


														<img id="galfram-review-main-img" data-interchange="[<?php echo $thumb_url_sm; ?>, small], [<?php echo $thumb_url; ?>, medium]" class="wp-post-image" alt="<?php the_title(); ?>">

													<?php 
													if ( $remove_prod_link_global == 'link-top-img' && !$remove_prod_link_post ) {
														echo '</a>';
													}
													else if ( $remove_prod_link_global == 'unlink-top-img' && $remove_prod_link_post ) {
														echo '</a>';
													}
													?>


												</div>

												

												<div class="clearfix"></div>


											</div> <!-- end .galfram-review-images -->

											
											

										</header> <!-- end article header -->
													

										<?php 
										$remove_top_link_bar_global = get_option('extension-prp-settings_hide_top_link_bar', true); 
										$remove_bottom_link_bar_global = get_option('extension-prp-settings_hide_bottom_link_bar', true);

										$remove_top_link_bar_post = get_post_meta( get_the_ID(), 'hide_top_link_bar', true );
										$remove_bottom_link_bar_post = get_post_meta( get_the_ID(), 'hide_bottom_link_bar', true );

										$top_link_bar_text_global = get_option('extension-prp-settings_top_link_bar_text', true); 
										$bottom_link_bar_text_global = get_option('extension-prp-settings_bottom_link_bar_text', true); 

										$top_link_bar_text_post = get_post_meta( get_the_ID(), 'top_link_bar_text', true );
										if ( $top_link_bar_text_post != '' ) {
											$top_link_text = $top_link_bar_text_post;
										}
										else if ( $top_link_bar_text_post == '' && $top_link_bar_text_global != '' ) {
											$top_link_text = $top_link_bar_text_global;
										}
										else {
											$top_link_text = 'Buy on Amazon';
										}

										$bottom_link_bar_text_post = get_post_meta( get_the_ID(), 'bottom_link_bar_text', true );
										if ( $bottom_link_bar_text_post != '' ) {
											$bottom_link_text = $bottom_link_bar_text_post;
										}
										else if ( $bottom_link_bar_text_post == '' && $bottom_link_bar_text_global != '' ) {
											$bottom_link_text = $bottom_link_bar_text_global;
										}
										else {
											$bottom_link_text = 'Buy on Amazon';
										}
										?>



										<?php 
										if ( $remove_top_link_bar_global == 'show-top-link-bar' && !$remove_top_link_bar_post ) {
											echo '<a href="'.$prod_url.'" class="button link-bar" target="_blank">'.$top_link_text.'</a>';
										}
										else if ( $remove_top_link_bar_global == 'hide-top-link-bar' && $remove_top_link_bar_post ) {
											echo '<a href="'.$prod_url.'" class="button link-bar" target="_blank">'.$top_link_text.'</a>';
										}
										?>

										<section class="entry-content" itemprop="reviewBody">
											<?php the_content(); ?>
										</section> <!-- end article section -->

										<?php 
										if ( $remove_bottom_link_bar_global == 'show-bottom-link-bar' && !$remove_bottom_link_bar_post ) {
											echo '<a href="'.$prod_url.'" class="button link-bar" target="_blank">'.$bottom_link_text.'</a>';
										}
										else if ( $remove_bottom_link_bar_global == 'hide-bottom-link-bar' && $remove_bottom_link_bar_post ) {
											echo '<a href="'.$prod_url.'" class="button link-bar" target="_blank">'.$bottom_link_text.'</a>';
										}
										?>


										<?php 
										$checked = get_field('hide_comments_pages', 'theme-options');
										if ( $checked != true ) :
											comments_template();
										endif;
										?>

									</article> <!-- end article -->

								<?php
								endwhile;
							else:
								galfram_missing_loop();
							endif;

							do_action( 'galfram_after_loop' );
						echo '</div>';
						?>



						<div id="sidebar1" class="sidebar" role="complementary">

						<?php

						if ( $xml_response->Items->Request->IsValid != 'True' ) {
							echo '<p class="amazon-api-failed-request">There was a problem retreiving the data from Amazon.</p>';
						}


						$prod_title = htmlspecialchars( $xml_response->Items->Item->ItemAttributes->Title );
						$prod_asin = $xml_response->Items->Item->ASIN;
						//$prod_url = $xml_response->Items->Item->DetailPageURL; // pulled in above for featured image
						$prod_brand = $xml_response->Items->Item->ItemAttributes->Brand;
						$prod_model = $xml_response->Items->Item->ItemAttributes->Model;
						$prod_upc = $xml_response->Items->Item->ItemAttributes->UPC;
						$prod_features_array = $xml_response->Items->Item->ItemAttributes->Feature;
						$prod_warranty = $xml_response->Items->Item->ItemAttributes->Warranty;

					    $prod_formatted_price = $xml_response->Items->Item->ItemAttributes->ListPrice->FormattedPrice;

					    $prod_lowest_new_price = $xml_response->Items->Item->OfferSummary->LowestNewPrice->FormattedPrice;
					    $prod_lowest_used_price = $xml_response->Items->Item->OfferSummary->LowestUsedPrice->FormattedPrice;

					    $prod_lowest_new_price_currency = $xml_response->Items->Item->OfferSummary->LowestNewPrice->CurrencyCode;
					    $prod_lowest_used_price_currency = $xml_response->Items->Item->OfferSummary->LowestUsedPrice->CurrencyCode;

					    $prod_price_offer = $xml_response->Items->Item->Offers->Offer->OfferListing->Price->FormattedPrice;
					    $prod_amount_saved = $xml_response->Items->Item->Offers->Offer->OfferListing->AmountSaved->FormattedPrice;
					    $prod_amount_saved_currency = $xml_response->Items->Item->Offers->Offer->OfferListing->AmountSaved->CurrencyCode;
					    $prod_percentage_saved = $xml_response->Items->Item->Offers->Offer->OfferListing->PercentageSaved;

					    $prod_total_new = $xml_response->Items->Item->OfferSummary->TotalNew;
					    $prod_total_used = $xml_response->Items->Item->OfferSummary->TotalNew;

					    $prod_listprice_currencycode = $xml_response->Items->Item->ItemAttributes->ListPrice->CurrencyCode;


						echo '<div class="product-specs-group amazon-only-specs">';

							echo '<h5>Specifications</h5>';

							//echo '<pre>'.print_r($chosen_specs,1).'</pre>';

							echo '<div class="specs-table">';

								if ( in_array( 'asin', $chosen_specs ) && $prod_asin != '' ) {

									echo '<div class="specs-table-row">';
										echo '<strong>ASIN:</strong> <span>'.$prod_asin.'</span>';
									echo '</div>';

								}

								if ( in_array( 'brand', $chosen_specs ) && $prod_brand != '' ) {

									echo '<div class="specs-table-row">';
										echo '<strong>Brand:</strong> <span>'.$prod_brand.'</span>';
									echo '</div>';

								}

								if ( in_array( 'model', $chosen_specs ) && $prod_model != '' ) {

									echo '<div class="specs-table-row">';
										echo '<strong>Model:</strong> <span>'.$prod_model.'</span>';
									echo '</div>';

								}

								if ( in_array( 'upc', $chosen_specs ) && $prod_upc != '' ) {

									echo '<div class="specs-table-row">';
										echo '<strong>UPC:</strong> <span>'.$prod_upc.'</span>';
									echo '</div>';

								}

								if ( in_array( 'warranty', $chosen_specs ) && $prod_warranty != '' ) {

									echo '<div class="specs-table-row">';
										echo '<strong>Warranty:</strong> <span>'.$prod_warranty.'</span>';
									echo '</div>';

								}

								if ( in_array( 'lowest-used-price', $chosen_specs ) && $prod_lowest_used_price != '' ) {

									echo '<div class="specs-table-row">';
										echo '<strong>Lowest Used Price:</strong> <span>'.$prod_lowest_used_price.' '.$prod_lowest_used_price_currency.'</span>';
									echo '</div>';

								}

								if ( in_array( 'lowest-new-price', $chosen_specs ) && $prod_lowest_used_price != '' ) {

									echo '<div class="specs-table-row">';
										echo '<strong>Lowest New Price:</strong> <span>'.$prod_lowest_new_price.' '.$prod_lowest_new_price_currency.'</span>';
									echo '</div>';

								}

								$editor_rating = get_post_meta( get_the_ID(), 'editor_rating', true );

								// echo gettype( (float) $editor_rating );
								// echo ' - '.$editor_rating;

								if ( $editor_rating && $editor_rating != 'no-rating' ) {

									$editor_rating_num = (float) $editor_rating;

									if ( $editor_rating_num == 0.0 ) {
										$stars = '<span class="editor-rating" title="'.$editor_rating_num.' Star Rating"><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i></span>';
									}
									else if ( $editor_rating_num >= 0.1 && $editor_rating_num <= 0.9 ) {
										$stars = '<span class="editor-rating" title="'.$editor_rating_num.' Star Rating"><i class="fa fa-star-half-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i></span>';
									}
									else if ( $editor_rating_num == 1.0 ) {
										$stars = '<span class="editor-rating" title="'.$editor_rating_num.' Star Rating"><i class="fa fa-star"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i></span>';
									}
									else if ( $editor_rating_num >= 1.1 && $editor_rating_num <= 1.9 ) {
										$stars = '<span class="editor-rating" title="'.$editor_rating_num.' Star Rating"><i class="fa fa-star"></i><i class="fa fa-star-half-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i></span>';
									}
									else if ( $editor_rating_num == 2.0 ) {
										$stars = '<span class="editor-rating" title="'.$editor_rating_num.' Star Rating"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i></span>';
									}
									else if ( $editor_rating_num >= 2.1 && $editor_rating_num <= 2.9 ) {
										$stars = '<span class="editor-rating" title="'.$editor_rating_num.' Star Rating"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star-half-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i></span>';
									}
									else if ( $editor_rating_num == 3.0 ) {
										$stars = '<span class="editor-rating" title="'.$editor_rating_num.' Star Rating"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i></span>';
									}
									else if ( $editor_rating_num >= 3.1 && $editor_rating_num <= 3.9 ) {
										$stars = '<span class="editor-rating" title="'.$editor_rating_num.' Star Rating"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star-half-o"></i><i class="fa fa-star-o"></i></span>';
									}
									else if ( $editor_rating_num == 4.0 ) {
										$stars = '<span class="editor-rating" title="'.$editor_rating_num.' Star Rating"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star-o"></i></span>';
									}
									else if ( $editor_rating_num >= 4.1 && $editor_rating_num <= 4.9 ) {
										$stars = '<span class="editor-rating" title="'.$editor_rating_num.' Star Rating"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star-half-o"></i></span>';
									}
									else if ( $editor_rating_num == 5.0 ) {
										$stars = '<span class="editor-rating" title="'.$editor_rating_num.' Star Rating"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i></span>';
									}

									echo '<div class="specs-table-row editor-rating" itemprop="reviewRating" itemscope itemtype="http://schema.org/Rating">';
										echo '<span><strong>Editor\'s Rating: <span  itemprop="ratingValue">'.$editor_rating.'</span> stars</strong> '.$stars.'</span>';

										$fname = get_the_author_meta('first_name');
										$lname = get_the_author_meta('last_name');

										echo '<meta itemprop="author" content="'.$fname.' '.$lname.'">';
										echo '<meta itemprop="bestRating" content="5">';
										echo '<meta itemprop="worstRating" content="0">';

									echo '</div>';

								}

								// if data for list price or lowest new price is set to display
								if ( in_array( 'list-price', $chosen_specs ) || in_array( 'price-offer', $chosen_specs ) ) {

									// if list price data is set to display, and lowest new price is set to diaplay also
									if ( in_array( 'list-price', $chosen_specs ) && in_array( 'price-offer', $chosen_specs ) ) {

										// display the slashed out list price and the lower new price
										echo '<div class="specs-table-row price" itemprop="offers" itemscope itemtype="http://schema.org/Offer">';
											echo '<span class="slashed-price"><span>'.$prod_formatted_price.' '.$prod_listprice_currencycode.'</span></span>';
											echo '<br />';
											echo '<span class="deal-price" itemprop="price">'.$prod_price_offer.' '.$prod_listprice_currencycode.'</span>';

											if ( in_array( 'amount-saved', $chosen_specs ) ) {

												if ( $prod_percentage_saved != '' && $prod_amount_saved != '' ) {
													echo '<span class="prod-amount-saved">Save '.$prod_percentage_saved.'% ('.$prod_amount_saved.')</span>';
												}
												else if ( $prod_percentage_saved != '' && $prod_amount_saved == '' ) {
													echo '<span class="prod-amount-saved">Save '.$prod_percentage_saved.'%</span>';
												}
												else if ( $prod_percentage_saved == '' && $prod_amount_saved != '' ) {
													echo '<span class="prod-amount-saved">Save '.$prod_amount_saved.'</span>';
												}

												

											}

										echo '</div>';

									}

									// if list price data is set to display, but lowest new price is not
									else if ( in_array( 'list-price', $chosen_specs ) && !in_array( 'price-offer', $chosen_specs ) ) {

										// display only the list price
										echo '<div class="specs-table-row price">';
											echo '<span class="deal-price" itemprop="price">'.$prod_formatted_price.' '.$prod_listprice_currencycode.'</span>';
										echo '</div>';

									}
									// if list price data is NOT set to display, but lowest new price is
									else if ( !in_array( 'list-price', $chosen_specs ) && in_array( 'price-offer', $chosen_specs ) ) {

										// display only the lowest new price
										echo '<div class="specs-table-row price" itemprop="offers" itemscope itemtype="http://schema.org/Offer">';
											echo '<span class="deal-price" itemprop="price">'.$prod_price_offer.' '.$prod_listprice_currencycode.'</span>';
										echo '</div>';

									}


								}


								$hide_sb_btn_global = get_option( 'extension-prp-settings_hide_sidebar_prod_link_btn', true );
								$hide_sb_btn_post = get_post_meta( get_the_ID(), 'hide_sidebar_product_link_button', true );

								//echo $hide_sidebar_prod_link_btn_global.' - '.$hide_sidebar_prod_link_btn_post;

								if ( $hide_sb_btn_global == 'show-sidebar-btn' && !$hide_sb_btn_post ) {
									echo '<a class="button buy-on-amazon" href="'.$prod_url.'" target="_blank">Buy on Amazon</a>';
								}
								else if ( $hide_sb_btn_global == 'hide-sidebar-btn' && $hide_sb_btn_post )  {
									echo '<a class="button buy-on-amazon" href="'.$prod_url.'" target="_blank">Buy on Amazon</a>';
								}

								

								if ( in_array( 'features', $chosen_specs ) && $prod_features_array != '' ) {

									echo '<div class="specs-table-row features">';
									echo '<h5>Features</h5>';
								    	echo '<ul id="chosen-api-product-features" itemprop="description">';

									    	foreach ( $prod_features_array as $feature ) {
									    		echo '<li>'.$feature.'</li>';
									    	}

								    	echo '</ul>';
							    	echo '</div>';

							    }


							echo '</div>';


						echo '</div>';



						?>

						
						</div>

					<?php
					echo '</div>'; // end .content-inner-row



			endif;

			
		} // end function galfram_review_loop_function()



	}

	// galfram_before_archive_loop
	public function galfram_review_archive() {

		global $post;

		if ( is_post_type_archive() == 'galfram_review' ) {
			remove_action('galfram_loop', 'galfram_loop_function', 10);
			add_action('galfram_loop', 'galfram_prp_archive_loop_function', 10);
		}

		function galfram_prp_archive_loop_function() {
			
			echo '<div class="content-inner-row">';
				echo '<div class="content-inner-row-main">';

				galfram_before_archive_loop();

				echo 'new loop here';

				galfram_after_archive_loop();

				echo '</div>';
			echo '</div>';
		}

	}




}
