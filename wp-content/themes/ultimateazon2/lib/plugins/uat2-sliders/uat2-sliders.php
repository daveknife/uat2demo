<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              https://incomegalaxy.com
 * @since             1.0.0
 * @package           galfram_Sliders
 *
 * @wordpress-plugin
 * Plugin Name:       Galaxy Framework - Advanced Sliders
 * Plugin URI:        https://incomegalaxy.com/sliders/
 * Description:       Add advanced post sliders, content slider, amazon API product slidrrs, and more.
 * Version:           0.1.0
 * Author:            Income Galaxy
 * Author URI:        https://incomegalaxy.com
 * License:           GPL-2.0+
 * License URI:       https://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       galaxyframework-sliders
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-galaxyframework-sliders-activator.php
 */
function activate_galfram_sliders() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-galaxyframework-sliders-activator.php';
	galfram_sliders_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-galaxyframework-sliders-deactivator.php
 */
function deactivate_galfram_sliders() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-galaxyframework-sliders-deactivator.php';
	galfram_sliders_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_galfram_sliders' );
register_deactivation_hook( __FILE__, 'deactivate_galfram_sliders' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-galaxyframework-sliders.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_galfram_sliders() {

	$plugin = new galfram_sliders();
	$plugin->run();

}
run_galfram_sliders();
