<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       https://incomegalaxy.com
 * @since      1.0.0
 *
 * @package    galfram_Sliders
 * @subpackage galfram_Sliders/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    galfram_Sliders
 * @subpackage galfram_Sliders/public
 * @author     Your Name <email@example.com>
 */
class galfram_sliders_functions {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */






	/** Set our table attributes
	 *
	 * @since    1.0.0
	 */
	public function get_galfram_slider_attributes( $post_id ) {


		$slider_attributes = '';
		$override = get_post_meta( $post_id, 'override_global_slider_settings', true );


		if ( $override ) {


			$slides_to_show_sm = get_post_meta( $post_id, 'slides_to_show_sm', true );
				if ( $slides_to_show_sm ) { $_slides_to_show_sm = '"slidesToShow": '.$slides_to_show_sm.', '; }
				else { $_slides_to_show_sm = '"slidesToShow": 1, '; }

			$slides_to_scroll_sm = get_post_meta( $post_id, 'slides_to_scroll_sm', true );
				if ( $slides_to_scroll_sm ) { $_slides_to_scroll_sm = '"slidesToScroll": '.$slides_to_scroll_sm.', '; }
				else { $_slides_to_scroll_sm = '"slidesToScroll": 1, '; }

			$hide_slider_dots_sm = get_post_meta( $post_id, 'hide_slider_dots_sm', true );
				if ( $hide_slider_dots_sm ) { $_hide_slider_dots_sm = '"dots": false, '; }
				else { $_hide_slider_dots_sm = '"dots": true, '; }

			$hide_slider_arrows_sm = get_post_meta( $post_id, 'hide_slider_arrows_sm', true );
				if ( $hide_slider_arrows_sm ) { $_hide_slider_arrows_sm = '"arrows": false, '; }
				else { $_hide_slider_arrows_sm = '"arrows": true, '; }


			$md_breakpoint = get_post_meta( $post_id, 'md_breakpoint', true );
				if ( !$md_breakpoint ) { $md_breakpoint = 640; }

			$slides_to_show_md = get_post_meta( $post_id, 'slides_to_show_md', true );
				if ( $slides_to_show_md ) { $_slides_to_show_md = '"slidesToShow": '.$slides_to_show_md.', '; }
				else { $_slides_to_show_md = '"slidesToShow": 3, '; }

			$slides_to_scroll_md = get_post_meta( $post_id, 'slides_to_scroll_md', true );
				if ( $slides_to_scroll_md ) { $_slides_to_scroll_md = '"slidesToScroll": '.$slides_to_scroll_md.', '; }
				else { $_slides_to_scroll_md = '"slidesToScroll": 1, '; }

			$hide_slider_dots_md = get_post_meta( $post_id, 'hide_slider_dots_md', true );
				if ( $hide_slider_dots_md ) { $_hide_slider_dots_md = '"dots": false, '; }
				else { $_hide_slider_dots_md = '"dots": true, '; }

			$hide_slider_arrows_md = get_post_meta( $post_id, 'hide_slider_arrows_md', true );
				if ( $hide_slider_arrows_md ) { $_hide_slider_arrows_md = '"arrows": false '; }
				else { $_hide_slider_arrows_md = '"arrows": true '; }


			$lg_breakpoint = get_post_meta( $post_id, 'lg_breakpoint', true );
				if ( !$lg_breakpoint ) { $lg_breakpoint = 1023; }

			$slides_to_show_lg = get_post_meta( $post_id, 'slides_to_show_lg', true );
				if ( $slides_to_show_lg ) { $_slides_to_show_lg = '"slidesToShow": '.$slides_to_show_lg.', '; }
				else { $_slides_to_show_lg = '"slidesToShow": 3, '; }

			$slides_to_scroll_lg = get_post_meta( $post_id, 'slides_to_scroll_lg', true );
				if ( $slides_to_scroll_lg ) { $_slides_to_scroll_lg = '"slidesToScroll": '.$slides_to_scroll_lg.', '; }
				else { $_slides_to_scroll_lg = '"slidesToScroll": 1, '; }

			$hide_slider_dots_lg = get_post_meta( $post_id, 'hide_slider_dots_lg', true );
				if ( $hide_slider_dots_lg ) { $_hide_slider_dots_lg = '"dots": false, '; }
				else { $_hide_slider_dots_lg = '"dots": true, '; }

			$hide_slider_arrows_lg = get_post_meta( $post_id, 'hide_slider_arrows_lg', true );
				if ( $hide_slider_arrows_lg ) { $_hide_slider_arrows_lg = '"arrows": false '; }
				else { $_hide_slider_arrows_lg = '"arrows": true '; }


			$enable_auto_play = get_post_meta( $post_id, 'enable_auto_play', true );
				if ( $enable_auto_play ) { $_enable_auto_play = '"autoplay": true, '; }
				else { $_enable_auto_play = '"autoplay": false, '; }

			$auto_play_speed = get_post_meta( $post_id, 'auto_play_speed', true );
				if ( $auto_play_speed && $enable_auto_play ) { $_auto_play_speed = '"autoplay": true, '; }
				else { $_auto_play_speed = ''; }

			$enable_center_mode = get_post_meta( $post_id, 'enable_center_mode', true );
				if ( $enable_center_mode ) { $_enable_center_mode = '"centerMode": true, '; }
				else { $_enable_center_mode = ''; }


			// $slider_dots_size = get_post_meta( $post_id, 'slider_dots_size', true );
			// $slider_dots_position = get_post_meta( $post_id, 'slider_dots_position', true );
			// $slider_arrows_style = get_post_meta( $post_id, 'slider_arrows_style', true );


		}

		else {


			$slides_to_show_sm = get_option( 'extension-sliders-settings_slides_to_show_sm' );
				if ( $slides_to_show_sm ) { $_slides_to_show_sm = '"slidesToShow": '.$slides_to_show_sm.', '; }
				else { $_slides_to_show_sm = '"slidesToShow": 1, '; }

			$slides_to_scroll_sm = get_option( 'extension-sliders-settings_slides_to_scroll_sm' );
				if ( $slides_to_scroll_sm ) { $_slides_to_scroll_sm = '"slidesToScroll": '.$slides_to_scroll_sm.', '; }
				else { $_slides_to_scroll_sm = '"slidesToScroll": 1, '; }

			$hide_slider_dots_sm = get_option( 'extension-sliders-settings_hide_slider_dots_sm' );
				if ( $hide_slider_dots_sm ) { $_hide_slider_dots_sm = '"dots": false, '; }
				else { $_hide_slider_dots_sm = '"dots": true, '; }

			$hide_slider_arrows_sm = get_option( 'extension-sliders-settings_hide_slider_arrows_sm' );
				if ( $hide_slider_arrows_sm ) { $_hide_slider_arrows_sm = '"arrows": false, '; }
				else { $_hide_slider_arrows_sm = '"arrows": true, '; }


			$md_breakpoint = get_option( 'extension-sliders-settings_md_breakpoint' );
				if ( !$md_breakpoint ) { $md_breakpoint = 640; }

			$slides_to_show_md = get_option( 'extension-sliders-settings_slides_to_show_md' );
				if ( $slides_to_show_md ) { $_slides_to_show_md = '"slidesToShow": '.$slides_to_show_md.', '; }
				else { $_slides_to_show_md = '"slidesToShow": 3, '; }

			$slides_to_scroll_md = get_option( 'extension-sliders-settings_slides_to_scroll_md' );
				if ( $slides_to_scroll_md ) { $_slides_to_scroll_md = '"slidesToScroll": '.$slides_to_scroll_md.', '; }
				else { $_slides_to_scroll_md = '"slidesToScroll": 1, '; }

			$hide_slider_dots_md = get_option( 'extension-sliders-settings_hide_slider_dots_md' );
				if ( $hide_slider_dots_md ) { $_hide_slider_dots_md = '"dots": false, '; }
				else { $_hide_slider_dots_md = '"dots": true, '; }

			$hide_slider_arrows_md = get_option( 'extension-sliders-settings_hide_slider_arrows_md' );
				if ( $hide_slider_arrows_md ) { $_hide_slider_arrows_md = '"arrows": false '; }
				else { $_hide_slider_arrows_md = '"arrows": true '; }


			$lg_breakpoint = get_option( 'extension-sliders-settings_lg_breakpoint' );
				if ( !$lg_breakpoint ) { $lg_breakpoint = 1023; }

			$slides_to_show_lg = get_option( 'extension-sliders-settings_slides_to_show_lg' );
				if ( $slides_to_show_lg ) { $_slides_to_show_lg = '"slidesToShow": '.$slides_to_show_lg.', '; }
				else { $_slides_to_show_lg = '"slidesToShow": 3, '; }

			$slides_to_scroll_lg = get_option( 'extension-sliders-settings_slides_to_scroll_lg' );
				if ( $slides_to_scroll_lg ) { $_slides_to_scroll_lg = '"slidesToScroll": '.$slides_to_scroll_lg.', '; }
				else { $_slides_to_scroll_lg = '"slidesToScroll": 1, '; }

			$hide_slider_dots_lg = get_option( 'extension-sliders-settings_hide_slider_dots_lg' );
				if ( $hide_slider_dots_lg ) { $_hide_slider_dots_lg = '"dots": false, '; }
				else { $_hide_slider_dots_lg = '"dots": true, '; }

			$hide_slider_arrows_lg = get_option( 'extension-sliders-settings_hide_slider_arrows_lg' );
				if ( $hide_slider_arrows_lg ) { $_hide_slider_arrows_lg = '"arrows": false '; }
				else { $_hide_slider_arrows_lg = '"arrows": true '; }


			$enable_auto_play = get_option( 'extension-sliders-settings_enable_auto_play' );
				if ( $enable_auto_play ) { $_enable_auto_play = '"autoplay": true, '; }
				else { $_enable_auto_play = '"autoplay": false, '; }

			$auto_play_speed = get_option( 'extension-sliders-settings_auto_play_speed' );
				if ( $auto_play_speed && $enable_auto_play ) { $_auto_play_speed = '"autoplay": true, '; }
				else { $_auto_play_speed = ''; }

			$enable_center_mode = get_option( 'extension-sliders-settings_enable_center_mode' );
				if ( $enable_center_mode ) { $_enable_center_mode = '"centerMode": true, '; }
				else { $_enable_center_mode = ''; }


			// These are used for CSS classes in class-galaxy-framework-sliders-public.php in the galfram_layout_slider() function
			// $slider_dots_size = get_option( 'extension-sliders-settings_slider_dots_size' );
			// $slider_dots_position = get_option( 'extension-sliders-settings_slider_dots_position' );
			// $slider_arrows_style = get_option( 'extension-sliders-settings_slider_arrows_style' );


		}


		$slider_attributes = ' data-slick=\'{ ';

			$slider_attributes .= '"mobileFirst":true, ';
			$slider_attributes .= $_slides_to_show_sm;
			$slider_attributes .= $_slides_to_scroll_sm;
			$slider_attributes .= $_hide_slider_dots_sm;
			$slider_attributes .= $_hide_slider_arrows_sm;

			$slider_attributes .= $_enable_center_mode;
			$slider_attributes .= $_enable_auto_play;
			$slider_attributes .= $_auto_play_speed;

				$responsive_md = '{ "breakpoint": '.$md_breakpoint.', "settings": { ';
					$responsive_md .= $_slides_to_show_md;
					$responsive_md .= $_slides_to_scroll_md;
					$responsive_md .= $_hide_slider_dots_md;
					$responsive_md .= $_hide_slider_arrows_md;
				$responsive_md .= ' } }';

				$responsive_lg = ', { "breakpoint": '.$lg_breakpoint.', "settings": { ';
					$responsive_lg .= $_slides_to_show_lg;
					$responsive_lg .= $_slides_to_scroll_lg;
					$responsive_lg .= $_hide_slider_dots_lg;
					$responsive_lg .= $_hide_slider_arrows_lg;
				$responsive_lg .= ' } }';

				$responsive = '"responsive": [ ';
					$responsive .= $responsive_md .= $responsive_lg;
				$responsive .= ' ]';

			$slider_attributes .= $responsive;
				
		$slider_attributes .= '}\'';


		return $slider_attributes;


	}






	/** Get just our chosen spec group array
	 *
	 * @since    1.0.0
	 */
	public function get_galfram_chosenspec_fields( $chosen_spec_group, $slider_id='', $choose_posts_fields='', $slider_type='' ) {

		$spec_choices = array();


		// if ( $slider_type == 'choose-all-products' ) {

		// 	if ( $choose_posts_fields ) {

		// 		$i=0;
		// 		foreach ( $choose_posts_fields as $field ) {
		// 			$spec_choices[$i]['name'] = $field;
		// 			$spec_choices[$i]['type'] = $field;
		// 			$spec_choices[$i]['ama_type'] = null;
		// 			$i++;
		// 		}

		// 	}

		// }

		if ($slider_type == 'choose-pull-amazon') {

			if ( $choose_posts_fields ) {

				for( $i = 0; $i < $choose_posts_fields; $i++ ) {
					$spec_choices[$i]['spec_id'] = get_post_meta( $slider_id, 'choose_products_fields_specgroup_prp_allamazon_' . $i . '_column', true );
					$spec_choices[$i]['spec_alignment'] = get_post_meta( $slider_id, 'choose_products_fields_specgroup_prp_allamazon_' . $i . '_field_alignment', true );
					$spec_choices[$i]['spec_label'] = get_post_meta( $slider_id, 'choose_products_fields_specgroup_prp_allamazon_' . $i . '_show_label', true );
					$spec_choices[$i]['spec_link'] = get_post_meta( $slider_id, 'choose_products_fields_specgroup_prp_allamazon_' . $i . '_field_link', true );
				}

			}

		}

		else {

			if ( $chosen_spec_group != 'pull-from-amazon' ) {

				// get all the spec groups so we can get only the one we need
				$user_specs_groups = get_option('galfram_ext_prp_all_userspecs_groups');

				// create an empty array to save our chosen spec group into
				$current_chosen_spec_group = array();

				// loop through all spc grooups and only get the one we want for this table post
				foreach ( $user_specs_groups as $user_spec_group ) {
					if ( $user_spec_group['field_57c35f7e8031b'] == $chosen_spec_group ) {
						$current_chosen_spec_group = $user_spec_group;
					}
				}

				
				if ( $current_chosen_spec_group['field_57c068f1622b7'] == 'create-my-own' ) {
					$specs_arr = $current_chosen_spec_group['field_57c06aa9622b9'];
				}


				$i=0;
				// lets save just the spec names and IDs form the specs group array to be our choices for our table columns
				foreach ( $specs_arr as $spec ) {
					$all_specs[$i]['spec_id'] = $spec['field_57c6bfb62908a'];
					$all_specs[$i]['spec_name'] = $spec['field_57c06afc622ba'];
					$all_specs[$i]['spec_type'] = $spec['field_57c06b19622bb'];
					$all_specs[$i]['spec_ama_type'] = $spec['field_57c7052ff242d'];
					$i++;
				}

				// let's organize our spec names and ids into our final array for our choices.
				foreach ( $all_specs as $spec ) {
					$spec_choices[ $spec['spec_id'] ]['name'] = $spec['spec_name'];
					$spec_choices[ $spec['spec_id'] ]['type'] = $spec['spec_type'];
					$spec_choices[ $spec['spec_id'] ]['ama_type'] = $spec['spec_ama_type'];
				}

			}

			else if ( $chosen_spec_group == 'pull-from-amazon' ) {

				$display_fields = get_post_meta( $slider_id, 'choose_products_fields_specgroup_prp', true );
				$display_fields_arr = array();

				if( $display_fields ) {
					for( $i = 0; $i < $display_fields; $i++ ) {

						$ama_type = esc_html( get_post_meta( $slider_id, 'choose_products_fields_specgroup_prp_' . $i . '_column', true ) );

						// get the amazon spec name cleaned for displaying in the slider
						$ama_name = $this->get_galfram_ama_spec_name_clean( $ama_type );
						
						$spec_choices[$i]['name'] = $ama_name;
						$spec_choices[$i]['type'] = 'ama';
						$spec_choices[$i]['ama_type'] = $ama_type;
						
					}
				}

			}

		}


		$spec_choices_serialized = serialize( $spec_choices );
		$spec_choices = null;

		return $spec_choices_serialized;

	}


















	/** Lay out our "posts" slider
	 *
	 * @since    1.0.0
	 */
	public function get_galfram_slider_post_fields( $post_id, $posts_fields_array ) {

		$slider_html = '';
		$post_tmp = get_post($post_id);

		foreach ( $posts_fields_array as $post_field ) {

			$post_field_id = $post_field['spec_id'];
			$post_field_alignment = $post_field['spec_alignment'];

			if ( $post_field_id == 'image' ) {

				if ( has_post_thumbnail( $post_id ) ) {
					$thumb_id = get_post_thumbnail_id( $post_id );
					$thumb_url_array = wp_get_attachment_image_src($thumb_id, 'full', true);
					$thumb_url = $thumb_url_array[0];
					$alt_text = get_post_meta($thumb_id, '_wp_attachment_image_alt', true);
				}
				else {
					if(get_field('default_featured_image', 'theme-options')) {
						$thumb_id = get_field('default_featured_image','theme-options');
						$img_array = wp_get_attachment_image_src( $thumb_id, 'archive-default-featured-image' );
						$thumb_url = $img_array[0];
						$alt_text = get_post_meta($thumb_id, '_wp_attachment_image_alt', true);
					}
					else {
						$thumb_url = get_template_directory_uri().'/lib/images/default-featured-image.png';
						$alt_text = 'no image';
					}
				}

				$slider_html .= '<div class="slide-post-img '.$post_field_alignment.'">';
					$slider_html .= '<img src="'.$thumb_url.'" alt="'.$alt_text.'" />';
				$slider_html .= '</div>';

			}

			else if ( $post_field_id == 'title' ) {
				$slider_html .= '<div class="slide-post-title '.$post_field_alignment.'">';
					$slider_html .= '<h3>';
					$slider_html .= get_the_title( $post_id );
					$slider_html .= '</h3>';
				$slider_html .= '</div>';
			}

			else if ( $post_field_id == 'date' ) {
				$slider_html .= '<div class="slide-post-date '.$post_field_alignment.'">';
					$slider_html .= get_the_date( '', $post_id );
				$slider_html .= '</div>';
			}

			else if ( $post_field_id == 'excerpt' ) {

				if ( $post_tmp->post_excerpt ) :

					$excerpt = apply_filters('get_the_excerpt', $post_tmp->post_excerpt);

				else :

					// neede because no way to get excerpt outside of loop
					$excerpt = $post_tmp->post_content;
	                $excerpt = strip_shortcodes( $excerpt );
	                $excerpt = apply_filters('the_content', $excerpt);
	                $excerpt = str_replace(']]>', ']]&gt;', $excerpt);
	                $excerpt = strip_tags($excerpt);

				endif;

				if ( $excerpt ) {
					$excerpt_trimmed = substr($excerpt,0,90);
					$slider_html .= '<div class="slide-post-excerpt '.$post_field_alignment.'">';
						$slider_html .= $excerpt_trimmed.'...';
					$slider_html .= '</div>';
				}
				
			}



			else if ( $post_field_id == 'categories' ) {
				
				if( has_category( $post_id ) ) {

					$categories = get_the_category( $post_id );
					$cat_string = '';

					$i=0;
					foreach ( $categories as $cat ) {
						if ( $i > 0 ) {
							$cat_string .= ', ';
						}
						$cat_string .= $cat->name;
						$i++;
					}

					$slider_html .= '<div class="slide-post-categories '.$post_field_alignment.'">';
						$slider_html .= '<span class="slide-cat-title">'.__('Categories', 'ultimateazon2').':</span> '.$cat_string;
					$slider_html .= '</div>';
				}
			}



			else if ( $post_field_id == 'tags' ) {

				if( has_tag( $post_id ) ) {

					$tags = get_the_tags( $post_id );
					$tags_string = '';

					$i=0;
					foreach ( $tags as $tag ) {
						if ( $i > 0 ) {
							$tags_string .= ', ';
						}
						$tags_string .= $tag->name;
						$i++;
					}

					$slider_html .= '<div class="slide-post-tags '.$post_field_alignment.'">';
						$slider_html .= '<span class="slide-cat-title">'.__('Tags', 'ultimateazon2').':</span> '.$tags_string;
					$slider_html .= '</div>';
				}

			}


			else if ( $post_field_id == 'author' ) {

				// $post_tmp = get_post($post_id);
				$user_id = $post_tmp->post_author;

				$slider_html .= '<div class="slide-post-author '.$post_field_alignment.'">';
					$slider_html .= '<span class="slide-author-title">'.__('by', 'ultimateazon2').':</span> '.get_the_author_meta( 'display_name', $user_id );
				$slider_html .= '</div>';

			}

			
		}

		return $slider_html;

	}

















	/** Build our product data in groups of 10 asins at a time
	 *
	 * @since    1.0.0
	 */
	public function galfram_slider_get_asin_groups_data( $all_prod_ids, $sending_asins=false ) {

		// create a new instance of our class
		$galfram_prp_amazon_api = new galfram_productreviewpro_amazon_api();

		// set our empty table_asins_groups array
		$table_asins_groups = array();

		// this is the array not broken into ten asins, that we will loop through below to create our final amazon data array to pull into the table below
		$table_asins_full_group = array();

		$i=0;
		$z = 0;
		// loop through our table rows and save the asins into groups of 10
		foreach ( $all_prod_ids as $post_id ) {

			if ( $sending_asins == true ) {
				$asin = $post_id;
			}
			else {

				$specs_group_id = get_post_meta( $post_id, 'prod_specs_group', true );

				if ( $specs_group_id == 'pull-from-amazon' ) {
					$asin = get_post_meta( $post_id , 'galfram_chosen_amazon_prod', true);
				}
				else {
					$asin = get_post_meta( $post_id , 'amazon_manual_product_asin', true);
				}

			}

			$table_asins_full_group[$i] = $asin;

			$table_asins_groups[$z][$i]['prod_id'] = $post_id;
			$table_asins_groups[$z][$i]['asin'] = $asin;
			$i++;

			if ($i % 10 == 0 ) {
				$z++;
			}
		}

		$x=0;

		

		// let's change each group of ten into a string and make our api calls for ten items at a time
		foreach ( $table_asins_groups as $asin_group ) {
			// create our empty asins string
			$asin_string = '';
			// add our asins to the string
			foreach ( $asin_group as $asin) {
				$asin_string .= $asin['asin'].',';
			}
			// make our api call to get the product data for the ten asins
			$api_response = $galfram_prp_amazon_api->amazon_api_item_lookup( $prod_id, 'Images,ItemAttributes,Offers', $asin_string );
			// save our products data into the $all_products_info_arr
			$all_products_info_arr[$x] = simplexml_load_string($api_response);
			$x++;

		}

		// narrow down our all products data array to just the "item" array form our xml response
		$new_products_info_arr = array();
		$w=0;
		$x=0;
		foreach ( $all_products_info_arr as $arr ) {
			foreach($arr->Items->Item as $item) {
				$items_array[$x] = $item;
				$x++;
			}
			$w++;
		}

		// let's now build our final array pf product data, organized ny ASIN to look up quickly later
		$final_item_data = array();
		// loop through all of our items
		foreach($items_array as $item) {
			$asin = $item->ASIN;
			// add each product's data to the final array, with the asin for the key for easy lookup later
			$final_item_data[''.$asin.''] = $item;
		}

		return $final_item_data;

	}























	/** Build our Pro Tool slider html
	 *
	 * @since    1.0.0
	 */
	public function get_galfram_protool_fields( $prod_id, $posts_fields_array, $slider_id, $specs_group_id, $final_item_data ) {


		//echo '<pre>'.print_r($posts_fields_array,1).'</pre>';

		$new_tab = get_option( 'extension-prp-settings_open_aff_links_in_new_window', true );
		if ( $new_tab == 1 ) { $new_tab_html = ' target="_blank"'; }
		else { $new_tab_html = ''; }

		$no_follow = get_option( 'extension-prp-settings_no-follow_affiliate_links', true );
		if ( $no_follow == 1 ) { $no_follow_html = ' rel="nofollow"'; }
		else { $no_follow_html = ''; }


		if ( $specs_group_id != 'pull-from-amazon' && $specs_group_id != 'choose-pull-amazon' ) {


			// get our chosen amazon ASIN for the current product
			$galfram_ama_asin = get_post_meta( $prod_id, 'amazon_manual_product_asin', true );


			// let's add the top field for each slide
			$slider_html = '<div class="galfram-protool-spec-value protool-row protool-row-0">';
				// get our chosen top slide top spec field
				$top_spec_id = get_post_meta( $slider_id, 'choose_top_product_field', true );
				// build our spec meta key
				$spec_meta = $specs_group_id.'_'.$top_spec_id;

				// get just our chosen spec group array
				$spec_choices = unserialize( $this->get_galfram_chosenspec_fields( $specs_group_id ) );

				$top_chosen_spec = $spec_choices[$top_spec_id];

				$spec_type = $top_chosen_spec['type'];
				$spec_type_ama = $top_chosen_spec['ama_type'];

				if ( $spec_type != 'amazon' ) {
					// build our spec meta key
					$spec_meta = $specs_group_id.'_'.$spec_field_id;
					// get our custom spec value for the slider
					$spec_value = $this->get_galfram_custom_specvalue( $spec_type, $spec_meta, $prod_id );
				}

				else if ( $spec_type == 'amazon' ) {
					// get our amazon api spec value for the slider
					$spec_value = $this->get_galfram_amazon_specvalue( $spec_type_ama, $galfram_ama_asin, $final_item_data );
				}


				$slider_html .= $spec_value;

				
			$slider_html .= '</div>';

			// get just our chosen spec group array
			$spec_choices = unserialize( $this->get_galfram_chosenspec_fields( $specs_group_id ) );

			// let's loop through all chosen specs and add their field values
			$i=1;
			foreach ( $posts_fields_array as $spec_field ) {


				$spec_field_id = $spec_field['spec_id'];
				$spec_field_alignment = $spec_field['spec_alignment'];
				$spec_link = $spec_field['spec_link'];

				$spec_type = $spec_choices[ $spec_field_id ]['type'];
				$spec_type_ama = $spec_choices[ $spec_field_id ]['ama_type'];

				// echo '<pre>'.print_r($spec_field,1).'</pre>';


				// let's get our spec value here

				if ( $spec_type != 'amazon' ) {
					// build our spec meta key
					$spec_meta = $specs_group_id.'_'.$spec_field_id;
					// get our custom spec value for the slider
					$spec_value = $this->get_galfram_custom_specvalue( $spec_type, $spec_meta, $prod_id );
				}

				else if ( $spec_type == 'amazon' ) {
					// get our amazon api spec value for the slider
					$spec_value = $this->get_galfram_amazon_specvalue( $spec_type_ama, $galfram_ama_asin, $final_item_data );
				}

				$spec_name = '<span class="protool-spec-label-hidden">'.$spec_choices[$spec_field_id]['name'].'</span>';

				$row_count = $i;
				$cover_pos = 'Row '.$row_count;


				$slider_html .= '<div class="galfram-protool-spec-value protool-row protool-row-'.$i.' '.$spec_type.' '.$spec_field_alignment.'" data-event-pos="'.$cover_pos.'">';
					$slider_html .= $spec_name.$spec_value;
					if ( $spec_link == 'field-link-affiliate' ) {
						$slider_html .= '<a class="field-link-cover gtm-field-linkcover" data-link-cover-pos="'.$cover_pos.'" href="'.$final_item_data[$galfram_ama_asin]->DetailPageURL.'"'.$no_follow_html.$new_tab_html.'></a>';
					}
					elseif ( $spec_link == 'field-link-review' ) {
						$slider_html .= '<a class="field-link-cover gtm-field-linkcover" data-link-cover-pos="'.$cover_pos.'" href="'.get_the_permalink($prod_id).'"></a>';
					}
				$slider_html .= '</div>';
				$i++;

			}

		}


		else if ( $specs_group_id == 'pull-from-amazon' ) {


			// get just our chosen spec group array
			$choose_posts_fields = $posts_fields_array;

			$posts_fields_array = array();

			if( $choose_posts_fields ) {
				for( $ii = 0; $ii < $choose_posts_fields; $ii++ ) {
					$posts_fields_array[$ii]['spec_id'] = esc_html( get_post_meta( get_the_ID(), 'choose_products_fields_specgroup_' . $ii . '_column', true ) );
					$posts_fields_array[$ii]['spec_alignment'] = esc_html( get_post_meta( get_the_ID(), 'choose_products_fields_specgroup_' . $ii . '_field_alignment_protool', true ) );
					$posts_fields_array[$ii]['spec_link'] = esc_html( get_post_meta( get_the_ID(), 'choose_products_fields_specgroup_' . $ii . '_field_link', true ) );
				}
			}

			// get our chosen amazon ASIN for the current product
			$galfram_ama_asin = get_post_meta( $prod_id, 'galfram_chosen_amazon_prod', true );

			// let's add the top field for each slide
			$slider_html = '<div class="galfram-protool-spec-value protool-row protool-row-0">';
				// get our chosen top slide top spec field
				$spec_type_ama = get_post_meta( $slider_id, 'choose_top_product_field', true );
				// build our spec meta key
				$spec_value = $this->get_galfram_amazon_specvalue( $spec_type_ama, $galfram_ama_asin, $final_item_data );

				$slider_html .= $spec_value;
			$slider_html .= '</div>';

			//echo '<pre>'.print_r($posts_fields_array,1).'</pre>';


			// let's loop through all chosen specs and add their field values
			$i=1;
			foreach ( $posts_fields_array as $field_spec ) {

				//echo '<pre>'.print_r($field_spec,1).'</pre>';

				$spec_type_ama = $field_spec['spec_id'];
				$spec_alignment = $field_spec['spec_alignment'];
				$spec_link = $field_spec['spec_link'];

				$spec_value = $this->get_galfram_amazon_specvalue( $spec_type_ama, $galfram_ama_asin, $final_item_data );

				$spec_label_clean = $this->get_galfram_ama_spec_name_clean( $spec_type_ama );

				$row_count = $i;
				$cover_pos = 'Row '.$row_count;

				$slider_html .= '<div class="galfram-protool-spec-value protool-row protool-row-'.$i.' ama-'.$spec_type_ama.' '.$spec_alignment.'">';
					$slider_html .= $spec_value.' ~ '.$spec_label_clean;
					if ( $spec_link == 'field-link-affiliate' ) {
						$slider_html .= '<a class="field-link-cover gtm-field-linkcover" data-link-cover-pos="'.$cover_pos.'" href="'.$final_item_data[$galfram_ama_asin]->DetailPageURL.'"'.$no_follow_html.$new_tab_html.'></a>';
					}
					elseif ( $spec_link == 'field-link-review' ) {
						$slider_html .= '<a class="field-link-cover gtm-field-linkcover" data-link-cover-pos="'.$cover_pos.'" href="'.get_the_permalink($prod_id).'"></a>';
					}
				$slider_html .= '</div>';
				$i++;

			}

		}
		elseif ( $specs_group_id == 'choose-pull-amazon' ) {

			// get just our chosen spec group array
			$posts_fields_array = $posts_fields_array;

			// get our chosen amazon ASIN for the current product
			$galfram_ama_asin = $prod_id;

			// let's add the top field for each slide
			$slider_html = '<div class="galfram-protool-spec-value protool-row protool-row-0">';
				// get our chosen top slide top spec field
				$spec_type_ama = get_post_meta( $slider_id, 'choose_top_product_field', true );
				// build our spec meta key
				$spec_value = $this->get_galfram_amazon_specvalue( $spec_type_ama, $galfram_ama_asin, $final_item_data );

				$slider_html .= $spec_value;
			$slider_html .= '</div>';


			// let's loop through all chosen specs and add their field values
			$i=1;
			foreach ( $posts_fields_array as $field_spec ) {

				$spec_type_ama = $field_spec['spec_id'];
				$spec_alignment = $field_spec['spec_alignment'];
				$spec_link = $field_spec['spec_link'];

				//echo '<pre>'.print_r($final_item_data[$galfram_ama_asin],1).'</pre>';

				$spec_value = $this->get_galfram_amazon_specvalue( $spec_type_ama, $galfram_ama_asin, $final_item_data );

				$row_count = $i;
				$cover_pos = 'Row '.$row_count;

				$slider_html .= '<div class="galfram-protool-spec-value protool-row protool-row-'.$i.' ama-'.$spec_type_ama.' '.$spec_alignment.'" data-event-pos="'.$cover_pos.'">';
					$slider_html .= $spec_value;
					if ( $spec_link == 'field-link-affiliate' ) {
						$slider_html .= '<a class="field-link-cover gtm-field-linkcover" data-link-cover-pos="'.$cover_pos.'" href="'.$final_item_data[$galfram_ama_asin]->DetailPageURL.'"'.$no_follow_html.$new_tab_html.'></a>';
					}
				$slider_html .= '</div>';
				$i++;

			}

		}

		return $slider_html;

	}







	/** Build our PRP slider html
	 *
	 * @since    1.0.0
	 */
	public function get_galfram_prp_slider_fields( $prod_id, $posts_fields_array, $slider_id, $specs_group_id, $final_item_data, $prod_custom='', $slider_type='' ) {

		
		$new_tab = get_option( 'extension-prp-settings_open_aff_links_in_new_window', true );
		if ( $new_tab == 1 ) { $new_tab_html = ' target="_blank"'; }
		else { $new_tab_html = ''; }

		$no_follow = get_option( 'extension-prp-settings_no-follow_affiliate_links', true );
		if ( $no_follow == 1 ) { $no_follow_html = ' rel="nofollow"'; }
		else { $no_follow_html = ''; }

		// get just our chosen spec group array
		$spec_choices = $posts_fields_array;

		if ( $specs_group_id != 'pull-from-amazon' ) {



			if ( $slider_type == 'choose-pull-amazon' ) {

				//echo '<pre>'.print_r($posts_fields_array,1).'</pre>';


				// let's loop through all chosen specs and add their field values
				$i=0;
				foreach ( $posts_fields_array as $spec_field ) {

					// get our chosen amazon ASIN for the current product
					$galfram_ama_asin = $prod_id;

					$spec_type = 'amazon';
					$spec_type_ama = $spec_field['spec_id'];
					$spec_field_alignment = $spec_field['spec_alignment'];
					$spec_label_show = $spec_field['spec_label'];
					$spec_link = $spec_field['spec_link'];

					// get our amazon api spec value for the slider
					$spec_value = $this->get_galfram_amazon_specvalue( $spec_type_ama, $galfram_ama_asin, $final_item_data, $prod_id );

					$spec_label_clean = $this->get_galfram_ama_spec_name_clean( $spec_type_ama );


					if ( $spec_label_show == 1 ) {
						$label = '<span class="spec-label spec-label-prp">'.$spec_label_clean.':</span>';
					}
					else {
						$label='';
					}

					$row_count = $i+1;
					$cover_pos = 'Row '.$row_count;


					$slider_html .= '<div class="galfram-prp-spec-value prpslider-row prpslider-row-'.$i.' '.$spec_type.'-'.$spec_type_ama.' '.$spec_field_alignment.'" data-event-pos="'.$cover_pos.'">';
						$slider_html .= $label.$spec_value;
						if ( $spec_link == 'field-link-affiliate' ) {
							$slider_html .= '<a class="field-link-cover gtm-field-linkcover" href="'.$final_item_data[$galfram_ama_asin]->DetailPageURL.'"'.$no_follow_html.$new_tab_html.'></a>';
						}
						elseif ( $spec_link == 'field-link-review' ) {
							$slider_html .= '<a class="field-link-cover gtm-field-linkcover" href="'.get_the_permalink($prod_id).'"></a>';
						}
					$slider_html .= '</div>';
					$i++;

				}

			}
			else {

				// get just our chosen spec group array
				//$spec_choices = unserialize( $this->get_galfram_chosenspec_fields( $specs_group_id, $slider_id ) );

				// get just our chosen spec group array
				$spec_choices = unserialize( $this->get_galfram_chosenspec_fields( $specs_group_id ) );

				// get our chosen amazon ASIN for the current product
				$galfram_ama_asin = get_post_meta( $prod_id, 'amazon_manual_product_asin', true );

				$galfram_ama_aff_link = get_post_meta( $prod_id, 'manual_affiliate_link', true );

				// let's loop through all chosen specs and add their field values
				$i=0;
				foreach ( $posts_fields_array as $spec_field ) {

					$spec_field_id = $spec_field['spec_id'];
					$spec_field_alignment = $spec_field['spec_alignment'];
					$spec_label_show = $spec_field['spec_label'];
					$spec_link = $spec_field['spec_link'];

					$spec_type = $spec_choices[ $spec_field_id ]['type'];
					$spec_type_ama = $spec_choices[ $spec_field_id ]['ama_type'];

					// let's get our spec value here

					if ( $spec_type != 'amazon' ) {
						// build our spec meta key
						$spec_meta = $specs_group_id.'_'.$spec_field_id;

						// get our custom spec value for the slider
						$spec_value = $this->get_galfram_custom_specvalue( $spec_type, $spec_meta, $prod_id );

					}

					else if ( $spec_type == 'amazon' ) {
						// get our amazon api spec value for the slider
						$spec_value = $this->get_galfram_amazon_specvalue( $spec_type_ama, $galfram_ama_asin, $final_item_data );
					}




					if ( $spec_label_show == 1 ) {
						$label = '<span class="spec-label spec-label-prp">'.stripslashes($spec_choices[$spec_field_id]['name']).'</span>';
					}
					else {
						$label='';
					}

					$row_count = $i+1;
					$cover_pos = 'Row '.$row_count;

					if ( $spec_type == 'amazon' ) {
						$spec_type_class = $spec_type .'-'. $spec_type_ama;
					}

					$slider_html .= '<div class="galfram-prp-spec-value prpslider-row prpslider-row-'.$i.' '.$spec_type_class.' '.$spec_field_alignment.'" data-event-pos="'.$cover_pos.'">';
						$slider_html .= $label.$spec_value;
						if ( $spec_link == 'field-link-affiliate' ) {
							$slider_html .= '<a class="field-link-cover gtm-field-linkcover" href="'.$galfram_ama_aff_link.'"'.$no_follow_html.$new_tab_html.'></a>';
						}
						elseif ( $spec_link == 'field-link-review' ) {
							$slider_html .= '<a class="field-link-cover gtm-field-linkcover" href="'.get_the_permalink($prod_id).'"></a>';
						}
					$slider_html .= '</div>';
					$i++;

				}

			}

		}


		else if ( $specs_group_id == 'pull-from-amazon' ) {

			//echo '<pre>'.print_r($posts_fields_array,1).'</pre>';

			// let's loop through all chosen specs and add their field values
			$i=0;
			foreach ( $posts_fields_array as $spec_field ) {

				$spec_field_id = $spec_field['spec_id'];
				$spec_field_alignment = $spec_field['spec_alignment'];
				$spec_label_show = $spec_field['spec_label'];
				$spec_link = $spec_field['spec_link'];

				// get our chosen amazon ASIN for the current product
				$galfram_ama_asin = get_post_meta( $prod_id, 'galfram_chosen_amazon_prod', true );

				$spec_type = 'amazon';
				$spec_type_ama = $spec_field[ 'spec_id' ];

				if ( $spec_field_id == 'editor-rating' ) {

					$editor_rating = esc_html( get_post_meta( $prod_id, 'editor_rating', true ) );
					$spec_value = '';

					if ( $editor_rating && $editor_rating != 'no-rating' ) {

						$editor_rating_num = (float) $editor_rating;

						$galfram_prp_amazon_api = new galfram_productreviewpro_amazon_api();
						$stars = $galfram_prp_amazon_api->get_star_rating_html( $editor_rating_num );

						if ( $stars != '' ) {

							$spec_value .= '<div class="editor-rating custom-rating spec-star-rating" itemprop="reviewRating" itemscope itemtype="http://schema.org/Rating">';
								$spec_value .= '<span>'.$stars.'</span>';

								$fname = get_the_author_meta('first_name');
								$lname = get_the_author_meta('last_name');

								$spec_value .= '<meta itemprop="author" content="'.$fname.' '.$lname.'">';
								$spec_value .= '<meta itemprop="bestRating" content="5">';
								$spec_value .= '<meta itemprop="worstRating" content="0">';

							$spec_value  .= '</div>';

						}

					}
				}
				else {
					// get our amazon api spec value for the slider
					$spec_value = $this->get_galfram_amazon_specvalue( $spec_type_ama, $galfram_ama_asin, $final_item_data, $prod_id );
					$spec_label_clean = $this->get_galfram_ama_spec_name_clean( $spec_type_ama );
				}

					


				if ( $spec_label_show == 1 ) {
					$label = '<span class="spec-label spec-label-prp">'.$spec_label_clean.':</span>';
				}
				else {
					$label='';
				}

				$row_count = $i+1;
				$cover_pos = 'Row '.$row_count;


				$slider_html .= '<div class="galfram-prp-spec-value prpslider-row prpslider-row-'.$i.' '.$spec_type.'-'.$spec_type_ama.' '.$spec_field_alignment.'" data-event-pos="'.$cover_pos.'">';
					$slider_html .= $label.$spec_value;
					if ( $spec_link == 'field-link-affiliate' ) {
						$slider_html .= '<a class="field-link-cover gtm-field-linkcover" href="'.$final_item_data[$galfram_ama_asin]->DetailPageURL.'"'.$no_follow_html.$new_tab_html.'></a>';
					}
					elseif ( $spec_link == 'field-link-review' ) {
						$slider_html .= '<a class="field-link-cover gtm-field-linkcover" href="'.get_the_permalink($prod_id).'"></a>';
					}
				$slider_html .= '</div>';
				$i++;

			}

		}

		return $slider_html;

	}



















	/** Get our amazon spec value for the slide
	 *
	 * @since    1.0.0
	 */
	public function get_galfram_amazon_specvalue( $spec_type_ama, $galfram_ama_asin, $final_item_data, $prod_id=null ) {

		//echo '<pre>'.print_r($final_item_data,1).'</pre>';


		if ( $spec_type_ama == 'features'  ) {

			$prod_features_array = $final_item_data[$galfram_ama_asin]->ItemAttributes->Feature;

			if ( is_object( $prod_features_array ) ) {
				$spec_value = '<ul class="chosen-api-product-features">';
	    		foreach ( $prod_features_array as $feature ) {
	    			$spec_value .= '<li>'.$feature.'</li>';
	    		}
		    	$spec_value .= '</ul>';
			}
			else{
				$spec_value = __('N/A', 'ultimateazon2');
			}

		}

		elseif ( $spec_type_ama == 'asin' ) {
			$api_val = $final_item_data[$galfram_ama_asin]->ASIN;
			if ( $api_val != '' ) {
				$spec_value = $api_val;
			}
			else {
				$spec_value = __('N/A', 'ultimateazon2');
			}
		}

		elseif ( $spec_type_ama == 'brand' ) {
			$api_val = $final_item_data[$galfram_ama_asin]->ItemAttributes->Brand;
			if ( $api_val != '' ) {
				$spec_value = $api_val;
			}
			else {
				$spec_value = __('N/A', 'ultimateazon2');
			}
		}

		elseif ( $spec_type_ama == 'upc' ) {
			$api_val = $final_item_data[$galfram_ama_asin]->ItemAttributes->UPC;
			if ( $api_val != '' ) {
				$spec_value = $api_val;
			}
			else {
				$spec_value = __('N/A', 'ultimateazon2');
			}
		}

		elseif ( $spec_type_ama == 'warranty' ) {
			$api_val = $final_item_data[$galfram_ama_asin]->ItemAttributes->Warranty;
			if ( $api_val != '' ) {
				$spec_value = $api_val;
			}
			else {
				$spec_value = __('N/A', 'ultimateazon2');
			}
		}

		elseif ( $spec_type_ama == 'model' ) {
			$api_val = $final_item_data[$galfram_ama_asin]->ItemAttributes->Model;
			if ( $api_val != '' ) {
				$spec_value = $api_val;
			}
			else {
				$spec_value = __('N/A', 'ultimateazon2');
			}
		}

		elseif ( $spec_type_ama == 'small-image' ) {
			$api_val = $final_item_data[$galfram_ama_asin]->ImageSets->ImageSet->SmallImage->URL;
			if ( $api_val ) {
				$spec_value = '<img src="'.$api_val.'" alt="" />';
			}
				
		}
		elseif ( $spec_type_ama == 'medium-image' ) {
			$api_val = $final_item_data[$galfram_ama_asin]->ImageSets->ImageSet->MediumImage->URL;
			if ( $api_val ) {
				$spec_value = '<img src="'.$api_val.'" alt="" />';
			}
		}
		elseif ( $spec_type_ama == 'large-image' ) {
			$api_val = $final_item_data[$galfram_ama_asin]->ImageSets->ImageSet->LargeImage->URL;
			if ( $api_val ) {
				$spec_value = '<img src="'.$api_val.'" alt="" />';
			}
		}

		elseif ( $spec_type_ama == 'lowest-new-price' ) {
			$api_val = $final_item_data[$galfram_ama_asin]->OfferSummary->LowestNewPrice->FormattedPrice;
			if ( $api_val ) {
				$spec_value = $api_val;
			} else {
				$spec_value = __('N/A', 'ultimateazon2');
			}
			
		}

		elseif ( $spec_type_ama == 'lowest-used-price' ) {
			$api_val = $final_item_data[$galfram_ama_asin]->OfferSummary->LowestUsedPrice->FormattedPrice;
			if ( $api_val ) {
				$spec_value = $api_val;
			} else {
				$spec_value = __('N/A', 'ultimateazon2');
			}
		}

		elseif ( $spec_type_ama == 'price'  ) {


			$prod_formatted_price = $final_item_data[$galfram_ama_asin]->ItemAttributes->ListPrice->FormattedPrice;
			$prod_listprice_currencycode = $final_item_data[$galfram_ama_asin]->ItemAttributes->ListPrice->CurrencyCode;

		    $prod_price_offer = $final_item_data[$galfram_ama_asin]->Offers->Offer->OfferListing->SalePrice->FormattedPrice;
		    $prod_amount_saved = $final_item_data[$galfram_ama_asin]->Offers->Offer->OfferListing->AmountSaved->FormattedPrice;
		    $prod_amount_saved_currency = $final_item_data[$galfram_ama_asin]->Offers->Offer->OfferListing->AmountSaved->CurrencyCode;
		    $prod_percentage_saved = $final_item_data[$galfram_ama_asin]->Offers->Offer->OfferListing->PercentageSaved;

			$api_val = $final_item_data[$galfram_ama_asin]->Offers->Offer->OfferListing->Price->FormattedPrice;
			$api_val_sale = $final_item_data[$galfram_ama_asin]->Offers->Offer->OfferListing->SalePrice->FormattedPrice;


			if ( $prod_formatted_price ) {

				$spec_value = '';

				if ( $prod_price_offer == '' ) {
					$spec_value .= '<span class="deal-price"><span>'.$prod_formatted_price.' '.$prod_listprice_currencycode.'</span></span>';
				}

				else if ( $prod_price_offer != '' ) {
					$spec_value .= '<span class="slashed-price"><span>'.$prod_formatted_price.' '.$prod_listprice_currencycode.'</span></span>';
					$spec_value .= '<br />';
					$spec_value .= '<span class="deal-price" itemprop="price">'.$prod_price_offer.' '.$prod_listprice_currencycode.'</span>';

					if ( $prod_percentage_saved != '' && $prod_amount_saved != '' ) {
						$spec_value .=  '<span class="prod-amount-saved">Save '.$prod_percentage_saved.'% ('.$prod_amount_saved.')</span>';
					}
					else if ( $prod_percentage_saved != '' && $prod_amount_saved == '' ) {
						$spec_value .=  '<span class="prod-amount-saved">'.__('Save', 'ultimateazon2').' '.$prod_percentage_saved.'%</span>';
					}
					else if ( $prod_percentage_saved == '' && $prod_amount_saved != '' ) {
						$spec_value .=  '<span class="prod-amount-saved">'.__('Save', 'ultimateazon2').' '.$prod_amount_saved.'</span>';
					}

				}
				
					
			}

			else {
				$spec_value = __( 'N/A', 'ultimateazon2' ) ;
			}
		}

		elseif ( $spec_type_ama == 'amazon-title' ) {
			$api_val = $final_item_data[$galfram_ama_asin]->ItemAttributes->Title;
			if ( $api_val ) {
				$spec_value = $api_val;
			} else {
				$spec_value = __('N/A', 'ultimateazon2');
			}
		}

		elseif ( $spec_type_ama == 'wp-title' ) {
			$spec_value = get_the_title($prod_id);
		}

		else if ( $spec_type_ama == 'editor-rating' ) {
			// get our actual spec value
			$editor_rating = esc_html( get_post_meta( $prod_id, 'editor_rating', true ) );

			$spec_value = '';

			if ( $editor_rating && $editor_rating != 'no-rating' ) {

				$editor_rating_num = (float) $editor_rating;

				$galfram_prp_amazon_api = new galfram_productreviewpro_amazon_api();
				$stars = $galfram_prp_amazon_api->get_star_rating_html( $editor_rating_num );

				if ( $stars != '' ) {

					$spec_value .= '<div class="editor-rating custom-rating spec-star-rating" itemprop="reviewRating" itemscope itemtype="http://schema.org/Rating">';
						$spec_value .= '<span>'.$stars.'</span>';

						$fname = get_the_author_meta('first_name');
						$lname = get_the_author_meta('last_name');

						$spec_value .= '<meta itemprop="author" content="'.$fname.' '.$lname.'">';
						$spec_value .= '<meta itemprop="bestRating" content="5">';
						$spec_value .= '<meta itemprop="worstRating" content="0">';

					$spec_value  .= '</div>';

				}

			}

		}

		return $spec_value;

	}


















	/** get our custom spec value for the slide
	 *
	 * @since    1.0.0
	 */
	public function get_galfram_custom_specvalue( $spec_type, $spec_meta, $prod_id ) {


		if ( $spec_type == 'text' ) {
			// get our actual spec value
			$spec_value = esc_html( get_post_meta( $prod_id, $spec_meta, true ) );
		}
		else if ( $spec_type == 'p-text' ) {
			// get our actual spec value
			$spec_value = esc_html( get_post_meta( $prod_id, $spec_meta, true ) );
		}
		else if ( $spec_type == 'star-rating' ) {

			// get our actual spec value
			$editor_rating = esc_html( get_post_meta( $prod_id, $spec_meta, true ) );

			$spec_value = '';

			if ( $editor_rating && $editor_rating != 'no-rating' ) {

				$editor_rating_num = (float) $editor_rating;

				$galfram_prp_amazon_api = new galfram_productreviewpro_amazon_api();
				$stars = $galfram_prp_amazon_api->get_star_rating_html( $editor_rating_num );

				if ( $stars != '' ) {

					$spec_value .= '<div class="editor-rating custom-rating spec-star-rating" itemprop="reviewRating" itemscope itemtype="http://schema.org/Rating">';
						$spec_value .= '<span>'.$stars.'</span>';

						$fname = get_the_author_meta('first_name');
						$lname = get_the_author_meta('last_name');

						$spec_value .= '<meta itemprop="author" content="'.$fname.' '.$lname.'">';
						$spec_value .= '<meta itemprop="bestRating" content="5">';
						$spec_value .= '<meta itemprop="worstRating" content="0">';

					$spec_value  .= '</div>';

				}

			}

		}

		else if ( $spec_type == 'yes-no' ) {

			// get our actual spec value
			$yesno = esc_html( get_post_meta( $prod_id, $spec_meta, true ) );

			$spec_value = '<div class="specs-table-row yes-no spec-yes-no">';
				
				if ( $yesno == 'yes' ) {
					$spec_value .= '<i class="fa fa-check"></i>';
				}
				elseif ( $yesno == 'no' ) {
					$spec_value .= '<i class="fa fa-close"></i>';
				}

			$spec_value .= '</div>';

		}

		else if ( $spec_type == 'image' ) {
			// get our actual spec value
			$thumb_id = esc_html( get_post_meta( $prod_id, $spec_meta, true ) );
			$thumb_url_array = wp_get_attachment_image_src($thumb_id, 'large', true);
			$thumb_url = $thumb_url_array[0];

			$image_alt = get_post_meta( $thumb_id, '_wp_attachment_image_alt', true);

			$spec_value = '<img class="galfram-review-spec-img" src="'.$thumb_url.'" alt="'.$image_alt.'" />';
			
		}

		else if ( $spec_type == 'price' ) {

			$spec_value = '';

			$listprice = get_post_meta( $prod_id, $spec_meta.'_listprice' , true);
			$saleprice = get_post_meta( $prod_id, $spec_meta.'_saleprice' , true);
			$percentagesaved = get_post_meta( $prod_id, $spec_meta.'_percentagediscounted' , true);
			$amountsaved = get_post_meta( $prod_id, $spec_meta.'_amountdiscounted' , true);

			$spec_value .=  '<div class="specs-table-row price spec-price" itemprop="offers" itemscope itemtype="http://schema.org/Offer">';

				// if data for list price or lowest new price is set to display
				if ( $listprice != '' && $saleprice != '' ) {

					// display the slashed out list price and the lower new price
					$spec_value .=  '<span class="slashed-price"><span>'.$listprice.'</span></span>';
					$spec_value .=  '<br />';
					$spec_value .=  '<span class="deal-price" itemprop="price">'.$saleprice.'</span>';

					if ( $percentagesaved != '' && $amountsaved != '' ) {
						$spec_value .=  '<span class="prod-amount-saved">'.__('Save', 'ultimateazon2').' '.$percentagesaved.' ('.$amountsaved.')</span>';
					}
					else if ( $percentagesaved != '' && $amountsaved == '' ) {
						$spec_value .=  '<span class="prod-amount-saved">'.__('Save', 'ultimateazon2').' '.$percentagesaved.'</span>';
					}
					else if ( $percentagesaved == '' && $amountsaved != '' ) {
						$spec_value .=  '<span class="prod-amount-saved">'.__('Save', 'ultimateazon2').' '.$amountsaved.'</span>';
					}

				}

				// if list price data is set to display, but lowest new price is not
				else if ( $listprice != '' && $saleprice == '' ) {
					// display only the list price
					$spec_value .=  '<span class="deal-price" itemprop="price">'.$listprice.'</span>';
				}
				// if list price data is NOT set to display, but lowest new price is
				else if ( $listprice == '' && $saleprice != '' ) {
					// display only the lowest new price
					$spec_value .=  '<span class="deal-price" itemprop="price">'.$saleprice.'</span>';
				}

			$spec_value .=  '</div>';
			
		}

		else if ( $spec_type == 'spec-button' ) {

			$spec_value = '<div class="specs-table-row spec-button">';
				$public_this = new galfram_productreviewpro_amazon_api();
				$spec_value .=  $public_this->galfram_get_spec_button_html( $prod_id, $spec_meta );
			$spec_value .=  '</div>';

		}

		else if ( $spec_type == 'spec-shortcode' ) {

			$spec_value = '<div class="specs-table-row spec-shortcode">';
				$spec_shortcode = get_post_meta( $prod_id, $spec_meta , true);
				$spec_value .= do_shortcode($spec_shortcode);
			$spec_value .= '</div>';

		}

		return $spec_value;

	}








	/** Get our amazon spec name clean for displaying in the slider specs
	 *
	 * @since    1.0.0
	 */
	public function get_galfram_ama_spec_name_clean( $ama_type ) {

		if ( $ama_type == 'wp-title' ) {
			$ama_name = __('Title', 'ultimateazon2');
		}
		if ( $ama_type == 'amazon-title' ) {
			$ama_name = __('Product', 'ultimateazon2');
		}
		if ( $ama_type == 'editor-rating' ) {
			$ama_name = __('Editor\'s Rating', 'ultimateazon2');
		}
		if ( $ama_type == 'asin' ) {
			$ama_name = __('ASIN', 'ultimateazon2');
		}
		if ( $ama_type == 'brand' ) {
			$ama_name = __('Brand', 'ultimateazon2');
		}
		if ( $ama_type == 'model' ) {
			$ama_name = __('Model', 'ultimateazon2');
		}
		if ( $ama_type == 'upc' ) {
			$ama_name = __('UPC', 'ultimateazon2');
		}
		if ( $ama_type == 'features' ) {
			$ama_name = __('Features', 'ultimateazon2');
		}
		if ( $ama_type == 'warranty' ) {
			$ama_name = __('Warranty', 'ultimateazon2');
		}
		if ( $ama_type == 'price' ) {
			$ama_name = __('Price', 'ultimateazon2');
		}
		if ( $ama_type == 'lowest-new-price' ) {
			$ama_name = __('Lowest New Price', 'ultimateazon2');
		}
		if ( $ama_type == 'lowest-used-price' ) {
			$ama_name = __('Lowest Used Price', 'ultimateazon2');
		}
		if ( $ama_type == 'small-image' ) {
			$ama_name = __('Image', 'ultimateazon2');
		}
		if ( $ama_type == 'medium-image' ) {
			$ama_name = __('Image', 'ultimateazon2');
		}
		if ( $ama_type == 'large-image' ) {
			$ama_name = __('Image', 'ultimateazon2');
		}

		return $ama_name;

	}




	


}


function in_array_r($needle, $haystack, $strict = false) {
    foreach ($haystack as $item) {
        if (($strict ? $item === $needle : $item == $needle) || (is_array($item) && in_array_r($needle, $item, $strict))) {
            return true;
        }
    }

    return false;
}



 /* Duplicate 'the_content' filters
 * 
 * @author Bill Erickson
 * @link http://www.billerickson.net/code/duplicate-the_content-filters/
 */
global $wp_embed;
add_filter( 'uat_the_content', array( $wp_embed, 'run_shortcode' ), 8 );
add_filter( 'uat_the_content', array( $wp_embed, 'autoembed'     ), 8 );
add_filter( 'uat_the_content', 'wptexturize'        );
add_filter( 'uat_the_content', 'convert_chars'      );
add_filter( 'uat_the_content', 'wpautop'            );
add_filter( 'uat_the_content', 'shortcode_unautop'  );
add_filter( 'uat_the_content', 'do_shortcode'       );



