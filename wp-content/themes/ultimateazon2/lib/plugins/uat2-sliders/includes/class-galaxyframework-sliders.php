<?php

/**
 * The file that defines the core plugin class
 *
 * A class definition that includes attributes and functions used across both the
 * public-facing side of the site and the admin area.
 *
 * @link       https://incomegalaxy.com
 * @since      1.0.0
 *
 * @package    galfram_Sliders
 * @subpackage galfram_Pluginname/includes
 */

/**
 * The core plugin class.
 *
 * This is used to define internationalization, admin-specific hooks, and
 * public-facing site hooks.
 *
 * Also maintains the unique identifier of this plugin as well as the current
 * version of the plugin.
 *
 * @since      1.0.0
 * @package    galfram_Sliders
 * @subpackage galfram_Pluginname/includes
 * @author     Your Name <email@example.com>
 */
class galfram_sliders {

	/**
	 * The loader that's responsible for maintaining and registering all hooks that power
	 * the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      galfram_sliders_Loader    $loader    Maintains and registers all hooks for the plugin.
	 */
	protected $loader;

	/**
	 * The unique identifier of this plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $galfram_sliders    The string used to uniquely identify this plugin.
	 */
	protected $galfram_sliders;

	/**
	 * The current version of the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $version    The current version of the plugin.
	 */
	protected $version;

	/**
	 * Define the core functionality of the plugin.
	 *
	 * Set the plugin name and the plugin version that can be used throughout the plugin.
	 * Load the dependencies, define the locale, and set the hooks for the admin area and
	 * the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function __construct() {

		$this->galfram_sliders = 'galaxyframework-sliders';
		$this->version = '0.1.0';
		$this->edd_product = 'EDD EXACT PRODUCT NAME';
		$this->edd_store = 'https://incomegalaxy.com';

		$this->load_dependencies();
		$this->set_locale();
		$this->define_admin_hooks();
		$this->define_public_hooks();

	}

	/**
	 * Load the required dependencies for this plugin.
	 *
	 * Include the following files that make up the plugin:
	 *
	 * - galfram_sliders_Loader. Orchestrates the hooks of the plugin.
	 * - galfram_sliders_i18n. Defines internationalization functionality.
	 * - galfram_sliders_Admin. Defines all hooks for the admin area.
	 * - galfram_sliders_Public. Defines all hooks for the public side of the site.
	 *
	 * Create an instance of the loader which will be used to register the hooks
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function load_dependencies() {

		/**
		 * The class responsible for orchestrating the actions and filters of the
		 * core plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-galaxyframework-sliders-loader.php';

		/**
		 * The class responsible for defining internationalization functionality
		 * of the plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-galaxyframework-sliders-i18n.php';

		/**
		 * The class responsible for defining all actions that occur in the admin area.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/class-galaxyframework-sliders-admin.php';

		/**
		 * The class responsible for defining all actions that occur in the public-facing
		 * side of the site.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'public/class-galaxyframework-sliders-public.php';


		/**
		 * 
		 *
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-galaxyframework-sliders-functions.php';


		// if( !class_exists( 'GALFRAM_ACB_Plugin_Updater' ) ) {
		// 	// load our custom updater
		// 	require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/partials/GALFRAM_ACB_Plugin_Updater.php';
		// }

		$this->loader = new galfram_sliders_Loader();

	}

	/**
	 * Define the locale for this plugin for internationalization.
	 *
	 * Uses the galfram_sliders_i18n class in order to set the domain and to register the hook
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function set_locale() {

		$plugin_i18n = new galfram_sliders_i18n();

		$this->loader->add_action( 'plugins_loaded', $plugin_i18n, 'load_plugin_textdomain' );

	}

	/**
	 * Register all of the hooks related to the admin area functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_admin_hooks() {

		$plugin_admin = new galfram_sliders_Admin( $this->get_galfram_sliders(), $this->get_version(), $this->edd_product, $this->edd_store );

		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_styles' );

		// load ACF fields for this plugin
		$this->loader->add_action( 'acf/init', $plugin_admin, 'galfram_pluginnameshort_include_acf_fields' );
		

		// Add our plugin options page
		$this->loader->add_action( 'after_setup_theme', $plugin_admin, 'gf_ext_sliders_add_style_options_page' );

		// Add our post types and taxonomies
		$this->loader->add_action( 'init', $plugin_admin, 'gf_ext_sliders_add_post_types' );


		// add our user generated spec groups to a select field
		$this->loader->add_filter( 'acf/load_field/name=choose_specification_group', $plugin_admin, 'galfram_slider_choose_spec_group');

		// add our chosen spec group fields to our select field to choose the top field in the Pro Tool
		$this->loader->add_filter( 'acf/load_field/name=choose_top_product_field', $plugin_admin, 'galfram_protool_choose_top_spec');
		$this->loader->add_filter( 'acf/load_field/name=choose_top_product_field_prp', $plugin_admin, 'galfram_protool_choose_top_spec');

		// add a repeater field to add spec columns to our TableSlide Pro Tool
		$this->loader->add_action( 'acf/init', $plugin_admin, 'galfram_sliders_add_spec_rows' );
		// add a repeater field to add spec columns to our PRP slider
		$this->loader->add_action( 'acf/init', $plugin_admin, 'galfram_sliders_add_spec_rows_prp' );


		// create a repeater field to add reviews to the ProTool Slider
		$this->loader->add_action( 'init', $plugin_admin, 'galfram_sliders_add_reviews' );
		// create a repeater field to add reviews to the PRP slider
		$this->loader->add_action( 'init', $plugin_admin, 'galfram_sliders_add_reviews_prp' );

		// create a repeater field to add reviews to the PRP slider
		$this->loader->add_action( 'init', $plugin_admin, 'galfram_sliders_add_spec_rows_prp_allamazon' );

		// create a repeater field to add reviews to the PROTOOL slider
		$this->loader->add_action( 'init', $plugin_admin, 'galfram_sliders_add_spec_rows_protool_allamazon' );


		//$this->loader->add_action( 'admin_footer', $plugin_admin, 'add_amazon_search_box' );

		
		

	}

	/**
	 * Register all of the hooks related to the public-facing functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_public_hooks() {

		$plugin_public = new galfram_sliders_Public( $this->get_galfram_sliders(), $this->get_version() );

		// add product review post type single loop function
		$this->loader->add_action( 'wp', $plugin_public, 'get_galfram_slider_post_type_template' );

		// add our shortcode function for displaying a table
		$this->loader->add_shortcode( 'uat2_slider', $plugin_public, 'galfram_layout_slider' );

		$this->loader->add_action( 'galfram_add_custom_ext_css', $plugin_public, 'galfram_add_custom_ext_css_sliders' );


	}

	/**
	 * Run the loader to execute all of the hooks with WordPress.
	 *
	 * @since    1.0.0
	 */
	public function run() {
		$this->loader->run();
	}

	/**
	 * The name of the plugin used to uniquely identify it within the context of
	 * WordPress and to define internationalization functionality.
	 *
	 * @since     1.0.0
	 * @return    string    The name of the plugin.
	 */
	public function get_galfram_sliders() {
		return $this->galfram_sliders;
	}

	/**
	 * The reference to the class that orchestrates the hooks with the plugin.
	 *
	 * @since     1.0.0
	 * @return    galfram_sliders_Loader    Orchestrates the hooks of the plugin.
	 */
	public function get_loader() {
		return $this->loader;
	}

	/**
	 * Retrieve the version number of the plugin.
	 *
	 * @since     1.0.0
	 * @return    string    The version number of the plugin.
	 */
	public function get_version() {
		return $this->version;
	}

}
