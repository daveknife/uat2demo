<?php

/**
 * Fired during plugin deactivation
 *
 * @link       https://incomegalaxy.com
 * @since      1.0.0
 *
 * @package    galfram_Sliders
 * @subpackage galfram_Pluginname/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    galfram_Sliders
 * @subpackage galfram_Pluginname/includes
 * @author     Your Name <email@example.com>
 */
class galfram_sliders_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

		// Let's set the 'galaxyframework-sliders' key to "off" when we deactivate this plugin
		$galfram_checker = get_option( 'galfram_checker' );
    	$galfram_checker['galaxyframework-sliders'] = 'off';
    	update_option( 'galfram_checker', $galfram_checker );

	}

}
