<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    galfram_Sliders
 * @subpackage galfram_Pluginname/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    galfram_Sliders
 * @subpackage galfram_Pluginname/admin
 * @author     Your Name <email@example.com>
 */
class galfram_sliders_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $galfram_sliders    The ID of this plugin.
	 */
	private $galfram_sliders;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $galfram_sliders       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $galfram_sliders, $version, $edd_product, $edd_store ) {

		$this->galfram_sliders = $galfram_sliders;
		$this->version = $version;
		$this->edd_product = $edd_product;
		$this->edd_store = $edd_store;

	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in galfram_sliders_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The galfram_sliders_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->galfram_sliders, get_stylesheet_directory_uri() . '/lib/plugins/uat2-sliders/admin/css/galaxyframework-sliders-admin.css', array(), $this->version, 'all' );

	}

	




    /**
	 * Create ACF Styles option page after theme setup
	 *
	 * @since    1.0.0
	 */
	function gf_ext_sliders_add_style_options_page() {
		// Add Site Styles Page
		if( function_exists('acf_add_options_page') ) {
			$option_page = acf_add_options_sub_page(array(
				'page_title' 	=> __('Advanced Sliders - Settings', 'ultimateazon2'),
				'menu_title' 	=> __('Sliders', 'ultimateazon2'),
				'menu_slug' 	=> 'extension-sliders-settings',
				'capability' 	=> 'edit_posts',
				'post_id'       => 'extension-sliders-settings',
				'parent_slug' 	=> 'theme-settings',
				'position'      => 350,
				'redirect' 	=> false
			));
		}
	}






	/**
	 * Create slider post type
	 *
	 * @since    1.0.0
	 */
	function gf_ext_sliders_add_post_types() {


	    $plural_name = __('Sliders', 'ultimateazon2');
	    $singular_name = __('Slider', 'ultimateazon2');


	    register_post_type( 'galfram_slider',
	        array(
	            'labels' => array(
	                'name' => $plural_name,
	                'singular_name' => $singular_name,
	                'add_new' => __('Add New', 'ultimateazon2'),
	                'add_new_item' => __('Add New','ultimateazon2').' '.$singular_name,
	                'edit' => __('Edit','ultimateazon2'),
	                'edit_item' => __('Edit','ultimateazon2').' '.$singular_name,
	                'new_item' => __('New','ultimateazon2').' '.$singular_name,
	                'view' => __('View','ultimateazon2'),
	                'view_item' => __('View','ultimateazon2').' '.$singular_name,
	                'search_items' => __('Search','ultimateazon2').' '.$plural_name,
	                'not_found' => __('No','ultimateazon2').' '.$plural_name.__('found','ultimateazon2'),
	                'not_found_in_trash' => __('No','ultimateazon2').' '.$plural_name.' '.__('found in Trash','ultimateazon2'),
	                'parent' => __('Parent','ultimateazon2').' '.$singular_name
	            ),
	            'public' => true,
	            'show_in_menu' => true,
	            'menu_position' => 7,
	            'supports' => array( 'title', 'author', 'revisions', 'editor' ),
	            'taxonomies' => array( '' ),
	            'menu_icon' => 'dashicons-arrow-right-alt',
	            'has_archive' => false
	        )
	    );

	}






	/**
	 * Add our user created specification groups to the select choices if the table type is set too prp-table
	 * EXTENDS PRODUCT REVIEW PRO
	 *
	 * @since    1.0.0
	 */
	public function galfram_slider_choose_spec_group( $field ) {

		// get user saved spefs fields array
		$user_specs_groups = get_option( 'galfram_ext_prp_all_userspecs_groups' );

		// set empty choices
    	$field['choices'] = array();
    	$choices = array();

    	$choices['pull-from-amazon'] = __('Amazon API Only Products', 'ultimateazon2' );

    	if ( $user_specs_groups ) {

			foreach ( $user_specs_groups as $group ) {
				$option_val = $group['field_57c35f7e8031b'];
				$option_txt = $group['field_57c068df622b6'];
				$choices[$option_val] = $option_txt;
			}

			// remove any unwanted white space
		    $choices = array_map('trim', $choices);

		    // loop through array and add to field 'choices'
		    if( is_array($choices) ) {
		        foreach( $choices as $value => $choice ) {
		            $field['choices'][ $value ] = $choice;
		        }
		    }

		}
		    
	    return $field;

	} // end public function galfram_table_choose_spec_group()






	/**
	 * Add our chosen spec group fields to our select field to choose the top field in the Pro Tool
	 * EXTENDS PRODUCT REVIEW PRO
	 *
	 * @since    1.0.0
	 */
	public function galfram_protool_choose_top_spec( $field ) {


		if ( !isset( $_GET ) || !isset( $_POST ) ) {
			return;
		}

		if ( isset( $_GET['post'] ) ) {
			$post_id = $_GET['post'];
		}
		elseif ( isset( $_POST['post_ID'] ) ) {
			$post_id = $_POST['post_ID'];
		}
		else {
			return;
		}

		if ( get_post_type($post_id) != 'galfram_slider' ) {
			return;
		}

		// set empty choices
    	$field['choices'] = array();
    	$choices = array();

		// get the chosen spec group for this table
		$specs_group_id = get_post_meta($post_id, 'choose_specification_group', true);


		if ( $specs_group_id && $specs_group_id != 'pull-from-amazon' ) {

			// create a new instance of our sliders class if it exists
			$galfram_sliders_functions = new galfram_sliders_functions(); 
			// get just our chosen spec group array
			$spec_choices = unserialize( $galfram_sliders_functions->get_galfram_chosenspec_fields( $specs_group_id ) );

		    // loop through array and add to field 'choices'
		    if( is_array($spec_choices) ) {
		        foreach( $spec_choices as $value => $choice ) {
		            $field['choices'][ $value ] = $choice['name'];
		        }
		    }

		}

		else if ( $specs_group_id && $specs_group_id == 'pull-from-amazon' ) {

			$field['choices']['wp-title'] = __('WP Title', 'ultimateazon2' );
			$field['choices']['amazon-title'] = __('Amazon Title', 'ultimateazon2' );
			$field['choices']['editor-rating'] = __('Editor\'s Rating', 'ultimateazon2' );
			$field['choices']['asin'] = __('ASIN', 'ultimateazon2' );
			$field['choices']['brand'] = __('Brand', 'ultimateazon2' );
			$field['choices']['model'] = __('Model', 'ultimateazon2' );
			$field['choices']['upc'] = __('UPC', 'ultimateazon2' );
			$field['choices']['features'] = __('Features', 'ultimateazon2' );
			$field['choices']['warranty'] = __('Warranty', 'ultimateazon2' );
			$field['choices']['price'] = __('Price', 'ultimateazon2' );
			$field['choices']['lowest-new-price'] = __('Lowest New Price', 'ultimateazon2' );
			$field['choices']['lowest-used-price'] = __('Lowest Used Price', 'ultimateazon2' );
			$field['choices']['small-image'] = __('Small Image', 'ultimateazon2' );
			$field['choices']['medium-image'] = __('Medium Image', 'ultimateazon2' );
			$field['choices']['large-image'] = __('Large Image', 'ultimateazon2' );

		}

		return $field;

	}






	/**
	 * Create the subfield for adding rows of products
	 * EXTENDS PRODUCT REVIEW PRO
	 *
	 * @since    1.0.0
	 */
	public function galfram_sliders_add_reviews() {


		if ( !isset( $_GET ) || !isset( $_POST ) ) {
			return;
		}

		if( isset( $_GET['post'] ) ){
		    $post_id = $_GET['post'];
		} elseif ( isset( $_POST['post_ID'] ) ){
		    $post_id = $_POST['post_ID'];
		} else {
		    return;
		}

		if ( get_post_type($post_id) != 'galfram_slider' ) {
			return;
		}

		// get the saved spec group for this table and create our value
		$specs_group_id = get_post_meta( $post_id, 'choose_specification_group', true );

		$args = array (
			'post_type' => 'galfram_review',
			'posts_per_page' => -1,
			'orderby' => 'title',
			'order' => 'ASC',
			'tax_query' => array(
				array(
					'taxonomy' => 'galfram_spec_group',
					'field' => 'slug',
					'terms' => $specs_group_id,
				)
			),

		);

		$reviews = get_posts($args);
		$review_choices = array();

		if ( $reviews ) {
			foreach ( $reviews as $review ) {
				$review_choices[$review->ID] = $review->post_title;
			}
		}
		else {
			$review_choices['no-results'] = __('No products Using This Specification Group', 'ultimateazon2');
		}


		// let's add the subfield to the repeater now with our new choices for our columns
		acf_add_local_field(array(
			'key' => 'field_hgybug76b7b35e',
			'label' => __('Slide', 'ultimateazon2'),
			'name' => 'galfram_slider_column_prod',
			'type' => 'select',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'choices' => $review_choices,
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'maxlength' => '',
			'parent' => 'field_58cabbd1bf136'
		));

	}





	/**
	 * Create the subfield for adding rows of products
	 * EXTENDS PRODUCT REVIEW PRO
	 *
	 * @since    1.0.0
	 */
	public function galfram_sliders_add_reviews_prp() {


		if ( !isset( $_GET ) || !isset( $_POST ) ) {
			return;
		}

		if( isset( $_GET['post'] ) ){
		    $post_id = $_GET['post'];
		} elseif ( isset( $_POST['post_ID'] ) ){
		    $post_id = $_POST['post_ID'];
		} else {
		    return;
		}

		if ( get_post_type($post_id) != 'galfram_slider' ) {
			return;
		}

		// get the saved spec group for this table and create our value
		$specs_group_id = get_post_meta( $post_id, 'choose_specification_group', true );

		$args = array (
			'post_type' => 'galfram_review',
			'posts_per_page' => -1,
			'orderby' => 'title',
			'order' => 'ASC',
			'tax_query' => array(
				array(
					'taxonomy' => 'galfram_spec_group',
					'field' => 'slug',
					'terms' => $specs_group_id,
				)
			),

		);

		$reviews = get_posts($args);
		$review_choices = array();

		if ( $reviews ) {
			foreach ( $reviews as $review ) {
				$review_choices[$review->ID] = $review->post_title;
			}
		}
		else {
			$review_choices['no-results'] = __('No products Using This Specification Group', 'ultimateazon2');
		}


		// let's add the subfield to the repeater now with our new choices for our columns
		acf_add_local_field(array(
			'key' => 'field_hgybug76b7b35e',
			'label' => __('Slide', 'ultimateazon2'),
			'name' => 'galfram_slider_column_prod',
			'type' => 'select',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'choices' => $review_choices,
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'maxlength' => '',
			'parent' => 'field_w982nx9n3882h87b'
		));

	}





	/**
	 * Create repeater rows for what specs you want to display in the Pro Tool's left specs column
	 * EXTENDS PRODUCT REVIEW PRO
	 *
	 * @since    1.0.0
	 */
	public function galfram_sliders_add_spec_rows() {


		if ( !isset( $_GET ) || !isset( $_POST ) ) {
			return;
		}

		if ( isset( $_GET['post'] ) ) {
			$post_id = $_GET['post'];
		}
		elseif ( isset( $_POST['post_ID'] ) ) {
			$post_id = $_POST['post_ID'];
		}
		else {
			return;
		}

		if ( get_post_type($post_id) != 'galfram_slider' ) {
			return;
		}


		// get the chosen spec group for this table
		$specs_group_id = get_post_meta($post_id, 'choose_specification_group', true);


		if ( $specs_group_id && $specs_group_id != 'pull-from-amazon' ) {

			// create a new instance of our sliders class if it exists
			$galfram_sliders_functions = new galfram_sliders_functions(); 
			// get just our chosen spec group array
			$spec_choices = unserialize( $galfram_sliders_functions->get_galfram_chosenspec_fields( $specs_group_id ) );

			// get just our spec names, we don't need the types
			if ( $spec_choices && is_array($spec_choices) ) {

				foreach ( $spec_choices as $choice => $value ) {
					$spec_choices_clean[ $choice ] = $value['name'];
				}

				// let's add the subfield to the repeater now with our new cjoices for our columns
				acf_add_local_field(array(
					'key' => 'field_76yh76t76tb76tg',
					'label' => __('Field', 'ultimateazon2'),
					'name' => 'column',
					'type' => 'select',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array (
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'choices' => $spec_choices_clean,
					'default_value' => '',
					'placeholder' => '',
					'prepend' => '',
					'append' => '',
					'maxlength' => '',
					'parent' => 'field_58cade2e0aa3e'
				));

				acf_add_local_field(array(
					'key' => 'field_876g8765f74d56',
					'label' => __('Field Alignment', 'ultimateazon2'),
					'name' => 'field_alignment_protool',
					'type' => 'select',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array (
						'width' => '20',
						'class' => '',
						'id' => '',
					),
					'choices' => array (
						'field-align-left' => __('Left', 'ultimateazon2'),
						'field-align-right' => __('Right', 'ultimateazon2'),
						'field-align-center' => __('Center', 'ultimateazon2'),
					),
					'default_value' => '',
					'placeholder' => '',
					'prepend' => '',
					'append' => '',
					'maxlength' => '',
					'parent' => 'field_58cade2e0aa3e'
				));

				acf_add_local_field(array(
					'key' => 'field_a9a8s8s7d65',
					'label' => __('Link', 'ultimateazon2'),
					'name' => 'field_link',
					'type' => 'select',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array (
						'width' => '20',
						'class' => '',
						'id' => '',
					),
					'choices' => array (
						'field-link-none' => __('No Link', 'ultimateazon2'),
						'field-link-review' => __('Full Review Link', 'ultimateazon2'),
						'field-link-affiliate' => __('Affiliate Link', 'ultimateazon2'),
					),
					'default_value' => '',
					'placeholder' => '',
					'prepend' => '',
					'append' => '',
					'maxlength' => '',
					'parent' => 'field_58cade2e0aa3e'
				));

			}

		}

		elseif ( $specs_group_id && $specs_group_id == 'pull-from-amazon' ) {

			$spec_choices = array (
				'wp-title' => __('WP Title', 'ultimateazon2' ),
				'amazon-title' => __('Amazon Title', 'ultimateazon2' ),
				'editor-rating' => __('Editor\'s Rating', 'ultimateazon2' ),
				'asin' => __('ASIN', 'ultimateazon2' ),
				'brand' => __('Brand', 'ultimateazon2' ),
				'model' => __('Model', 'ultimateazon2' ),
				'upc' => __('UPC', 'ultimateazon2' ),
				'features' => __('Features', 'ultimateazon2' ),
				'warranty' => __('Warranty', 'ultimateazon2' ),
				'price' => __('Price', 'ultimateazon2' ),
				'lowest-new-price' => __('Lowest New Price', 'ultimateazon2' ),
				'lowest-used-price' => __('Lowest Used Price', 'ultimateazon2' ),
				'small-image' => __('Small Image', 'ultimateazon2' ),
				'medium-image' => __('Medium Image', 'ultimateazon2' ),
				'large-image' => __('Large Image', 'ultimateazon2' ),
			);

			acf_add_local_field(array(
				'key' => 'field_yh76t76tb76t6',
				'label' => __('Fields', 'ultimateazon2'),
				'name' => 'column',
				'type' => 'select',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array (
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'choices' => $spec_choices,
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'maxlength' => '',
				'parent' => 'field_58cade2e0aa3e'
			));

			acf_add_local_field(array(
				'key' => 'field_876g8765f74d56',
				'label' => __('Field Alignment', 'ultimateazon2'),
				'name' => 'field_alignment_protool',
				'type' => 'select',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array (
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'choices' => array (
					'field-align-left' => __('Left', 'ultimateazon2'),
					'field-align-right' => __('Right', 'ultimateazon2'),
					'field-align-center' => __('Center', 'ultimateazon2'),
				),
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'maxlength' => '',
				'parent' => 'field_58cade2e0aa3e'
			));

			acf_add_local_field(array(
				'key' => 'field_a9a8s8s7d65',
				'label' => __('Link', 'ultimateazon2'),
				'name' => 'field_link',
				'type' => 'select',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array (
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'choices' => array (
					'field-link-none' => __('No Link', 'ultimateazon2'),
					'field-link-review' => __('Full Review Link', 'ultimateazon2'),
					'field-link-affiliate' => __('Affiliate Link', 'ultimateazon2'),
				),
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'maxlength' => '',
				'parent' => 'field_58cade2e0aa3e'
			));

		}
		else {
			return;
		}

	}

















	/**
	 * Create repeater rows for what specs you want to display in the Pro Tool's left specs column
	 * EXTENDS PRODUCT REVIEW PRO
	 *
	 * @since    1.0.0
	 */
	public function galfram_sliders_add_spec_rows_prp() {


		if ( !isset( $_GET ) || !isset( $_POST ) ) {
			return;
		}

		if ( isset( $_GET['post'] ) ) {
			$post_id = $_GET['post'];
		}
		elseif ( isset( $_POST['post_ID'] ) ) {
			$post_id = $_POST['post_ID'];
		}
		else {
			return;
		}

		if ( get_post_type($post_id) != 'galfram_slider' ) {
			return;
		}


		$slider_type = get_post_meta( $post_id, 'product_slider_types', true);


		if ( $slider_type == 'choose-pull-amazon' ) {

			$spec_choices_ama = array (
				'amazon-title' => __('Amazon Title', 'ultimateazon2' ),
				'asin' => __('ASIN', 'ultimateazon2' ),
				'brand' => __('Brand', 'ultimateazon2' ),
				'model' => __('Model', 'ultimateazon2' ),
				'upc' => __('UPC', 'ultimateazon2' ),
				'features' => __('Features', 'ultimateazon2' ),
				'warranty' => __('Warranty', 'ultimateazon2' ),
				'price' => __('Price', 'ultimateazon2' ),
				'lowest-new-price' => __('Lowest New Price', 'ultimateazon2' ),
				'lowest-used-price' => __('Lowest Used Price', 'ultimateazon2' ),
				'small-image' => __('Small Image', 'ultimateazon2' ),
				'medium-image' => __('Medium Image', 'ultimateazon2' ),
				'large-image' => __('Large Image', 'ultimateazon2' ),
			);

			acf_add_local_field(array(
				'key' => 'field_t545v45t4t4t45ctuyg',
				'label' => __('Fields', 'ultimateazon2'),
				'name' => 'column',
				'type' => 'select',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array (
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'choices' => $spec_choices_ama,
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'maxlength' => '',
				'parent' => 'field_58cade2e0aa3e7hy87hy876'
			));

			acf_add_local_field(array(
				'key' => 'field_768v75c489b7v',
				'label' => __('Alignment', 'ultimateazon2'),
				'name' => 'field_alignment',
				'type' => 'select',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array (
					'width' => '20',
					'class' => '',
					'id' => '',
				),
				'choices' => array (
					'field-align-left' => __('Left', 'ultimateazon2'),
					'field-align-right' => __('Right', 'ultimateazon2'),
					'field-align-center' => __('Center', 'ultimateazon2'),
				),
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'maxlength' => '',
				'parent' => 'field_58cade2e0aa3e7hy87hy876'
			));

			acf_add_local_field(array(
				'key' => 'field_78y8767665e65r6tvuy',
				'label' => __('Link', 'ultimateazon2'),
				'name' => 'field_link',
				'type' => 'select',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array (
					'width' => '20',
					'class' => '',
					'id' => '',
				),
				'choices' => array (
					'field-link-none' => __('No Link', 'ultimateazon2'),
					'field-link-affiliate' => __('Affiliate Link', 'ultimateazon2'),
				),
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'maxlength' => '',
				'parent' => 'field_58cade2e0aa3e7hy87hy876'
			));

			acf_add_local_field(array (
				'key' => 'field_9b7v6c5x34zubu',
				'label' => __('Show Label', 'ultimateazon2'),
				'name' => 'show_label',
				'type' => 'true_false',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array (
					'width' => '10',
					'class' => '',
					'id' => '',
				),
				'message' => '',
				'default_value' => 0,
				'parent' => 'field_58cade2e0aa3e7hy87hy876'
			));

		}

		else {

			// get the chosen spec group for this table
			$specs_group_id = get_post_meta($post_id, 'choose_specification_group', true);

			if ( $specs_group_id && $specs_group_id != 'pull-from-amazon' ) {

				// create a new instance of our sliders class if it exists
				$galfram_sliders_functions = new galfram_sliders_functions(); 
				// get just our chosen spec group array
				$spec_choices = unserialize( $galfram_sliders_functions->get_galfram_chosenspec_fields( $specs_group_id ) );


				if ( $spec_choices && is_array($spec_choices) ) {

					// get just our spec names, we don't need the types
					foreach ( $spec_choices as $choice => $value ) {
						$spec_choices_clean[ $choice ] = $value['name'];
					}

					// let's add the subfield to the repeater now with our new cjoices for our columns
					acf_add_local_field(array(
						'key' => 'field_76th78g876g78g',
						'label' => __('Field', 'ultimateazon2'),
						'name' => 'column',
						'type' => 'select',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'choices' => $spec_choices_clean,
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'maxlength' => '',
						'parent' => 'field_h876h867g867bg87'
					));

					acf_add_local_field(array(
						'key' => 'field_5f65f6ryuyttuy',
						'label' => __('Alignment', 'ultimateazon2'),
						'name' => 'field_alignment',
						'type' => 'select',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '20',
							'class' => '',
							'id' => '',
						),
						'choices' => array (
							'field-align-left' => __('Left', 'ultimateazon2'),
							'field-align-right' => __('Right', 'ultimateazon2'),
							'field-align-center' => __('Center', 'ultimateazon2'),
						),
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'maxlength' => '',
						'parent' => 'field_h876h867g867bg87'
					));

					acf_add_local_field(array(
						'key' => 'field_78y8767665e65r6tvuy',
						'label' => __('Link', 'ultimateazon2'),
						'name' => 'field_link',
						'type' => 'select',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '20',
							'class' => '',
							'id' => '',
						),
						'choices' => array (
							'field-link-none' => __('No Link', 'ultimateazon2'),
							'field-link-review' => __('Full Review Link', 'ultimateazon2'),
							'field-link-affiliate' => __('Affiliate Link', 'ultimateazon2'),
						),
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'maxlength' => '',
						'parent' => 'field_h876h867g867bg87'
					));

					acf_add_local_field(array (
						'key' => 'field_8768796986897gh',
						'label' => __('Show Label', 'ultimateazon2'),
						'name' => 'show_label',
						'type' => 'true_false',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '10',
							'class' => '',
							'id' => '',
						),
						'message' => '',
						'default_value' => 0,
						'parent' => 'field_h876h867g867bg87'
					));

				}
					
			}

			elseif ( $specs_group_id && $specs_group_id == 'pull-from-amazon' ) {

				$spec_choices_ama = array (
					'wp-title' => __('WP Title', 'ultimateazon2' ),
					'amazon-title' => __('Amazon Title', 'ultimateazon2' ),
					'editor-rating' => __('Editor\'s Rating', 'ultimateazon2' ),
					'asin' => __('ASIN', 'ultimateazon2' ),
					'brand' => __('Brand', 'ultimateazon2' ),
					'model' => __('Model', 'ultimateazon2' ),
					'upc' => __('UPC', 'ultimateazon2' ),
					'features' => __('Features', 'ultimateazon2' ),
					'warranty' => __('Warranty', 'ultimateazon2' ),
					'price' => __('Price', 'ultimateazon2' ),
					'lowest-new-price' => __('Lowest New Price', 'ultimateazon2' ),
					'lowest-used-price' => __('Lowest Used Price', 'ultimateazon2' ),
					'small-image' => __('Small Image', 'ultimateazon2' ),
					'medium-image' => __('Medium Image', 'ultimateazon2' ),
					'large-image' => __('Large Image', 'ultimateazon2' ),
				);

				acf_add_local_field(array(
					'key' => 'field_76th78g876g78g',
					'label' => __('Fields', 'ultimateazon2'),
					'name' => 'column',
					'type' => 'select',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array (
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'choices' => $spec_choices_ama,
					'default_value' => '',
					'placeholder' => '',
					'prepend' => '',
					'append' => '',
					'maxlength' => '',
					'parent' => 'field_h876h867g867bg87'
				));

				acf_add_local_field(array(
					'key' => 'field_3b37ebr87ttnt',
					'label' => __('Alignment', 'ultimateazon2'),
					'name' => 'field_alignment',
					'type' => 'select',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array (
						'width' => '20',
						'class' => '',
						'id' => '',
					),
					'choices' => array (
						'field-align-left' => __('Left', 'ultimateazon2'),
						'field-align-right' => __('Right', 'ultimateazon2'),
						'field-align-center' => __('Center', 'ultimateazon2'),
					),
					'default_value' => '',
					'placeholder' => '',
					'prepend' => '',
					'append' => '',
					'maxlength' => '',
					'parent' => 'field_h876h867g867bg87'
				));

				acf_add_local_field(array(
					'key' => 'field_78y8767665e65r6tvuy',
					'label' => __('Link', 'ultimateazon2'),
					'name' => 'field_link',
					'type' => 'select',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array (
						'width' => '20',
						'class' => '',
						'id' => '',
					),
					'choices' => array (
						'field-link-none' => __('No Link', 'ultimateazon2'),
						'field-link-review' => __('Full Review Link', 'ultimateazon2'),
						'field-link-affiliate' => __('Affiliate Link', 'ultimateazon2'),
					),
					'default_value' => '',
					'placeholder' => '',
					'prepend' => '',
					'append' => '',
					'maxlength' => '',
					'parent' => 'field_h876h867g867bg87'
				));

				acf_add_local_field(array (
					'key' => 'field_86tv876v865v54c6',
					'label' => __('Show Label', 'ultimateazon2'),
					'name' => 'show_label',
					'type' => 'true_false',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array (
						'width' => '10',
						'class' => '',
						'id' => '',
					),
					'message' => '',
					'default_value' => 0,
					'parent' => 'field_h876h867g867bg87'
				));

			}
			else {
				return;
			}

		}

	}








	/**
	 * Create repeater rows for what specs you want to display in the Pro Tool's left specs column
	 * EXTENDS PRODUCT REVIEW PRO
	 *
	 * @since    1.0.0
	 */
	public function galfram_sliders_add_spec_rows_prp_allamazon() {


		if ( !isset( $_GET ) || !isset( $_POST ) ) {
			return;
		}

		if ( isset( $_GET['post'] ) ) {
			$post_id = $_GET['post'];
		}
		elseif ( isset( $_POST['post_ID'] ) ) {
			$post_id = $_POST['post_ID'];
		}
		else {
			return;
		}

		if ( get_post_type($post_id) != 'galfram_slider' ) {
			return;
		}

		$slider_type = get_post_meta( $post_id, 'protool_slider_types', true);


		if ( $slider_type == 'choose-pull-amazon' ) {

			$spec_choices_ama = array (
				'amazon-title' => __('Amazon Title', 'ultimateazon2' ),
				'asin' => __('ASIN', 'ultimateazon2' ),
				'brand' => __('Brand', 'ultimateazon2' ),
				'model' => __('Model', 'ultimateazon2' ),
				'upc' => __('UPC', 'ultimateazon2' ),
				'features' => __('Features', 'ultimateazon2' ),
				'warranty' => __('Warranty', 'ultimateazon2' ),
				'price' => __('Price', 'ultimateazon2' ),
				'lowest-new-price' => __('Lowest New Price', 'ultimateazon2' ),
				'lowest-used-price' => __('Lowest Used Price', 'ultimateazon2' ),
				'small-image' => __('Small Image', 'ultimateazon2' ),
				'medium-image' => __('Medium Image', 'ultimateazon2' ),
				'large-image' => __('Large Image', 'ultimateazon2' ),
			);

			acf_add_local_field(array(
				'key' => 'field_87tv765c654x6e',
				'label' => __('Fields', 'ultimateazon2'),
				'name' => 'column',
				'type' => 'select',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array (
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'choices' => $spec_choices_ama,
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'maxlength' => '',
				'parent' => 'field_yf6f6d5s4s49h8h'
			));

			acf_add_local_field(array(
				'key' => 'field_65765c564453345xe5x',
				'label' => __('Alignment', 'ultimateazon2'),
				'name' => 'field_alignment',
				'type' => 'select',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array (
					'width' => '20',
					'class' => '',
					'id' => '',
				),
				'choices' => array (
					'field-align-left' => __('Left', 'ultimateazon2'),
					'field-align-right' => __('Right', 'ultimateazon2'),
					'field-align-center' => __('Center', 'ultimateazon2'),
				),
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'maxlength' => '',
				'parent' => 'field_yf6f6d5s4s49h8h'
			));

			acf_add_local_field(array(
				'key' => 'field_78y8767665e65r6tvuy',
				'label' => __('Link', 'ultimateazon2'),
				'name' => 'field_link',
				'type' => 'select',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array (
					'width' => '20',
					'class' => '',
					'id' => '',
				),
				'choices' => array (
					'field-link-none' => __('No Link', 'ultimateazon2'),
					'field-link-affiliate' => __('Affiliate Link', 'ultimateazon2'),
				),
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'maxlength' => '',
				'parent' => 'field_yf6f6d5s4s49h8h'
			));

		}

		else {
			return;
		}

	}







	/**
	 * Create repeater rows for what specs you want to display in the Pro Tool's left specs column
	 * EXTENDS PRODUCT REVIEW PRO
	 *
	 * @since    1.0.0
	 */
	public function galfram_sliders_add_spec_rows_protool_allamazon() {


		if ( !isset( $_GET ) || !isset( $_POST ) ) {
			return;
		}

		if ( isset( $_GET['post'] ) ) {
			$post_id = $_GET['post'];
		}
		elseif ( isset( $_POST['post_ID'] ) ) {
			$post_id = $_POST['post_ID'];
		}
		else {
			return;
		}

		if ( get_post_type($post_id) != 'galfram_slider' ) {
			return;
		}

		$slider_type = get_post_meta( $post_id, 'product_slider_types', true);


		if ( $slider_type == 'choose-pull-amazon' ) {

			$spec_choices_ama = array (
				'amazon-title' => __('Amazon Title', 'ultimateazon2' ),
				'asin' => __('ASIN', 'ultimateazon2' ),
				'brand' => __('Brand', 'ultimateazon2' ),
				'model' => __('Model', 'ultimateazon2' ),
				'upc' => __('UPC', 'ultimateazon2' ),
				'features' => __('Features', 'ultimateazon2' ),
				'warranty' => __('Warranty', 'ultimateazon2' ),
				'price' => __('Price', 'ultimateazon2' ),
				'lowest-new-price' => __('Lowest New Price', 'ultimateazon2' ),
				'lowest-used-price' => __('Lowest Used Price', 'ultimateazon2' ),
				'small-image' => __('Small Image', 'ultimateazon2' ),
				'medium-image' => __('Medium Image', 'ultimateazon2' ),
				'large-image' => __('Large Image', 'ultimateazon2' ),
			);

			acf_add_local_field(array(
				'key' => 'field_t545v45t4t4t45ctuyg',
				'label' => __('Fields', 'ultimateazon2'),
				'name' => 'column',
				'type' => 'select',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array (
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'choices' => $spec_choices_ama,
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'maxlength' => '',
				'parent' => 'field_58cade2e0aa3e7hy87hy876'
			));

		}

		else {
			return;
		}

	}


 
	





	/**
	 * Include the ACF fields
	 *
	 * @since    1.0.0
	 */
	public function galfram_pluginnameshort_include_acf_fields() {


		if( function_exists('acf_add_local_field_group') ):

			acf_add_local_field_group(array (
				'key' => 'group_58c9955b14efe',
				'title' => 'Slider Single Settings',
				'fields' => array (
					array (
						'key' => 'field_58c9956950a1e',
						'label' => __('Type of Slider', 'ultimateazon2' ),
						'name' => 'type_of_slider',
						'type' => 'select',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'choices' => array (
							'posts' => __('Posts Slider', 'ultimateazon2' ),
							'pages' => __('Pages Slider', 'ultimateazon2' ),
							'custom' => __('Custom Content Slider', 'ultimateazon2' ),
							'galfram-prp' => __('Product Reviews Slider', 'ultimateazon2' ),
							'galfram-tableslide' => __('Ultimate Comparison Pro', 'ultimateazon2' ),
						),
						'default_value' => array (
						),
						'allow_null' => 1,
						'multiple' => 0,
						'ui' => 0,
						'ajax' => 0,
						'return_format' => 'value',
						'placeholder' => '',
					),
					array (
						'key' => 'field_58c9e7e2827be',
						'label' => __('Posts Slider Settings', 'ultimateazon2' ),
						'name' => '',
						'type' => 'tab',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_58c9956950a1e',
									'operator' => '==',
									'value' => 'posts',
								),
							),
						),
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'placement' => 'top',
						'endpoint' => 0,
					),
					array (
						'key' => 'field_58c996dde3f13',
						'label' => __('Posts Source', 'ultimateazon2' ),
						'name' => 'posts_source',
						'type' => 'select',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_58c9956950a1e',
									'operator' => '==',
									'value' => 'posts',
								),
							),
						),
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'choices' => array (
							'latest' => __('Latest Posts', 'ultimateazon2' ),
							'manual' => __('Choose Specific Posts', 'ultimateazon2' ),
						),
						'default_value' => array (
						),
						'allow_null' => 0,
						'multiple' => 0,
						'ui' => 0,
						'ajax' => 0,
						'return_format' => 'value',
						'placeholder' => '',
					),
					array (
						'key' => 'field_58c997eb795ea',
						'label' => __('Latest Posts Count', 'ultimateazon2' ),
						'name' => 'latest_posts_count',
						'type' => 'number',
						'instructions' => __('Choose how many of the latest posts you want to display in this slider.', 'ultimateazon2' ),
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_58c9956950a1e',
									'operator' => '==',
									'value' => 'posts',
								),
								array (
									'field' => 'field_58c996dde3f13',
									'operator' => '==',
									'value' => 'latest',
								),
							),
						),
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => 'posts',
						'min' => '1',
						'max' => '',
						'step' => '',
					),
					array (
						'key' => 'field_58c9e4d2b8905',
						'label' => __('Choose Posts Fields', 'ultimateazon2' ),
						'name' => 'choose_posts_fields',
						'type' => 'repeater',
						'instructions' => __('The fields are displayed vertically in this order.', 'ultimateazon2' ),
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_58c9956950a1e',
									'operator' => '==',
									'value' => 'posts',
								),
							),
						),
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'collapsed' => '',
						'min' => '',
						'max' => '',
						'layout' => 'table',
						'button_label' => __('Add Field', 'ultimateazon2' ),
						'sub_fields' => array (
							array (
								'key' => 'field_58caaa7776795',
								'label' => __('Field Type', 'ultimateazon2' ),
								'name' => 'field_type',
								'type' => 'select',
								'instructions' => '',
								'required' => 0,
								'conditional_logic' => 0,
								'wrapper' => array (
									'width' => '',
									'class' => '',
									'id' => '',
								),
								'choices' => array (
									'image' => __('Post Featured Image', 'ultimateazon2' ),
									'title' => __('Post Title', 'ultimateazon2' ),
									'excerpt' => __('Post Excerpt', 'ultimateazon2' ),
									'date' => __('Publish Date', 'ultimateazon2' ),
									'categories' => __('Post Categories', 'ultimateazon2' ),
									'tags' => __('Post Tags', 'ultimateazon2' ),
									'author' => __('Post Author', 'ultimateazon2' ),
								),
								'default_value' => array (
								),
								'allow_null' => 1,
								'multiple' => 0,
								'ui' => 0,
								'ajax' => 0,
								'return_format' => 'value',
								'placeholder' => '',
							),
							array(
								'key' => 'field_987jgh876v6g6v',
								'label' => __('Field Alignment', 'ultimateazon2'),
								'name' => 'field_alignment_posts',
								'type' => 'select',
								'instructions' => '',
								'required' => 0,
								'conditional_logic' => 0,
								'wrapper' => array (
									'width' => '',
									'class' => '',
									'id' => '',
								),
								'choices' => array (
									'field-align-left' => __('Left', 'ultimateazon2' ),
									'field-align-right' => __('Right', 'ultimateazon2' ),
									'field-align-center' => __('Center', 'ultimateazon2' ),
								),
								'default_value' => '',
								'placeholder' => '',
								'prepend' => '',
								'append' => '',
								'maxlength' => '',
							),
						),
					),
					array (
						'key' => 'field_58c9973de3f14',
						'label' => __('Choose Posts', 'ultimateazon2' ),
						'name' => 'choose_posts',
						'type' => 'repeater',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_58c9956950a1e',
									'operator' => '==',
									'value' => 'posts',
								),
								array (
									'field' => 'field_58c996dde3f13',
									'operator' => '==',
									'value' => 'manual',
								),
							),
						),
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'collapsed' => 'field_58c9975ae3f16',
						'min' => '',
						'max' => '',
						'layout' => 'row',
						'button_label' => __('Add Post', 'ultimateazon2' ),
						'sub_fields' => array (
							array (
								'key' => 'field_58c9975ae3f16',
								'label' => __('Add a Post', 'ultimateazon2' ),
								'name' => 'add_a_post',
								'type' => 'post_object',
								'instructions' => '',
								'required' => 0,
								'conditional_logic' => 0,
								'wrapper' => array (
									'width' => '',
									'class' => '',
									'id' => '',
								),
								'post_type' => array (
									0 => 'post',
								),
								'taxonomy' => array (
								),
								'allow_null' => 0,
								'multiple' => 0,
								'return_format' => 'id',
								'ui' => 1,
							),
						),
					),
					array (
						'key' => 'field_58c9e804827bf',
						'label' => __('Pages Slider Settings', 'ultimateazon2' ),
						'name' => '',
						'type' => 'tab',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_58c9956950a1e',
									'operator' => '==',
									'value' => 'pages',
								),
							),
						),
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'placement' => 'top',
						'endpoint' => 0,
					),
					array (
						'key' => 'field_58c9e790827bd',
						'label' => __('Choose Pages Fields', 'ultimateazon2' ),
						'name' => 'choose_pages_fields',
						'type' => 'repeater',
						'instructions' => __('The fields are displayed vertically in this order. Drag up and down to reorder the fields.', 'ultimateazon2' ),
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_58c9956950a1e',
									'operator' => '==',
									'value' => 'pages',
								),
							),
						),
						'wrapper' => array (
							'width' => '100',
							'class' => '',
							'id' => '',
						),
						'collapsed' => '',
						'min' => '',
						'max' => '',
						'layout' => 'table',
						'button_label' => __('Add Field', 'ultimateazon2' ),
						'sub_fields' => array (
							array (
								'key' => 'field_58caabc1f16c3',
								'label' => __('Page Field', 'ultimateazon2' ),
								'name' => 'page_field',
								'type' => 'select',
								'instructions' => '',
								'required' => 0,
								'conditional_logic' => 0,
								'wrapper' => array (
									'width' => '',
									'class' => '',
									'id' => '',
								),
								'choices' => array (
									'image' => __('Page Featured Image', 'ultimateazon2' ),
									'title' => __('Page Title', 'ultimateazon2' ),
									'excerpt' => __('Page Excerpt', 'ultimateazon2' ),
									'date' => __('Publish Date', 'ultimateazon2' ),
									'author' => __('Post Author', 'ultimateazon2' ),
								),
								'default_value' => array (
								),
								'allow_null' => 1,
								'multiple' => 0,
								'ui' => 0,
								'ajax' => 0,
								'return_format' => 'value',
								'placeholder' => '',
							),
							array(
								'key' => 'field_dgdgdy38838238dh',
								'label' => __('Field Alignment', 'ultimateazon2'),
								'name' => 'field_alignment_pages',
								'type' => 'select',
								'instructions' => '',
								'required' => 0,
								'conditional_logic' => 0,
								'wrapper' => array (
									'width' => '',
									'class' => '',
									'id' => '',
								),
								'choices' => array (
									'field-align-left' => __('Left', 'ultimateazon2' ),
									'field-align-right' => __('Right', 'ultimateazon2' ),
									'field-align-center' => __('Center', 'ultimateazon2' ),
								),
								'default_value' => '',
								'placeholder' => '',
								'prepend' => '',
								'append' => '',
								'maxlength' => '',
							),
						),
					),
					array (
						'key' => 'field_58c99865f7514',
						'label' => __('Choose Pages', 'ultimateazon2' ),
						'name' => 'choose_pages',
						'type' => 'repeater',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_58c9956950a1e',
									'operator' => '==',
									'value' => 'pages',
								),
							),
						),
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'collapsed' => 'field_58c9975ae3f16',
						'min' => '',
						'max' => '',
						'layout' => 'row',
						'button_label' => __('Add Post', 'ultimateazon2' ),
						'sub_fields' => array (
							array (
								'key' => 'field_58c99865f7515',
								'label' => __('Add a Page', 'ultimateazon2' ),
								'name' => 'add_a_page',
								'type' => 'post_object',
								'instructions' => '',
								'required' => 0,
								'conditional_logic' => 0,
								'wrapper' => array (
									'width' => '',
									'class' => '',
									'id' => '',
								),
								'post_type' => array (
									0 => 'page',
								),
								'taxonomy' => array (
								),
								'allow_null' => 0,
								'multiple' => 0,
								'return_format' => 'id',
								'ui' => 1,
							),
						),
					),
					array (
						'key' => 'field_58c9e84a827c1',
						'label' => __('Custom Content Slider Settings', 'ultimateazon2' ),
						'name' => '',
						'type' => 'tab',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_58c9956950a1e',
									'operator' => '==',
									'value' => 'custom',
								),
							),
						),
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'placement' => 'top',
						'endpoint' => 0,
					),
					array (
						'key' => 'field_58c998ce53f45',
						'label' => __('Add Custom Slides', 'ultimateazon2' ),
						'name' => 'add_custom_slides',
						'type' => 'repeater',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_58c9956950a1e',
									'operator' => '==',
									'value' => 'custom',
								),
							),
						),
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'collapsed' => '',
						'min' => '',
						'max' => '',
						'layout' => 'row',
						'button_label' => __('Add Slide', 'ultimateazon2' ),
						'sub_fields' => array (
							array (
								'key' => 'field_58c998e653f46',
								'label' => __('Slide Content', 'ultimateazon2' ),
								'name' => 'slide_content',
								'type' => 'wysiwyg',
								'instructions' => '',
								'required' => 0,
								'conditional_logic' => 0,
								'wrapper' => array (
									'width' => '',
									'class' => '',
									'id' => '',
								),
								'default_value' => '',
								'tabs' => 'all',
								'toolbar' => 'full',
								'media_upload' => 1,
							),
						),
					),
















					array (
						'key' => 'field_58c9e82d827c0',
						'label' => __('Product Reviews Slider Settings', 'ultimateazon2' ),
						'name' => '',
						'type' => 'tab',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_58c9956950a1e',
									'operator' => '==',
									'value' => 'galfram-prp',
								),
							),
						),
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'placement' => 'top',
						'endpoint' => 0,
					),
					array (
						'key' => 'field_58c9a3959c660',
						'label' => __('Product Review Slider Types', 'ultimateazon2' ),
						'name' => 'product_slider_types',
						'type' => 'select',
						'instructions' => '<p style="color:red;">'.__('If pulling all products directly from amazon, you must update this post to make your display fields available.', 'ultimateazon2' ).'</p>',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_58c9956950a1e',
									'operator' => '==',
									'value' => 'galfram-prp',
								),
							),
						),
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'choices' => array (
							// 'choose-all-products' => 'Choose From All Products',
							'choose-spec-group' => __('Choose Specification Group', 'ultimateazon2' ),
							'choose-pull-amazon' => __('Pull all Products directly from Amazon', 'ultimateazon2' ),
						),
						'default_value' => array (
						),
						'allow_null' => 0,
						'multiple' => 0,
						'ui' => 0,
						'ajax' => 0,
						'return_format' => 'value',
						'placeholder' => '',
					),
					array (
						'key' => 'field_58cade2e0aa3e7hy87hy876',
						'label' => __('Choose Slides Fields', 'ultimateazon2' ),
						'name' => 'choose_products_fields_specgroup_prp_allamazon',
						'type' => 'repeater',
						'instructions' => __('The fields are displayed vertically in this order within each slide. Drag vertically to reorder them. 4444', 'ultimateazon2' ),
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_58c9956950a1e',
									'operator' => '==',
									'value' => 'galfram-prp',
								),
								array (
									'field' => 'field_58c9a3959c660',
									'operator' => '==',
									'value' => 'choose-pull-amazon',
								),
							),
						),
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'collapsed' => '',
						'min' => '',
						'max' => '',
						'layout' => 'table',
						'button_label' => __('Add Field', 'ultimateazon2' ),
						'sub_fields' => array (
						),
					),
					array (
						'key' => 'field_3e4rc35tv5t45t',
						'label' => __('Add Amazon Product Slides', 'ultimateazon2' ),
						'name' => 'add_amazon_slides_prp',
						'type' => 'repeater',
						'instructions' => __('Drag to reorder your slides.', 'ultimateazon2' ),
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_58c9956950a1e',
									'operator' => '==',
									'value' => 'galfram-prp',
								),
								array (
									'field' => 'field_58c9a3959c660',
									'operator' => '==',
									'value' => 'choose-pull-amazon',
								),
							),
						),
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'collapsed' => '',
						'min' => '',
						'max' => '',
						'layout' => 'row',
						'button_label' => __('Add Field', 'ultimateazon2' ),
						'sub_fields' => array (
							array (
								'key' => 'field_54tv5t46tv46yvt46',
								'label' => __('Chosen Amazon ASIN', 'ultimateazon2' ),
								'name' => 'ama_asin',
								'type' => 'text',
								'instructions' => '<a class="manual-search-amazon thickbox" href="#TB_inline?width=630&height=500&inlineId=galfram-amazon-lookup"><strong>'.__('Search Amazon', 'ultimateazon2' ).'</strong></a>',
								'required' => 0,
								'conditional_logic' => 0,
								'wrapper' => array (
									'width' => '',
									'class' => '',
									'id' => '',
								),
								'default_value' => '',
								'placeholder' => '',
								'prepend' => '',
								'append' => '',
								'maxlength' => '',
							),
						),
					),
					array (
						'key' => 'field_n78y87nx278n8ynwie',
						'label' => __('Choose Specification Group', 'ultimateazon2' ),
						'name' => 'choose_specification_group',
						'type' => 'select',
						'instructions' => '<p style="color:red;">'.__('You must update this post after choosing "Choose Specification Group" to make the group\'s fields available.', 'ultimateazon2' ).'</p>',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_58c9956950a1e',
									'operator' => '==',
									'value' => 'galfram-prp',
								),
								array (
									'field' => 'field_58c9a3959c660',
									'operator' => '==',
									'value' => 'choose-spec-group',
								),
							),
						),
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'choices' => array (
							'pull-from-amazon' => __('Amazon API Only Products', 'ultimateazon2' ),
						),
						'default_value' => array (
						),
						'allow_null' => 1,
						'multiple' => 0,
						'ui' => 0,
						'ajax' => 0,
						'return_format' => 'value',
						'placeholder' => '',
					),
					array (
						'key' => 'field_h876h867g867bg87',
						'label' => __('Choose Slides Fields', 'ultimateazon2' ),
						'name' => 'choose_products_fields_specgroup_prp',
						'type' => 'repeater',
						'instructions' => __('The fields are displayed vertically in this order within each slide. Drag vertically to reorder them. 1111', 'ultimateazon2' ),
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_58c9956950a1e',
									'operator' => '==',
									'value' => 'galfram-prp',
								),
								array (
									'field' => 'field_58c9a3959c660',
									'operator' => '==',
									'value' => 'choose-spec-group',
								),
							),
						),
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'collapsed' => '',
						'min' => '',
						'max' => '',
						'layout' => 'table',
						'button_label' => __('Add Field', 'ultimateazon2' ),
						'sub_fields' => array (
						),
					),
					// array (
					// 	'key' => 'field_58c9f2a89ea31',
					// 	'label' => 'Choose Slides Fields',
					// 	'name' => 'choose_products_fields',
					// 	'type' => 'checkbox',
					// 	'instructions' => 'The fields are displayed vertically in this order. Check which ones you want to display in the slider. 2222',
					// 	'required' => 0,
					// 	'conditional_logic' => array (
					// 		array (
					// 			array (
					// 				'field' => 'field_58c9956950a1e',
					// 				'operator' => '==',
					// 				'value' => 'galfram-prp',
					// 			),
					// 			array (
					// 				'field' => 'field_58c9a3959c660',
					// 				'operator' => '==',
					// 				'value' => 'choose-all-products',
					// 			),
					// 		),
					// 	),
					// 	'wrapper' => array (
					// 		'width' => '50',
					// 		'class' => '',
					// 		'id' => '',
					// 	),
					// 	'choices' => array (
					// 		'image' => 'Product Review Featured Image',
					// 		'title' => 'Product Review Title',
					// 		'categories' => 'Product Categories',
					// 		'custom' => 'Custom Field',
					// 	),
					// 	'default_value' => array (
					// 		0 => 'image',
					// 		1 => 'title',
					// 	),
					// 	'layout' => 'vertical',
					// 	'toggle' => 0,
					// 	'return_format' => 'value',
					// ),
					array (
						'key' => 'field_u6tc765654x56',
						'label' => __('Product Review Featured Image Link', 'ultimateazon2' ),
						'name' => 'prp_link_image',
						'type' => 'select',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_58c9956950a1e',
									'operator' => '==',
									'value' => 'galfram-prp',
								),
								array (
									'field' => 'field_58c9a3959c660',
									'operator' => '==',
									'value' => 'choose-all-products',
								),
							),
						),
						'wrapper' => array (
							'width' => '50',
							'class' => '',
							'id' => '',
						),
						'choices' => array (
							'field-link-none' => __('No Link', 'ultimateazon2' ),
							'field-link-review' => __('Full Review Link', 'ultimateazon2' ),
							'field-link-affiliate' => __('Affiliate Link', 'ultimateazon2' ),
						),
						'default_value' => array (
						),
						'layout' => 'vertical',
						'toggle' => 0,
						'return_format' => 'value',
					),
					array (
						'key' => 'field_7y8675rce4xtv',
						'label' => __('Product Review Title Link', 'ultimateazon2' ),
						'name' => 'prp_link_title',
						'type' => 'select',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_58c9956950a1e',
									'operator' => '==',
									'value' => 'galfram-prp',
								),
								array (
									'field' => 'field_58c9a3959c660',
									'operator' => '==',
									'value' => 'choose-all-products',
								),
							),
						),
						'wrapper' => array (
							'width' => '50',
							'class' => '',
							'id' => '',
						),
						'choices' => array (
							'field-link-none' => __('No Link', 'ultimateazon2' ),
							'field-link-review' => __('Full Review Link', 'ultimateazon2' ),
							'field-link-affiliate' => __('Affiliate Link', 'ultimateazon2' ),
						),
						'default_value' => array (
						),
						'layout' => 'vertical',
						'toggle' => 0,
						'return_format' => 'value',
					),
					// array(
					// 	'key' => 'field_t7876v876t7b7hhjb',
					// 	'label' => __('Field Alignment', 'ultimateazon2'),
					// 	'name' => 'field_alignment_all_prods',
					// 	'type' => 'select',
					// 	'instructions' => '',
					// 	'required' => 0,
					// 	'conditional_logic' => 0,
					// 	'wrapper' => array (
					// 		'width' => '',
					// 		'class' => '',
					// 		'id' => '',
					// 	),
					// 	'choices' => array (
					// 		'field-align-left' => __('Left', 'ultimateazon2' ),
					// 		'field-align-right' => __('Right', 'ultimateazon2' ),
					// 		'field-align-center' => __('Center', 'ultimateazon2' ),
					// 	),
					// 	'default_value' => '',
					// 	'placeholder' => '',
					// 	'prepend' => '',
					// 	'append' => '',
					// 	'maxlength' => '',
					// ),
					array (
						'key' => 'field_w982nx9n3882h87b',
						'label' => __('Choose Slides from your chosen Spec Group', 'ultimateazon2' ),
						'name' => 'choose_products_from_specs_prp',
						'type' => 'repeater',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_58c9956950a1e',
									'operator' => '==',
									'value' => 'galfram-prp',
								),
								array (
									'field' => 'field_58c9a3959c660',
									'operator' => '==',
									'value' => 'choose-spec-group',
								),
							),
						),
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'collapsed' => '',
						'min' => '',
						'max' => '',
						'layout' => 'row',
						'button_label' => __('Add Slide', 'ultimateazon2' ),
						'sub_fields' => array (
						),
					),
					array (
						'key' => 'field_58c9efe6a1ef0',
						'label' => __('Add Slides', 'ultimateazon2' ),
						'name' => 'add_products',
						'type' => 'repeater',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_58c9956950a1e',
									'operator' => '==',
									'value' => 'galfram-prp',
								),
								array (
									'field' => 'field_58c9a3959c660',
									'operator' => '==',
									'value' => 'choose-all-products',
								),
							),
						),
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'collapsed' => 'field_58c9f004a1ef2',
						'min' => '',
						'max' => '',
						'layout' => 'row',
						'button_label' => __('Add Slide', 'ultimateazon2' ),
						'sub_fields' => array (
							array (
								'key' => 'field_58c9f004a1ef2',
								'label' => __('Choose Slide', 'ultimateazon2' ),
								'name' => 'choose_product',
								'type' => 'post_object',
								'instructions' => __('Title, Image, & Categories get pulled from this product review.', 'ultimateazon2' ),
								'required' => 0,
								'conditional_logic' => 0,
								'wrapper' => array (
									'width' => '',
									'class' => '',
									'id' => '',
								),
								'post_type' => array (
									0 => 'galfram_review',
								),
								'taxonomy' => array (
								),
								'allow_null' => 0,
								'multiple' => 0,
								'return_format' => 'id',
								'ui' => 1,
							),
							array (
								'key' => 'field_58c9f34632a68',
								'label' => __('Custom Field', 'ultimateazon2' ),
								'name' => 'custom_field',
								'type' => 'wysiwyg',
								'instructions' => '',
								'required' => 0,
								'conditional_logic' => array (
									array (
										array (
											'field' => 'field_58c9f2a89ea31',
											'operator' => '==',
											'value' => 'custom',
										),
									),
								),
								'wrapper' => array (
									'width' => '',
									'class' => '',
									'id' => '',
								),
								'default_value' => '',
								'tabs' => 'all',
								'toolbar' => 'full',
								'media_upload' => 1,
							),
						),
					),






















					array (
						'key' => 'field_58c9e87c827c2',
						'label' => __('Ultimate Comparison Pro Settings', 'ultimateazon2' ),
						'name' => '',
						'type' => 'tab',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_58c9956950a1e',
									'operator' => '==',
									'value' => 'galfram-tableslide',
								),
							),
						),
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'placement' => 'top',
						'endpoint' => 0,
					),
					array (
						'key' => 'field_t87658758758766gu',
						'label' => __('ProTool Slider Types', 'ultimateazon2' ),
						'name' => 'protool_slider_types',
						'type' => 'select',
						'instructions' => '<p style="color:red;">'.__('If pulling all products directly from amazon, you must update this post to make your display fields available.', 'ultimateazon2' ).'</p>',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_58c9956950a1e',
									'operator' => '==',
									'value' => 'galfram-tableslide',
								),
							),
						),
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'choices' => array (
							'choose-spec-group' => __('Choose Specification Group', 'ultimateazon2' ),
							'choose-pull-amazon' => __('Pull all Products directly from Amazon', 'ultimateazon2' ),
						),
						'default_value' => array (
						),
						'allow_null' => 0,
						'multiple' => 0,
						'ui' => 0,
						'ajax' => 0,
						'return_format' => 'value',
						'placeholder' => '',
					),
					array (
						'key' => 'field_58c9f72ae66a1',
						'label' => __('Choose Specification Group', 'ultimateazon2' ),
						'name' => 'choose_specification_group',
						'type' => 'select',
						'instructions' => '<p style="color:red;">'.__('You must update this post after choosing "Choose Specification Group" to make the group\'s fields available.', 'ultimateazon2' ).'</p>',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_58c9956950a1e',
									'operator' => '==',
									'value' => 'galfram-tableslide',
								),
								array (
									'field' => 'field_t87658758758766gu',
									'operator' => '==',
									'value' => 'choose-spec-group',
								),
							),
						),
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'choices' => array (
						),
						'default_value' => array (
						),
						'allow_null' => 1,
						'multiple' => 0,
						'ui' => 0,
						'ajax' => 0,
						'return_format' => 'value',
						'placeholder' => '',
					),
					array (
						'key' => 'field_h6t7g76r5g75g756r',
						'label' => __('Choose Top Slide Field', 'ultimateazon2' ),
						'name' => 'choose_top_product_field',
						'type' => 'select',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_58c9956950a1e',
									'operator' => '==',
									'value' => 'galfram-tableslide',
								),
							),
						),
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'choices' => array (),
						'default_value' => array (
						),
						'allow_null' => 1,
						'multiple' => 0,
						'ui' => 0,
						'ajax' => 0,
						'return_format' => 'value',
						'placeholder' => '',
					),
					array (
						'key' => 'field_58cade2e0aa3e',
						'label' => __('Choose Slides Fields', 'ultimateazon2' ),
						'name' => 'choose_products_fields_specgroup',
						'type' => 'repeater',
						'instructions' => __('The fields are displayed vertically in this order within each slide. Drag vertically to reorder them. 3333', 'ultimateazon2' ),
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_58c9956950a1e',
									'operator' => '==',
									'value' => 'galfram-tableslide',
								),
								array (
									'field' => 'field_t87658758758766gu',
									'operator' => '==',
									'value' => 'choose-spec-group',
								),
							),
						),
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'collapsed' => '',
						'min' => '',
						'max' => '',
						'layout' => 'table',
						'button_label' => __('Add Field', 'ultimateazon2' ),
						'sub_fields' => array (
						),
					),
					array (
						'key' => 'field_yf6f6d5s4s49h8h',
						'label' => __('Choose Slides Fields', 'ultimateazon2' ),
						'name' => 'choose_products_fields_specgroup_protool_allamazon',
						'type' => 'repeater',
						'instructions' => __('The fields are displayed vertically in this order within each slide. Drag vertically to reorder them. 5555', 'ultimateazon2' ),
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_58c9956950a1e',
									'operator' => '==',
									'value' => 'galfram-tableslide',
								),
								array (
									'field' => 'field_t87658758758766gu',
									'operator' => '==',
									'value' => 'choose-pull-amazon',
								),
							),
						),
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'collapsed' => '',
						'min' => '',
						'max' => '',
						'layout' => 'table',
						'button_label' => __('Add Field', 'ultimateazon2' ),
						'sub_fields' => array (
						),
					),
					array (
						'key' => 'field_58cabbd1bf136',
						'label' => __('Choose Slides from your chosen Spec Group', 'ultimateazon2' ),
						'name' => 'choose_products_from_specs',
						'type' => 'repeater',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_58c9956950a1e',
									'operator' => '==',
									'value' => 'galfram-tableslide',
								),
								array (
									'field' => 'field_t87658758758766gu',
									'operator' => '==',
									'value' => 'choose-spec-group',
								),
							),
						),
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'collapsed' => '',
						'min' => '',
						'max' => '',
						'layout' => 'row',
						'button_label' => __('Add Slide', 'ultimateazon2' ),
						'sub_fields' => array (
						),
					),
					array (
						'key' => 'field_54654c765v876b987b',
						'label' => __('Add Amazon Product Slides', 'ultimateazon2' ),
						'name' => 'add_amazon_slides_protool',
						'type' => 'repeater',
						'instructions' => __('Drag to reorder your slides.', 'ultimateazon2' ),
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_58c9956950a1e',
									'operator' => '==',
									'value' => 'galfram-tableslide',
								),
								array (
									'field' => 'field_t87658758758766gu',
									'operator' => '==',
									'value' => 'choose-pull-amazon',
								),
							),
						),
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'collapsed' => '',
						'min' => '',
						'max' => '',
						'layout' => 'row',
						'button_label' => __('Add Field', 'ultimateazon2' ),
						'sub_fields' => array (
							array (
								'key' => 'field_54tv5t46tv46yvt46',
								'label' => __('Chosen Amazon ASIN', 'ultimateazon2' ),
								'name' => 'ama_asin',
								'type' => 'text',
								'instructions' => '<a class="manual-search-amazon thickbox" href="#TB_inline?width=630&height=500&inlineId=galfram-amazon-lookup"><strong>'.__('Search Amazon', 'ultimateazon2' ).'</strong></a>',
								'required' => 0,
								'conditional_logic' => 0,
								'wrapper' => array (
									'width' => '',
									'class' => '',
									'id' => '',
								),
								'default_value' => '',
								'placeholder' => '',
								'prepend' => '',
								'append' => '',
								'maxlength' => '',
							),
						),
					),
				),
				'location' => array (
					array (
						array (
							'param' => 'post_type',
							'operator' => '==',
							'value' => 'galfram_slider',
						),
					),
				),
				'menu_order' => 0,
				'position' => 'normal',
				'style' => 'default',
				'label_placement' => 'top',
				'instruction_placement' => 'label',
				'hide_on_screen' => '',
				'active' => 1,
				'description' => '',
				'local' => 'php',
			));












			/*****************************************
			*********** SLIDER GLOBAL STYLES *********
			*****************************************/


			acf_add_local_field_group(array (
				'key' => 'group_591dfb6445739',
				'title' => __('Global Advanced Slider Settings', 'ultimateazon2' ),
				'fields' => array (
					array (
						'key' => 'field_591dfca34083a',
						'label' => __('Small Screens', 'ultimateazon2' ),
						'name' => '',
						'type' => 'message',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'message' => '',
						'new_lines' => 'wpautop',
						'esc_html' => 0,
					),
					array (
						'key' => 'field_591dff06fcd2e',
						'label' => __('Slides to Show (small screens)', 'ultimateazon2' ),
						'name' => 'slides_to_show_sm',
						'type' => 'number',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '50',
							'class' => '',
							'id' => '',
						),
						'default_value' => 1,
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'min' => 1,
						'max' => '',
						'step' => '',
					),
					array (
						'key' => 'field_591dffa0fcd31',
						'label' => __('Slides to Scroll (small screens)', 'ultimateazon2' ),
						'name' => 'slides_to_scroll_sm',
						'type' => 'number',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '50',
							'class' => '',
							'id' => '',
						),
						'default_value' => 1,
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'min' => 1,
						'max' => '',
						'step' => '',
					),
					array (
						'key' => 'field_591e004772cc8',
						'label' => __('Hide Slider Dots (small screens)', 'ultimateazon2' ),
						'name' => 'hide_slider_dots_sm',
						'type' => 'true_false',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '50',
							'class' => '',
							'id' => '',
						),
						'message' => __('Check this box to hide the slider dots.', 'ultimateazon2' ),
						'default_value' => 0,
					),
					array (
						'key' => 'field_591e01bc11664',
						'label' => __('Hide Slider Arrows (small screens)', 'ultimateazon2' ),
						'name' => 'hide_slider_arrows_sm',
						'type' => 'true_false',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '50',
							'class' => '',
							'id' => '',
						),
						'message' => __('Check this box to hide the slider arrows.', 'ultimateazon2' ),
						'default_value' => 0,
					),
					array (
						'key' => 'field_591e022511666',
						'label' => __('Medium Screens', 'ultimateazon2' ),
						'name' => '',
						'type' => 'message',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'message' => '',
						'new_lines' => 'wpautop',
						'esc_html' => 0,
					),
					array (
						'key' => 'field_591dfc1ac67ce',
						'label' => __('Medium Screen Breakpoint', 'ultimateazon2' ),
						'name' => 'md_breakpoint',
						'type' => 'number',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'default_value' => 640,
						'placeholder' => '',
						'prepend' => '',
						'append' => 'px',
						'min' => 560,
						'max' => '',
						'step' => '',
					),
					array (
						'key' => 'field_591dff45fcd2f',
						'label' => __('Slides to Show (medium breakpoint)', 'ultimateazon2' ),
						'name' => 'slides_to_show_md',
						'type' => 'number',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '50',
							'class' => '',
							'id' => '',
						),
						'default_value' => 3,
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'min' => 1,
						'max' => '',
						'step' => '',
					),
					array (
						'key' => 'field_591dffadfcd32',
						'label' => __('Slides to Scroll (medium breakpoint)', 'ultimateazon2' ),
						'name' => 'slides_to_scroll_md',
						'type' => 'number',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '50',
							'class' => '',
							'id' => '',
						),
						'default_value' => 1,
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'min' => 1,
						'max' => '',
						'step' => '',
					),
					array (
						'key' => 'field_591e03f4021a3',
						'label' => __('Hide Slider Dots (medium screens)', 'ultimateazon2' ),
						'name' => 'hide_slider_dots_md',
						'type' => 'true_false',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '50',
							'class' => '',
							'id' => '',
						),
						'message' => __('Check this box to hide the slider dots.', 'ultimateazon2' ),
						'default_value' => 0,
					),
					array (
						'key' => 'field_591e0470021a4',
						'label' => __('Hide Slider Arrows (medium screens)', 'ultimateazon2' ),
						'name' => 'hide_slider_arrows_md',
						'type' => 'true_false',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '50',
							'class' => '',
							'id' => '',
						),
						'message' => __('Check this box to hide the slider arrows.', 'ultimateazon2' ),
						'default_value' => 0,
					),
					array (
						'key' => 'field_591e022e11667',
						'label' => __('Large Screens', 'ultimateazon2' ),
						'name' => '',
						'type' => 'message',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'message' => '',
						'new_lines' => 'wpautop',
						'esc_html' => 0,
					),
					array (
						'key' => 'field_591dfc40c67cf',
						'label' => __('Large Screen Breakpoint', 'ultimateazon2' ),
						'name' => 'lg_breakpoint',
						'type' => 'number',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'default_value' => 1023,
						'placeholder' => '',
						'prepend' => '',
						'append' => 'px',
						'min' => 760,
						'max' => '',
						'step' => '',
					),
					array (
						'key' => 'field_591dff6ffcd30',
						'label' => __('Slides to Show (large screens)', 'ultimateazon2' ),
						'name' => 'slides_to_show_lg',
						'type' => 'number',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '50',
							'class' => '',
							'id' => '',
						),
						'default_value' => 4,
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'min' => 1,
						'max' => '',
						'step' => '',
					),
					array (
						'key' => 'field_591dffb9fcd33',
						'label' => __('Slides to Scroll (large screens)', 'ultimateazon2' ),
						'name' => 'slides_to_scroll_lg',
						'type' => 'number',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '50',
							'class' => '',
							'id' => '',
						),
						'default_value' => 1,
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'min' => 1,
						'max' => '',
						'step' => '',
					),
					array (
						'key' => 'field_591e048c021a5',
						'label' => __('Hide Slider Dots (large screens)', 'ultimateazon2' ),
						'name' => 'hide_slider_dots_lg',
						'type' => 'true_false',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '50',
							'class' => '',
							'id' => '',
						),
						'message' => __('Check this box to hide the slider dots.', 'ultimateazon2' ),
						'default_value' => 0,
					),
					array (
						'key' => 'field_591e049b021a6',
						'label' => __('Hide Slider Arrows (large screens)', 'ultimateazon2' ),
						'name' => 'hide_slider_arrows_lg',
						'type' => 'true_false',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '50',
							'class' => '',
							'id' => '',
						),
						'message' => __('Check this box to hide the slider arrows.', 'ultimateazon2' ),
						'default_value' => 0,
					),
					array (
						'key' => 'field_591e0127c4424',
						'label' => __('Auto Play Settings', 'ultimateazon2' ),
						'name' => '',
						'type' => 'message',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'message' => '',
						'new_lines' => 'wpautop',
						'esc_html' => 0,
					),
					array (
						'key' => 'field_591e0132c4425',
						'label' => __('Enable Auto Play', 'ultimateazon2' ),
						'name' => 'enable_auto_play',
						'type' => 'true_false',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'message' => __('Check this box to enable Autoplay', 'ultimateazon2' ),
						'default_value' => 0,
					),
					array (
						'key' => 'field_591e0154c4426',
						'label' => __('Auto Play Speed', 'ultimateazon2' ),
						'name' => 'auto_play_speed',
						'type' => 'number',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_591e0132c4425',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'default_value' => 7000,
						'placeholder' => '',
						'prepend' => '',
						'append' => 'milliseconds',
						'min' => 1000,
						'max' => '',
						'step' => '',
					),
					array (
						'key' => 'field_591dfef7fcd2d',
						'label' => __('Miscellaneous Slider Settings', 'ultimateazon2' ),
						'name' => '',
						'type' => 'message',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'message' => '',
						'new_lines' => 'wpautop',
						'esc_html' => 0,
					),
					array (
						'key' => 'field_591e009a72cca',
						'label' => __('Slider Dots Size', 'ultimateazon2' ),
						'name' => 'slider_dots_size',
						'type' => 'select',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '40',
							'class' => '',
							'id' => '',
						),
						'choices' => array (
							'5px' => __('small', 'ultimateazon2' ),
							'10px' => __('medium', 'ultimateazon2' ),
							'20px' => __('large', 'ultimateazon2' ),
						),
						'default_value' => array (
						),
						'allow_null' => 0,
						'multiple' => 0,
						'ui' => 0,
						'ajax' => 0,
						'return_format' => 'value',
						'placeholder' => '',
					),
					array (
						'key' => 'field_591e007272cc9',
						'label' => __('Slider Dots Position', 'ultimateazon2' ),
						'name' => 'slider_dots_position',
						'type' => 'select',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '60',
							'class' => '',
							'id' => '',
						),
						'choices' => array (
							'top' => 'top',
							'bottom' => 'bottom',
						),
						'default_value' => array (
						),
						'allow_null' => 0,
						'multiple' => 0,
						'ui' => 0,
						'ajax' => 0,
						'return_format' => 'value',
						'placeholder' => '',
					),
					array (
						'key' => 'field_591e01e811665',
						'label' => __('Slider Arrows Style', 'ultimateazon2' ),
						'name' => 'slider_arrows_style',
						'type' => 'select',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'choices' => array (
							'outside-bars' => __('Outside the Slider (Bars/Arrows)', 'ultimateazon2' ),
							'outside-arrows' => __('Outside the Slider (Arrows)', 'ultimateazon2' ),
							'inside-bars' => __('Inside the Slider (Bars)', 'ultimateazon2' ),
						),
						'default_value' => array (
						),
						'allow_null' => 0,
						'multiple' => 0,
						'ui' => 0,
						'ajax' => 0,
						'return_format' => 'value',
						'placeholder' => '',
					),
					array (
						'key' => 'field_591e0e51fd58e',
						'label' => __('Enable Center Mode', 'ultimateazon2' ),
						'name' => 'enable_center_mode',
						'type' => 'true_false',
						'instructions' => __('If you enable this setting, make sure to only display an odd number of slides for all screen sizes above.', 'ultimateazon2' ),
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'message' => __('Check this box to enable center mode.', 'ultimateazon2' ),
						'default_value' => 0,
					),
				),
				'location' => array (
					array (
						array (
							'param' => 'options_page',
							'operator' => '==',
							'value' => 'extension-sliders-settings',
						),
					),
				),
				'menu_order' => 0,
				'position' => 'normal',
				'style' => 'default',
				'label_placement' => 'top',
				'instruction_placement' => 'label',
				'hide_on_screen' => '',
				'active' => 1,
				'description' => '',
			));











			
			/**************************************************
			*********** SLIDER SINGLE OVERRIDE STYLES *********
			**************************************************/

			acf_add_local_field_group(array (
				'key' => 'group_591e3091de85b',
				'title' => 'Advanced Slider Settings',
				'fields' => array (
					array (
						'key' => 'field_591e309c00b9c',
						'label' => __('Override Global Slider Settings', 'ultimateazon2' ),
						'name' => 'override_global_slider_settings',
						'type' => 'true_false',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'message' => __('Check this box to override the global slider settings', 'ultimateazon2' ),
						'default_value' => 0,
					),
					array (
						'key' => 'field_591e3091e5c4c',
						'label' => __('Small Screens', 'ultimateazon2' ),
						'name' => '',
						'type' => 'message',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_591e309c00b9c',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'message' => '',
						'new_lines' => 'wpautop',
						'esc_html' => 0,
					),
					array (
						'key' => 'field_591e3091e5c66',
						'label' => __('Slides to Show (small screens)', 'ultimateazon2' ),
						'name' => 'slides_to_show_sm',
						'type' => 'number',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_591e309c00b9c',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '50',
							'class' => '',
							'id' => '',
						),
						'default_value' => 1,
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'min' => 1,
						'max' => '',
						'step' => '',
					),
					array (
						'key' => 'field_591e3091e5c71',
						'label' => __('Slides to Scroll (small screens)', 'ultimateazon2' ),
						'name' => 'slides_to_scroll_sm',
						'type' => 'number',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_591e309c00b9c',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '50',
							'class' => '',
							'id' => '',
						),
						'default_value' => 1,
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'min' => 1,
						'max' => '',
						'step' => '',
					),
					array (
						'key' => 'field_591e3091e5c7c',
						'label' => __('Hide Slider Dots (small screens)', 'ultimateazon2' ),
						'name' => 'hide_slider_dots_sm',
						'type' => 'true_false',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_591e309c00b9c',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '50',
							'class' => '',
							'id' => '',
						),
						'message' => __('Check this box to hide the slider dots.', 'ultimateazon2' ),
						'default_value' => 0,
					),
					array (
						'key' => 'field_591e3091e5c87',
						'label' => __('Hide Slider Arrows (small screens)', 'ultimateazon2' ),
						'name' => 'hide_slider_arrows_sm',
						'type' => 'true_false',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_591e309c00b9c',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '50',
							'class' => '',
							'id' => '',
						),
						'message' => __('Check this box to hide the slider arrows.', 'ultimateazon2' ),
						'default_value' => 0,
					),
					array (
						'key' => 'field_591e3091e5c92',
						'label' => __('Medium Screens', 'ultimateazon2' ),
						'name' => '',
						'type' => 'message',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_591e309c00b9c',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'message' => '',
						'new_lines' => 'wpautop',
						'esc_html' => 0,
					),
					array (
						'key' => 'field_591e3091e5c9e',
						'label' => __('Medium Screen Breakpoint', 'ultimateazon2' ),
						'name' => 'md_breakpoint',
						'type' => 'number',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_591e309c00b9c',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'default_value' => 640,
						'placeholder' => '',
						'prepend' => '',
						'append' => 'px',
						'min' => 560,
						'max' => '',
						'step' => '',
					),
					array (
						'key' => 'field_591e3091e5ca9',
						'label' => __('Slides to Show (medium breakpoint)', 'ultimateazon2' ),
						'name' => 'slides_to_show_md',
						'type' => 'number',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_591e309c00b9c',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '50',
							'class' => '',
							'id' => '',
						),
						'default_value' => 3,
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'min' => 1,
						'max' => '',
						'step' => '',
					),
					array (
						'key' => 'field_591e3091e5cb5',
						'label' => __('Slides to Scroll (medium breakpoint)', 'ultimateazon2' ),
						'name' => 'slides_to_scroll_md',
						'type' => 'number',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_591e309c00b9c',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '50',
							'class' => '',
							'id' => '',
						),
						'default_value' => 1,
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'min' => 1,
						'max' => '',
						'step' => '',
					),
					array (
						'key' => 'field_591e3091e5cc0',
						'label' => __('Hide Slider Dots (medium screens)', 'ultimateazon2' ),
						'name' => 'hide_slider_dots_md',
						'type' => 'true_false',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_591e309c00b9c',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '50',
							'class' => '',
							'id' => '',
						),
						'message' => __('Check this box to hide the slider dots.', 'ultimateazon2' ),
						'default_value' => 0,
					),
					array (
						'key' => 'field_591e3091e5ccb',
						'label' => __('Hide Slider Arrows (medium screens)', 'ultimateazon2' ),
						'name' => 'hide_slider_arrows_md',
						'type' => 'true_false',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_591e309c00b9c',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '50',
							'class' => '',
							'id' => '',
						),
						'message' => __('Check this box to hide the slider arrows.', 'ultimateazon2' ),
						'default_value' => 0,
					),
					array (
						'key' => 'field_591e3091e5cd5',
						'label' => __('Large Screens', 'ultimateazon2' ),
						'name' => '',
						'type' => 'message',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_591e309c00b9c',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'message' => '',
						'new_lines' => 'wpautop',
						'esc_html' => 0,
					),
					array (
						'key' => 'field_591e3091e5ce1',
						'label' => __('Large Screen Breakpoint', 'ultimateazon2' ),
						'name' => 'lg_breakpoint',
						'type' => 'number',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_591e309c00b9c',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'default_value' => 1023,
						'placeholder' => '',
						'prepend' => '',
						'append' => 'px',
						'min' => 760,
						'max' => '',
						'step' => '',
					),
					array (
						'key' => 'field_591e3091e5cec',
						'label' => __('Slides to Show (large screens)', 'ultimateazon2' ),
						'name' => 'slides_to_show_lg',
						'type' => 'number',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_591e309c00b9c',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '50',
							'class' => '',
							'id' => '',
						),
						'default_value' => 4,
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'min' => 1,
						'max' => '',
						'step' => '',
					),
					array (
						'key' => 'field_591e3091e5cf8',
						'label' => __('Slides to Scroll (large screens)', 'ultimateazon2' ),
						'name' => 'slides_to_scroll_lg',
						'type' => 'number',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_591e309c00b9c',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '50',
							'class' => '',
							'id' => '',
						),
						'default_value' => 1,
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'min' => 1,
						'max' => '',
						'step' => '',
					),
					array (
						'key' => 'field_591e3091e5d03',
						'label' => __('Hide Slider Dots (large screens)', 'ultimateazon2' ),
						'name' => 'hide_slider_dots_lg',
						'type' => 'true_false',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_591e309c00b9c',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '50',
							'class' => '',
							'id' => '',
						),
						'message' => __('Check this box to hide the slider dots.', 'ultimateazon2' ),
						'default_value' => 0,
					),
					array (
						'key' => 'field_591e3091e5d0e',
						'label' => __('Hide Slider Arrows (large screens)', 'ultimateazon2' ),
						'name' => 'hide_slider_arrows_lg',
						'type' => 'true_false',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_591e309c00b9c',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '50',
							'class' => '',
							'id' => '',
						),
						'message' => __('Check this box to hide the slider arrows.', 'ultimateazon2' ),
						'default_value' => 0,
					),
					array (
						'key' => 'field_591e3091e5d19',
						'label' => __('Auto Play Settings', 'ultimateazon2' ),
						'name' => '',
						'type' => 'message',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_591e309c00b9c',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'message' => '',
						'new_lines' => 'wpautop',
						'esc_html' => 0,
					),
					array (
						'key' => 'field_591e3091e5d23',
						'label' => __('Enable Auto Play', 'ultimateazon2' ),
						'name' => 'enable_auto_play',
						'type' => 'true_false',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_591e309c00b9c',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'message' => __('Check this box to enable Autoplay', 'ultimateazon2' ),
						'default_value' => 0,
					),
					array (
						'key' => 'field_591e3091e5d2e',
						'label' => __('Auto Play Speed', 'ultimateazon2' ),
						'name' => 'auto_play_speed',
						'type' => 'number',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_591e3091e5d23',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_591e309c00b9c',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'default_value' => 7000,
						'placeholder' => '',
						'prepend' => '',
						'append' => 'milliseconds',
						'min' => 1000,
						'max' => '',
						'step' => '',
					),
					array (
						'key' => 'field_591e3091e5d3a',
						'label' => __('Miscellaneous Slider Settings', 'ultimateazon2' ),
						'name' => '',
						'type' => 'message',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_591e309c00b9c',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'message' => '',
						'new_lines' => 'wpautop',
						'esc_html' => 0,
					),
					array (
						'key' => 'field_591e3091e5d46',
						'label' => __('Slider Dots Size', 'ultimateazon2' ),
						'name' => 'slider_dots_size',
						'type' => 'select',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_591e309c00b9c',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '40',
							'class' => '',
							'id' => '',
						),
						'choices' => array (
							'5px' => __('small', 'ultimateazon2' ),
							'10px' => __('medium', 'ultimateazon2' ),
							'20px' => __('large', 'ultimateazon2' ),
						),
						'default_value' => array (
						),
						'allow_null' => 0,
						'multiple' => 0,
						'ui' => 0,
						'ajax' => 0,
						'return_format' => 'value',
						'placeholder' => '',
					),
					array (
						'key' => 'field_591e3091e5d50',
						'label' => __('Slider Dots Position', 'ultimateazon2' ),
						'name' => 'slider_dots_position',
						'type' => 'select',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_591e309c00b9c',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '60',
							'class' => '',
							'id' => '',
						),
						'choices' => array (
							'top' => 'top',
							'bottom' => 'bottom',
						),
						'default_value' => array (
						),
						'allow_null' => 0,
						'multiple' => 0,
						'ui' => 0,
						'ajax' => 0,
						'return_format' => 'value',
						'placeholder' => '',
					),
					array (
						'key' => 'field_591e3091e5d5c',
						'label' => __('Slider Arrows Style', 'ultimateazon2' ),
						'name' => 'slider_arrows_style',
						'type' => 'select',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_591e309c00b9c',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'choices' => array (
							'outside-bars' => __('Outside the Slider (Bars/Arrows)', 'ultimateazon2' ),
							'outside-arrows' => __('Outside the Slider (Arrows)', 'ultimateazon2' ),
							'inside-bars' => __('Inside the Slider (Bars)', 'ultimateazon2' ),
						),
						'default_value' => array (
						),
						'allow_null' => 0,
						'multiple' => 0,
						'ui' => 0,
						'ajax' => 0,
						'return_format' => 'value',
						'placeholder' => '',
					),
					array (
						'key' => 'field_591e3091e5d65',
						'label' => __('Enable Center Mode', 'ultimateazon2' ),
						'name' => 'enable_center_mode',
						'type' => 'true_false',
						'instructions' => __('If you enable this setting, make sure to only display an odd number of slides for all screen sizes above.', 'ultimateazon2' ),
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_591e309c00b9c',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'message' => __('Check this box to enable center mode.', 'ultimateazon2' ),
						'default_value' => 0,
					),
				),
				'location' => array (
					array (
						array (
							'param' => 'post_type',
							'operator' => '==',
							'value' => 'galfram_slider',
						),
					),
				),
				'menu_order' => 1,
				'position' => 'normal',
				'style' => 'default',
				'label_placement' => 'top',
				'instruction_placement' => 'label',
				'hide_on_screen' => '',
				'active' => 1,
				'description' => '',
			));






			acf_add_local_field_group(array (
				'key' => 'group_5935bf4423add',
				'title' => 'Slider Styles',
				'fields' => array (
					array (
						'key' => 'field_596e61c4f0b5f',
						'label' => 'Posts & Pages Slider',
						'name' => '',
						'type' => 'message',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '',
							'class' => 'djn-section-heading',
							'id' => '',
						),
						'message' => '',
						'new_lines' => '',
						'esc_html' => 0,
					),
					array (
						'key' => 'field_596e6a4f85dcd',
						'label' => 'Slide Background Color',
						'name' => 'posts_slide_bg_color',
						'type' => 'color_picker',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '40',
							'class' => '',
							'id' => '',
						),
						'default_value' => '#FFFFFF',
					),
					array (
						'key' => 'field_596e6b6aee48c',
						'label' => 'Slide Text Color',
						'name' => 'posts_slide_text_color',
						'type' => 'color_picker',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '60',
							'class' => '',
							'id' => '',
						),
						'default_value' => '#505050',
					),
					array (
						'key' => 'field_596f6aae6bae2',
						'label' => 'Slider Previous/Next Arrows Color',
						'name' => 'posts_prevnext_color',
						'type' => 'color_picker',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '40',
							'class' => '',
							'id' => '',
						),
						'default_value' => '#999999',
					),
					array (
						'key' => 'field_596f6b2272a7f',
						'label' => 'Slider Dots Color',
						'name' => 'posts_slider_dots_color',
						'type' => 'color_picker',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '30',
							'class' => '',
							'id' => '',
						),
						'default_value' => '#DEDEDE',
					),
					array (
						'key' => 'field_596f6b909c02e',
						'label' => 'Slider Dots Color Active Slide',
						'name' => 'posts_slider_dots_color_active',
						'type' => 'color_picker',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '30',
							'class' => '',
							'id' => '',
						),
						'default_value' => '#666666',
					),
					array (
						'key' => 'field_596e61e4f0b60',
						'label' => 'Custom Slider',
						'name' => '',
						'type' => 'message',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '',
							'class' => 'djn-section-heading',
							'id' => '',
						),
						'message' => '',
						'new_lines' => '',
						'esc_html' => 0,
					),
					array (
						'key' => 'field_596f7a5756bb0',
						'label' => 'Slide Background Color',
						'name' => 'custom_slide_bg_color',
						'type' => 'color_picker',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '40',
							'class' => '',
							'id' => '',
						),
						'default_value' => '#FFFFFF',
					),
					array (
						'key' => 'field_596f7a6856bb1',
						'label' => 'Slide Text Color',
						'name' => 'custom_slide_text_color',
						'type' => 'color_picker',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '60',
							'class' => '',
							'id' => '',
						),
						'default_value' => '#505050',
					),
					array (
						'key' => 'field_596f7aa256bb2',
						'label' => 'Slider Previous/Next Arrows Color',
						'name' => 'custom_prevnext_color',
						'type' => 'color_picker',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '40',
							'class' => '',
							'id' => '',
						),
						'default_value' => '#999999',
					),
					array (
						'key' => 'field_596f7aa556bb3',
						'label' => 'Slider Dots Color',
						'name' => 'custom_slider_dots_color',
						'type' => 'color_picker',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '30',
							'class' => '',
							'id' => '',
						),
						'default_value' => '#DEDEDE',
					),
					array (
						'key' => 'field_596f7aa756bb4',
						'label' => 'Slider Dots Color Active Slide',
						'name' => 'custom_slider_dots_color_active',
						'type' => 'color_picker',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '30',
							'class' => '',
							'id' => '',
						),
						'default_value' => '#666666',
					),
					array (
						'key' => 'field_596e61eef0b61',
						'label' => 'Product Reviews Slider',
						'name' => '',
						'type' => 'message',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '',
							'class' => 'djn-section-heading',
							'id' => '',
						),
						'message' => '',
						'new_lines' => '',
						'esc_html' => 0,
					),
					array (
						'key' => 'field_596fba129b040',
						'label' => 'Slide Background Color',
						'name' => 'prp_slide_bg_color',
						'type' => 'color_picker',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '50',
							'class' => '',
							'id' => '',
						),
						'default_value' => '#FFFFFF',
					),
					array (
						'key' => 'field_596fba819b045',
						'label' => 'Slide Border Color',
						'name' => 'prp_slide_border_color',
						'type' => 'color_picker',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '50',
							'class' => '',
							'id' => '',
						),
						'default_value' => '#DEDEDE',
					),



//////////////////////////




					array (
						'key' => 'field_efvevevevgbebetbt4b',
						'label' => __('Enable Slider Font Styles', 'ultimateazon2'),
						'name' => 'prp_enable_slider_font_styles',
						'type' => 'true_false',
						'instructions' => __('(This may be over-written by other font styles when using the WYSIWYG or other field types)', 'ultimateazon2'),
						'required' => 0,
						'conditional_logic' => array(),
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'message' => __('Check This to enable the slider font styles', 'ultimateazon2'),
						'default_value' => 0,
					),
					array (
						'key' => 'field_skw938rhf9eowsj',
						'label' => __('Slider Global Font Size', 'ultimateazon2'),
						'name' => 'prp_global_font_size',
						'type' => 'number',
						'instructions' => __('(This may be over-written by other font styles when using the WYSIWYG or other field types)', 'ultimateazon2'),
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_efvevevevgbebetbt4b',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '33',
							'class' => '',
							'id' => '',
						),
						'default_value' => 14,
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'min' => '',
						'max' => '',
						'step' => '',
					),
					array (
						'key' => 'field_908h7g6tvybu',
						'label' => __('Slider Global Line Height', 'ultimateazon2'),
						'name' => 'prp_global_line_height',
						'type' => 'number',
						'instructions' => __('(This may be over-written by other font styles when using the WYSIWYG or other field types)', 'ultimateazon2'),
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_efvevevevgbebetbt4b',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '33',
							'class' => '',
							'id' => '',
						),
						'default_value' => 16,
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'min' => '',
						'max' => '',
						'step' => '',
					),
					array (
						'key' => 'field_wrgerfwetreg',
						'label' => __('Slider Global Text Color', 'ultimateazon2'),
						'name' => 'prp_global_color',
						'type' => 'color_picker',
						'instructions' => __('(This may be over-written by other font styles when using the WYSIWYG or other field types)', 'ultimateazon2'),
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_efvevevevgbebetbt4b',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '34',
							'class' => '',
							'id' => '',
						),
						'default_value' => '#505050',
					),
					array (
						'key' => 'field_98umn98ny87n78nnj98m',
						'label' => __('Slider Field Padding', 'ultimateazon2'),
						'name' => 'prp_slider_field_padding',
						'type' => 'number',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_efvevevevgbebetbt4b',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '100',
							'class' => '',
							'id' => '',
						),
						'default_value' => 10,
						'placeholder' => '',
						'prepend' => '',
						'append' => 'px',
						'min' => '',
						'max' => '',
						'step' => '',
					),
					array (
						'key' => 'field_uytvbuibyt',
						'label' => __('Enable Font Styles for Special Field Types', 'ultimateazon2'),
						'name' => 'prp_enable_font_styles_for_field_types',
						'type' => 'true_false',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_efvevevevgbebetbt4b',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'message' => __('These apply to these special field type: Amazon Product Title, WordPress Title, ASIN, Brand, Model, UPC, Features, Warranty, Price, Lowest Used Price, Lowest New Price, Star Rating, Yes/No', 'ultimateazon2'),
						'default_value' => 0,
					),
					array (
						'key' => 'field_9786g5g8h96g5f',
						'label' => __('Enable Amazon Product Title Field Styles', 'ultimateazon2'),
						'name' => 'prp_enable_ama_title_field_styles',
						'type' => 'true_false',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_efvevevevgbebetbt4b',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_uytvbuibyt',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'message' => __('Check here to enable "Amazon Product Title" field styles', 'ultimateazon2'),
						'default_value' => 0,
					),
					array (
						'key' => 'field_g45etrwd345t65',
						'label' => __('Amazon Product Title Font Size', 'ultimateazon2'),
						'name' => 'prp_ama_title_font_size',
						'type' => 'number',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_efvevevevgbebetbt4b',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_uytvbuibyt',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_9786g5g8h96g5f',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '33',
							'class' => '',
							'id' => '',
						),
						'default_value' => 14,
						'placeholder' => '',
						'prepend' => '',
						'append' => 'px',
						'min' => '',
						'max' => '',
						'step' => '',
					),
					array (
						'key' => 'field_43546untyrerwe',
						'label' => __('Amazon Product Title Line Height', 'ultimateazon2'),
						'name' => 'prp_ama_title_line_height',
						'type' => 'number',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_efvevevevgbebetbt4b',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_uytvbuibyt',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_9786g5g8h96g5f',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '33',
							'class' => '',
							'id' => '',
						),
						'default_value' => 16,
						'placeholder' => '',
						'prepend' => '',
						'append' => 'px',
						'min' => '',
						'max' => '',
						'step' => '',
					),
					array (
						'key' => 'field_8976g5g8h96g5f7g8yh9',
						'label' => __('Amazon Product Title Text Color', 'ultimateazon2'),
						'name' => 'prp_ama_title_text_color',
						'type' => 'color_picker',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_efvevevevgbebetbt4b',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_uytvbuibyt',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_9786g5g8h96g5f',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '34',
							'class' => '',
							'id' => '',
						),
						'default_value' => '#000',
					),
					array (
						'key' => 'field_43546uyt4wgrfewrgth',
						'label' => __('Enable WordPress Title Field Styles', 'ultimateazon2'),
						'name' => 'prp_enable_wp_title_field_styles',
						'type' => 'true_false',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_efvevevevgbebetbt4b',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_uytvbuibyt',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'message' => __('Check here to enable "WordPress Title" field styles', 'ultimateazon2'),
						'default_value' => 0,
					),
					array (
						'key' => 'field_srtgr6yuey2v4y24u2',
						'label' => __('WordPress Title Font Size', 'ultimateazon2'),
						'name' => 'prp_wp_title_font_size',
						'type' => 'number',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_efvevevevgbebetbt4b',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_uytvbuibyt',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_43546uyt4wgrfewrgth',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '33',
							'class' => '',
							'id' => '',
						),
						'default_value' => 14,
						'placeholder' => '',
						'prepend' => '',
						'append' => 'px',
						'min' => '',
						'max' => '',
						'step' => '',
					),
					array (
						'key' => 'field_12324i7ytr',
						'label' => __('WordPress Title Line Height', 'ultimateazon2'),
						'name' => 'prp_wp_title_line_height',
						'type' => 'number',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_efvevevevgbebetbt4b',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_uytvbuibyt',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_43546uyt4wgrfewrgth',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '33',
							'class' => '',
							'id' => '',
						),
						'default_value' => 16,
						'placeholder' => '',
						'prepend' => '',
						'append' => 'px',
						'min' => '',
						'max' => '',
						'step' => '',
					),
					array (
						'key' => 'field_43bn57h4d24fvg4brhyt',
						'label' => __('WordPress Title Text Color', 'ultimateazon2'),
						'name' => 'prp_wp_title_text_color',
						'type' => 'color_picker',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_efvevevevgbebetbt4b',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_uytvbuibyt',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_43546uyt4wgrfewrgth',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '34',
							'class' => '',
							'id' => '',
						),
						'default_value' => '#000',
					),
					array (
						'key' => 'field_kjghhiuyb876vt76cr',
						'label' => __('Enable Custom Button Field Styles', 'ultimateazon2'),
						'name' => 'prp_enable_button_field_styles',
						'type' => 'true_false',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_efvevevevgbebetbt4b',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_uytvbuibyt',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'message' => __('Check here to enable "ASIN" field styles', 'ultimateazon2'),
						'default_value' => 0,
					),
					array (
						'key' => 'field_596fbc52da004',
						'label' => 'Custom Button Background Color',
						'name' => 'prp_custom_button_bg_color',
						'type' => 'color_picker',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_efvevevevgbebetbt4b',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_uytvbuibyt',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_kjghhiuyb876vt76cr',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '25',
							'class' => '',
							'id' => '',
						),
						'default_value' => '#40b676',
					),
					array (
						'key' => 'field_596fbc7fda005',
						'label' => 'Custom Button Text Color',
						'name' => 'prp_custom_button_text_color',
						'type' => 'color_picker',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_efvevevevgbebetbt4b',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_uytvbuibyt',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_kjghhiuyb876vt76cr',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '25',
							'class' => '',
							'id' => '',
						),
						'default_value' => '#FFFFFF',
					),
					array (
						'key' => 'field_596fbc8dda006',
						'label' => 'Custom Button Background Color Hover',
						'name' => 'prp_custom_button_bg_color_hover',
						'type' => 'color_picker',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_efvevevevgbebetbt4b',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_uytvbuibyt',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_kjghhiuyb876vt76cr',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '25',
							'class' => '',
							'id' => '',
						),
						'default_value' => '#40b676',
					),
					array (
						'key' => 'field_596fbc9fda007',
						'label' => 'Custom Button Text Color Hover',
						'name' => 'prp_custom_button_text_color_hover',
						'type' => 'color_picker',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_efvevevevgbebetbt4b',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_uytvbuibyt',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_kjghhiuyb876vt76cr',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '25',
							'class' => '',
							'id' => '',
						),
						'default_value' => '#FFFFFF',
					),
					array (
						'key' => 'field_786g8tft77yvg88gu',
						'label' => __('Enable Text Link Field Styles', 'ultimateazon2'),
						'name' => 'prp_enable_textlink_field_styles',
						'type' => 'true_false',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_efvevevevgbebetbt4b',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_uytvbuibyt',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'message' => __('Check here to enable plain text link field styles', 'ultimateazon2'),
						'default_value' => 0,
					),
					array (
						'key' => 'field_596fbd2d83e00',
						'label' => 'Text Link Color',
						'name' => 'prp_text_link_color',
						'type' => 'color_picker',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_efvevevevgbebetbt4b',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_uytvbuibyt',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_786g8tft77yvg88gu',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '50',
							'class' => '',
							'id' => '',
						),
						'default_value' => '#40b676',
					),
					array (
						'key' => 'field_596fbd5a83e02',
						'label' => 'Text Link Color Hover',
						'name' => 'prp_text_link_color_hover',
						'type' => 'color_picker',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_efvevevevgbebetbt4b',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_uytvbuibyt',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_786g8tft77yvg88gu',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '50',
							'class' => '',
							'id' => '',
						),
						'default_value' => '#40b676',
					),
					array (
						'key' => 'field_ytuvgbhnihuyvghj',
						'label' => __('Enable ASIN Field Styles', 'ultimateazon2'),
						'name' => 'prp_enable_asin_field_styles',
						'type' => 'true_false',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_efvevevevgbebetbt4b',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_uytvbuibyt',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'message' => __('Check here to enable "ASIN" field styles', 'ultimateazon2'),
						'default_value' => 0,
					),
					array (
						'key' => 'field_oiuytvghjknhbg',
						'label' => __('ASIN Font Size', 'ultimateazon2'),
						'name' => 'prp_asin_font_size',
						'type' => 'number',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_efvevevevgbebetbt4b',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_uytvbuibyt',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_ytuvgbhnihuyvghj',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '33',
							'class' => '',
							'id' => '',
						),
						'default_value' => 14,
						'placeholder' => '',
						'prepend' => '',
						'append' => 'px',
						'min' => '',
						'max' => '',
						'step' => '',
					),
					array (
						'key' => 'field_87g6f5rtvy7biu',
						'label' => __('ASIN Line Height', 'ultimateazon2'),
						'name' => 'prp_asin_line_height',
						'type' => 'number',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_efvevevevgbebetbt4b',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_uytvbuibyt',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_ytuvgbhnihuyvghj',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '33',
							'class' => '',
							'id' => '',
						),
						'default_value' => 16,
						'placeholder' => '',
						'prepend' => '',
						'append' => 'px',
						'min' => '',
						'max' => '',
						'step' => '',
					),
					array (
						'key' => 'field_b78g6f8756ctv7',
						'label' => __('ASIN Text Color', 'ultimateazon2'),
						'name' => 'prp_asin_text_color',
						'type' => 'color_picker',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_efvevevevgbebetbt4b',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_uytvbuibyt',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_ytuvgbhnihuyvghj',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '34',
							'class' => '',
							'id' => '',
						),
						'default_value' => '#000',
					),
					array (
						'key' => 'field_i87v6cv7b8n9m0',
						'label' => __('Enable Brand Field Styles', 'ultimateazon2'),
						'name' => 'prp_enable_brand_field_styles',
						'type' => 'true_false',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_efvevevevgbebetbt4b',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_uytvbuibyt',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'message' => __('Check here to enable "brand" field styles', 'ultimateazon2'),
						'default_value' => 0,
					),
					array (
						'key' => 'field_i98765rdfghj',
						'label' => __('Brand Font Size', 'ultimateazon2'),
						'name' => 'prp_brand_font_size',
						'type' => 'number',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_efvevevevgbebetbt4b',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_uytvbuibyt',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_i87v6cv7b8n9m0',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '33',
							'class' => '',
							'id' => '',
						),
						'default_value' => 14,
						'placeholder' => '',
						'prepend' => '',
						'append' => 'px',
						'min' => '',
						'max' => '',
						'step' => '',
					),
					array (
						'key' => 'field_sjs8d8f9d9ssod',
						'label' => __('Brand Line Height', 'ultimateazon2'),
						'name' => 'prp_brand_line_height',
						'type' => 'number',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_efvevevevgbebetbt4b',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_uytvbuibyt',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_i87v6cv7b8n9m0',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '33',
							'class' => '',
							'id' => '',
						),
						'default_value' => 16,
						'placeholder' => '',
						'prepend' => '',
						'append' => 'px',
						'min' => '',
						'max' => '',
						'step' => '',
					),
					array (
						'key' => 'field_rcgrvyh6hv5yh4yhv',
						'label' => __('Brand Text Color', 'ultimateazon2'),
						'name' => 'prp_brand_text_color',
						'type' => 'color_picker',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_efvevevevgbebetbt4b',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_uytvbuibyt',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_i87v6cv7b8n9m0',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '34',
							'class' => '',
							'id' => '',
						),
						'default_value' => '#000',
					),
					array (
						'key' => 'field_rfv46y5v45trc35t4',
						'label' => __('Enable Model Field Styles', 'ultimateazon2'),
						'name' => 'prp_enable_model_field_styles',
						'type' => 'true_false',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_efvevevevgbebetbt4b',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_uytvbuibyt',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'message' => __('Check here to enable "model" field styles', 'ultimateazon2'),
						'default_value' => 0,
					),
					array (
						'key' => 'field_tv5hb7yt45wt',
						'label' => __('Model Font Size', 'ultimateazon2'),
						'name' => 'prp_model_font_size',
						'type' => 'number',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_efvevevevgbebetbt4b',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_uytvbuibyt',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_rfv46y5v45trc35t4',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '33',
							'class' => '',
							'id' => '',
						),
						'default_value' => 14,
						'placeholder' => '',
						'prepend' => '',
						'append' => 'px',
						'min' => '',
						'max' => '',
						'step' => '',
					),
					array (
						'key' => 'field_9lk8j7h6g5f4',
						'label' => __('Model Line Height', 'ultimateazon2'),
						'name' => 'prp_model_line_height',
						'type' => 'number',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_efvevevevgbebetbt4b',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_uytvbuibyt',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_rfv46y5v45trc35t4',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '33',
							'class' => '',
							'id' => '',
						),
						'default_value' => 16,
						'placeholder' => '',
						'prepend' => '',
						'append' => 'px',
						'min' => '',
						'max' => '',
						'step' => '',
					),
					array (
						'key' => 'field_ftg5yh6ju6hy5gt4fr',
						'label' => __('Model Text Color', 'ultimateazon2'),
						'name' => 'prp_model_text_color',
						'type' => 'color_picker',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_efvevevevgbebetbt4b',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_uytvbuibyt',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_rfv46y5v45trc35t4',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '34',
							'class' => '',
							'id' => '',
						),
						'default_value' => '#000',
					),
					array (
						'key' => 'field_df45ft4dr3er4ft',
						'label' => __('Enable UPC Field Styles', 'ultimateazon2'),
						'name' => 'prp_enable_upc_field_styles',
						'type' => 'true_false',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_efvevevevgbebetbt4b',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_uytvbuibyt',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'message' => __('Check here to enable "UPC" field styles', 'ultimateazon2'),
						'default_value' => 0,
					),
					array (
						'key' => 'field_w020e9d9d0d0d',
						'label' => __('UPC Font Size', 'ultimateazon2'),
						'name' => 'prp_upc_font_size',
						'type' => 'number',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_efvevevevgbebetbt4b',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_uytvbuibyt',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_df45ft4dr3er4ft',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '33',
							'class' => '',
							'id' => '',
						),
						'default_value' => 14,
						'placeholder' => '',
						'prepend' => '',
						'append' => 'px',
						'min' => '',
						'max' => '',
						'step' => '',
					),
					array (
						'key' => 'field_a2s3w23ed3sw23e',
						'label' => __('UPC Line Height', 'ultimateazon2'),
						'name' => 'prp_upc_line_height',
						'type' => 'number',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_efvevevevgbebetbt4b',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_uytvbuibyt',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_df45ft4dr3er4ft',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '33',
							'class' => '',
							'id' => '',
						),
						'default_value' => 16,
						'placeholder' => '',
						'prepend' => '',
						'append' => 'px',
						'min' => '',
						'max' => '',
						'step' => '',
					),
					array (
						'key' => 'field_4f5g65f4ddrf',
						'label' => __('UPC Text Color', 'ultimateazon2'),
						'name' => 'prp_upc_text_color',
						'type' => 'color_picker',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_efvevevevgbebetbt4b',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_uytvbuibyt',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_df45ft4dr3er4ft',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '34',
							'class' => '',
							'id' => '',
						),
						'default_value' => '#000',
					),
					array (
						'key' => 'field_rtgyhgtf54cecf',
						'label' => __('Enable Features Field Styles', 'ultimateazon2'),
						'name' => 'prp_enable_features_field_styles',
						'type' => 'true_false',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_efvevevevgbebetbt4b',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_uytvbuibyt',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'message' => __('Check here to enable "features" field styles', 'ultimateazon2'),
						'default_value' => 0,
					),
					array (
						'key' => 'field_v5654f45v54cv5',
						'label' => __('Features Font Size', 'ultimateazon2'),
						'name' => 'prp_features_font_size',
						'type' => 'number',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_efvevevevgbebetbt4b',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_uytvbuibyt',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_rtgyhgtf54cecf',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '33',
							'class' => '',
							'id' => '',
						),
						'default_value' => 12,
						'placeholder' => '',
						'prepend' => '',
						'append' => 'px',
						'min' => '',
						'max' => '',
						'step' => '',
					),
					array (
						'key' => 'field_f 4gv5hyb6ujn75hy41',
						'label' => __('Features Line Height', 'ultimateazon2'),
						'name' => 'prp_features_line_height',
						'type' => 'number',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_efvevevevgbebetbt4b',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_uytvbuibyt',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_rtgyhgtf54cecf',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '33',
							'class' => '',
							'id' => '',
						),
						'default_value' => 14,
						'placeholder' => '',
						'prepend' => '',
						'append' => 'px',
						'min' => '',
						'max' => '',
						'step' => '',
					),
					array (
						'key' => 'field_d4f4f4d4d4d3er',
						'label' => __('Features Text Color', 'ultimateazon2'),
						'name' => 'prp_features_text_color',
						'type' => 'color_picker',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_efvevevevgbebetbt4b',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_uytvbuibyt',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_rtgyhgtf54cecf',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '34',
							'class' => '',
							'id' => '',
						),
						'default_value' => '#000',
					),
					array (
						'key' => 'field_rcf5c4xd4xdftc6g',
						'label' => __('Enable Warranty Field Styles', 'ultimateazon2'),
						'name' => 'prp_enable_warranty_field_styles',
						'type' => 'true_false',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_efvevevevgbebetbt4b',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_uytvbuibyt',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'message' => __('Check here to enable "warranty" field styles', 'ultimateazon2'),
						'default_value' => 0,
					),
					array (
						'key' => 'field_r4c5r5t5ct55f5r',
						'label' => __('Warranty Font Size', 'ultimateazon2'),
						'name' => 'prp_warranty_font_size',
						'type' => 'number',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_efvevevevgbebetbt4b',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_uytvbuibyt',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_rcf5c4xd4xdftc6g',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '33',
							'class' => '',
							'id' => '',
						),
						'default_value' => 14,
						'placeholder' => '',
						'prepend' => '',
						'append' => 'px',
						'min' => '',
						'max' => '',
						'step' => '',
					),
					array (
						'key' => 'field_j87h6gh76g5f4de3s',
						'label' => __('Warranty Line Height', 'ultimateazon2'),
						'name' => 'prp_warranty_line_height',
						'type' => 'number',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_efvevevevgbebetbt4b',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_uytvbuibyt',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_rcf5c4xd4xdftc6g',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '33',
							'class' => '',
							'id' => '',
						),
						'default_value' => 16,
						'placeholder' => '',
						'prepend' => '',
						'append' => 'px',
						'min' => '',
						'max' => '',
						'step' => '',
					),
					array (
						'key' => 'field_c46g5tgtcg3tgct5g5t',
						'label' => __('Warranty Text Color', 'ultimateazon2'),
						'name' => 'prp_warranty_text_color',
						'type' => 'color_picker',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_efvevevevgbebetbt4b',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_uytvbuibyt',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_rcf5c4xd4xdftc6g',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '34',
							'class' => '',
							'id' => '',
						),
						'default_value' => '#000',
					),
					array (
						'key' => 'field_fc35f45f4f4f4f4f4f',
						'label' => __('Enable Star Rating Field Styles', 'ultimateazon2'),
						'name' => 'prp_enable_star_rating_field_styles',
						'type' => 'true_false',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_efvevevevgbebetbt4b',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_uytvbuibyt',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'message' => __('Check here to enable "star rating" field styles', 'ultimateazon2'),
						'default_value' => 0,
					),
					array (
						'key' => 'field_trgrtegcf34tgc3tgtgtg',
						'label' => __('Star Icon Size', 'ultimateazon2'),
						'name' => 'prp_star_icon_size',
						'type' => 'number',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_efvevevevgbebetbt4b',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_uytvbuibyt',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_fc35f45f4f4f4f4f4f',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '50',
							'class' => '',
							'id' => '',
						),
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => 'px',
						'min' => '',
						'max' => '',
						'step' => '',
					),
					array (
						'key' => 'field_t5v4revtbvrcvtbgvrfc',
						'label' => __('Star Icon Color', 'ultimateazon2'),
						'name' => 'prp_star_icon_color',
						'type' => 'color_picker',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_efvevevevgbebetbt4b',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_uytvbuibyt',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_fc35f45f4f4f4f4f4f',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '50',
							'class' => '',
							'id' => '',
						),
						'default_value' => '#FFCC00',
					),
					array (
						'key' => 'field_crfvgt54frcd3rcf4tv5gfc',
						'label' => __('Enable Yes/No Rating Field Styles', 'ultimateazon2'),
						'name' => 'prp_enable_yesno_rating_field_styles',
						'type' => 'true_false',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_efvevevevgbebetbt4b',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_uytvbuibyt',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'message' => __('Check here to enable "yes/no" field styles', 'ultimateazon2'),
						'default_value' => 0,
					),
					array (
						'key' => 'field_vtg66g6g6g5f5f5',
						'label' => __('Yes/No Icon Size', 'ultimateazon2'),
						'name' => 'prp_yesno_icon_size',
						'type' => 'number',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_efvevevevgbebetbt4b',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_uytvbuibyt',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_crfvgt54frcd3rcf4tv5gfc',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '34',
							'class' => '',
							'id' => '',
						),
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => 'px',
						'min' => '',
						'max' => '',
						'step' => '',
					),
					array (
						'key' => 'field_rf5g65f45g65f4',
						'label' => __('Yes/No "YES" Icon Color', 'ultimateazon2'),
						'name' => 'prp_yesno_yes_icon_color',
						'type' => 'color_picker',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_efvevevevgbebetbt4b',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_uytvbuibyt',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_crfvgt54frcd3rcf4tv5gfc',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '33',
							'class' => '',
							'id' => '',
						),
						'default_value' => '#FFCC00',
					),
					array (
						'key' => 'field_5hgt34cfrtgvefw',
						'label' => __('Yes/No "NO" Icon Color', 'ultimateazon2'),
						'name' => 'prp_yesno_no_icon_color',
						'type' => 'color_picker',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_efvevevevgbebetbt4b',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_uytvbuibyt',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_crfvgt54frcd3rcf4tv5gfc',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '33',
							'class' => '',
							'id' => '',
						),
						'default_value' => '#B20000',
					),
					array (
						'key' => 'field_s3d43sd4fd3sd4frtg',
						'label' => __('Enable Price Field Styles', 'ultimateazon2'),
						'name' => 'prp_enable_price_field_styles',
						'type' => 'true_false',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_efvevevevgbebetbt4b',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_uytvbuibyt',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'message' => __('Check here to enable "price" field styles', 'ultimateazon2'),
						'default_value' => 0,
					),
					// array (
					// 	'key' => 'field_5tf4df54df5g',
					// 	'label' => __('Slashed Price Font Size', 'ultimateazon2'),
					// 	'name' => 'prp_slashed_price_font_size',
					// 	'type' => 'number',
					// 	'instructions' => '',
					// 	'required' => 0,
					// 	'conditional_logic' => array (
					// 		array (
					// 			array (
					// 				'field' => 'field_efvevevevgbebetbt4b',
					// 				'operator' => '==',
					// 				'value' => '1',
					// 			),
					// 			array (
					// 				'field' => 'field_uytvbuibyt',
					// 				'operator' => '==',
					// 				'value' => '1',
					// 			),
					// 			array (
					// 				'field' => 'field_s3d43sd4fd3sd4frtg',
					// 				'operator' => '==',
					// 				'value' => '1',
					// 			),
					// 		),
					// 	),
					// 	'wrapper' => array (
					// 		'width' => '50',
					// 		'class' => '',
					// 		'id' => '',
					// 	),
					// 	'default_value' => 14,
					// 	'placeholder' => '',
					// 	'prepend' => '',
					// 	'append' => 'px',
					// 	'min' => '',
					// 	'max' => '',
					// 	'step' => '',
					// ),
					// array (
					// 	'key' => 'field_5g65f45gf4d3sd4f5g',
					// 	'label' => __('Slashed Price Color', 'ultimateazon2'),
					// 	'name' => 'prp_slashed_price_color',
					// 	'type' => 'color_picker',
					// 	'instructions' => '',
					// 	'required' => 0,
					// 	'conditional_logic' => array (
					// 		array (
					// 			array (
					// 				'field' => 'field_efvevevevgbebetbt4b',
					// 				'operator' => '==',
					// 				'value' => '1',
					// 			),
					// 			array (
					// 				'field' => 'field_uytvbuibyt',
					// 				'operator' => '==',
					// 				'value' => '1',
					// 			),
					// 			array (
					// 				'field' => 'field_s3d43sd4fd3sd4frtg',
					// 				'operator' => '==',
					// 				'value' => '1',
					// 			),
					// 		),
					// 	),
					// 	'wrapper' => array (
					// 		'width' => '50',
					// 		'class' => '',
					// 		'id' => '',
					// 	),
					// 	'default_value' => '#999999',
					// ),
					array (
						'key' => 'field_h77u88u8u8h7h7',
						'label' => __('Price Font Size', 'ultimateazon2'),
						//'label' => __('Sale Price Font Size', 'ultimateazon2'),
						'name' => 'prp_sale_price_font_size',
						'type' => 'number',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_efvevevevgbebetbt4b',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_uytvbuibyt',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_s3d43sd4fd3sd4frtg',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '50',
							'class' => '',
							'id' => '',
						),
						'default_value' => 18,
						'placeholder' => '',
						'prepend' => '',
						'append' => 'px',
						'min' => '',
						'max' => '',
						'step' => '',
					),
					array (
						'key' => 'field_34f5tg4re324f5g',
						'label' => __('Price Color', 'ultimateazon2'),
						//'label' => __('Sale Price Color', 'ultimateazon2'),
						'name' => 'prp_sale_price_color',
						'type' => 'color_picker',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_efvevevevgbebetbt4b',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_uytvbuibyt',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_s3d43sd4fd3sd4frtg',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '50',
							'class' => '',
							'id' => '',
						),
						'default_value' => '#3ABB22',
					),
					// array (
					// 	'key' => 'field_yh5gt4rf4tg54rf3e',
					// 	'label' => __('Amount/Percentage Saved Font Size', 'ultimateazon2'),
					// 	'name' => 'prp_amountpercentage_saved_font_size',
					// 	'type' => 'number',
					// 	'instructions' => '',
					// 	'required' => 0,
					// 	'conditional_logic' => array (
					// 		array (
					// 			array (
					// 				'field' => 'field_efvevevevgbebetbt4b',
					// 				'operator' => '==',
					// 				'value' => '1',
					// 			),
					// 			array (
					// 				'field' => 'field_uytvbuibyt',
					// 				'operator' => '==',
					// 				'value' => '1',
					// 			),
					// 			array (
					// 				'field' => 'field_s3d43sd4fd3sd4frtg',
					// 				'operator' => '==',
					// 				'value' => '1',
					// 			),
					// 		),
					// 	),
					// 	'wrapper' => array (
					// 		'width' => '50',
					// 		'class' => '',
					// 		'id' => '',
					// 	),
					// 	'default_value' => 18,
					// 	'placeholder' => '',
					// 	'prepend' => '',
					// 	'append' => 'px',
					// 	'min' => '',
					// 	'max' => '',
					// 	'step' => '',
					// ),
					// array (
					// 	'key' => 'field_c4f5cr4ed34rf5c4e3',
					// 	'label' => __('Amount/Percentage Saved Color', 'ultimateazon2'),
					// 	'name' => 'prp_amountpercentage_saved_color',
					// 	'type' => 'color_picker',
					// 	'instructions' => '',
					// 	'required' => 0,
					// 	'conditional_logic' => array (
					// 		array (
					// 			array (
					// 				'field' => 'field_efvevevevgbebetbt4b',
					// 				'operator' => '==',
					// 				'value' => '1',
					// 			),
					// 			array (
					// 				'field' => 'field_uytvbuibyt',
					// 				'operator' => '==',
					// 				'value' => '1',
					// 			),
					// 			array (
					// 				'field' => 'field_s3d43sd4fd3sd4frtg',
					// 				'operator' => '==',
					// 				'value' => '1',
					// 			),
					// 		),
					// 	),
					// 	'wrapper' => array (
					// 		'width' => '50',
					// 		'class' => '',
					// 		'id' => '',
					// 	),
					// 	'default_value' => '#000000',
					// ),
					array (
						'key' => 'field_g6hg5f4d3ervtby',
						'label' => __('Enable Lowest New Price Field Styles', 'ultimateazon2'),
						'name' => 'prp_enable_lowest_new_price_field_styles',
						'type' => 'true_false',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_efvevevevgbebetbt4b',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_uytvbuibyt',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'message' => __('Check here to enable "lowest new price" field styles', 'ultimateazon2'),
						'default_value' => 0,
					),
					array (
						'key' => 'h6g5f46g5f4g5f4d',
						'label' => __('Lowest New Price Font Size', 'ultimateazon2'),
						'name' => 'prp_lowest_new_price_font_size',
						'type' => 'number',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_efvevevevgbebetbt4b',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_uytvbuibyt',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_g6hg5f4d3ervtby',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '33',
							'class' => '',
							'id' => '',
						),
						'default_value' => 14,
						'placeholder' => '',
						'prepend' => '',
						'append' => 'px',
						'min' => '',
						'max' => '',
						'step' => '',
					),
					array (
						'key' => 'field_2r3c32cr423crr',
						'label' => __('Lowest New Price Line Height', 'ultimateazon2'),
						'name' => 'prp_lowest_new_price_line_height',
						'type' => 'number',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_efvevevevgbebetbt4b',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_uytvbuibyt',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_g6hg5f4d3ervtby',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '33',
							'class' => '',
							'id' => '',
						),
						'default_value' => 16,
						'placeholder' => '',
						'prepend' => '',
						'append' => 'px',
						'min' => '',
						'max' => '',
						'step' => '',
					),
					array (
						'key' => 'field_evrfcvcerfcer',
						'label' => __('Lowest New Price Text Color', 'ultimateazon2'),
						'name' => 'prp_lowest_new_price_text_color',
						'type' => 'color_picker',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_efvevevevgbebetbt4b',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_uytvbuibyt',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_g6hg5f4d3ervtby',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '34',
							'class' => '',
							'id' => '',
						),
						'default_value' => '#000',
					),
					array (
						'key' => 'field_vtvrcedrftgrfed',
						'label' => __('Enable Lowest Used Price Field Styles', 'ultimateazon2'),
						'name' => 'prp_enable_lowest_used_price_field_styles',
						'type' => 'true_false',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_efvevevevgbebetbt4b',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_uytvbuibyt',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'message' => __('Check here to enable "lowest used price" field styles', 'ultimateazon2'),
						'default_value' => 0,
					),
					array (
						'key' => 'field_8u8u8ui9i9i9i9i9',
						'label' => __('Lowest Used Price Font Size', 'ultimateazon2'),
						'name' => 'prp_lowest_used_price_font_size',
						'type' => 'number',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_efvevevevgbebetbt4b',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_uytvbuibyt',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_vtvrcedrftgrfed',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '33',
							'class' => '',
							'id' => '',
						),
						'default_value' => 14,
						'placeholder' => '',
						'prepend' => '',
						'append' => 'px',
						'min' => '',
						'max' => '',
						'step' => '',
					),
					array (
						'key' => 'field_h6g5f4d35gf4d34fd3s2',
						'label' => __('Lowest Used Price Line Height', 'ultimateazon2'),
						'name' => 'prp_lowest_used_price_line_height',
						'type' => 'number',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_efvevevevgbebetbt4b',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_uytvbuibyt',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_vtvrcedrftgrfed',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '33',
							'class' => '',
							'id' => '',
						),
						'default_value' => 16,
						'placeholder' => '',
						'prepend' => '',
						'append' => 'px',
						'min' => '',
						'max' => '',
						'step' => '',
					),
					array (
						'key' => 'field_t3grewthgwfq',
						'label' => __('Lowest Used Price Text Color', 'ultimateazon2'),
						'name' => 'prp_lowest_used_price_text_color',
						'type' => 'color_picker',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_efvevevevgbebetbt4b',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_uytvbuibyt',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_vtvrcedrftgrfed',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '34',
							'class' => '',
							'id' => '',
						),
						'default_value' => '#000',
					),



////////////////////




					array (
						'key' => 'field_78tv76tv7t6b78b78y',
						'label' => __('Enable Slider Navigation Styles', 'ultimateazon2'),
						'name' => 'prp_enable_slider_nav_styles',
						'type' => 'true_false',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'message' => __('Check here to enable "ASIN" field styles', 'ultimateazon2'),
						'default_value' => 0,
					),
					array (
						'key' => 'field_596fba219b042',
						'label' => 'Slider Previous/Next Arrows Color',
						'name' => 'prp_prevnext_color',
						'type' => 'color_picker',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_78tv76tv7t6b78b78y',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '40',
							'class' => '',
							'id' => '',
						),
						'default_value' => '#999999',
					),
					array (
						'key' => 'field_596fba249b043',
						'label' => 'Slider Dots Color',
						'name' => 'prp_slider_dots_color',
						'type' => 'color_picker',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_78tv76tv7t6b78b78y',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '30',
							'class' => '',
							'id' => '',
						),
						'default_value' => '#DEDEDE',
					),
					array (
						'key' => 'field_596fba269b044',
						'label' => 'Slider Dots Color Active Slide',
						'name' => 'prp_slider_dots_color_active',
						'type' => 'color_picker',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_78tv76tv7t6b78b78y',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '30',
							'class' => '',
							'id' => '',
						),
						'default_value' => '#666666',
					),
					array (
						'key' => 'field_596e61faf0b62',
						'label' => 'Comparison ProTool Styles',
						'name' => '',
						'type' => 'message',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '',
							'class' => 'djn-section-heading',
							'id' => '',
						),
						'message' => '',
						'new_lines' => '',
						'esc_html' => 0,
					),
					array (
						'key' => 'field_596fbdbb08dc4',
						'label' => 'ProTool Previous/Next Arrows Color',
						'name' => 'protool_prevnext_color',
						'type' => 'color_picker',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '40',
							'class' => '',
							'id' => '',
						),
						'default_value' => '#999999',
					),
					array (
						'key' => 'field_596fbdca08dc5',
						'label' => 'ProTool Dots Color',
						'name' => 'protool_slider_dots_color',
						'type' => 'color_picker',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '30',
							'class' => '',
							'id' => '',
						),
						'default_value' => '#DEDEDE',
					),
					array (
						'key' => 'field_596fbdd508dc6',
						'label' => 'ProTool Dots Color Active Slide',
						'name' => 'protool_slider_dots_color_active',
						'type' => 'color_picker',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '30',
							'class' => '',
							'id' => '',
						),
						'default_value' => '#666666',
					),
				),
				'location' => array (
					array (
						array (
							'param' => 'options_page',
							'operator' => '==',
							'value' => 'theme-styles',
						),
					),
				),
				'menu_order' => 600,
				'position' => 'normal',
				'style' => 'default',
				'label_placement' => 'top',
				'instruction_placement' => 'label',
				'hide_on_screen' => '',
				'active' => 1,
				'description' => '',
			));








		endif;

	}



	function add_amazon_search_box() {
	?>

	<div id="galfram-api-lookup">

		<form>

			<label><?php _e('Enter your Keywords or the product ASIN here', 'ultimateazon2' ); ?></label>
			<input id="galfram-api-search-term" type="text" value="">

			<?php $chosen_locale = get_option('extension-prp-settings_default_amazon_search_locale', true); ?>

			<input id="galfram-search-locale" type="hidden" name="galfram-search-locale" value="<?php echo $chosen_locale; ?>">
			<input id="galfram-search-specs" type="hidden" name="galfram-search-specs" value="amazon-only">

			<?php 
			if ( $chosen_locale == 'US') { echo '<span class="search-locale-label">'.__('Locale: United States', 'ultimateazon2' ).'</span>'; }
			else if ( $chosen_locale == 'UK') { echo '<span class="search-locale-label">'.__('Locale: United Kingdom', 'ultimateazon2' ).'</span>'; }
			else if ( $chosen_locale == 'BR') { echo '<span class="search-locale-label">'.__('Locale: Brazil', 'ultimateazon2' ).'</span>'; }
			else if ( $chosen_locale == 'CA') { echo '<span class="search-locale-label">'.__('Locale: Canada', 'ultimateazon2' ).'</span>'; }
			else if ( $chosen_locale == 'CN') { echo '<span class="search-locale-label">'.__('Locale: China', 'ultimateazon2' ).'</span>'; }
			else if ( $chosen_locale == 'FR') { echo '<span class="search-locale-label">'.__('Locale: France', 'ultimateazon2' ).'</span>'; }
			else if ( $chosen_locale == 'DE') { echo '<span class="search-locale-label">'.__('Locale: Germany', 'ultimateazon2' ).'</span>'; }
			else if ( $chosen_locale == 'IN') { echo '<span class="search-locale-label">'.__('Locale: India', 'ultimateazon2' ).'</span>'; }
			else if ( $chosen_locale == 'MX') { echo '<span class="search-locale-label">'.__('Locale: Mexico', 'ultimateazon2' ).'</span>'; }
			else if ( $chosen_locale == 'IT') { echo '<span class="search-locale-label">'.__('Locale: Italy', 'ultimateazon2' ).'</span>'; }
			else if ( $chosen_locale == 'JP') { echo '<span class="search-locale-label">'.__('Locale: Japan', 'ultimateazon2' ).'</span>'; }
			else if ( $chosen_locale == 'ES') { echo '<span class="search-locale-label">'.__('Locale: Spain', 'ultimateazon2' ).'</span>'; }
			else { _e('Locale: United States', 'ultimateazon2' ); }
			?>

			<input id="galfram-api-search-submit" type="button" class="button button-primary button-large" value="<?php _e('Search', 'ultimateazon2' ); ?>" />

		</form>

	</div>

	<?php }


}
