<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    galfram_Sliders
 * @subpackage galfram_Pluginname/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    galfram_Sliders
 * @subpackage galfram_Pluginname/public
 * @author     Your Name <email@example.com>
 */
class galfram_sliders_Public {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $galfram_sliders    The ID of this plugin.
	 */
	private $galfram_sliders;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $galfram_sliders       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $galfram_sliders, $version ) {

		$this->galfram_sliders = $galfram_sliders;
		$this->version = $version;

	}


	/**Set our custom loop to use with our post type galfram_table
	 *
	 * @since    1.0.0
	 */
	function get_galfram_slider_post_type_template() {

		global $post;

		if ( is_singular('galfram_slider') ) {
			remove_action( 'uat2_post_loop', 'uat2_post_loop_function', 10 );
			add_action( 'uat2_post_loop', 'galfram_slider_loop_function', 10, 1 );
		}

		

		// pass our slider attributes to our slider layout loop
		function galfram_slider_loop_function( $table_attributes ) {

			global $post;

			echo '<div class="content-inner-row">';
				echo '<div class="content-inner-row-main galfram-slider">';
					do_action( 'galfram_before_loop' );

					if (have_posts()) : 
						while (have_posts()) : 
							the_post();
								?>
							<article id="post-<?php the_ID(); ?>" <?php post_class(''); ?> role="review">

								<header class="article-header">
									<h1 class="page-title" itemprop="itemReviewed"><?php the_title(); ?></h1>
								</header>

								<section class="entry-content" itemprop="articleBody">
									<?php uat2_before_loop_content(); ?>
									<div class="slider-post-content"><?php the_content(); ?></div>
									<?php echo do_shortcode('[uat2_slider id="'.get_the_id().'"]'); ?>
									<?php uat2_after_loop_content(); ?>
								</section> <!-- end article section -->

							</article> <!-- end article -->

						<?php
						endwhile;
					else:
						uat2_missing_loop();
					endif;

					do_action( 'uat2_after_loop' );
				echo '</div> <!-- end .content-inner-row-main.galfram-table -->';
				echo '</div> <!-- end .content-inner-row -->';

		} // end galfram_table_loop_function()

	}





	// [uat2_slider id="id-value"]
	public function galfram_layout_slider( $atts ) {

		// set our table ID variable form the id attribute of the shortcode
	    $slider_id = $atts['id'];

	    // set our empty variable to return our table with at the end
	    $slider_html = '';

	    // Get our table type
		$slider_type = get_post_meta( $slider_id, 'type_of_slider', true );

		// create a new instance of our sliders class if it exists
		$galfram_sliders_functions = new galfram_sliders_functions(); 



		// get our Slider Attributes
		$slider_attributes = $galfram_sliders_functions->get_galfram_slider_attributes( $slider_id );


		$slider_nav_types = ' ';
		$override = get_post_meta( $slider_id, 'override_global_slider_settings', true );


		if ( $override ) {
			$slider_dots_size = get_post_meta( $slider_id, 'slider_dots_size', true );
			$slider_dots_position = get_post_meta( $slider_id, 'slider_dots_position', true );
			$slider_arrows_style = get_post_meta( $slider_id, 'slider_arrows_style', true );
		}
		else {
			$slider_dots_size = get_option( 'extension-sliders-settings_slider_dots_size' );
			$slider_dots_position = get_option( 'extension-sliders-settings_slider_dots_position' );
			$slider_arrows_style = get_option( 'extension-sliders-settings_slider_arrows_style' );
		}


		if ( $slider_dots_size != '' ) {
			$slider_nav_types .=  'dots-size-'.$slider_dots_size;
		}
		if ( $slider_dots_position != '' ) {
			$slider_nav_types .=  ' dots-pos-'.$slider_dots_position;
		}
		if ( $slider_arrows_style != '' ) {
			$slider_nav_types .=  ' arrows-'.$slider_arrows_style;
		}

			
		if ( $slider_type ) {


				// output the posts slider
				if ( $slider_type == 'posts' ) {

					$choose_posts_fields = get_post_meta( $slider_id, 'choose_posts_fields', true );

					if ( $choose_posts_fields ) {

						$slider_html .= '<div class="galfram-slider-outer galfram-post-slider galfram-slider-'.$slider_id.$slider_nav_types.'" data-slider-name="'.get_the_title($slider_id).'">';
						$slider_html .= '<div class="galfram-slick-slider"'.$slider_attributes.'>';

						// get our posts slider source
						$posts_source = get_post_meta( $slider_id, 'posts_source', true );

						// build an array of our chosen fields to display in this slider
						$posts_fields_array = array();
						if( $choose_posts_fields ) {
							for( $i = 0; $i < $choose_posts_fields; $i++ ) {
								$posts_fields_array[$i]['spec_id'] = esc_html( get_post_meta( $slider_id, 'choose_posts_fields_' . $i . '_field_type', true ) );
								$posts_fields_array[$i]['spec_alignment'] = esc_html( get_post_meta( $slider_id, 'choose_posts_fields_' . $i . '_field_alignment_posts', true ) );
							}
						}

						if ( $posts_source == 'latest' ) {

							// get outr posts slider count
							$slider_count = get_post_meta( $slider_id, 'latest_posts_count', true );

							$args = array( 
								'post_type' => 'post',
								'posts_per_page' => $slider_count,
							);

							$the_query = new WP_Query( $args );

							if ( $the_query->have_posts() ) :
								$i=0;
								while ( $the_query->have_posts() ) : $the_query->the_post();

									$post_id = get_the_ID();

									//echo '<pre>'.print_r($posts_fields_array,1).'</pre>';

									$spec_alignment = $posts_fields_array[$i]['spec_alignment'];
						
									// get our Table Attributes
									$slider_html_fields = $galfram_sliders_functions->get_galfram_slider_post_fields( $post_id, $posts_fields_array );

									$slider_html .= '<div class="galfram-post-slide galfram-slide">';
										$slider_html .= $slider_html_fields;
										$slider_html .= '<a href="'.get_the_permalink().'" class="slide-cover-link"></a>';
									$slider_html .= '</div>'; // END .galfram-post-slide.galfram-slide

									$i++;

								endwhile;
							endif;

							wp_reset_postdata();

						
						}

						elseif ( $posts_source == 'manual' ) {

							$choose_posts = get_post_meta( $slider_id, 'choose_posts', true );

							if( $choose_posts ) {

								for( $i = 0; $i < $choose_posts; $i++ ) {

									$post_id = esc_html( get_post_meta( $slider_id, 'choose_posts_' . $i . '_add_a_post', true ) );
						
									// get our slide fields
									$slider_html_fields = $galfram_sliders_functions->get_galfram_slider_post_fields( $post_id, $posts_fields_array );

									$slider_html .= '<div class="galfram-post-slide galfram-slide">';
										$slider_html .= $slider_html_fields;
										$slider_html .= '<a href="'.get_the_permalink( $post_id ).'" class="slide-cover-link"></a>';
									$slider_html .= '</div>'; // END .galfram-post-slide.galfram-slide

								}

							}

						}

						else {
							return;
						}

						$slider_html .= '</div> <!-- END .galfram-slick-slider -->'; // END .galfram-slick-slider
						//$slider_html .= '</div> <!-- END .galfram-slider-outer -->'; // END .galfram-slider-outer

					}
					else {
						$slider_html .= '<p class="callout alert">'.__('You must add at least one field for posts to be displayed in this slider.', 'ultimateazon2').'</p>';
					}

				}











				// output the pages slider
				else if ( $slider_type == 'pages' ) {

					$choose_pages_fields = get_post_meta( $slider_id, 'choose_pages_fields', true );

					if ( $choose_pages_fields ) {

						$choose_pages = get_post_meta( $slider_id, 'choose_pages', true );

						if ( $choose_pages ) {

							$slider_html .= '<div class="galfram-slider-outer galfram-page-slider galfram-slider-'.$slider_id.$slider_nav_types.'" data-slider-name="'.get_the_title($slider_id).'">';
							$slider_html .= '<div class="galfram-slick-slider"'.$slider_attributes.'>';

								// build an array of our chosen fields to display in this slider
								$pages_fields_array = array();
								if( $choose_pages_fields ) {
									for( $i = 0; $i < $choose_pages_fields; $i++ ) {
										$pages_fields_array[$i]['spec_id'] = esc_html( get_post_meta( $slider_id, 'choose_pages_fields_' . $i . '_page_field', true ) );
										$pages_fields_array[$i]['spec_alignment'] = esc_html( get_post_meta( $slider_id, 'choose_pages_fields_' . $i . '_field_alignment_pages', true ) );
									}
								}
								
								if( $choose_pages ) {
									for( $i = 0; $i < $choose_pages; $i++ ) {
										$post_id = esc_html( get_post_meta( $slider_id, 'choose_pages_' . $i . '_add_a_page', true ) );
										// get our slide fields
										$slider_html_fields = $galfram_sliders_functions->get_galfram_slider_post_fields( $post_id, $pages_fields_array );
										$slider_html .= '<div class="galfram-post-slide galfram-slide">';
											$slider_html .= $slider_html_fields;
											$slider_html .= '<a href="'.get_the_permalink( $post_id ).'" class="slide-cover-link"></a>';
										$slider_html .= '</div>'; // END .galfram-post-slide.galfram-slide
									}
								}
							$slider_html .= '</div>'; // END .galfram-slick-slider
							//$slider_html .= '</div>'; // END .galfram-slider-outer

						}
						else {
							$slider_html .= '<p class="callout alert">'.__('You must add at least one page for a slider be displayed here.', 'ultimateazon2').'</p>';
						}

					}
					else {
						$slider_html .= '<p class="callout alert">'.__('You must add at least one field for pages to be displayed in this slider.', 'ultimateazon2').'</p>';
					}

				}












				// output the custom slider
				else if ( $slider_type == 'custom' ) {

					$slides_content = get_post_meta( $slider_id, 'add_custom_slides', true );

					if ( $slides_content ) {

						$slider_html .= '<div class="galfram-slider-outer galfram-custom-slider galfram-slider-'.$slider_id.$slider_nav_types.'" data-slider-name="'.get_the_title($slider_id).'">';
						$slider_html .= '<div class="galfram-slick-slider"'.$slider_attributes.'>';
							
							if( $slides_content ) {
								for( $i = 0; $i < $slides_content; $i++ ) {
									$slide_content = wpautop( get_post_meta( $slider_id, 'add_custom_slides_' . $i . '_slide_content', true ) );
									$slider_html .= '<div class="galfram-post-slide galfram-slide">';
										$slider_html .= apply_filters( 'the_content', $slide_content );
									$slider_html .= '</div>'; // END .galfram-post-slide.galfram-slide
								}
							}
						$slider_html .= '</div>'; // END .galfram-slick-slider
						//$slider_html .= '</div>'; // END .galfram-slider-outer

					}
					else {
						$slider_html .= '<p class="callout alert">'.__('You must create at least one custom slide.', 'ultimateazon2').'</p>';
					}

				}















				// output the product review pro slider
				else if ( $slider_type == 'galfram-prp' ) {

					// create a new instance of our sliders class if it exists
					$galfram_sliders_functions = new galfram_sliders_functions(); 

					
						// get the chosen spec group for this slider
						$specs_group_id = get_post_meta( $slider_id, 'choose_specification_group', true );

						$slider_type = get_post_meta( $slider_id, 'product_slider_types', true );

						// if ( $slider_type == 'choose-all-products' ) {
						// 	$choose_posts_fields = get_post_meta( get_the_ID(), 'choose_products_fields', true );
						// }
						if ( $slider_type == 'choose-spec-group' ) {
							$choose_posts_fields = get_post_meta( $slider_id, 'choose_products_fields_specgroup_prp', true );
						}
						else if ( $slider_type == 'choose-pull-amazon' ) {
							$choose_posts_fields = get_post_meta( $slider_id, 'choose_products_fields_specgroup_prp_allamazon', true );
						}


						if ( $choose_posts_fields ) {

							// get just our chosen spec group array
							$spec_choices_serialized = $galfram_sliders_functions->get_galfram_chosenspec_fields( $specs_group_id, $slider_id, $choose_posts_fields, $slider_type);
							$spec_choices = unserialize( $spec_choices_serialized );
							$spec_choices_serialized=null;

							//echo '<pre>'.print_r($spec_choices,1).'</pre>';


							if ( $spec_choices ) {

								// if ( $slider_type == 'choose-all-products' ) {
								// 	$posts_fields_array = $choose_posts_fields;
								// }
								if ( $slider_type == 'choose-pull-amazon' ) {
									$posts_fields_array = $spec_choices;
								}
								else {
									// build an array of our chosen fields to display in this slider
									$posts_fields_array = array();

									if( $choose_posts_fields ) {

										for( $i = 0; $i < $choose_posts_fields; $i++ ) {
											$posts_fields_array[$i]['spec_id'] = get_post_meta( $slider_id, 'choose_products_fields_specgroup_prp_' . $i . '_column', true );

											$posts_fields_array[$i]['spec_alignment'] = get_post_meta( $slider_id, 'choose_products_fields_specgroup_prp_' . $i . '_field_alignment', true );

											$posts_fields_array[$i]['spec_label'] = get_post_meta( $slider_id, 'choose_products_fields_specgroup_prp_' . $i . '_show_label', true );

											$posts_fields_array[$i]['spec_link'] = get_post_meta( $slider_id, 'choose_products_fields_specgroup_prp_' . $i . '_field_link', true );
										}

									}
								}


							}


							// if ( $slider_type == 'choose-all-products' ) {
							// 	// get our chosen product reviews from our slider
							// 	$chosen_products = get_post_meta( get_the_ID(), 'add_products', true );
							// }
							if ( $slider_type == 'choose-pull-amazon' ) {
								// get our chosen product reviews from our slider
								$chosen_products = get_post_meta( $slider_id, 'add_amazon_slides_prp', true );
							}
							else {
								// get our chosen product reviews from our slider
								$chosen_products = get_post_meta( $slider_id, 'choose_products_from_specs_prp', true );
							}

							if( $chosen_products ) {

								$slider_html .= '<div class="galfram-slider-outer galfram-prp-slider galfram-slider-'.$slider_id.$slider_nav_types.'" data-slider-name="'.get_the_title($slider_id).'">';
								$slider_html .= '<div class="galfram-slick-slider"'.$slider_attributes.'>';

								// save all of our chosen product review IDs into an array
								$all_prod_ids = array();

								// if ( $slider_type == 'choose-all-products' ) {
								// 	$all_prod_ids = array();
								// 	$sending_asins=false;
								// }
								if ( $slider_type == 'choose-pull-amazon' ) {
									for( $ix = 0; $ix < $chosen_products; $ix++ ) {
										$all_prod_ids[$ix] = esc_html( get_post_meta( $slider_id, 'add_amazon_slides_prp_' . $ix . '_ama_asin', true ) );

									}
									$sending_asins=true;
								}
								else {
									for( $ix = 0; $ix < $chosen_products; $ix++ ) {
										$all_prod_ids[$ix] = esc_html( get_post_meta( $slider_id, 'choose_products_from_specs_prp_' . $ix . '_galfram_slider_column_prod', true ) );

									}
									$sending_asins=false;
								}

								


								// if an amazon spec is being used, get our asin groups data
								if ( $slider_type == "choose-pull-amazon" || in_array_r( "amazon", $spec_choices ) || in_array_r( "ama", $spec_choices) ) {
									// get all of our amazon api data in an array. organized by product asin
									$final_item_data = $galfram_sliders_functions->galfram_slider_get_asin_groups_data( $all_prod_ids, $sending_asins );
								}
								// else set an empty array since we do not need one to get our spec data
								else {
									$final_item_data = array();
								}

								//echo '$final_item_data<pre>'.print_r($final_item_data,1).'</pre>';

								for( $i = 0; $i < $chosen_products; $i++ ) {

									// if ( $slider_type == 'choose-all-products' ) {
									// 	$prod_id = get_post_meta( get_the_ID(), 'add_products_' . $i . '_choose_product', true );
									// }
									if ( $slider_type == 'choose-pull-amazon') {
										$prod_id = $all_prod_ids[$i];
									}
									else {
										$prod_id = get_post_meta( $slider_id, 'choose_products_from_specs_prp_' . $i . '_galfram_slider_column_prod', true );
									}

									// get our slide fields
									$slider_html_fields = $galfram_sliders_functions->get_galfram_prp_slider_fields( $prod_id, $posts_fields_array, $slider_id, $specs_group_id, $final_item_data, $prod_custom, $slider_type );


									$slider_html .= '<div class="galfram-prp-slide galfram-match-specs-height" itemscope itemtype="http://schema.org/Product">';
										$slider_html .= $slider_html_fields;
									$slider_html .= '</div>'; // END .galfram-post-slide.galfram-slide

								}

								$slider_html .= '</div>'; // END .galfram-slick-slider
								$slider_html .= '<div class="clear"></div>';
								//$slider_html .= '</div>'; // END .galfram-slider-outer


							}
							else {
								$slider_html .= '<p class="callout alert">'.__('You must choose at least one product for a slider to appear here.', 'ultimateazon2').'</p>';
							}


						} // END if ( $choose_posts_fields )
						else {
							$slider_html .= '<p class="callout alert">'.__('You must choose which fields are displayed for a slider to appear here.', 'ultimateazon2').'</p>';
						}


				}














				// output the product review pro tool
				else if ( $slider_type == 'galfram-tableslide' ) {

					// create a new instance of our sliders class if it exists
					$galfram_sliders_functions = new galfram_sliders_functions(); 

					$slider_type = get_post_meta( $slider_id, 'protool_slider_types', true );

					if ( $slider_type == 'choose-spec-group' ) {

						// get the chosen spec group for this slider
						$specs_group_id = get_post_meta( $slider_id, 'choose_specification_group', true );
							

						if ( $specs_group_id ) {
						

							$slider_html_specs .= '<div class="galfram-slider-outer galfram-protool galfram-slider-'.$slider_id.$slider_nav_types.'" data-slider-name="'.get_the_title($slider_id).'">';
							$slider_html_specs .= '<div class="galfram-protool-specs galfram-match-specs-height">';


								if ( $specs_group_id != 'pull-from-amazon' ) {

									// get just our chosen spec group array
									$spec_choices = unserialize( $galfram_sliders_functions->get_galfram_chosenspec_fields( $specs_group_id ) );

									if ( $spec_choices ) {

										// build an array of our chosen fields to display in this slider
										$choose_posts_fields = get_post_meta( $slider_id, 'choose_products_fields_specgroup', true );
										$posts_fields_array = array();

										if( $choose_posts_fields ) {
											for( $i = 0; $i < $choose_posts_fields; $i++ ) {
												$posts_fields_array[$i]['spec_id'] = esc_html( get_post_meta( $slider_id, 'choose_products_fields_specgroup_' . $i . '_column', true ) );
												$posts_fields_array[$i]['spec_alignment'] = esc_html( get_post_meta( $slider_id, 'choose_products_fields_specgroup_' . $i . '_field_alignment_protool', true ) );
												$posts_fields_array[$i]['spec_link'] = esc_html( get_post_meta( $slider_id, 'choose_products_fields_specgroup_' . $i . '_field_link', true ) );
											}
										}

										$slider_html_specs .=  '<div class="galfram-protool-spec protool-row protool-row-0"></div>';

										$i=1;

										foreach ( $posts_fields_array as $field ) {

											$field_id = $field['spec_id'];
											$slider_html_specs .=  '<div class="galfram-protool-spec protool-row protool-row-'.$i.' '.$spec_choices[$field_id]['type'].'"><span class="spec-text">';
												$slider_html_specs .=  $spec_choices[$field_id]['name'];
											$slider_html_specs .=  '</span></div>';
											$i++;
										}

									}


								}

								else if ( $specs_group_id == 'pull-from-amazon' ) {


									$display_fields = get_post_meta( $slider_id, 'choose_products_fields_specgroup', true );
									$spec_choices = array();

									if( $display_fields ) {
										for( $i = 0; $i < $display_fields; $i++ ) {

											$ama_type = esc_html( get_post_meta( $slider_id, 'choose_products_fields_specgroup_' . $i . '_column', true ) );
											$field_alignment = esc_html( get_post_meta( $slider_id, 'choose_products_fields_specgroup_' . $i . '_field_alignment_protool', true ) );
											$field_link = esc_html( get_post_meta( $slider_id, 'choose_products_fields_specgroup_' . $i . '_field_link', true ) );

											// get the amazon spec name cleaned for displaying in the slider
											$ama_name = $galfram_sliders_functions->get_galfram_ama_spec_name_clean( $ama_type );
											
											$spec_choices[$i]['name'] = $ama_name;
											$spec_choices[$i]['type'] = 'ama';
											$spec_choices[$i]['ama_type'] = $ama_type;
											$spec_choices[$i]['spec_alignment'] = $field_alignment;
											$spec_choices[$i]['spec_link'] = $field_link;
											
										}
									}


									if ( $spec_choices ) {

										// build an array of our chosen fields to display in this slider
										$choose_posts_fields = get_post_meta( $slider_id, 'choose_products_fields_specgroup', true );

										// get our chosen specs to display
										$posts_fields_array = $display_fields;

										$slider_html_specs .=  '<div class="galfram-protool-spec protool-row protool-row-0"></div>';

										$i=1;
										$iii=0;

										foreach ( $spec_choices as $field ) {
											$slider_html_specs .=  '<div class="galfram-protool-spec protool-row protool-row-'.$i.' '.$spec_choices[$field]['type'].'">';
												$slider_html_specs .=  $spec_choices[$iii]['name'];
											$slider_html_specs .=  '</div>';
											$i++;
											$iii++;
										}

									}


								}


							$slider_html_specs .= '</div>'; // END .galfram-protool-specs






							

							// get our chosen product reviews from our slider
							$chosen_products = get_post_meta( $slider_id, 'choose_products_from_specs', true );

							if( $chosen_products ) {

								if ( $posts_fields_array ) {

									// save all of our chosen product review IDs into an array
									$all_prod_ids = array();
									for( $ix = 0; $ix < $chosen_products; $ix++ ) {
										$all_prod_ids[$ix] = esc_html( get_post_meta( $slider_id, 'choose_products_from_specs_' . $ix . '_galfram_slider_column_prod', true ) );

									}

									// get all of our amazon api data in an array. organized by product asin
									$final_item_data = $galfram_sliders_functions->galfram_slider_get_asin_groups_data( $all_prod_ids );

									$slider_html .= $slider_html_specs;

									$slider_html .= '<div class="galfram-protool-slider"'.$slider_attributes.'>';

										for( $i = 0; $i < $chosen_products; $i++ ) {

											$prod_id = esc_html( get_post_meta( $slider_id, 'choose_products_from_specs_' . $i . '_galfram_slider_column_prod', true ) );

											// get our slide fields
											$slider_html_fields = $galfram_sliders_functions->get_galfram_protool_fields( $prod_id, $posts_fields_array, $slider_id, $specs_group_id, $final_item_data );

											$slider_html .= '<div class="galfram-protool-slide galfram-match-specs-height">';
												$slider_html .= $slider_html_fields;
											$slider_html .= '</div>'; // END .galfram-post-slide.galfram-slide

										}

									$slider_html .= '</div>'; // END .galfram-protool-slider

								}
								else {
									$slider_html .= '<p class="callout alert">'.__('You must add at least one product specification to display a Comparison ProTool here.', 'ultimateazon2').'</p>';
								}

							}
							else {
								$slider_html .= '<p class="callout alert">'.__('You must add at least one product to display a Comparison ProTool here.', 'ultimateazon2').'</p>';
							}

							$slider_html .= '<div class="galfram-slider-clear"></div>';

						} // END if ( $specs_group_id )
						else {
							$slider_html .= '<p class="callout alert">'.__('You must choose a specification group to display a Comparison ProTool here.', 'ultimateazon2').'</p>';
						}

					}

					else if ( $slider_type == 'choose-pull-amazon' ) {
						


						$choose_posts_fields = get_post_meta( $slider_id, 'choose_products_fields_specgroup_protool_allamazon', true );
						$posts_fields_array = array();

						if( $choose_posts_fields ) {

							$slider_html_specs .= '<div class="galfram-slider-outer galfram-protool'.$slider_nav_types.'">';
							$slider_html_specs .= '<div class="galfram-protool-specs galfram-match-specs-height">';

							for( $i = 0; $i < $choose_posts_fields; $i++ ) {

								$ama_type = esc_html( get_post_meta( $slider_id, 'choose_products_fields_specgroup_protool_allamazon_' . $i . '_column', true ) );

								// get the amazon spec name cleaned for displaying in the slider
								$ama_name = $galfram_sliders_functions->get_galfram_ama_spec_name_clean( $ama_type );

								$posts_fields_array[$i]['spec_name'] = $ama_name;
								$posts_fields_array[$i]['spec_id'] = $ama_type;
								$posts_fields_array[$i]['spec_alignment'] = esc_html( get_post_meta( $slider_id, 'choose_products_fields_specgroup_protool_allamazon_' . $i . '_field_alignment', true ) );
								$posts_fields_array[$i]['spec_link'] = esc_html( get_post_meta( $slider_id, 'choose_products_fields_specgroup_protool_allamazon_' . $i . '_field_link', true ) );
							}

							$slider_html_specs .=  '<div class="galfram-protool-spec protool-row protool-row-0"></div>';

							$i=1;
							foreach ( $posts_fields_array as $field ) {
								// $field_id = $field['spec_id'];
								$slider_html_specs .=  '<div class="galfram-protool-spec protool-row protool-row-'.$i.' '.$field['spec_type'].'">';
									$slider_html_specs .=  $field['spec_name'];
								$slider_html_specs .=  '</div>';
								$i++;
							}

							$slider_html_specs .= '</div>'; // END .galfram-protool-specs

							$slider_html .= $slider_html_specs;






							// get our chosen product reviews from our slider
							$chosen_products = get_post_meta( $slider_id, 'add_amazon_slides_protool', true );

							//echo '<pre>'.print_r($chosen_products,1).'</pre>';

							if( $chosen_products ) {

								

								// save all of our chosen product review IDs into an array
								$all_prod_ids = array();

								for( $ix = 0; $ix < $chosen_products; $ix++ ) {
									$all_prod_ids[$ix] = esc_html( get_post_meta( $slider_id, 'add_amazon_slides_protool_' . $ix . '_ama_asin', true ) );

								}
								$sending_asins=true;

								//echo '<pre>'.print_r($all_prod_ids,1).'</pre>';

								// get all of our amazon api data in an array. organized by product asin
								$final_item_data = $galfram_sliders_functions->galfram_slider_get_asin_groups_data( $all_prod_ids, $sending_asins );

								//echo '<pre>'.print_r($final_item_data,1).'</pre>';


								$slider_html .= '<div class="galfram-protool-slider"'.$slider_attributes.'>';

								for( $i = 0; $i < $chosen_products; $i++ ) {

									$prod_id = $all_prod_ids[$i];

									$specs_group_id = $slider_type;

									//echo '<pre>'.print_r($final_item_data,1).'</pre>';

									// get our slide fields
									$slider_html_fields = $galfram_sliders_functions->get_galfram_protool_fields( $prod_id, $posts_fields_array, $slider_id, $specs_group_id, $final_item_data );


									$slider_html .= '<div class="galfram-protool-slide galfram-match-specs-height">';
										$slider_html .= $slider_html_fields;
									$slider_html .= '</div>'; // END .galfram-post-slide.galfram-slide

								}

								$slider_html .= '</div>'; // END .galfram-slick-slider
								//$slider_html .= '</div>'; // END .galfram-slider-outer


							}
							else {
								$slider_html .= '<p class="callout alert">'.__('You must choose at least one product for a slider to appear here.', 'ultimateazon2').'</p>';
							}

							$slider_html .= '<div class="galfram-slider-clear"></div>';


						}
						else {
							$slider_html .= '<p class="callout alert">'.__('You must choose a fields to display a Comparison ProTool here.', 'ultimateazon2').'</p>';
						}



					}



				}

				$slider_html .= '</div><!-- END .galfram-protool-slider-outer -->'; // END .galfram-protool-slider-outer


		} // END if ( $slider_type )
		else {
			$slider_html .= '<p class="callout alert">'.__('You must choose a slider type for a slider to be displayed here.', 'ultimateazon2').'</p>';
		}

		return $slider_html;

	}







	/** Add our custom styles to the Stylizer Child Theme
	 *
	 * @since    1.0.0
	 */
	public function galfram_add_custom_ext_css_sliders() {


		/***************************/
		// posts/pages slider styles
		/***************************/

		// posts slider styles
		if ( get_field('posts_slide_bg_color','theme-styles') ) :
			echo '.galfram-post-slider .galfram-slide,
				  .galfram-page-slider .galfram-slide{';
				echo 'background-color:'.get_field('posts_slide_bg_color','theme-styles').';';
			echo '}';
		endif;

		// table wrapper styles
		if ( get_field('posts_slide_text_color','theme-styles') ) :
			echo '.galfram-post-slider .galfram-slide .slide-post-title h3, .galfram-post-slider .galfram-slide > div,
				  .galfram-post-slider .galfram-slide .slide-post-title h3, .galfram-post-slider .galfram-slide > div{';
				echo 'color:'.get_field('posts_slide_text_color','theme-styles').';';
			echo '}';
		endif;


		if ( get_field('posts_prevnext_color','theme-styles') ) :
			echo '.galfram-post-slider.arrows-outside-bars .slick-arrow,
				  .galfram-page-slider.arrows-outside-bars .slick-arrow{';
				echo 'background-color:'.get_field('posts_prevnext_color','theme-styles').';';
			echo '}';
		endif;


		if ( get_field('posts_slider_dots_color','theme-styles') ) :
			echo '.galfram-post-slider .slick-dots li button,
				  .galfram-page-slider .slick-dots li button{';
				echo 'background-color:'.get_field('posts_slider_dots_color','theme-styles').';';
			echo '}';
		endif;

		if ( get_field('posts_slider_dots_color_active','theme-styles') ) :
			echo '.galfram-post-slider .slick-dots li.slick-active button,
				  .galfram-post-slider .slick-dots li.slick-active button{';
				echo 'background-color:'.get_field('posts_slider_dots_color_active','theme-styles').';';
			echo '}';
		endif;





		/***************************/
		// custom slider styles
		/***************************/

		// table wrapper styles
		if ( get_field('custom_slide_bg_color','theme-styles') ) :
			echo '.galfram-custom-slider .galfram-slide, .galfram-page-slider .galfram-slide{';
				echo 'background-color:'.get_field('custom_slide_bg_color','theme-styles').';';
			echo '}';
		endif;

		// table wrapper styles
		if ( get_field('custom_slide_text_color','theme-styles') ) :
			echo '#content .galfram-custom-slider .galfram-slide h1,
				  #content .galfram-custom-slider .galfram-slide h1, 
				  #content .galfram-custom-slider .galfram-slide h2, 
				  #content .galfram-custom-slider .galfram-slide h3, 
				  #content .galfram-custom-slider .galfram-slide h4, 
				  #content .galfram-custom-slider .galfram-slide h5, 
				  #content .galfram-custom-slider .galfram-slide h6, 
				  #content .galfram-custom-slider .galfram-slide p, 
				  #content .galfram-custom-slider .galfram-slide span,
				  #content .galfram-custom-slider .galfram-slide li{';
				echo 'color:'.get_field('custom_slide_text_color','theme-styles').';';
			echo '}';
		endif;


		if ( get_field('custom_prevnext_color','theme-styles') ) :
			echo '.galfram-custom-slider.arrows-outside-bars .slick-arrow{';
				echo 'background-color:'.get_field('custom_prevnext_color','theme-styles').';';
			echo '}';
		endif;


		if ( get_field('custom_slider_dots_color','theme-styles') ) :
			echo '.galfram-custom-slider .slick-dots li button{';
				echo 'background-color:'.get_field('custom_slider_dots_color','theme-styles').';';
			echo '}';
		endif;

		if ( get_field('custom_slider_dots_color_active','theme-styles') ) :
			echo '.galfram-custom-slider .slick-dots li.slick-active button{';
				echo 'background-color:'.get_field('custom_slider_dots_color_active','theme-styles').';';
			echo '}';
		endif;











		/***************************/
		// PRP slider styles
		/***************************/


		// slide background color
		if ( get_field('prp_slide_bg_color','theme-styles') ) :
			echo '.galfram-prp-slider .galfram-prp-slide,.galfram-prp-slider .galfram-prp-spec-value{';
				echo 'background-color:'.get_field('prp_slide_bg_color','theme-styles').';';
			echo '}';
		endif;

		// slide border color
		if ( get_field('prp_slide_border_color','theme-styles') ) :
			echo '.galfram-prp-slider,.galfram-prp-slider .galfram-prp-spec-value{';
				echo 'border-color:'.get_field('prp_slide_border_color','theme-styles').';';
			echo '}';
		endif;




		if ( get_field('prp_enable_slider_font_styles','theme-styles') ) :



				// slide font size & line height
				if ( get_field('prp_global_font_size','theme-styles') || get_field('prp_global_line_height','theme-styles') ) :
					echo '.galfram-prp-slider .galfram-prp-spec-value, #content .galfram-prp-slider .galfram-prp-spec-value span{';
						// slide font sixe
						if ( get_field('prp_global_font_size','theme-styles') ) :
							echo 'font-size:'.get_field('prp_global_font_size','theme-styles').'px;';
						endif;
						// slide font line height
						if ( get_field('prp_global_line_height','theme-styles') ) :
							echo 'line-height:'.get_field('prp_global_line_height','theme-styles').'px;';
						endif;
						// slide font color
						if ( get_field('prp_global_color','theme-styles') ) :
							echo 'color:'.get_field('prp_global_color','theme-styles').';';
						endif;
					echo '}';
				endif;


				// slide spec padding
				if ( get_field('prp_slider_field_padding','theme-styles') ) :
					echo '.galfram-prp-slider .galfram-prp-spec-value{';
						echo 'padding:'.get_field('prp_slider_field_padding','theme-styles').'px;';
					echo '}';
				endif;







				/***********************/
				// special spec styles
				/***********************/
				if ( get_field('prp_enable_font_styles_for_field_types','theme-styles') ) :




						// Amazpn Title Field Styles
						if ( get_field('prp_enable_ama_title_field_styles','theme-styles') ) :

							echo '.galfram-prp-slider .galfram-prp-spec-value.amazon-amazon-title {';
								if ( get_field('prp_ama_title_text_color','theme-styles') ) :
									echo 'color:'.get_field('prp_ama_title_text_color','theme-styles').';';
									else:
									echo 'color:#000;';
									endif;

								if ( get_field('prp_ama_title_font_size','theme-styles') ) :
									echo 'font-size:'.get_field('prp_ama_title_font_size','theme-styles').'px;';
									else:
									echo 'font-size:14px;';
									endif;

								if ( get_field('prp_ama_title_line_height','theme-styles') ) :
									echo 'line-height:'.get_field('prp_ama_title_line_height','theme-styles').'px;';
									else:
									echo 'line-height:16px;';
									endif;
							echo '}';

						endif;



						// WordPress Title Field Styles
						if ( get_field('prp_enable_wp_title_field_styles','theme-styles') ) :

							echo '.galfram-prp-slider .galfram-prp-spec-value.amazon-wp-title {';
								if ( get_field('prp_wp_title_text_color','theme-styles') ) :
									echo 'color:'.get_field('prp_wp_title_text_color','theme-styles').';';
									else:
									echo 'color:#000;';
									endif;

								if ( get_field('prp_wp_title_font_size','theme-styles') ) :
									echo 'font-size:'.get_field('prp_wp_title_font_size','theme-styles').'px;';
									else:
									echo 'font-size:14px;';
									endif;

								if ( get_field('prp_wp_title_line_height','theme-styles') ) :
									echo 'line-height:'.get_field('prp_wp_title_line_height','theme-styles').'px;';
									else:
									echo 'line-height:16px;';
									endif;
							echo '}';

						endif;



						// Custom Button special field styles
						if ( get_field('prp_enable_button_field_styles','theme-styles') ) :
							echo '.galfram-prp-slider a.spec-button:not(.button-custom-img) {';
								if ( get_field('prp_custom_button_bg_color','theme-styles') ) :
									echo 'background-color:'.get_field('prp_custom_button_bg_color','theme-styles').';';
								endif;
								if ( get_field('prp_custom_button_text_color','theme-styles') ) :
									echo 'color:'.get_field('prp_custom_button_text_color','theme-styles').';';
								endif;
							echo '}';
							echo '.galfram-prp-slider a.spec-button:not(.button-custom-img):hover {';
								if ( get_field('prp_custom_button_bg_color_hover','theme-styles') ) :
									echo 'background-color:'.get_field('prp_custom_button_bg_color_hover','theme-styles').';';
								endif;
								if ( get_field('prp_custom_button_text_color_hover','theme-styles') ) :
									echo 'color:'.get_field('prp_custom_button_text_color_hover','theme-styles').';';
								endif;
							echo '}';
						endif;




						// Text Link special field styles
						if ( get_field('prp_enable_textlink_field_styles','theme-styles') ) :
							echo '.galfram-prp-slider a:not(.spec-button) {';
								if ( get_field('prp_text_link_color','theme-styles') ) :
									echo 'color:'.get_field('prp_text_link_color','theme-styles').';';
								endif;
							echo '}';
							echo '.galfram-prp-slider a:not(.spec-button):hover {';
								if ( get_field('prp_text_link_color_hover','theme-styles') ) :
									echo 'color:'.get_field('prp_text_link_color_hover','theme-styles').';';
								endif;
							echo '}';
						endif;



						// ASIN special field styles
						if ( get_field('prp_enable_asin_field_styles','theme-styles') ) :

							echo '.galfram-prp-slider .galfram-prp-spec-value.amazon-asin, #content .galfram-prp-slider .galfram-prp-spec-value.amazon-asin .spec-label {';
								if ( get_field('prp_asin_text_color','theme-styles') ) :
									echo 'color:'.get_field('prp_asin_text_color','theme-styles').';';
									else:
									echo 'color:#000;';
									endif;

								if ( get_field('prp_asin_font_size','theme-styles') ) :
									echo 'font-size:'.get_field('prp_asin_font_size','theme-styles').'px;';
									else:
									echo 'font-size:14px;';
									endif;

								if ( get_field('prp_asin_line_height','theme-styles') ) :
									echo 'line-height:'.get_field('prp_asin_line_height','theme-styles').'px;';
									else:
									echo 'line-height:16px;';
									endif;
							echo '}';

						endif;


						// Brand special field styles
						if ( get_field('prp_enable_brand_field_styles','theme-styles') ) :

							echo '.galfram-prp-slider .galfram-prp-spec-value.amazon-brand, #content .galfram-prp-slider .galfram-prp-spec-value.amazon-brand .spec-label {';
								if ( get_field('prp_brand_text_color','theme-styles') ) :
									echo 'color:'.get_field('prp_brand_text_color','theme-styles').';';
									else:
									echo 'color:#000;';
									endif;

								if ( get_field('prp_brand_font_size','theme-styles') ) :
									echo 'font-size:'.get_field('prp_brand_font_size','theme-styles').'px;';
									else:
									echo 'font-size:14px;';
									endif;

								if ( get_field('prp_brand_line_height','theme-styles') ) :
									echo 'line-height:'.get_field('prp_brand_line_height','theme-styles').'px;';
									else:
									echo 'line-height:16px;';
									endif;
							echo '}';

						endif;



						// Model special field styles
						if ( get_field('prp_enable_model_field_styles','theme-styles') ) :

							echo '.galfram-prp-slider .galfram-prp-spec-value.amazon-model, #content .galfram-prp-slider .galfram-prp-spec-value.amazon-model .spec-label {';
								if ( get_field('prp_model_text_color','theme-styles') ) :
									echo 'color:'.get_field('prp_model_text_color','theme-styles').';';
									else:
									echo 'color:#000;';
									endif;

								if ( get_field('prp_model_font_size','theme-styles') ) :
									echo 'font-size:'.get_field('prp_model_font_size','theme-styles').'px;';
									else:
									echo 'font-size:14px;';
									endif;

								if ( get_field('prp_model_line_height','theme-styles') ) :
									echo 'line-height:'.get_field('prp_model_line_height','theme-styles').'px;';
									else:
									echo 'line-height:16px;';
									endif;
							echo '}';

						endif;




						// UPC special field styles
						if ( get_field('prp_enable_upc_field_styles','theme-styles') ) :

							echo '.galfram-prp-slider .galfram-prp-spec-value.amazon-upc, #content .galfram-prp-slider .galfram-prp-spec-value.amazon-upc .spec-label {';
								if ( get_field('prp_upc_text_color','theme-styles') ) :
									echo 'color:'.get_field('prp_upc_text_color','theme-styles').';';
									else:
									echo 'color:#000;';
									endif;

								if ( get_field('prp_upc_font_size','theme-styles') ) :
									echo 'font-size:'.get_field('prp_upc_font_size','theme-styles').'px;';
									else:
									echo 'font-size:14px;';
									endif;

								if ( get_field('prp_upc_line_height','theme-styles') ) :
									echo 'line-height:'.get_field('prp_upc_line_height','theme-styles').'px;';
									else:
									echo 'line-height:16px;';
									endif;
							echo '}';

						endif;







						// Features special field styles
						if ( get_field('prp_enable_features_field_styles','theme-styles') ) :

							echo '#content .galfram-prp-slider .galfram-prp-spec-value.amazon-features li{';
								if ( get_field('prp_features_text_color','theme-styles') ) :
									echo 'color:'.get_field('prp_features_text_color','theme-styles').';';
									else:
									echo 'color:#505050;';
									endif;

								if ( get_field('prp_features_font_size','theme-styles') ) :
									echo 'font-size:'.get_field('prp_features_font_size','theme-styles').'px;';
									else:
									echo 'font-size:14px;';
									endif;

								if ( get_field('prp_features_line_height','theme-styles') ) :
									echo 'line-height:'.get_field('prp_features_line_height','theme-styles').'px;';
									else:
									echo 'line-height:16px;';
									endif;
							echo '}';

						endif;





						// Warranty special field styles
						if ( get_field('prp_enable_warranty_field_styles','theme-styles') ) :

							echo '.galfram-prp-slider .galfram-prp-spec-value.amazon-warranty, #content .galfram-prp-slider .galfram-prp-spec-value.amazon-warranty .spec-label  {';
								if ( get_field('prp_warranty_text_color','theme-styles') ) :
									echo 'color:'.get_field('prp_warranty_text_color','theme-styles').';';
									else:
									echo 'color:#000;';
									endif;

								if ( get_field('prp_warranty_font_size','theme-styles') ) :
									echo 'font-size:'.get_field('prp_warranty_font_size','theme-styles').'px;';
									else:
									echo 'font-size:14px;';
									endif;

								if ( get_field('prp_warranty_line_height','theme-styles') ) :
									echo 'line-height:'.get_field('prp_warranty_line_height','theme-styles').'px;';
									else:
									echo 'line-height:16px;';
									endif;
							echo '}';

						endif;






						// Star Rating special field styles
						if ( get_field('prp_enable_star_rating_field_styles','theme-styles') ) :

							echo '#content .galfram-prp-slider .galfram-prp-spec-value .editor-rating span{';
								if ( get_field('prp_star_icon_size','theme-styles') ) :
									echo 'font-size:'.get_field('prp_star_icon_size','theme-styles').'px;';
								echo 'line-height:'.get_field('prp_star_icon_size','theme-styles').'px;';
								endif; 
								if ( get_field('prp_star_icon_color','theme-styles') ) :
									echo 'color:'.get_field('prp_star_icon_color','theme-styles').';';
								endif;
							echo '}';

						endif;






						// Yes/No special field styles
						// if ( get_field('prp_enable_yesno_rating_field_styles','theme-styles') ) :

						// 	echo '#content .galfram-prp-slider .galfram-prp-spec-value .editor-rating span{';
						// 		if ( get_field('prp_star_icon_size','theme-styles') ) :
						// 			echo 'font-size:'.get_field('prp_star_icon_size','theme-styles').'px;';
						// 		echo 'line-height:'.get_field('prp_star_icon_size','theme-styles').'px;';
						// 		endif; 
						// 		if ( get_field('prp_star_icon_color','theme-styles') ) :
						// 			echo 'color:'.get_field('prp_star_icon_color','theme-styles').';';
						// 		endif;
						// 	echo '}';

						// endif;






						// Price special field styles
						if ( get_field('prp_enable_price_field_styles', 'theme-styles') ) :

							//die();

							if ( get_field('prp_sale_price_font_size', 'theme-styles') || get_field('prp_sale_price_color','theme-styles') ) :

								echo '.galfram-prp-slider .galfram-prp-spec-value.amazon-price {';

								//echo '.galfram-table tbody tr:nth-child(odd) td.galfram_price .deal-price,.galfram-table tbody tr:nth-child(even) td.galfram_price .deal-price{';

									if ( get_field('prp_sale_price_font_size','theme-styles') ) :
										echo 'font-size:'.get_field('prp_sale_price_font_size','theme-styles').'px;';
										endif;

									if ( get_field('prp_sale_price_color','theme-styles') ) :
										echo 'color:'.get_field('prp_sale_price_color','theme-styles').';';
										endif;

								echo '}';

							endif;

						endif;






						// Lowest NEW Price special field styles
						if ( get_field('prp_enable_lowest_new_price_field_styles', 'theme-styles') ) :

							//die();

							if ( get_field('prp_lowest_new_price_font_size', 'theme-styles') || get_field('prp_lowest_new_price_line_height','theme-styles') || get_field('prp_lowest_new_price_text_color','theme-styles') ) :

								echo '.galfram-prp-slider .galfram-prp-spec-value.amazon-lowest-new-price {';

								//echo '.galfram-table tbody tr:nth-child(odd) td.galfram_price .deal-price,.galfram-table tbody tr:nth-child(even) td.galfram_price .deal-price{';

									if ( get_field('prp_lowest_new_price_font_size','theme-styles') ) :
										echo 'font-size:'.get_field('prp_lowest_new_price_font_size','theme-styles').'px;';
										endif;

									if ( get_field('prp_lowest_new_price_line_height','theme-styles') ) :
										echo 'line-height:'.get_field('prp_lowest_new_price_line_height','theme-styles').'px;';
										endif;

									if ( get_field('prp_lowest_new_price_text_color','theme-styles') ) :
										echo 'color:'.get_field('prp_lowest_new_price_text_color','theme-styles').';';
										endif;

								echo '}';

							endif;

						endif;






						// Lowest USED Price special field styles
						if ( get_field('prp_enable_lowest_used_price_field_styles', 'theme-styles') ) :

							//die();

							if ( get_field('prp_lowest_used_price_font_size', 'theme-styles') || get_field('prp_lowest_used_price_line_height','theme-styles') || get_field('prp_lowest_used_price_text_color','theme-styles') ) :

								echo '.galfram-prp-slider .galfram-prp-spec-value.amazon-lowest-used-price {';

								//echo '.galfram-table tbody tr:nth-child(odd) td.galfram_price .deal-price,.galfram-table tbody tr:nth-child(even) td.galfram_price .deal-price{';

									if ( get_field('prp_lowest_used_price_font_size','theme-styles') ) :
										echo 'font-size:'.get_field('prp_lowest_used_price_font_size','theme-styles').'px;';
										endif;

									if ( get_field('prp_lowest_used_price_line_height','theme-styles') ) :
										echo 'line-height:'.get_field('prp_lowest_used_price_line_height','theme-styles').'px;';
										endif;

									if ( get_field('prp_lowest_used_price_text_color','theme-styles') ) :
										echo 'color:'.get_field('prp_lowest_used_price_text_color','theme-styles').';';
										endif;

								echo '}';

							endif;

						endif;




				endif; // END if ( get_field('prp_enable_font_styles_for_field_types','theme-styles') ) 














						/*************************************/
						// table row font special field styles
						/*************************************/
						if ( get_field('enable_font_styles_for_field_types','theme-styles') ) :




								// Yes/No special field styles
								if ( get_field('enable_yesno_rating_field_styles','theme-styles') ) :

									echo '.galfram-table tbody tr td.galfram_yesno {';
										
									echo '}';

								endif;




								



								// Lowest New Price special field styles
								if ( get_field('enable_lowest_new_price_field_styles','theme-styles') ) :

									echo '.galfram-table tbody tr td.galfram_lowest-new-price {';
										if ( get_field('lowest_new_price_text_color','theme-styles') ) :
											echo 'color:'.get_field('lowest_new_price_text_color','theme-styles').';';
											else:
											echo 'color:#000;';
											endif;

										if ( get_field('lowest_new_price_font_size','theme-styles') ) :
											echo 'font-size:'.get_field('lowest_new_price_font_size','theme-styles').'px;';
											else:
											echo 'font-size:14px;';
											endif;

										if ( get_field('lowest_new_price_line_height','theme-styles') ) :
											echo 'line-height:'.get_field('lowest_new_price_line_height','theme-styles').'px;';
											else:
											echo 'line-height:16px;';
											endif;
									echo '}';

								endif;




								// Lowest USED Price special field styles
								if ( get_field('enable_lowest_used_price_field_styles','theme-styles') ) :

									echo '.galfram-table tbody tr td.galfram_lowest-used-price {';
										if ( get_field('lowest_used_price_text_color','theme-styles') ) :
											echo 'color:'.get_field('lowest_used_price_text_color','theme-styles').';';
											else:
											echo 'color:#000;';
											endif;

										if ( get_field('lowest_used_price_font_size','theme-styles') ) :
											echo 'font-size:'.get_field('lowest_used_price_font_size','theme-styles').'px;';
											else:
											echo 'font-size:14px;';
											endif;

										if ( get_field('lowest_used_price_line_height','theme-styles') ) :
											echo 'line-height:'.get_field('lowest_used_price_line_height','theme-styles').'px;';
											else:
											echo 'line-height:16px;';
											endif;
									echo '}';

								endif;




								// Lowest NEW Price special field styles
								if ( get_field('enable_lowest_new_price_field_styles','theme-styles') ) :

									echo '.galfram-table tbody tr td.galfram_lowest-new-price {';
										if ( get_field('lowest_new_price_text_color','theme-styles') ) :
											echo 'color:'.get_field('lowest_new_price_text_color','theme-styles').';';
											else:
											echo 'color:#000;';
											endif;

										if ( get_field('lowest_new_price_font_size','theme-styles') ) :
											echo 'font-size:'.get_field('lowest_new_price_font_size','theme-styles').'px;';
											else:
											echo 'font-size:14px;';
											endif;

										if ( get_field('lowest_new_price_line_height','theme-styles') ) :
											echo 'line-height:'.get_field('lowest_new_price_line_height','theme-styles').'px;';
											else:
											echo 'line-height:16px;';
											endif;
									echo '}';

								endif;






						endif; // END if ( get_field('enable_font_styles_for_field_types','theme-styles') )

				endif; // end if ( get_field('enable_table_row_font_styles','theme-styles') )

		//endif; // END if ( get_field('prp_enable_slider_font_styles','theme-styles') )








	}




}
