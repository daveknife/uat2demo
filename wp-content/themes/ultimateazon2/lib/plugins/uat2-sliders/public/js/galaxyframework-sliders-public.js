(function( $ ) {
	'use strict';

	/**
	 * All of the code for your public-facing JavaScript source
	 * should reside in this file.
	 *
	 * Note: It has been assumed you will write jQuery code here, so the
	 * $ function reference has been prepared for usage within the scope
	 * of this function.
	 *
	 * This enables you to define handlers, for when the DOM is ready:
	 *
	 * $(function() {
	 *
	 * });
	 *
	 * When the window is loaded:
	 *
	 * $( window ).load(function() {
	 *
	 * });
	 *
	 * ...and/or other possibilities.
	 *
	 * Ideally, it is not considered best practise to attach more than a
	 * single DOM-ready or window-load handler for a particular page.
	 * Although scripts in the WordPress core, Plugins and Themes may be
	 * practising this, we should strive to set a better example in our own work.
	 */


	 /**
	* jquery.matchHeight-min.js master
	* http://brm.io/jquery-match-height/
	* License: MIT
	*/
	(function(c){var n=-1,f=-1,g=function(a){return parseFloat(a)||0},r=function(a){var b=null,d=[];c(a).each(function(){var a=c(this),k=a.offset().top-g(a.css("margin-top")),l=0<d.length?d[d.length-1]:null;null===l?d.push(a):1>=Math.floor(Math.abs(b-k))?d[d.length-1]=l.add(a):d.push(a);b=k});return d},p=function(a){var b={byRow:!0,property:"height",target:null,remove:!1};if("object"===typeof a)return c.extend(b,a);"boolean"===typeof a?b.byRow=a:"remove"===a&&(b.remove=!0);return b},b=c.fn.matchHeight=
	function(a){a=p(a);if(a.remove){var e=this;this.css(a.property,"");c.each(b._groups,function(a,b){b.elements=b.elements.not(e)});return this}if(1>=this.length&&!a.target)return this;b._groups.push({elements:this,options:a});b._apply(this,a);return this};b._groups=[];b._throttle=80;b._maintainScroll=!1;b._beforeUpdate=null;b._afterUpdate=null;b._apply=function(a,e){var d=p(e),h=c(a),k=[h],l=c(window).scrollTop(),f=c("html").outerHeight(!0),m=h.parents().filter(":hidden");m.each(function(){var a=c(this);
	a.data("style-cache",a.attr("style"))});m.css("display","block");d.byRow&&!d.target&&(h.each(function(){var a=c(this),b="inline-block"===a.css("display")?"inline-block":"block";a.data("style-cache",a.attr("style"));a.css({display:b,"padding-top":"0","padding-bottom":"0","margin-top":"0","margin-bottom":"0","border-top-width":"0","border-bottom-width":"0",height:"100px"})}),k=r(h),h.each(function(){var a=c(this);a.attr("style",a.data("style-cache")||"")}));c.each(k,function(a,b){var e=c(b),f=0;if(d.target)f=
	d.target.outerHeight(!1);else{if(d.byRow&&1>=e.length){e.css(d.property,"");return}e.each(function(){var a=c(this),b={display:"inline-block"===a.css("display")?"inline-block":"block"};b[d.property]="";a.css(b);a.outerHeight(!1)>f&&(f=a.outerHeight(!1));a.css("display","")})}e.each(function(){var a=c(this),b=0;d.target&&a.is(d.target)||("border-box"!==a.css("box-sizing")&&(b+=g(a.css("border-top-width"))+g(a.css("border-bottom-width")),b+=g(a.css("padding-top"))+g(a.css("padding-bottom"))),a.css(d.property,
	f-b))})});m.each(function(){var a=c(this);a.attr("style",a.data("style-cache")||null)});b._maintainScroll&&c(window).scrollTop(l/f*c("html").outerHeight(!0));return this};b._applyDataApi=function(){var a={};c("[data-match-height], [data-mh]").each(function(){var b=c(this),d=b.attr("data-mh")||b.attr("data-match-height");a[d]=d in a?a[d].add(b):b});c.each(a,function(){this.matchHeight(!0)})};var q=function(a){b._beforeUpdate&&b._beforeUpdate(a,b._groups);c.each(b._groups,function(){b._apply(this.elements,
	this.options)});b._afterUpdate&&b._afterUpdate(a,b._groups)};b._update=function(a,e){if(e&&"resize"===e.type){var d=c(window).width();if(d===n)return;n=d}a?-1===f&&(f=setTimeout(function(){q(e);f=-1},b._throttle)):q(e)};c(b._applyDataApi);c(window).bind("load",function(a){b._update(!1,a)});c(window).bind("resize orientationchange",function(a){b._update(!0,a)})})(jQuery);



	 jQuery(document).ready(function() {

		/****************************************
		These functions are for front end styling
		****************************************/


		// initialize the slick slider for our galfram_slider
		// options are set via data attributes
		$('.galfram-post-slider .galfram-slick-slider').slick();

		$('.galfram-post-slider .galfram-slick-slider').on('beforeChange', function(event, slick, currentSlide, nextSlide){
			// equalizes slides after slide change
			$('.galfram-post-slide').matchHeight({byRow: false});
		});

		$('.galfram-post-slide').matchHeight({byRow: false});





		$('.galfram-page-slider .galfram-slick-slider').slick({

		});

		$('.galfram-page-slider .galfram-slick-slider').on('beforeChange', function(event, slick, currentSlide, nextSlide){
			// equalizes slides after slide change
			$('.galfram-post-slide').matchHeight({byRow: false});
		});






		$('.galfram-custom-slider .galfram-slick-slider').slick({

		});

		$('.galfram-custom-slider .galfram-slick-slider').on('beforeChange', function(event, slick, currentSlide, nextSlide){
			// equalizes slides after slide change
			$('.galfram-post-slide').matchHeight({byRow: false});
		});






		// options are set via data attributes
		$('.galfram-prp-slider .galfram-slick-slider').slick();

		$('.galfram-prp-slider .galfram-slick-slider').on('beforeChange', function(event, slick, currentSlide, nextSlide){
			// equalizes slides after slide change
			// equalizer the Pro Tool Rows
			$( ".protool-row" ).each(function( index ) {
				$('.protool-row-'+index).matchHeight({byRow: false});
			});
		});

		// Equalize the PRP Slider Rows
		$( ".prpslider-row" ).each(function( index ) {
			$('.prpslider-row-'+index).matchHeight({byRow: false});
		});





		// initialize the slick slider for our galfram ProTool
		// options are set via data attributes
		$('.galfram-protool-slider').slick();

		$('.galfram-protool-slider').on('beforeChange', function(event, slick, currentSlide, nextSlide){
			// equalizes slides after slide change
			console.log('next slide');
			// equalizer the Pro Tool Rows
			$( ".protool-row" ).each(function( index ) {
				$('.protool-row-'+index).matchHeight({byRow: false});
			});
		});

		// equalizer the Pro Tool Rows
		$( ".protool-row" ).each(function( index ) {
			$('.protool-row-'+index).matchHeight({byRow: false});
		});





		// $('div[data-equalizer=slide-inner]').on('postequalized.zf.equalizer', function() {
	 //        $(this).find('.resources-slider').addClass('sliderloaded');
	 //        $(this).addClass('sliderloaded');
	 //    });

	 if ( $('.galfram-slider-outer').length ) {

	    $.fn.matchHeight._afterUpdate = function(event, groups) {
		    $('.galfram-slider-outer').addClass('sliderloaded');
		    $('.galfram-slick-slider').addClass('sliderloaded');
		    $('.galfram-protool-specs').addClass('sliderloaded');
		    $('.galfram-protool-slider').addClass('sliderloaded');
		    
		}

	}









	});



})( jQuery );
