<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       https://incomegalaxy.com
 * @since      1.0.0
 *
 * @package    galfram_Tables
 * @subpackage galfram_Tables/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    galfram_Tables
 * @subpackage galfram_Tables/public
 * @author     Your Name <email@example.com>
 */
class galfram_tables_Public {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $galfram_tables    The ID of this plugin.
	 */
	private $galfram_tables;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $galfram_tables       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $galfram_tables, $version ) {

		$this->galfram_tables = $galfram_tables;
		$this->version = $version;

	}


	




	/**Set our custom loop to use with our post type galfram_table
	 *
	 * @since    1.0.0
	 */
	function get_galfram_table_post_type_template() {

		global $post;

		if ( is_singular('galfram_table') ) {
			remove_action( 'uat2_post_loop', 'uat2_post_loop_function', 10 );
			add_action( 'uat2_post_loop', 'galfram_table_loop_function', 10, 1 );
		}

		

		// pass our table attributes to our table layout loop
		function galfram_table_loop_function( $table_attributes ) {

			global $post;

			echo '<div class="content-inner-row">';
				echo '<div class="content-inner-row-main galfram-table">';
					do_action( 'galfram_before_loop' );

					if (have_posts()) : 
						while (have_posts()) : 
							the_post();
								?>
							<article id="post-<?php the_ID(); ?>" <?php post_class(''); ?> role="review">

								<header class="article-header">
									<h1 class="page-title"><?php the_title(); ?></h1>
								</header>

								<section class="entry-content">
									<?php uat2_before_loop_content(); ?>
									<div class="table-post-content"><?php the_content(); ?></div>
									<?php echo do_shortcode('[uat2_table id="'.get_the_id().'"]'); ?>
									<?php uat2_after_loop_content(); ?>
								</section> <!-- end article section -->

							</article> <!-- end article -->

						<?php
						endwhile;
					else:
						uat2_missing_loop();
					endif;

					do_action( 'uat2_after_loop' );
				echo '</div> <!-- end .content-inner-row-main.galfram-table -->';
				echo '</div> <!-- end .content-inner-row -->';

		} // end galfram_table_loop_function()

	}




	

	
	// [uat2_table id="id-value"]
	public function galfram_layout_table( $atts ) {
		
	    // set our table ID variable form the id attribute of the shortcode
	    $table_id = $atts['id'];

	    // set our empty variable to return our table with at the end
	    $table_html = '';

	    // create a new instance of our tables class if it exists
		$galfram_tables_functions = new galfram_tables_functions(); 
			
		// get our Table Attributes
		$table_attributes = $galfram_tables_functions->get_galfram_table_attributes( $table_id );

		// Get our table type
		$table_type = get_post_meta( $table_id, 'galfram_type_of_table', true );

		$new_tab = get_option( 'extension-prp-settings_open_aff_links_in_new_window', true );
		if ( $new_tab == 1 ) { $new_tab_html = ' target="_blank"'; }
		else { $new_tab_html = ''; }

		$no_follow = get_option( 'extension-prp-settings_no-follow_affiliate_links', true );
		if ( $no_follow == 1 ) { $no_follow_html = ' rel="nofollow"'; }
		else { $no_follow_html = ''; }


		if ( $table_type == 'prp-table' ) {

			$table_arr = get_post_meta( $table_id, 'galfram_prp_table_array', true );


			if ( $table_arr ) {

				// get the saved spec group for this table and create our value
				$specs_group_id = get_post_meta( $table_id, 'galfram_product_spec_group', true);

				// // update our $final_table to include the table body rows
				if ( isset( $table_arr['final_table'] ) ) {
					$final_table = $table_arr['final_table'];
				}

				// // get our $prod_id
				$prod_id = $table_arr['prod_id'];

				// create our table tag and outer container
				if ( $final_table ) {
					$table_html .= '<div class="galfram-table-outer galfram-prp-table galfram-table-'.$table_id.'" data-table-name="'.get_the_title($table_id).'">';
					$table_html .= '<table class="galfram-table"'.$table_attributes.'>';
				}

				//echo '<pre>'.print_r($final_table,1).'</pre>';


				// if the specs group is a custom one, and not 100% pulled from Amazon
				if ( $specs_group_id != 'pull-from-amazon' ) {

					// let's build out table header
			        if ( $final_table['header'] ) {

			            $table_html .= '<thead>';
			                $table_html .= '<tr>';

			                	$tbl=0;

			                    foreach ( $final_table['header'] as $th ) {

			                		$breakpoints = '';
			                		$breakpoints_string = '';

			                		$col_hidden_on = $th['col_hidden_on'];

			                		if ( $col_hidden_on ) {
			                			foreach ( $col_hidden_on as $mq ) { $breakpoints_string .= $mq.' '; }
			                		}

			                		//USE data-filterable="false" for columns not suited for filtering like mages

			                		$breakpoints = ' data-breakpoints="'.$breakpoints_string.'"';
			                		$coltype = $th['col_type'];
			                	
			                		// custom spec types
			                		if ( $coltype == 'text' || $coltype == 'p-text' || $coltype == 'yes-no'  ) { 
			                			$coltype_string = ' data-value-type="'.$coltype.'" data-type="text"'; 
			                		}
			                		else if ( $coltype == 'star-rating' || $coltype == 'price'  ) { 
			                			$coltype_string = ' data-value-type="'.$coltype.'" data-type="html" data-decimal-separator="."'; 
			                		}
			                		else if ( $coltype == 'image' || $coltype == 'spec-button' || $coltype == 'spec-shortcode'  ) { 
			                			$coltype_string = ' data-value-type="'.$coltype.'" data-sortable="false" data-type="html"'; 
			                		}
			                		// amazon spec types
			                		else if ( $coltype == 'amazon_features'  ) { 
			                			$coltype_string = ' data-value-type="'.$coltype.'" data-type="html"';
			                		}
			                		else if ( $coltype == 'amazon_asin' || $coltype == 'amazon_brand' || $coltype == 'amazon_upc' || $coltype == 'amazon_model' || $coltype == 'amazon_warranty' ) { 
			                			$coltype_string = ' data-value-type="'.$coltype.'" data-type="text"';
			                		}
			                		else if ( $coltype == 'amazon_price' || $coltype == 'amazon_lowest-new-price' || $coltype == 'amazon_lowest-used-price'  ) { 
			                			$coltype_string = ' data-value-type="'.$coltype.'" data-type="price"';
			                		}
			                		else if ( $coltype == 'amazon_small-image' || $coltype == 'amazon_medium-image' || $coltype == 'amazon_large-image'  ) { 
			                			$coltype_string = ' data-sortable="false" data-type="html"';
			                		}
			                		else { $coltype_string = ''; }


			                		if ( $th['col_disable_sort'] == 1 ) {
			                			$disable_sort = ' data-value-type="'.$coltype.'" data-sortable="false"';
			                		}
			                		else {
			                			$disable_sort = '';
			                		}

			                		// if ( $th['col_aff_link'] == '1' ) {
			                		// 	$coltype_string = ' data-value-type="'.$coltype.'" data-type="html"';
			                		// }

			                		$col_link = $th['col_aff_link'];

			                		if ( $col_link == 'field-link-affiliate' || $col_link == 'field-link-review'  ) {
			                			$coltype_string = ' data-value-type="'.$coltype.'" data-type="html"';
			                		}

			                		$table_html .= '<th'.$breakpoints.$coltype_string.$disable_sort.'>';
			                        	$table_html .= $th['col_name'];
			                   		$table_html .= '</th>';

			                        $tbl++;
			                    }

			                $table_html .= '</tr>';
			            $table_html .= '</thead>';
			        }


			        // if there is any amazon specs present, let's build an array of the product asins to loop through below to make amazon api calls for ten products at a time
					if ( $final_table['amazon_specs'] == true && $final_table['body'] ) {
						$final_item_data = $galfram_tables_functions->galfram_table_get_asin_groups_data( $final_table, $prod_id, $specs_group_id );
					}


					if ( $final_table['body'] ) {


				        $table_html .= '<tbody>';

				        	// MAKE SURE TO USE CLEAN FILTER VALUES data-filter-value="myFilterValue"
				        	$tr_counter=1;
				            foreach ( $final_table['body'] as $tr ) {

				            	if ( $tr_counter % 2 == 0 ) { $td_eo = 'even'; } 
				                else { $td_eo = 'odd'; }

				                $table_html .= '<tr data-table-row="Row '.$tr_counter.'" data-table-row-evenodd="'.$td_eo.'">';


				                	$td_counter=1;

				                    foreach ( $tr as $td ) {

				                    	// reset our td value for each iteration
				                    	$td_val = '';

				                    	$prod_id = $td['prod_id'];
				                    	$col_type = $td['col_type'];
				                    	$col_aff_link = $td['col_aff_link'];
				                    	$col_spec_meta = $td['col_spec_meta'];

				                    	//echo $col_aff_link;

				                    	// set an empty data sort value by default
										$data_sort_value = '';



				                    	// set our cell sort value and cell value for the custom star rating field
				                    	if ( $col_type == 'star-rating' || $col_type == 'editor-rating' ) {
				                    		$data_sort_value = ' data-sort-value="'.$td['col_value'].'"';
				                    		$rating = $td['col_value'];
				                    		$galfram_prp_amazon_api = new galfram_productreviewpro_amazon_api();
				                    		$td_val .= $galfram_prp_amazon_api->get_star_rating_html( $rating );
				                    	}
				                    	// set our cell sort value to empty and our cell image for our "image" type
				                    	elseif ( $col_type == 'image' ) {
				                    		$thumb_id = $td['col_value'];
				                    		$thumb_url_array = wp_get_attachment_image_src($thumb_id, 'full', true);
											$thumb_url = $thumb_url_array[0];
											$alt_text = get_post_meta($thumb_id, '_wp_attachment_image_alt', true);
											$td_val .= '<img src="'.$thumb_url.'" alt="'.$alt_text.'" />';
				                    	}
				                    	elseif ( $col_type == 'spec-button' ) {
				                    		$public_this = new galfram_productreviewpro_amazon_api();
											$td_val .= $public_this->galfram_get_spec_button_html( $prod_id, $col_spec_meta );
				                    	}
				                    	elseif ( $col_type == 'spec-shortcode' ) {
											$spec_shortcode = get_post_meta( $prod_id, $col_spec_meta , true);
											$td_val .= do_shortcode( $spec_shortcode );
				                    	}

				                    	elseif ( $col_type == 'price' ) {

				                    		$listprice = get_post_meta($prod_id, $col_spec_meta.'_listprice', true);
				                    		$saleprice = get_post_meta($prod_id, $col_spec_meta.'_saleprice', true);
				                    		$percentagesaved = get_post_meta($prod_id, $col_spec_meta.'_percentagediscounted', true);
				                    		$amountsaved = get_post_meta($prod_id, $col_spec_meta.'_amountdiscounted', true);

											// if data for list price or lowest new price is set to display
											if ( $listprice != '' && $saleprice != '' ) {

												// display the slashed out list price and the lower new price
												$td_val .=  '<span class="slashed-price"><span>'.$listprice.'</span></span>';
												$td_val .=  '<br />';
												$td_val .=  '<span class="deal-price" itemprop="price">'.$saleprice.'</span>';


												if ( $percentagesaved != '' && $amountsaved != '' ) {
													$td_val .=  '<span class="prod-amount-saved">Save '.$percentagesaved.' ('.$amountsaved.')</span>';
												}
												else if ( $percentagesaved != '' && $amountsaved == '' ) {
													$td_val .=  '<span class="prod-amount-saved">Save '.$percentagesaved.'</span>';
												}
												else if ( $percentagesaved == '' && $amountsaved != '' ) {
													$td_val .=  '<span class="prod-amount-saved">Save '.$amountsaved.'</span>';
												}
											}
											// if list price data is set to display, but lowest new price is not
											else if ( $listprice != '' && $saleprice == '' ) {
												// display only the list price
												$td_val .=  '<span class="deal-price" itemprop="price">'.$listprice.'</span>';
											}
											// if list price data is NOT set to display, but lowest new price is
											else if ( $listprice == '' && $saleprice != '' ) {
												// display only the lowest new price
												$td_val .=  '<span class="deal-price" itemprop="price">'.$saleprice.'</span>';
											}

				                    	}

				                    	// set our sort value and cell value for our 12 Amazon api spec types
				                    	elseif ( 
			                    			$col_type == 'amazon_price' ||
			                    			$col_type == 'amazon_features' ||
			                    			$col_type == 'amazon_model' ||
			                    			$col_type == 'amazon_small-image' ||
			                    			$col_type == 'amazon_medium-image' ||
			                    			$col_type == 'amazon_large-image' ||
			                    			$col_type == 'amazon_asin' ||
			                    			$col_type == 'amazon_brand' ||
			                    			$col_type == 'amazon_upc' ||
			                    			$col_type == 'amazon_lowest-new-price' ||
			                    			$col_type == 'amazon_lowest-used-price' ||
			                    			$col_type == 'amazon_warranty'
			                    		) {

											$galfram_ama_asin = get_post_meta($prod_id, 'amazon_manual_product_asin', true);

											// create an empty string for our table cell value
											$td_val = '';

											if ( $col_type == 'amazon_features'  ) {

												$prod_features_array = $final_item_data[$galfram_ama_asin]->ItemAttributes->Feature;

												if ( is_object( $prod_features_array ) ) {
													$td_val .= '<ul class="chosen-api-product-features">';
										    		foreach ( $prod_features_array as $feature ) {
										    			$td_val .= '<li>'.$feature.'</li>';
										    		}
											    	$td_val .= '</ul>';
												}

											}

											elseif ( $col_type == 'amazon_asin' ) {
												$api_val = $final_item_data[$galfram_ama_asin]->ASIN;
												if ( $api_val != '' ) {
													$td_val .= $api_val;
												}
											}

											elseif ( $col_type == 'amazon_brand' ) {
												$api_val = $final_item_data[$galfram_ama_asin]->ItemAttributes->Brand;
												if ( $api_val != '' ) {
													$td_val .= $api_val;
												}
											}

											elseif ( $col_type == 'amazon_upc' ) {
												$api_val = $final_item_data[$galfram_ama_asin]->ItemAttributes->UPC;
												if ( $api_val != '' ) {
													$td_val .= $api_val;
												}
											}

											elseif ( $col_type == 'amazon_warranty' ) {
												$api_val = $final_item_data[$galfram_ama_asin]->ItemAttributes->Warranty;
												if ( $api_val != '' ) {
													$td_val .= $api_val;
												}
											}

											elseif ( $col_type == 'amazon_model' ) {
												$api_val = $final_item_data[$galfram_ama_asin]->ItemAttributes->Model;
												if ( $api_val != '' ) {
													$td_val .= $api_val;
												}
											}

											elseif ( $col_type == 'amazon_small-image' ) {
												$api_val = $final_item_data[$galfram_ama_asin]->ImageSets->ImageSet->SmallImage->URL;
												if ( $api_val ) {
													$td_val .= '<img src="'.$api_val.'" alt="" />';
												}
													
											}
											elseif ( $col_type == 'amazon_medium-image' ) {
												$api_val = $final_item_data[$galfram_ama_asin]->ImageSets->ImageSet->MediumImage->URL;
												if ( $api_val ) {
													$td_val .= '<img src="'.$api_val.'" alt="" />';
												}
											}
											elseif ( $col_type == 'amazon_large-image' ) {
												$api_val = $final_item_data[$galfram_ama_asin]->ImageSets->ImageSet->LargeImage->URL;
												if ( $api_val ) {
													$td_val .= '<img src="'.$api_val.'" alt="" />';
												}
											}

											elseif ( $col_type == 'amazon_lowest-new-price' ) {
												$td_val .= $final_item_data[$galfram_ama_asin]->OfferSummary->LowestNewPrice->FormattedPrice;
												$data_sort_value = ' data-sort-value="'.$final_item_data[$galfram_ama_asin]->OfferSummary->LowestUsedPrice->Amount.'"';
											}

											elseif ( $col_type == 'amazon_lowest-used-price' ) {
												$td_val .= $final_item_data[$galfram_ama_asin]->OfferSummary->LowestUsedPrice->FormattedPrice;
												$data_sort_value = ' data-sort-value="'.$final_item_data[$galfram_ama_asin]->OfferSummary->LowestUsedPrice->Amount.'"';
											}


											elseif ( $col_type == 'amazon_price'  ) {


												if ( $final_item_data[$galfram_ama_asin]->Offers->TotalOffers != 0 ) {
													$td_val .= $final_item_data[$galfram_ama_asin]->Offers->Offer->OfferListing->Price->FormattedPrice;

													$data_sort_value = ' data-sort-value="'.$final_item_data[$galfram_ama_asin]->Offers->Offer->OfferListing->Price->Amount.'"';
												}

												else {
													$td_val .= __( 'N/A', 'ultimateazon2' ) ;
												}

											}
			    
				                    	}
				                    	// set an empty cell sort value and cell value if not a star rating, image, or Amazon api spec
				                    	else {
				                    		$td_val = ''.$td['col_value'];
				                    	}


				                    	if ( $col_aff_link == 'field-link-affiliate' ) {
											$td_val .= '<a class="galfam-table-cell-cover-link gtm-tablecell-coverlink" href="'.get_post_meta($prod_id, 'manual_affiliate_link', true).'"'.$no_follow_html.$new_tab_html.'>';
										}
										elseif ( $col_aff_link == 'field-link-review' ) {
											$permalink = get_the_permalink( $prod_id );
											$td_val .= '<a class="galfam-table-cell-cover-link gtm-tablecell-coverlink" href="'.$permalink.'">';
										}




				                    	$td_styles=' style="';

				                    		// get our column width settings
				                    		$col_width = $td['col_width'];
				                    		$col_width_type = $td['col_width_type'];

					                    	// if our col width setting are set, let's use them
					                    	if ( $col_width != '' && $col_width_type != '' ) {
					                    		$td_styles.= 'min-width: '.$col_width.$col_width_type.';';
					                    	}

					                    	$horz_align = $td['col_horz_align'];
					                    		if ( $horz_align ) $td_styles.='text-align:'.$horz_align.';';

					                    	$vert_align = $td['col_vert_align'];
					                    		if ( $vert_align ) $td_styles.='vertical-align:'.$vert_align.';';

				                    	$td_styles.='"';


				                    	// echo out our actual cell
				                        $table_html .= '<td'.$data_sort_value.$td_styles.' class="galfram_'.$col_type.'" data-table-col="Column '.$td_counter.'">';
				                            $table_html .= $td_val;
				                        $table_html .= '</td>';

				                        $td_counter++;

				                    }

				                $table_html .= '</tr>';

				                $tr_counter++;
				            }

				        $table_html .= '</tbody>';

				    } // end if ( $final_table['body'] )
					
				} // end if ( $specs_group_id != 'pull-from-amazon' ) {





				elseif ( $specs_group_id == 'pull-from-amazon' ) {
						

					//echo '<pre>'.print_r($final_table,1).'</pre>';

			        if ( $final_table['header'] ) {

			            $table_html .= '<thead>';
			                $table_html .= '<tr>';

			                $tbl=0;

			                    foreach ( $final_table['header'] as $th ) {

			                		$breakpoints = '';
			                		$breakpoints_string = '';

			                		$col_hidden_on = $th['col_hidden_on'];
			                		$col_hidden_on = $th['col_hidden_on'];

			                		if ( $col_hidden_on ) {
			                			foreach ( $col_hidden_on as $mq ) {
			                				$breakpoints_string .= $mq.' ';
			                			}
			                		}

			                		//USE data-filterable="false" for columns not suited for filtering like mages

			                		$breakpoints = ' data-breakpoints="'.$breakpoints_string.'"';
			                		$coltype = $th['col_type'];
			             

			                		// amazon spec types
			                		if ( $coltype == 'features'  ) { 
			                			$coltype_string = ' data-value-type="'.$coltype.'" data-type="html"';
			                		}
			                		else if ( $coltype == 'asin' || $coltype == 'brand' || $coltype == 'upc' || $coltype == 'model' || $coltype == 'warranty' || $coltype == 'wp-title' || $coltype == 'amazon-title'  ) { 
			                			$coltype_string = ' data-value-type="'.$coltype.'" data-type="text"';
			                		}
			                		else if ( $coltype == 'price' || $coltype == 'lowest-new-price' || $coltype == 'lowest-used-price'  ) { 
			                			$coltype_string = ' data-value-type="'.$coltype.'" data-type="html"';
			                		}
			                		else if ( $coltype == 'editor-rating' || $coltype == 'small-image' || $coltype == 'medium-image' || $coltype == 'large-image'  ) { 
			                			$coltype_string = ' data-value-type="'.$coltype.'" data-type="html"';
			                		}


			                		if ( $th['col_disable_sort'] == 1 ) {
			                			$disable_sort = ' data-sortable="false"';
			                		}
			                		else {
			                			$disable_sort = '';
			                		}

			                		// set a data type to html if an affiliate link is enabled for this column
			                		//echo $th['col_aff_link'];

			                		if ( $col_aff_link == 'field-link-affiliate' ) {
										$table_html .= '<th'.$breakpoints.$coltype_string.$disable_sort.'>';
									}
									elseif ( $col_aff_link == 'field-link-review' ) {
										$table_html .= '<th'.$breakpoints.$coltype_string.$disable_sort.'>';
									}

			                		$col_link = $th['col_aff_link'];

			                		if ( $col_link == 'field-link-affiliate' || $col_link == 'field-link-review'  ) {
			                			$coltype_string = ' data-value-type="'.$coltype.'" data-type="html"';
			                		}

			                		$table_html .= '<th'.$breakpoints.$coltype_string.$disable_sort.'>';
			                    
			                        	$table_html .= $th['col_name'];

			                   		$table_html .= '</th>';

			                        $tbl++;
			                    }

			                $table_html .= '</tr>';

			            $table_html .= '</thead>';

			        } // end if ( $final_table['header'] ) 




					// finally let's lay out our table!!!
					if ( $final_table['body'] ) {

						// Let's build an array of the product asins to loop through below to make amazon api calls for ten products at a time
						$final_item_data = $galfram_tables_functions->galfram_table_get_asin_groups_data( $final_table, $prod_id, $specs_group_id );

					} //  end if ( $final_table )


					if ( $final_table['body'] ) {

				        $table_html .= '<tbody>';

				        // MAKE SURE TO USE CLEAN FILTER VALUES data-filter-value="myFilterValue"

				        	$tr_counter=1;
				            foreach ( $final_table['body'] as $tr ) {

				            	if ( $tr_counter % 2 == 0 ) { $td_eo = 'even'; } 
				                else { $td_eo = 'odd'; }

				                $table_html .= '<tr data-table-row="Row '.$tr_counter.'" data-table-row-evenodd="'.$td_eo.'">';

				                	$td_counter=1;

				                    foreach ( $tr as $td ) {

				                    	// reset our td value for each iteration
				                    	$td_val = '';

				                    	$prod_id = $td['prod_id'];
				                    	$col_type = $td['col_type'];
				                    	$col_aff_link = $td['col_aff_link'];

										$galfram_ama_asin = get_post_meta($prod_id, 'galfram_chosen_amazon_prod', true);

										// create an empty string for our table cell value
										$td_val = '';

										// set an empty data sort value by default
										$data_sort_value = '';

										
										
										// Add an opening link tag if it is chosen to be displayed for this column
										// if ( $col_aff_link == 1 ) {
										// 	$td_val .= '<a href="'.$final_item_data[$galfram_ama_asin]->DetailPageURL.'"'.$no_follow_html.$new_tab_html.'">';
										// }


										if ( $col_type == 'editor-rating' ) {
				                    		$data_sort_value = ' data-sort-value="'.$td['col_value'].'"';
				                    		$rating = $td['col_value'];
				                    		$galfram_prp_amazon_api = new galfram_productreviewpro_amazon_api();
				                    		$td_val .= $galfram_prp_amazon_api->get_star_rating_html( $rating );
				                    	}

				                    	elseif ( $col_type == 'wp-title'  ) {
				                    		$td_val .= $td['col_value'];
				                    	}


										elseif ( $col_type == 'features'  ) {

											$prod_features_array = $final_item_data[$galfram_ama_asin]->ItemAttributes->Feature;

											if ( is_object( $prod_features_array ) ) {
												$td_val .= '<ul class="chosen-api-product-features">';
									    		foreach ( $prod_features_array as $feature ) {
									    			$td_val .= '<li>'.$feature.'</li>';
									    		}
										    	$td_val .= '</ul>';
											}

										}

										elseif ( $col_type == 'amazon-title' ) {

											$api_val = $final_item_data[$galfram_ama_asin]->ItemAttributes->Title;
											if ( $api_val != '' ) {
												$td_val .= $api_val;
											}
										}

										elseif ( $col_type == 'asin' ) {
											$api_val = $final_item_data[$galfram_ama_asin]->ASIN;
											if ( $api_val != '' ) {
												$td_val .= $api_val;
											}
										}

										elseif ( $col_type == 'brand' ) {
											$api_val = $final_item_data[$galfram_ama_asin]->ItemAttributes->Brand;
											if ( $api_val != '' ) {
												$td_val .= $api_val;
											}
										}

										elseif ( $col_type == 'upc' ) {
											$api_val = $final_item_data[$galfram_ama_asin]->ItemAttributes->UPC;
											if ( $api_val != '' ) {
												$td_val .= $api_val;
											}
										}

										elseif ( $col_type == 'warranty' ) {
											$api_val = $final_item_data[$galfram_ama_asin]->ItemAttributes->Warranty;
											if ( $api_val != '' ) {
												$td_val .= $api_val;
											}
										}

										elseif ( $col_type == 'model' ) {
											$api_val = $final_item_data[$galfram_ama_asin]->ItemAttributes->Model;
											if ( $api_val != '' ) {
												$td_val .= $api_val;
											}
										}

										elseif ( $col_type == 'small-image' ) {
											$api_val = $final_item_data[$galfram_ama_asin]->ImageSets->ImageSet->SmallImage->URL;
											if ( $api_val ) {
												$td_val .= '<img src="'.$api_val.'" alt="" />';
											}
												
										}
										elseif ( $col_type == 'medium-image' ) {
											$api_val = $final_item_data[$galfram_ama_asin]->ImageSets->ImageSet->MediumImage->URL;
											if ( $api_val ) {
												$td_val .= '<img src="'.$api_val.'" alt="" />';
											}
										}
										elseif ( $col_type == 'large-image' ) {
											$api_val = $final_item_data[$galfram_ama_asin]->ImageSets->ImageSet->LargeImage->URL;
											if ( $api_val ) {
												$td_val .= '<img src="'.$api_val.'" alt="" />';
											}
										}

										elseif ( $col_type == 'lowest-new-price' ) {
											$td_val .= $final_item_data[$galfram_ama_asin]->OfferSummary->LowestNewPrice->FormattedPrice;
											$data_sort_value = ' data-sort-value="'.$final_item_data[$galfram_ama_asin]->OfferSummary->LowestUsedPrice->Amount.'"';
										}

										elseif ( $col_type == 'lowest-used-price' ) {
											$td_val .= $final_item_data[$galfram_ama_asin]->OfferSummary->LowestUsedPrice->FormattedPrice;
											$data_sort_value = ' data-sort-value="'.$final_item_data[$galfram_ama_asin]->OfferSummary->LowestUsedPrice->Amount.'"';
										}


										elseif ( $col_type == 'price'  ) {

											//echo '<pre>'.print_r($final_item_data,1).'</pre>';

											$price_fields = $td['price_fields'];

											//echo '<pre>'.print_r($price_fields,1).'</pre>';

											if ( $price_fields['list_price'] == 'on' ) {
												$listprice = $final_item_data[$galfram_ama_asin]->ItemAttributes->ListPrice->FormattedPrice;
												$listprice_num = $final_item_data[$galfram_ama_asin]->ItemAttributes->ListPrice->Amount;
												$data_sort_value_price = $listprice_num;
											}else { $listprice = 'off'; }

											if ( $price_fields['sale_price'] == 'on' ) {
												$saleprice = $final_item_data[$galfram_ama_asin]->Offers->Offer->OfferListing->Price->FormattedPrice;
												$saleprice_num = $final_item_data[$galfram_ama_asin]->Offers->Offer->OfferListing->Price->Amount;
												$data_sort_value_price = $saleprice_num;
											}else { $saleprice = 'off'; }

											if ( $price_fields['per_saved'] == 'on' ) {
												$amountsaved = $final_item_data[$galfram_ama_asin]->Offers->Offer->OfferListing->AmountSaved->FormattedPrice;
											}else { $amountsaved = 'off'; }

											if ( $price_fields['amt_saved'] == 'on' ) {
												$percentagesaved = $final_item_data[$galfram_ama_asin]->Offers->Offer->OfferListing->PercentageSaved;
											}else { $percentagesaved = 'off'; }

											// in case our list price matches the offer price and there really is no deal
											if ( trim($listprice_num) == trim($saleprice_num) ) {
												$saleprice = 'off';
												$amountsaved = 'off';
												$percentagesaved = 'off';
											}


											$td_prices = '<div class="specs-table-row price spec-price" itemprop="offers" itemscope itemtype="http://schema.org/Offer">';


											// if data for list price or lowest new price is set to display
											if ( $listprice != 'off' && $saleprice != 'off' ) {

												// display the slashed out list price and the lower new price
												$td_prices .= '<span class="slashed-price"><span>'.$listprice.'</span></span>';
												$td_prices .= '<br />';
												$td_prices .= '<span class="deal-price" itemprop="price">'.$saleprice.'</span>';

												if ( $percentagesaved != 'off' && $amountsaved != 'off' ) {
													$td_prices .= '<span class="prod-amount-saved">'.__('Save', 'ultimateazon2').' '.$percentagesaved.' ('.$amountsaved.')</span>';
												}
												else if ( $percentagesaved != 'off' && $amountsaved == 'off' ) {
													$td_prices .= '<span class="prod-amount-saved">'.__('Save', 'ultimateazon2').' '.$percentagesaved.'</span>';
												}
												else if ( $percentagesaved == 'off' && $amountsaved != 'off' ) {
													$td_prices .= '<span class="prod-amount-saved">'.__('Save', 'ultimateazon2').' '.$amountsaved.'</span>';
												}

											}

											// if list price data is set to display, but lowest new price is not
											else if ( $listprice != 'off' && $saleprice == 'off' ) {

												// display only the list price
												$td_prices .= '<span class="deal-price" itemprop="price">'.$listprice.'</span>';

											}
											// if list price data is NOT set to display, but lowest new price is
											else if ( $listprice == 'off' && $saleprice != 'off' ) {

												// display only the lowest new price
												$td_prices .= '<span class="deal-price" itemprop="price">'.$saleprice.'</span>';

											}

											$td_prices .= '</div>';



											if ( $listprice != 'off' || $saleprice != 'off' ) {
												$td_val .= $td_prices;
												$data_sort_value = ' data-sort-value="'.$data_sort_value_price.'"';
											}

											else {
												$td_val .= 'N/A';
												$data_sort_value = ' data-sort-value="0"';
											}

										}

										if ( $col_aff_link == 'field-link-affiliate' ) {
											$td_val .= '<a class="galfam-table-cell-cover-link gtm-tablecell-coverlink" href="'.$final_item_data[$galfram_ama_asin]->DetailPageURL.'"'.$no_follow_html.$new_tab_html.'>';
										}
										elseif ( $col_aff_link == 'field-link-review' ) {
											$permalink = get_the_permalink( $prod_id );
											$td_val .= '<a class="galfam-table-cell-cover-link gtm-tablecell-coverlink" href="'.$permalink.'">';
										}

				                    

				                    	$td_styles=' style="';

				                    		// get our column width settings
				                    		$col_width = $td['col_width'];
				                    		$col_width_type = $td['col_width_type'];

					                    	// if our col width setting are set, let's use them
					                    	if ( $col_width != '' && $col_width_type != '' ) {
					                    		$td_styles.= 'min-width: '.$col_width.$col_width_type.';';
					                    	}

					                    	$horz_align = $td['col_horz_align'];
					                    		if ( $horz_align ) { $td_styles.='text-align:'.$horz_align.';';}
					                    		
					                    	$vert_align = $td['col_vert_align'];
					                    		if ( $vert_align ) { $td_styles.='vertical-align:'.$vert_align.';';}

				                    	$td_styles.='"';

				                        $table_html .= '<td'.$data_sort_value.$td_styles.' class="galfram_'.$col_type.'" data-table-col="Column '.$td_counter.'">';
				                            $table_html .= $td_val;
				                        $table_html .= '</td>';

				                        $td_counter++;
				                    }

				                    

				                $table_html .= '</tr>';

				                $tr_counter++;
				            }

				        $table_html .= '</tbody>';


					} // END if ( $final_table['body'] )


				} // END elseif ( $specs_group_id == 'pull-from-amazon' ) {


				// close our table tag and outer container
				if ( $final_table ) {
					$table_html .= '</table>';
					$table_html .= '</div>'; // end .galfram-table-outer
				}


			} // end if ( $table_arr )
				
			

		} //  end if ( $table_type == 'prp-table' ) {








		elseif ( $table_type == 'prp-table-custom' ) {

			// let's get our final table array
			$final_table = get_post_meta( $table_id, 'galfram_prp_table_custom_array', true );

			//echo '<pre>'.print_r($final_table,1).'</pre>';

			// create our table tag and outer container
			if ( $final_table ) {
				$table_html .= '<div class="galfram-table-outer galfram-custom-table" data-table-name="'.get_the_title($table_id).'">';
				$table_html .= '<table class="galfram-table"'.$table_attributes.' data-table-name="">';
			}

			// let's build out table header
	        if ( $final_table['header'] ) {

	            $table_html .= '<thead>';
	                $table_html .= '<tr>';

	                	$tbl=0;

	                    foreach ( $final_table['header'] as $th ) {

	                		$breakpoints = '';
	                		$breakpoints_string = '';

	                		$col_hidden_on = $th['col_hidden_on'];

	                		if ( $col_hidden_on ) {
	                			foreach ( $col_hidden_on as $mq ) { $breakpoints_string .= $mq.' '; }
	                		}

	                		//USE data-filterable="false" for columns not suited for filtering like mages

	                		$breakpoints = ' data-breakpoints="'.$breakpoints_string.'"';
	                		$coltype = $th['col_type'];
	                	
	                		// custom spec types
	                		if ( $coltype == 'wysiwyg' || $coltype == 'plaintext' || $coltype == 'wysiwyg' || $coltype == 'text-link' || $coltype == 'price'  ) { 
	                			$coltype = ' data-value-type="'.$coltype.'" data-type="html"'; 
	                		}
	                		else if ( $coltype == 'image' || $coltype == 'image-link' ) { 
	                			$coltype = ' data-value-type="'.$coltype.'" data-sortable="false" data-type="html"'; 
	                		}
	                		else if ( $coltype == 'number' ) { 
	                			$coltype = ' data-value-type="'.$coltype.'" data-type="number"'; 
	                		}
	                		else { $coltype = ''; }


	                		if ( $th['col_disable_sort'] == 1 ) {
	                			$disable_sort = ' data-value-type="'.$coltype.'" data-sortable="false"';
	                		}
	                		else {
	                			$disable_sort = '';
	                		}

	                		$table_html .= '<th'.$breakpoints.$coltype.$disable_sort.'>';
	                        	$table_html .= $th['col_label'];
	                   		$table_html .= '</th>';

	                        $tbl++;
	                    }

	                $table_html .= '</tr>';
	            $table_html .= '</thead>';
	        }


	        if ( $final_table['body'] ) {

		        $table_html .= '<tbody>';

		        	// MAKE SURE TO USE CLEAN FILTER VALUES data-filter-value="myFilterValue"

		            foreach ( $final_table['body'] as $tr ) {

		                $table_html .= '<tr>';

		                    foreach ( $tr as $td ) {

		                    	// reset our td value for each iteration
		                    	$td_val = '';

		                    	$col_type = $td['col_type'];

		                    	// set an empty data sort value by default
		                    	$data_sort_value = '';

		                    	if ( $col_type == 'text' ) {
		                    		$td_val .= $td['td'];
		                    	}

		                    	else if ( $col_type == 'number' ) {
		                    		$data_sort_value = ' data-sort-value="'.$td['td'].'"';
		                    		$td_val .= $td['td'];
		                    	}

		                    	else if ( $col_type == 'image' ) {
		                    		$thumb_id = $td['td'];
		                    		$thumb_url_array = wp_get_attachment_image_src($thumb_id, 'thumbnail-size', true);
									$thumb_url = $thumb_url_array[0];
									$image_alt = get_post_meta( $thumb_id, '_wp_attachment_image_alt', true);
		                    		$td_val .= '<img src="'.$thumb_url.'" alt="'.$image_alt.'" />';
		                    	}

		                    	else if ( $col_type == 'wysiwyg' ) {
		                    		$td_val .= apply_filters( 'the_content', $td['td'] );
		                    	}

		                    	else if ( $col_type == 'text-link' ) {
		                    		
		                    		$link_url = $td['td']['link_url'];
		                    		$link_text = $td['td']['link_text'];
		                    		if ( $link_text == '' ) {
		                    			$link_text = $link_url;
		                    		}
		                    		$new_tab_check = $td['td']['new_tab'];
		                    		if ( $new_tab_check == '1' ) {
		                    			$new_tab = ' target="_blank"';
		                    		}
		                    		else {
		                    			$new_tab = '';
		                    		}

		                    		$td_val .=  '<a href="'.$link_url.'"'.$new_tab.$no_follow_html.'>'.$link_text.'</a>';
		                    	}

		                    	else if ( $col_type == 'image-link' ) {
		                    		$thumb_id = $td['td']['image'];
		                    		$thumb_url_array = wp_get_attachment_image_src($thumb_id, 'thumbnail-size', true);
									$thumb_url = $thumb_url_array[0];
									$image_alt = get_post_meta( $thumb_id, '_wp_attachment_image_alt', true);
									$link_url = $td['td']['link_url'];
									$new_tab_check = $td['td']['new_tab'];
		                    		if ( $new_tab_check == '1' ) {
		                    			$new_tab = ' target="_blank"';
		                    		}
		                    		else {
		                    			$new_tab = '';
		                    		}
		                    		$td_val .=  '<a href="'.$link_url.'"'.$new_tab.$no_follow_html.'>';
		                    		$td_val .= '<img src="'.$thumb_url.'" alt="'.$image_alt.'" />';
		                    		$td_val .= '</a>';
		                    	}

		                    	else if ( $col_type == 'price' ) {

		                    		$list_price = $td['td']['list_price'];
                                    $sale_price = $td['td']['sale_price'];
                                    $amountsaved = $td['td']['amount_off'];
                                    $percentagesaved = $td['td']['per_off'];

                                    // if data for list price or lowest new price is set to display
									if ( $list_price != '' && $sale_price != '' ) {


										// display the slashed out list price and the lower new price
										$td_val .= '<span class="slashed-price"><span>'.$list_price.'</span></span>';
										$td_val .= '<br />';
										$td_val .= '<span class="deal-price" itemprop="price">'.$sale_price.'</span>';



										if ( $percentagesaved != '' && $amountsaved != '' ) {
											$td_val .= '<span class="prod-amount-saved">'.__('Save', 'ultimateazon2').' '.$percentagesaved.' ('.$amountsaved.')</span>';
										}
										else if ( $percentagesaved != '' && $amountsaved == '' ) {
											$td_val .= '<span class="prod-amount-saved">'.__('Save', 'ultimateazon2').' '.$percentagesaved.'</span>';
										}
										else if ( $percentagesaved == '' && $amountsaved != '' ) {
											$td_val .= '<span class="prod-amount-saved">'.__('Save', 'ultimateazon2').' '.$amountsaved.'</span>';
										}


									}

									// if list price data is set to display, but lowest new price is not
									else if ( $list_price != '' && $sale_price == '' ) {

										// display only the list price
										$td_val .= '<span class="deal-price" itemprop="price">'.$list_price.'</span>';

									}
									// if list price data is NOT set to display, but lowest new price is
									else if ( $list_price == '' && $sale_price != '' ) {

										// display only the lowest new price
										$td_val .= '<span class="deal-price" itemprop="price">'.$sale_price.'</span>';

									}



		                    	}


		                    	else {
		                    		$td_val .= '';
		                    	}

		                    	$td_styles=' style="';

		                    		// get our column width settings
		                    		$col_width = $td['col_width'];
		                    		$col_width_type = $td['col_width_type'];

			                    	// if our col width setting are set, let's use them
			                    	if ( $col_width != '' && $col_width_type != '' ) {
			                    		$td_styles.= 'min-width: '.$col_width.$col_width_type.';';
			                    	}

			                    	$horz_align = $td['col_horz_align'];
			                    		if ( $horz_align ) $td_styles.='text-align:'.$horz_align.';';
			                    		
			                    	$vert_align = $td['col_vert_align'];
			                    		if ( $vert_align ) $td_styles.='vertical-align:'.$vert_align.';';

		                    	$td_styles.='"';

								$table_html .= '<td'.$data_sort_value.$td_styles.' class="galfram-table-td-'.$col_type.'">';
		                            $table_html .= $td_val;
		                        $table_html .= '</td>';

		                    }

		                $table_html .= '</tr>';

		            }

		        $table_html .= '</tbody>';

			}


			// close our table tag and outer container
			if ( $final_table ) {
				$table_html .= '</table>';
				$table_html .= '</div>'; // end .galfram-table-outer
			}

		} // end elseif ( $table_type == 'prp-custom-table' )

		return $table_html;

	}




	/** Add our custom styles to the Stylizer Child Theme
	 *
	 * @since    1.0.0
	 */
	public function galfram_add_custom_ext_css_tables() {


		//     .galfram-table-outer
		//     .galfram-table
		//     .galfram-table thead
		//     .galfram-table thead tr
		//     .galfram-table thead tr th

		//     .galfram-table tbody
		//     .galfram-table tbody tr
		//     .galfram-table tbody tr td



				// if ( get_field('xxx','theme-styles') ) :
				// 	echo 'xxx:'.get_field('xxx','theme-styles').';';
				// else:
				// 	echo 'xxx:xxx;';
				// endif;
		
		
		// table wrapper styles
		if ( get_field('enable_table_wrapper_border','theme-styles') ) :

			echo '.galfram-table-outer{';

				if ( get_field('table_wrapper_border_width','theme-styles') ) :
					echo 'border-width:'.get_field('table_wrapper_border_width','theme-styles').'px;';
					else:
					echo 'border-width:1px;';
					endif;

				if ( get_field('table_wrapper_border_style','theme-styles') ) :
					echo 'border-style:'.get_field('table_wrapper_border_style','theme-styles').';';
					else:
					echo 'border-style:solid;';
					endif;

				if ( get_field('table_wrapper_border_color','theme-styles') ) :
					echo 'border-color:'.get_field('table_wrapper_border_color','theme-styles').';';
					else:
					echo 'border-color:#dedede;';
					endif;

			echo '}';

		endif;


		// table header styles
		if ( get_field('enable_table_header_styles','theme-styles') ) :

			echo '.galfram-table thead tr th{';

				if ( get_field('table_header_bg_color','theme-styles') ) :
					echo 'background-color:'.get_field('table_header_bg_color','theme-styles').';';
					else:
					echo 'background-color:#fff;';
					endif;

				if ( get_field('table_header_color','theme-styles') ) :
					echo 'color:'.get_field('table_header_color','theme-styles').';';
					else:
					echo 'color:#000;';
					endif;

				if ( get_field('table_header_font_size','theme-styles') ) :
					echo 'font-size:'.get_field('table_header_font_size','theme-styles').'px;';
					else:
					echo 'font-size:14px;';
					endif;

				if ( get_field('table_header_line_height','theme-styles') ) :
					echo 'line-height:'.get_field('table_header_line_height','theme-styles').'px;';
					else:
					echo 'line-height:16px;';
					endif;

				if ( get_field('th_padding_top','theme-styles') ) :
					echo 'padding-top:'.get_field('th_padding_top','theme-styles').'px;';
					else:
					echo 'padding-top:10px;';
					endif;

				if ( get_field('th_padding_right','theme-styles') ) :
					echo 'padding-right:'.get_field('th_padding_right','theme-styles').'px!important;';
					else:
					echo 'padding-right:15px;';
					endif;

				if ( get_field('th_padding_bottom','theme-styles') ) :
					echo 'padding-bottom:'.get_field('th_padding_bottom','theme-styles').'px;';
					else:
					echo 'padding-bottom:10px;';
					endif;

				if ( get_field('th_padding_left','theme-styles') ) :
					echo 'padding-left:'.get_field('th_padding_left','theme-styles').'px;';
					else:
					echo 'padding-left:10px;';
					endif;

			echo '}';


			echo '.galfram-table .footable .btn-primary{';
				echo 'background-color:'.get_field('table_header_color','theme-styles').';';
				echo 'border-color:'.get_field('table_header_color','theme-styles').';';
			echo '}';


			// table header border styles
			if ( get_field('enable_table_header_border','theme-styles') ) :

				echo '.galfram-table thead{';

					if ( get_field('table_header_border_width','theme-styles') ) :
						echo 'border-width:'.get_field('table_header_border_width','theme-styles').'px;';
						else:
						echo 'border-width:1px;';
						endif;

					if ( get_field('table_header_border_style','theme-styles') ) :
						echo 'border-style:'.get_field('table_header_border_style','theme-styles').';';
						else:
						echo 'border-style:solid;';
						endif;

					if ( get_field('table_header_border_color','theme-styles') ) :
						echo 'border-color:'.get_field('table_header_border_color','theme-styles').';';
						else:
						echo 'border-color:#dedede;';
						endif;

					$sides = get_field('table_header_border_sides','theme-styles');

					if ( is_array( $sides ) ) : 

						if( !in_array( 'top', $sides ) ) :
							echo 'border-top: none;';
							endif;
						if( !in_array( 'right', $sides ) ) :
							echo 'border-right: none;';
							endif;
						if( !in_array( 'bottom', $sides ) ) :
							echo 'border-bottom: none;';
							endif;
						if( !in_array( 'left', $sides ) ) :
							echo 'border-left: none;';
							endif;

					else:

						if( 'top' != $sides ) :
							echo 'border-top: none;';
							endif;
						if( 'right' != $sides ) :
							echo 'border-right: none;';
							endif;
						if( 'bottom' != $sides ) :
							echo 'border-bottom: none;';
							endif;
						if( 'left' != $sides ) :
							echo 'border-left: none;';
							endif;

					endif;

				echo '}';

			endif;

		endif;



		/**********************/
		// table footer styles
		/**********************/


		// table footer styles
		if ( get_field('enable_table_footer_styles','theme-styles') ) :

				if ( get_field('table_footer_bg_color','theme-styles') ) :
					echo '.galfram-table tfoot, .footable .label-default {';
						echo 'background-color:'.get_field('table_header_bg_color','theme-styles').';';
						echo 'border-color:'.get_field('table_header_bg_color','theme-styles').';';
					echo '}';
				endif;

				if ( get_field('table_footer_text_color','theme-styles') ) :
					echo '.galfram-table tfoot, #content .galfram-table tfoot span, .galfram-table .footable .pagination > li > a {';
						echo 'color:'.get_field('table_footer_text_color','theme-styles').';';
					echo '}';
				endif;

				if ( get_field('table_footer_text_color','theme-styles') ) :
					echo '.footable .pagination > .active > a, .footable .pagination > .active > a:hover {';
						echo 'background-color:'.get_field('table_footer_text_color','theme-styles').';';
						echo 'border-color:'.get_field('table_footer_text_color','theme-styles').';';
					echo '}';
				endif;

		endif;





		/******************/
		// table row styles
		/******************/
		if ( get_field('enable_table_row_styles','theme-styles') ) :

				// table odd row styles
				if ( get_field('odd_row_background_color','theme-styles') ) :
					echo '.galfram-table tbody tr[data-table-row-evenodd="odd"], .galfram-table tbody tr[data-table-row-evenodd="odd"] + .footable-detail-row, .galfram-table tbody tr[data-table-row-evenodd="odd"] + .footable-detail-row tbody {';
						echo 'background-color:'.get_field('odd_row_background_color','theme-styles');
					echo '}';
				endif;

				// table even row styles
				if ( get_field('even_row_background_color','theme-styles') ) :
					echo '.galfram-table tbody tr[data-table-row-evenodd="even"], .galfram-table tbody tr[data-table-row-evenodd="even"] + .footable-detail-row, .galfram-table tbody tr[data-table-row-evenodd="even"] + .footable-detail-row tbody{';
						echo 'background-color:'.get_field('even_row_background_color','theme-styles');
					echo '}';
				endif;

				/*************************/
				// table row bottom border
				/*************************/
				if ( get_field('enable_table_row_bottom_border','theme-styles') ) :
					echo '.galfram-table tbody tr{';
						if ( get_field('table_row_bottom_border_width','theme-styles') ) :
							echo 'border-bottom-width:'.get_field('table_row_bottom_border_width','theme-styles').'px;';
							else:
							echo 'border-bottom-width:1px;';
							endif;
						if ( get_field('table_row_bottom_border_style','theme-styles') ) :
							echo 'border-bottom-style:'.get_field('table_row_bottom_border_style','theme-styles').';';
							else:
							echo 'border-bottom-style:solid;';
							endif;
						if ( get_field('table_row_bottom_border_color','theme-styles') ) :
							echo 'border-bottom-color:'.get_field('table_row_bottom_border_color','theme-styles').';';
							else:
							echo 'border-bottom-color:#dedede;';
							endif;
					echo '}';
					echo '.galfram-table tbody tr:last-child{border-bottom:none;}';
				endif;





				/***********************/
				// table row font styles
				/***********************/
				if ( get_field('enable_table_row_font_styles','theme-styles') ) :

						echo '.galfram-table tbody tr td,.galfram-table tbody tr td.galfram_features li,.galfram-table tbody tr td.galfram_price .deal-price,.galfram-table tbody tr td.galfram_price .slashed-price span,.galfram-table tbody tr td.galfram_price .prod-amount-saved{';
							// table row font styles
							if ( get_field('table_row_global_font_size','theme-styles') ) :
								echo 'font-size:'.get_field('table_row_global_font_size','theme-styles').'px;';
								else:
								echo 'font-size:14px;';
								endif;

							if ( get_field('table_row_global_line_height','theme-styles') ) :
								echo 'line-height:'.get_field('table_row_global_line_height','theme-styles').'px;';
								else:
								echo 'line-height:16px;';
								endif;
						echo '}';






						// table odd row font styles
						if ( get_field('table_row_font_color_odd_rows','theme-styles') ) :
							echo '.galfram-table tbody tr[data-table-row-evenodd="odd"] td,.galfram-table tbody tr[data-table-row-evenodd="odd"] td.galfram_features li,.galfram-table tbody tr[data-table-row-evenodd="odd"] td.galfram_price .deal-price,.galfram-table tbody tr[data-table-row-evenodd="odd"] td.galfram_price .slashed-price span,.galfram-table tbody tr[data-table-row-evenodd="odd"] td.galfram_price .prod-amount-saved{';
								echo 'color:'.get_field('table_row_font_color_odd_rows','theme-styles').';';
							echo '}';

							echo '.galfram-table tbody tr[data-table-row-evenodd="odd"] + .footable-detail-row td{';
								echo 'color:'.get_field('table_row_font_color_odd_rows','theme-styles').';';
							echo '}';

							echo '#content .galfram-table tbody tr[data-table-row-evenodd="odd"] td .footable-toggle{';
								echo 'color:'.get_field('table_row_font_color_odd_rows','theme-styles').';';
							echo '}';

						endif;

						if ( get_field('table_row_link_color_odd_rows','theme-styles') ) :
							echo '.galfram-table tbody tr[data-table-row-evenodd="odd"]) td a:not(.spec-button), .galfram-table tbody tr[data-table-row-evenodd="odd"]) + .footable-detail-row td a:not(.spec-button){';
								echo 'color:'.get_field('table_row_link_color_odd_rows','theme-styles').';';
							echo '}';
						endif;

						if ( get_field('table_row_button_bg_color_odd_rows','theme-styles') || get_field('table_row_button_text_color_odd_rows','theme-styles') ) :
							echo '.galfram-table tbody tr[data-table-row-evenodd="odd"] td a.spec-button, .galfram-table tbody tr[data-table-row-evenodd="odd"]) + .footable-detail-row td a.spec-button{';
								if ( get_field('table_row_button_bg_color_odd_rows','theme-styles') ) :
									echo 'background-color:'.get_field('table_row_button_bg_color_odd_rows','theme-styles').';';
								endif;
								if ( get_field('table_row_button_text_color_odd_rows','theme-styles') ) :
									echo 'color:'.get_field('table_row_button_text_color_odd_rows','theme-styles').';';
								endif;
							echo '}';
						endif;







						// table even row font styles
						if ( get_field('table_row_font_color_even_rows','theme-styles') ) :
							echo '.galfram-table tbody tr[data-table-row-evenodd="even"] td,.galfram-table tbody tr[data-table-row-evenodd="even"] td.galfram_features li,.galfram-table tbody tr[data-table-row-evenodd="even"] td.galfram_price .deal-price,.galfram-table tbody tr[data-table-row-evenodd="even"] td.galfram_price .slashed-price span,.galfram-table tbody tr[data-table-row-evenodd="even"] td.galfram_price .prod-amount-saved{';
								echo 'color:'.get_field('table_row_font_color_even_rows','theme-styles').';';
							echo '}';

							echo '.galfram-table tbody tr[data-table-row-evenodd="even"] + .footable-detail-row td{';
								echo 'color:'.get_field('table_row_font_color_even_rows','theme-styles').';';
							echo '}';

							echo '#content .galfram-table tbody tr[data-table-row-evenodd="even"] td .footable-toggle{';
								echo 'color:'.get_field('table_row_font_color_even_rows','theme-styles').';';
							echo '}';


						endif;

						if ( get_field('table_row_link_color_even_rows','theme-styles') ) :
							echo '.galfram-table tbody tr[data-table-row-evenodd="even"] td a:not(.spec-button), .galfram-table tbody tr[data-table-row-evenodd="even"]) + .footable-detail-row td a:not(.spec-button){';
								echo 'color:'.get_field('table_row_link_color_even_rows','theme-styles').';';
							echo '}';
						endif;

						if ( get_field('table_row_button_bg_color_even_rows','theme-styles') || get_field('table_row_button_text_color_even_rows','theme-styles') ) :
							echo '.galfram-table tbody tr[data-table-row-evenodd="even"] td a.spec-button, .galfram-table tbody tr[data-table-row-evenodd="even"]) + .footable-detail-row td aa.spec-button{';
								if ( get_field('table_row_button_bg_color_even_rows','theme-styles') ) :
									echo 'background-color:'.get_field('table_row_button_bg_color_even_rows','theme-styles').';';
								endif;
								if ( get_field('table_row_button_text_color_even_rows','theme-styles') ) :
									echo 'color:'.get_field('table_row_button_text_color_even_rows','theme-styles').';';
								endif;
							echo '}';
						endif;







						/*************************************/
						// table row font special field styles
						/*************************************/
						if ( get_field('enable_font_styles_for_field_types','theme-styles') ) :




								// ASIN special field styles
								if ( get_field('enable_asin_field_styles','theme-styles') ) :

									echo '.galfram-table tbody tr td.galfram_asin {';
										if ( get_field('asin_text_color','theme-styles') ) :
											echo 'color:'.get_field('asin_text_color','theme-styles').';';
											else:
											echo 'color:#000;';
											endif;

										if ( get_field('asin_font_size','theme-styles') ) :
											echo 'font-size:'.get_field('asin_font_size','theme-styles').'px;';
											else:
											echo 'font-size:14px;';
											endif;

										if ( get_field('asin_line_height','theme-styles') ) :
											echo 'line-height:'.get_field('asin_line_height','theme-styles').'px;';
											else:
											echo 'line-height:16px;';
											endif;
									echo '}';

								endif;




								// Brand special field styles
								if ( get_field('enable_brand_field_styles','theme-styles') ) :

									echo '.galfram-table tbody tr td.galfram_brand {';
										if ( get_field('brand_text_color','theme-styles') ) :
											echo 'color:'.get_field('brand_text_color','theme-styles').';';
											else:
											echo 'color:#000;';
											endif;

										if ( get_field('brand_font_size','theme-styles') ) :
											echo 'font-size:'.get_field('brand_font_size','theme-styles').'px;';
											else:
											echo 'font-size:14px;';
											endif;

										if ( get_field('brand_line_height','theme-styles') ) :
											echo 'line-height:'.get_field('brand_line_height','theme-styles').'px;';
											else:
											echo 'line-height:16px;';
											endif;
									echo '}';

								endif;




								// Model special field styles
								if ( get_field('enable_model_field_styles','theme-styles') ) :

									echo '.galfram-table tbody tr td.galfram_model {';
										if ( get_field('model_text_color','theme-styles') ) :
											echo 'color:'.get_field('model_text_color','theme-styles').';';
											else:
											echo 'color:#000;';
											endif;

										if ( get_field('model_font_size','theme-styles') ) :
											echo 'font-size:'.get_field('model_font_size','theme-styles').'px;';
											else:
											echo 'font-size:14px;';
											endif;

										if ( get_field('model_line_height','theme-styles') ) :
											echo 'line-height:'.get_field('model_line_height','theme-styles').'px;';
											else:
											echo 'line-height:16px;';
											endif;
									echo '}';

								endif;




								// UPC special field styles
								if ( get_field('enable_upc_field_styles','theme-styles') ) :

									echo '.galfram-table tbody tr td.galfram_upc {';
										if ( get_field('upc_text_color','theme-styles') ) :
											echo 'color:'.get_field('upc_text_color','theme-styles').';';
											else:
											echo 'color:#000;';
											endif;

										if ( get_field('upc_font_size','theme-styles') ) :
											echo 'font-size:'.get_field('upc_font_size','theme-styles').'px;';
											else:
											echo 'font-size:14px;';
											endif;

										if ( get_field('upc_line_height','theme-styles') ) :
											echo 'line-height:'.get_field('upc_line_height','theme-styles').'px;';
											else:
											echo 'line-height:16px;';
											endif;
									echo '}';

								endif;




								// Features special field styles
								if ( get_field('enable_features_field_styles','theme-styles') ) :

									echo '.galfram-table tbody tr td.galfram_features li,.galfram-table tbody tr:nth-child(odd) td.galfram_features li,.galfram-table tbody tr:nth-child(even) td.galfram_features li{';
										if ( get_field('features_text_color','theme-styles') ) :
											echo 'color:'.get_field('features_text_color','theme-styles').';';
											else:
											echo 'color:#000;';
											endif;

										if ( get_field('features_font_size','theme-styles') ) :
											echo 'font-size:'.get_field('features_font_size','theme-styles').'px;';
											else:
											echo 'font-size:14px;';
											endif;

										if ( get_field('features_line_height','theme-styles') ) :
											echo 'line-height:'.get_field('features_line_height','theme-styles').'px;';
											else:
											echo 'line-height:16px;';
											endif;
									echo '}';

								endif;




								// Warranty special field styles
								if ( get_field('enable_warranty_field_styles','theme-styles') ) :

									echo '.galfram-table tbody tr td.galfram_warranty {';
										if ( get_field('warranty_text_color','theme-styles') ) :
											echo 'color:'.get_field('warranty_text_color','theme-styles').';';
											else:
											echo 'color:#000;';
											endif;

										if ( get_field('warranty_font_size','theme-styles') ) :
											echo 'font-size:'.get_field('warranty_font_size','theme-styles').'px;';
											else:
											echo 'font-size:14px;';
											endif;

										if ( get_field('warranty_line_height','theme-styles') ) :
											echo 'line-height:'.get_field('warranty_line_height','theme-styles').'px;';
											else:
											echo 'line-height:16px;';
											endif;
									echo '}';

								endif;




								// Star Rating special field styles
								if ( get_field('enable_star_rating_field_styles','theme-styles') ) :

									echo '.galfram-table tbody tr td.galfram_asin {';
										
									echo '}';

								endif;




								// Yes/No special field styles
								if ( get_field('enable_yesno_rating_field_styles','theme-styles') ) :

									echo '.galfram-table tbody tr td.galfram_yesno {';
										
									echo '}';

								endif;




								// Price special field styles
								if ( get_field('enable_price_field_styles','theme-styles') ) :

									if ( get_field('sale_price_font_size','theme-styles') || get_field('sale_price_color','theme-styles') ) :

										echo '.galfram-table tbody tr:nth-child(odd) td.galfram_price .deal-price,.galfram-table tbody tr:nth-child(even) td.galfram_price .deal-price{';

											if ( get_field('sale_price_font_size','theme-styles') ) :
												echo 'font-size:'.get_field('sale_price_font_size','theme-styles').'px;';
												else:
												echo 'font-size:18px;';
												endif;

											if ( get_field('sale_price_color','theme-styles') ) :
												echo 'color:'.get_field('sale_price_color','theme-styles').';';
												else:
												echo 'color:#000;';
												endif;

										echo '}';

									endif;

									if ( get_field('slashed_price_font_size','theme-styles') || get_field('slashed_price_color','theme-styles') ) :

										echo '.galfram-table tbody tr:nth-child(odd) td.galfram_price .slashed-price span,.galfram-table tbody tr:nth-child(even) td.galfram_price .slashed-price span{';
									
											if ( get_field('slashed_price_font_size','theme-styles') ) :
												echo 'font-size:'.get_field('slashed_price_font_size','theme-styles').'px;';
												else:
												echo 'font-size:18px;';
												endif;

											if ( get_field('slashed_price_color','theme-styles') ) :
												echo 'color:'.get_field('slashed_price_color','theme-styles').';';
												else:
												echo 'color:#000;';
												endif;

										echo '}';

									endif;

									if ( get_field('amountpercentage_saved_font_size','theme-styles') || get_field('amountpercentage_saved_color','theme-styles') ) :

										echo '.galfram-table tbody tr:nth-child(odd) td.galfram_price .prod-amount-saved,.galfram-table tbody tr:nth-child(even) td.galfram_price .prod-amount-saved{';
									
											if ( get_field('amountpercentage_saved_font_size','theme-styles') ) :
												echo 'font-size:'.get_field('amountpercentage_saved_font_size','theme-styles').'px;';
												else:
												echo 'font-size:18px;';
												endif;

											if ( get_field('amountpercentage_saved_color','theme-styles') ) :
												echo 'color:'.get_field('amountpercentage_saved_color','theme-styles').';';
												else:
												echo 'color:#000;';
												endif;

										echo '}';

									endif;


								endif;




								// Lowest New Price special field styles
								if ( get_field('enable_lowest_new_price_field_styles','theme-styles') ) :

									echo '.galfram-table tbody tr td.galfram_lowest-new-price {';
										if ( get_field('lowest_new_price_text_color','theme-styles') ) :
											echo 'color:'.get_field('lowest_new_price_text_color','theme-styles').';';
											else:
											echo 'color:#000;';
											endif;

										if ( get_field('lowest_new_price_font_size','theme-styles') ) :
											echo 'font-size:'.get_field('lowest_new_price_font_size','theme-styles').'px;';
											else:
											echo 'font-size:14px;';
											endif;

										if ( get_field('lowest_new_price_line_height','theme-styles') ) :
											echo 'line-height:'.get_field('lowest_new_price_line_height','theme-styles').'px;';
											else:
											echo 'line-height:16px;';
											endif;
									echo '}';

								endif;




								// Lowest Used Price special field styles
								if ( get_field('enable_lowest_used_price_field_styles','theme-styles') ) :

									echo '.galfram-table tbody tr td.galfram_lowest-used-price {';
										if ( get_field('lowest_used_price_text_color','theme-styles') ) :
											echo 'color:'.get_field('lowest_used_price_text_color','theme-styles').';';
											else:
											echo 'color:#000;';
											endif;

										if ( get_field('lowest_used_price_font_size','theme-styles') ) :
											echo 'font-size:'.get_field('lowest_used_price_font_size','theme-styles').'px;';
											else:
											echo 'font-size:14px;';
											endif;

										if ( get_field('lowest_used_price_line_height','theme-styles') ) :
											echo 'line-height:'.get_field('lowest_used_price_line_height','theme-styles').'px;';
											else:
											echo 'line-height:16px;';
											endif;
									echo '}';

								endif;






						endif; // END if ( get_field('enable_font_styles_for_field_types','theme-styles') )

				endif; // end if ( get_field('enable_table_row_font_styles','theme-styles') )

		endif; // END if ( get_field('enable_table_row_styles','theme-styles') )


		



	}




}
