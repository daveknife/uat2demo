<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       https://incomegalaxy.com
 * @since      1.0.0
 *
 * @package    galfram_Tables
 * @subpackage galfram_Tables/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    galfram_Tables
 * @subpackage galfram_Tables/public
 * @author     Your Name <email@example.com>
 */
class galfram_tables_functions {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */



	/** Set our table attributes
	 *
	 * @since    1.0.0
	 */
	public function get_galfram_table_attributes( $post_id ) {


		$table_attributes = '';
		$override = get_post_meta( $post_id, 'overide_global_table_settings', true );

		if ( $override ) {

			//enable state saving
			$enable_table_memory = get_post_meta( $post_id, 'enable_table_memory', true );
			if ( $enable_table_memory ) { $table_attributes .= ' data-state="true"'; }

			// look into "key" for persistent state on different pages

			// enable hide the header row
			$hide_header = get_post_meta( $post_id, 'hide_the_table_header', true );
			if ( $hide_header ) { $table_attributes .= ' data-show-header="false"'; }

			//enable table sorting
			$enable_table_sorting = get_post_meta( $post_id, 'enable_table_sorting', true );
			if ( $enable_table_sorting ) { $table_attributes .= ' data-sorting="true"'; }

			//enable filtering
			$enable_table_filtering = get_post_meta( $post_id, 'enable_table_filtering', true );
			if ( $enable_table_filtering ) { 

				$table_attributes .= ' data-filtering="true"'; 

				// add a no results message when filtering returns 0 results
				$no_results_message = get_post_meta( $post_id, 'no_results_message', true );
				if ( $no_results_message ) { $table_attributes .= ' data-empty="'.$no_results_message.'"'; }


				// ad custom filter input placeholder text
				$filter_placeholder_text = get_post_meta( $post_id, 'filter_placeholder_text', true );
				if ( $filter_placeholder_text ) { $table_attributes .= ' data-filter-placeholder="'.$filter_placeholder_text.'"'; }


				// add a title to the filter columns dropdown
				$filter_dropdown_heading = get_post_meta( $post_id, 'filter_dropdown_heading', true );
				if ( $filter_dropdown_heading ) { $table_attributes .= ' data-filter-dropdown-title="'.$filter_dropdown_heading.'"'; }

			}


			//enable pagination
			$enable_table_pagination = get_post_meta( $post_id, 'enable_table_pagination', true );
			if ( $enable_table_pagination ) { 

				$table_attributes .= ' data-paging="true"'; 

				$number_of_rows_per_page = get_post_meta( $post_id, 'number_of_rows_per_page', true );
				if ( $number_of_rows_per_page ) { 
					$table_attributes .= ' data-paging-size="'.$number_of_rows_per_page.'"'; 
				}

				//enable pagination limit to pages shown in pagination
				$pages_displayed_limit = get_post_meta( $post_id, 'pages_displayed_limit', true );
				if ( $pages_displayed_limit ) { $table_attributes .= ' data-paging-limit="'.$pages_displayed_limit.'"'; }

				// set the pagination positioning
				$pagination_position = get_post_meta( $post_id, 'pagination_position', true );
				if ( $pagination_position ) { $table_attributes .= ' data-paging-position="'.$pagination_position.'"'; }

			}


			//enable breakpoints to run off parent container width
			$breakpoints_off_container = get_post_meta( $post_id, 'breakpoints_off_container', true );
			if ( $breakpoints_off_container ) { $table_attributes .= ' data-use-parent-width="true"'; }


			// use custom breakpoints
			$custom_breakpoints = get_post_meta( $post_id, 'custom_breakpoints', true );

			if ( $custom_breakpoints ) {

				// set defaults as a fallback
				$default_breakpoints = array(
					'xs' => '480',
					'sm' => '768',
					'md' => '992',
					'lg' => '1200',
					'xl' => (int) '1400'
				);

				// set our user chosen values if they exist and don't equal 0
				$xs = get_post_meta( $post_id, 'extra_small_breakpoint', true );
				$sm = get_post_meta( $post_id, 'small_breakpoint', true );
				$md = get_post_meta( $post_id, 'medium_breakpoint', true );
				$lg = get_post_meta( $post_id, 'large_breakpoint', true );

				if ( $xs && $xs != '0' ) { $default_breakpoints['xs'] = (int) $xs; }
				if ( $sm && $sm != '0' ) { $default_breakpoints['sm'] = (int) $sm; }
				if ( $md && $md != '0' ) { $default_breakpoints['md'] = (int) $md; }
				if ( $lg && $lg != '0' ) { $default_breakpoints['lg'] = (int) $lg; }

				$encoded = json_encode( $default_breakpoints );
				$table_attributes .= " data-breakpoints='".htmlspecialchars($encoded)."'";

			}


			// display the first row open by default
			$expand_the_first_row_by_default = get_post_meta( $post_id, 'expand_the_first_row_by_default', true );
			if ( $expand_the_first_row_by_default ) { $table_attributes .= ' data-expand-first="true"'; }


			// display the first row open by default
			$expand_all_rows_by_default = get_post_meta( $post_id, 'expand_all_rows_by_default', true );
			if ( $expand_all_rows_by_default ) { $table_attributes .= ' data-expand-all="true"'; }

			// hide the row toggle button
			$hide_the_toggle_button = get_post_meta( $post_id, 'hide_the_toggle_button', true );
			if ( $hide_the_toggle_button ) { $table_attributes .= ' data-show-toggle="false"'; }
			

			// hide the row toggle button
			$filter_position = get_post_meta( $post_id, 'filter_position', true );
			if ( $filter_position ) { $table_attributes .= ' data-filter-position="'.$filter_position.'"'; }

		} // end if $override

		else { // else use global settings


			//enable state saving
			$enable_table_memory = get_option( 'extension-tbls-settings_enable_table_memory' );
			if ( $enable_table_memory ) { $table_attributes .= ' data-state="true"'; }

			// look into "key" for persistent state on different pages


			// enable hide the header row
			$hide_header = get_option( 'extension-tbls-settings_hide_the_table_header' );
			if ( $hide_header ) { $table_attributes .= ' data-show-header="false"'; }

			//enable table sorting
			$enable_table_sorting = get_option( 'extension-tbls-settings_enable_table_sorting' );
			if ( $enable_table_sorting ) { $table_attributes .= ' data-sorting="true"'; }

			//echo '::: '.$enable_table_sorting;

			//enable filtering
			$enable_table_filtering = get_option( 'extension-tbls-settings_enable_table_filtering' );
			if ( $enable_table_filtering ) { 

				$table_attributes .= ' data-filtering="true"'; 

				// add a no results message when filtering returns 0 results
				$no_results_message = get_option( 'extension-tbls-settings_no_results_message' );
				if ( $no_results_message ) { $table_attributes .= ' data-empty="'.$no_results_message.'"'; }


				// ad custom filter input placeholder text
				$filter_placeholder_text = get_option( 'extension-tbls-settings_filter_placeholder_text' );
				if ( $filter_placeholder_text ) { $table_attributes .= ' data-filter-placeholder="'.$filter_placeholder_text.'"'; }


				// add a title to the filter columns dropdown
				$filter_dropdown_heading = get_option( 'extension-tbls-settings_filter_dropdown_heading' );
				if ( $filter_dropdown_heading ) { $table_attributes .= ' data-filter-dropdown-title="'.$filter_dropdown_heading.'"'; }

			}


			//enable pagination
			$enable_table_pagination = get_option( 'extension-tbls-settings_enable_table_pagination' );
			if ( $enable_table_pagination ) { 

				$table_attributes .= ' data-paging="true"'; 

				$number_of_rows_per_page = get_option( 'extension-tbls-settings_number_of_rows_per_page' );
				if ( $number_of_rows_per_page ) { 
					$table_attributes .= ' data-paging-size="'.$number_of_rows_per_page.'"'; 
				}

				//enable pagination limit to pages shown in pagination
				$pages_displayed_limit = get_option( 'extension-tbls-settings_pages_displayed_limit' );
				if ( $pages_displayed_limit ) { $table_attributes .= ' data-paging-limit="'.$pages_displayed_limit.'"'; }

				// set the pagination positioning
				$pagination_position = get_option( 'extension-tbls-settings_pagination_position' );
				if ( $pagination_position ) { $table_attributes .= ' data-paging-position="'.$pagination_position.'"'; }

			}


			//enable breakpoints to run off parent container width
			$breakpoints_off_container = get_option( 'extension-tbls-settings_breakpoints_off_container' );
			if ( $breakpoints_off_container ) { $table_attributes .= ' data-use-parent-width="true"'; }


			// use custom breakpoints
			$custom_breakpoints = get_option( 'extension-tbls-settings_custom_breakpoints' );

			if ( $custom_breakpoints ) {

				// set defaults as a fallback
				$default_breakpoints = array(
					'xs' => '480',
					'sm' => '768',
					'md' => '992',
					'lg' => '1200',
					'xl' => (int) '1400'
				);

				// set our user chosen values if they exist and don't equal 0
				$xs = get_option( 'extension-tbls-settings_extra_small_breakpoint' );
				$sm = get_option( 'extension-tbls-settings_small_breakpoint' );
				$md = get_option( 'extension-tbls-settings_medium_breakpoint' );
				$lg = get_option( 'extension-tbls-settings_large_breakpoint' );

				if ( $xs && $xs != '0' ) { $default_breakpoints['xs'] = (int) $xs; }
				if ( $sm && $sm != '0' ) { $default_breakpoints['sm'] = (int) $sm; }
				if ( $md && $md != '0' ) { $default_breakpoints['md'] = (int) $md; }
				if ( $lg && $lg != '0' ) { $default_breakpoints['lg'] = (int) $lg; }

				$encoded = json_encode( $default_breakpoints );
				$table_attributes .= " data-breakpoints='".htmlspecialchars($encoded)."'";

			}


			// display the first row open by default
			$expand_the_first_row_by_default = get_option( 'extension-tbls-settings_expand_the_first_row_by_default' );
			if ( $expand_the_first_row_by_default ) { $table_attributes .= ' data-expand-first="true"'; }


			// display the first row open by default
			$expand_all_rows_by_default = get_option( 'extension-tbls-settings_expand_all_rows_by_default' );
			if ( $expand_all_rows_by_default ) { $table_attributes .= ' data-expand-all="true"'; }

			// hide the row toggle button
			$hide_the_toggle_button = get_option( 'extension-tbls-settings_hide_the_toggle_button' );
			if ( $hide_the_toggle_button ) { $table_attributes .= ' data-show-toggle="false"'; }
			

			// hide the row toggle button
			$filter_position = get_option( 'extension-tbls-settings_filter_position' );
			if ( $filter_position ) { $table_attributes .= ' data-filter-position="'.$filter_position.'"'; }

		}

		return $table_attributes;


	}




	/** Build our table header four our product review pro tables
	 *
	 * @since    1.0.0
	 */
	public function galfram_table_build_final_table_header( $specs_group_id, $post_id ) {

		// get our list of all our spec groups
		$user_specs_groups = get_option('galfram_ext_prp_all_userspecs_groups');

		// create an empty array to save our chosen spec group into
		$current_chosen_spec_group = array();

		$final_table = array();


		if ( $specs_group_id != 'pull-from-amazon' ) {

			// loop through all spec grooups and only get the one we want for this table post
			foreach ( $user_specs_groups as $user_spec_group ) {
				if ( $user_spec_group['field_57c35f7e8031b'] == $specs_group_id ) {
					$current_chosen_spec_group = $user_spec_group;
				}
			}

			// isolate just our actual specs
			$specs_arr = $current_chosen_spec_group['field_57c06aa9622b9'];

			$columns = get_post_meta( $post_id, 'galfram_choose_columns', true );

			if( $columns ) {
				for( $i = 0; $i < $columns; $i++ ) {
					$spec_cols[$i]['spec_id'] =  get_post_meta( $post_id, 'galfram_choose_columns_' . $i . '_column', true );
					$spec_cols[$i]['spec_hide_cols'] =  get_post_meta( $post_id, 'galfram_choose_columns_' . $i . '_responsive_settings', true );
					$spec_cols[$i]['spec_disable_sort'] =  get_post_meta( $post_id, 'galfram_choose_columns_' . $i . '_disable_sorting', true );
					$spec_cols[$i]['spec_col_width'] =  get_post_meta( $post_id, 'galfram_choose_columns_' . $i . '_column_width', true );
					$spec_cols[$i]['spec_col_width_type'] =  get_post_meta( $post_id, 'galfram_choose_columns_' . $i . '_column_width_type', true );
					$spec_cols[$i]['spec_col_aff_link'] = get_post_meta( $post_id, 'galfram_choose_columns_' . $i . '_field_link', true );
					$spec_cols[$i]['spec_col_horz_align'] =  get_post_meta( $post_id, 'galfram_choose_columns_' . $i . '_column_horz_align', true );
					$spec_cols[$i]['spec_col_vert_align'] =  get_post_meta( $post_id, 'galfram_choose_columns_' . $i . '_column_vert_align', true );

				}


				$j=0;

				$final_table['amazon_specs'] = false;

				// loop through each of our user chosen spec columns


				foreach ( $spec_cols as $col ) {

					// echo '<pre>'.print_r($col,1).'</pre>';
					// die();

					// get spec id of user chosen column
					$col_spec_id = $col['spec_id'];

					// get columns we'll be hiding responsively
					$col_spec_hide_cols = $col['spec_hide_cols'];

					// get columns disable sortable value
					$col_disable_sort = $col['spec_disable_sort'];

					// get the column width
					$col_width = $col['spec_col_width'];

					// get the column width unit
					$col_width_type = $col['spec_col_width_type'];

					// check if this column data should link to affiliate link
					$col_aff_link = $col['spec_col_aff_link'];

					// get the column horizontal alignment
					$col_horz_align = $col['spec_col_horz_align'];

					// get the column vertical alignment
					$col_vert_align = $col['spec_col_vert_align'];

					// loop through our all specs array form the user chosen specs set
					foreach ( $specs_arr as $spec ) {

						// get the spec id of each one for comparison below
						$spec_id = $spec['field_57c6bfb62908a'];

						// if the chosen spec id matches the id in the all specs array
						if ( $spec_id == $col_spec_id ) {
							// save the spec id to our final table array
							$final_table['header'][$j]['col_id'] = $spec['field_57c6bfb62908a'];
							// save the spec name to our final table array
							$final_table['header'][$j]['col_name'] = $spec['field_57c06afc622ba'];
							// save the spec type to our final table array
							if ( $spec['field_57c06b19622bb'] == 'amazon' ) { // if an amazon spec, add the amazon spec type
								// if an amazon spec is present, set our flag to true
								if ( $final_table['amazon_specs'] == false ) {
									$final_table['amazon_specs'] = true;
								}
								$final_table['header'][$j]['col_type'] = $spec['field_57c06b19622bb'].'_'.$spec['field_57c7052ff242d'];
							}
							else {
								$final_table['header'][$j]['col_type'] = $spec['field_57c06b19622bb'];
							}

							$final_table['header'][$j]['col_hidden_on'] = $col_spec_hide_cols;
							$final_table['header'][$j]['col_disable_sort'] = $col_disable_sort;
							$final_table['header'][$j]['col_width'] = $col_width;
							$final_table['header'][$j]['col_width_type'] = $col_width_type;
							$final_table['header'][$j]['col_aff_link'] = $col_aff_link;
							$final_table['header'][$j]['col_horz_align'] = $col_horz_align;
							$final_table['header'][$j]['col_vert_align'] = $col_vert_align;


							$j++;

						}
					
					}

				}

			} //  END if( $columns ) {


		}


		elseif ( $specs_group_id == 'pull-from-amazon' ) {

			// get our user chosen columns
			$columns = get_post_meta( $post_id, 'galfram_choose_columns', true );

			$j=0;

			// If the columns have been set
			if( $columns ) {

				for( $j = 0; $j < $columns; $j++ ) {

					// get the amazon column ID, it's the same as the column type
					$col_id = get_post_meta( $post_id, 'galfram_choose_columns_' . $j . '_column', true );

					// set up ourcolumn headervnames, since they need to be converted form the col type
					if ( $col_id == 'wp-title' ) {
						$clean_name = __('Title', 'ultimateazon2');
					}
					elseif ( $col_id == 'amazon-title' ) {
						$clean_name = __('Product Title', 'ultimateazon2');
					}
					elseif ( $col_id == 'editor-rating' ) {
						$clean_name = __('Editor\'s Rating', 'ultimateazon2');
					}
					elseif ( $col_id == 'asin' ) {
						$clean_name = __('ASIN', 'ultimateazon2');
					}
					elseif ( $col_id == 'brand' ) {
						$clean_name = __('Brand', 'ultimateazon2');
					}
					elseif ( $col_id == 'model' ) {
						$clean_name = __('Model', 'ultimateazon2');
					}
					elseif ( $col_id == 'upc' ) {
						$clean_name = __('UPC', 'ultimateazon2');
					}
					elseif ( $col_id == 'features' ) {
						$clean_name = __('Features', 'ultimateazon2');
					}
					elseif ( $col_id == 'warranty' ) {
						$clean_name = __('Warranty', 'ultimateazon2');
					}
					elseif ( $col_id == 'price' ) {
						$clean_name = __('Price', 'ultimateazon2');
					}
					elseif ( $col_id == 'lowest-new-price' ) {
						$clean_name = __('Lowest New Price', 'ultimateazon2');
					}
					elseif ( $col_id == 'lowest-used-price' ) {
						$clean_name = __('Lowest Used Price', 'ultimateazon2');
					}
					elseif ( $col_id == 'small-image' ) {
						$clean_name = __('Image', 'ultimateazon2');
					}
					elseif ( $col_id == 'medium-image' ) {
						$clean_name = __('Image', 'ultimateazon2');
					}
					elseif ( $col_id == 'large-image' ) {
						$clean_name = __('Image', 'ultimateazon2');
					}

					else {
						$clean_name = '';
					}

					// save our columns data into our final table array for layout
					$final_table['header'][$j]['col_id'] = $col_id;
					$final_table['header'][$j]['col_name'] = $clean_name;
					$final_table['header'][$j]['col_type'] = $col_id;
					$final_table['header'][$j]['col_hidden_on'] = get_post_meta( $post_id, 'galfram_choose_columns_' . $j . '_responsive_settings',true );
					$final_table['header'][$j]['col_disable_sort'] = get_post_meta( $post_id, 'galfram_choose_columns_' . $j . '_disable_sorting', true );
					$final_table['header'][$j]['col_width'] = get_post_meta( $post_id, 'galfram_choose_columns_' . $j . '_column_width', true );
					$final_table['header'][$j]['col_width_type'] = get_post_meta( $post_id, 'galfram_choose_columns_' . $j . '_column_width_type', true );
					$final_table['header'][$j]['col_aff_link'] = get_post_meta( $post_id, 'galfram_choose_columns_' . $j . '_field_link', true );
					$final_table['header'][$j]['col_horz_align'] = get_post_meta( $post_id, 'galfram_choose_columns_' . $j . '_column_horz_align', true );
					$final_table['header'][$j]['col_vert_align'] = get_post_meta( $post_id, 'galfram_choose_columns_' . $j . '_column_vert_align', true );

				}

			}

		}

		if ($final_table) {
			return $final_table;
		}
		else {
			return;
		}


	}





	/** Build our table body
	 *
	 * @since    1.0.0
	 */
	public function galfram_table_build_final_table_body_rows( $specs_group_id, $post_id, $final_table ) {

		// get our user chosen products for this table
		$reviews = get_post_meta( $post_id, 'add_products', true );
		// grab our table header sub-array
		$header_cols = $final_table['header'];

		$table_body = array();

		//echo '<pre>'.print_r($header_cols,1).'</pre>';

		if( $reviews && $header_cols ) {

			for( $i = 0; $i < $reviews; $i++ ) {

				$m = 0;
				$prod_id = get_post_meta( $post_id, 'add_products_' . $i . '_galfram_table_row_prod', true );

				//echo '<pre>'.print_r($header_cols,1).'</pre>'; die();

				foreach ( $header_cols as $col ) {

					$col_id =  $specs_group_id.'_'.$col['col_id'];

					//echo '<pre>'.print_r($header_cols,1).'</pre>';

					if ( $specs_group_id != 'pull-from-amazon' ) {
						$final_table['body'][$i][$m]['col_value'] = esc_html( get_post_meta( $prod_id, $col_id, true ) );
					}
					else {
						// add our editor rating here to save time on the front
						if ( $col['col_type'] == 'editor-rating' ) {
							$final_table['body'][$i][$m]['col_value'] = get_post_meta( $prod_id, 'editor_rating', true );
						}
						// add our WP Post titles here to save time on the front
						else if ( $col['col_type'] == 'wp-title' ) {
							$final_table['body'][$i][$m]['col_value'] = get_the_title( $prod_id );
						}
						else {
							$final_table['body'][$i][$m]['col_value'] = '';
						}
					}

					$final_table['body'][$i][$m]['col_spec_meta'] = $col_id;
					$final_table['body'][$i][$m]['col_type'] = $col['col_type'];
					$final_table['body'][$i][$m]['prod_id'] = $prod_id;
					$final_table['body'][$i][$m]['col_width'] = $col['col_width'];
					$final_table['body'][$i][$m]['col_width_type'] = $col['col_width_type'];
					$final_table['body'][$i][$m]['col_aff_link'] = $col['col_aff_link'];
					$final_table['body'][$i][$m]['col_horz_align'] = $col['col_horz_align'];
					$final_table['body'][$i][$m]['col_vert_align'] = $col['col_vert_align'];

					if ( $col['col_type'] == 'price' ) {

						$price_display_fields = get_post_meta( $prod_id, 'galfram_chosen_amazon_prod_specs_display', true );

						//echo '<pre>'.print_r($price_display_fields,1).'</pre>';
						//die();

						if ( is_array($price_display_fields) && in_array( 'list-price', $price_display_fields ) ) {
							$list_price = 'on';
						} else { $list_price = 'off'; }

						if ( is_array($price_display_fields) && in_array( 'price-offer', $price_display_fields ) ) {
							$sale_price = 'on';
						} else { $sale_price = 'off'; }

						if ( is_array($price_display_fields) && in_array( 'amount-saved', $price_display_fields ) ) {
							$amt_off = 'on';
						} else { $amt_off = 'off'; }

						$final_table['body'][$i][$m]['price_fields']['list_price'] = $list_price;
						$final_table['body'][$i][$m]['price_fields']['sale_price'] = $sale_price;
						$final_table['body'][$i][$m]['price_fields']['per_saved'] = $amt_off;
						$final_table['body'][$i][$m]['price_fields']['amt_saved'] = $amt_off;
					}

					$m++;
				}

			}

			$table_body['final_table'] = $final_table;
			$table_body['prod_id'] = $prod_id;

		}

		return $table_body;

	}



	/** Build our product data in groups of 10 asins at a time
	 *
	 * @since    1.0.0
	 */
	public function galfram_table_get_asin_groups_data( $final_table, $prod_id, $specs_group_id ) {

		// create a new instance of our class
		$galfram_prp_amazon_api = new galfram_productreviewpro_amazon_api();

		// set our empty table_asins_groups array
		$table_asins_groups = array();

		// this is the array not broken into ten asins, that we will loop through below to create our final amazon data array to pull into the table below
		$table_asins_full_group = array();

		$i=0;
		$z = 0;
		// loop through our table rows and save the asins into groups of 10
		foreach ( $final_table['body'] as $tr ) {

			if ( $specs_group_id == 'pull-from-amazon' ) {
				$asin = get_post_meta( $tr[0]['prod_id'] , 'galfram_chosen_amazon_prod', true);
			}
			else {
				$asin = get_post_meta( $tr[0]['prod_id'] , 'amazon_manual_product_asin', true);
			}

			$table_asins_full_group[$i] = $asin;

			$table_asins_groups[$z][$i]['prod_id'] = $tr[0]['prod_id'];
			$table_asins_groups[$z][$i]['asin'] = $asin;
			$i++;

			if ($i % 10 == 0 ) {
				$z++;
			}
		}

		$x=0;

		// let's change each group of ten into a string and make our api calls for ten items at a time
		foreach ( $table_asins_groups as $asin_group ) {
			// create our empty asins string
			$asin_string = '';
			// add our asins to the string
			foreach ( $asin_group as $asin) {
				$asin_string .= $asin['asin'].',';
			}
			// make our api call to get the product data for the ten asins
			$api_response = $galfram_prp_amazon_api->amazon_api_item_lookup( $prod_id, 'Images,ItemAttributes,Offers', $asin_string );
			// save our products data into the $all_products_info_arr
			$all_products_info_arr[$x] = simplexml_load_string($api_response);
			$x++;

		}

		// narrow down our all prodyucts data array to just the "item" array form our xml response
		$new_products_info_arr = array();
		$w=0;
		$x=0;
		foreach ( $all_products_info_arr as $arr ) {
			foreach($arr->Items->Item as $item) {
				$items_array[$x] = $item;
				$x++;
			}
			$w++;
		}

		// let's now build our final array pf product data, organized ny ASIN to look up quickly later
		$final_item_data = array();
		// loop through all of our items
		foreach($items_array as $item) {
			$asin = $item->ASIN;
			// add each product's data to the final array, with the asin for the key for easy lookup later
			$final_item_data[''.$asin.''] = $item;
		}

		return $final_item_data;

	}




	/** Build our final table array for user generated custom tables
	 *
	 * @since    1.0.0
	 */
	public function galfram_table_custom_build( $post_id ) {

		// Let's build our table header

		// get the saved spec group for this table and create our value
		$columns = get_post_meta( $post_id, 'galfram_choose_columns_custom_tbl', true );

		// set our empty array for our table columns
		$final_table = array();

		if( $columns ) {
			for( $i = 0; $i < $columns; $i++ ) {
				// save our needed subfield values to an array for our table header
				$final_table['header'][$i]['col_id'] =  get_post_meta( $post_id, 'galfram_choose_columns_custom_tbl_' . $i . '_column_id', true );
				$final_table['header'][$i]['col_label'] = get_post_meta( $post_id, 'galfram_choose_columns_custom_tbl_' . $i . '_column', true );
				$final_table['header'][$i]['col_type'] = get_post_meta( $post_id, 'galfram_choose_columns_custom_tbl_' . $i . '_column_data_type', true );
				$final_table['header'][$i]['col_hidden_on'] =  get_post_meta( $post_id, 'galfram_choose_columns_' . $i . '_responsive_settings', true );
				$final_table['header'][$i]['col_disable_sort'] =  get_post_meta( $post_id, 'galfram_choose_columns_' . $i . '_disable_sorting', true );
				$final_table['header'][$i]['col_width'] =  get_post_meta( $post_id, 'galfram_choose_columns_' . $i . '_column_width', true );
				$final_table['header'][$i]['col_width_type'] =  get_post_meta( $post_id, 'galfram_choose_columns_' . $i . '_column_width_type', true );
			}
		}

		// get our user chosen products for this table
		$table_rows = get_post_meta( $post_id, 'add_custom_data', true );

		// echo '<pre>'.print_r($table_rows,1).'</pre>';
		// die();

		// grab our table header sub-array
		$header_cols = $final_table['header'];

		$i=0;

		if( $table_rows ) {
			for( $i = 0; $i < $table_rows; $i++ ) {

				$k=0;

				foreach ( $header_cols as $col ) {

					$col_id = $col['col_id'];
					$col_type = $col['col_type'];
					$col_spec_meta = $col['spec_id'];

					//$final_table['body'][$i][$k]['co_id'] = $col_id;
					$final_table['body'][$i][$k]['col_type'] = $col_type;

					if ( $col_type == 'wysiwyg' ) {
						$final_table['body'][$i][$k]['td'] = get_post_meta( $post_id, 'add_custom_data_' . $i . '_wysiwyg_field_'.$col_id, true );
					}
					else if ( $col_type == 'image' ) {
						$final_table['body'][$i][$k]['td'] = get_post_meta( $post_id, 'add_custom_data_' . $i . '_image_field_'.$col_id, true );
					}
					else if ( $col_type == 'text' ) {
						$final_table['body'][$i][$k]['td'] = get_post_meta( $post_id, 'add_custom_data_' . $i . '_text_field_'.$col_id, true );
					}
					else if ( $col_type == 'number' ) {
						$final_table['body'][$i][$k]['td'] = get_post_meta( $post_id, 'add_custom_data_' . $i . '_number_field_'.$col_id, true );
					}
					else if ( $col_type == 'text-link' ) {
						$final_table['body'][$i][$k]['td']['link_url'] = get_post_meta( $post_id, 'add_custom_data_' . $i . '_textlink_field_'.$col_id.'_0_link_url', true );
						$final_table['body'][$i][$k]['td']['link_text'] = get_post_meta( $post_id, 'add_custom_data_' . $i . '_textlink_field_'.$col_id.'_0_link_text', true );
						$final_table['body'][$i][$k]['td']['new_tab'] = get_post_meta( $post_id, 'add_custom_data_' . $i . '_textlink_field_'.$col_id.'_0_new_tab', true );
					}
					else if ( $col_type == 'image-link' ) {
						$final_table['body'][$i][$k]['td']['image'] = get_post_meta( $post_id, 'add_custom_data_' . $i . '_imagelink_field_'.$col_id.'_0_image', true );
						$final_table['body'][$i][$k]['td']['link_url'] = get_post_meta( $post_id, 'add_custom_data_' . $i . '_imagelink_field_'.$col_id.'_0_link_url', true );
						$final_table['body'][$i][$k]['td']['new_tab'] = get_post_meta( $post_id, 'add_custom_data_' . $i . '_imagelink_field_'.$col_id.'_0_new_tab', true );
					}
					else if ( $col_type == 'price' ) {
						$final_table['body'][$i][$k]['td']['list_price'] = get_post_meta( $post_id, 'add_custom_data_' . $i . '_price_field_'.$col_id.'_0_list_price', true );
						$final_table['body'][$i][$k]['td']['sale_price'] = get_post_meta( $post_id, 'add_custom_data_' . $i . '_price_field_'.$col_id.'_0_sale_price', true );
						$final_table['body'][$i][$k]['td']['amount_off'] = get_post_meta( $post_id, 'add_custom_data_' . $i . '_price_field_'.$col_id.'_0_amount_off', true );
						$final_table['body'][$i][$k]['td']['per_off'] = get_post_meta( $post_id, 'add_custom_data_' . $i . '_price_field_'.$col_id.'_0_percentage_off', true );
					}

					$final_table['body'][$i][$k]['col_width'] = $col['col_width'];
					$final_table['body'][$i][$k]['col_width_type'] = $col['col_width_type'];

					$k++;
				}

			}

		}

		return $final_table;

	}


	


}



