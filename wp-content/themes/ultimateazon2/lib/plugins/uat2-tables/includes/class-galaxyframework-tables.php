<?php

/**
 * The file that defines the core plugin class
 *
 * A class definition that includes attributes and functions used across both the
 * public-facing side of the site and the admin area.
 *
 * @link       https://incomegalaxy.com
 * @since      1.0.0
 *
 * @package    galfram_Tables
 * @subpackage galfram_Tables/includes
 */

/**
 * The core plugin class.
 *
 * This is used to define internationalization, admin-specific hooks, and
 * public-facing site hooks.
 *
 * Also maintains the unique identifier of this plugin as well as the current
 * version of the plugin.
 *
 * @since      1.0.0
 * @package    galfram_Tables
 * @subpackage galfram_Tables/includes
 * @author     Your Name <email@example.com>
 */
class galfram_tables {

	/**
	 * The loader that's responsible for maintaining and registering all hooks that power
	 * the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      galfram_tables_Loader    $loader    Maintains and registers all hooks for the plugin.
	 */
	protected $loader;

	/**
	 * The unique identifier of this plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $galfram_tables    The string used to uniquely identify this plugin.
	 */
	protected $galfram_tables;

	/**
	 * The current version of the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $version    The current version of the plugin.
	 */
	protected $version;

	/**
	 * Define the core functionality of the plugin.
	 *
	 * Set the plugin name and the plugin version that can be used throughout the plugin.
	 * Load the dependencies, define the locale, and set the hooks for the admin area and
	 * the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function __construct() {

		$this->galfram_tables = 'galaxyframework-tables';
		$this->version = '0.1.0';
		$this->edd_product = 'Responsive Tables';
		$this->edd_store = 'https://incomegalaxy.com';

		$this->load_dependencies();
		$this->set_locale();
		$this->define_admin_hooks();
		$this->define_public_hooks();

	}

	/**
	 * Load the required dependencies for this plugin.
	 *
	 * Include the following files that make up the plugin:
	 *
	 * - galfram_tables_Loader. Orchestrates the hooks of the plugin.
	 * - galfram_tables_i18n. Defines internationalization functionality.
	 * - galfram_tables_Admin. Defines all hooks for the admin area.
	 * - galfram_tables_Public. Defines all hooks for the public side of the site.
	 *
	 * Create an instance of the loader which will be used to register the hooks
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function load_dependencies() {

		/**
		 * The class responsible for orchestrating the actions and filters of the
		 * core plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-galaxyframework-tables-loader.php';

		/**
		 * The class responsible for defining internationalization functionality
		 * of the plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-galaxyframework-tables-i18n.php';

		/**
		 * The class responsible for defining all actions that occur in the admin area.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/class-galaxyframework-tables-admin.php';

		/**
		 * The class responsible for defining all actions that occur in the public-facing
		 * side of the site.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'public/class-galaxyframework-tables-public.php';

		/**
		 * 
		 *
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-galaxyframework-table-functions.php';


		// if( !class_exists( 'GALFRAM_TBLS_Plugin_Updater' ) ) {
		// 	// load our custom updater
		// 	require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/partials/GALFRAM_TBLS_Plugin_Updater.php';
		// }

		$this->loader = new galfram_tables_Loader();

	}

	/**
	 * Define the locale for this plugin for internationalization.
	 *
	 * Uses the galfram_tables_i18n class in order to set the domain and to register the hook
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function set_locale() {

		$plugin_i18n = new galfram_tables_i18n();
		$this->loader->add_action( 'plugins_loaded', $plugin_i18n, 'load_plugin_textdomain' );

	}

	/**
	 * Register all of the hooks related to the admin area functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_admin_hooks() {

		$plugin_admin = new galfram_tables_Admin( $this->get_galfram_tables(), $this->get_version(), $this->edd_product, $this->edd_store );

		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_styles' );
		

		// load ACF fields for this plugin
		$this->loader->add_action( 'acf/init', $plugin_admin, 'galfram_tbls_include_acf_fields' );

		// Add our plugin options page
		$this->loader->add_action( 'after_setup_theme', $plugin_admin, 'gf_ext_tbls_add_style_options_page' );

		// Add our post types and taxonomies
		$this->loader->add_action( 'init', $plugin_admin, 'gf_ext_tbls_add_post_types' );


		// Add our type of table options to the select field
		$this->loader->add_filter( 'acf/load_field/name=galfram_type_of_table', $plugin_admin, 'galfram_type_of_table_choices');

		// add our user generated spec groups to a select field
		$this->loader->add_filter( 'acf/load_field/name=galfram_product_spec_group', $plugin_admin, 'galfram_table_choose_spec_group');

		// add a repeater field to add spec columns
		$this->loader->add_action( 'acf/init', $plugin_admin, 'galfram_table_add_spec_columns' );

		// create a repeater field to add reviews to the table
		$this->loader->add_action( 'init', $plugin_admin, 'galfram_table_add_reviews' );

		// add our columns and columns type to the custom data in custom data tables
		$this->loader->add_action( 'init', $plugin_admin, 'galfram_custom_table_add_data_subfields' );

		// create unique ids for our custom table columns
		$this->loader->add_action( 'acf/save_post', $plugin_admin, 'galfram_custom_table_generate_column_id', 20 );
		//$this->loader->add_action( 'acf/save_post', $plugin_admin, 'save_spec_unique_id', 20 );

		$this->loader->add_action( 'acf/save_post', $plugin_admin, 'galfram_save_table_array', 50 );

	}

	/**
	 * Register all of the hooks related to the public-facing functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_public_hooks() {

		$plugin_public = new galfram_tables_Public( $this->get_galfram_tables(), $this->get_version() );

		// add product review post type single loop function
		$this->loader->add_action( 'wp', $plugin_public, 'get_galfram_table_post_type_template' );

		// add our shortcode function for displaying a table
		$this->loader->add_shortcode( 'uat2_table', $plugin_public, 'galfram_layout_table' );

		// check that the STylizer child theme is active.
		$current_theme = wp_get_theme();
		$current_theme_slug = $current_theme->get( 'TextDomain' );
		// If it is active
		//if( $current_theme_slug == 'galaxyframework-child-stylizer' ) {
			// add our custom Stylizer styles to that child theme
			$this->loader->add_action( 'galfram_add_custom_ext_css', $plugin_public, 'galfram_add_custom_ext_css_tables' );

		//}

	}

	/**
	 * Run the loader to execute all of the hooks with WordPress.
	 *
	 * @since    1.0.0
	 */
	public function run() {
		$this->loader->run();
	}

	/**
	 * The name of the plugin used to uniquely identify it within the context of
	 * WordPress and to define internationalization functionality.
	 *
	 * @since     1.0.0
	 * @return    string    The name of the plugin.
	 */
	public function get_galfram_tables() {
		return $this->galfram_tables;
	}

	/**
	 * The reference to the class that orchestrates the hooks with the plugin.
	 *
	 * @since     1.0.0
	 * @return    galfram_tables_Loader    Orchestrates the hooks of the plugin.
	 */
	public function get_loader() {
		return $this->loader;
	}

	/**
	 * Retrieve the version number of the plugin.
	 *
	 * @since     1.0.0
	 * @return    string    The version number of the plugin.
	 */
	public function get_version() {
		return $this->version;
	}

}
