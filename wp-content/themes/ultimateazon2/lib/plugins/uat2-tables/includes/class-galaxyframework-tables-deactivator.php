<?php

/**
 * Fired during plugin deactivation
 *
 * @link       https://incomegalaxy.com
 * @since      1.0.0
 *
 * @package    galfram_Tables
 * @subpackage galfram_Tables/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    galfram_Tables
 * @subpackage galfram_Tables/includes
 * @author     Your Name <email@example.com>
 */
class galfram_tables_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

		// Let's set the 'galaxyframework-tables' key to "off" when we deactivate this plugin
		$galfram_checker = get_option( 'galfram_checker' );
    	$galfram_checker['galaxyframework-tables'] = 'off';
    	update_option( 'galfram_checker', $galfram_checker );

	}

}
