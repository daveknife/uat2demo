<?php

/**
 * Fired during plugin activation
 *
 * @link       https://incomegalaxy.com
 * @since      1.0.0
 *
 * @package    galfram_Tables
 * @subpackage galfram_Tables/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    galfram_Tables
 * @subpackage galfram_Tables/includes
 * @author     Your Name <email@example.com>
 */
class galfram_tables_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {




		/**
		 * Set up our default licensing values
		 *
		 * @since    1.0.0
		 */

		$galaxy_plugin_acb_license = get_option( 'galfram_plugin_acb_license' );
	    // Are our options saved in the DB?
	    if ( false === $galaxy_plugin_acb_license ) {
	        // If not, we'll save our default options
	        add_option( 'galfram_plugin_acb_license', '' );
	        add_option( 'galfram_plugin_acb_license_status', 'deactivated' );
	    }




		// let's save out extension checker array in the database if it doesn't exist, or set it to "yes" if it already exists
		    
	    // get our framework and extension option from the database
	    $galfram_checker = get_option( 'galfram_checker' );

	    // if the option exists
	    if ( $galfram_checker ) {
	        // check to see if the 'galaxyframework-child-stylizer' key exists
	        if ( array_key_exists( 'galaxyframework-tables' , $galfram_checker ) ) {
	            // if it does, and it is set to "off"
	            if ( $galfram_checker['galaxyframework-tables'] == 'off' ) {
	                // set it to "on"
	                $galfram_checker['galaxyframework-tables'] = 'on';
	                // and update the option in the database
	                update_option( 'galfram_checker', $galfram_checker );
	            }
	        } 
	        // if the key does not yet exist ... 
	        else {
	            // create our key and set it to "on"
	            $galfram_checker['galaxyframework-tables'] = 'on';
	            // and update the option in the database
	            update_option( 'galfram_checker', $galfram_checker );
	        }
	    }
	    // if the option does not exist, the galaxy framework was never installed so we'll just get out
	    else {
	        return;
	    }


	}

}
