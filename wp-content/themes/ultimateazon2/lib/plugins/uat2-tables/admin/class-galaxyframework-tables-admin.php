<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       https://incomegalaxy.com
 * @since      1.0.0
 *
 * @package    galfram_Tables
 * @subpackage galfram_Tables/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    galfram_Tables
 * @subpackage galfram_Tables/admin
 * @author     Your Name <email@example.com>
 */
class galfram_tables_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $galfram_tables    The ID of this plugin.
	 */
	private $galfram_tables;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $galfram_tables       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $galfram_tables, $version, $edd_product, $edd_store ) {

		$this->galfram_tables = $galfram_tables;
		$this->version = $version;
		$this->edd_product = $edd_product;
		$this->edd_store = $edd_store;

	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in galfram_tables_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The galfram_tables_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->galfram_tables, get_stylesheet_directory_uri() . '/lib/plugins/uat2-tables/admin/css/galaxyframework-tables-admin.css', array(), $this->version, 'all' );

	}

	









    /**
	 * Create ACF Styles option page after theme setup
	 *
	 * @since    1.0.0
	 */
	function gf_ext_tbls_add_style_options_page() {
		// Add Site Styles Page
		if( function_exists('acf_add_options_page') ) {
			$option_page = acf_add_options_sub_page(array(
				'page_title' 	=> __('Responsive Tables - Settings', 'ultimateazon2'),
				'menu_title' 	=> __('Tables', 'ultimateazon2'),
				'menu_slug' 	=> 'extension-tbls-settings',
				'capability' 	=> 'edit_posts',
				'post_id'       => 'extension-tbls-settings',
				'parent_slug' 	=> 'theme-settings',
				'position'      => 300,
				'redirect' 	=> false
			));
		}
	}







	/**
	 * Create tables post type
	 *
	 * @since    1.0.0
	 */
	function gf_ext_tbls_add_post_types() {



	    $plural_name = __('Tables', 'ultimateazon2');
	    $singular_name = __('Table', 'ultimateazon2');


	    register_post_type( 'galfram_table',
	        array(
	            'labels' => array(
	                'name' => $plural_name,
	                'singular_name' => $singular_name,
	                'add_new' => __('Add New', 'ultimateazon2'),
	                'add_new_item' => __('Add New','ultimateazon2').' '.$singular_name,
	                'edit' => __('Edit','ultimateazon2'),
	                'edit_item' => __('Edit','ultimateazon2').' '.$singular_name,
	                'new_item' => __('New','ultimateazon2').' '.$singular_name,
	                'view' => __('View','ultimateazon2'),
	                'view_item' => __('View','ultimateazon2').' '.$singular_name,
	                'search_items' => __('Search','ultimateazon2').' '.$plural_name,
	                'not_found' => __('No','ultimateazon2').' '.$plural_name.__('found','ultimateazon2'),
	                'not_found_in_trash' => __('No','ultimateazon2').' '.$plural_name.' '.__('found in Trash','ultimateazon2'),
	                'parent' => __('Parent','ultimateazon2').' '.$singular_name
	            ),
	            'public' => true,
	            'show_in_menu' => true,
	            'menu_position' => 7,
	            'supports' => array( 'title', 'author', 'revisions', 'editor' ),
	            'taxonomies' => array( '' ),
	            'menu_icon' => 'dashicons-editor-table',
	            'has_archive' => false
	        )
	    );

	}



 
	
	/**
	 * Add our table type choices to the select field
	 * EXTENDS PRODUCT REVIEW PRO
	 *
	 * @since    1.0.0
	 */
	public function galfram_type_of_table_choices( $field ) {


		// reset choices
    	$field['choices'] = array();

		$field['choices']['prp-table-custom'] = __('Custom Table', 'ultimateazon2');

		// $prp_active = galfram_extension_checker( 'galaxyframework-productreviewpro');

		// if ( $prp_active == 'on' ) {
		$field['choices']['prp-table'] = __('Product Review Pro Default Table', 'ultimateazon2');
		// 	//$field['choices']['prp-table-api'] = __('Product Review Pro Amazon API Table', 'ultimateazon2');
		// }

		// return the field
    	return $field;

	} // end public function add_contentbuilder_loop()




	/**
	 * Add our user created specification groups to the select choices if the table type is set too prp-table
	 * EXTENDS PRODUCT REVIEW PRO
	 *
	 * @since    1.0.0
	 */
	public function galfram_table_choose_spec_group( $field ) {

		// get user saved spefs fields array
		$user_specs_groups = get_option( 'galfram_ext_prp_all_userspecs_groups' );

		// set empty choices
    	$field['choices'] = array();
    	$choices = array();

    	$choices['pull-from-amazon'] = __('Pull All Data from Amazon', 'ultimateazon2' );

    	if ( $user_specs_groups ) {
    		foreach ( $user_specs_groups as $group ) {
				$option_val = $group['field_57c35f7e8031b'];
				$option_txt = $group['field_57c068df622b6'];
				$choices[$option_val] = $option_txt;
			}

			// remove any unwanted white space
		    $choices = array_map('trim', $choices);

		    // loop through array and add to field 'choices'
		    if( is_array($choices) ) {
		        foreach( $choices as $value => $choice ) {
		            $field['choices'][ $value ] = $choice;
		        }
		    }
    	}

	    
	    return $field;

	} // end public function galfram_table_choose_spec_group()








	



	/**
	 * Include the ACF fields
	 *
	 * @since    1.0.0
	 */
	public function galfram_tbls_include_acf_fields() {

		$url = 'http://'.$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'];

		if (!empty($_GET['post'])) {
			$post_id = $_GET['post'];

			$spec_type = get_post_meta( $post_id, 'galfram_product_spec_group', true );

			if ( $spec_type == 'pull-from-amazon' ) {
				$collapsed_row = 'field_58361280dbab8jhmn';
			} else {
				$collapsed_row = 'field_58361280dbab8';
			}

		}

		else {
			$collapsed_row = '';
		}


			acf_add_local_field_group(array (
				'key' => 'group_5834aff82da78',
				'title' => __('Responsive Table', 'ultimateazon2'),
				'fields' => array (
					array (
						'key' => 'field_5834fb9519689',
						'label' => __('Type of Table', 'ultimateazon2'),
						'name' => 'galfram_type_of_table',
						'type' => 'select',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'choices' => array (
							'prp-table-custom' => __('Custom Table', 'ultimateazon2'),
						),
						'default_value' => array (
						),
						'allow_null' => 0,
						'multiple' => 0,
						'ui' => 0,
						'ajax' => 0,
						'return_format' => 'value',
						'placeholder' => '',
					),
					array (
						'key' => 'field_583595d1d82b7',
						'label' => __('Choose Product Specification Group', 'ultimateazon2'),
						'name' => 'galfram_product_spec_group',
						'type' => 'select',
						'instructions' => __('A Product Review Pro table can only use one set of custom specifications. After you choose this, you will be able to choose which reviews using that specification set you want to include in your table.', 'ultimateazon2'). '<strong style="color: red;">'.__('You must save your post to make the specifications group available on this edit screen', 'ultimateazon2').'</strong>',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_5834fb9519689',
									'operator' => '==',
									'value' => 'prp-table',
								),
							),
						),
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'choices' => array (),
						'default_value' => array (
						),
						'allow_null' => 0,
						'multiple' => 0,
						'ui' => 0,
						'ajax' => 0,
						'return_format' => 'value',
						'placeholder' => '',
					),
					array (
						'key' => 'field_5836125bdbab7',
						'label' => __('Choose Columns', 'ultimateazon2'),
						'name' => 'galfram_choose_columns',
						'type' => 'repeater',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_5834fb9519689',
									'operator' => '==',
									'value' => 'prp-table',
								),
							),
						),
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'collapsed' => $collapsed_row,
						'min' => '',
						'max' => '',
						'layout' => 'row',
						'button_label' => __('Add Column', 'ultimateazon2'),
						'sub_fields' => array (),
					),
					array (
						'key' => 'field_5836125bdbab7_custom_tbl',
						'label' => __('Choose Columns', 'ultimateazon2'),
						'name' => 'galfram_choose_columns_custom_tbl',
						'type' => 'repeater',
						'instructions' => '<strong style="color: red;">'. __('You must save/update this page after editing your columns to make the columns available for your table data below.', 'ultimateazon2').'</strong>',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_5834fb9519689',
									'operator' => '==',
									'value' => 'prp-table-custom',
								),
							),
						),
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'collapsed' => $collapsed_row,
						'min' => '',
						'max' => '',
						'layout' => 'row',
						'button_label' => __('Add Column', 'ultimateazon2'),
						'sub_fields' => array (),
					),
					array (
						'key' => 'field_5838867e43db1',
						'label' => __('Add Products', 'ultimateazon2'),
						'name' => 'add_products',
						'type' => 'repeater',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_5834fb9519689',
									'operator' => '==',
									'value' => 'prp-table',
								),

							),
						),
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'collapsed' => '',
						'min' => '',
						'max' => '',
						'layout' => 'row',
						'button_label' => __('Add Row', 'ultimateazon2'),
						'sub_fields' => array (),
					),
					array (
						'key' => 'field_5838867e43db1_add_custom_data',
						'label' => __('Add Data', 'ultimateazon2'),
						'name' => 'add_custom_data',
						'type' => 'repeater',
						'instructions' => __('Scroll Horizontally to view all of the fields.', 'ultimateazon2').'<br />'.__('You can toggle the rows in the left side using the toggle button.', 'ultimateazon2').'<br />'.__('You can delete rows by using the "minus" button all the way on the right.', 'ultimateazon2').'<br />'.__('You can re-order the rows by dragging them up and down.', 'ultimateazon2'),
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_5834fb9519689',
									'operator' => '==',
									'value' => 'prp-table-custom',
								),
								
							),
						),
						'wrapper' => array (
							'width' => '',
							'class' => 'galfram-custom-data-rows',
							'id' => '',
						),
						'collapsed' => 'field_chc7j4j48frjfr8r',
						'min' => '',
						'max' => '',
						'layout' => 'table',
						'button_label' => __('Add Row', 'ultimateazon2'),
						'sub_fields' => array (),
					),
				),
				'location' => array (
					array (
						array (
							'param' => 'post_type',
							'operator' => '==',
							'value' => 'galfram_table',
						),
					),
				),
				'menu_order' => 0,
				'position' => 'normal',
				'style' => 'default',
				'label_placement' => 'top',
				'instruction_placement' => 'label',
				'hide_on_screen' => '',
				'active' => 1,
				'description' => '',
			));




		// Add Global Table Settings Fields

		acf_add_local_field_group(array (
			'key' => 'group_583f974b29e12',
			'title' => __('Global Advanced Table Settings', 'ultimateazon2'),
			'fields' => array (
				array (
					'key' => 'field_583f974b317f5',
					'label' => __('Table Heading', 'ultimateazon2'),
					'name' => '',
					'type' => 'message',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array (
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'message' => '',
					'new_lines' => 'wpautop',
					'esc_html' => 0,
				),
				array (
					'key' => 'field_583f974b31802',
					'label' => __('Hide the Table Header', 'ultimateazon2'),
					'name' => 'hide_the_table_header',
					'type' => 'true_false',
					'instructions' => __('Sortable rows are not possible if this is checked.', 'ultimateazon2'),
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array (
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'message' => __('Check this box to hide the table header row.', 'ultimateazon2'),
					'default_value' => 0,
				),
				array (
					'key' => 'field_583f974b3180e',
					'label' => __('Table Sorting', 'ultimateazon2'),
					'name' => '',
					'type' => 'message',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array (
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'message' => '',
					'new_lines' => 'wpautop',
					'esc_html' => 0,
				),
				array (
					'key' => 'field_583f974b3181a',
					'label' => __('Enable Table Sorting', 'ultimateazon2'),
					'name' => 'enable_table_sorting',
					'type' => 'true_false',
					'instructions' => __('(Table sorting will not work if you have the "Hide the Table Header" checkbox checked.)', 'ultimateazon2'),
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array (
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'message' => __('Check this box to make this table sortable', 'ultimateazon2'),
					'default_value' => 0,
				),
				array (
					'key' => 'field_583f974b31826',
					'label' => __('Table Filtering', 'ultimateazon2'),
					'name' => '',
					'type' => 'message',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array (
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'message' => '',
					'new_lines' => 'wpautop',
					'esc_html' => 0,
				),
				array (
					'key' => 'field_583f974b31833',
					'label' => __('Enable Table Filtering', 'ultimateazon2'),
					'name' => 'enable_table_filtering',
					'type' => 'true_false',
					'instructions' => __('Enabling this will add a search box on top of the table that will filter the table rows by the value entered.', 'ultimateazon2'),
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array (
						'width' => '',
						'class' => 'djn-clear-all',
						'id' => '',
					),
					'message' => __('Check this box to enable table filtering', 'ultimateazon2'),
					'default_value' => 0,
				),
				array (
					'key' => 'field_583f974b31840',
					'label' => __('No Results Message', 'ultimateazon2'),
					'name' => 'no_results_message',
					'type' => 'text',
					'instructions' => __('Enter the message you would like to display if the filter returns 0 results.', 'ultimateazon2'),
					'required' => 0,
					'conditional_logic' => array (
						array (
							array (
								'field' => 'field_583f974b31833',
								'operator' => '==',
								'value' => '1',
							),
						),
					),
					'wrapper' => array (
						'width' => '50',
						'class' => '',
						'id' => '',
					),
					'default_value' => '',
					'placeholder' => '',
					'prepend' => '',
					'append' => '',
					'maxlength' => '',
				),
				array (
					'key' => 'field_583f974b3184c',
					'label' => __('Filter Placeholder Text', 'ultimateazon2'),
					'name' => 'filter_placeholder_text',
					'type' => 'text',
					'instructions' => __('Enter the text that will display in the empty filter box.', 'ultimateazon2'),
					'required' => 0,
					'conditional_logic' => array (
						array (
							array (
								'field' => 'field_583f974b31833',
								'operator' => '==',
								'value' => '1',
							),
						),
					),
					'wrapper' => array (
						'width' => '50',
						'class' => '',
						'id' => '',
					),
					'default_value' => '',
					'placeholder' => '',
					'prepend' => '',
					'append' => '',
					'maxlength' => '',
				),
				array (
					'key' => 'field_583f974b31858',
					'label' => __('Filter Dropdown Heading', 'ultimateazon2'),
					'name' => 'filter_dropdown_heading',
					'type' => 'text',
					'instructions' => __('Enter a Heading for the filter dropdown for which columns are searchable. Leave this blank for no heading.', 'ultimateazon2'),
					'required' => 0,
					'conditional_logic' => array (
						array (
							array (
								'field' => 'field_583f974b31833',
								'operator' => '==',
								'value' => '1',
							),
						),
					),
					'wrapper' => array (
						'width' => '50',
						'class' => 'djn-clear-all',
						'id' => '',
					),
					'default_value' => '',
					'placeholder' => '',
					'prepend' => '',
					'append' => '',
					'maxlength' => '',
				),
				array (
					'key' => 'field_583f974b31881',
					'label' => __('Filter Position', 'ultimateazon2'),
					'name' => 'filter_position',
					'type' => 'select',
					'instructions' => __('The filter displays to the right by default. It can be placed to the left, center, or right.', 'ultimateazon2'),
					'required' => 0,
					'conditional_logic' => array (
						array (
							array (
								'field' => 'field_583f974b31833',
								'operator' => '==',
								'value' => '1',
							),
						),
					),
					'wrapper' => array (
						'width' => '50',
						'class' => '',
						'id' => '',
					),
					'choices' => array (
						'left' => __('Left', 'ultimateazon2'),
						'center' => __('Center', 'ultimateazon2'),
						'right' => __('Right', 'ultimateazon2'),
					),
					'default_value' => array (
						0 => 'right',
					),
					'allow_null' => 0,
					'multiple' => 0,
					'ui' => 0,
					'ajax' => 0,
					'return_format' => 'value',
					'placeholder' => '',
				),
				array (
					'key' => 'field_583f974b3189f',
					'label' => __('Table Pagination', 'ultimateazon2'),
					'name' => '',
					'type' => 'message',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array (
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'message' => '',
					'new_lines' => 'wpautop',
					'esc_html' => 0,
				),
				array (
					'key' => 'field_583f974b318ac',
					'label' => __('Enable Table Pagination', 'ultimateazon2'),
					'name' => 'enable_table_pagination',
					'type' => 'true_false',
					'instructions' => __('This should be used for tables with a large amount of rows.', 'ultimateazon2'),
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array (
						'width' => '',
						'class' => 'djn-clear-all',
						'id' => '',
					),
					'message' => __('Check this box to enable table pagination', 'ultimateazon2'),
					'default_value' => 0,
				),
				array (
					'key' => 'field_583f974b318b9',
					'label' => __('Number of Rows Per Page', 'ultimateazon2'),
					'name' => 'number_of_rows_per_page',
					'type' => 'number',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => array (
						array (
							array (
								'field' => 'field_583f974b318ac',
								'operator' => '==',
								'value' => '1',
							),
						),
					),
					'wrapper' => array (
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'default_value' => 10,
					'placeholder' => '',
					'prepend' => '',
					'append' => __('Rows', 'ultimateazon2'),
					'min' => '',
					'max' => '',
					'step' => '',
				),
				array (
					'key' => 'field_583f974b318c5',
					'label' => __('Pagination Position', 'ultimateazon2'),
					'name' => 'pagination_position',
					'type' => 'select',
					'instructions' => __('The pagination displays to the center by default. It can be placed to the left, center, or right.', 'ultimateazon2'),
					'required' => 0,
					'conditional_logic' => array (
						array (
							array (
								'field' => 'field_583f974b318ac',
								'operator' => '==',
								'value' => '1',
							),
						),
					),
					'wrapper' => array (
						'width' => '50',
						'class' => '',
						'id' => '',
					),
					'choices' => array (
						'left' => __('Left', 'ultimateazon2'),
						'center' => __('Center', 'ultimateazon2'),
						'right' => __('Right', 'ultimateazon2'),
					),
					'default_value' => array (
						0 => 'center',
					),
					'allow_null' => 0,
					'multiple' => 0,
					'ui' => 0,
					'ajax' => 0,
					'return_format' => 'value',
					'placeholder' => '',
				),
				array (
					'key' => 'field_583f974b318d1',
					'label' => __('Table Pagination - Pages Displayed Limit', 'ultimateazon2'),
					'name' => 'pages_displayed_limit',
					'type' => 'number',
					'instructions' => __('This setting does not limit the number of pages in your table, it only limits the number of pages that are displayed in the pagination at the same time. Users can still access every page of your full table. This is useful for tables a large amount of rows.', 'ultimateazon2'),
					'required' => 0,
					'conditional_logic' => array (
						array (
							array (
								'field' => 'field_583f974b318ac',
								'operator' => '==',
								'value' => '1',
							),
						),
					),
					'wrapper' => array (
						'width' => '50',
						'class' => '',
						'id' => '',
					),
					'default_value' => '',
					'placeholder' => '',
					'prepend' => '',
					'append' => 'Pages',
					'min' => '',
					'max' => '',
					'step' => '',
				),
				array (
					'key' => 'field_583f974b318f2',
					'label' => __('Table Memory', 'ultimateazon2'),
					'name' => '',
					'type' => 'message',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array (
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'message' => '',
					'new_lines' => 'wpautop',
					'esc_html' => 0,
				),
				array (
					'key' => 'field_583f974b31915',
					'label' => __('Enable Table Memory', 'ultimateazon2'),
					'name' => 'enable_table_memory',
					'type' => 'true_false',
					'instructions' => __('Enabling this option will store the state of your table in the visitor\'s browser local storage, so the next time they visit the page the table will remember the sorting, filtering, and pagination that the visitor left it as.', 'ultimateazon2'),
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array (
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'message' => __('Check this box to enable table memory for your visitors.', 'ultimateazon2'),
					'default_value' => 0,
				),
				array (
					'key' => 'field_583f974b31923',
					'label' => __('Table Custom Responsive Settings', 'ultimateazon2'),
					'name' => '',
					'type' => 'message',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array (
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'message' => '',
					'new_lines' => 'wpautop',
					'esc_html' => 0,
				),
				array (
					'key' => 'field_583f974b3192e',
					'label' => __('Base Responsive Breakpoints off the Width of the Container', 'ultimateazon2'),
					'name' => 'breakpoints_off_container',
					'type' => 'true_false',
					'instructions' => __('By default the breakpoints are based off the width of the window. Check this box to base the breakpoints off the width of the container the table resides in.', 'ultimateazon2'),
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array (
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'message' => __('Check this box to base breakpoints off the parent container width.', 'ultimateazon2'),
					'default_value' => 0,
				),
				array (
					'key' => 'field_583f974b3193a',
					'label' => __('Custom Breakpoints', 'ultimateazon2'),
					'name' => 'custom_breakpoints',
					'type' => 'true_false',
					'instructions' => __('This setting is if you want to use custom breakpoints for your responsive tables. If you are not familiar with responsive breakpoints, it is best to leave this unchecked.', 'ultimateazon2'),
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array (
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'message' => __('Check this box to use custom breakpoints for this table.', 'ultimateazon2'),
					'default_value' => 0,
				),
				array (
					'key' => 'field_583f974b31946',
					'label' => __('Extra Small Breakpoint', 'ultimateazon2'),
					'name' => 'extra_small_breakpoint',
					'type' => 'number',
					'instructions' => __('This breakpoint is for extra small screens.', 'ultimateazon2'),
					'required' => 0,
					'conditional_logic' => array (
						array (
							array (
								'field' => 'field_583f974b3193a',
								'operator' => '==',
								'value' => '1',
							),
						),
					),
					'wrapper' => array (
						'width' => '25',
						'class' => '',
						'id' => '',
					),
					'default_value' => '',
					'placeholder' => '',
					'prepend' => '',
					'append' => 'px',
					'min' => '',
					'max' => '',
					'step' => '',
				),
				array (
					'key' => 'field_583f974b31951',
					'label' => __('Small Breakpoint', 'ultimateazon2'),
					'name' => 'small_breakpoint',
					'type' => 'number',
					'instructions' => __('This breakpoint is for small screens.', 'ultimateazon2'),
					'required' => 0,
					'conditional_logic' => array (
						array (
							array (
								'field' => 'field_583f974b3193a',
								'operator' => '==',
								'value' => '1',
							),
						),
					),
					'wrapper' => array (
						'width' => '25',
						'class' => '',
						'id' => '',
					),
					'default_value' => '',
					'placeholder' => '',
					'prepend' => '',
					'append' => 'px',
					'min' => '',
					'max' => '',
					'step' => '',
				),
				array (
					'key' => 'field_583f974b3195e',
					'label' => __('Medium Breakpoint', 'ultimateazon2'),
					'name' => 'medium_breakpoint',
					'type' => 'number',
					'instructions' => __('This breakpoint is for medium screens.', 'ultimateazon2'),
					'required' => 0,
					'conditional_logic' => array (
						array (
							array (
								'field' => 'field_583f974b3193a',
								'operator' => '==',
								'value' => '1',
							),
						),
					),
					'wrapper' => array (
						'width' => '25',
						'class' => '',
						'id' => '',
					),
					'default_value' => '',
					'placeholder' => '',
					'prepend' => '',
					'append' => 'px',
					'min' => '',
					'max' => '',
					'step' => '',
				),
				array (
					'key' => 'field_583f974b3196a',
					'label' => __('Large Breakpoint', 'ultimateazon2'),
					'name' => 'large_breakpoint',
					'type' => 'number',
					'instructions' => __('This breakpoint is for large screens.', 'ultimateazon2'),
					'required' => 0,
					'conditional_logic' => array (
						array (
							array (
								'field' => 'field_583f974b3193a',
								'operator' => '==',
								'value' => '1',
							),
						),
					),
					'wrapper' => array (
						'width' => '25',
						'class' => '',
						'id' => '',
					),
					'default_value' => '',
					'placeholder' => '',
					'prepend' => '',
					'append' => 'px',
					'min' => '',
					'max' => '',
					'step' => '',
				),
				array (
					'key' => 'field_583f974b31976',
					'label' => __('Expand the First Row by Default', 'ultimateazon2'),
					'name' => 'expand_the_first_row_by_default',
					'type' => 'true_false',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array (
						'width' => '50',
						'class' => 'djn-clear-all',
						'id' => '',
					),
					'message' => __('Check this box to expand the first row by default', 'ultimateazon2'),
					'default_value' => 0,
				),
				array (
					'key' => 'field_583f974b3198c',
					'label' => __('Expand All Rows by Default', 'ultimateazon2'),
					'name' => 'expand_all_rows_by_default',
					'type' => 'true_false',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array (
						'width' => '50',
						'class' => '',
						'id' => '',
					),
					'message' => __('Check this box to expand all rows by default', 'ultimateazon2'),
					'default_value' => 0,
				),
				array (
					'key' => 'field_583f974b319a0',
					'label' => __('Hide the Toggle Button', 'ultimateazon2'),
					'name' => 'hide_the_toggle_button',
					'type' => 'true_false',
					'instructions' => __('Check this box if you wish to hide the row toggle button. This will not prevent the rows form being opened and closed if clicked.', 'ultimateazon2'),
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array (
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'message' => __('Check this box to hide the toggle button on each row.', 'ultimateazon2'),
					'default_value' => 0,
				),
			),
			'location' => array (
				array (
					array (
						'param' => 'options_page',
						'operator' => '==',
						'value' => 'extension-tbls-settings',
					),
				),
			),
			'menu_order' => 50,
			'position' => 'normal',
			'style' => 'default',
			'label_placement' => 'top',
			'instruction_placement' => 'label',
			'hide_on_screen' => '',
			'active' => 1,
			'description' => '',
		));







		// Add Override Table Settings Fields



		acf_add_local_field_group(array (
			'key' => 'group_583df06daf0b0',
			'title' => __('Advanced Table Settings', 'ultimateazon2'),
			'fields' => array (
				array (
					'key' => 'field_583f359fc3be7',
					'label' => __('Overide Global Table Settings', 'ultimateazon2'),
					'name' => 'overide_global_table_settings',
					'type' => 'true_false',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array (
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'message' => __('Check this box to override the global table settings', 'ultimateazon2'),
					'default_value' => 0,
				),
				array (
					'key' => 'field_583f3505c8549',
					'label' => __('Table Heading', 'ultimateazon2'),
					'name' => '',
					'type' => 'message',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => array (
						array (
							array (
								'field' => 'field_583f359fc3be7',
								'operator' => '==',
								'value' => '1',
							),
						),
					),
					'wrapper' => array (
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'message' => '',
					'new_lines' => 'wpautop',
					'esc_html' => 0,
				),
				array (
					'key' => 'field_583df314a93a1',
					'label' => __('Hide the Table Header', 'ultimateazon2'),
					'name' => 'hide_the_table_header',
					'type' => 'true_false',
					'instructions' => __('Sortable rows are not possible if this is checked.', 'ultimateazon2'),
					'required' => 0,
					'conditional_logic' => array (
						array (
							array (
								'field' => 'field_583f359fc3be7',
								'operator' => '==',
								'value' => '1',
							),
						),
					),
					'wrapper' => array (
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'message' => __('Check this box to hide the table header row.', 'ultimateazon2'),
					'default_value' => 0,
				),
				array (
					'key' => 'field_583f34e6c8548',
					'label' => __('Table Sorting', 'ultimateazon2'),
					'name' => '',
					'type' => 'message',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => array (
						array (
							array (
								'field' => 'field_583f359fc3be7',
								'operator' => '==',
								'value' => '1',
							),
						),
					),
					'wrapper' => array (
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'message' => '',
					'new_lines' => 'wpautop',
					'esc_html' => 0,
				),
				array (
					'key' => 'field_583df6a975909',
					'label' => __('Enable Table Sorting', 'ultimateazon2'),
					'name' => 'enable_table_sorting',
					'type' => 'true_false',
					'instructions' => __('(Table sorting will not work if you have the "Hide the Table Header" checkbox checked.)', 'ultimateazon2'),
					'required' => 0,
					'conditional_logic' => array (
						array (
							array (
								'field' => 'field_583f359fc3be7',
								'operator' => '==',
								'value' => '1',
							),
						),
					),
					'wrapper' => array (
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'message' => __('Check this box to make this table sortable', 'ultimateazon2'),
					'default_value' => 0,
				),
				array (
					'key' => 'field_583f3510c854a',
					'label' => __('Table Filtering', 'ultimateazon2'),
					'name' => '',
					'type' => 'message',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => array (
						array (
							array (
								'field' => 'field_583f359fc3be7',
								'operator' => '==',
								'value' => '1',
							),
						),
					),
					'wrapper' => array (
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'message' => '',
					'new_lines' => 'wpautop',
					'esc_html' => 0,
				),
				array (
					'key' => 'field_583df7147590a',
					'label' => __('Enable Table Filtering', 'ultimateazon2'),
					'name' => 'enable_table_filtering',
					'type' => 'true_false',
					'instructions' => __('Enabling this will add a search box on top of the table that will filter the table rows by the value entered.', 'ultimateazon2'),
					'required' => 0,
					'conditional_logic' => array (
						array (
							array (
								'field' => 'field_583f359fc3be7',
								'operator' => '==',
								'value' => '1',
							),
						),
					),
					'wrapper' => array (
						'width' => '',
						'class' => 'djn-clear-all',
						'id' => '',
					),
					'message' => __('Check this box to enable table filtering', 'ultimateazon2'),
					'default_value' => 0,
				),
				array (
					'key' => 'field_583f31b5a801b',
					'label' => __('No Results Message', 'ultimateazon2'),
					'name' => 'no_results_message',
					'type' => 'text',
					'instructions' => __('Enter the message you would like to display if the filter returns 0 results.', 'ultimateazon2'),
					'required' => 0,
					'conditional_logic' => array (
						array (
							array (
								'field' => 'field_583df7147590a',
								'operator' => '==',
								'value' => '1',
							),
							array (
								'field' => 'field_583f359fc3be7',
								'operator' => '==',
								'value' => '1',
							),
						),
					),
					'wrapper' => array (
						'width' => '50',
						'class' => '',
						'id' => '',
					),
					'default_value' => '',
					'placeholder' => '',
					'prepend' => '',
					'append' => '',
					'maxlength' => '',
				),
				array (
					'key' => 'field_583f32a85ad5a',
					'label' => __('Filter Placeholder Text', 'ultimateazon2'),
					'name' => 'filter_placeholder_text',
					'type' => 'text',
					'instructions' => __('Enter the text that will display in the empty filter box.', 'ultimateazon2'),
					'required' => 0,
					'conditional_logic' => array (
						array (
							array (
								'field' => 'field_583df7147590a',
								'operator' => '==',
								'value' => '1',
							),
							array (
								'field' => 'field_583f359fc3be7',
								'operator' => '==',
								'value' => '1',
							),
						),
					),
					'wrapper' => array (
						'width' => '50',
						'class' => '',
						'id' => '',
					),
					'default_value' => '',
					'placeholder' => '',
					'prepend' => '',
					'append' => '',
					'maxlength' => '',
				),
				array (
					'key' => 'field_583f338b180a2',
					'label' => __('Filter Dropdown Heading', 'ultimateazon2'),
					'name' => 'filter_dropdown_heading',
					'type' => 'text',
					'instructions' => __('Enter a Heading for the filter dropdown for which columns are searchable. Leave this blank for no heading.', 'ultimateazon2'),
					'required' => 0,
					'conditional_logic' => array (
						array (
							array (
								'field' => 'field_583df7147590a',
								'operator' => '==',
								'value' => '1',
							),
							array (
								'field' => 'field_583f359fc3be7',
								'operator' => '==',
								'value' => '1',
							),
						),
					),
					'wrapper' => array (
						'width' => '50',
						'class' => 'djn-clear-all',
						'id' => '',
					),
					'default_value' => '',
					'placeholder' => '',
					'prepend' => '',
					'append' => '',
					'maxlength' => '',
				),
				array (
					'key' => 'field_583f3487c8547',
					'label' => __('Filter Position', 'ultimateazon2'),
					'name' => 'filter_position',
					'type' => 'select',
					'instructions' => __('The filter displays to the right by default. It can be placed to the left, center, or right.', 'ultimateazon2'),
					'required' => 0,
					'conditional_logic' => array (
						array (
							array (
								'field' => 'field_583df7147590a',
								'operator' => '==',
								'value' => '1',
							),
							array (
								'field' => 'field_583f359fc3be7',
								'operator' => '==',
								'value' => '1',
							),
						),
					),
					'wrapper' => array (
						'width' => '50',
						'class' => '',
						'id' => '',
					),
					'choices' => array (
						'left' => __('Left', 'ultimateazon2'),
						'center' => __('Center', 'ultimateazon2'),
						'right' => __('Right', 'ultimateazon2'),
					),
					'default_value' => array (
						0 => 'right',
					),
					'allow_null' => 0,
					'multiple' => 0,
					'ui' => 0,
					'ajax' => 0,
					'return_format' => 'value',
					'placeholder' => '',
				),
				array (
					'key' => 'field_583f3521c854b',
					'label' => __('Table Pagination', 'ultimateazon2'),
					'name' => '',
					'type' => 'message',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => array (
						array (
							array (
								'field' => 'field_583f359fc3be7',
								'operator' => '==',
								'value' => '1',
							),
						),
					),
					'wrapper' => array (
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'message' => '',
					'new_lines' => 'wpautop',
					'esc_html' => 0,
				),
				array (
					'key' => 'field_583dfa558afbd',
					'label' => __('Enable Table Pagination', 'ultimateazon2'),
					'name' => 'enable_table_pagination',
					'type' => 'true_false',
					'instructions' => __('This should be used for tables with a large amount of rows.', 'ultimateazon2'),
					'required' => 0,
					'conditional_logic' => array (
						array (
							array (
								'field' => 'field_583f359fc3be7',
								'operator' => '==',
								'value' => '1',
							),
						),
					),
					'wrapper' => array (
						'width' => '',
						'class' => 'djn-clear-all',
						'id' => '',
					),
					'message' => __('Check this box to enable table pagination', 'ultimateazon2'),
					'default_value' => 0,
				),
				array (
					'key' => 'field_583e5969692b1',
					'label' => __('Number of Rows Per Page', 'ultimateazon2'),
					'name' => 'number_of_rows_per_page',
					'type' => 'number',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => array (
						array (
							array (
								'field' => 'field_583dfa558afbd',
								'operator' => '==',
								'value' => '1',
							),
							array (
								'field' => 'field_583f359fc3be7',
								'operator' => '==',
								'value' => '1',
							),
						),
					),
					'wrapper' => array (
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'default_value' => 10,
					'placeholder' => '',
					'prepend' => '',
					'append' => 'Rows',
					'min' => '',
					'max' => '',
					'step' => '',
				),
				array (
					'key' => 'field_583f3c0e05158',
					'label' => __('Pagination Position', 'ultimateazon2'),
					'name' => 'pagination_position',
					'type' => 'select',
					'instructions' => __('The pagination displays to the center by default. It can be placed to the left, center, or right.', 'ultimateazon2'),
					'required' => 0,
					'conditional_logic' => array (
						array (
							array (
								'field' => 'field_583dfa558afbd',
								'operator' => '==',
								'value' => '1',
							),
							array (
								'field' => 'field_583f359fc3be7',
								'operator' => '==',
								'value' => '1',
							),
						),
					),
					'wrapper' => array (
						'width' => '50',
						'class' => '',
						'id' => '',
					),
					'choices' => array (
						'left' => __('Left', 'ultimateazon2'),
						'center' => __('Center', 'ultimateazon2'),
						'right' => __('Right', 'ultimateazon2'),
					),
					'default_value' => array (
						0 => 'center',
					),
					'allow_null' => 0,
					'multiple' => 0,
					'ui' => 0,
					'ajax' => 0,
					'return_format' => 'value',
					'placeholder' => '',
				),
				array (
					'key' => 'field_583f3b1a9884f',
					'label' => __('Table Pagination - Pages Displayed Limit', 'ultimateazon2'),
					'name' => 'pages_displayed_limit',
					'type' => 'number',
					'instructions' => __('This setting does not limit the number of pages in your table, it only limits the number of pages that are displayed in the pagination at the same time. Users can still access every page of your full table. This is useful for tables a large amount of rows.', 'ultimateazon2'),
					'required' => 0,
					'conditional_logic' => array (
						array (
							array (
								'field' => 'field_583f359fc3be7',
								'operator' => '==',
								'value' => '1',
							),
							array (
								'field' => 'field_583dfa558afbd',
								'operator' => '==',
								'value' => '1',
							),
						),
					),
					'wrapper' => array (
						'width' => '50',
						'class' => '',
						'id' => '',
					),
					'default_value' => '',
					'placeholder' => '',
					'prepend' => '',
					'append' => 'Pages',
					'min' => '',
					'max' => '',
					'step' => '',
				),
				array (
					'key' => 'field_583f3530c854c',
					'label' => __('Table Memory', 'ultimateazon2'),
					'name' => '',
					'type' => 'message',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => array (
						array (
							array (
								'field' => 'field_583f359fc3be7',
								'operator' => '==',
								'value' => '1',
							),
						),
					),
					'wrapper' => array (
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'message' => '',
					'new_lines' => 'wpautop',
					'esc_html' => 0,
				),
				array (
					'key' => 'field_583dfab28afbe',
					'label' => __('Enable Table Memory', 'ultimateazon2'),
					'name' => 'enable_table_memory',
					'type' => 'true_false',
					'instructions' => __('Enabling this option will store the state of your table in the visitor\'s browser local storage, so the next time they visit the page the table will remember the sorting, filtering, and pagination that the visitor left it as.', 'ultimateazon2'),
					'required' => 0,
					'conditional_logic' => array (
						array (
							array (
								'field' => 'field_583f359fc3be7',
								'operator' => '==',
								'value' => '1',
							),
						),
					),
					'wrapper' => array (
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'message' => __('Check this box to enable table memory for your visitors.', 'ultimateazon2'),
					'default_value' => 0,
				),
				array (
					'key' => 'field_583f353ec854d',
					'label' => __('Table Custom Responsive Settings', 'ultimateazon2'),
					'name' => '',
					'type' => 'message',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => array (
						array (
							array (
								'field' => 'field_583f359fc3be7',
								'operator' => '==',
								'value' => '1',
							),
						),
					),
					'wrapper' => array (
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'message' => '',
					'new_lines' => 'wpautop',
					'esc_html' => 0,
				),
				array (
					'key' => 'field_583df3d8a93a3',
					'label' => __('Base Responsive Breakpoints off the Width of the Container', 'ultimateazon2'),
					'name' => 'breakpoints_off_container',
					'type' => 'true_false',
					'instructions' => __('By default the breakpoints are based off the width of the window. Check this box to base the breakpoints off the width of the container the table resides in.', 'ultimateazon2'),
					'required' => 0,
					'conditional_logic' => array (
						array (
							array (
								'field' => 'field_583f359fc3be7',
								'operator' => '==',
								'value' => '1',
							),
						),
					),
					'wrapper' => array (
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'message' => __('Check this box to base breakpoints off the parent container width.', 'ultimateazon2'),
					'default_value' => 0,
				),
				array (
					'key' => 'field_583df083fe439',
					'label' => __('Custom Breakpoints', 'ultimateazon2'),
					'name' => 'custom_breakpoints',
					'type' => 'true_false',
					'instructions' => __('This setting is if you want to use custom breakpoints for your responsive tables. If you are not familiar with responsive breakpoints, it is best to leave this unchecked.', 'ultimateazon2'),
					'required' => 0,
					'conditional_logic' => array (
						array (
							array (
								'field' => 'field_583f359fc3be7',
								'operator' => '==',
								'value' => '1',
							),
						),
					),
					'wrapper' => array (
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'message' => __('Check this box to use custom breakpoints for this table.', 'ultimateazon2'),
					'default_value' => 0,
				),
				array (
					'key' => 'field_583df0fbfe43a',
					'label' => __('Extra Small Breakpoint', 'ultimateazon2'),
					'name' => 'extra_small_breakpoint',
					'type' => 'number',
					'instructions' => __('This breakpoint is for extra small screens.', 'ultimateazon2'),
					'required' => 0,
					'conditional_logic' => array (
						array (
							array (
								'field' => 'field_583df083fe439',
								'operator' => '==',
								'value' => '1',
							),
							array (
								'field' => 'field_583f359fc3be7',
								'operator' => '==',
								'value' => '1',
							),
						),
					),
					'wrapper' => array (
						'width' => '25',
						'class' => '',
						'id' => '',
					),
					'default_value' => '',
					'placeholder' => '',
					'prepend' => '',
					'append' => 'px',
					'min' => '',
					'max' => '',
					'step' => '',
				),
				array (
					'key' => 'field_583df170fe43c',
					'label' => __('Small Breakpoint', 'ultimateazon2'),
					'name' => 'small_breakpoint',
					'type' => 'number',
					'instructions' => __('This breakpoint is for small screens.', 'ultimateazon2'),
					'required' => 0,
					'conditional_logic' => array (
						array (
							array (
								'field' => 'field_583df083fe439',
								'operator' => '==',
								'value' => '1',
							),
							array (
								'field' => 'field_583f359fc3be7',
								'operator' => '==',
								'value' => '1',
							),
						),
					),
					'wrapper' => array (
						'width' => '25',
						'class' => '',
						'id' => '',
					),
					'default_value' => '',
					'placeholder' => '',
					'prepend' => '',
					'append' => 'px',
					'min' => '',
					'max' => '',
					'step' => '',
				),
				array (
					'key' => 'field_583df17ffe43d',
					'label' => __('Medium Breakpoint', 'ultimateazon2'),
					'name' => 'medium_breakpoint',
					'type' => 'number',
					'instructions' => __('This breakpoint is for medium screens.', 'ultimateazon2'),
					'required' => 0,
					'conditional_logic' => array (
						array (
							array (
								'field' => 'field_583df083fe439',
								'operator' => '==',
								'value' => '1',
							),
							array (
								'field' => 'field_583f359fc3be7',
								'operator' => '==',
								'value' => '1',
							),
						),
					),
					'wrapper' => array (
						'width' => '25',
						'class' => '',
						'id' => '',
					),
					'default_value' => '',
					'placeholder' => '',
					'prepend' => '',
					'append' => 'px',
					'min' => '',
					'max' => '',
					'step' => '',
				),
				array (
					'key' => 'field_583df19cfe43e',
					'label' => __('Large Breakpoint', 'ultimateazon2'),
					'name' => 'large_breakpoint',
					'type' => 'number',
					'instructions' => __('This breakpoint is for large screens.', 'ultimateazon2'),
					'required' => 0,
					'conditional_logic' => array (
						array (
							array (
								'field' => 'field_583df083fe439',
								'operator' => '==',
								'value' => '1',
							),
							array (
								'field' => 'field_583f359fc3be7',
								'operator' => '==',
								'value' => '1',
							),
						),
					),
					'wrapper' => array (
						'width' => '25',
						'class' => '',
						'id' => '',
					),
					'default_value' => '',
					'placeholder' => '',
					'prepend' => '',
					'append' => 'px',
					'min' => '',
					'max' => '',
					'step' => '',
				),
				array (
					'key' => 'field_583df297270c6',
					'label' => __('Expand the First Row by Default', 'ultimateazon2'),
					'name' => 'expand_the_first_row_by_default',
					'type' => 'true_false',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => array (
						array (
							array (
								'field' => 'field_583f359fc3be7',
								'operator' => '==',
								'value' => '1',
							),
						),
					),
					'wrapper' => array (
						'width' => '50',
						'class' => 'djn-clear-all',
						'id' => '',
					),
					'message' => __('Check this box to expand the first row by default', 'ultimateazon2'),
					'default_value' => 0,
				),
				array (
					'key' => 'field_583df2d5270c7',
					'label' => __('Expand All Rows by Default', 'ultimateazon2'),
					'name' => 'expand_all_rows_by_default',
					'type' => 'true_false',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => array (
						array (
							array (
								'field' => 'field_583f359fc3be7',
								'operator' => '==',
								'value' => '1',
							),
						),
					),
					'wrapper' => array (
						'width' => '50',
						'class' => '',
						'id' => '',
					),
					'message' => __('Check this box to expand all rows by default', 'ultimateazon2'),
					'default_value' => 0,
				),
				array (
					'key' => 'field_583df35aa93a2',
					'label' => __('Hide the Toggle Button', 'ultimateazon2'),
					'name' => 'hide_the_toggle_button',
					'type' => 'true_false',
					'instructions' => __('Check this box if you wish to hide the row toggle button. This will not prevent the rows form being opened and closed if clicked.', 'ultimateazon2'),
					'required' => 0,
					'conditional_logic' => array (
						array (
							array (
								'field' => 'field_583f359fc3be7',
								'operator' => '==',
								'value' => '1',
							),
						),
					),
					'wrapper' => array (
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'message' => __('Check this box to hide the toggle button on each row.', 'ultimateazon2'),
					'default_value' => 0,
				),
			),
			'location' => array (
				array (
					array (
						'param' => 'post_type',
						'operator' => '==',
						'value' => 'galfram_table',
					),
				),
			),
			'menu_order' => 50,
			'position' => 'normal',
			'style' => 'default',
			'label_placement' => 'top',
			'instruction_placement' => 'label',
			'hide_on_screen' => '',
			'active' => 1,
			'description' => '',
		));








		/*********************************
		* Add Stylizer Fields ************
		*********************************/

		// $stylizer_active = galfram_extension_checker('galaxyframework-child-stylizer');

		// if ( $stylizer_active ) :



			acf_add_local_field_group(array (
				'key' => 'group_58a33ad6d9ea3',
				'title' => __('Responsive Table Styles', 'ultimateazon2'),
				'fields' => array (
					array (
						'key' => 'field_58975613cdbfa',
						'label' => __('Table Global Styles', 'ultimateazon2'),
						'name' => '',
						'type' => 'message',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '',
							'class' => 'djn-section-heading',
							'id' => '',
						),
						'message' => '',
						'new_lines' => 'wpautop',
						'esc_html' => 0,
					),
					array (
						'key' => 'field_5897549c4c266',
						'label' => __('Enable Table Wrapper Border', 'ultimateazon2'),
						'name' => 'enable_table_wrapper_border',
						'type' => 'true_false',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'message' => __('Check here to enable a border around the whole table', 'ultimateazon2'),
						'default_value' => 0,
					),
					array (
						'key' => 'field_589535a8ce0e4',
						'label' => __('Table Wrapper Border Width', 'ultimateazon2'),
						'name' => 'table_wrapper_border_width',
						'type' => 'number',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_5897549c4c266',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '34',
							'class' => '',
							'id' => '',
						),
						'default_value' => 1,
						'placeholder' => '',
						'prepend' => '',
						'append' => 'px',
						'min' => '',
						'max' => '',
						'step' => '',
					),
					array (
						'key' => 'field_589535d6ce0e5',
						'label' => __('Table Wrapper Border Style', 'ultimateazon2'),
						'name' => 'table_wrapper_border_style',
						'type' => 'select',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_5897549c4c266',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '33',
							'class' => '',
							'id' => '',
						),
						'choices' => array (
							'solid' => __('solid', 'ultimateazon2'),
							'dotted' => __('dotted', 'ultimateazon2'),
							'dashed' => __('dashed', 'ultimateazon2'),
						),
						'default_value' => array (
							0 => 'solid',
						),
						'allow_null' => 0,
						'multiple' => 0,
						'ui' => 0,
						'ajax' => 0,
						'return_format' => 'value',
						'placeholder' => '',
					),
					array (
						'key' => 'field_58953accce0e6',
						'label' => __('Table Wrapper Border Color', 'ultimateazon2'),
						'name' => 'table_wrapper_border_color',
						'type' => 'color_picker',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_5897549c4c266',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '33',
							'class' => '',
							'id' => '',
						),
						'default_value' => '#dedede',
					),
					array (
						'key' => 'field_589756713b990',
						'label' => __('Table Header Styles', 'ultimateazon2'),
						'name' => '',
						'type' => 'message',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '',
							'class' => 'djn-section-heading',
							'id' => '',
						),
						'message' => '',
						'new_lines' => 'wpautop',
						'esc_html' => 0,
					),
					array (
						'key' => 'field_5893ea8c652ee',
						'label' => __('Enable Table Header Styles', 'ultimateazon2'),
						'name' => 'enable_table_header_styles',
						'type' => 'true_false',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'message' => __('Check here to enable the table header styles', 'ultimateazon2'),
						'default_value' => 0,
					),
					array (
						'key' => 'field_589753f085ea1',
						'label' => __('Table Header Background Color', 'ultimateazon2'),
						'name' => 'table_header_bg_color',
						'type' => 'color_picker',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_5893ea8c652ee',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '25',
							'class' => '',
							'id' => '',
						),
						'default_value' => '',
					),
					array (
						'key' => 'field_5893f557652f1',
						'label' => __('Table Header Color', 'ultimateazon2'),
						'name' => 'table_header_color',
						'type' => 'color_picker',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_5893ea8c652ee',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '25',
							'class' => '',
							'id' => '',
						),
						'default_value' => '#000000',
					),
					array (
						'key' => 'field_5893eaa4652ef',
						'label' => __('Table Header Font Size', 'ultimateazon2'),
						'name' => 'table_header_font_size',
						'type' => 'number',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_5893ea8c652ee',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '25',
							'class' => '',
							'id' => '',
						),
						'default_value' => 14,
						'placeholder' => '',
						'prepend' => '',
						'append' => 'px',
						'min' => '',
						'max' => '',
						'step' => '',
					),
					array (
						'key' => 'field_5893eb25652f0',
						'label' => __('Table Header Line Height', 'ultimateazon2'),
						'name' => 'table_header_line_height',
						'type' => 'number',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_5893ea8c652ee',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '25',
							'class' => '',
							'id' => '',
						),
						'default_value' => 16,
						'placeholder' => '',
						'prepend' => '',
						'append' => 'px',
						'min' => '',
						'max' => '',
						'step' => '',
					),
					array (
						'key' => 'field_5893ffea83af9',
						'label' => __('Table Header Padding Top', 'ultimateazon2'),
						'name' => 'th_padding_top',
						'type' => 'number',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_5893ea8c652ee',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '25',
							'class' => 'djn-clear-all',
							'id' => '',
						),
						'default_value' => 5,
						'placeholder' => '',
						'prepend' => '',
						'append' => 'px',
						'min' => '',
						'max' => '',
						'step' => '',
					),
					array (
						'key' => 'field_5894000d83afa',
						'label' => __('Table Header Padding Right', 'ultimateazon2'),
						'name' => 'th_padding_right',
						'type' => 'number',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_5893ea8c652ee',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '25',
							'class' => '',
							'id' => '',
						),
						'default_value' => 5,
						'placeholder' => '',
						'prepend' => '',
						'append' => 'px',
						'min' => '',
						'max' => '',
						'step' => '',
					),
					array (
						'key' => 'field_5894003b83afb',
						'label' => __('Table Header Padding Bottom', 'ultimateazon2'),
						'name' => 'th_padding_bottom',
						'type' => 'number',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_5893ea8c652ee',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '25',
							'class' => '',
							'id' => '',
						),
						'default_value' => 5,
						'placeholder' => '',
						'prepend' => '',
						'append' => 'px',
						'min' => '',
						'max' => '',
						'step' => '',
					),
					array (
						'key' => 'field_5894007783afc',
						'label' => __('Table Header Padding Left', 'ultimateazon2'),
						'name' => 'th_padding_left',
						'type' => 'number',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_5893ea8c652ee',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '25',
							'class' => '',
							'id' => '',
						),
						'default_value' => 5,
						'placeholder' => '',
						'prepend' => '',
						'append' => 'px',
						'min' => '',
						'max' => '',
						'step' => '',
					),
					array (
						'key' => 'field_589756aa0ac13',
						'label' => __('Enable Table Header Border', 'ultimateazon2'),
						'name' => 'enable_table_header_border',
						'type' => 'true_false',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_5893ea8c652ee',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'message' => __('Check here to enable a border on your table header', 'ultimateazon2'),
						'default_value' => 0,
					),
					array (
						'key' => 'field_589754564c263',
						'label' => __('Table Header Border Width', 'ultimateazon2'),
						'name' => 'table_header_border_width',
						'type' => 'number',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_589756aa0ac13',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_5893ea8c652ee',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '25',
							'class' => '',
							'id' => '',
						),
						'default_value' => 5,
						'placeholder' => '',
						'prepend' => '',
						'append' => 'px',
						'min' => '',
						'max' => '',
						'step' => '',
					),
					array (
						'key' => 'field_5897546f4c264',
						'label' => __('Table Header Border Style', 'ultimateazon2'),
						'name' => 'table_header_border_style',
						'type' => 'select',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_589756aa0ac13',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_5893ea8c652ee',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '25',
							'class' => '',
							'id' => '',
						),
						'choices' => array (
							'solid' => 'solid',
							'dotted' => 'dotted',
							'dashed' => 'dashed',
						),
						'default_value' => array (
							0 => 'solid',
						),
						'allow_null' => 0,
						'multiple' => 0,
						'ui' => 0,
						'ajax' => 0,
						'return_format' => 'value',
						'placeholder' => '',
					),
					array (
						'key' => 'field_5897548a4c265',
						'label' => __('Table Header Border Color', 'ultimateazon2'),
						'name' => 'table_header_border_color',
						'type' => 'color_picker',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_589756aa0ac13',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_5893ea8c652ee',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '25',
							'class' => '',
							'id' => '',
						),
						'default_value' => '#dedede',
					),
					array (
						'key' => 'field_589756cb0ac14',
						'label' => __('Table Header Border Sides', 'ultimateazon2'),
						'name' => 'table_header_border_sides',
						'type' => 'checkbox',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_589756aa0ac13',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_5893ea8c652ee',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '25',
							'class' => '',
							'id' => '',
						),
						'choices' => array (
							'top' => __('top', 'ultimateazon2'),
							'right' => __('right', 'ultimateazon2'),
							'bottom' => __('bottom', 'ultimateazon2'),
							'left' => __('left', 'ultimateazon2'),
						),
						'default_value' => array (
							0 => 'bottom',
						),
						'layout' => 'vertical',
						'toggle' => 0,
						'return_format' => 'value',
					),
					array (
						'key' => 'field_589757dc3af19',
						'label' => __('Table Row Styles', 'ultimateazon2'),
						'name' => '',
						'type' => 'message',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '',
							'class' => 'djn-section-heading',
							'id' => '',
						),
						'message' => '',
						'new_lines' => 'wpautop',
						'esc_html' => 0,
					),
					array (
						'key' => 'field_589757eb3af1a',
						'label' => __('Enable Table Row Styles', 'ultimateazon2'),
						'name' => 'enable_table_row_styles',
						'type' => 'true_false',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'message' => __('Check here to enable table row styles', 'ultimateazon2'),
						'default_value' => 0,
					),
					array (
						'key' => 'field_5899d97d031f9',
						'label' => __('Odd Row Background Color', 'ultimateazon2'),
						'name' => 'odd_row_background_color',
						'type' => 'color_picker',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_589757eb3af1a',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '50',
							'class' => '',
							'id' => '',
						),
						'default_value' => '',
					),
					array (
						'key' => 'field_5899d961031f8',
						'label' => __('Even Row Background Color', 'ultimateazon2'),
						'name' => 'even_row_background_color',
						'type' => 'color_picker',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_589757eb3af1a',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '50',
							'class' => '',
							'id' => '',
						),
						'default_value' => '',
					),
					array (
						'key' => 'field_5899daad73092',
						'label' => __('Enable Table Row Bottom Border', 'ultimateazon2'),
						'name' => 'enable_table_row_bottom_border',
						'type' => 'true_false',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_589757eb3af1a',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'message' => __('Check Here to enable the table row bottom border styles', 'ultimateazon2'),
						'default_value' => 0,
					),
					array (
						'key' => 'field_5899df061be97',
						'label' => __('Table Row Bottom Border Width', 'ultimateazon2'),
						'name' => 'table_row_bottom_border_width',
						'type' => 'number',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_589757eb3af1a',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_5899daad73092',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '33',
							'class' => '',
							'id' => '',
						),
						'default_value' => 1,
						'placeholder' => '',
						'prepend' => '',
						'append' => 'px',
						'min' => '',
						'max' => '',
						'step' => '',
					),
					array (
						'key' => 'field_5899df351be98',
						'label' => __('Table Row Bottom Border Style', 'ultimateazon2'),
						'name' => 'table_row_bottom_border_style',
						'type' => 'select',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_589757eb3af1a',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_5899daad73092',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '33',
							'class' => '',
							'id' => '',
						),
						'choices' => array (
							'solid' => __('solid', 'ultimateazon2'),
							'dotted' => __('dotted', 'ultimateazon2'),
							'dashed' => __('dashed', 'ultimateazon2'),
						),
						'default_value' => array (
							0 => 'solid',
						),
						'allow_null' => 0,
						'multiple' => 0,
						'ui' => 0,
						'ajax' => 0,
						'return_format' => 'value',
						'placeholder' => '',
					),
					array (
						'key' => 'field_5899df4d1be99',
						'label' => __('Table Row Bottom Border Color', 'ultimateazon2'),
						'name' => 'table_row_bottom_border_color',
						'type' => 'color_picker',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_589757eb3af1a',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_5899daad73092',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '34',
							'class' => '',
							'id' => '',
						),
						'default_value' => '#dedede',
					),
					array (
						'key' => 'field_5899df7967928',
						'label' => __('Enable Table Row Font Styles', 'ultimateazon2'),
						'name' => 'enable_table_row_font_styles',
						'type' => 'true_false',
						'instructions' => __('(This may be over-written by other font styles when using the WYSIWYG or other field types)', 'ultimateazon2'),
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_589757eb3af1a',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'message' => __('Check This to enable the table row font styles', 'ultimateazon2'),
						'default_value' => 0,
					),
					array (
						'key' => 'field_5899ef4894554',
						'label' => __('Table Row Global Font Size', 'ultimateazon2'),
						'name' => 'table_row_global_font_size',
						'type' => 'number',
						'instructions' => __('(This may be over-written by other font styles when using the WYSIWYG or other field types)', 'ultimateazon2'),
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_589757eb3af1a',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_5899df7967928',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '50',
							'class' => '',
							'id' => '',
						),
						'default_value' => 14,
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'min' => '',
						'max' => '',
						'step' => '',
					),
					array (
						'key' => 'field_5899ef9f94555',
						'label' => __('Table Row Global Line Height', 'ultimateazon2'),
						'name' => 'table_row_global_line_height',
						'type' => 'number',
						'instructions' => __('(This may be over-written by other font styles when using the WYSIWYG or other field types)', 'ultimateazon2'),
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_589757eb3af1a',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_5899df7967928',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '50',
							'class' => '',
							'id' => '',
						),
						'default_value' => 16,
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'min' => '',
						'max' => '',
						'step' => '',
					),
					array (
						'key' => 'field_589a170a7aee0',
						'label' => __('Table Row Font Color Odd Rows', 'ultimateazon2'),
						'name' => 'table_row_font_color_odd_rows',
						'type' => 'color_picker',
						'instructions' => __('(This may be over-written by other font styles when using the WYSIWYG or other field types)', 'ultimateazon2'),
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_589757eb3af1a',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_5899df7967928',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '25',
							'class' => '',
							'id' => '',
						),
						'default_value' => '#000000',
					),
					array (
						'key' => 'field_3x54c6v7b8v5c',
						'label' => __('Table Row Link Color Odd Rows', 'ultimateazon2'),
						'name' => 'table_row_link_color_odd_rows',
						'type' => 'color_picker',
						'instructions' => __('(This may be over-written by other font styles when using the WYSIWYG or other field types)', 'ultimateazon2'),
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_589757eb3af1a',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_5899df7967928',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '25',
							'class' => '',
							'id' => '',
						),
						'default_value' => '#000000',
					),
					array (
						'key' => 'field_kjnkb9878y78y78yb',
						'label' => __('Table Row Button Background Color Odd Rows', 'ultimateazon2'),
						'name' => 'table_row_button_bg_color_odd_rows',
						'type' => 'color_picker',
						'instructions' => __('(This may be over-written by other font styles when using the WYSIWYG or other field types)', 'ultimateazon2'),
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_589757eb3af1a',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_5899df7967928',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '25',
							'class' => '',
							'id' => '',
						),
						'default_value' => '#000000',
					),
					array (
						'key' => 'field_i7yb8v67t756r',
						'label' => __('Table Row Button Text Color Odd Rows', 'ultimateazon2'),
						'name' => 'table_row_button_text_color_odd_rows',
						'type' => 'color_picker',
						'instructions' => __('(This may be over-written by other font styles when using the WYSIWYG or other field types)', 'ultimateazon2'),
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_589757eb3af1a',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_5899df7967928',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '25',
							'class' => '',
							'id' => '',
						),
						'default_value' => '#000000',
					),
					array (
						'key' => 'field_5899efb094556',
						'label' => __('Table Row Font Color Even Rows', 'ultimateazon2'),
						'name' => 'table_row_font_color_even_rows',
						'type' => 'color_picker',
						'instructions' => __('(This may be over-written by other font styles when using the WYSIWYG or other field types)', 'ultimateazon2'),
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_589757eb3af1a',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_5899df7967928',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '25',
							'class' => '',
							'id' => '',
						),
						'default_value' => '#000000',
					),
					array (
						'key' => 'field_20ain2098nx30x8n30x39n3',
						'label' => __('Table Row Link Color Even Rows', 'ultimateazon2'),
						'name' => 'table_row_link_color_even_rows',
						'type' => 'color_picker',
						'instructions' => __('(This may be over-written by other font styles when using the WYSIWYG or other field types)', 'ultimateazon2'),
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_589757eb3af1a',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_5899df7967928',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '25',
							'class' => '',
							'id' => '',
						),
						'default_value' => '#000000',
					),
					array (
						'key' => 'field_z298i30309304',
						'label' => __('Table Row Button Background Color Even Rows', 'ultimateazon2'),
						'name' => 'table_row_button_bg_color_even_rows',
						'type' => 'color_picker',
						'instructions' => __('(This may be over-written by other font styles when using the WYSIWYG or other field types)', 'ultimateazon2'),
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_589757eb3af1a',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_5899df7967928',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '25',
							'class' => '',
							'id' => '',
						),
						'default_value' => '#000000',
					),
					array (
						'key' => 'field_wrtv35t45t425',
						'label' => __('Table Row Button Text Color Even Rows', 'ultimateazon2'),
						'name' => 'table_row_button_text_color_even_rows',
						'type' => 'color_picker',
						'instructions' => __('(This may be over-written by other font styles when using the WYSIWYG or other field types)', 'ultimateazon2'),
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_589757eb3af1a',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_5899df7967928',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '25',
							'class' => '',
							'id' => '',
						),
						'default_value' => '#000000',
					),
					array (
						'key' => 'field_589a173174147',
						'label' => __('Enable Font Styles for Special Field Types', 'ultimateazon2'),
						'name' => 'enable_font_styles_for_field_types',
						'type' => 'true_false',
						'instructions' => __('These apply to these special field type: ASIN, Brand, Model, UPC, Features, Warranty, Price, Lowest Used Price, Lowest New Price, Star Rating, Yes/No', 'ultimateazon2'),
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_589757eb3af1a',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_5899df7967928',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'message' => '',
						'default_value' => 0,
					),
					array (
						'key' => 'field_589a24e769fcc',
						'label' => __('Enable ASIN Field Styles', 'ultimateazon2'),
						'name' => 'enable_asin_field_styles',
						'type' => 'true_false',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_589757eb3af1a',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_5899df7967928',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_589a173174147',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'message' => __('Check here to enable "ASIN" field styles', 'ultimateazon2'),
						'default_value' => 0,
					),
					array (
						'key' => 'field_589f029cc4f1e',
						'label' => __('ASIN Font Size', 'ultimateazon2'),
						'name' => 'asin_font_size',
						'type' => 'number',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_589757eb3af1a',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_5899df7967928',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_589a173174147',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_589a24e769fcc',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '33',
							'class' => '',
							'id' => '',
						),
						'default_value' => 14,
						'placeholder' => '',
						'prepend' => '',
						'append' => 'px',
						'min' => '',
						'max' => '',
						'step' => '',
					),
					array (
						'key' => 'field_58a3392e30c2d',
						'label' => __('ASIN Line Height', 'ultimateazon2'),
						'name' => 'asin_line_height',
						'type' => 'number',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_589757eb3af1a',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_5899df7967928',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_589a173174147',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_589a24e769fcc',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '33',
							'class' => '',
							'id' => '',
						),
						'default_value' => 16,
						'placeholder' => '',
						'prepend' => '',
						'append' => 'px',
						'min' => '',
						'max' => '',
						'step' => '',
					),
					array (
						'key' => 'field_58a33adf0678f',
						'label' => __('ASIN Text Color', 'ultimateazon2'),
						'name' => 'asin_text_color',
						'type' => 'color_picker',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_589757eb3af1a',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_5899df7967928',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_589a173174147',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_589a24e769fcc',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '34',
							'class' => '',
							'id' => '',
						),
						'default_value' => '#000',
					),
					array (
						'key' => 'field_589a24fe69fcd',
						'label' => __('Enable Brand Field Styles', 'ultimateazon2'),
						'name' => 'enable_brand_field_styles',
						'type' => 'true_false',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_589757eb3af1a',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_5899df7967928',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_589a173174147',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'message' => __('Check here to enable "brand" field styles', 'ultimateazon2'),
						'default_value' => 0,
					),
					array (
						'key' => 'field_589f02d4c4f20',
						'label' => __('Brand Font Size', 'ultimateazon2'),
						'name' => 'brand_font_size',
						'type' => 'number',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_589757eb3af1a',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_5899df7967928',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_589a173174147',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_589a24fe69fcd',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '33',
							'class' => '',
							'id' => '',
						),
						'default_value' => 14,
						'placeholder' => '',
						'prepend' => '',
						'append' => 'px',
						'min' => '',
						'max' => '',
						'step' => '',
					),
					array (
						'key' => 'field_58a3393d30c2e',
						'label' => __('Brand Line Height', 'ultimateazon2'),
						'name' => 'brand_line_height',
						'type' => 'number',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_589757eb3af1a',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_5899df7967928',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_589a173174147',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_589a24fe69fcd',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '33',
							'class' => '',
							'id' => '',
						),
						'default_value' => 16,
						'placeholder' => '',
						'prepend' => '',
						'append' => 'px',
						'min' => '',
						'max' => '',
						'step' => '',
					),
					array (
						'key' => 'field_58a33b0606790',
						'label' => __('Brand Text Color', 'ultimateazon2'),
						'name' => 'brand_text_color',
						'type' => 'color_picker',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_589757eb3af1a',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_5899df7967928',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_589a173174147',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_589a24fe69fcd',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '34',
							'class' => '',
							'id' => '',
						),
						'default_value' => '#000',
					),
					array (
						'key' => 'field_589a250969fce',
						'label' => __('Enable Model Field Styles', 'ultimateazon2'),
						'name' => 'enable_model_field_styles',
						'type' => 'true_false',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_589757eb3af1a',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_5899df7967928',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_589a173174147',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'message' => __('Check here to enable "model" field styles', 'ultimateazon2'),
						'default_value' => 0,
					),
					array (
						'key' => 'field_589f02e7c4f21',
						'label' => __('Model Font Size', 'ultimateazon2'),
						'name' => 'model_font_size',
						'type' => 'number',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_589757eb3af1a',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_5899df7967928',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_589a173174147',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_589a250969fce',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '33',
							'class' => '',
							'id' => '',
						),
						'default_value' => 14,
						'placeholder' => '',
						'prepend' => '',
						'append' => 'px',
						'min' => '',
						'max' => '',
						'step' => '',
					),
					array (
						'key' => 'field_58a3395d30c2f',
						'label' => __('Model Line Height', 'ultimateazon2'),
						'name' => 'model_line_height',
						'type' => 'number',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_589757eb3af1a',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_5899df7967928',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_589a173174147',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_589a250969fce',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '33',
							'class' => '',
							'id' => '',
						),
						'default_value' => 16,
						'placeholder' => '',
						'prepend' => '',
						'append' => 'px',
						'min' => '',
						'max' => '',
						'step' => '',
					),
					array (
						'key' => 'field_58a33b1c06791',
						'label' => __('Model Text Color', 'ultimateazon2'),
						'name' => 'model_text_color',
						'type' => 'color_picker',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_589757eb3af1a',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_5899df7967928',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_589a173174147',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_589a250969fce',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '34',
							'class' => '',
							'id' => '',
						),
						'default_value' => '#000',
					),
					array (
						'key' => 'field_589a251069fcf',
						'label' => __('Enable UPC Field Styles', 'ultimateazon2'),
						'name' => 'enable_upc_field_styles',
						'type' => 'true_false',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_589757eb3af1a',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_5899df7967928',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_589a173174147',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'message' => __('Check here to enable "UPC" field styles', 'ultimateazon2'),
						'default_value' => 0,
					),
					array (
						'key' => 'field_58a20fcf80ff0',
						'label' => __('UPC Font Size', 'ultimateazon2'),
						'name' => 'upc_font_size',
						'type' => 'number',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_589757eb3af1a',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_5899df7967928',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_589a173174147',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_589a251069fcf',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '33',
							'class' => '',
							'id' => '',
						),
						'default_value' => 14,
						'placeholder' => '',
						'prepend' => '',
						'append' => 'px',
						'min' => '',
						'max' => '',
						'step' => '',
					),
					array (
						'key' => 'field_58a3396830c30',
						'label' => __('UPC Line Height', 'ultimateazon2'),
						'name' => 'upc_line_height',
						'type' => 'number',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_589757eb3af1a',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_5899df7967928',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_589a173174147',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_589a251069fcf',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '33',
							'class' => '',
							'id' => '',
						),
						'default_value' => 16,
						'placeholder' => '',
						'prepend' => '',
						'append' => 'px',
						'min' => '',
						'max' => '',
						'step' => '',
					),
					array (
						'key' => 'field_58a33b3b06792',
						'label' => __('UPC Text Color', 'ultimateazon2'),
						'name' => 'upc_text_color',
						'type' => 'color_picker',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_589757eb3af1a',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_5899df7967928',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_589a173174147',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_589a251069fcf',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '34',
							'class' => '',
							'id' => '',
						),
						'default_value' => '#000',
					),
					array (
						'key' => 'field_589a251b69fd0',
						'label' => __('Enable Features Field Styles', 'ultimateazon2'),
						'name' => 'enable_features_field_styles',
						'type' => 'true_false',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_589757eb3af1a',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_5899df7967928',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_589a173174147',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'message' => __('Check here to enable "features" field styles', 'ultimateazon2'),
						'default_value' => 0,
					),
					array (
						'key' => 'field_58a20fe980ff1',
						'label' => __('Features Font Size', 'ultimateazon2'),
						'name' => 'features_font_size',
						'type' => 'number',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_589757eb3af1a',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_5899df7967928',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_589a173174147',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_589a251b69fd0',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '33',
							'class' => '',
							'id' => '',
						),
						'default_value' => 12,
						'placeholder' => '',
						'prepend' => '',
						'append' => 'px',
						'min' => '',
						'max' => '',
						'step' => '',
					),
					array (
						'key' => 'field_58a3397130c31',
						'label' => __('Features Line Height', 'ultimateazon2'),
						'name' => 'features_line_height',
						'type' => 'number',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_589757eb3af1a',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_5899df7967928',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_589a173174147',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_589a251b69fd0',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '33',
							'class' => '',
							'id' => '',
						),
						'default_value' => 14,
						'placeholder' => '',
						'prepend' => '',
						'append' => 'px',
						'min' => '',
						'max' => '',
						'step' => '',
					),
					array (
						'key' => 'field_58a33bc226088',
						'label' => __('Features Text Color', 'ultimateazon2'),
						'name' => 'features_text_color',
						'type' => 'color_picker',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_589757eb3af1a',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_5899df7967928',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_589a173174147',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_589a251b69fd0',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '34',
							'class' => '',
							'id' => '',
						),
						'default_value' => '#000',
					),
					array (
						'key' => 'field_589a252469fd1',
						'label' => __('Enable Warranty Field Styles', 'ultimateazon2'),
						'name' => 'enable_warranty_field_styles',
						'type' => 'true_false',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_589757eb3af1a',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_5899df7967928',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_589a173174147',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'message' => __('Check here to enable "warranty" field styles', 'ultimateazon2'),
						'default_value' => 0,
					),
					array (
						'key' => 'field_58a2199ff339e',
						'label' => __('Warranty Font Size', 'ultimateazon2'),
						'name' => 'warranty_font_size',
						'type' => 'number',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_589757eb3af1a',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_5899df7967928',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_589a173174147',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_589a252469fd1',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '33',
							'class' => '',
							'id' => '',
						),
						'default_value' => 14,
						'placeholder' => '',
						'prepend' => '',
						'append' => 'px',
						'min' => '',
						'max' => '',
						'step' => '',
					),
					array (
						'key' => 'field_58a3397d30c32',
						'label' => __('Warranty Line Height', 'ultimateazon2'),
						'name' => 'warranty_line_height',
						'type' => 'number',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_589757eb3af1a',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_5899df7967928',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_589a173174147',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_589a252469fd1',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '33',
							'class' => '',
							'id' => '',
						),
						'default_value' => 16,
						'placeholder' => '',
						'prepend' => '',
						'append' => 'px',
						'min' => '',
						'max' => '',
						'step' => '',
					),
					array (
						'key' => 'field_58a33bd626089',
						'label' => __('Warranty Text Color', 'ultimateazon2'),
						'name' => 'warranty_text_color',
						'type' => 'color_picker',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_589757eb3af1a',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_5899df7967928',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_589a173174147',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_589a252469fd1',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '34',
							'class' => '',
							'id' => '',
						),
						'default_value' => '#000',
					),
					array (
						'key' => 'field_589a255e69fd6',
						'label' => __('Enable Star Rating Field Styles', 'ultimateazon2'),
						'name' => 'enable_star_rating_field_styles',
						'type' => 'true_false',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_589757eb3af1a',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_5899df7967928',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_589a173174147',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'message' => __('Check here to enable "star rating" field styles', 'ultimateazon2'),
						'default_value' => 0,
					),
					array (
						'key' => 'field_58a219f2f33a1',
						'label' => __('Star Icon Size', 'ultimateazon2'),
						'name' => 'star_icon_size',
						'type' => 'number',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_589757eb3af1a',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_5899df7967928',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_589a173174147',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_589a255e69fd6',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '50',
							'class' => '',
							'id' => '',
						),
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => 'px',
						'min' => '',
						'max' => '',
						'step' => '',
					),
					array (
						'key' => 'field_58a33bf32608a',
						'label' => __('Star Icon Color', 'ultimateazon2'),
						'name' => 'star_icon_color',
						'type' => 'color_picker',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_589757eb3af1a',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_5899df7967928',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_589a173174147',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_589a255e69fd6',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '50',
							'class' => '',
							'id' => '',
						),
						'default_value' => '#FFCC00',
					),
					array (
						'key' => 'field_589a256a69fd8',
						'label' => __('Enable Yes/No Rating Field Styles', 'ultimateazon2'),
						'name' => 'enable_yesno_rating_field_styles',
						'type' => 'true_false',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_589757eb3af1a',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_5899df7967928',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_589a173174147',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'message' => __('Check here to enable "yes/no" field styles', 'ultimateazon2'),
						'default_value' => 0,
					),
					array (
						'key' => 'field_58a21a26f33a2',
						'label' => __('Yes/No Icon Size', 'ultimateazon2'),
						'name' => 'yesno_icon_size',
						'type' => 'number',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_589757eb3af1a',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_5899df7967928',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_589a173174147',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_589a256a69fd8',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '34',
							'class' => '',
							'id' => '',
						),
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => 'px',
						'min' => '',
						'max' => '',
						'step' => '',
					),
					array (
						'key' => 'field_58a33c1f2608b',
						'label' => __('Yes/No "YES" Icon Color', 'ultimateazon2'),
						'name' => 'yesno_yes_icon_color',
						'type' => 'color_picker',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_589757eb3af1a',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_5899df7967928',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_589a173174147',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_589a256a69fd8',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '33',
							'class' => '',
							'id' => '',
						),
						'default_value' => '#FFCC00',
					),
					array (
						'key' => 'field_58a33c542608c',
						'label' => __('Yes/No "NO" Icon Color', 'ultimateazon2'),
						'name' => 'yesno_no_icon_color',
						'type' => 'color_picker',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_589757eb3af1a',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_5899df7967928',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_589a173174147',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_589a256a69fd8',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '33',
							'class' => '',
							'id' => '',
						),
						'default_value' => '#B20000',
					),
					array (
						'key' => 'field_589a253769fd2',
						'label' => __('Enable Price Field Styles', 'ultimateazon2'),
						'name' => 'enable_price_field_styles',
						'type' => 'true_false',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_589757eb3af1a',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_5899df7967928',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_589a173174147',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'message' => __('Check here to enable "price" field styles', 'ultimateazon2'),
						'default_value' => 0,
					),
					array (
						'key' => 'field_58a34282c3ecb',
						'label' => __('Slashed Price Font Size', 'ultimateazon2'),
						'name' => 'slashed_price_font_size',
						'type' => 'number',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_589757eb3af1a',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_5899df7967928',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_589a173174147',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_589a253769fd2',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '50',
							'class' => '',
							'id' => '',
						),
						'default_value' => 14,
						'placeholder' => '',
						'prepend' => '',
						'append' => 'px',
						'min' => '',
						'max' => '',
						'step' => '',
					),
					array (
						'key' => 'field_58a342b1c3ecd',
						'label' => __('Slashed Price Color', 'ultimateazon2'),
						'name' => 'slashed_price_color',
						'type' => 'color_picker',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_589757eb3af1a',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_5899df7967928',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_589a173174147',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_589a253769fd2',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '50',
							'class' => '',
							'id' => '',
						),
						'default_value' => '#999999',
					),
					array (
						'key' => 'field_58a342e3c3ece',
						'label' => __('Sale Price Font Size', 'ultimateazon2'),
						'name' => 'sale_price_font_size',
						'type' => 'number',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_589757eb3af1a',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_5899df7967928',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_589a173174147',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_589a253769fd2',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '50',
							'class' => '',
							'id' => '',
						),
						'default_value' => 18,
						'placeholder' => '',
						'prepend' => '',
						'append' => 'px',
						'min' => '',
						'max' => '',
						'step' => '',
					),
					array (
						'key' => 'field_58a342f3c3ecf',
						'label' => __('Sale Price Color', 'ultimateazon2'),
						'name' => 'sale_price_color',
						'type' => 'color_picker',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_589757eb3af1a',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_5899df7967928',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_589a173174147',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_589a253769fd2',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '50',
							'class' => '',
							'id' => '',
						),
						'default_value' => '#3ABB22',
					),
					array (
						'key' => 'field_58a34318c3ed0',
						'label' => __('Amount/Percentage Saved Font Size', 'ultimateazon2'),
						'name' => 'amountpercentage_saved_font_size',
						'type' => 'number',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_589757eb3af1a',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_5899df7967928',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_589a173174147',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_589a253769fd2',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '50',
							'class' => '',
							'id' => '',
						),
						'default_value' => 18,
						'placeholder' => '',
						'prepend' => '',
						'append' => 'px',
						'min' => '',
						'max' => '',
						'step' => '',
					),
					array (
						'key' => 'field_58a3433fc3ed1',
						'label' => __('Amount/Percentage Saved Color', 'ultimateazon2'),
						'name' => 'amountpercentage_saved_color',
						'type' => 'color_picker',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_589757eb3af1a',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_5899df7967928',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_589a173174147',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_589a253769fd2',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '50',
							'class' => '',
							'id' => '',
						),
						'default_value' => '#000000',
					),
					array (
						'key' => 'field_589a254769fd3',
						'label' => __('Enable Lowest New Price Field Styles', 'ultimateazon2'),
						'name' => 'enable_lowest_new_price_field_styles',
						'type' => 'true_false',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_589757eb3af1a',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_5899df7967928',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_589a173174147',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'message' => __('Check here to enable "lowest new price" field styles', 'ultimateazon2'),
						'default_value' => 0,
					),
					array (
						'key' => 'field_58a219c2f339f',
						'label' => __('Lowest New Price Font Size', 'ultimateazon2'),
						'name' => 'lowest_new_price_font_size',
						'type' => 'number',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_589757eb3af1a',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_5899df7967928',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_589a173174147',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_589a254769fd3',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '33',
							'class' => '',
							'id' => '',
						),
						'default_value' => 14,
						'placeholder' => '',
						'prepend' => '',
						'append' => 'px',
						'min' => '',
						'max' => '',
						'step' => '',
					),
					array (
						'key' => 'field_58a3398930c33',
						'label' => __('Lowest New Price Line Height', 'ultimateazon2'),
						'name' => 'lowest_new_price_line_height',
						'type' => 'number',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_589757eb3af1a',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_5899df7967928',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_589a173174147',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_589a254769fd3',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '33',
							'class' => '',
							'id' => '',
						),
						'default_value' => 16,
						'placeholder' => '',
						'prepend' => '',
						'append' => 'px',
						'min' => '',
						'max' => '',
						'step' => '',
					),
					array (
						'key' => 'field_58a33caa2608d',
						'label' => __('Lowest New Price Text Color', 'ultimateazon2'),
						'name' => 'lowest_new_price_text_color',
						'type' => 'color_picker',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_589757eb3af1a',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_5899df7967928',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_589a173174147',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_589a254769fd3',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '34',
							'class' => '',
							'id' => '',
						),
						'default_value' => '#000',
					),
					array (
						'key' => 'field_589a254f69fd4',
						'label' => __('Enable Lowest Used Price Field Styles', 'ultimateazon2'),
						'name' => 'enable_lowest_used_price_field_styles',
						'type' => 'true_false',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_589757eb3af1a',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_5899df7967928',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_589a173174147',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'message' => __('Check here to enable "lowest used price" field styles', 'ultimateazon2'),
						'default_value' => 0,
					),
					array (
						'key' => 'field_58a219dbf33a0',
						'label' => __('Lowest Used Price Font Size', 'ultimateazon2'),
						'name' => 'lowest_used_price_font_size',
						'type' => 'number',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_589757eb3af1a',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_5899df7967928',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_589a173174147',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_589a254f69fd4',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '33',
							'class' => '',
							'id' => '',
						),
						'default_value' => 14,
						'placeholder' => '',
						'prepend' => '',
						'append' => 'px',
						'min' => '',
						'max' => '',
						'step' => '',
					),
					array (
						'key' => 'field_58a3399330c34',
						'label' => __('Lowest Used Price Line Height', 'ultimateazon2'),
						'name' => 'lowest_used_price_line_height',
						'type' => 'number',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_589757eb3af1a',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_5899df7967928',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_589a173174147',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_589a254f69fd4',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '33',
							'class' => '',
							'id' => '',
						),
						'default_value' => 16,
						'placeholder' => '',
						'prepend' => '',
						'append' => 'px',
						'min' => '',
						'max' => '',
						'step' => '',
					),
					array (
						'key' => 'field_58a33d481735a',
						'label' => __('Lowest Used Price Text Color', 'ultimateazon2'),
						'name' => 'lowest_used_price_text_color',
						'type' => 'color_picker',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_589757eb3af1a',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_5899df7967928',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_589a173174147',
									'operator' => '==',
									'value' => '1',
								),
								array (
									'field' => 'field_589a254f69fd4',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '34',
							'class' => '',
							'id' => '',
						),
						'default_value' => '#000',
					),
					array (
						'key' => 'field_i7v6c5r7v85c',
						'label' => __('Table Footer Styles', 'ultimateazon2'),
						'name' => '',
						'type' => 'message',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '',
							'class' => 'djn-section-heading',
							'id' => '',
						),
						'message' => '',
						'new_lines' => 'wpautop',
						'esc_html' => 0,
					),
					array (
						'key' => 'field_o7yb6vt5c4vt7by8',
						'label' => __('Enable Table Footer Styles', 'ultimateazon2'),
						'name' => 'enable_table_footer_styles',
						'type' => 'true_false',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'message' => __('Check here to enable the table footer styles', 'ultimateazon2'),
						'default_value' => 0,
					),
					array (
						'key' => 'field_i7v65c4rtvy7b86v5c',
						'label' => __('Table Footer Background Color', 'ultimateazon2'),
						'name' => 'table_footer_bg_color',
						'type' => 'color_picker',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_o7yb6vt5c4vt7by8',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '25',
							'class' => '',
							'id' => '',
						),
						'default_value' => '',
					),

					array (
						'key' => 'field_oiiyugbhuibj',
						'label' => __('Table Footer Button Background Color', 'ultimateazon2'),
						'name' => 'table_footer_button_bg_color',
						'type' => 'color_picker',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_o7yb6vt5c4vt7by8',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '25',
							'class' => '',
							'id' => '',
						),
						'default_value' => '#FFFFFF',
					),
					array (
						'key' => 'field_n87b6v5cv7b8v5',
						'label' => __('Table Footer Button Text Color', 'ultimateazon2'),
						'name' => 'table_footer_button_text_color',
						'type' => 'color_picker',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_o7yb6vt5c4vt7by8',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '25',
							'class' => '',
							'id' => '',
						),
						'default_value' => '#FFFFFF',
					),
					array (
						'key' => 'field_6v57b8b6v5c4',
						'label' => __('Table Footer Text Color', 'ultimateazon2'),
						'name' => 'table_footer_text_color',
						'type' => 'color_picker',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_o7yb6vt5c4vt7by8',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '25',
							'class' => '',
							'id' => '',
						),
						'default_value' => '#FFFFFF',
					),
				),
				'location' => array (
					array (
						array (
							'param' => 'options_page',
							'operator' => '==',
							'value' => 'theme-styles',
						),
					),
				),
				'menu_order' => 500,
				'position' => 'normal',
				'style' => 'default',
				'label_placement' => 'top',
				'instruction_placement' => 'label',
				'hide_on_screen' => '',
				'active' => 1,
				'description' => '',
			));





		//endif;





	}









	/**
	 * Create repeater rows for what specs you want to display in the table columns
	 * EXTENDS PRODUCT REVIEW PRO
	 *
	 * @since    1.0.0
	 */
	public function galfram_table_add_spec_columns() {


		if ( !isset( $_GET ) || !isset( $_POST ) ) {
			return;
		}

		if ( isset( $_GET['post'] ) ) {
			$post_id = $_GET['post'];
		}
		elseif ( isset( $_POST['post_ID'] ) ) {
			$post_id = $_POST['post_ID'];
		}
		else {
			return;
		}

		if ( get_post_type($post_id) != 'galfram_table' ) {
			return;
		}


		// Get our table type
		$table_type = get_field('galfram_type_of_table', $post_id );


		if ( $table_type == 'prp-table' ) {


			// get the chosen spec group for this table
			$specs_group_id = get_post_meta($post_id, 'galfram_product_spec_group', true);
			// get all the spec groups so we can get only the one we need
			$user_specs_groups = get_option('galfram_ext_prp_all_userspecs_groups');

			// create an empty array to save our chosen spec group into
			$current_chosen_spec_group = array();

			if ( $specs_group_id && $specs_group_id != 'pull-from-amazon' ) {

				// loop through all spc grooups and only get the one we want for this table post
				foreach ( $user_specs_groups as $user_spec_group ) {
					if ( $user_spec_group['field_57c35f7e8031b'] == $specs_group_id ) {
						$current_chosen_spec_group = $user_spec_group;
					}
				}

				
				if ( $current_chosen_spec_group['field_57c068f1622b7'] == 'create-my-own' ) {
					$specs_arr = $current_chosen_spec_group['field_57c06aa9622b9'];
				}


				$i=0;
				// lets save just the spec names and IDs form the specs group array to be our choices for our table columns
				foreach ( $specs_arr as $spec ) {
					$all_specs[$i]['spec_id'] = $spec['field_57c6bfb62908a'];
					$all_specs[$i]['spec_name'] = $spec['field_57c06afc622ba'];
					$i++;
				}

				// let's organize our spec names and ids into our final array for our choices.
				foreach ( $all_specs as $spec ) {
					$spec_choices[ $spec['spec_id'] ] = $spec['spec_name'];
				}


				// let's add the subfield to the repeater now with our new cjoices for our columns
				acf_add_local_field(array(
					'key' => 'field_58361280dbab8',
					'label' => __('Column', 'ultimateazon2'),
					'name' => 'column',
					'type' => 'select',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array (
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'choices' => $spec_choices,
					'default_value' => '',
					'placeholder' => '',
					'prepend' => '',
					'append' => '',
					'maxlength' => '',
					'parent' => 'field_5836125bdbab7'
				));

				acf_add_local_field(array(
					'key' => 'field_58361280dbab8responsive_settings',
					'label' => __('Responsive Settings', 'ultimateazon2'),
					'name' => 'responsive_settings',
					'type' => 'checkbox',
					'instructions' => __('Check which screen sizes you would like to hide this column on.', 'ultimateazon2'),
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array (
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'choices' => array(
						'xs' => __('Extra Small', 'ultimateazon2'),
						'sm' => __('Small', 'ultimateazon2'),
						'md' => __('Medium', 'ultimateazon2'),
						'lg' => __('Large', 'ultimateazon2'),
					),
					'default_value' => '',
					'placeholder' => '',
					'prepend' => '',
					'append' => '',
					'maxlength' => '',
					'parent' => 'field_5836125bdbab7'
				));

				acf_add_local_field(array (
					'key' => 'field_583f359fc3be7_disable_sorting',
					'label' => __('Column Sorting', 'ultimateazon2'),
					'name' => 'disable_sorting',
					'type' => 'true_false',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array (
						'width' => 50,
						'class' => '',
						'id' => '',
					),
					'message' => __('Check this box to disable sorting on this column. (Image specification types are not sortable already)', 'ultimateazon2'),
					'default_value' => 0,
					'parent' => 'field_5836125bdbab7'
				));

				acf_add_local_field(array (
					'key' => 'field_58757ae4f0a04',
					'label' => __('Column Width', 'ultimateazon2'),
					'name' => 'column_width',
					'type' => 'number',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array (
						'width' => '70',
						'class' => '',
						'id' => '',
					),
					'default_value' => '',
					'placeholder' => '',
					'prepend' => '',
					'append' => '',
					'min' => '',
					'max' => '',
					'step' => '',
					'parent' => 'field_5836125bdbab7',
				));

				acf_add_local_field(array (
					'key' => 'field_58757b12f0a05',
					'label' => __('Column Width Type', 'ultimateazon2'),
					'name' => 'column_width_type',
					'type' => 'select',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array (
						'width' => '30',
						'class' => '',
						'id' => '',
					),
					'choices' => array (
						'px' => 'px',
						'%' => '%',
					),
					'default_value' => array (
					),
					'allow_null' => 1,
					'multiple' => 0,
					'ui' => 0,
					'ajax' => 0,
					'return_format' => 'value',
					'placeholder' => '',
					'parent' => 'field_5836125bdbab7',
				));

				acf_add_local_field(array(
					'key' => 'field_87b6v8657c564c',
					'label' => __('Link', 'ultimateazon2'),
					'name' => 'field_link',
					'type' => 'select',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array (
						'width' => '20',
						'class' => '',
						'id' => '',
					),
					'choices' => array (
						'field-link-none' => __('No Link', 'ultimateazon2'),
						'field-link-review' => __('Full Review Link', 'ultimateazon2'),
						'field-link-affiliate' => __('Affiliate Link', 'ultimateazon2'),
					),
					'default_value' => '',
					'placeholder' => '',
					'prepend' => '',
					'append' => '',
					'maxlength' => '',
					'parent' => 'field_5836125bdbab7'
				));

				// acf_add_local_field(array (
				// 	'key' => 'field_583f359fc3be7_add_aff_link',
				// 	'label' => __('Add Affiliate Link', 'ultimateazon2'),
				// 	'name' => 'add_aff_link',
				// 	'type' => 'true_false',
				// 	'instructions' => '',
				// 	'required' => 0,
				// 	'conditional_logic' => 0,
				// 	'wrapper' => array (
				// 		'width' => 50,
				// 		'class' => '',
				// 		'id' => '',
				// 	),
				// 	'message' => __('Check this box to add your Amazon Affiliate Link to the values in this column.', 'ultimateazon2'),
				// 	'default_value' => 0,
				// 	'parent' => 'field_5836125bdbab7'
				// ));

				acf_add_local_field(array (
					'key' => 'field_58757b12f0a05_column_horz_align',
					'label' => __('Column Horizontal Alignment', 'ultimateazon2'),
					'name' => 'column_horz_align',
					'type' => 'select',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array (
						'width' => '30',
						'class' => '',
						'id' => '',
					),
					'choices' => array (
						'left' => 'left',
						'center' => 'center',
						'right' => 'right',
					),
					'default_value' => array (
					),
					'allow_null' => 1,
					'multiple' => 0,
					'ui' => 0,
					'ajax' => 0,
					'return_format' => 'value',
					'placeholder' => '',
					'parent' => 'field_5836125bdbab7',
				));

				acf_add_local_field(array (
					'key' => 'field_58757b12f0a05_column_vert_align',
					'label' => __('Column Vertical Alignment', 'ultimateazon2'),
					'name' => 'column_vert_align',
					'type' => 'select',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array (
						'width' => '30',
						'class' => '',
						'id' => '',
					),
					'choices' => array (
						'top' => 'top',
						'middle' => 'middle',
						'bottom' => 'bottom',
					),
					'default_value' => array (
					),
					'allow_null' => 1,
					'multiple' => 0,
					'ui' => 0,
					'ajax' => 0,
					'return_format' => 'value',
					'placeholder' => '',
					'parent' => 'field_5836125bdbab7',
				));

			}
			elseif ( $specs_group_id && $specs_group_id == 'pull-from-amazon' ) {

				$spec_choices = array (
					'wp-title' => __('WP Title', 'ultimateazon2' ),
					'amazon-title' => __('Amazon Title', 'ultimateazon2' ),
					'editor-rating' => __('Editor\'s Rating', 'ultimateazon2' ),
					'asin' => __('ASIN', 'ultimateazon2' ),
					'brand' => __('Brand', 'ultimateazon2' ),
					'model' => __('Model', 'ultimateazon2' ),
					'upc' => __('UPC', 'ultimateazon2' ),
					'features' => __('Features', 'ultimateazon2' ),
					'warranty' => __('Warranty', 'ultimateazon2' ),
					'price' => __('Price', 'ultimateazon2' ),
					'lowest-new-price' => __('Lowest New Price', 'ultimateazon2' ),
					'lowest-used-price' => __('Lowest Used Price', 'ultimateazon2' ),
					'small-image' => __('Small Image', 'ultimateazon2' ),
					'medium-image' => __('Medium Image', 'ultimateazon2' ),
					'large-image' => __('Large Image', 'ultimateazon2' ),
				);

				acf_add_local_field(array(
					'key' => 'field_58361280dbab8jhmn',
					'label' => __('Columns', 'ultimateazon2'),
					'name' => 'column',
					'type' => 'select',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array (
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'choices' => $spec_choices,
					'default_value' => '',
					'placeholder' => '',
					'prepend' => '',
					'append' => '',
					'maxlength' => '',
					'parent' => 'field_5836125bdbab7'
				));

				acf_add_local_field(array(
					'key' => 'field_58361280dbab8responsive_settings',
					'label' => __('Responsive Settings', 'ultimateazon2'),
					'name' => 'responsive_settings',
					'type' => 'checkbox',
					'instructions' => __('Check which screen sizes you would like to hide this column on.', 'ultimateazon2'),
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array (
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'choices' => array(
						'xs' => __('Extra Small', 'ultimateazon2'),
						'sm' => __('Small', 'ultimateazon2'),
						'md' => __('Medium', 'ultimateazon2'),
						'lg' => __('Large', 'ultimateazon2'),
					),
					'default_value' => '',
					'placeholder' => '',
					'prepend' => '',
					'append' => '',
					'maxlength' => '',
					'parent' => 'field_5836125bdbab7'
				));

				acf_add_local_field(array (
					'key' => 'field_583f359fc3be7_disable_sorting',
					'label' => __('Column Sorting', 'ultimateazon2'),
					'name' => 'disable_sorting',
					'type' => 'true_false',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array (
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'message' => __('Check this box to disable sorting on this column. (Image specification types are not sortable already)', 'ultimateazon2'),
					'default_value' => 0,
					'parent' => 'field_5836125bdbab7'
				));

				acf_add_local_field(array (
					'key' => 'field_58757ae4f0a04',
					'label' => __('Column Width', 'ultimateazon2'),
					'name' => 'column_width',
					'type' => 'number',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array (
						'width' => '70',
						'class' => '',
						'id' => '',
					),
					'default_value' => '',
					'placeholder' => '',
					'prepend' => '',
					'append' => '',
					'min' => '',
					'max' => '',
					'step' => '',
					'parent' => 'field_5836125bdbab7',
				));

				acf_add_local_field(array (
					'key' => 'field_58757b12f0a05',
					'label' => __('Column Width Type', 'ultimateazon2'),
					'name' => 'column_width_type',
					'type' => 'select',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array (
						'width' => '30',
						'class' => '',
						'id' => '',
					),
					'choices' => array (
						'px' => 'px',
						'%' => '%',
					),
					'default_value' => array (
					),
					'allow_null' => 1,
					'multiple' => 0,
					'ui' => 0,
					'ajax' => 0,
					'return_format' => 'value',
					'placeholder' => '',
					'parent' => 'field_5836125bdbab7',
				));

				acf_add_local_field(array(
					'key' => 'field_87b6v8657c564c',
					'label' => __('Link', 'ultimateazon2'),
					'name' => 'field_link',
					'type' => 'select',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array (
						'width' => '20',
						'class' => '',
						'id' => '',
					),
					'choices' => array (
						'field-link-none' => __('No Link', 'ultimateazon2'),
						'field-link-review' => __('Full Review Link', 'ultimateazon2'),
						'field-link-affiliate' => __('Affiliate Link', 'ultimateazon2'),
					),
					'default_value' => '',
					'placeholder' => '',
					'prepend' => '',
					'append' => '',
					'maxlength' => '',
					'parent' => 'field_5836125bdbab7'
				));

				// acf_add_local_field(array (
				// 	'key' => 'field_583f359fc3be7_add_aff_link',
				// 	'label' => __('Add Affiliate Link', 'ultimateazon2'),
				// 	'name' => 'add_aff_link',
				// 	'type' => 'true_false',
				// 	'instructions' => '',
				// 	'required' => 0,
				// 	'conditional_logic' => 0,
				// 	'wrapper' => array (
				// 		'width' => 50,
				// 		'class' => '',
				// 		'id' => '',
				// 	),
				// 	'message' => __('Check this box to add your Amazon Affiliate Link to the values in this column.', 'ultimateazon2'),
				// 	'default_value' => 0,
				// 	'parent' => 'field_5836125bdbab7'
				// ));

				acf_add_local_field(array (
					'key' => 'field_58757b12f0a05_column_horz_align',
					'label' => __('Column Horizontal Alignment', 'ultimateazon2'),
					'name' => 'column_horz_align',
					'type' => 'select',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array (
						'width' => '30',
						'class' => '',
						'id' => '',
					),
					'choices' => array (
						'left' => 'left',
						'center' => 'center',
						'right' => 'right',
					),
					'default_value' => array (
					),
					'allow_null' => 1,
					'multiple' => 0,
					'ui' => 0,
					'ajax' => 0,
					'return_format' => 'value',
					'placeholder' => '',
					'parent' => 'field_5836125bdbab7',
				));

				acf_add_local_field(array (
					'key' => 'field_58757b12f0a05_column_vert_align',
					'label' => __('Column Vertical Alignment', 'ultimateazon2'),
					'name' => 'column_vert_align',
					'type' => 'select',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array (
						'width' => '30',
						'class' => '',
						'id' => '',
					),
					'choices' => array (
						'top' => 'top',
						'middle' => 'middle',
						'bottom' => 'bottom',
					),
					'default_value' => array (
					),
					'allow_null' => 1,
					'multiple' => 0,
					'ui' => 0,
					'ajax' => 0,
					'return_format' => 'value',
					'placeholder' => '',
					'parent' => 'field_5836125bdbab7',
				));

			}
			else {
				return;
			}

		}

		elseif ( $table_type == 'prp-table-api' ) {

		}

		elseif ( $table_type == 'prp-table-custom' ) {

			// let's add the subfield to the repeater now with our new cjoices for our columns
			acf_add_local_field(array(
				'key' => 'field_58361280dbab8',
				'label' => __('Column Name', 'ultimateazon2'),
				'name' => 'column',
				'type' => 'text',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array (
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'maxlength' => '',
				'parent' => 'field_5836125bdbab7_custom_tbl'
			));

			// let's add the subfield to the repeater now with our new cjoices for our columns
			acf_add_local_field(array(
				'key' => 'field_58361280dbab8_columnid',
				'label' => __('Column ID', 'ultimateazon2'),
				'name' => 'column_id',
				'type' => 'text',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array (
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'maxlength' => '',
				'parent' => 'field_5836125bdbab7_custom_tbl'
			));

			acf_add_local_field(array (
				'key' => 'field_58757b12f0a05_column_data_type',
				'label' => __('Column Type', 'ultimateazon2'),
				'name' => 'column_data_type',
				'type' => 'select',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array (
					'width' => '30',
					'class' => '',
					'id' => '',
				),
				'choices' => array (
					'text' => __('Plain Text', 'ultimateazon2'),
					'image' => __('Plain Image', 'ultimateazon2'),
					'wysiwyg' => __('WYSIWYG', 'ultimateazon2'),
					'number' => __('Number', 'ultimateazon2'),
					'text-link' => __('Text Link', 'ultimateazon2'),
					'image-link' => __('Image Link', 'ultimateazon2'),
					'price' => __('Price', 'ultimateazon2'),

				),
				'default_value' => array (
				),
				'allow_null' => 1,
				'multiple' => 0,
				'ui' => 0,
				'ajax' => 0,
				'return_format' => 'value',
				'placeholder' => '',
				'parent' => 'field_5836125bdbab7_custom_tbl',
			));

			acf_add_local_field(array(
				'key' => 'field_58361280dbab8responsive_settings',
				'label' => __('Responsive Settings', 'ultimateazon2'),
				'name' => 'responsive_settings',
				'type' => 'checkbox',
				'instructions' => __('Check which screen sizes you would like to hide this column on.', 'ultimateazon2'),
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array (
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'choices' => array(
					'xs' => __('Extra Small', 'ultimateazon2'),
					'sm' => __('Small', 'ultimateazon2'),
					'md' => __('Medium', 'ultimateazon2'),
					'lg' => __('Large', 'ultimateazon2'),
				),
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'maxlength' => '',
				'parent' => 'field_5836125bdbab7_custom_tbl'
			));

			acf_add_local_field(array (
				'key' => 'field_583f359fc3be7_disable_sorting',
				'label' => __('Column Sorting', 'ultimateazon2'),
				'name' => 'disable_sorting',
				'type' => 'true_false',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array (
					'width' => 50,
					'class' => '',
					'id' => '',
				),
				'message' => __('Check this box to disable sorting on this column. (Image specification types are not sortable already)', 'ultimateazon2'),
				'default_value' => 0,
				'parent' => 'field_5836125bdbab7_custom_tbl'
			));

			acf_add_local_field(array (
				'key' => 'field_58757ae4f0a04',
				'label' => __('Column Width', 'ultimateazon2'),
				'name' => 'column_width',
				'type' => 'number',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array (
					'width' => '70',
					'class' => '',
					'id' => '',
				),
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'min' => '',
				'max' => '',
				'step' => '',
				'parent' => 'field_5836125bdbab7_custom_tbl',
			));

			acf_add_local_field(array (
				'key' => 'field_58757b12f0a05',
				'label' => __('Column Width Type', 'ultimateazon2'),
				'name' => 'column_width_type',
				'type' => 'select',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array (
					'width' => '30',
					'class' => '',
					'id' => '',
				),
				'choices' => array (
					'px' => 'px',
					'%' => '%',
				),
				'default_value' => array (
				),
				'allow_null' => 1,
				'multiple' => 0,
				'ui' => 0,
				'ajax' => 0,
				'return_format' => 'value',
				'placeholder' => '',
				'parent' => 'field_5836125bdbab7_custom_tbl',
			));

			acf_add_local_field(array (
				'key' => 'field_7t678t670a05_column_horz_align',
				'label' => __('Column Horizontal Alignment', 'ultimateazon2'),
				'name' => 'column_horz_align',
				'type' => 'select',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array (
					'width' => '30',
					'class' => '',
					'id' => '',
				),
				'choices' => array (
					'left' => 'left',
					'center' => 'center',
					'right' => 'right',
				),
				'default_value' => array (
				),
				'allow_null' => 1,
				'multiple' => 0,
				'ui' => 0,
				'ajax' => 0,
				'return_format' => 'value',
				'placeholder' => '',
				'parent' => 'field_5836125bdbab7_custom_tbl',
			));

			acf_add_local_field(array (
				'key' => 'field_5y8787ny705_column_vert_align',
				'label' => __('Column Vertical Alignment', 'ultimateazon2'),
				'name' => 'column_vert_align',
				'type' => 'select',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array (
					'width' => '30',
					'class' => '',
					'id' => '',
				),
				'choices' => array (
					'top' => 'top',
					'middle' => 'middle',
					'bottom' => 'bottom',
				),
				'default_value' => array (
				),
				'allow_null' => 1,
				'multiple' => 0,
				'ui' => 0,
				'ajax' => 0,
				'return_format' => 'value',
				'placeholder' => '',
				'parent' => 'field_5836125bdbab7_custom_tbl',
			));

		}



	}







	/**
	 * Create the subfield for adding rows of products
	 * EXTENDS PRODUCT REVIEW PRO
	 *
	 * @since    1.0.0
	 */
	public function galfram_table_add_reviews() {


		if ( !isset( $_GET ) || !isset( $_POST ) ) {
			return;
		}

		if( isset( $_GET['post'] ) ){
		    $post_id = $_GET['post'];
		} elseif ( isset( $_POST['post_ID'] ) ){
		    $post_id = $_POST['post_ID'];
		} else {
		    return;
		}

		if ( get_post_type($post_id) != 'galfram_table' ) {
			return;
		}


		// get the saved spec group for this table and create our value
		$specs_group_id = get_post_meta( $post_id, 'galfram_product_spec_group', true );

		$args = array (
			'post_type' => 'galfram_review',
			'posts_per_page' => -1,
			'orderby' => 'title',
			'order' => 'ASC',
			'tax_query' => array(
				array(
					'taxonomy' => 'galfram_spec_group',
					'field' => 'slug',
					'terms' => $specs_group_id,
				)
			),


		);

		$reviews = get_posts($args);
		$review_choices = array();

		foreach ( $reviews as $review ) {
			$review_choices[$review->ID] = $review->post_title;
		}


		// let's add the subfield to the repeater now with our new choices for our columns
		acf_add_local_field(array(
			'key' => 'field_58361280dbab8sfgefg',
			'label' => __('Product', 'ultimateazon2'),
			'name' => 'galfram_table_row_prod',
			'type' => 'select',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'choices' => $review_choices,
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'maxlength' => '',
			'parent' => 'field_5838867e43db1'
		));

	}






	/**
	 * Create the unique ids for our custom user generated columns
	 * EXTENDS PRODUCT REVIEW PRO
	 *
	 * @since    1.0.0
	 */
	public function galfram_custom_table_generate_column_id( $post_id ) {
		$screen = get_current_screen($post_id);
		// bail early if no ACF data or not on the correct screen
	    if( empty($_POST['acf']) || $screen->id != 'galfram_table' ) {
	        return;
	    }

	    if( have_rows('galfram_choose_columns_custom_tbl', $post_id) ) {
			$i = 0;
			while( have_rows('galfram_choose_columns_custom_tbl', $post_id) ) {
				the_row();
				if ( !get_sub_field('column_id') ) {
					$random_spec_id = 'spec_'.substr(md5(microtime()),rand(0,26),10).$i;
					update_sub_field( 'column_id', $random_spec_id );
				}
				$i++;
			}
		}
	}




	/**
	 * Create the subfields for the custom data tables, based off the columns the user has created
	 * EXTENDS PRODUCT REVIEW PRO
	 *
	 * @since    1.0.0
	 */
	public function galfram_custom_table_add_data_subfields() {


		if ( !isset( $_GET ) || !isset( $_POST ) ) {
			return;
		}

		if( isset( $_GET['post'] ) ){
		    $post_id = $_GET['post'];
		} elseif ( isset( $_POST['post_ID'] ) ){
		    $post_id = $_POST['post_ID'];
		} else {
		    return;
		}

		if ( get_post_type($post_id) != 'galfram_table' ) {
			return;
		}

		// get the saved spec group for this table and create our value
		$columns = get_post_meta( $post_id, 'galfram_choose_columns_custom_tbl', true );

		if( $columns ) {

			for( $i = 0; $i < $columns; $i++ ) {

				$column_label = get_post_meta( $post_id, 'galfram_choose_columns_custom_tbl_' . $i . '_column', true );
				$column_name = sanitize_title( $column_label ).'_'.$i;
				$column_type = get_post_meta( $post_id, 'galfram_choose_columns_custom_tbl_' . $i . '_column_data_type', true );
				$column_id = get_post_meta( $post_id, 'galfram_choose_columns_custom_tbl_' . $i . '_column_id', true );

				if ( $column_type == 'text' ) {
					acf_add_local_field(array(
						'key' => 'field_'.$column_id,
						'label' => $column_label,
						'name' => 'text_field_'.$column_id,
						'type' => 'text',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '',
							'class' => 'galfram-tbl-text',
							'id' => '',
						),
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'maxlength' => '',
						'parent' => 'field_5838867e43db1_add_custom_data'
					));
				}
				elseif ( $column_type == 'wysiwyg' ) {
					acf_add_local_field(array(
						'key' => 'field_'.$column_id,
						'label' => $column_label,
						'name' => 'wysiwyg_field_'.$column_id,
						'type' => 'wysiwyg',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '',
							'class' => 'galfram-tbl-wysiwyg',
							'id' => '',
						),
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'maxlength' => '',
						'parent' => 'field_5838867e43db1_add_custom_data'
					));
				}
				elseif ( $column_type == 'image' ) {
					acf_add_local_field(array(
						'key' => 'field_'.$column_id,
						'label' => $column_label,
						'name' => 'image_field_'.$column_id,
						'type' => 'image',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '',
							'class' => 'galfram-tbl-image',
							'id' => '',
						),
						'return_format' => 'id',
						'preview_size' => 'full',
						'library' => 'all',
						'min_width' => '',
						'min_height' => '',
						'min_size' => '',
						'max_width' => '',
						'max_height' => '',
						'max_size' => '',
						'mime_types' => '',
						'parent' => 'field_5838867e43db1_add_custom_data'
					));
				}
				elseif ( $column_type == 'number' ) {
					acf_add_local_field(array(
						'key' => 'field_'.$column_id,
						'label' => $column_label,
						'name' => 'number_field_'.$column_id,
						'type' => 'number',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '',
							'class' => 'galfram-tbl-number',
							'id' => '',
						),
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'min' => '',
						'max' => '',
						'step' => '',
						'parent' => 'field_5838867e43db1_add_custom_data'
					));
				}
				elseif ( $column_type == 'image-link' ) {
					acf_add_local_field(array(
						'key' => 'field_'.$column_id,
						'label' => $column_label,
						'name' => 'imagelink_field_'.$column_id,
						'type' => 'repeater',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '',
							'class' => 'galfram-tbl-imagelink',
							'id' => '',
						),
						'collapsed' => '',
						'min' => '1',
						'max' => '1',
						'layout' => 'row',
						'button_label' => __('Add Row', 'ultimateazon2'),
						'sub_fields' => array (
							array (
								'key' => 'field_n9487nr37n837',
								'label' => __('Image', 'ultimateazon2'),
								'name' => 'image',
								'type' => 'image',
								'instructions' => '',
								'required' => 0,
								'conditional_logic' => 0,
								'wrapper' => array (
									'width' => '',
									'class' => '',
									'id' => '',
								),
								'return_format' => 'id',
								'preview_size' => 'full',
								'library' => 'all',
								'min_width' => '',
								'min_height' => '',
								'min_size' => '',
								'max_width' => '',
								'max_height' => '',
								'max_size' => '',
								'mime_types' => '',
							),
							array (
								'key' => 'field_nuiw49n498394',
								'label' => __('Link URL', 'ultimateazon2'),
								'name' => 'link_url',
								'type' => 'text',
								'instructions' => '',
								'required' => 0,
								'conditional_logic' => 0,
								'wrapper' => array (
									'width' => '',
									'class' => '',
									'id' => '',
								),
								'default_value' => '',
								'placeholder' => '',
								'prepend' => '',
								'append' => '',
								'maxlength' => '',
							),
							array (
								'key' => 'field_7y8786bt86',
								'label' => __('Open in New Tab', 'ultimateazon2'),
								'name' => 'new_tab',
								'type' => 'true_false',
								'instructions' => '',
								'required' => 0,
								'conditional_logic' => 0,
								'wrapper' => array (
									'width' => '',
									'class' => '',
									'id' => '',
								),
								'message' => __('Open link in new tab', 'ultimateazon2'),
								'default_value' => 0,
							)
						),
						'parent' => 'field_5838867e43db1_add_custom_data'
					));
				}
				elseif ( $column_type == 'text-link' ) {
					acf_add_local_field(array(
						'key' => 'field_'.$column_id,
						'label' => $column_label,
						'name' => 'textlink_field_'.$column_id,
						'type' => 'repeater',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '',
							'class' => 'galfram-tbl-textlink',
							'id' => '',
						),
						'collapsed' => '',
						'min' => '1',
						'max' => '1',
						'layout' => 'row',
						'button_label' => __('Add Row', 'ultimateazon2'),
						'sub_fields' => array (
							array (
								'key' => 'field_n987ny97y78n',
								'label' => __('Link URL', 'ultimateazon2'),
								'name' => 'link_url',
								'type' => 'text',
								'instructions' => '',
								'required' => 0,
								'conditional_logic' => 0,
								'wrapper' => array (
									'width' => '',
									'class' => '',
									'id' => '',
								),
								'default_value' => '',
								'placeholder' => '',
								'prepend' => '',
								'append' => '',
								'maxlength' => '',
							),
							array (
								'key' => 'field_n987ny97y78n',
								'label' => __('Link Text', 'ultimateazon2'),
								'name' => 'link_text',
								'type' => 'text',
								'instructions' => '',
								'required' => 0,
								'conditional_logic' => 0,
								'wrapper' => array (
									'width' => '',
									'class' => '',
									'id' => '',
								),
								'default_value' => '',
								'placeholder' => '',
								'prepend' => '',
								'append' => '',
								'maxlength' => '',
							),
							array (
								'key' => 'field_zmi83m298mi27',
								'label' => __('Open Link in New Tab', 'ultimateazon2'),
								'name' => 'new_tab',
								'type' => 'true_false',
								'instructions' => '',
								'required' => 0,
								'conditional_logic' => 0,
								'wrapper' => array (
									'width' => '',
									'class' => '',
									'id' => '',
								),
								'message' => __('Open link in new tab', 'ultimateazon2'),
								'default_value' => 0,
							)
						),
						'parent' => 'field_5838867e43db1_add_custom_data'
					));

					
				}

				elseif ( $column_type == 'price' ) {

					acf_add_local_field(array(
						'key' => 'field_'.$column_id,
						'label' => $column_label,
						'name' => 'price_field_'.$column_id,
						'type' => 'repeater',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '',
							'class' => 'galfram-tbl-price',
							'id' => '',
						),
						'collapsed' => '',
						'min' => '1',
						'max' => '1',
						'layout' => 'row',
						'button_label' => __('Add Row', 'ultimateazon2'),
						'sub_fields' => array (
							array (
								'key' => 'field_frferferfe64346',
								'label' => __('List Price', 'ultimateazon2'),
								'name' => 'list_price',
								'type' => 'text',
								'instructions' => '',
								'required' => 0,
								'conditional_logic' => 0,
								'wrapper' => array (
									'width' => '',
									'class' => '',
									'id' => '',
								),
								'default_value' => '',
								'placeholder' => '',
								'prepend' => '',
								'append' => '',
								'maxlength' => '',
							),
							array (
								'key' => 'field_y737438943974',
								'label' => __('Sale Price', 'ultimateazon2'),
								'name' => 'sale_price',
								'type' => 'text',
								'instructions' => '',
								'required' => 0,
								'conditional_logic' => 0,
								'wrapper' => array (
									'width' => '',
									'class' => '',
									'id' => '',
								),
								'default_value' => '',
								'placeholder' => '',
								'prepend' => '',
								'append' => '',
								'maxlength' => '',
							),
							array (
								'key' => 'field_87y3n923x784n3897',
								'label' => __('Amount Off', 'ultimateazon2'),
								'name' => 'amount_off',
								'type' => 'text',
								'instructions' => '',
								'required' => 0,
								'conditional_logic' => 0,
								'wrapper' => array (
									'width' => '',
									'class' => '',
									'id' => '',
								),
								'default_value' => '',
								'placeholder' => '',
								'prepend' => '',
								'append' => '',
								'maxlength' => '',
							),
							array (
								'key' => 'field_8fg927r382g93n879rf',
								'label' => __('% Off', 'ultimateazon2'),
								'name' => 'percentage_off',
								'type' => 'text',
								'instructions' => '',
								'required' => 0,
								'conditional_logic' => 0,
								'wrapper' => array (
									'width' => '',
									'class' => '',
									'id' => '',
								),
								'default_value' => '',
								'placeholder' => '',
								'prepend' => '',
								'append' => '',
								'maxlength' => '',
							),
						),
						'parent' => 'field_5838867e43db1_add_custom_data'
					));
				}

			}

			// this is simply here to show a collapsed row number
			acf_add_local_field(array(
				'key' => 'field_chc7j4j48frjfr8r',
				'label' => __('Row ', 'ultimateazon2'),
				'name' => 'table_row',
				'type' => 'text',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array (
					'width' => '',
					'class' => 'djn-hidden-field',
					'id' => '',
				),
				'default_value' => 'Row '.$i,
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'maxlength' => '',
				'parent' => 'field_5838867e43db1_add_custom_data'
			));

		}



	}




	/**
	 * Build our final table array on post save, to make the front end load faster.
	 * EXTENDS PRODUCT REVIEW PRO
	 *
	 * @since    1.0.0
	 */
	public function galfram_save_table_array( $post_id ) {

		$screen = get_current_screen( $post_id );
		// bail early if no ACF data or not on the correct screen
	    if( empty($_POST['acf']) || $screen->id != 'galfram_table' ) {
	        return;
	    }

	    // exit this function if the galfram_tables_functions class does not exist
		if ( !class_exists( 'galfram_tables_functions' ) ) {
			return;
		}

		// create a new instance of our tables class if it exists
		$galfram_tables_functions = new galfram_tables_functions(); 

		$table_type = get_post_meta( $post_id, 'galfram_type_of_table', true );

	    if ( $table_type == 'prp-table' ) {

	    	// get the saved spec group for this table and create our value
			$specs_group_id = get_post_meta( $post_id, 'galfram_product_spec_group', true);
			// let's build our table header
			$final_table = $galfram_tables_functions->galfram_table_build_final_table_header( $specs_group_id, $post_id );
			// let's build our table body rows
			$final_table = $galfram_tables_functions->galfram_table_build_final_table_body_rows( $specs_group_id, $post_id, $final_table );

	    	$prev_final_table = get_post_meta( $post_id, 'galfram_prp_table_array', true );

	    	if ( $final_table != $prev_final_table ) {
	    		update_post_meta( $post_id, 'galfram_prp_table_array', $final_table, $prev_final_table );
	    	}

	    }

	    else if ( $table_type == 'prp-table-custom' ) {

	    	// let's build our final table array
			$final_table = $galfram_tables_functions->galfram_table_custom_build( $post_id );

	    	$prev_final_table = get_post_meta( $post_id, 'galfram_prp_table_custom_array', true );

	    	if ( $final_table != $prev_final_table ) {
	    		update_post_meta( $post_id, 'galfram_prp_table_custom_array', $final_table, $prev_final_table );
	    	}
	    	
	    }

	    // else if ( $table_type == 'prp-table-api' ) {
	    // 	$prev_final_table = get_post_meta( $post_id, 'galfram_prp_table_api_array', true );
	    // 	if ( $final_table != $prev_final_table ) {
	    // 		update_post_meta( $post_id, 'galfram_prp_table_api_array', $final_table, $prev_final_table );
	    // 	}
	    // }

	    else {
	    	return;
	    }


	}


}

