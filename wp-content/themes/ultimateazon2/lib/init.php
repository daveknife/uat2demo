<?php
/**
 * ultimateazon2
 *
 * WARNING: This file is part of the core Ultimate Azon Theme 2 Theme. DO NOT edit this file under any circumstances. Do all modifications in a child theme. http://codex.wordpress.org/Child_Themes
 *
 * @package ultimateazon2
 * @author  Dave Nicosia
 * @license GPL-2.0+
 * @link    https://ultimateazontheme.com
 */

function uat2_constants() {
    do_action('uat2_constants');
}

add_action( 'uat2_constants', 'uat2_constants_function' );
/**
 * This function defines the Ultimate Azon Theme 2 theme constants
 *
 * @since 1.0.0
 */
function uat2_constants_function() {

	//* Define Theme Info Constants
	define( 'PARENT_THEME_NAME', 'Ultimate Azon Theme 2.0' );
	define( 'PARENT_THEME_VERSION', '2.0.3' );

	//* Define Directory Location Constants
	define( 'PARENT_DIR', get_template_directory() );
	define( 'CHILD_DIR', get_stylesheet_directory() );
	define( 'uat2_LIB_DIR', PARENT_DIR . '/lib' );
	define( 'uat2_CSS_DIR', uat2_LIB_DIR . '/css' );
	define( 'uat2_FONTS_DIR', uat2_CSS_DIR . '/fonts' );
	define( 'uat2_USER_CSS_DIR', uat2_CSS_DIR . '/usercss' );
	define( 'uat2_FUNCTIONS_DIR', uat2_LIB_DIR . '/functions' );
	define( 'uat2_IMAGES_DIR', PARENT_DIR . '/images' );
	define( 'uat2_JS_DIR', uat2_LIB_DIR . '/js' );
	define( 'uat2_PLUGINS_DIR', uat2_LIB_DIR . '/plugins' );
	define( 'uat2_SCSS_DIR', uat2_LIB_DIR . '/scss' );
	define( 'uat2_SHORTCODES_DIR', uat2_LIB_DIR . '/shortcodes' );
	define( 'uat2_STRUCTURE_DIR', uat2_LIB_DIR . '/structure' );
	define( 'uat2_TRANSLATION_DIR', uat2_LIB_DIR . '/translation' );
	define( 'MAXPRESS_STORE', 'https://ultimateazontheme.com' );
	define( 'MAXPRESS_STORE_ITEM', 'Ultimate Azon Theme 2' );
	
}





function uat2_functions() {
    do_action('uat2_functions');
}

add_action( 'uat2_functions', 'uat2_functions_function' );
/**
 * This function imports the Ultimate Azon Theme 2 theme functions files
 *
 * @since 1.0.0
 */
function uat2_functions_function() {

	// Theme support options
	require_once(uat2_FUNCTIONS_DIR.'/theme-support.php'); 

	// Customize the WordPress admin
	require_once(uat2_FUNCTIONS_DIR.'/admin.php');

	// Licensing
	require_once(uat2_FUNCTIONS_DIR.'/licensing.php'); 

	// WP Head and other cleanup functions
	require_once(uat2_FUNCTIONS_DIR.'/cleanup.php'); 

	// Register scripts and stylesheets
	require_once(uat2_FUNCTIONS_DIR.'/enqueue-scripts.php');

	// Register custom menus and menu walkers
	require_once(uat2_FUNCTIONS_DIR.'/menu.php');
	require_once(uat2_FUNCTIONS_DIR.'/menu-walkers.php');

	// Register sidebars/widget areas
	require_once(uat2_FUNCTIONS_DIR.'/sidebar.php');

	// Makes WordPress comments suck less
	require_once(uat2_FUNCTIONS_DIR.'/comments.php');

	// Replace 'older/newer' post links with numbered navigation
	require_once(uat2_FUNCTIONS_DIR.'/page-navi.php');

	// Adds support for multiple languages
	require_once(uat2_TRANSLATION_DIR.'/translation.php');

	// Functions related to the Front End Code
	require_once(uat2_FUNCTIONS_DIR.'/frontend.php');

	// The theme's custom field are registerd and managed here
	require_once(uat2_FUNCTIONS_DIR.'/custom-fields.php');

	require_once(uat2_FUNCTIONS_DIR.'/stylizer.php');

	require_once(uat2_FUNCTIONS_DIR.'/tools.php');

	// The theme's font choices for the font-family elect fields to use
	//require_once(uat2_FUNCTIONS_DIR.'/fonts.php');

	// The theme's automatic updater functions
	require_once(PARENT_DIR.'/updater/theme-updater.php');



	// The theme's Product Review Engine
	require_once(uat2_PLUGINS_DIR.'/uat2-prp/uat2-prp.php');

	// The theme's Product Review Engine
	require_once(uat2_PLUGINS_DIR.'/uat2-sliders/uat2-sliders.php');

	// The theme's Product Review Engine
	require_once(uat2_PLUGINS_DIR.'/uat2-tables/uat2-tables.php');



	// Adds site styles to the WordPress editor
	//require_once(uat2_FUNCTIONS_DIR.'/editor-styles.php'); 

	// Related post function - no need to rely on plugins
	// require_once(uat2_FUNCTIONS_DIR.'/related-posts.php'); 

	// Use this as a template for custom post types
	// require_once(uat2_FUNCTIONS_DIR.'/custom-post-type.php');

	// Customize the WordPress login menu
	// require_once(uat2_FUNCTIONS_DIR.'/login.php'); 
	
}





function uat2_structure() {
    do_action('uat2_structure');
}

add_action('uat2_structure', 'uat2_structure_function', 10);
/**
 * This function includes our functions for the tructure of the website
 *
 * @since 1.0.0
 */
function uat2_structure_function() {

	// Include the header hooks and functions
	require_once(uat2_STRUCTURE_DIR.'/header.php');

	// Include all of the loops hooks and functions
	require_once(uat2_STRUCTURE_DIR.'/loops.php');

	// Include the footer hooks and functions
	require_once(uat2_STRUCTURE_DIR.'/footer.php');

	// Include the sidebar hooks and functions
	// Sidebars are registered in /ultimateazon2/lib/functions/sidebar.php
	require_once(uat2_STRUCTURE_DIR.'/sidebar.php');

	// Include the mobile navigation hooks and functions
	require_once(uat2_STRUCTURE_DIR.'/offcanvas.php');

	// Include the byline hooks and functions
	require_once(uat2_STRUCTURE_DIR.'/byline.php');

	// Include the comments hooks and functions
	require_once(uat2_STRUCTURE_DIR.'/comments.php');

	// Include the search form hooks and functions
	require_once(uat2_STRUCTURE_DIR.'/searchform.php');

	// Include the pagebuilder hooks and functions
	require_once(uat2_STRUCTURE_DIR.'/pagebuilder/pagebuilder.php');
	
}





function uat2_extensions() {
    do_action('uat2_extensions');
}

add_action('uat2_extensions', 'uat2_extensions_function', 10);
/**
 * This function checks if addons are installed and activates
 *
 * @since 1.0.0
 */
function uat2_extensions_function() {

	// includes the extensions file which tests for our extensions
	require_once(uat2_TRANSLATION_DIR.'/extensions.php');
	
}





function uat2_framework() {
    do_action('uat2_framework');
}

add_action( 'uat2_framework', 'uat2_framework_function' );
/**
 * This function loads our main framework
 *
 * @since 1.0.0
 */
function uat2_framework_function() {

	// Import the main framework here
	require_once(uat2_LIB_DIR.'/framework.php'); 

}



// Load our constants defined above
uat2_constants();

// Load our theme specific functions
uat2_functions();

// Load our framework structure
uat2_structure();

// Load our framework now that all dependencies have been loaded
uat2_framework();
