<?php

// Add "Licenses" link to the "Incomme Galaxy" menu
function uat2_tools_options() {
    // add_theme_page( $page_title, $menu_title, $capability, $menu_slug, $function);
    add_submenu_page('theme-settings', __('Tools - Ultimate Azon Theme','ultimateazon2'), __('Tools','ultimateazon2'), 'edit_theme_options', 'theme-tools', 'uat2_tools_page', '' );
}
// Load the Admin Options page
add_action('admin_menu', 'uat2_tools_options', 200);


function uat2_tools_page () {
?>


    <div class="wrap settings-section">
        <h2><?php _e( 'Ultimate Azon Theme - Tools', 'ultimateazon2' ); ?></h2>
        
        <?php // the ID in the box must be the license option name: uat2_CHILDTHEMEOREXTENSIONNAME_license ?>
        <div class="gf-option-box" id="uat2_license">

            <h3><?php _e( 'Import Export Styles', 'ultimateazon2' ) ?></h3>

            <div class="gf-option-box-inner">

                
                <p><?php _e( 'Import/Export settings, and more tools will be coming in future updates', 'ultimateazon2'); ?>.</p>


            </div>

        </div>


    </div>
    <div class="clear"></div>

<?php
}
