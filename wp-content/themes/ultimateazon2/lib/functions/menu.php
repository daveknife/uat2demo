<?php
/**
 * ultimateazon2
 *
 * WARNING: This file is part of the core Ultimate Azon Theme 2 Theme. DO NOT edit this file under any circumstances. Do all modifications in a child theme. http://codex.wordpress.org/Child_Themes
 *
 * @package ultimateazon2
 * @author  Dave Nicosia
 * @license GPL-2.0+
 * @link    https://ultimateazontheme.com
 */
 

function uat2_register_menus() {
    do_action('uat2_register_menus');
}
add_action('uat2_register_menus', 'uat2_register_menus_function', 10);

function uat2_register_menus_function(){


    // Register menus
    register_nav_menus(
    	array(
    		'main-nav' => __( 'The Main Navigation Bar', 'ultimateazon2' ),
            'top-nav' => __( 'The Top Navigation Links', 'ultimateazon2' ),
            'offcanvas-nav' => __( 'The Mobile Navigation', 'ultimateazon2' ),   // Main nav in header
    		//'footer-links' => __( 'Footer Links', 'ultimateazon2' ) // Secondary nav in footer
    	)
    );



    // The Main Navigation
    function uat2_main_nav() {
         wp_nav_menu(array(
            'container' => false,                           // Remove nav container
            'menu_class' => 'vertical medium-horizontal menu',       // Adding custom nav class
            'items_wrap' => '<ul id="%1$s" class="dropdown menu" data-dropdown-menu data-disable-hover="true" itemscope itemtype="http://schema.org/SiteNavigationElement">%3$s</ul>',
            'theme_location' => 'main-nav',                  // Where it's located in the theme
            'depth' => 4,                                   // Limit the depth of the nav
            'fallback_cb' => 'wp_mainnav_fallback',                         // Fallback function (see below)
            'walker' => new Topbar_Menu_Walker()
        ));
    } /* End Top Menu */

    // The Main Navigation
    function uat2_top_nav() {
         wp_nav_menu(array(
            'container' => false,                           // Remove nav container
            'menu_class' => 'vertical medium-horizontal menu',       // Adding custom nav class
            'items_wrap' => '<ul id="%1$s" class="dropdown menu" data-dropdown-menu data-disable-hover="true" itemscope itemtype="http://schema.org/SiteNavigationElement">%3$s</ul>',
            'theme_location' => 'top-nav',                  // Where it's located in the theme
            'depth' => 2,                                   // Limit the depth of the nav
            'fallback_cb' => false,                         // Fallback function (see below)
            'walker' => new Topbar_Menu_Walker()
        ));
    } /* End Top Menu */

    // The Off Canvas Menu
    function uat2_off_canvas_nav() {
    	 wp_nav_menu(array(
            'container' => false,                           // Remove nav container
            'menu_class' => 'vertical menu',       // Adding custom nav class
            'items_wrap' => '<ul id="%1$s" class="%2$s" data-accordion-menu itemscope itemtype="http://schema.org/SiteNavigationElement">%3$s</ul>',
            'theme_location' => 'offcanvas-nav',        			// Where it's located in the theme
            'depth' => 5,                                   // Limit the depth of the nav
            'fallback_cb' => 'wp_mobilenav_fallback',                // Fallback function (see below)
            'walker' => new Off_Canvas_Menu_Walker()
        ));
    } /* End Off Canvas Menu */

    function wp_mainnav_fallback() {
        echo '<span class="no-nav no-main-nav">Please create your main navigation.</span>';
    }

    function wp_mobilenav_fallback() {
        echo '<span class="no-nav no-offcanvas-nav">Please create your mobile navigation.</span>';
    }

}
uat2_register_menus();