<div class="row">
<div class="columns small-12">
<?php the_sub_field('before_slider'); ?>
<?php
if ( get_sub_field('override_slider_colors') && get_sub_field('slider_type' ) ) {

	$slider_type = get_sub_field('slider_type');

	$slide_bg_color = get_sub_field('slide_bg_color');
	$slide_border_color = get_sub_field('slide_border_color');
	$slide_text_color = get_sub_field('slide_text_color');
	$slide_link_color = get_sub_field('slide_link_color');
	$slider_navigation_color = get_sub_field('slider_navigation_color');

	if ( $slider_type == 'protool') {
		$spec_bg_color = get_sub_field('spec_bg_color');
		$spec_text_color = get_sub_field('spec_text_color');
		$spec_border_color = get_sub_field('spec_border_color');
		$slide_heading_color = get_sub_field('slide_heading_color');
		$slider_colors = ' data-slider-type="'.$slider_type.'" data-color-specbg="'.$spec_bg_color.'" data-color-spectext="'.$spec_text_color.'" data-color-slidebg="'.$slide_bg_color.'" data-color-slidetext="'.$slide_text_color.'" data-color-slidelinkcolor="'.$slide_link_color.'" data-color-slidebordercolor="'.$slide_border_color.'" data-color-specbordercolor="'.$spec_border_color.'" data-color-slideheader="'.$slide_heading_color.'" data-color-nav="'.$slider_navigation_color.'"';
	}
	else if ( $slider_type == 'prp' ) {
		$slider_colors = ' data-slider-type="'.$slider_type.'" data-color-slidebg="'.$slide_bg_color.'" data-color-slidelinkcolor="'.$slide_link_color.'" data-color-slidebordercolor="'.$slide_border_color.'" data-color-slidetext="'.$slide_text_color.'" data-color-nav="'.$slider_navigation_color.'"';
	}
	else {
		$slider_colors = ' data-slider-type="'.$slider_type.'" data-color-slidebg="'.$slide_bg_color.'" data-color-slidelinkcolor="'.$slide_link_color.'" data-color-slidetext="'.$slide_text_color.'" data-color-nav="'.$slider_navigation_color.'"';
	}

}
else {
	$slider_colors = '';
}
?>
<div class="pb-slider-wrap"<?php echo $slider_colors; ?>>
<?php echo do_shortcode('[uat2_slider id="'.get_sub_field('choose_a_slider').'" /]'); ?>
</div>
<?php the_sub_field('after_slider'); ?>
</div>
</div>