<?php

function uat2_pagebuilder() {
    do_action('uat2_pagebuilder');
}
add_action('uat2_pagebuilder', 'uat2_pagebuilder_function', 10);

function uat2_pagebuilder_function(){

	if( have_rows('uat2_page_builder') ):
		$counter = 0;
		while ( have_rows('uat2_page_builder') ) : the_row();
			$bg_color = get_sub_field('background_color');
			echo '<div class="pb pb-'.get_row_layout().' pb-section-'.$counter.'" '.(!empty($bg_color) ? ' style="background-color: '.$bg_color.'" ' : '').'>';
				get_template_part( 'lib/structure/pagebuilder/pb', get_row_layout() );
			echo '</div>';
			$counter++;
		endwhile;
	else:
		echo '<div class="row"><div class="large-12 columns first">';
			echo '<h1>Add some Page Builder Sections!</h1>';
		echo '</div></div>';
	endif;


}
