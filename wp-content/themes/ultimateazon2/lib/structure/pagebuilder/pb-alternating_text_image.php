<?php 
if ( get_sub_field('alternate_rows') ) { $row_alignment = ' uat-alternate-rows'; }
else { $row_alignment = ' yo'; }
?>
<?php if( have_rows('text_image_rows') ): ?>
	<?php while ( have_rows('text_image_rows') ) : the_row(); ?>
		<div class="row<?php echo $row_alignment; ?>">
			<div class="columns small-12 medium-6 uat-col-image">
				<?php 
				$thumb_id = get_sub_field('image'); 
				$thumb_url_array = wp_get_attachment_image_src($thumb_id, 'full', true);
				$img_src = $thumb_url_array[0];
				?>
				<img src="<?php echo $img_src; ?>" alt="" />
			</div>
			<div class="columns small-12 medium-6 uat-col-text">
				<?php the_sub_field('content'); ?>
			</div>
		</div>
	<?php endwhile; ?>
<?php endif; ?>
