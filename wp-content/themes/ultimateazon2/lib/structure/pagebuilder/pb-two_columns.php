<div class="row">
<div class="columns small-12 medium-6">
<?php the_sub_field('column_1'); ?>
</div>
<div class="columns small-12 medium-6">
<?php the_sub_field('column_2'); ?>
</div>
</div>