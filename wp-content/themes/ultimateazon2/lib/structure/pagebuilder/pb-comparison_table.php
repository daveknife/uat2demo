<div class="row">
<div class="columns small-12">
<?php the_sub_field('before_table'); ?>
<?php
if ( get_sub_field('override_table_colors') ) {

	$table_bg_color = get_sub_field('table_background_color');
	$table_text_color = get_sub_field('table_text_color');
	$table_link_color = get_sub_field('table_link_color');
	$table_border_color = get_sub_field('table_border_color');
	$table_header_bg_color = get_sub_field('table_header_bg_color');
	$table_header_text_color = get_sub_field('table_header_text_color');

	$table_colors = ' data-table-headerbgcolor="'.$table_header_bg_color.'" data-table-headertextcolor="'.$table_header_text_color.'" data-color-tablebg="'.$table_bg_color.'" data-color-tablelinkcolor="'.$table_link_color.'" data-color-tabletext="'.$table_text_color.'" data-table-bordercolor="'.$table_border_color.'"';

}
else {
	$table_colors = '';
}
?>
<div class="pb-table-wrap"<?php echo $table_colors; ?>>
<?php echo do_shortcode('[uat2_table id="'.get_sub_field('choose_comparison_table').'" /]'); ?>
</div>
<?php the_sub_field('after_table'); ?>
</div>
</div>