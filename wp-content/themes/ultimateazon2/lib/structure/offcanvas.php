<?php
/**
 * ultimateazon2
 *
 * WARNING: This file is part of the core Ultimate Azon Theme 2 Theme. DO NOT edit this file under any circumstances. Do all modifications in a child theme. http://codex.wordpress.org/Child_Themes
 *
 * @package ultimateazon2
 * @author  Dave Nicosia
 * @license GPL-2.0+
 * @link    https://ultimateazontheme.com
 */



// hook for getting header content
function uat2_offcanvas() {
    do_action('uat2_offcanvas');
}
add_action('uat2_offcanvas', 'uat2_offcanvas_function', 10);

function uat2_offcanvas_function(){
?>
	<div class="off-canvas" id="off-canvas">
		<?php

		echo '<div class="offcanvas-header-area">';
			uat2_site_title();
			echo '<button class="js-open-offcanvas c-hamburger c-hamburger--htx hide-for-large is-active">';
				echo '<span>' . __( 'Toggle Menu','ultimateazon2' ) . '</span>';
			echo '</button>';
		echo '</div>';
		
		uat2_off_canvas_nav();
		if ( is_active_sidebar( 'offcanvas' ) ) :
			echo '<div class="offcanvas-widget-area">';
				dynamic_sidebar( 'offcanvas' );
			echo '</div>';
		endif;
		?>
	</div>

<?php
}