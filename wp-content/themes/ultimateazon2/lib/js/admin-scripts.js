
jQuery(document).ready(function() {

    // license activate button trigger
	jQuery('body').on('click', '.incomegalaxy-activate', function() {
		// identify current license box
		var current_license_box = jQuery(this).closest('.gf-option-box');
		// get ID of current license box
        var current_license_box_ID = current_license_box.attr('id');
        // get the license value in the license field
		var license = current_license_box.find('.incomegalaxy-license').attr('value');
        // get the current product name
        var incomegalaxy_product = current_license_box.find('.incomegalaxy-product').attr('value');
        // set our ajax call data
		var data = {
            action: 'activate_incomegalaxy_license',
            license_box_ID: current_license_box_ID,
            incomegalaxy_product: incomegalaxy_product,
            license: license
        };
        if ( license != '' ) {
        	// show the loading spinner and add a class to identify it later to remove on completion
        	current_license_box.find('.license-loading-gif').addClass('spinner-on').show();
        	// call our deactivate ajax function and pass the data to it
        	activate_incomegalaxy_license(data);
        }
        else {
        	alert('Your license field does not contain a valid license. Please try again.');
        }
	});

	function activate_incomegalaxy_license(data) {
		jQuery.ajax({
            type: 'POST',
            url: ajaxurl,
            data: data,
            dataType: 'json',
            complete: function(response){
                jQuery('.spinner-on').hide();
            },
            success: function(response) {
            	if ( response.result == 'valid' ) {
                    var current_license_box = '#'+response.license_box_ID;
                    jQuery(current_license_box).find('.license-message').html('Your license is active.').addClass('ultimateazon2-license-valid').removeClass('ultimateazon2-license-deactivated');
                    jQuery(current_license_box).find('.incomegalaxy-activate').removeClass('incomegalaxy-activate').addClass('incomegalaxy-deactivate').attr('value', 'Dectivate');
                }
                else if ( response.result == 'invalid' ) {
                    alert('This license is invalid. Please try again.');
                }
            }
        });
	}


    // license deactivate button trigger
    jQuery('body').on('click', '.incomegalaxy-deactivate', function() {
        // identify current license box
        var current_license_box = jQuery(this).closest('.gf-option-box');
        // get ID of current license box
        var current_license_box_ID = current_license_box.attr('id');
        // get the license value in the license field
        var license = current_license_box.find('.incomegalaxy-license').attr('value');
        // get the current product name
        var incomegalaxy_product = current_license_box.find('.incomegalaxy-product').attr('value');
        // create our data for our ajax call
        var data = {
            action: 'deactivate_incomegalaxy_license',
            license_box_ID: current_license_box_ID,
            incomegalaxy_product: incomegalaxy_product,
            license: license
        };
        // show the loading spinner and add a class to identify it later to remove on completion
        current_license_box.find('.license-loading-gif').addClass('spinner-on').show();
        // call our deactivate ajax function and pass the data to it
        deactivate_incomegalaxy_license(data);
    });

    function deactivate_incomegalaxy_license(data) {
        jQuery.ajax({
            type: 'POST',
            url: ajaxurl,
            data: data,
            dataType: 'json',
            complete: function(response){
                jQuery('.spinner-on').hide();
            },
            success: function(response){
                console.log(data);
                if ( response.result == 'deactivated' ) {
                    var current_license_box = '#'+response.license_box_ID;
                    jQuery(current_license_box).find('.license-message').html('The license key you entered has been deactivated on this website.').addClass('ultimateazon2-license-deactivated').removeClass('ultimateazon2-license-valid');
                    jQuery(current_license_box).find('.incomegalaxy-deactivate').removeClass('incomegalaxy-deactivate').addClass('incomegalaxy-activate').attr('value', 'Activate');

                } 
            }
        });
    }




});