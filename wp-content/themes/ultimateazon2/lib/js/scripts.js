jQuery(document).foundation();
/* 
These functions make sure WordPress 
and Foundation play nice together.
*/



jQuery(document).ready(function() {

	//jQuery(document).foundation();

    
    // Remove empty P tags created by WP inside of Accordion and Orbit
    jQuery('.accordion p:empty, .orbit p:empty').remove();
    
	 // Makes sure last grid item floats left
	jQuery('.archive-grid .columns').last().addClass( 'end' );
	
	// Adds Flex Video to YouTube and Vimeo Embeds
	jQuery('iframe[src*="youtube.com"], iframe[src*="vimeo.com"]').wrap("<div class='flex-video'/>");



	// function to re-initialize equalizer on resize
	function resize_equalizer_boxes() {
		jQuery(window).on('resize', function(e) {
			initialize_equalizer_boxes()
		});
	}

	// function to initialize equalizer
	function initialize_equalizer_boxes() {
		var equalized = new Foundation.Equalizer( jQuery('.panel'), {
			equalizeOnStack: false,
			equalizeByRow: true
		});
	}

	// let's equalize our archive grid by rows
	if ( ( jQuery('#content').hasClass('content-grid') || jQuery('#content').hasClass('content-grid-sidebar') || jQuery('#content').hasClass('sidebar-content-grid')  ) && ( jQuery('body').hasClass('blog') || jQuery('body').hasClass('archive') ) ) {

		// re-equalizes the panels after the initial interchange src load
		jQuery('img').on('load', function(){
			// reinitialize equazlizer
			Foundation.reInit('equalizer');
		}); 

		// initialize equazlizer
		initialize_equalizer_boxes();
		// re-initialize panels on resize
		resize_equalizer_boxes();
	}



	if ( jQuery('body.logged-in').length ) {
		var header_height = jQuery('header.header').outerHeight();
		var wp_adminbar_height = jQuery('#wpadminbar').outerHeight();
		var top_offset = header_height + wp_adminbar_height;
	}
	else {
		var top_offset = jQuery('header.header').outerHeight();
	}
	jQuery('.offcanvas-header-area').css('minHeight', top_offset);


	jQuery('body').on('click', '.js-open-offcanvas, .page-overlay', function () {

		//menu_icon = jQuery('.c-hamburger');
		
		jQuery('.off-canvas').toggleClass('menu-open');
		//menu_icon.toggleClass('is-active');
		jQuery('#page-overlay').toggleClass('active');
		jQuery('body').toggleClass('scroll-locked');

	});

	
	
	if( jQuery('header.header').hasClass('sticky-mobile') ) {
		var sticky = new Waypoint.Sticky({
			element: jQuery('header.header')[0],
			wrapper: '<div class="sticky-wrapper-small" />'
		});
	}

	if( jQuery('.nav-primary').hasClass('sticky-desktop') ) {
		var sticky = new Waypoint.Sticky({
			element: jQuery('.nav-primary')[0],
			wrapper: '<div class="sticky-wrapper-large" />'
		});
	}

	
	jQuery(window).on('changed.zf.mediaquery', function(event, newSize, oldSize) {
	
		if ( newSize == 'medium' || newSize == 'small' ) {
			var header_height = jQuery('.header-inner').outerHeight();
			jQuery('.sticky-wrapper-small').css('height', header_height);
		}

	});


	if ( jQuery('body').hasClass('uat-pb') ) {

		var color_specbg = '';
		var color_spectext = '';
		var color_slidebg = '';
		var color_slidetext = '';
		var color_slideheader = '';
		var slide_link_color = '';
		var slide_border_color = '';
		var spec_border_color = '';
		var slider_color_nav = '';

		

		jQuery( ".pb-slider-wrap" ).each(function( index ) {

			var slider_type = jQuery(this).attr('data-slider-type');

			var slider_color_nav = jQuery(this).attr('data-color-nav');

			if ( slider_type == 'protool' ) {

				color_specbg = jQuery(this).attr('data-color-specbg');
				color_spectext = jQuery(this).attr('data-color-spectext');
				color_slidebg = jQuery(this).attr('data-color-slidebg');
				color_slidetext = jQuery(this).attr('data-color-slidetext');
				color_slideheader = jQuery(this).attr('data-color-slideheader');
				slide_link_color = jQuery(this).attr('data-color-slidelinkcolor');
				slide_border_color = jQuery(this).attr('data-color-slidebordercolor');
				spec_border_color = jQuery(this).attr('data-color-specbordercolor');

				jQuery(this).find('.galfram-protool-spec').slice(1).css({ 'background':color_specbg, 'borderBottomColor':spec_border_color, 'borderLeftColor':spec_border_color, 'borderRightColor':spec_border_color } );
				jQuery(this).find('.galfram-protool-spec:nth-child(2)').css( 'borderTopColor', spec_border_color );
				jQuery(this).find('.galfram-protool-spec').slice(1).css( 'color', color_spectext );
				jQuery(this).find('.galfram-protool-spec-value').slice(1).css({'background': color_slidebg, 'borderBottomColor':slide_border_color, 'borderLeftColor':slide_border_color, 'borderRightColor':slide_border_color });
				jQuery(this).find('.galfram-protool-spec-value:nth-child(2)').css( 'borderTopColor', slide_border_color );
				jQuery(this).find('.galfram-protool-spec-value').slice(1).css( 'color', color_slidetext );
				jQuery(this).find('.protool-row-0').css( 'color', color_slideheader );
				jQuery(this).find('.galfram-protool-spec-value a').slice(1).css( 'color', slide_link_color );

				jQuery(this).find('.slick-arrow:before').css( 'color', slider_color_nav );


			}
			else if ( slider_type == 'prp' ) {

				color_slidebg = jQuery(this).attr('data-color-slidebg');
				color_slidetext = jQuery(this).attr('data-color-slidetext');
				slide_link_color = jQuery(this).attr('data-color-slidelinkcolor');
				slide_border_color = jQuery(this).attr('data-color-slidebordercolor');
				
				slider_color_nav = jQuery(this).attr('data-color-nav');

				
				jQuery(this).find('.galfram-prp-spec-value').css( {'backgroundColor': color_slidebg, 'color': color_slidetext, 'borderBottomColor':slide_border_color, 'borderRightColor':slide_border_color } );
				jQuery(this).find('.galfram-prp-spec-value a').css( 'color', slide_link_color );
				jQuery(this).find('.galfram-prp-slider').css( 'borderColor', slide_border_color );

				


			}
			else if ( slider_type == 'posts-pages' || slider_type == 'custom' ) {

				color_slidebg = jQuery(this).attr('data-color-slidebg');
				color_slidetext = jQuery(this).attr('data-color-slidetext');
				slide_link_color = jQuery(this).attr('data-color-slidelinkcolor');
				

				jQuery(this).find('.galfram-slide').css( {'backgroundColor': color_slidebg, 'color': color_slidetext } );
				jQuery(this).find('.galfram-slide a').css( 'color', slide_link_color );

			}

			if ( slider_color_nav === 'light' ) {
				jQuery(this).find('.galfram-slider-outer').addClass('slider-nav-light');
			}
			else if ( slider_color_nav === 'dark' ) {
				jQuery(this).find('.galfram-slider-outer').addClass('slider-nav-dark');
			}

		});



		jQuery( ".pb-table-wrap" ).each(function( index ) {

			color_tablebg = jQuery(this).attr('data-color-tablebg');
			color_tabletext = jQuery(this).attr('data-color-tabletext');
			table_link_color = jQuery(this).attr('data-color-tablelinkcolor');
			table_border_color = jQuery(this).attr('data-table-bordercolor');
			table_headerbg_color = jQuery(this).attr('data-table-headerbgcolor');
			table_headertext_color = jQuery(this).attr('data-table-headertextcolor');

			jQuery(this).find('tbody').css( {'backgroundColor': color_tablebg, 'color': color_tabletext } );
			jQuery(this).find('tbody tr td a').css( 'color', table_link_color );
			jQuery(this).find('tbody tr').css( 'borderColor', table_border_color );
			jQuery(this).find('thead th').css( {'backgroundColor': table_headerbg_color, 'color': table_headertext_color });


		});
		

	}


	// check to see if Event Tracking is enabled && Analytics is Installed
	if ( jQuery('body').hasClass('ga-event-tracking') && typeof ga === 'function' ) {

		console.log('GA loaded');




		/********* SINGLE REVIEW PAGE **************/

		// 1. Review Single - Sidebar - Bottom Affiliate Button
		jQuery('body.single-galfram_review').on('click', '.sidebar .gtm-sidebar-ama-btn', function () {
			var cat = 'Review Single ('+jQuery('h1.page-title').text()+') ~ Sidebar';
			var action = 'Click';
			var label = 'Bottom Affiliate Button - '+event.target.href;
			handleOutboundLinkClicks( cat, action, label );
		});

		// 2. Review Single - Sidebar - Custom Spec Button
		jQuery('body.single-galfram_review').on('click', '.sidebar .gtm-spec-button', function () {
			var cat = 'Review Single ('+jQuery('h1.page-title').text()+') ~ Sidebar';
			var action = 'Click';
			var label = 'Custom Spec Button - '+event.target.href;
			handleOutboundLinkClicks( cat, action, label );
		});

		// 3. Review Single - Top Link Bar
		jQuery('body.single-galfram_review').on('click', '.gtm-top-linkbar', function () {
			var cat = 'Review Single ('+jQuery('h1.page-title').text()+')';
			var action = 'Click';
			var label = 'Top Link Bar - '+event.target.href;
			handleOutboundLinkClicks( cat, action, label );
		});

		// 4. Review Single - Bottom Link Bar
		jQuery('body.single-galfram_review').on('click', '.gtm-bottom-linkbar', function () {
			var cat = 'Review Single ('+jQuery('h1.page-title').text()+')';
			var action = 'Click';
			var label = 'Bottom Link Bar - '+event.target.href;
			handleOutboundLinkClicks( cat, action, label );
		});

		// 5. Review Single - Swap Main Image
		jQuery('body.single-galfram_review').on('click', '.gtm-swap-review-img', function () {
			var cat = 'Review Single ('+jQuery('h1.page-title').text()+')';
			var action = 'Image Swap';
			var label = 'Image URL - '+event.target.src;
			handleOutboundLinkClicks( cat, action, label );
		});

		// 6. Review Single - Featured Image Link
		jQuery('body.single-galfram_review').on('click', '.gtm-review-featured-img-link', function () {
			var cat = 'Review Single ('+jQuery('h1.page-title').text()+')';
			var action = 'Click';
			var label = 'Featured Image - '+event.target.src;
			handleOutboundLinkClicks( cat, action, label );
		});

		// 7. Review Single - Sidebar Text Link Only
		jQuery('body.single-galfram_review').on('click', '.specs-table-row a:not(.gtm-spec-button, .gtm-sidebar-ama-btn)', function () {
			var cat = 'Review Single ('+jQuery('h1.page-title').text()+')';
			var action = 'Click';
			var label = 'Custom Spec Text Link - '+event.target.href;
			handleOutboundLinkClicks( cat, action, label );
		});



		/********* REVIEW MAIN & CATEGORY ARCHIVES **************/

		// 8. Review Archive - Quickview Click
		jQuery('body.post-type-archive-galfram_review').on('click', '.gtm-quickview-trigger', function () {
			var cat = 'Reviews Archive - '+jQuery(this).closest('.galfram_review').find('h2 a').text();
			var action = 'Quick View';
			var label = 'Quick View Trigger';
			handleOutboundLinkClicks( cat, action, label );
		});

		// 9. Review Archive - Quickview Top Image Link
		jQuery('body.post-type-archive-galfram_review').on('click', '.gtm-quickview-top-imglink', function () {
			var cat = 'Reviews Archive - '+jQuery(this).closest('.galfram-quickview-top').find('h3').text();
			var action = 'Quick View';
			var label = 'Top Image Affiliate Button';
			handleOutboundLinkClicks( cat, action, label );
		});

		// 10. Review Archive - Quickview Top Affiliate Button
		jQuery('body.post-type-archive-galfram_review').on('click', '.gtm-quickview-top-afflink', function () {
			var cat = 'Reviews Archive - '+jQuery(this).closest('.galfram-quickview-top').find('h3').text();
			var action = 'Quick View';
			var label = 'Top Affiliate Button';
			handleOutboundLinkClicks( cat, action, label );
		});

		// 11. Review Archive - Quickview Top Full Review Button
		jQuery('body.post-type-archive-galfram_review').on('click', '.gtm-quickview-top-fulllink', function () {
			var cat = 'Reviews Archive - '+jQuery(this).closest('.galfram-quickview-top').find('h3').text();
			var action = 'Quick View';
			var label = 'Top Full Review Button';
			handleOutboundLinkClicks( cat, action, label );
		});

		// 12. Review Archive - Quickview Spec Custom Button
		jQuery('body.post-type-archive-galfram_review').on('click', '.md-content-inner .gtm-spec-button', function () {
			var cat = 'Reviews Archive - '+jQuery(this).closest('.md-content').find('.galfram-quickview-top h3').text();
			var action = 'Quick View';
			var label = 'Custom Spec Button - '+this.href;
			handleOutboundLinkClicks( cat, action, label );
		});

		// 13. Review Archive - Quickview Bottom Affiliate Button
		jQuery('body.post-type-archive-galfram_review').on('click', '.gtm-quickview-bottom-afflink', function () {
			var cat = 'Reviews Archive - '+jQuery(this).closest('.md-content').find('.galfram-quickview-top h3').text();
			var action = 'Quick View';
			var label = 'Bottom Affiliate Button';
			handleOutboundLinkClicks( cat, action, label );
		});

		// 14. Review Archive - Quick View Text Link Only
		jQuery('body.post-type-archive-galfram_review').on('click', '.md-content-inner .specs-table-row a:not(.gtm-spec-button, .gtm-sidebar-ama-btn)', function () {
			var cat = 'Reviews Archive - '+jQuery(this).closest('.md-content').find('.galfram-quickview-top h3').text();
			var action = 'Quick View';
			var label = 'Custom Spec Text Link - '+event.target.href;
			handleOutboundLinkClicks( cat, action, label );
		});

		// 15. Review Archive - Review Category Tag Link
		jQuery('body.post-type-archive-galfram_review').on('click', '.galfram_review .gtm-category-tag', function () {
			var cat = 'Reviews Archive - '+jQuery(this).closest('.galfram_review').find('.article-header h2 a').text();
			var action = 'Click';
			var label = 'Review Category Link - '+event.target.text;
			handleOutboundLinkClicks( cat, action, label );
		});

		// 16. Review Archive - Review Title
		jQuery('body.post-type-archive-galfram_review').on('click', '.galfram_review .gtm-review-title', function () {
			var cat = 'Reviews Archive - '+jQuery(this).text();
			var action = 'Click';
			var label = 'Review Title Link - '+event.target.text;
			handleOutboundLinkClicks( cat, action, label );
		});

		// 17. Review Archive - Quickview Click
		jQuery('body.tax-galfram_review_cat').on('click', '.gtm-quickview-trigger', function () {
			var cat = 'Review Category Archive - '+jQuery('h1.archive-title').text()+' - '+jQuery(this).closest('.galfram_review').find('h2 a').text();
			var action = 'Quick View';
			var label = 'Quick View Trigger';
			handleOutboundLinkClicks( cat, action, label );
		});

		// 18. Review Archive - Quickview Top Image Link
		jQuery('body.tax-galfram_review_cat').on('click', '.gtm-quickview-top-imglink', function () {
			var cat = 'Review Category Archive - '+jQuery('h1.archive-title').text()+' - '+jQuery(this).closest('.galfram_review').find('h2 a').text();
			var action = 'Quick View';
			var label = 'Top Image Affiliate Button';
			handleOutboundLinkClicks( cat, action, label );
		});

		// 19. Review Archive - Quickview Top Affiliate Button
		jQuery('body.tax-galfram_review_cat').on('click', '.gtm-quickview-top-afflink', function () {
			var cat = 'Review Category Archive - '+jQuery('h1.archive-title').text()+' - '+jQuery(this).closest('.galfram_review').find('h2 a').text();
			var action = 'Quick View';
			var label = 'Top Affiliate Button';
			handleOutboundLinkClicks( cat, action, label );
		});

		// 20. Review Archive - Quickview Top Full Review Button
		jQuery('body.tax-galfram_review_cat').on('click', '.gtm-quickview-top-fulllink', function () {
			var cat = 'Review Category Archive - '+jQuery('h1.archive-title').text()+' - '+jQuery(this).closest('.galfram_review').find('h2 a').text();
			var action = 'Quick View';
			var label = 'Top Full Review Button';
			handleOutboundLinkClicks( cat, action, label );
		});

		// 21. Review Archive - Quickview Spec Custom Button
		jQuery('body.tax-galfram_review_cat').on('click', '.md-content-inner .gtm-spec-button', function () {
			var cat = 'Review Category Archive - '+jQuery('h1.archive-title').text()+' - '+jQuery(this).closest('.galfram_review').find('h2 a').text();
			var action = 'Quick View';
			var label = 'Custom Spec Button - '+this.href;
			handleOutboundLinkClicks( cat, action, label );
		});

		// 22. Review Archive - Quickview Bottom Affiliate Button
		jQuery('body.tax-galfram_review_cat').on('click', '.gtm-quickview-bottom-afflink', function () {
			var cat = 'Review Category Archive - '+jQuery('h1.archive-title').text()+' - '+jQuery(this).closest('.galfram_review').find('h2 a').text();
			var action = 'Quick View';
			var label = 'Bottom Affiliate Button';
			handleOutboundLinkClicks( cat, action, label );
		});

		// 23. Review Archive - Quick View Text Link Only
		jQuery('body.tax-galfram_review_cat').on('click', '.md-content-inner .specs-table-row a:not(.gtm-spec-button, .gtm-sidebar-ama-btn)', function () {
			var cat = 'Review Category Archive - '+jQuery('h1.archive-title').text()+' - '+jQuery(this).closest('.galfram_review').find('h2 a').text();
			var action = 'Quick View';
			var label = 'Custom Spec Text Link - '+event.target.href;
			handleOutboundLinkClicks( cat, action, label );
		});

		// 24. Review Archive - Review Category Tag Link
		jQuery('body.tax-galfram_review_cat').on('click', '.galfram_review .gtm-category-tag', function () {
			var cat = 'Review Category Archive - '+jQuery('h1.archive-title').text()+' - '+jQuery(this).closest('.galfram_review').find('h2 a').text();
			var action = 'Click';
			var label = 'Review Category Link - '+event.target.text;
			handleOutboundLinkClicks( cat, action, label );
		});

		// 25. Review Archive - Review Title
		jQuery('body.tax-galfram_review_cat').on('click', '.galfram_review .gtm-review-title', function () {
			var cat = 'Review Category Archive - '+jQuery('h1.archive-title').text();
			var action = 'Click';
			var label = 'Review Title Link - '+jQuery(this).text();
			handleOutboundLinkClicks( cat, action, label );
		});


		/********* SLIDER EVENTS **************/
		/*** PRP SLIDER ***/


		// 26. PRP Slider - Spec Cover Link
		jQuery('body').on('click', '.galfram-prp-slider .gtm-field-linkcover', function () {
			var cat = 'Reviews Slider - '+jQuery('.article-header h1').text()+' - '+jQuery(this).closest('.galfram-slider-outer').attr('data-slider-name');
			var action = 'Click';
			var label = 'Spec Cover Link - '+jQuery(this).closest('.prpslider-row').attr('data-event-pos')+' - '+event.target.href;
			handleOutboundLinkClicks( cat, action, label );
		});

		// 27. PRP Slider - Spec Custom Button Link
		jQuery('body').on('click', '.galfram-prp-slider .gtm-spec-button', function () {
			var cat = 'Slider - '+jQuery('.article-header h1').text()+' - '+jQuery(this).closest('.galfram-slider-outer').attr('data-slider-name');
			var action = 'Click';
			if ( event.target.href ) {
				var target_url = event.target.href;
			} else {
				var target_url = jQuery(this).attr('href');
			}
			var label = 'Custom Spec Button - '+jQuery(this).closest('.prpslider-row').attr('data-event-pos')+' - '+target_url;
			handleOutboundLinkClicks( cat, action, label );
		});

		// 28. PRP Slider - Text Link Only
		jQuery('body').on('click', '.galfram-prp-slider a:not(.gtm-spec-button, .gtm-sidebar-ama-btn)', function () {
			var cat = 'Reviews Slider - '+jQuery('.article-header h1').text()+' - '+jQuery(this).closest('.galfram-slider-outer').attr('data-slider-name');
			var action = 'Click';
			var label = 'Custom Spec Text Link - '+jQuery(this).closest('.prpslider-row').attr('data-event-pos')+' - '+event.target.href;
			handleOutboundLinkClicks( cat, action, label );
		});

		// 29. PRP Slider - Controls - Dots
		jQuery('body').on('click', '.galfram-prp-slider .slick-dots li button', function () {
			var cat = 'Reviews Slider Controls - '+jQuery('.article-header h1').text()+' - '+jQuery(this).closest('.galfram-slider-outer').attr('data-slider-name');
			var action = 'Control Interaction';
			var li_index = jQuery(this).parent().index()+1;
			var label = 'Dots Button - '+li_index;
			handleOutboundLinkClicks( cat, action, label );
		});

		// 30. PRP Slider - Controls - Previous Slide
		jQuery('body').on('click', '.galfram-prp-slider .slick-prev', function () {
			var cat = 'Reviews Slider Controls - '+jQuery('.article-header h1').text()+' - '+jQuery(this).closest('.galfram-slider-outer').attr('data-slider-name');
			var action = 'Control Interaction';
			var label = 'Previous Slide';
			handleOutboundLinkClicks( cat, action, label );
		});

		// 31. PRP Slider - Controls - Next Slide
		jQuery('body').on('click', '.galfram-prp-slider .slick-next', function () {
			var cat = 'Reviews Slider Controls - '+jQuery('.article-header h1').text()+' - '+jQuery(this).closest('.galfram-slider-outer').attr('data-slider-name');
			var action = 'Control Interaction';
			var label = 'Next Slide';
			handleOutboundLinkClicks( cat, action, label );
		});


		/********* SLIDER EVENTS **************/
		/*** ProTool SLIDER ***/


		// 32. ProTool Slider - Spec Cover Link
		jQuery('body').on('click', '.galfram-protool .gtm-field-linkcover', function () {
			var cat = 'Reviews Slider - '+jQuery('.article-header h1').text()+' - '+jQuery(this).closest('.galfram-slider-outer').attr('data-slider-name');
			var action = 'Click';
			var label = 'Spec Cover Link - '+jQuery(this).closest('.protool-row').attr('data-event-pos')+' - '+event.target.href;
			handleOutboundLinkClicks( cat, action, label );
		});

		// 33. ProTool Slider - Spec Custom Button Link
		jQuery('body').on('click', '.galfram-protool .gtm-spec-button', function () {
			var cat = 'Reviews Slider - '+jQuery('.article-header h1').text()+' - '+jQuery(this).closest('.galfram-slider-outer').attr('data-slider-name');
			var action = 'Click';
			if ( event.target.href ) {
				var target_url = event.target.href;
			} else {
				var target_url = jQuery(this).attr('href');
			}
			
			var label = 'Custom Spec Button - '+jQuery(this).closest('.protool-row').attr('data-event-pos')+' - '+target_url;
				
			handleOutboundLinkClicks( cat, action, label );
		});

		// 34. ProTool Slider - Text Link Only
		jQuery('body').on('click', '.galfram-protool a:not(.gtm-spec-button, .gtm-sidebar-ama-btn)', function () {
			var cat = 'Reviews Slider - '+jQuery('.article-header h1').text()+' - '+jQuery(this).closest('.galfram-slider-outer').attr('data-slider-name');
			var action = 'Click';
			var label = 'Custom Spec Text Link - '+jQuery(this).closest('.protool-row').attr('data-event-pos')+' - '+event.target.href;
			handleOutboundLinkClicks( cat, action, label );
		});

		// 35. PRP Slider - Controls - Dots
		jQuery('body').on('click', '.galfram-protool .slick-dots li button', function () {
			var cat = 'Reviews Slider Controls - '+jQuery('.article-header h1').text()+' - '+jQuery(this).closest('.galfram-slider-outer').attr('data-slider-name');
			var action = 'Control Interaction';
			var li_index = jQuery(this).parent().index()+1;
			var label = 'Dots Button - '+li_index;
			handleOutboundLinkClicks( cat, action, label );
		});

		// 36. ProTool Slider - Controls - Previous Slide
		jQuery('body').on('click', '.galfram-protool .slick-prev', function () {
			var cat = 'Reviews Slider Controls - '+jQuery('.article-header h1').text()+' - '+jQuery(this).closest('.galfram-slider-outer').attr('data-slider-name');
			var action = 'Control Interaction';
			var label = 'Previous Slide';
			handleOutboundLinkClicks( cat, action, label );
		});

		// 37. ProTool Slider - Controls - Next Slide
		jQuery('body').on('click', '.galfram-protool .slick-next', function () {
			var cat = 'Reviews Slider Controls - '+jQuery('.article-header h1').text()+' - '+jQuery(this).closest('.galfram-slider-outer').attr('data-slider-name');
			var action = 'Control Interaction';
			var label = 'Next Slide';
			handleOutboundLinkClicks( cat, action, label );
		});


		/********* SLIDER EVENTS **************/
		/*** POSTS SLIDER ***/

		// 38. POSTS Slider - Text Link Only
		jQuery('body').on('click', '.galfram-post-slider .galfram-slide .slide-cover-link', function () {
			var cat = 'Posts Slider - '+jQuery('.article-header h1').text()+' - '+jQuery(this).closest('.galfram-slider-outer').attr('data-slider-name');
			var action = 'Click';
			var label = 'Custom Link - '+event.target.href;
			handleOutboundLinkClicks( cat, action, label );
		});

		// 39. POSTS Slider - Controls - Dots
		jQuery('body').on('click', '.galfram-post-slider .slick-dots li button', function () {
			var cat = 'Posts Slider Controls - '+jQuery('.article-header h1').text()+' - '+jQuery(this).closest('.galfram-slider-outer').attr('data-slider-name');
			var action = 'Control Interaction';
			var li_index = jQuery(this).parent().index()+1;
			var label = 'Dots Button - '+li_index;
			handleOutboundLinkClicks( cat, action, label );
		});

		// 40. POSTS Slider - Controls - Previous Slide
		jQuery('body').on('click', '.galfram-post-slider .slick-prev', function () {
			var cat = 'Posts Slider Controls - '+jQuery('.article-header h1').text()+' - '+jQuery(this).closest('.galfram-slider-outer').attr('data-slider-name');
			var action = 'Control Interaction';
			var label = 'Previous Slide';
			handleOutboundLinkClicks( cat, action, label );
		});

		// 41. POSTS Slider - Controls - Next Slide
		jQuery('body').on('click', '.galfram-post-slider .slick-next', function () {
			var cat = 'Posts Slider Controls - '+jQuery('.article-header h1').text()+' - '+jQuery(this).closest('.galfram-slider-outer').attr('data-slider-name');
			var action = 'Control Interaction';
			var label = 'Next Slide';
			handleOutboundLinkClicks( cat, action, label );
		});


		/********* SLIDER EVENTS **************/
		/*** PAGES SLIDER ***/


		// 42. PAGES Slider - Text Link Only
		jQuery('body').on('click', '.galfram-page-slider .galfram-slide .slide-cover-link', function () {
			var cat = 'Pages Slider - '+jQuery('.article-header h1').text()+' - '+jQuery(this).closest('.galfram-slider-outer').attr('data-slider-name');
			var action = 'Click';
			var label = 'Custom Link - '+event.target.href;
			handleOutboundLinkClicks( cat, action, label );
		});


		// 43. PAGES Slider - Controls - Dots
		jQuery('body').on('click', '.galfram-page-slider .slick-dots li button', function () {
			var cat = 'Pages Slider Controls - '+jQuery('.article-header h1').text()+' - '+jQuery(this).closest('.galfram-slider-outer').attr('data-slider-name');
			var action = 'Control Interaction';
			var li_index = jQuery(this).parent().index()+1;
			var label = 'Dots Button - '+li_index;
			handleOutboundLinkClicks( cat, action, label );
		});

		// 44. PAGES Slider - Controls - Previous Slide
		jQuery('body').on('click', '.galfram-page-slider .slick-prev', function () {
			var cat = 'Pages Slider Controls - '+jQuery('.article-header h1').text()+' - '+jQuery(this).closest('.galfram-slider-outer').attr('data-slider-name');
			var action = 'Control Interaction';
			var label = 'Previous Slide';
			handleOutboundLinkClicks( cat, action, label );
		});

		// 45. PAGES Slider - Controls - Next Slide
		jQuery('body').on('click', '.galfram-page-slider .slick-next', function () {
			var cat = 'Pages Slider Controls - '+jQuery('.article-header h1').text()+' - '+jQuery(this).closest('.galfram-slider-outer').attr('data-slider-name');
			var action = 'Control Interaction';
			var label = 'Next Slide';
			handleOutboundLinkClicks( cat, action, label );
		});


		/********* SLIDER EVENTS **************/
		/*** CUSTOM SLIDER ***/


		// 46. CUSTOM Slider - Text Link Only
		jQuery('body').on('click', '.galfram-custom-slider .galfram-slide a', function () {
			var cat = 'Custom Slider - '+jQuery('.article-header h1').text()+' - '+jQuery(this).closest('.galfram-slider-outer').attr('data-slider-name');
			var action = 'Click';
			var label = 'Custom Link - '+event.target.href;
			handleOutboundLinkClicks( cat, action, label );
		});

		// 47. CUSTOM Slider - Controls - Previous Slide
		jQuery('body').on('click', '.galfram-custom-slider .slick-prev', function () {
			var cat = 'Custom Slider Controls - '+jQuery('.article-header h1').text()+' - '+jQuery(this).closest('.galfram-slider-outer').attr('data-slider-name');
			var action = 'Control Interaction';
			var label = 'Previous Slide';
			handleOutboundLinkClicks( cat, action, label );
		});

		// 48. CUSTOM Slider - Controls - Next Slide
		jQuery('body').on('click', '.galfram-custom-slider .slick-next', function () {
			var cat = 'Custom Slider Controls - '+jQuery('.article-header h1').text()+' - '+jQuery(this).closest('.galfram-slider-outer').attr('data-slider-name');
			var action = 'Control Interaction';
			var label = 'Next Slide';
			handleOutboundLinkClicks( cat, action, label );
		});

		// 49. PAGES Slider - Controls - Dots
		jQuery('body').on('click', '.galfram-custom-slider .slick-dots li button', function () {
			var cat = 'Custom Slider Controls - '+jQuery('.article-header h1').text()+' - '+jQuery(this).closest('.galfram-slider-outer').attr('data-slider-name');
			var action = 'Control Interaction';
			var li_index = jQuery(this).parent().index()+1;
			var label = 'Dots Button - '+li_index;
			handleOutboundLinkClicks( cat, action, label );
		});


		/********* NAVIGATION EVENTS **************/
		/*** Site Nav ***/


		// 50. NAVIGATION - Logo
		jQuery('body').on('click', 'header.header .site-title', function () {
			var cat = 'Logo/Site Title Site Navigation - Page: '+jQuery('.article-header h1').text();
			var action = 'Logo/Site Title Click';
			var label = jQuery(this).attr('href');
			handleOutboundLinkClicks( cat, action, label );
		});

		// 51. NAVIGATION - Main Nav
		jQuery('body').on('click', '#menu-main-menu li a', function () {
			var cat = 'Main Site Navigation - Page: '+jQuery('.article-header h1').text();
			var action = 'Navigation Click';
			var label = event.target.text+' - '+event.target.href;
			handleOutboundLinkClicks( cat, action, label );
		});

		// 52. NAVIGATION - Top Nav
		jQuery('body').on('click', '#menu-top-nav-links li a', function () {
			var cat = 'Top Site Navigation - Page: '+jQuery('.article-header h1').text();
			var action = 'Navigation Click';
			var label = event.target.text+' - '+event.target.href;
			handleOutboundLinkClicks( cat, action, label );
		});

		// 53. NAVIGATION - Offcanvas Nav
		jQuery('body').on('click', '#menu-mobile-nav li a', function () {
			var cat = 'Mobile Site Navigation - Page: '+jQuery('.article-header h1').text();
			var action = 'Navigation Click';
			var label = event.target.text+' - '+event.target.href;
			handleOutboundLinkClicks( cat, action, label );
		});

		// 54. NAVIGATION - Sidebar Widget
		jQuery('body').on('click', '.sidebar .widget a', function () {
			var cat = 'Sidebar Widgets - Page: '+jQuery('.article-header h1').text();
			var action = jQuery(this).closest('.widget').attr('class')+' Click';
			//var label = jQuery(this).closest('.widget').attr('class');
			var label = event.target.text+' - '+event.target.href;
			handleOutboundLinkClicks( cat, action, label );
		});

		// 55. NAVIGATION - Footer Widget
		jQuery('body').on('click', '.footer-widgets .widget a', function () {
			var cat = 'Footer Widgets - Page: '+jQuery('.article-header h1').text();
			var action = jQuery(this).closest('.widget').attr('class')+' Click';
			//var label = jQuery(this).closest('.widget').attr('class');
			var label = event.target.text+' - '+event.target.href;
			handleOutboundLinkClicks( cat, action, label );
		});






		/********* TABLES EVENTS **************/
		/*** PRP TABLE ***/

		// 56. PRP Table - Spec Cover Link
		jQuery('body').on('click', '.galfram-prp-table .gtm-tablecell-coverlink', function () {
			var cat = 'Review Comparison Table - '+jQuery('.article-header h1').text()+' - '+jQuery(this).closest('.galfram-prp-table').attr('data-table-name');
			var action = 'Click';
			var label = 'Table Cell Cover Link - '+jQuery(this).closest('tr').attr('data-table-row')+'/'+jQuery(this).closest('td').attr('data-table-col')+' - '+event.target.href;
			handleOutboundLinkClicks( cat, action, label );
		});

		// 57. PRP Table - COLUMN SORT
		jQuery('body').on('click', '.galfram-prp-table th.footable-sortable', function () {
			var cat = 'Review Comparison Table - '+jQuery('.article-header h1').text()+' - '+jQuery(this).closest('.galfram-prp-table').attr('data-table-name');
			var action = 'Control Interaction';
			var th_index = jQuery(this).index()+1;
			var label = 'Column Sorted - Column '+th_index;
			handleOutboundLinkClicks( cat, action, label );
		});

		// 58. PRP Table - Pagination - First Page
		jQuery('body').on('click', '.galfram-prp-table li.footable-page-nav[data-page=first] .footable-page-link', function () {
			var cat = 'Review Comparison Table - '+jQuery('.article-header h1').text()+' - '+jQuery(this).closest('.galfram-prp-table').attr('data-table-name');
			var action = 'Control Interaction';			
			var label = 'Pagination Click - First Page';
			handleOutboundLinkClicks( cat, action, label );
		});

		// 59. PRP Table - Pagination - Last Page
		jQuery('body').on('click', '.galfram-prp-table li.footable-page-nav[data-page=last] .footable-page-link', function () {
			var cat = 'Review Comparison Table - '+jQuery('.article-header h1').text()+' - '+jQuery(this).closest('.galfram-prp-table').attr('data-table-name');
			var action = 'Control Interaction';			
			var label = 'Pagination Click - Last Page';
			handleOutboundLinkClicks( cat, action, label );
		});

		// 60. PRP Table - Pagination - Previous Page
		jQuery('body').on('click', '.galfram-prp-table li.footable-page-nav[data-page=prev] .footable-page-link', function () {
			var cat = 'Review Comparison Table - '+jQuery('.article-header h1').text()+' - '+jQuery(this).closest('.galfram-prp-table').attr('data-table-name');
			var action = 'Control Interaction';			
			var label = 'Pagination Click - Previous Page';
			handleOutboundLinkClicks( cat, action, label );
		});

		// 61. PRP Table - Pagination - Next Page
		jQuery('body').on('click', '.galfram-prp-table li.footable-page-nav[data-page=next] .footable-page-link', function () {
			var cat = 'Review Comparison Table - '+jQuery('.article-header h1').text()+' - '+jQuery(this).closest('.galfram-prp-table').attr('data-table-name');
			var action = 'Control Interaction';			
			var label = 'Pagination Click - Next Page';
			handleOutboundLinkClicks( cat, action, label );
		});

		// 62. PRP Table - Pagination - Page Link
		jQuery('body').on('click', '.galfram-prp-table li.footable-page .footable-page-link', function () {
			var cat = 'Review Comparison Table - '+jQuery('.article-header h1').text()+' - '+jQuery(this).closest('.galfram-prp-table').attr('data-table-name');
			var action = 'Control Interaction';			
			var li_index = jQuery(this).parent().index()-1;
			var label = 'Pagination Click - Page '+li_index;
			handleOutboundLinkClicks( cat, action, label );
		});

		// 63. PRP Table - Text Link Only
		jQuery('body').on('click', '.galfram-prp-table tbody tr td a:not(.gtm-spec-button, .gtm-sidebar-ama-btn)', function () {
			var cat = 'Review Comparison Table - '+jQuery('.article-header h1').text()+' - '+jQuery(this).closest('.galfram-prp-table').attr('data-table-name');
			var action = 'Click';
			var tr_index = jQuery(this).parent().index()+1;
			var td_index = jQuery(this).parent().parent().index()+1;
			var label = 'Custom Spec Text Link - Row '+tr_index+' / Column '+td_index+' - '+event.target.href;
			handleOutboundLinkClicks( cat, action, label );
		});

		// 64. PRP Table - Text Link Only
		jQuery('body').on('click', '.galfram-prp-table tbody tr td a.gtm-spec-button', function () {
			var cat = 'Review Comparison Table - '+jQuery('.article-header h1').text()+' - '+jQuery(this).closest('.galfram-prp-table').attr('data-table-name');
			var action = 'Click';
			var td_index = jQuery(this).parent().index()+1;
			var tr_index = jQuery(this).parent().parent().index()+1;
			if ( event.target.href ) {
				var target_url = event.target.href;
			} else {
				var target_url = jQuery(this).attr('href');
			}
			var label = 'Custom Spec Button - Row '+tr_index+' / Column '+td_index+' - '+target_url;
			handleOutboundLinkClicks( cat, action, label );
		});


		/*** CUSTOM TABLE ***/


		// 65. CUSTOM Table - COLUMN SORT
		jQuery('body').on('click', '.galfram-custom-table th.footable-sortable', function () {
			var cat = 'Custom Table - '+jQuery('.article-header h1').text()+' - '+jQuery(this).closest('.galfram-custom-table').attr('data-table-name');
			var action = 'Control Interaction';
			var th_index = jQuery(this).index()+1;
			var label = 'Column Sorted - Column '+th_index;
			handleOutboundLinkClicks( cat, action, label );
		});

		// 66. CUSTOM Table - Pagination - First Page
		jQuery('body').on('click', '.galfram-custom-table li.footable-page-nav[data-page=first] .footable-page-link', function () {
			var cat = 'Custom Table - '+jQuery('.article-header h1').text()+' - '+jQuery(this).closest('.galfram-custom-table').attr('data-table-name');
			var action = 'Control Interaction';			
			var label = 'Pagination Click - First Page';
			handleOutboundLinkClicks( cat, action, label );
		});

		// 67. CUSTOM Table - Pagination - Last Page
		jQuery('body').on('click', '.galfram-custom-table li.footable-page-nav[data-page=last] .footable-page-link', function () {
			var cat = 'Custom Table - '+jQuery('.article-header h1').text()+' - '+jQuery(this).closest('.galfram-custom-table').attr('data-table-name');
			var action = 'Control Interaction';			
			var label = 'Pagination Click - Last Page';
			handleOutboundLinkClicks( cat, action, label );
		});

		// 68. CUSTOM Table - Pagination - Previous Page
		jQuery('body').on('click', '.galfram-custom-table li.footable-page-nav[data-page=prev] .footable-page-link', function () {
			var cat = 'Custom Table - '+jQuery('.article-header h1').text()+' - '+jQuery(this).closest('.galfram-custom-table').attr('data-table-name');
			var action = 'Control Interaction';			
			var label = 'Pagination Click - Previous Page';
			handleOutboundLinkClicks( cat, action, label );
		});

		// 69. CUSTOM Table - Pagination - Next Page
		jQuery('body').on('click', '.galfram-custom-table li.footable-page-nav[data-page=next] .footable-page-link', function () {
			var cat = 'Custom Table - '+jQuery('.article-header h1').text()+' - '+jQuery(this).closest('.galfram-custom-table').attr('data-table-name');
			var action = 'Control Interaction';			
			var label = 'Pagination Click - Next Page';
			handleOutboundLinkClicks( cat, action, label );
		});

		// 70. CUSTOM Table - Pagination - Page Link
		jQuery('body').on('click', '.galfram-custom-table li.footable-page .footable-page-link', function () {
			var cat = 'Custom Table - '+jQuery('.article-header h1').text()+' - '+jQuery(this).closest('.galfram-custom-table').attr('data-table-name');
			var action = 'Control Interaction';			
			var li_index = jQuery(this).parent().index()-1;
			var label = 'Pagination Click - Page '+li_index;
			handleOutboundLinkClicks( cat, action, label );
		});

		// 71. CUSTOM Table - Text Link Only
		jQuery('body').on('click', '.galfram-custom-table tbody tr td.galfram-table-td-image-link a', function () {
			var cat = 'Custom Table - '+jQuery('.article-header h1').text()+' - '+jQuery(this).closest('.galfram-custom-table').attr('data-table-name');
			var action = 'Click';
			var td_index = jQuery(this).parent().index()+1;
			var tr_index = jQuery(this).parent().parent().index()+1;
			if ( event.target.href ) {
				var target_url = event.target.href;
			} else {
				var target_url = jQuery(this).attr('href');
			}
			var label = 'Image/Link - Row '+tr_index+' / Column '+td_index+' - '+target_url;
			handleOutboundLinkClicks( cat, action, label );
		});

		// 72. CUSTOM Table - Text Link Only
		jQuery('body').on('click', '.galfram-custom-table tbody tr td.galfram-table-td-text-link a', function () {
			var cat = 'Custom Table - '+jQuery('.article-header h1').text()+' - '+jQuery(this).closest('.galfram-custom-table').attr('data-table-name');
			var action = 'Click';
			var td_index = jQuery(this).parent().index()+1;
			var tr_index = jQuery(this).parent().parent().index()+1;
			if ( event.target.href ) {
				var target_url = event.target.href;
			} else {
				var target_url = jQuery(this).attr('href');
			}
			var label = 'Text Link - Row '+tr_index+' / Column '+td_index+' - '+target_url;
			handleOutboundLinkClicks( cat, action, label );
		});

		// 73. CUSTOM Table - Text Link Only
		jQuery('body').on('click', '.galfram-custom-table tbody tr td.galfram-table-td-wysiwyg a', function () {
			var cat = 'Custom Table - '+jQuery('.article-header h1').text()+' - '+jQuery(this).closest('.galfram-custom-table').attr('data-table-name');
			var action = 'Click';
			var td_index = jQuery(this).parent().index()+1;
			var tr_index = jQuery(this).parent().parent().index()+1;
			if ( event.target.href ) {
				var target_url = event.target.href;
			} else {
				var target_url = jQuery(this).attr('href');
			}
			var label = 'WYSIWYG/Text Link - Row '+tr_index+' / Column '+td_index+' - '+target_url;
			handleOutboundLinkClicks( cat, action, label );
		});



		function handleOutboundLinkClicks( cat, action, label ) {
			ga('send', 'event', {
				eventCategory: cat,
				eventAction: action,
				eventLabel: label,
				transport: 'beacon'
			});
		}



	}
	else {
		//console.log('GA NOT loaded');
	}
			





});


