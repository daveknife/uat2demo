<?php
/**
 * ultimateazon2
 *
 * WARNING: This file is part of the core Ultimate Azon Theme 2 Theme. DO NOT edit this file under any circumstances. Do all modifications in a child theme. http://codex.wordpress.org/Child_Themes
 *
 * @package ultimateazon2
 * @author  Dave Nicosia
 * @license GPL-2.0+
 * @link    https://ultimateazontheme.com
 */

/**
 * Used to initialize the framework in the various template files.
 *
 * It pulls in all the necessary components like header and footer, the basic
 * markup structure, and hooks.
 *
 * @since 1.0.0
 */
function ultimateazon2() {
	uat2_head(); // get the document head 
	// set our page builder body class if activated
	$body_class='';
	$pb = get_post_meta( get_the_ID(), 'enable_page_builder', true );
	if ( $pb ) {$body_class.=' uat-pb';}
	// set our event tracking body class
	$event_tracking = get_option( 'theme-options_enable_ga_event_tracking' );
	if ( $event_tracking ) {$body_class.=' ga-event-tracking';}
	?>
	<body <?php body_class($body_class); ?>>
	<?php uat2_after_opening_body_tag(); ?>
		<div class="off-canvas-content">
			<?php 
			uat2_before_header();
			uat2_header_function();
			uat2_after_header();
			$content_class = add_content_class();
			?>
			<div id="content" class="content<?php echo $content_class; ?>">
				<div class="content-inner">
				<?php uat2_loop(); ?>
				</div> <!-- end .content-inner -->
			</div> <!-- end #content -->
			<?php uat2_footer(); ?>
			<?php uat2_page_overlay(); ?>
		</div>  <!-- end .off-canvas-content -->
		<?php uat2_offcanvas(); ?>
		<?php wp_footer(); ?>
		<?php if ( get_option( 'theme-options_custom_footer_code' ) ) : ?>
			<?php echo get_option( 'theme-options_custom_footer_code' ); ?>
		<?php endif; ?>
	</body>
</html> <!-- end page -->
<?php
}